﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerator_1_t5722_il2cpp_TypeInfo;


// System.Array
#include "mscorlib_System_Array.h"

// T System.Collections.Generic.IEnumerator`1<System.Collections.IEnumerable>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Collections.IEnumerable>
extern MethodInfo IEnumerator_1_get_Current_m41307_MethodInfo;
static PropertyInfo IEnumerator_1_t5722____Current_PropertyInfo = 
{
	&IEnumerator_1_t5722_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41307_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5722_PropertyInfos[] =
{
	&IEnumerator_1_t5722____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEnumerable_t1126_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41307_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Collections.IEnumerable>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41307_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5722_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_t1126_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41307_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5722_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41307_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t5722_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5722_0_0_0;
extern Il2CppType IEnumerator_1_t5722_1_0_0;
struct IEnumerator_1_t5722;
extern Il2CppGenericClass IEnumerator_1_t5722_GenericClass;
TypeInfo IEnumerator_1_t5722_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5722_MethodInfos/* methods */
	, IEnumerator_1_t5722_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5722_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5722_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5722_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5722_0_0_0/* byval_arg */
	, &IEnumerator_1_t5722_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5722_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Collections.IEnumerable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_15.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2731_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Collections.IEnumerable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_15MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1493_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m14111_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7651_MethodInfo;
extern MethodInfo Array_get_Length_m7656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEnumerable_t1126_m31617_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m31436_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m31436(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<System.Collections.IEnumerable>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Collections.IEnumerable>(System.Int32)
#define Array_InternalArray__get_Item_TisIEnumerable_t1126_m31617(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Collections.IEnumerable>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2731____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2731_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2731, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2731____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2731_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2731, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2731_FieldInfos[] =
{
	&InternalEnumerator_1_t2731____array_0_FieldInfo,
	&InternalEnumerator_1_t2731____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14108_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2731____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2731_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14108_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2731____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2731_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14111_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2731_PropertyInfos[] =
{
	&InternalEnumerator_1_t2731____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2731____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2731_InternalEnumerator_1__ctor_m14107_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14107_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14107_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2731_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2731_InternalEnumerator_1__ctor_m14107_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14107_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14108_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14108_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2731_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14108_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14109_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14109_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2731_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14109_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14110_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14110_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2731_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14110_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_t1126_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14111_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Collections.IEnumerable>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14111_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2731_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_t1126_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14111_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2731_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14107_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14108_MethodInfo,
	&InternalEnumerator_1_Dispose_m14109_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14110_MethodInfo,
	&InternalEnumerator_1_get_Current_m14111_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m14110_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14109_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2731_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14108_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14110_MethodInfo,
	&InternalEnumerator_1_Dispose_m14109_MethodInfo,
	&InternalEnumerator_1_get_Current_m14111_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2731_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5722_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2731_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5722_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2731_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14111_MethodInfo/* Method Usage */,
	&IEnumerable_t1126_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEnumerable_t1126_m31617_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2731_0_0_0;
extern Il2CppType InternalEnumerator_1_t2731_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t2731_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t2731_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2731_MethodInfos/* methods */
	, InternalEnumerator_1_t2731_PropertyInfos/* properties */
	, InternalEnumerator_1_t2731_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2731_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2731_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2731_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2731_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2731_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2731_1_0_0/* this_arg */
	, InternalEnumerator_1_t2731_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2731_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2731_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2731)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7256_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Collections.IEnumerable>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Collections.IEnumerable>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Collections.IEnumerable>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Collections.IEnumerable>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Collections.IEnumerable>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Collections.IEnumerable>
extern MethodInfo IList_1_get_Item_m41308_MethodInfo;
extern MethodInfo IList_1_set_Item_m41309_MethodInfo;
static PropertyInfo IList_1_t7256____Item_PropertyInfo = 
{
	&IList_1_t7256_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41308_MethodInfo/* get */
	, &IList_1_set_Item_m41309_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7256_PropertyInfos[] =
{
	&IList_1_t7256____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEnumerable_t1126_0_0_0;
extern Il2CppType IEnumerable_t1126_0_0_0;
static ParameterInfo IList_1_t7256_IList_1_IndexOf_m41310_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_t1126_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41310_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Collections.IEnumerable>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41310_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7256_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7256_IList_1_IndexOf_m41310_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41310_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEnumerable_t1126_0_0_0;
static ParameterInfo IList_1_t7256_IList_1_Insert_m41311_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEnumerable_t1126_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41311_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Collections.IEnumerable>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41311_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7256_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7256_IList_1_Insert_m41311_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41311_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7256_IList_1_RemoveAt_m41312_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41312_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Collections.IEnumerable>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41312_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7256_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7256_IList_1_RemoveAt_m41312_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41312_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7256_IList_1_get_Item_m41308_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEnumerable_t1126_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41308_GenericMethod;
// T System.Collections.Generic.IList`1<System.Collections.IEnumerable>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41308_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7256_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerable_t1126_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7256_IList_1_get_Item_m41308_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41308_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEnumerable_t1126_0_0_0;
static ParameterInfo IList_1_t7256_IList_1_set_Item_m41309_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEnumerable_t1126_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41309_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Collections.IEnumerable>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41309_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7256_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7256_IList_1_set_Item_m41309_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41309_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7256_MethodInfos[] =
{
	&IList_1_IndexOf_m41310_MethodInfo,
	&IList_1_Insert_m41311_MethodInfo,
	&IList_1_RemoveAt_m41312_MethodInfo,
	&IList_1_get_Item_m41308_MethodInfo,
	&IList_1_set_Item_m41309_MethodInfo,
	NULL
};
extern TypeInfo ICollection_1_t7255_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7257_il2cpp_TypeInfo;
static TypeInfo* IList_1_t7256_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7255_il2cpp_TypeInfo,
	&IEnumerable_1_t7257_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7256_0_0_0;
extern Il2CppType IList_1_t7256_1_0_0;
struct IList_1_t7256;
extern Il2CppGenericClass IList_1_t7256_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7256_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7256_MethodInfos/* methods */
	, IList_1_t7256_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7256_il2cpp_TypeInfo/* element_class */
	, IList_1_t7256_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7256_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7256_0_0_0/* byval_arg */
	, &IList_1_t7256_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7256_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7258_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.ICloneable>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.ICloneable>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.ICloneable>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.ICloneable>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.ICloneable>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.ICloneable>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.ICloneable>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.ICloneable>
extern MethodInfo ICollection_1_get_Count_m41313_MethodInfo;
static PropertyInfo ICollection_1_t7258____Count_PropertyInfo = 
{
	&ICollection_1_t7258_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41313_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41314_MethodInfo;
static PropertyInfo ICollection_1_t7258____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7258_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41314_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7258_PropertyInfos[] =
{
	&ICollection_1_t7258____Count_PropertyInfo,
	&ICollection_1_t7258____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41313_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.ICloneable>::get_Count()
MethodInfo ICollection_1_get_Count_m41313_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7258_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41313_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41314_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ICloneable>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41314_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7258_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41314_GenericMethod/* genericMethod */

};
extern Il2CppType ICloneable_t481_0_0_0;
extern Il2CppType ICloneable_t481_0_0_0;
static ParameterInfo ICollection_1_t7258_ICollection_1_Add_m41315_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ICloneable_t481_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41315_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ICloneable>::Add(T)
MethodInfo ICollection_1_Add_m41315_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7258_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7258_ICollection_1_Add_m41315_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41315_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41316_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ICloneable>::Clear()
MethodInfo ICollection_1_Clear_m41316_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7258_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41316_GenericMethod/* genericMethod */

};
extern Il2CppType ICloneable_t481_0_0_0;
static ParameterInfo ICollection_1_t7258_ICollection_1_Contains_m41317_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ICloneable_t481_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41317_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ICloneable>::Contains(T)
MethodInfo ICollection_1_Contains_m41317_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7258_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7258_ICollection_1_Contains_m41317_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41317_GenericMethod/* genericMethod */

};
extern Il2CppType ICloneableU5BU5D_t5207_0_0_0;
extern Il2CppType ICloneableU5BU5D_t5207_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7258_ICollection_1_CopyTo_m41318_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ICloneableU5BU5D_t5207_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41318_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ICloneable>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41318_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7258_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7258_ICollection_1_CopyTo_m41318_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41318_GenericMethod/* genericMethod */

};
extern Il2CppType ICloneable_t481_0_0_0;
static ParameterInfo ICollection_1_t7258_ICollection_1_Remove_m41319_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ICloneable_t481_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41319_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ICloneable>::Remove(T)
MethodInfo ICollection_1_Remove_m41319_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7258_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7258_ICollection_1_Remove_m41319_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41319_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7258_MethodInfos[] =
{
	&ICollection_1_get_Count_m41313_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41314_MethodInfo,
	&ICollection_1_Add_m41315_MethodInfo,
	&ICollection_1_Clear_m41316_MethodInfo,
	&ICollection_1_Contains_m41317_MethodInfo,
	&ICollection_1_CopyTo_m41318_MethodInfo,
	&ICollection_1_Remove_m41319_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7260_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7258_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7260_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7258_0_0_0;
extern Il2CppType ICollection_1_t7258_1_0_0;
struct ICollection_1_t7258;
extern Il2CppGenericClass ICollection_1_t7258_GenericClass;
TypeInfo ICollection_1_t7258_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7258_MethodInfos/* methods */
	, ICollection_1_t7258_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7258_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7258_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7258_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7258_0_0_0/* byval_arg */
	, &ICollection_1_t7258_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7258_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ICloneable>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ICloneable>
extern Il2CppType IEnumerator_1_t5724_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41320_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ICloneable>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41320_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7260_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5724_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41320_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7260_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41320_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7260_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7260_0_0_0;
extern Il2CppType IEnumerable_1_t7260_1_0_0;
struct IEnumerable_1_t7260;
extern Il2CppGenericClass IEnumerable_1_t7260_GenericClass;
TypeInfo IEnumerable_1_t7260_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7260_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7260_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7260_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7260_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7260_0_0_0/* byval_arg */
	, &IEnumerable_1_t7260_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7260_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5724_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.ICloneable>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ICloneable>
extern MethodInfo IEnumerator_1_get_Current_m41321_MethodInfo;
static PropertyInfo IEnumerator_1_t5724____Current_PropertyInfo = 
{
	&IEnumerator_1_t5724_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41321_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5724_PropertyInfos[] =
{
	&IEnumerator_1_t5724____Current_PropertyInfo,
	NULL
};
extern Il2CppType ICloneable_t481_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41321_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.ICloneable>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41321_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5724_il2cpp_TypeInfo/* declaring_type */
	, &ICloneable_t481_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41321_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5724_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41321_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5724_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5724_0_0_0;
extern Il2CppType IEnumerator_1_t5724_1_0_0;
struct IEnumerator_1_t5724;
extern Il2CppGenericClass IEnumerator_1_t5724_GenericClass;
TypeInfo IEnumerator_1_t5724_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5724_MethodInfos/* methods */
	, IEnumerator_1_t5724_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5724_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5724_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5724_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5724_0_0_0/* byval_arg */
	, &IEnumerator_1_t5724_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5724_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.ICloneable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_16.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2732_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.ICloneable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_16MethodDeclarations.h"

extern TypeInfo ICloneable_t481_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14116_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisICloneable_t481_m31628_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.ICloneable>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.ICloneable>(System.Int32)
#define Array_InternalArray__get_Item_TisICloneable_t481_m31628(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.ICloneable>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.ICloneable>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.ICloneable>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.ICloneable>::MoveNext()
// T System.Array/InternalEnumerator`1<System.ICloneable>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.ICloneable>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2732____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2732_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2732, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2732____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2732_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2732, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2732_FieldInfos[] =
{
	&InternalEnumerator_1_t2732____array_0_FieldInfo,
	&InternalEnumerator_1_t2732____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14113_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2732____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2732_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14113_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2732____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2732_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14116_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2732_PropertyInfos[] =
{
	&InternalEnumerator_1_t2732____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2732____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2732_InternalEnumerator_1__ctor_m14112_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14112_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ICloneable>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14112_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2732_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2732_InternalEnumerator_1__ctor_m14112_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14112_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14113_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.ICloneable>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14113_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2732_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14113_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14114_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ICloneable>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14114_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2732_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14114_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14115_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.ICloneable>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14115_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2732_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14115_GenericMethod/* genericMethod */

};
extern Il2CppType ICloneable_t481_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14116_GenericMethod;
// T System.Array/InternalEnumerator`1<System.ICloneable>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14116_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2732_il2cpp_TypeInfo/* declaring_type */
	, &ICloneable_t481_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14116_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2732_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14112_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14113_MethodInfo,
	&InternalEnumerator_1_Dispose_m14114_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14115_MethodInfo,
	&InternalEnumerator_1_get_Current_m14116_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14115_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14114_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2732_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14113_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14115_MethodInfo,
	&InternalEnumerator_1_Dispose_m14114_MethodInfo,
	&InternalEnumerator_1_get_Current_m14116_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2732_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5724_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2732_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5724_il2cpp_TypeInfo, 7},
};
extern TypeInfo ICloneable_t481_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2732_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14116_MethodInfo/* Method Usage */,
	&ICloneable_t481_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisICloneable_t481_m31628_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2732_0_0_0;
extern Il2CppType InternalEnumerator_1_t2732_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2732_GenericClass;
TypeInfo InternalEnumerator_1_t2732_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2732_MethodInfos/* methods */
	, InternalEnumerator_1_t2732_PropertyInfos/* properties */
	, InternalEnumerator_1_t2732_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2732_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2732_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2732_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2732_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2732_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2732_1_0_0/* this_arg */
	, InternalEnumerator_1_t2732_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2732_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2732_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2732)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7259_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.ICloneable>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.ICloneable>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.ICloneable>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.ICloneable>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.ICloneable>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.ICloneable>
extern MethodInfo IList_1_get_Item_m41322_MethodInfo;
extern MethodInfo IList_1_set_Item_m41323_MethodInfo;
static PropertyInfo IList_1_t7259____Item_PropertyInfo = 
{
	&IList_1_t7259_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41322_MethodInfo/* get */
	, &IList_1_set_Item_m41323_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7259_PropertyInfos[] =
{
	&IList_1_t7259____Item_PropertyInfo,
	NULL
};
extern Il2CppType ICloneable_t481_0_0_0;
static ParameterInfo IList_1_t7259_IList_1_IndexOf_m41324_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ICloneable_t481_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41324_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.ICloneable>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41324_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7259_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7259_IList_1_IndexOf_m41324_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41324_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ICloneable_t481_0_0_0;
static ParameterInfo IList_1_t7259_IList_1_Insert_m41325_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ICloneable_t481_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41325_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ICloneable>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41325_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7259_IList_1_Insert_m41325_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41325_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7259_IList_1_RemoveAt_m41326_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41326_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ICloneable>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41326_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7259_IList_1_RemoveAt_m41326_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41326_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7259_IList_1_get_Item_m41322_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ICloneable_t481_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41322_GenericMethod;
// T System.Collections.Generic.IList`1<System.ICloneable>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41322_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7259_il2cpp_TypeInfo/* declaring_type */
	, &ICloneable_t481_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7259_IList_1_get_Item_m41322_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41322_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ICloneable_t481_0_0_0;
static ParameterInfo IList_1_t7259_IList_1_set_Item_m41323_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ICloneable_t481_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41323_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ICloneable>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41323_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7259_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7259_IList_1_set_Item_m41323_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41323_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7259_MethodInfos[] =
{
	&IList_1_IndexOf_m41324_MethodInfo,
	&IList_1_Insert_m41325_MethodInfo,
	&IList_1_RemoveAt_m41326_MethodInfo,
	&IList_1_get_Item_m41322_MethodInfo,
	&IList_1_set_Item_m41323_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7259_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7258_il2cpp_TypeInfo,
	&IEnumerable_1_t7260_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7259_0_0_0;
extern Il2CppType IList_1_t7259_1_0_0;
struct IList_1_t7259;
extern Il2CppGenericClass IList_1_t7259_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7259_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7259_MethodInfos/* methods */
	, IList_1_t7259_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7259_il2cpp_TypeInfo/* element_class */
	, IList_1_t7259_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7259_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7259_0_0_0/* byval_arg */
	, &IList_1_t7259_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7259_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7261_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>
extern MethodInfo ICollection_1_get_Count_m41327_MethodInfo;
static PropertyInfo ICollection_1_t7261____Count_PropertyInfo = 
{
	&ICollection_1_t7261_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41327_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41328_MethodInfo;
static PropertyInfo ICollection_1_t7261____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7261_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41328_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7261_PropertyInfos[] =
{
	&ICollection_1_t7261____Count_PropertyInfo,
	&ICollection_1_t7261____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41327_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::get_Count()
MethodInfo ICollection_1_get_Count_m41327_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7261_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41327_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41328_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41328_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7261_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41328_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2322_0_0_0;
extern Il2CppType IComparable_1_t2322_0_0_0;
static ParameterInfo ICollection_1_t7261_ICollection_1_Add_m41329_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2322_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41329_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::Add(T)
MethodInfo ICollection_1_Add_m41329_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7261_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7261_ICollection_1_Add_m41329_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41329_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41330_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::Clear()
MethodInfo ICollection_1_Clear_m41330_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7261_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41330_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2322_0_0_0;
static ParameterInfo ICollection_1_t7261_ICollection_1_Contains_m41331_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2322_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41331_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::Contains(T)
MethodInfo ICollection_1_Contains_m41331_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7261_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7261_ICollection_1_Contains_m41331_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41331_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1U5BU5D_t5208_0_0_0;
extern Il2CppType IComparable_1U5BU5D_t5208_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7261_ICollection_1_CopyTo_m41332_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1U5BU5D_t5208_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41332_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41332_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7261_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7261_ICollection_1_CopyTo_m41332_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41332_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2322_0_0_0;
static ParameterInfo ICollection_1_t7261_ICollection_1_Remove_m41333_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2322_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41333_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.String>>::Remove(T)
MethodInfo ICollection_1_Remove_m41333_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7261_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7261_ICollection_1_Remove_m41333_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41333_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7261_MethodInfos[] =
{
	&ICollection_1_get_Count_m41327_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41328_MethodInfo,
	&ICollection_1_Add_m41329_MethodInfo,
	&ICollection_1_Clear_m41330_MethodInfo,
	&ICollection_1_Contains_m41331_MethodInfo,
	&ICollection_1_CopyTo_m41332_MethodInfo,
	&ICollection_1_Remove_m41333_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7263_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7261_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7263_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7261_0_0_0;
extern Il2CppType ICollection_1_t7261_1_0_0;
struct ICollection_1_t7261;
extern Il2CppGenericClass ICollection_1_t7261_GenericClass;
TypeInfo ICollection_1_t7261_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7261_MethodInfos/* methods */
	, ICollection_1_t7261_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7261_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7261_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7261_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7261_0_0_0/* byval_arg */
	, &ICollection_1_t7261_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7261_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.String>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.String>>
extern Il2CppType IEnumerator_1_t5726_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41334_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.String>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41334_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7263_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5726_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41334_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7263_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41334_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7263_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7263_0_0_0;
extern Il2CppType IEnumerable_1_t7263_1_0_0;
struct IEnumerable_1_t7263;
extern Il2CppGenericClass IEnumerable_1_t7263_GenericClass;
TypeInfo IEnumerable_1_t7263_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7263_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7263_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7263_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7263_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7263_0_0_0/* byval_arg */
	, &IEnumerable_1_t7263_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7263_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5726_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.String>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.String>>
extern MethodInfo IEnumerator_1_get_Current_m41335_MethodInfo;
static PropertyInfo IEnumerator_1_t5726____Current_PropertyInfo = 
{
	&IEnumerator_1_t5726_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41335_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5726_PropertyInfos[] =
{
	&IEnumerator_1_t5726____Current_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2322_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41335_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.String>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41335_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5726_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2322_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41335_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5726_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41335_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5726_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5726_0_0_0;
extern Il2CppType IEnumerator_1_t5726_1_0_0;
struct IEnumerator_1_t5726;
extern Il2CppGenericClass IEnumerator_1_t5726_GenericClass;
TypeInfo IEnumerator_1_t5726_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5726_MethodInfos/* methods */
	, IEnumerator_1_t5726_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5726_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5726_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5726_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5726_0_0_0/* byval_arg */
	, &IEnumerator_1_t5726_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5726_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparable_1_t2322_il2cpp_TypeInfo;



// System.Int32 System.IComparable`1<System.String>::CompareTo(T)
// Metadata Definition System.IComparable`1<System.String>
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo IComparable_1_t2322_IComparable_1_CompareTo_m35181_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m35181_GenericMethod;
// System.Int32 System.IComparable`1<System.String>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m35181_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t2322_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IComparable_1_t2322_IComparable_1_CompareTo_m35181_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m35181_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t2322_MethodInfos[] =
{
	&IComparable_1_CompareTo_m35181_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t2322_1_0_0;
struct IComparable_1_t2322;
extern Il2CppGenericClass IComparable_1_t2322_GenericClass;
TypeInfo IComparable_1_t2322_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t2322_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t2322_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t2322_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t2322_0_0_0/* byval_arg */
	, &IComparable_1_t2322_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t2322_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparable_1_t5829_il2cpp_TypeInfo;



// System.Int32 System.IComparable`1<System.Object>::CompareTo(T)
// Metadata Definition System.IComparable`1<System.Object>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IComparable_1_t5829_IComparable_1_CompareTo_m32285_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m32285_GenericMethod;
// System.Int32 System.IComparable`1<System.Object>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m32285_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t5829_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IComparable_1_t5829_IComparable_1_CompareTo_m32285_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m32285_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t5829_MethodInfos[] =
{
	&IComparable_1_CompareTo_m32285_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t5829_0_0_0;
extern Il2CppType IComparable_1_t5829_1_0_0;
struct IComparable_1_t5829;
extern Il2CppGenericClass IComparable_1_t5829_GenericClass;
TypeInfo IComparable_1_t5829_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t5829_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t5829_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t5829_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t5829_0_0_0/* byval_arg */
	, &IComparable_1_t5829_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t5829_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_17.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2733_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_17MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m14121_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIComparable_1_t2322_m31639_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.String>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.String>>(System.Int32)
#define Array_InternalArray__get_Item_TisIComparable_1_t2322_m31639(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2733____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2733_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2733, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2733____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2733_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2733, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2733_FieldInfos[] =
{
	&InternalEnumerator_1_t2733____array_0_FieldInfo,
	&InternalEnumerator_1_t2733____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14118_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2733____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2733_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14118_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2733____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2733_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14121_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2733_PropertyInfos[] =
{
	&InternalEnumerator_1_t2733____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2733____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2733_InternalEnumerator_1__ctor_m14117_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14117_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14117_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2733_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2733_InternalEnumerator_1__ctor_m14117_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14117_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14118_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14118_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2733_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14118_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14119_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14119_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2733_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14119_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14120_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14120_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2733_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14120_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2322_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14121_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.String>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14121_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2733_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2322_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14121_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2733_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14117_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14118_MethodInfo,
	&InternalEnumerator_1_Dispose_m14119_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14120_MethodInfo,
	&InternalEnumerator_1_get_Current_m14121_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14120_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14119_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2733_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14118_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14120_MethodInfo,
	&InternalEnumerator_1_Dispose_m14119_MethodInfo,
	&InternalEnumerator_1_get_Current_m14121_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2733_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5726_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2733_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5726_il2cpp_TypeInfo, 7},
};
extern TypeInfo IComparable_1_t2322_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2733_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14121_MethodInfo/* Method Usage */,
	&IComparable_1_t2322_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIComparable_1_t2322_m31639_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2733_0_0_0;
extern Il2CppType InternalEnumerator_1_t2733_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2733_GenericClass;
TypeInfo InternalEnumerator_1_t2733_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2733_MethodInfos/* methods */
	, InternalEnumerator_1_t2733_PropertyInfos/* properties */
	, InternalEnumerator_1_t2733_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2733_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2733_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2733_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2733_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2733_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2733_1_0_0/* this_arg */
	, InternalEnumerator_1_t2733_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2733_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2733_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2733)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7262_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.String>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.String>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.String>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IComparable`1<System.String>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.String>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.String>>
extern MethodInfo IList_1_get_Item_m41336_MethodInfo;
extern MethodInfo IList_1_set_Item_m41337_MethodInfo;
static PropertyInfo IList_1_t7262____Item_PropertyInfo = 
{
	&IList_1_t7262_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41336_MethodInfo/* get */
	, &IList_1_set_Item_m41337_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7262_PropertyInfos[] =
{
	&IList_1_t7262____Item_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2322_0_0_0;
static ParameterInfo IList_1_t7262_IList_1_IndexOf_m41338_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2322_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41338_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.String>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41338_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7262_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7262_IList_1_IndexOf_m41338_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41338_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IComparable_1_t2322_0_0_0;
static ParameterInfo IList_1_t7262_IList_1_Insert_m41339_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2322_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41339_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.String>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41339_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7262_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7262_IList_1_Insert_m41339_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41339_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7262_IList_1_RemoveAt_m41340_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41340_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.String>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41340_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7262_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7262_IList_1_RemoveAt_m41340_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41340_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7262_IList_1_get_Item_m41336_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IComparable_1_t2322_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41336_GenericMethod;
// T System.Collections.Generic.IList`1<System.IComparable`1<System.String>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41336_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7262_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2322_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7262_IList_1_get_Item_m41336_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41336_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IComparable_1_t2322_0_0_0;
static ParameterInfo IList_1_t7262_IList_1_set_Item_m41337_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2322_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41337_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.String>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41337_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7262_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7262_IList_1_set_Item_m41337_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41337_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7262_MethodInfos[] =
{
	&IList_1_IndexOf_m41338_MethodInfo,
	&IList_1_Insert_m41339_MethodInfo,
	&IList_1_RemoveAt_m41340_MethodInfo,
	&IList_1_get_Item_m41336_MethodInfo,
	&IList_1_set_Item_m41337_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7262_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7261_il2cpp_TypeInfo,
	&IEnumerable_1_t7263_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7262_0_0_0;
extern Il2CppType IList_1_t7262_1_0_0;
struct IList_1_t7262;
extern Il2CppGenericClass IList_1_t7262_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7262_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7262_MethodInfos/* methods */
	, IList_1_t7262_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7262_il2cpp_TypeInfo/* element_class */
	, IList_1_t7262_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7262_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7262_0_0_0/* byval_arg */
	, &IList_1_t7262_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7262_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7264_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>
extern MethodInfo ICollection_1_get_Count_m41341_MethodInfo;
static PropertyInfo ICollection_1_t7264____Count_PropertyInfo = 
{
	&ICollection_1_t7264_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41341_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41342_MethodInfo;
static PropertyInfo ICollection_1_t7264____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7264_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41342_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7264_PropertyInfos[] =
{
	&ICollection_1_t7264____Count_PropertyInfo,
	&ICollection_1_t7264____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41341_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::get_Count()
MethodInfo ICollection_1_get_Count_m41341_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7264_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41341_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41342_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41342_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7264_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41342_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2323_0_0_0;
extern Il2CppType IEquatable_1_t2323_0_0_0;
static ParameterInfo ICollection_1_t7264_ICollection_1_Add_m41343_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2323_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41343_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::Add(T)
MethodInfo ICollection_1_Add_m41343_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7264_ICollection_1_Add_m41343_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41343_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41344_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::Clear()
MethodInfo ICollection_1_Clear_m41344_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41344_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2323_0_0_0;
static ParameterInfo ICollection_1_t7264_ICollection_1_Contains_m41345_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2323_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41345_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::Contains(T)
MethodInfo ICollection_1_Contains_m41345_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7264_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7264_ICollection_1_Contains_m41345_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41345_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1U5BU5D_t5209_0_0_0;
extern Il2CppType IEquatable_1U5BU5D_t5209_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7264_ICollection_1_CopyTo_m41346_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1U5BU5D_t5209_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41346_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41346_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7264_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7264_ICollection_1_CopyTo_m41346_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41346_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2323_0_0_0;
static ParameterInfo ICollection_1_t7264_ICollection_1_Remove_m41347_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2323_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41347_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.String>>::Remove(T)
MethodInfo ICollection_1_Remove_m41347_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7264_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7264_ICollection_1_Remove_m41347_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41347_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7264_MethodInfos[] =
{
	&ICollection_1_get_Count_m41341_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41342_MethodInfo,
	&ICollection_1_Add_m41343_MethodInfo,
	&ICollection_1_Clear_m41344_MethodInfo,
	&ICollection_1_Contains_m41345_MethodInfo,
	&ICollection_1_CopyTo_m41346_MethodInfo,
	&ICollection_1_Remove_m41347_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7266_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7264_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7266_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7264_0_0_0;
extern Il2CppType ICollection_1_t7264_1_0_0;
struct ICollection_1_t7264;
extern Il2CppGenericClass ICollection_1_t7264_GenericClass;
TypeInfo ICollection_1_t7264_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7264_MethodInfos/* methods */
	, ICollection_1_t7264_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7264_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7264_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7264_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7264_0_0_0/* byval_arg */
	, &ICollection_1_t7264_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7264_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.String>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.String>>
extern Il2CppType IEnumerator_1_t5728_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41348_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.String>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41348_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7266_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5728_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41348_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7266_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41348_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7266_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7266_0_0_0;
extern Il2CppType IEnumerable_1_t7266_1_0_0;
struct IEnumerable_1_t7266;
extern Il2CppGenericClass IEnumerable_1_t7266_GenericClass;
TypeInfo IEnumerable_1_t7266_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7266_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7266_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7266_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7266_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7266_0_0_0/* byval_arg */
	, &IEnumerable_1_t7266_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7266_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5728_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.String>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.String>>
extern MethodInfo IEnumerator_1_get_Current_m41349_MethodInfo;
static PropertyInfo IEnumerator_1_t5728____Current_PropertyInfo = 
{
	&IEnumerator_1_t5728_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41349_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5728_PropertyInfos[] =
{
	&IEnumerator_1_t5728____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2323_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41349_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.String>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41349_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5728_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2323_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41349_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5728_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41349_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5728_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5728_0_0_0;
extern Il2CppType IEnumerator_1_t5728_1_0_0;
struct IEnumerator_1_t5728;
extern Il2CppGenericClass IEnumerator_1_t5728_GenericClass;
TypeInfo IEnumerator_1_t5728_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5728_MethodInfos/* methods */
	, IEnumerator_1_t5728_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5728_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5728_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5728_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5728_0_0_0/* byval_arg */
	, &IEnumerator_1_t5728_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5728_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEquatable_1_t2323_il2cpp_TypeInfo;



// System.Boolean System.IEquatable`1<System.String>::Equals(T)
// Metadata Definition System.IEquatable`1<System.String>
extern Il2CppType String_t_0_0_0;
static ParameterInfo IEquatable_1_t2323_IEquatable_1_Equals_m41350_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEquatable_1_Equals_m41350_GenericMethod;
// System.Boolean System.IEquatable`1<System.String>::Equals(T)
MethodInfo IEquatable_1_Equals_m41350_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEquatable_1_t2323_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, IEquatable_1_t2323_IEquatable_1_Equals_m41350_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEquatable_1_Equals_m41350_GenericMethod/* genericMethod */

};
static MethodInfo* IEquatable_1_t2323_MethodInfos[] =
{
	&IEquatable_1_Equals_m41350_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1_t2323_1_0_0;
struct IEquatable_1_t2323;
extern Il2CppGenericClass IEquatable_1_t2323_GenericClass;
TypeInfo IEquatable_1_t2323_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, IEquatable_1_t2323_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t2323_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1_t2323_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1_t2323_0_0_0/* byval_arg */
	, &IEquatable_1_t2323_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEquatable_1_t2323_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEquatable_1_t9274_il2cpp_TypeInfo;



// System.Boolean System.IEquatable`1<System.Object>::Equals(T)
// Metadata Definition System.IEquatable`1<System.Object>
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IEquatable_1_t9274_IEquatable_1_Equals_m41351_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEquatable_1_Equals_m41351_GenericMethod;
// System.Boolean System.IEquatable`1<System.Object>::Equals(T)
MethodInfo IEquatable_1_Equals_m41351_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEquatable_1_t9274_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, IEquatable_1_t9274_IEquatable_1_Equals_m41351_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEquatable_1_Equals_m41351_GenericMethod/* genericMethod */

};
static MethodInfo* IEquatable_1_t9274_MethodInfos[] =
{
	&IEquatable_1_Equals_m41351_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1_t9274_0_0_0;
extern Il2CppType IEquatable_1_t9274_1_0_0;
struct IEquatable_1_t9274;
extern Il2CppGenericClass IEquatable_1_t9274_GenericClass;
TypeInfo IEquatable_1_t9274_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, IEquatable_1_t9274_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t9274_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1_t9274_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1_t9274_0_0_0/* byval_arg */
	, &IEquatable_1_t9274_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEquatable_1_t9274_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_18.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2734_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_18MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m14126_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEquatable_1_t2323_m31650_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.String>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.String>>(System.Int32)
#define Array_InternalArray__get_Item_TisIEquatable_1_t2323_m31650(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2734____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2734_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2734, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2734____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2734_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2734, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2734_FieldInfos[] =
{
	&InternalEnumerator_1_t2734____array_0_FieldInfo,
	&InternalEnumerator_1_t2734____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14123_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2734____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2734_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14123_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2734____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2734_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14126_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2734_PropertyInfos[] =
{
	&InternalEnumerator_1_t2734____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2734____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2734_InternalEnumerator_1__ctor_m14122_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14122_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14122_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2734_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2734_InternalEnumerator_1__ctor_m14122_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14122_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14123_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14123_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2734_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14123_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14124_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14124_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2734_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14124_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14125_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14125_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2734_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14125_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2323_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14126_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.String>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14126_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2734_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2323_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14126_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2734_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14122_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14123_MethodInfo,
	&InternalEnumerator_1_Dispose_m14124_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14125_MethodInfo,
	&InternalEnumerator_1_get_Current_m14126_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14125_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14124_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2734_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14123_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14125_MethodInfo,
	&InternalEnumerator_1_Dispose_m14124_MethodInfo,
	&InternalEnumerator_1_get_Current_m14126_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2734_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5728_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2734_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5728_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEquatable_1_t2323_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2734_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14126_MethodInfo/* Method Usage */,
	&IEquatable_1_t2323_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEquatable_1_t2323_m31650_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2734_0_0_0;
extern Il2CppType InternalEnumerator_1_t2734_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2734_GenericClass;
TypeInfo InternalEnumerator_1_t2734_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2734_MethodInfos/* methods */
	, InternalEnumerator_1_t2734_PropertyInfos/* properties */
	, InternalEnumerator_1_t2734_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2734_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2734_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2734_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2734_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2734_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2734_1_0_0/* this_arg */
	, InternalEnumerator_1_t2734_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2734_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2734_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2734)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7265_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.String>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.String>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.String>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.String>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.String>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.String>>
extern MethodInfo IList_1_get_Item_m41352_MethodInfo;
extern MethodInfo IList_1_set_Item_m41353_MethodInfo;
static PropertyInfo IList_1_t7265____Item_PropertyInfo = 
{
	&IList_1_t7265_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41352_MethodInfo/* get */
	, &IList_1_set_Item_m41353_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7265_PropertyInfos[] =
{
	&IList_1_t7265____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2323_0_0_0;
static ParameterInfo IList_1_t7265_IList_1_IndexOf_m41354_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2323_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41354_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.String>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41354_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7265_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7265_IList_1_IndexOf_m41354_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41354_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEquatable_1_t2323_0_0_0;
static ParameterInfo IList_1_t7265_IList_1_Insert_m41355_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2323_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41355_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.String>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41355_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7265_IList_1_Insert_m41355_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41355_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7265_IList_1_RemoveAt_m41356_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41356_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.String>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41356_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7265_IList_1_RemoveAt_m41356_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41356_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7265_IList_1_get_Item_m41352_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEquatable_1_t2323_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41352_GenericMethod;
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.String>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41352_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7265_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2323_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7265_IList_1_get_Item_m41352_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41352_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEquatable_1_t2323_0_0_0;
static ParameterInfo IList_1_t7265_IList_1_set_Item_m41353_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2323_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41353_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.String>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41353_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7265_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7265_IList_1_set_Item_m41353_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41353_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7265_MethodInfos[] =
{
	&IList_1_IndexOf_m41354_MethodInfo,
	&IList_1_Insert_m41355_MethodInfo,
	&IList_1_RemoveAt_m41356_MethodInfo,
	&IList_1_get_Item_m41352_MethodInfo,
	&IList_1_set_Item_m41353_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7265_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7264_il2cpp_TypeInfo,
	&IEnumerable_1_t7266_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7265_0_0_0;
extern Il2CppType IList_1_t7265_1_0_0;
struct IList_1_t7265;
extern Il2CppGenericClass IList_1_t7265_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7265_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7265_MethodInfos/* methods */
	, IList_1_t7265_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7265_il2cpp_TypeInfo/* element_class */
	, IList_1_t7265_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7265_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7265_0_0_0/* byval_arg */
	, &IList_1_t7265_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7265_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t1689_il2cpp_TypeInfo;

// System.Char
#include "mscorlib_System_Char.h"


// T System.Collections.Generic.IEnumerator`1<System.Char>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Char>
extern MethodInfo IEnumerator_1_get_Current_m41357_MethodInfo;
static PropertyInfo IEnumerator_1_t1689____Current_PropertyInfo = 
{
	&IEnumerator_1_t1689_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41357_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t1689_PropertyInfos[] =
{
	&IEnumerator_1_t1689____Current_PropertyInfo,
	NULL
};
extern Il2CppType Char_t109_0_0_0;
extern void* RuntimeInvoker_Char_t109 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41357_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Char>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41357_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t1689_il2cpp_TypeInfo/* declaring_type */
	, &Char_t109_0_0_0/* return_type */
	, RuntimeInvoker_Char_t109/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41357_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t1689_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41357_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t1689_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t1689_0_0_0;
extern Il2CppType IEnumerator_1_t1689_1_0_0;
struct IEnumerator_1_t1689;
extern Il2CppGenericClass IEnumerator_1_t1689_GenericClass;
TypeInfo IEnumerator_1_t1689_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t1689_MethodInfos/* methods */
	, IEnumerator_1_t1689_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t1689_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t1689_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t1689_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t1689_0_0_0/* byval_arg */
	, &IEnumerator_1_t1689_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t1689_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Char>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_19.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2735_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Char>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_19MethodDeclarations.h"

extern TypeInfo Char_t109_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14131_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisChar_t109_m31661_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Char>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Char>(System.Int32)
 uint16_t Array_InternalArray__get_Item_TisChar_t109_m31661 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m14127_MethodInfo;
 void InternalEnumerator_1__ctor_m14127 (InternalEnumerator_1_t2735 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14128_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14128 (InternalEnumerator_1_t2735 * __this, MethodInfo* method){
	{
		uint16_t L_0 = InternalEnumerator_1_get_Current_m14131(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m14131_MethodInfo);
		uint16_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Char_t109_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m14129_MethodInfo;
 void InternalEnumerator_1_Dispose_m14129 (InternalEnumerator_1_t2735 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m14130_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m14130 (InternalEnumerator_1_t2735 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
 uint16_t InternalEnumerator_1_get_Current_m14131 (InternalEnumerator_1_t2735 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint16_t L_8 = Array_InternalArray__get_Item_TisChar_t109_m31661(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisChar_t109_m31661_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Char>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2735____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2735_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2735, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2735____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2735_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2735, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2735_FieldInfos[] =
{
	&InternalEnumerator_1_t2735____array_0_FieldInfo,
	&InternalEnumerator_1_t2735____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t2735____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2735_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14128_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2735____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2735_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14131_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2735_PropertyInfos[] =
{
	&InternalEnumerator_1_t2735____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2735____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2735_InternalEnumerator_1__ctor_m14127_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14127_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14127_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14127/* method */
	, &InternalEnumerator_1_t2735_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2735_InternalEnumerator_1__ctor_m14127_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14127_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14128_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14128_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14128/* method */
	, &InternalEnumerator_1_t2735_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14128_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14129_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14129_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14129/* method */
	, &InternalEnumerator_1_t2735_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14129_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14130_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14130_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14130/* method */
	, &InternalEnumerator_1_t2735_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14130_GenericMethod/* genericMethod */

};
extern Il2CppType Char_t109_0_0_0;
extern void* RuntimeInvoker_Char_t109 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14131_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14131_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14131/* method */
	, &InternalEnumerator_1_t2735_il2cpp_TypeInfo/* declaring_type */
	, &Char_t109_0_0_0/* return_type */
	, RuntimeInvoker_Char_t109/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14131_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2735_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14127_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14128_MethodInfo,
	&InternalEnumerator_1_Dispose_m14129_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14130_MethodInfo,
	&InternalEnumerator_1_get_Current_m14131_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t2735_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14128_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14130_MethodInfo,
	&InternalEnumerator_1_Dispose_m14129_MethodInfo,
	&InternalEnumerator_1_get_Current_m14131_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2735_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t1689_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2735_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t1689_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2735_0_0_0;
extern Il2CppType InternalEnumerator_1_t2735_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2735_GenericClass;
TypeInfo InternalEnumerator_1_t2735_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2735_MethodInfos/* methods */
	, InternalEnumerator_1_t2735_PropertyInfos/* properties */
	, InternalEnumerator_1_t2735_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2735_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2735_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2735_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2735_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2735_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2735_1_0_0/* this_arg */
	, InternalEnumerator_1_t2735_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2735_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2735)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7267_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Char>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Char>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Char>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Char>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Char>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Char>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Char>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Char>
extern MethodInfo ICollection_1_get_Count_m41358_MethodInfo;
static PropertyInfo ICollection_1_t7267____Count_PropertyInfo = 
{
	&ICollection_1_t7267_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41358_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41359_MethodInfo;
static PropertyInfo ICollection_1_t7267____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7267_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41359_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7267_PropertyInfos[] =
{
	&ICollection_1_t7267____Count_PropertyInfo,
	&ICollection_1_t7267____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41358_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Char>::get_Count()
MethodInfo ICollection_1_get_Count_m41358_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7267_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41358_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41359_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Char>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41359_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7267_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41359_GenericMethod/* genericMethod */

};
extern Il2CppType Char_t109_0_0_0;
extern Il2CppType Char_t109_0_0_0;
static ParameterInfo ICollection_1_t7267_ICollection_1_Add_m41360_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Char_t109_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int16_t480 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41360_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Char>::Add(T)
MethodInfo ICollection_1_Add_m41360_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7267_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int16_t480/* invoker_method */
	, ICollection_1_t7267_ICollection_1_Add_m41360_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41360_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41361_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Char>::Clear()
MethodInfo ICollection_1_Clear_m41361_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7267_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41361_GenericMethod/* genericMethod */

};
extern Il2CppType Char_t109_0_0_0;
static ParameterInfo ICollection_1_t7267_ICollection_1_Contains_m41362_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Char_t109_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int16_t480 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41362_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Char>::Contains(T)
MethodInfo ICollection_1_Contains_m41362_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7267_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int16_t480/* invoker_method */
	, ICollection_1_t7267_ICollection_1_Contains_m41362_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41362_GenericMethod/* genericMethod */

};
extern Il2CppType CharU5BU5D_t108_0_0_0;
extern Il2CppType CharU5BU5D_t108_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7267_ICollection_1_CopyTo_m41363_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CharU5BU5D_t108_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41363_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Char>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41363_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7267_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7267_ICollection_1_CopyTo_m41363_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41363_GenericMethod/* genericMethod */

};
extern Il2CppType Char_t109_0_0_0;
static ParameterInfo ICollection_1_t7267_ICollection_1_Remove_m41364_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Char_t109_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int16_t480 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41364_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Char>::Remove(T)
MethodInfo ICollection_1_Remove_m41364_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7267_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int16_t480/* invoker_method */
	, ICollection_1_t7267_ICollection_1_Remove_m41364_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41364_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7267_MethodInfos[] =
{
	&ICollection_1_get_Count_m41358_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41359_MethodInfo,
	&ICollection_1_Add_m41360_MethodInfo,
	&ICollection_1_Clear_m41361_MethodInfo,
	&ICollection_1_Contains_m41362_MethodInfo,
	&ICollection_1_CopyTo_m41363_MethodInfo,
	&ICollection_1_Remove_m41364_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t2324_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7267_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t2324_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7267_0_0_0;
extern Il2CppType ICollection_1_t7267_1_0_0;
struct ICollection_1_t7267;
extern Il2CppGenericClass ICollection_1_t7267_GenericClass;
TypeInfo ICollection_1_t7267_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7267_MethodInfos/* methods */
	, ICollection_1_t7267_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7267_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7267_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7267_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7267_0_0_0/* byval_arg */
	, &ICollection_1_t7267_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7267_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Char>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Char>
extern Il2CppType IEnumerator_1_t1689_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41365_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Char>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41365_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t2324_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t1689_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41365_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t2324_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41365_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t2324_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t2324_0_0_0;
extern Il2CppType IEnumerable_1_t2324_1_0_0;
struct IEnumerable_1_t2324;
extern Il2CppGenericClass IEnumerable_1_t2324_GenericClass;
TypeInfo IEnumerable_1_t2324_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t2324_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t2324_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t2324_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t2324_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t2324_0_0_0/* byval_arg */
	, &IEnumerable_1_t2324_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t2324_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7268_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Char>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Char>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Char>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Char>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Char>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Char>
extern MethodInfo IList_1_get_Item_m41366_MethodInfo;
extern MethodInfo IList_1_set_Item_m41367_MethodInfo;
static PropertyInfo IList_1_t7268____Item_PropertyInfo = 
{
	&IList_1_t7268_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41366_MethodInfo/* get */
	, &IList_1_set_Item_m41367_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7268_PropertyInfos[] =
{
	&IList_1_t7268____Item_PropertyInfo,
	NULL
};
extern Il2CppType Char_t109_0_0_0;
static ParameterInfo IList_1_t7268_IList_1_IndexOf_m41368_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Char_t109_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int16_t480 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41368_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Char>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41368_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7268_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int16_t480/* invoker_method */
	, IList_1_t7268_IList_1_IndexOf_m41368_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41368_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Char_t109_0_0_0;
static ParameterInfo IList_1_t7268_IList_1_Insert_m41369_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Char_t109_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int16_t480 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41369_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Char>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41369_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7268_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int16_t480/* invoker_method */
	, IList_1_t7268_IList_1_Insert_m41369_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41369_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7268_IList_1_RemoveAt_m41370_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41370_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Char>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41370_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7268_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7268_IList_1_RemoveAt_m41370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41370_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7268_IList_1_get_Item_m41366_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Char_t109_0_0_0;
extern void* RuntimeInvoker_Char_t109_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41366_GenericMethod;
// T System.Collections.Generic.IList`1<System.Char>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41366_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7268_il2cpp_TypeInfo/* declaring_type */
	, &Char_t109_0_0_0/* return_type */
	, RuntimeInvoker_Char_t109_Int32_t93/* invoker_method */
	, IList_1_t7268_IList_1_get_Item_m41366_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41366_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Char_t109_0_0_0;
static ParameterInfo IList_1_t7268_IList_1_set_Item_m41367_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Char_t109_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int16_t480 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41367_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Char>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41367_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7268_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int16_t480/* invoker_method */
	, IList_1_t7268_IList_1_set_Item_m41367_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41367_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7268_MethodInfos[] =
{
	&IList_1_IndexOf_m41368_MethodInfo,
	&IList_1_Insert_m41369_MethodInfo,
	&IList_1_RemoveAt_m41370_MethodInfo,
	&IList_1_get_Item_m41366_MethodInfo,
	&IList_1_set_Item_m41367_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7268_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7267_il2cpp_TypeInfo,
	&IEnumerable_1_t2324_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7268_0_0_0;
extern Il2CppType IList_1_t7268_1_0_0;
struct IList_1_t7268;
extern Il2CppGenericClass IList_1_t7268_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7268_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7268_MethodInfos/* methods */
	, IList_1_t7268_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7268_il2cpp_TypeInfo/* element_class */
	, IList_1_t7268_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7268_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7268_0_0_0/* byval_arg */
	, &IList_1_t7268_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7268_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7269_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>
extern MethodInfo ICollection_1_get_Count_m41371_MethodInfo;
static PropertyInfo ICollection_1_t7269____Count_PropertyInfo = 
{
	&ICollection_1_t7269_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41371_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41372_MethodInfo;
static PropertyInfo ICollection_1_t7269____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7269_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41372_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7269_PropertyInfos[] =
{
	&ICollection_1_t7269____Count_PropertyInfo,
	&ICollection_1_t7269____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41371_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::get_Count()
MethodInfo ICollection_1_get_Count_m41371_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7269_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41371_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41372_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41372_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7269_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41372_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2317_0_0_0;
extern Il2CppType IComparable_1_t2317_0_0_0;
static ParameterInfo ICollection_1_t7269_ICollection_1_Add_m41373_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2317_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41373_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Add(T)
MethodInfo ICollection_1_Add_m41373_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7269_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7269_ICollection_1_Add_m41373_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41373_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41374_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Clear()
MethodInfo ICollection_1_Clear_m41374_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7269_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41374_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2317_0_0_0;
static ParameterInfo ICollection_1_t7269_ICollection_1_Contains_m41375_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2317_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41375_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Contains(T)
MethodInfo ICollection_1_Contains_m41375_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7269_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7269_ICollection_1_Contains_m41375_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41375_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1U5BU5D_t5210_0_0_0;
extern Il2CppType IComparable_1U5BU5D_t5210_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7269_ICollection_1_CopyTo_m41376_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1U5BU5D_t5210_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41376_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41376_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7269_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7269_ICollection_1_CopyTo_m41376_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41376_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2317_0_0_0;
static ParameterInfo ICollection_1_t7269_ICollection_1_Remove_m41377_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2317_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41377_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Char>>::Remove(T)
MethodInfo ICollection_1_Remove_m41377_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7269_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7269_ICollection_1_Remove_m41377_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41377_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7269_MethodInfos[] =
{
	&ICollection_1_get_Count_m41371_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41372_MethodInfo,
	&ICollection_1_Add_m41373_MethodInfo,
	&ICollection_1_Clear_m41374_MethodInfo,
	&ICollection_1_Contains_m41375_MethodInfo,
	&ICollection_1_CopyTo_m41376_MethodInfo,
	&ICollection_1_Remove_m41377_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7271_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7269_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7271_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7269_0_0_0;
extern Il2CppType ICollection_1_t7269_1_0_0;
struct ICollection_1_t7269;
extern Il2CppGenericClass ICollection_1_t7269_GenericClass;
TypeInfo ICollection_1_t7269_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7269_MethodInfos/* methods */
	, ICollection_1_t7269_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7269_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7269_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7269_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7269_0_0_0/* byval_arg */
	, &ICollection_1_t7269_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7269_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Char>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Char>>
extern Il2CppType IEnumerator_1_t5730_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41378_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Char>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41378_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7271_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5730_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41378_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7271_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41378_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7271_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7271_0_0_0;
extern Il2CppType IEnumerable_1_t7271_1_0_0;
struct IEnumerable_1_t7271;
extern Il2CppGenericClass IEnumerable_1_t7271_GenericClass;
TypeInfo IEnumerable_1_t7271_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7271_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7271_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7271_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7271_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7271_0_0_0/* byval_arg */
	, &IEnumerable_1_t7271_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7271_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5730_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Char>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Char>>
extern MethodInfo IEnumerator_1_get_Current_m41379_MethodInfo;
static PropertyInfo IEnumerator_1_t5730____Current_PropertyInfo = 
{
	&IEnumerator_1_t5730_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41379_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5730_PropertyInfos[] =
{
	&IEnumerator_1_t5730____Current_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2317_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41379_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Char>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41379_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5730_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2317_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41379_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5730_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41379_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5730_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5730_0_0_0;
extern Il2CppType IEnumerator_1_t5730_1_0_0;
struct IEnumerator_1_t5730;
extern Il2CppGenericClass IEnumerator_1_t5730_GenericClass;
TypeInfo IEnumerator_1_t5730_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5730_MethodInfos/* methods */
	, IEnumerator_1_t5730_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5730_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5730_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5730_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5730_0_0_0/* byval_arg */
	, &IEnumerator_1_t5730_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5730_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparable_1_t2317_il2cpp_TypeInfo;



// System.Int32 System.IComparable`1<System.Char>::CompareTo(T)
// Metadata Definition System.IComparable`1<System.Char>
extern Il2CppType Char_t109_0_0_0;
static ParameterInfo IComparable_1_t2317_IComparable_1_CompareTo_m41380_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Char_t109_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int16_t480 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m41380_GenericMethod;
// System.Int32 System.IComparable`1<System.Char>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m41380_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t2317_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int16_t480/* invoker_method */
	, IComparable_1_t2317_IComparable_1_CompareTo_m41380_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m41380_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t2317_MethodInfos[] =
{
	&IComparable_1_CompareTo_m41380_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t2317_1_0_0;
struct IComparable_1_t2317;
extern Il2CppGenericClass IComparable_1_t2317_GenericClass;
TypeInfo IComparable_1_t2317_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t2317_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t2317_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t2317_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t2317_0_0_0/* byval_arg */
	, &IComparable_1_t2317_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t2317_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_20.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2736_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_20MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m14136_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIComparable_1_t2317_m31672_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.Char>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.Char>>(System.Int32)
#define Array_InternalArray__get_Item_TisIComparable_1_t2317_m31672(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2736____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2736_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2736, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2736____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2736_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2736, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2736_FieldInfos[] =
{
	&InternalEnumerator_1_t2736____array_0_FieldInfo,
	&InternalEnumerator_1_t2736____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14133_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2736____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2736_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14133_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2736____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2736_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14136_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2736_PropertyInfos[] =
{
	&InternalEnumerator_1_t2736____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2736____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2736_InternalEnumerator_1__ctor_m14132_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14132_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14132_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2736_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2736_InternalEnumerator_1__ctor_m14132_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14132_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14133_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14133_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2736_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14133_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14134_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14134_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2736_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14134_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14135_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14135_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2736_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14135_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2317_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14136_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.Char>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14136_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2736_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2317_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14136_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2736_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14132_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14133_MethodInfo,
	&InternalEnumerator_1_Dispose_m14134_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14135_MethodInfo,
	&InternalEnumerator_1_get_Current_m14136_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14135_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14134_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2736_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14133_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14135_MethodInfo,
	&InternalEnumerator_1_Dispose_m14134_MethodInfo,
	&InternalEnumerator_1_get_Current_m14136_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2736_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5730_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2736_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5730_il2cpp_TypeInfo, 7},
};
extern TypeInfo IComparable_1_t2317_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2736_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14136_MethodInfo/* Method Usage */,
	&IComparable_1_t2317_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIComparable_1_t2317_m31672_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2736_0_0_0;
extern Il2CppType InternalEnumerator_1_t2736_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2736_GenericClass;
TypeInfo InternalEnumerator_1_t2736_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2736_MethodInfos/* methods */
	, InternalEnumerator_1_t2736_PropertyInfos/* properties */
	, InternalEnumerator_1_t2736_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2736_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2736_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2736_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2736_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2736_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2736_1_0_0/* this_arg */
	, InternalEnumerator_1_t2736_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2736_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2736_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2736)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7270_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>
extern MethodInfo IList_1_get_Item_m41381_MethodInfo;
extern MethodInfo IList_1_set_Item_m41382_MethodInfo;
static PropertyInfo IList_1_t7270____Item_PropertyInfo = 
{
	&IList_1_t7270_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41381_MethodInfo/* get */
	, &IList_1_set_Item_m41382_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7270_PropertyInfos[] =
{
	&IList_1_t7270____Item_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2317_0_0_0;
static ParameterInfo IList_1_t7270_IList_1_IndexOf_m41383_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2317_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41383_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41383_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7270_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7270_IList_1_IndexOf_m41383_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41383_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IComparable_1_t2317_0_0_0;
static ParameterInfo IList_1_t7270_IList_1_Insert_m41384_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2317_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41384_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41384_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7270_IList_1_Insert_m41384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41384_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7270_IList_1_RemoveAt_m41385_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41385_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41385_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7270_IList_1_RemoveAt_m41385_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41385_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7270_IList_1_get_Item_m41381_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IComparable_1_t2317_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41381_GenericMethod;
// T System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41381_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7270_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2317_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7270_IList_1_get_Item_m41381_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41381_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IComparable_1_t2317_0_0_0;
static ParameterInfo IList_1_t7270_IList_1_set_Item_m41382_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2317_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41382_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Char>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41382_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7270_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7270_IList_1_set_Item_m41382_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41382_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7270_MethodInfos[] =
{
	&IList_1_IndexOf_m41383_MethodInfo,
	&IList_1_Insert_m41384_MethodInfo,
	&IList_1_RemoveAt_m41385_MethodInfo,
	&IList_1_get_Item_m41381_MethodInfo,
	&IList_1_set_Item_m41382_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7270_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7269_il2cpp_TypeInfo,
	&IEnumerable_1_t7271_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7270_0_0_0;
extern Il2CppType IList_1_t7270_1_0_0;
struct IList_1_t7270;
extern Il2CppGenericClass IList_1_t7270_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7270_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7270_MethodInfos/* methods */
	, IList_1_t7270_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7270_il2cpp_TypeInfo/* element_class */
	, IList_1_t7270_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7270_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7270_0_0_0/* byval_arg */
	, &IList_1_t7270_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7270_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7272_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>
extern MethodInfo ICollection_1_get_Count_m41386_MethodInfo;
static PropertyInfo ICollection_1_t7272____Count_PropertyInfo = 
{
	&ICollection_1_t7272_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41386_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41387_MethodInfo;
static PropertyInfo ICollection_1_t7272____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7272_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41387_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7272_PropertyInfos[] =
{
	&ICollection_1_t7272____Count_PropertyInfo,
	&ICollection_1_t7272____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41386_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::get_Count()
MethodInfo ICollection_1_get_Count_m41386_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7272_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41386_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41387_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41387_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7272_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41387_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2318_0_0_0;
extern Il2CppType IEquatable_1_t2318_0_0_0;
static ParameterInfo ICollection_1_t7272_ICollection_1_Add_m41388_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2318_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41388_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Add(T)
MethodInfo ICollection_1_Add_m41388_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7272_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7272_ICollection_1_Add_m41388_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41388_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41389_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Clear()
MethodInfo ICollection_1_Clear_m41389_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7272_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41389_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2318_0_0_0;
static ParameterInfo ICollection_1_t7272_ICollection_1_Contains_m41390_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2318_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41390_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Contains(T)
MethodInfo ICollection_1_Contains_m41390_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7272_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7272_ICollection_1_Contains_m41390_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41390_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1U5BU5D_t5211_0_0_0;
extern Il2CppType IEquatable_1U5BU5D_t5211_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7272_ICollection_1_CopyTo_m41391_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1U5BU5D_t5211_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41391_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41391_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7272_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7272_ICollection_1_CopyTo_m41391_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41391_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2318_0_0_0;
static ParameterInfo ICollection_1_t7272_ICollection_1_Remove_m41392_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2318_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41392_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Char>>::Remove(T)
MethodInfo ICollection_1_Remove_m41392_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7272_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7272_ICollection_1_Remove_m41392_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41392_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7272_MethodInfos[] =
{
	&ICollection_1_get_Count_m41386_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41387_MethodInfo,
	&ICollection_1_Add_m41388_MethodInfo,
	&ICollection_1_Clear_m41389_MethodInfo,
	&ICollection_1_Contains_m41390_MethodInfo,
	&ICollection_1_CopyTo_m41391_MethodInfo,
	&ICollection_1_Remove_m41392_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7274_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7272_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7274_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7272_0_0_0;
extern Il2CppType ICollection_1_t7272_1_0_0;
struct ICollection_1_t7272;
extern Il2CppGenericClass ICollection_1_t7272_GenericClass;
TypeInfo ICollection_1_t7272_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7272_MethodInfos/* methods */
	, ICollection_1_t7272_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7272_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7272_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7272_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7272_0_0_0/* byval_arg */
	, &ICollection_1_t7272_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7272_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Char>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Char>>
extern Il2CppType IEnumerator_1_t5732_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41393_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Char>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41393_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7274_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5732_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41393_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7274_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41393_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7274_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7274_0_0_0;
extern Il2CppType IEnumerable_1_t7274_1_0_0;
struct IEnumerable_1_t7274;
extern Il2CppGenericClass IEnumerable_1_t7274_GenericClass;
TypeInfo IEnumerable_1_t7274_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7274_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7274_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7274_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7274_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7274_0_0_0/* byval_arg */
	, &IEnumerable_1_t7274_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7274_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5732_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Char>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Char>>
extern MethodInfo IEnumerator_1_get_Current_m41394_MethodInfo;
static PropertyInfo IEnumerator_1_t5732____Current_PropertyInfo = 
{
	&IEnumerator_1_t5732_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41394_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5732_PropertyInfos[] =
{
	&IEnumerator_1_t5732____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2318_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41394_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Char>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41394_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5732_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2318_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41394_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5732_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41394_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5732_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5732_0_0_0;
extern Il2CppType IEnumerator_1_t5732_1_0_0;
struct IEnumerator_1_t5732;
extern Il2CppGenericClass IEnumerator_1_t5732_GenericClass;
TypeInfo IEnumerator_1_t5732_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5732_MethodInfos/* methods */
	, IEnumerator_1_t5732_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5732_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5732_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5732_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5732_0_0_0/* byval_arg */
	, &IEnumerator_1_t5732_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5732_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEquatable_1_t2318_il2cpp_TypeInfo;



// System.Boolean System.IEquatable`1<System.Char>::Equals(T)
// Metadata Definition System.IEquatable`1<System.Char>
extern Il2CppType Char_t109_0_0_0;
static ParameterInfo IEquatable_1_t2318_IEquatable_1_Equals_m41395_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Char_t109_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int16_t480 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEquatable_1_Equals_m41395_GenericMethod;
// System.Boolean System.IEquatable`1<System.Char>::Equals(T)
MethodInfo IEquatable_1_Equals_m41395_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEquatable_1_t2318_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int16_t480/* invoker_method */
	, IEquatable_1_t2318_IEquatable_1_Equals_m41395_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEquatable_1_Equals_m41395_GenericMethod/* genericMethod */

};
static MethodInfo* IEquatable_1_t2318_MethodInfos[] =
{
	&IEquatable_1_Equals_m41395_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1_t2318_1_0_0;
struct IEquatable_1_t2318;
extern Il2CppGenericClass IEquatable_1_t2318_GenericClass;
TypeInfo IEquatable_1_t2318_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, IEquatable_1_t2318_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t2318_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1_t2318_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1_t2318_0_0_0/* byval_arg */
	, &IEquatable_1_t2318_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEquatable_1_t2318_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_21.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2737_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_21MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m14141_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEquatable_1_t2318_m31683_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.Char>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.Char>>(System.Int32)
#define Array_InternalArray__get_Item_TisIEquatable_1_t2318_m31683(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2737____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2737_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2737, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2737____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2737_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2737, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2737_FieldInfos[] =
{
	&InternalEnumerator_1_t2737____array_0_FieldInfo,
	&InternalEnumerator_1_t2737____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14138_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2737____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2737_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14138_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2737____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2737_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14141_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2737_PropertyInfos[] =
{
	&InternalEnumerator_1_t2737____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2737____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2737_InternalEnumerator_1__ctor_m14137_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14137_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14137_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2737_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2737_InternalEnumerator_1__ctor_m14137_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14137_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14138_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14138_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2737_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14138_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14139_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14139_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2737_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14139_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14140_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14140_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2737_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14140_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2318_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14141_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.Char>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14141_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2737_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2318_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14141_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2737_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14137_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14138_MethodInfo,
	&InternalEnumerator_1_Dispose_m14139_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14140_MethodInfo,
	&InternalEnumerator_1_get_Current_m14141_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14140_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14139_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2737_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14138_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14140_MethodInfo,
	&InternalEnumerator_1_Dispose_m14139_MethodInfo,
	&InternalEnumerator_1_get_Current_m14141_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2737_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5732_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2737_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5732_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEquatable_1_t2318_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2737_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14141_MethodInfo/* Method Usage */,
	&IEquatable_1_t2318_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEquatable_1_t2318_m31683_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2737_0_0_0;
extern Il2CppType InternalEnumerator_1_t2737_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2737_GenericClass;
TypeInfo InternalEnumerator_1_t2737_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2737_MethodInfos/* methods */
	, InternalEnumerator_1_t2737_PropertyInfos/* properties */
	, InternalEnumerator_1_t2737_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2737_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2737_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2737_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2737_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2737_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2737_1_0_0/* this_arg */
	, InternalEnumerator_1_t2737_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2737_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2737_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2737)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7273_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>
extern MethodInfo IList_1_get_Item_m41396_MethodInfo;
extern MethodInfo IList_1_set_Item_m41397_MethodInfo;
static PropertyInfo IList_1_t7273____Item_PropertyInfo = 
{
	&IList_1_t7273_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41396_MethodInfo/* get */
	, &IList_1_set_Item_m41397_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7273_PropertyInfos[] =
{
	&IList_1_t7273____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2318_0_0_0;
static ParameterInfo IList_1_t7273_IList_1_IndexOf_m41398_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2318_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41398_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41398_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7273_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7273_IList_1_IndexOf_m41398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41398_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEquatable_1_t2318_0_0_0;
static ParameterInfo IList_1_t7273_IList_1_Insert_m41399_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2318_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41399_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41399_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7273_IList_1_Insert_m41399_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41399_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7273_IList_1_RemoveAt_m41400_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41400_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41400_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7273_IList_1_RemoveAt_m41400_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41400_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7273_IList_1_get_Item_m41396_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEquatable_1_t2318_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41396_GenericMethod;
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41396_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7273_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2318_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7273_IList_1_get_Item_m41396_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41396_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEquatable_1_t2318_0_0_0;
static ParameterInfo IList_1_t7273_IList_1_set_Item_m41397_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2318_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41397_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Char>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41397_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7273_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7273_IList_1_set_Item_m41397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41397_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7273_MethodInfos[] =
{
	&IList_1_IndexOf_m41398_MethodInfo,
	&IList_1_Insert_m41399_MethodInfo,
	&IList_1_RemoveAt_m41400_MethodInfo,
	&IList_1_get_Item_m41396_MethodInfo,
	&IList_1_set_Item_m41397_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7273_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7272_il2cpp_TypeInfo,
	&IEnumerable_1_t7274_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7273_0_0_0;
extern Il2CppType IList_1_t7273_1_0_0;
struct IList_1_t7273;
extern Il2CppGenericClass IList_1_t7273_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7273_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7273_MethodInfos/* methods */
	, IList_1_t7273_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7273_il2cpp_TypeInfo/* element_class */
	, IList_1_t7273_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7273_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7273_0_0_0/* byval_arg */
	, &IList_1_t7273_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7273_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5734_il2cpp_TypeInfo;

// Raycaster
#include "AssemblyU2DCSharp_Raycaster.h"


// T System.Collections.Generic.IEnumerator`1<Raycaster>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Raycaster>
extern MethodInfo IEnumerator_1_get_Current_m41401_MethodInfo;
static PropertyInfo IEnumerator_1_t5734____Current_PropertyInfo = 
{
	&IEnumerator_1_t5734_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41401_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5734_PropertyInfos[] =
{
	&IEnumerator_1_t5734____Current_PropertyInfo,
	NULL
};
extern Il2CppType Raycaster_t20_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41401_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Raycaster>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41401_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5734_il2cpp_TypeInfo/* declaring_type */
	, &Raycaster_t20_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41401_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5734_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41401_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5734_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5734_0_0_0;
extern Il2CppType IEnumerator_1_t5734_1_0_0;
struct IEnumerator_1_t5734;
extern Il2CppGenericClass IEnumerator_1_t5734_GenericClass;
TypeInfo IEnumerator_1_t5734_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5734_MethodInfos/* methods */
	, IEnumerator_1_t5734_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5734_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5734_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5734_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5734_0_0_0/* byval_arg */
	, &IEnumerator_1_t5734_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5734_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Raycaster>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_22.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2738_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Raycaster>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_22MethodDeclarations.h"

extern TypeInfo Raycaster_t20_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14146_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRaycaster_t20_m31694_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Raycaster>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Raycaster>(System.Int32)
#define Array_InternalArray__get_Item_TisRaycaster_t20_m31694(__this, p0, method) (Raycaster_t20 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Raycaster>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Raycaster>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Raycaster>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Raycaster>::MoveNext()
// T System.Array/InternalEnumerator`1<Raycaster>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Raycaster>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2738____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2738_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2738, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2738____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2738_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2738, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2738_FieldInfos[] =
{
	&InternalEnumerator_1_t2738____array_0_FieldInfo,
	&InternalEnumerator_1_t2738____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14143_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2738____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2738_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14143_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2738____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2738_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14146_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2738_PropertyInfos[] =
{
	&InternalEnumerator_1_t2738____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2738____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2738_InternalEnumerator_1__ctor_m14142_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14142_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Raycaster>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14142_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2738_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2738_InternalEnumerator_1__ctor_m14142_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14142_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14143_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Raycaster>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14143_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2738_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14143_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14144_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Raycaster>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14144_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2738_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14144_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14145_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Raycaster>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14145_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2738_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14145_GenericMethod/* genericMethod */

};
extern Il2CppType Raycaster_t20_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14146_GenericMethod;
// T System.Array/InternalEnumerator`1<Raycaster>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14146_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2738_il2cpp_TypeInfo/* declaring_type */
	, &Raycaster_t20_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14146_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2738_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14142_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14143_MethodInfo,
	&InternalEnumerator_1_Dispose_m14144_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14145_MethodInfo,
	&InternalEnumerator_1_get_Current_m14146_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14145_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14144_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2738_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14143_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14145_MethodInfo,
	&InternalEnumerator_1_Dispose_m14144_MethodInfo,
	&InternalEnumerator_1_get_Current_m14146_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2738_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5734_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2738_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5734_il2cpp_TypeInfo, 7},
};
extern TypeInfo Raycaster_t20_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2738_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14146_MethodInfo/* Method Usage */,
	&Raycaster_t20_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisRaycaster_t20_m31694_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2738_0_0_0;
extern Il2CppType InternalEnumerator_1_t2738_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2738_GenericClass;
TypeInfo InternalEnumerator_1_t2738_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2738_MethodInfos/* methods */
	, InternalEnumerator_1_t2738_PropertyInfos/* properties */
	, InternalEnumerator_1_t2738_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2738_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2738_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2738_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2738_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2738_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2738_1_0_0/* this_arg */
	, InternalEnumerator_1_t2738_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2738_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2738_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2738)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7275_il2cpp_TypeInfo;

#include "Assembly-CSharp_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Raycaster>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Raycaster>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Raycaster>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Raycaster>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Raycaster>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Raycaster>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Raycaster>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Raycaster>
extern MethodInfo ICollection_1_get_Count_m41402_MethodInfo;
static PropertyInfo ICollection_1_t7275____Count_PropertyInfo = 
{
	&ICollection_1_t7275_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41402_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41403_MethodInfo;
static PropertyInfo ICollection_1_t7275____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7275_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41403_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7275_PropertyInfos[] =
{
	&ICollection_1_t7275____Count_PropertyInfo,
	&ICollection_1_t7275____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41402_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Raycaster>::get_Count()
MethodInfo ICollection_1_get_Count_m41402_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7275_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41402_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41403_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Raycaster>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41403_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7275_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41403_GenericMethod/* genericMethod */

};
extern Il2CppType Raycaster_t20_0_0_0;
extern Il2CppType Raycaster_t20_0_0_0;
static ParameterInfo ICollection_1_t7275_ICollection_1_Add_m41404_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Raycaster_t20_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41404_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Raycaster>::Add(T)
MethodInfo ICollection_1_Add_m41404_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7275_ICollection_1_Add_m41404_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41404_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41405_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Raycaster>::Clear()
MethodInfo ICollection_1_Clear_m41405_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41405_GenericMethod/* genericMethod */

};
extern Il2CppType Raycaster_t20_0_0_0;
static ParameterInfo ICollection_1_t7275_ICollection_1_Contains_m41406_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Raycaster_t20_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41406_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Raycaster>::Contains(T)
MethodInfo ICollection_1_Contains_m41406_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7275_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7275_ICollection_1_Contains_m41406_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41406_GenericMethod/* genericMethod */

};
extern Il2CppType RaycasterU5BU5D_t5162_0_0_0;
extern Il2CppType RaycasterU5BU5D_t5162_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7275_ICollection_1_CopyTo_m41407_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &RaycasterU5BU5D_t5162_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41407_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Raycaster>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41407_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7275_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7275_ICollection_1_CopyTo_m41407_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41407_GenericMethod/* genericMethod */

};
extern Il2CppType Raycaster_t20_0_0_0;
static ParameterInfo ICollection_1_t7275_ICollection_1_Remove_m41408_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Raycaster_t20_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41408_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Raycaster>::Remove(T)
MethodInfo ICollection_1_Remove_m41408_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7275_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7275_ICollection_1_Remove_m41408_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41408_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7275_MethodInfos[] =
{
	&ICollection_1_get_Count_m41402_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41403_MethodInfo,
	&ICollection_1_Add_m41404_MethodInfo,
	&ICollection_1_Clear_m41405_MethodInfo,
	&ICollection_1_Contains_m41406_MethodInfo,
	&ICollection_1_CopyTo_m41407_MethodInfo,
	&ICollection_1_Remove_m41408_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7277_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7275_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7277_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7275_0_0_0;
extern Il2CppType ICollection_1_t7275_1_0_0;
struct ICollection_1_t7275;
extern Il2CppGenericClass ICollection_1_t7275_GenericClass;
TypeInfo ICollection_1_t7275_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7275_MethodInfos/* methods */
	, ICollection_1_t7275_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7275_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7275_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7275_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7275_0_0_0/* byval_arg */
	, &ICollection_1_t7275_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7275_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Raycaster>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Raycaster>
extern Il2CppType IEnumerator_1_t5734_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41409_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Raycaster>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41409_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7277_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5734_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41409_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7277_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41409_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7277_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7277_0_0_0;
extern Il2CppType IEnumerable_1_t7277_1_0_0;
struct IEnumerable_1_t7277;
extern Il2CppGenericClass IEnumerable_1_t7277_GenericClass;
TypeInfo IEnumerable_1_t7277_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7277_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7277_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7277_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7277_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7277_0_0_0/* byval_arg */
	, &IEnumerable_1_t7277_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7277_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7276_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Raycaster>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Raycaster>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Raycaster>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Raycaster>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Raycaster>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Raycaster>
extern MethodInfo IList_1_get_Item_m41410_MethodInfo;
extern MethodInfo IList_1_set_Item_m41411_MethodInfo;
static PropertyInfo IList_1_t7276____Item_PropertyInfo = 
{
	&IList_1_t7276_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41410_MethodInfo/* get */
	, &IList_1_set_Item_m41411_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7276_PropertyInfos[] =
{
	&IList_1_t7276____Item_PropertyInfo,
	NULL
};
extern Il2CppType Raycaster_t20_0_0_0;
static ParameterInfo IList_1_t7276_IList_1_IndexOf_m41412_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Raycaster_t20_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41412_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Raycaster>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41412_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7276_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7276_IList_1_IndexOf_m41412_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41412_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Raycaster_t20_0_0_0;
static ParameterInfo IList_1_t7276_IList_1_Insert_m41413_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Raycaster_t20_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41413_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Raycaster>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41413_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7276_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7276_IList_1_Insert_m41413_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41413_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7276_IList_1_RemoveAt_m41414_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41414_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Raycaster>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41414_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7276_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7276_IList_1_RemoveAt_m41414_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41414_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7276_IList_1_get_Item_m41410_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Raycaster_t20_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41410_GenericMethod;
// T System.Collections.Generic.IList`1<Raycaster>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41410_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7276_il2cpp_TypeInfo/* declaring_type */
	, &Raycaster_t20_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7276_IList_1_get_Item_m41410_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41410_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Raycaster_t20_0_0_0;
static ParameterInfo IList_1_t7276_IList_1_set_Item_m41411_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Raycaster_t20_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41411_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Raycaster>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41411_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7276_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7276_IList_1_set_Item_m41411_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41411_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7276_MethodInfos[] =
{
	&IList_1_IndexOf_m41412_MethodInfo,
	&IList_1_Insert_m41413_MethodInfo,
	&IList_1_RemoveAt_m41414_MethodInfo,
	&IList_1_get_Item_m41410_MethodInfo,
	&IList_1_set_Item_m41411_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7276_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7275_il2cpp_TypeInfo,
	&IEnumerable_1_t7277_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7276_0_0_0;
extern Il2CppType IList_1_t7276_1_0_0;
struct IList_1_t7276;
extern Il2CppGenericClass IList_1_t7276_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7276_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7276_MethodInfos/* methods */
	, IList_1_t7276_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7276_il2cpp_TypeInfo/* element_class */
	, IList_1_t7276_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7276_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7276_0_0_0/* byval_arg */
	, &IList_1_t7276_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7276_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Raycaster>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_8.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2739_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Raycaster>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_8MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// UnityEngine.Events.InvokableCall`1<Raycaster>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_4.h"
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t2740_il2cpp_TypeInfo;
extern TypeInfo Void_t99_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Raycaster>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_4MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14149_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14151_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Raycaster>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Raycaster>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Raycaster>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2739____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2739_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2739, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2739_FieldInfos[] =
{
	&CachedInvokableCall_1_t2739____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Raycaster_t20_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2739_CachedInvokableCall_1__ctor_m14147_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Raycaster_t20_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14147_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Raycaster>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14147_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2739_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2739_CachedInvokableCall_1__ctor_m14147_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14147_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2739_CachedInvokableCall_1_Invoke_m14148_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14148_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Raycaster>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14148_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2739_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2739_CachedInvokableCall_1_Invoke_m14148_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14148_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2739_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14147_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14148_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
extern MethodInfo Object_ToString_m306_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m14148_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14152_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2739_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14148_MethodInfo,
	&InvokableCall_1_Find_m14152_MethodInfo,
};
extern Il2CppType UnityAction_1_t2741_0_0_0;
extern TypeInfo UnityAction_1_t2741_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisRaycaster_t20_m31704_MethodInfo;
extern TypeInfo Raycaster_t20_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14154_MethodInfo;
extern TypeInfo Raycaster_t20_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2739_RGCTXData[8] = 
{
	&UnityAction_1_t2741_0_0_0/* Type Usage */,
	&UnityAction_1_t2741_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRaycaster_t20_m31704_MethodInfo/* Method Usage */,
	&Raycaster_t20_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14154_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14149_MethodInfo/* Method Usage */,
	&Raycaster_t20_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14151_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2739_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2739_1_0_0;
struct CachedInvokableCall_1_t2739;
extern Il2CppGenericClass CachedInvokableCall_1_t2739_GenericClass;
TypeInfo CachedInvokableCall_1_t2739_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2739_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2739_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2740_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2739_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2739_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2739_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2739_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2739_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2739_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2739_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2739)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Raycaster>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_11.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t2741_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t507_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<Raycaster>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_11MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6358_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m395_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m456_MethodInfo;
extern MethodInfo Delegate_Combine_m2149_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6357_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2468_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6359_MethodInfo;
extern MethodInfo Delegate_get_Target_m6536_MethodInfo;
extern MethodInfo Delegate_get_Method_m6534_MethodInfo;
struct BaseInvokableCall_t1075;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Raycaster>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Raycaster>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisRaycaster_t20_m31704(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Raycaster>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Raycaster>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Raycaster>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Raycaster>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Raycaster>
extern Il2CppType UnityAction_1_t2741_0_0_1;
FieldInfo InvokableCall_1_t2740____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2741_0_0_1/* type */
	, &InvokableCall_1_t2740_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2740, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2740_FieldInfos[] =
{
	&InvokableCall_1_t2740____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2740_InvokableCall_1__ctor_m14149_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14149_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Raycaster>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14149_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2740_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2740_InvokableCall_1__ctor_m14149_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14149_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2741_0_0_0;
static ParameterInfo InvokableCall_1_t2740_InvokableCall_1__ctor_m14150_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2741_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14150_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Raycaster>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14150_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2740_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2740_InvokableCall_1__ctor_m14150_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14150_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2740_InvokableCall_1_Invoke_m14151_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14151_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Raycaster>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14151_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2740_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2740_InvokableCall_1_Invoke_m14151_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14151_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2740_InvokableCall_1_Find_m14152_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14152_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Raycaster>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14152_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2740_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2740_InvokableCall_1_Find_m14152_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14152_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2740_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14149_MethodInfo,
	&InvokableCall_1__ctor_m14150_MethodInfo,
	&InvokableCall_1_Invoke_m14151_MethodInfo,
	&InvokableCall_1_Find_m14152_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2740_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14151_MethodInfo,
	&InvokableCall_1_Find_m14152_MethodInfo,
};
extern TypeInfo UnityAction_1_t2741_il2cpp_TypeInfo;
extern TypeInfo Raycaster_t20_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2740_RGCTXData[5] = 
{
	&UnityAction_1_t2741_0_0_0/* Type Usage */,
	&UnityAction_1_t2741_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRaycaster_t20_m31704_MethodInfo/* Method Usage */,
	&Raycaster_t20_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14154_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2740_0_0_0;
extern Il2CppType InvokableCall_1_t2740_1_0_0;
extern TypeInfo BaseInvokableCall_t1075_il2cpp_TypeInfo;
struct InvokableCall_1_t2740;
extern Il2CppGenericClass InvokableCall_1_t2740_GenericClass;
TypeInfo InvokableCall_1_t2740_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2740_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2740_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2740_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2740_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2740_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2740_0_0_0/* byval_arg */
	, &InvokableCall_1_t2740_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2740_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2740_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2740)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<Raycaster>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Raycaster>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Raycaster>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Raycaster>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Raycaster>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2741_UnityAction_1__ctor_m14153_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14153_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Raycaster>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14153_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2741_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2741_UnityAction_1__ctor_m14153_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14153_GenericMethod/* genericMethod */

};
extern Il2CppType Raycaster_t20_0_0_0;
static ParameterInfo UnityAction_1_t2741_UnityAction_1_Invoke_m14154_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Raycaster_t20_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14154_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Raycaster>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14154_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2741_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2741_UnityAction_1_Invoke_m14154_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14154_GenericMethod/* genericMethod */

};
extern Il2CppType Raycaster_t20_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2741_UnityAction_1_BeginInvoke_m14155_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Raycaster_t20_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14155_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Raycaster>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14155_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2741_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2741_UnityAction_1_BeginInvoke_m14155_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14155_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2741_UnityAction_1_EndInvoke_m14156_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14156_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Raycaster>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14156_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2741_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2741_UnityAction_1_EndInvoke_m14156_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14156_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2741_MethodInfos[] =
{
	&UnityAction_1__ctor_m14153_MethodInfo,
	&UnityAction_1_Invoke_m14154_MethodInfo,
	&UnityAction_1_BeginInvoke_m14155_MethodInfo,
	&UnityAction_1_EndInvoke_m14156_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2241_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2242_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2243_MethodInfo;
extern MethodInfo Delegate_Clone_m2244_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2245_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2246_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2247_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m14155_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14156_MethodInfo;
static MethodInfo* UnityAction_1_t2741_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14154_MethodInfo,
	&UnityAction_1_BeginInvoke_m14155_MethodInfo,
	&UnityAction_1_EndInvoke_m14156_MethodInfo,
};
extern TypeInfo ISerializable_t482_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t2741_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2741_1_0_0;
extern TypeInfo MulticastDelegate_t325_il2cpp_TypeInfo;
struct UnityAction_1_t2741;
extern Il2CppGenericClass UnityAction_1_t2741_GenericClass;
TypeInfo UnityAction_1_t2741_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2741_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2741_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2741_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2741_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2741_0_0_0/* byval_arg */
	, &UnityAction_1_t2741_1_0_0/* this_arg */
	, UnityAction_1_t2741_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2741_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2741)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5736_il2cpp_TypeInfo;

// SetRenderQueue
#include "AssemblyU2DCSharp_SetRenderQueue.h"


// T System.Collections.Generic.IEnumerator`1<SetRenderQueue>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<SetRenderQueue>
extern MethodInfo IEnumerator_1_get_Current_m41415_MethodInfo;
static PropertyInfo IEnumerator_1_t5736____Current_PropertyInfo = 
{
	&IEnumerator_1_t5736_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41415_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5736_PropertyInfos[] =
{
	&IEnumerator_1_t5736____Current_PropertyInfo,
	NULL
};
extern Il2CppType SetRenderQueue_t22_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41415_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<SetRenderQueue>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41415_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5736_il2cpp_TypeInfo/* declaring_type */
	, &SetRenderQueue_t22_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41415_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5736_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41415_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5736_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5736_0_0_0;
extern Il2CppType IEnumerator_1_t5736_1_0_0;
struct IEnumerator_1_t5736;
extern Il2CppGenericClass IEnumerator_1_t5736_GenericClass;
TypeInfo IEnumerator_1_t5736_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5736_MethodInfos/* methods */
	, IEnumerator_1_t5736_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5736_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5736_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5736_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5736_0_0_0/* byval_arg */
	, &IEnumerator_1_t5736_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5736_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<SetRenderQueue>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_23.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2742_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<SetRenderQueue>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_23MethodDeclarations.h"

extern TypeInfo SetRenderQueue_t22_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14161_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSetRenderQueue_t22_m31706_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<SetRenderQueue>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<SetRenderQueue>(System.Int32)
#define Array_InternalArray__get_Item_TisSetRenderQueue_t22_m31706(__this, p0, method) (SetRenderQueue_t22 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<SetRenderQueue>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<SetRenderQueue>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<SetRenderQueue>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<SetRenderQueue>::MoveNext()
// T System.Array/InternalEnumerator`1<SetRenderQueue>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<SetRenderQueue>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2742____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2742_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2742, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2742____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2742_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2742, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2742_FieldInfos[] =
{
	&InternalEnumerator_1_t2742____array_0_FieldInfo,
	&InternalEnumerator_1_t2742____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2742____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2742_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2742____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2742_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14161_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2742_PropertyInfos[] =
{
	&InternalEnumerator_1_t2742____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2742____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2742_InternalEnumerator_1__ctor_m14157_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14157_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<SetRenderQueue>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14157_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2742_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2742_InternalEnumerator_1__ctor_m14157_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14157_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<SetRenderQueue>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2742_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14159_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<SetRenderQueue>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14159_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2742_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14159_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14160_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<SetRenderQueue>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14160_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2742_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14160_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueue_t22_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14161_GenericMethod;
// T System.Array/InternalEnumerator`1<SetRenderQueue>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14161_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2742_il2cpp_TypeInfo/* declaring_type */
	, &SetRenderQueue_t22_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14161_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2742_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14157_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_MethodInfo,
	&InternalEnumerator_1_Dispose_m14159_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14160_MethodInfo,
	&InternalEnumerator_1_get_Current_m14161_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14160_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14159_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2742_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14158_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14160_MethodInfo,
	&InternalEnumerator_1_Dispose_m14159_MethodInfo,
	&InternalEnumerator_1_get_Current_m14161_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2742_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5736_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2742_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5736_il2cpp_TypeInfo, 7},
};
extern TypeInfo SetRenderQueue_t22_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2742_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14161_MethodInfo/* Method Usage */,
	&SetRenderQueue_t22_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSetRenderQueue_t22_m31706_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2742_0_0_0;
extern Il2CppType InternalEnumerator_1_t2742_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2742_GenericClass;
TypeInfo InternalEnumerator_1_t2742_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2742_MethodInfos/* methods */
	, InternalEnumerator_1_t2742_PropertyInfos/* properties */
	, InternalEnumerator_1_t2742_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2742_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2742_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2742_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2742_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2742_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2742_1_0_0/* this_arg */
	, InternalEnumerator_1_t2742_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2742_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2742_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2742)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7278_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<SetRenderQueue>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<SetRenderQueue>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<SetRenderQueue>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<SetRenderQueue>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<SetRenderQueue>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<SetRenderQueue>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<SetRenderQueue>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<SetRenderQueue>
extern MethodInfo ICollection_1_get_Count_m41416_MethodInfo;
static PropertyInfo ICollection_1_t7278____Count_PropertyInfo = 
{
	&ICollection_1_t7278_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41416_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41417_MethodInfo;
static PropertyInfo ICollection_1_t7278____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7278_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41417_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7278_PropertyInfos[] =
{
	&ICollection_1_t7278____Count_PropertyInfo,
	&ICollection_1_t7278____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41416_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<SetRenderQueue>::get_Count()
MethodInfo ICollection_1_get_Count_m41416_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7278_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41416_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41417_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<SetRenderQueue>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41417_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7278_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41417_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueue_t22_0_0_0;
extern Il2CppType SetRenderQueue_t22_0_0_0;
static ParameterInfo ICollection_1_t7278_ICollection_1_Add_m41418_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueue_t22_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41418_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<SetRenderQueue>::Add(T)
MethodInfo ICollection_1_Add_m41418_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7278_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7278_ICollection_1_Add_m41418_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41418_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41419_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<SetRenderQueue>::Clear()
MethodInfo ICollection_1_Clear_m41419_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7278_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41419_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueue_t22_0_0_0;
static ParameterInfo ICollection_1_t7278_ICollection_1_Contains_m41420_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueue_t22_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41420_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<SetRenderQueue>::Contains(T)
MethodInfo ICollection_1_Contains_m41420_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7278_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7278_ICollection_1_Contains_m41420_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41420_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueueU5BU5D_t5163_0_0_0;
extern Il2CppType SetRenderQueueU5BU5D_t5163_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7278_ICollection_1_CopyTo_m41421_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueueU5BU5D_t5163_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41421_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<SetRenderQueue>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41421_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7278_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7278_ICollection_1_CopyTo_m41421_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41421_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueue_t22_0_0_0;
static ParameterInfo ICollection_1_t7278_ICollection_1_Remove_m41422_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueue_t22_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41422_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<SetRenderQueue>::Remove(T)
MethodInfo ICollection_1_Remove_m41422_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7278_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7278_ICollection_1_Remove_m41422_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41422_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7278_MethodInfos[] =
{
	&ICollection_1_get_Count_m41416_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41417_MethodInfo,
	&ICollection_1_Add_m41418_MethodInfo,
	&ICollection_1_Clear_m41419_MethodInfo,
	&ICollection_1_Contains_m41420_MethodInfo,
	&ICollection_1_CopyTo_m41421_MethodInfo,
	&ICollection_1_Remove_m41422_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7280_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7278_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7280_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7278_0_0_0;
extern Il2CppType ICollection_1_t7278_1_0_0;
struct ICollection_1_t7278;
extern Il2CppGenericClass ICollection_1_t7278_GenericClass;
TypeInfo ICollection_1_t7278_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7278_MethodInfos/* methods */
	, ICollection_1_t7278_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7278_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7278_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7278_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7278_0_0_0/* byval_arg */
	, &ICollection_1_t7278_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7278_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<SetRenderQueue>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<SetRenderQueue>
extern Il2CppType IEnumerator_1_t5736_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41423_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<SetRenderQueue>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41423_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7280_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5736_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41423_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7280_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41423_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7280_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7280_0_0_0;
extern Il2CppType IEnumerable_1_t7280_1_0_0;
struct IEnumerable_1_t7280;
extern Il2CppGenericClass IEnumerable_1_t7280_GenericClass;
TypeInfo IEnumerable_1_t7280_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7280_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7280_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7280_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7280_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7280_0_0_0/* byval_arg */
	, &IEnumerable_1_t7280_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7280_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7279_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<SetRenderQueue>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<SetRenderQueue>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<SetRenderQueue>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<SetRenderQueue>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<SetRenderQueue>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<SetRenderQueue>
extern MethodInfo IList_1_get_Item_m41424_MethodInfo;
extern MethodInfo IList_1_set_Item_m41425_MethodInfo;
static PropertyInfo IList_1_t7279____Item_PropertyInfo = 
{
	&IList_1_t7279_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41424_MethodInfo/* get */
	, &IList_1_set_Item_m41425_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7279_PropertyInfos[] =
{
	&IList_1_t7279____Item_PropertyInfo,
	NULL
};
extern Il2CppType SetRenderQueue_t22_0_0_0;
static ParameterInfo IList_1_t7279_IList_1_IndexOf_m41426_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueue_t22_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41426_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<SetRenderQueue>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41426_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7279_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7279_IList_1_IndexOf_m41426_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41426_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SetRenderQueue_t22_0_0_0;
static ParameterInfo IList_1_t7279_IList_1_Insert_m41427_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SetRenderQueue_t22_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41427_GenericMethod;
// System.Void System.Collections.Generic.IList`1<SetRenderQueue>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41427_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7279_IList_1_Insert_m41427_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41427_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7279_IList_1_RemoveAt_m41428_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41428_GenericMethod;
// System.Void System.Collections.Generic.IList`1<SetRenderQueue>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41428_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7279_IList_1_RemoveAt_m41428_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41428_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7279_IList_1_get_Item_m41424_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType SetRenderQueue_t22_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41424_GenericMethod;
// T System.Collections.Generic.IList`1<SetRenderQueue>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41424_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7279_il2cpp_TypeInfo/* declaring_type */
	, &SetRenderQueue_t22_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7279_IList_1_get_Item_m41424_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41424_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SetRenderQueue_t22_0_0_0;
static ParameterInfo IList_1_t7279_IList_1_set_Item_m41425_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SetRenderQueue_t22_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41425_GenericMethod;
// System.Void System.Collections.Generic.IList`1<SetRenderQueue>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41425_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7279_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7279_IList_1_set_Item_m41425_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41425_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7279_MethodInfos[] =
{
	&IList_1_IndexOf_m41426_MethodInfo,
	&IList_1_Insert_m41427_MethodInfo,
	&IList_1_RemoveAt_m41428_MethodInfo,
	&IList_1_get_Item_m41424_MethodInfo,
	&IList_1_set_Item_m41425_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7279_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7278_il2cpp_TypeInfo,
	&IEnumerable_1_t7280_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7279_0_0_0;
extern Il2CppType IList_1_t7279_1_0_0;
struct IList_1_t7279;
extern Il2CppGenericClass IList_1_t7279_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7279_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7279_MethodInfos/* methods */
	, IList_1_t7279_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7279_il2cpp_TypeInfo/* element_class */
	, IList_1_t7279_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7279_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7279_0_0_0/* byval_arg */
	, &IList_1_t7279_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7279_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<SetRenderQueue>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_9.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2743_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<SetRenderQueue>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_9MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<SetRenderQueue>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_5.h"
extern TypeInfo InvokableCall_1_t2744_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<SetRenderQueue>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_5MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14164_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14166_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<SetRenderQueue>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<SetRenderQueue>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<SetRenderQueue>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2743____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2743_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2743, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2743_FieldInfos[] =
{
	&CachedInvokableCall_1_t2743____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType SetRenderQueue_t22_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2743_CachedInvokableCall_1__ctor_m14162_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &SetRenderQueue_t22_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14162_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<SetRenderQueue>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14162_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2743_CachedInvokableCall_1__ctor_m14162_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14162_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2743_CachedInvokableCall_1_Invoke_m14163_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14163_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<SetRenderQueue>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14163_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2743_CachedInvokableCall_1_Invoke_m14163_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14163_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2743_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14162_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14163_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14163_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14167_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2743_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14163_MethodInfo,
	&InvokableCall_1_Find_m14167_MethodInfo,
};
extern Il2CppType UnityAction_1_t2745_0_0_0;
extern TypeInfo UnityAction_1_t2745_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSetRenderQueue_t22_m31716_MethodInfo;
extern TypeInfo SetRenderQueue_t22_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14169_MethodInfo;
extern TypeInfo SetRenderQueue_t22_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2743_RGCTXData[8] = 
{
	&UnityAction_1_t2745_0_0_0/* Type Usage */,
	&UnityAction_1_t2745_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSetRenderQueue_t22_m31716_MethodInfo/* Method Usage */,
	&SetRenderQueue_t22_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14169_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14164_MethodInfo/* Method Usage */,
	&SetRenderQueue_t22_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14166_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2743_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2743_1_0_0;
struct CachedInvokableCall_1_t2743;
extern Il2CppGenericClass CachedInvokableCall_1_t2743_GenericClass;
TypeInfo CachedInvokableCall_1_t2743_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2743_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2743_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2744_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2743_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2743_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2743_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2743_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2743_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2743_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2743_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2743)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<SetRenderQueue>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_12.h"
extern TypeInfo UnityAction_1_t2745_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<SetRenderQueue>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_12MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<SetRenderQueue>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<SetRenderQueue>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSetRenderQueue_t22_m31716(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<SetRenderQueue>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<SetRenderQueue>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<SetRenderQueue>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<SetRenderQueue>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<SetRenderQueue>
extern Il2CppType UnityAction_1_t2745_0_0_1;
FieldInfo InvokableCall_1_t2744____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2745_0_0_1/* type */
	, &InvokableCall_1_t2744_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2744, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2744_FieldInfos[] =
{
	&InvokableCall_1_t2744____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2744_InvokableCall_1__ctor_m14164_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14164_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<SetRenderQueue>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14164_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2744_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2744_InvokableCall_1__ctor_m14164_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14164_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2745_0_0_0;
static ParameterInfo InvokableCall_1_t2744_InvokableCall_1__ctor_m14165_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2745_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14165_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<SetRenderQueue>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14165_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2744_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2744_InvokableCall_1__ctor_m14165_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14165_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2744_InvokableCall_1_Invoke_m14166_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14166_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<SetRenderQueue>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14166_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2744_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2744_InvokableCall_1_Invoke_m14166_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14166_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2744_InvokableCall_1_Find_m14167_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14167_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<SetRenderQueue>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14167_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2744_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2744_InvokableCall_1_Find_m14167_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14167_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2744_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14164_MethodInfo,
	&InvokableCall_1__ctor_m14165_MethodInfo,
	&InvokableCall_1_Invoke_m14166_MethodInfo,
	&InvokableCall_1_Find_m14167_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2744_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14166_MethodInfo,
	&InvokableCall_1_Find_m14167_MethodInfo,
};
extern TypeInfo UnityAction_1_t2745_il2cpp_TypeInfo;
extern TypeInfo SetRenderQueue_t22_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2744_RGCTXData[5] = 
{
	&UnityAction_1_t2745_0_0_0/* Type Usage */,
	&UnityAction_1_t2745_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSetRenderQueue_t22_m31716_MethodInfo/* Method Usage */,
	&SetRenderQueue_t22_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14169_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2744_0_0_0;
extern Il2CppType InvokableCall_1_t2744_1_0_0;
struct InvokableCall_1_t2744;
extern Il2CppGenericClass InvokableCall_1_t2744_GenericClass;
TypeInfo InvokableCall_1_t2744_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2744_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2744_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2744_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2744_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2744_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2744_0_0_0/* byval_arg */
	, &InvokableCall_1_t2744_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2744_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2744_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2744)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<SetRenderQueue>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<SetRenderQueue>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<SetRenderQueue>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<SetRenderQueue>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<SetRenderQueue>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2745_UnityAction_1__ctor_m14168_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14168_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<SetRenderQueue>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14168_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2745_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2745_UnityAction_1__ctor_m14168_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14168_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueue_t22_0_0_0;
static ParameterInfo UnityAction_1_t2745_UnityAction_1_Invoke_m14169_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueue_t22_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14169_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<SetRenderQueue>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14169_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2745_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2745_UnityAction_1_Invoke_m14169_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14169_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueue_t22_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2745_UnityAction_1_BeginInvoke_m14170_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueue_t22_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14170_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<SetRenderQueue>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14170_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2745_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2745_UnityAction_1_BeginInvoke_m14170_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14170_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2745_UnityAction_1_EndInvoke_m14171_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14171_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<SetRenderQueue>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14171_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2745_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2745_UnityAction_1_EndInvoke_m14171_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14171_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2745_MethodInfos[] =
{
	&UnityAction_1__ctor_m14168_MethodInfo,
	&UnityAction_1_Invoke_m14169_MethodInfo,
	&UnityAction_1_BeginInvoke_m14170_MethodInfo,
	&UnityAction_1_EndInvoke_m14171_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14170_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14171_MethodInfo;
static MethodInfo* UnityAction_1_t2745_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14169_MethodInfo,
	&UnityAction_1_BeginInvoke_m14170_MethodInfo,
	&UnityAction_1_EndInvoke_m14171_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2745_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2745_1_0_0;
struct UnityAction_1_t2745;
extern Il2CppGenericClass UnityAction_1_t2745_GenericClass;
TypeInfo UnityAction_1_t2745_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2745_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2745_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2745_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2745_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2745_0_0_0/* byval_arg */
	, &UnityAction_1_t2745_1_0_0/* this_arg */
	, UnityAction_1_t2745_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2745_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2745)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t3161_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Int32>
extern MethodInfo IEnumerator_1_get_Current_m41429_MethodInfo;
static PropertyInfo IEnumerator_1_t3161____Current_PropertyInfo = 
{
	&IEnumerator_1_t3161_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41429_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t3161_PropertyInfos[] =
{
	&IEnumerator_1_t3161____Current_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41429_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41429_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t3161_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41429_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t3161_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41429_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t3161_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t3161_0_0_0;
extern Il2CppType IEnumerator_1_t3161_1_0_0;
struct IEnumerator_1_t3161;
extern Il2CppGenericClass IEnumerator_1_t3161_GenericClass;
TypeInfo IEnumerator_1_t3161_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t3161_MethodInfos/* methods */
	, IEnumerator_1_t3161_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t3161_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t3161_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t3161_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t3161_0_0_0/* byval_arg */
	, &IEnumerator_1_t3161_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t3161_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Int32>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_24.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2746_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Int32>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_24MethodDeclarations.h"

extern TypeInfo Int32_t93_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14176_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisInt32_t93_m31718_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisInt32_t93_m31718 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m14172_MethodInfo;
 void InternalEnumerator_1__ctor_m14172 (InternalEnumerator_1_t2746 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14173_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14173 (InternalEnumerator_1_t2746 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m14176(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m14176_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Int32_t93_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m14174_MethodInfo;
 void InternalEnumerator_1_Dispose_m14174 (InternalEnumerator_1_t2746 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m14175_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m14175 (InternalEnumerator_1_t2746 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m14176 (InternalEnumerator_1_t2746 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisInt32_t93_m31718(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisInt32_t93_m31718_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Int32>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2746____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2746_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2746, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2746____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2746_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2746, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2746_FieldInfos[] =
{
	&InternalEnumerator_1_t2746____array_0_FieldInfo,
	&InternalEnumerator_1_t2746____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t2746____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2746_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14173_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2746____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2746_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14176_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2746_PropertyInfos[] =
{
	&InternalEnumerator_1_t2746____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2746____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2746_InternalEnumerator_1__ctor_m14172_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14172_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14172_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14172/* method */
	, &InternalEnumerator_1_t2746_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2746_InternalEnumerator_1__ctor_m14172_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14172_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14173_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14173_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14173/* method */
	, &InternalEnumerator_1_t2746_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14173_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14174_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14174_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14174/* method */
	, &InternalEnumerator_1_t2746_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14174_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14175_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14175_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14175/* method */
	, &InternalEnumerator_1_t2746_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14175_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14176_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14176_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14176/* method */
	, &InternalEnumerator_1_t2746_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14176_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2746_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14172_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14173_MethodInfo,
	&InternalEnumerator_1_Dispose_m14174_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14175_MethodInfo,
	&InternalEnumerator_1_get_Current_m14176_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t2746_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14173_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14175_MethodInfo,
	&InternalEnumerator_1_Dispose_m14174_MethodInfo,
	&InternalEnumerator_1_get_Current_m14176_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2746_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t3161_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2746_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t3161_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2746_0_0_0;
extern Il2CppType InternalEnumerator_1_t2746_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2746_GenericClass;
TypeInfo InternalEnumerator_1_t2746_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2746_MethodInfos/* methods */
	, InternalEnumerator_1_t2746_PropertyInfos/* properties */
	, InternalEnumerator_1_t2746_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2746_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2746_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2746_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2746_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2746_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2746_1_0_0/* this_arg */
	, InternalEnumerator_1_t2746_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2746_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2746)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t3900_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Int32>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Int32>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Int32>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Int32>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Int32>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Int32>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Int32>
extern MethodInfo ICollection_1_get_Count_m36156_MethodInfo;
static PropertyInfo ICollection_1_t3900____Count_PropertyInfo = 
{
	&ICollection_1_t3900_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m36156_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41430_MethodInfo;
static PropertyInfo ICollection_1_t3900____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t3900_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41430_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t3900_PropertyInfos[] =
{
	&ICollection_1_t3900____Count_PropertyInfo,
	&ICollection_1_t3900____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m36156_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Int32>::get_Count()
MethodInfo ICollection_1_get_Count_m36156_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t3900_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m36156_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41430_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Int32>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41430_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t3900_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41430_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t3900_ICollection_1_Add_m41431_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41431_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Int32>::Add(T)
MethodInfo ICollection_1_Add_m41431_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t3900_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t3900_ICollection_1_Add_m41431_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41431_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41432_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Int32>::Clear()
MethodInfo ICollection_1_Clear_m41432_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t3900_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41432_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t3900_ICollection_1_Contains_m41433_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41433_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Int32>::Contains(T)
MethodInfo ICollection_1_Contains_m41433_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t3900_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t3900_ICollection_1_Contains_m41433_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41433_GenericMethod/* genericMethod */

};
extern Il2CppType Int32U5BU5D_t21_0_0_0;
extern Il2CppType Int32U5BU5D_t21_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t3900_ICollection_1_CopyTo_m36157_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Int32U5BU5D_t21_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m36157_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Int32>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m36157_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t3900_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t3900_ICollection_1_CopyTo_m36157_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m36157_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t3900_ICollection_1_Remove_m41434_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41434_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Int32>::Remove(T)
MethodInfo ICollection_1_Remove_m41434_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t3900_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t3900_ICollection_1_Remove_m41434_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41434_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t3900_MethodInfos[] =
{
	&ICollection_1_get_Count_m36156_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41430_MethodInfo,
	&ICollection_1_Add_m41431_MethodInfo,
	&ICollection_1_Clear_m41432_MethodInfo,
	&ICollection_1_Contains_m41433_MethodInfo,
	&ICollection_1_CopyTo_m36157_MethodInfo,
	&ICollection_1_Remove_m41434_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t3899_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t3900_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t3899_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t3900_0_0_0;
extern Il2CppType ICollection_1_t3900_1_0_0;
struct ICollection_1_t3900;
extern Il2CppGenericClass ICollection_1_t3900_GenericClass;
TypeInfo ICollection_1_t3900_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t3900_MethodInfos/* methods */
	, ICollection_1_t3900_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t3900_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t3900_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t3900_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t3900_0_0_0/* byval_arg */
	, &ICollection_1_t3900_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t3900_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Int32>
extern Il2CppType IEnumerator_1_t3161_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41435_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41435_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t3899_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3161_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41435_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t3899_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41435_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t3899_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t3899_0_0_0;
extern Il2CppType IEnumerable_1_t3899_1_0_0;
struct IEnumerable_1_t3899;
extern Il2CppGenericClass IEnumerable_1_t3899_GenericClass;
TypeInfo IEnumerable_1_t3899_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t3899_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t3899_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t3899_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t3899_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t3899_0_0_0/* byval_arg */
	, &IEnumerable_1_t3899_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t3899_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t3904_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Int32>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Int32>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Int32>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Int32>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Int32>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Int32>
extern MethodInfo IList_1_get_Item_m41436_MethodInfo;
extern MethodInfo IList_1_set_Item_m41437_MethodInfo;
static PropertyInfo IList_1_t3904____Item_PropertyInfo = 
{
	&IList_1_t3904_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41436_MethodInfo/* get */
	, &IList_1_set_Item_m41437_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t3904_PropertyInfos[] =
{
	&IList_1_t3904____Item_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t3904_IList_1_IndexOf_m41438_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41438_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Int32>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41438_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t3904_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t3904_IList_1_IndexOf_m41438_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41438_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t3904_IList_1_Insert_m41439_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41439_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Int32>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41439_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t3904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t3904_IList_1_Insert_m41439_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41439_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t3904_IList_1_RemoveAt_m41440_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41440_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Int32>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41440_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t3904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t3904_IList_1_RemoveAt_m41440_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41440_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t3904_IList_1_get_Item_m41436_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41436_GenericMethod;
// T System.Collections.Generic.IList`1<System.Int32>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41436_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t3904_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t3904_IList_1_get_Item_m41436_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41436_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t3904_IList_1_set_Item_m41437_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41437_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Int32>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41437_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t3904_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t3904_IList_1_set_Item_m41437_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41437_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t3904_MethodInfos[] =
{
	&IList_1_IndexOf_m41438_MethodInfo,
	&IList_1_Insert_m41439_MethodInfo,
	&IList_1_RemoveAt_m41440_MethodInfo,
	&IList_1_get_Item_m41436_MethodInfo,
	&IList_1_set_Item_m41437_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t3904_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t3900_il2cpp_TypeInfo,
	&IEnumerable_1_t3899_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t3904_0_0_0;
extern Il2CppType IList_1_t3904_1_0_0;
struct IList_1_t3904;
extern Il2CppGenericClass IList_1_t3904_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t3904_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t3904_MethodInfos/* methods */
	, IList_1_t3904_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t3904_il2cpp_TypeInfo/* element_class */
	, IList_1_t3904_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t3904_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t3904_0_0_0/* byval_arg */
	, &IList_1_t3904_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t3904_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7281_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>
extern MethodInfo ICollection_1_get_Count_m41441_MethodInfo;
static PropertyInfo ICollection_1_t7281____Count_PropertyInfo = 
{
	&ICollection_1_t7281_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41441_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41442_MethodInfo;
static PropertyInfo ICollection_1_t7281____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7281_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41442_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7281_PropertyInfos[] =
{
	&ICollection_1_t7281____Count_PropertyInfo,
	&ICollection_1_t7281____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41441_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::get_Count()
MethodInfo ICollection_1_get_Count_m41441_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7281_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41441_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41442_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41442_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7281_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41442_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2284_0_0_0;
extern Il2CppType IComparable_1_t2284_0_0_0;
static ParameterInfo ICollection_1_t7281_ICollection_1_Add_m41443_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2284_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41443_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::Add(T)
MethodInfo ICollection_1_Add_m41443_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7281_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7281_ICollection_1_Add_m41443_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41443_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41444_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::Clear()
MethodInfo ICollection_1_Clear_m41444_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7281_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41444_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2284_0_0_0;
static ParameterInfo ICollection_1_t7281_ICollection_1_Contains_m41445_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2284_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41445_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::Contains(T)
MethodInfo ICollection_1_Contains_m41445_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7281_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7281_ICollection_1_Contains_m41445_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41445_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1U5BU5D_t5212_0_0_0;
extern Il2CppType IComparable_1U5BU5D_t5212_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7281_ICollection_1_CopyTo_m41446_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1U5BU5D_t5212_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41446_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41446_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7281_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7281_ICollection_1_CopyTo_m41446_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41446_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2284_0_0_0;
static ParameterInfo ICollection_1_t7281_ICollection_1_Remove_m41447_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2284_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41447_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Int32>>::Remove(T)
MethodInfo ICollection_1_Remove_m41447_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7281_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7281_ICollection_1_Remove_m41447_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41447_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7281_MethodInfos[] =
{
	&ICollection_1_get_Count_m41441_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41442_MethodInfo,
	&ICollection_1_Add_m41443_MethodInfo,
	&ICollection_1_Clear_m41444_MethodInfo,
	&ICollection_1_Contains_m41445_MethodInfo,
	&ICollection_1_CopyTo_m41446_MethodInfo,
	&ICollection_1_Remove_m41447_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7283_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7281_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7283_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7281_0_0_0;
extern Il2CppType ICollection_1_t7281_1_0_0;
struct ICollection_1_t7281;
extern Il2CppGenericClass ICollection_1_t7281_GenericClass;
TypeInfo ICollection_1_t7281_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7281_MethodInfos/* methods */
	, ICollection_1_t7281_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7281_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7281_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7281_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7281_0_0_0/* byval_arg */
	, &ICollection_1_t7281_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7281_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Int32>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Int32>>
extern Il2CppType IEnumerator_1_t5738_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41448_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Int32>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41448_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7283_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5738_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41448_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7283_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41448_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7283_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7283_0_0_0;
extern Il2CppType IEnumerable_1_t7283_1_0_0;
struct IEnumerable_1_t7283;
extern Il2CppGenericClass IEnumerable_1_t7283_GenericClass;
TypeInfo IEnumerable_1_t7283_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7283_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7283_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7283_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7283_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7283_0_0_0/* byval_arg */
	, &IEnumerable_1_t7283_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7283_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5738_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Int32>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Int32>>
extern MethodInfo IEnumerator_1_get_Current_m41449_MethodInfo;
static PropertyInfo IEnumerator_1_t5738____Current_PropertyInfo = 
{
	&IEnumerator_1_t5738_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41449_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5738_PropertyInfos[] =
{
	&IEnumerator_1_t5738____Current_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2284_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41449_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Int32>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41449_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5738_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41449_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5738_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41449_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5738_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5738_0_0_0;
extern Il2CppType IEnumerator_1_t5738_1_0_0;
struct IEnumerator_1_t5738;
extern Il2CppGenericClass IEnumerator_1_t5738_GenericClass;
TypeInfo IEnumerator_1_t5738_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5738_MethodInfos/* methods */
	, IEnumerator_1_t5738_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5738_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5738_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5738_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5738_0_0_0/* byval_arg */
	, &IEnumerator_1_t5738_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5738_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparable_1_t2284_il2cpp_TypeInfo;



// System.Int32 System.IComparable`1<System.Int32>::CompareTo(T)
// Metadata Definition System.IComparable`1<System.Int32>
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IComparable_1_t2284_IComparable_1_CompareTo_m35570_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m35570_GenericMethod;
// System.Int32 System.IComparable`1<System.Int32>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m35570_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t2284_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IComparable_1_t2284_IComparable_1_CompareTo_m35570_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m35570_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t2284_MethodInfos[] =
{
	&IComparable_1_CompareTo_m35570_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t2284_1_0_0;
struct IComparable_1_t2284;
extern Il2CppGenericClass IComparable_1_t2284_GenericClass;
TypeInfo IComparable_1_t2284_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t2284_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t2284_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t2284_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t2284_0_0_0/* byval_arg */
	, &IComparable_1_t2284_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t2284_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_25.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2747_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_25MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m14181_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIComparable_1_t2284_m31729_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.Int32>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.Int32>>(System.Int32)
#define Array_InternalArray__get_Item_TisIComparable_1_t2284_m31729(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2747____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2747_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2747, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2747____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2747_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2747, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2747_FieldInfos[] =
{
	&InternalEnumerator_1_t2747____array_0_FieldInfo,
	&InternalEnumerator_1_t2747____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14178_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2747____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2747_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14178_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2747____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2747_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14181_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2747_PropertyInfos[] =
{
	&InternalEnumerator_1_t2747____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2747____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2747_InternalEnumerator_1__ctor_m14177_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14177_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14177_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2747_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2747_InternalEnumerator_1__ctor_m14177_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14177_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14178_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14178_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2747_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14178_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14179_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14179_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2747_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14179_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14180_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14180_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2747_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14180_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2284_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14181_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.Int32>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14181_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2747_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14181_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2747_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14177_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14178_MethodInfo,
	&InternalEnumerator_1_Dispose_m14179_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14180_MethodInfo,
	&InternalEnumerator_1_get_Current_m14181_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14180_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14179_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2747_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14178_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14180_MethodInfo,
	&InternalEnumerator_1_Dispose_m14179_MethodInfo,
	&InternalEnumerator_1_get_Current_m14181_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2747_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5738_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2747_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5738_il2cpp_TypeInfo, 7},
};
extern TypeInfo IComparable_1_t2284_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2747_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14181_MethodInfo/* Method Usage */,
	&IComparable_1_t2284_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIComparable_1_t2284_m31729_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2747_0_0_0;
extern Il2CppType InternalEnumerator_1_t2747_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2747_GenericClass;
TypeInfo InternalEnumerator_1_t2747_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2747_MethodInfos/* methods */
	, InternalEnumerator_1_t2747_PropertyInfos/* properties */
	, InternalEnumerator_1_t2747_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2747_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2747_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2747_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2747_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2747_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2747_1_0_0/* this_arg */
	, InternalEnumerator_1_t2747_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2747_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2747_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2747)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7282_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.Int32>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Int32>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Int32>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IComparable`1<System.Int32>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Int32>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Int32>>
extern MethodInfo IList_1_get_Item_m41450_MethodInfo;
extern MethodInfo IList_1_set_Item_m41451_MethodInfo;
static PropertyInfo IList_1_t7282____Item_PropertyInfo = 
{
	&IList_1_t7282_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41450_MethodInfo/* get */
	, &IList_1_set_Item_m41451_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7282_PropertyInfos[] =
{
	&IList_1_t7282____Item_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2284_0_0_0;
static ParameterInfo IList_1_t7282_IList_1_IndexOf_m41452_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2284_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41452_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.Int32>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41452_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7282_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7282_IList_1_IndexOf_m41452_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41452_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IComparable_1_t2284_0_0_0;
static ParameterInfo IList_1_t7282_IList_1_Insert_m41453_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2284_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41453_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Int32>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41453_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7282_IList_1_Insert_m41453_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41453_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7282_IList_1_RemoveAt_m41454_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41454_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Int32>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41454_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7282_IList_1_RemoveAt_m41454_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41454_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7282_IList_1_get_Item_m41450_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IComparable_1_t2284_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41450_GenericMethod;
// T System.Collections.Generic.IList`1<System.IComparable`1<System.Int32>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41450_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7282_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7282_IList_1_get_Item_m41450_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41450_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IComparable_1_t2284_0_0_0;
static ParameterInfo IList_1_t7282_IList_1_set_Item_m41451_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2284_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41451_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Int32>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41451_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7282_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7282_IList_1_set_Item_m41451_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41451_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7282_MethodInfos[] =
{
	&IList_1_IndexOf_m41452_MethodInfo,
	&IList_1_Insert_m41453_MethodInfo,
	&IList_1_RemoveAt_m41454_MethodInfo,
	&IList_1_get_Item_m41450_MethodInfo,
	&IList_1_set_Item_m41451_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7282_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7281_il2cpp_TypeInfo,
	&IEnumerable_1_t7283_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7282_0_0_0;
extern Il2CppType IList_1_t7282_1_0_0;
struct IList_1_t7282;
extern Il2CppGenericClass IList_1_t7282_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7282_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7282_MethodInfos/* methods */
	, IList_1_t7282_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7282_il2cpp_TypeInfo/* element_class */
	, IList_1_t7282_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7282_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7282_0_0_0/* byval_arg */
	, &IList_1_t7282_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7282_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7284_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>
extern MethodInfo ICollection_1_get_Count_m41455_MethodInfo;
static PropertyInfo ICollection_1_t7284____Count_PropertyInfo = 
{
	&ICollection_1_t7284_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41455_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41456_MethodInfo;
static PropertyInfo ICollection_1_t7284____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7284_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41456_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7284_PropertyInfos[] =
{
	&ICollection_1_t7284____Count_PropertyInfo,
	&ICollection_1_t7284____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41455_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::get_Count()
MethodInfo ICollection_1_get_Count_m41455_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7284_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41455_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41456_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41456_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7284_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41456_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2285_0_0_0;
extern Il2CppType IEquatable_1_t2285_0_0_0;
static ParameterInfo ICollection_1_t7284_ICollection_1_Add_m41457_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2285_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41457_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::Add(T)
MethodInfo ICollection_1_Add_m41457_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7284_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7284_ICollection_1_Add_m41457_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41457_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41458_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::Clear()
MethodInfo ICollection_1_Clear_m41458_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7284_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41458_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2285_0_0_0;
static ParameterInfo ICollection_1_t7284_ICollection_1_Contains_m41459_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2285_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41459_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::Contains(T)
MethodInfo ICollection_1_Contains_m41459_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7284_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7284_ICollection_1_Contains_m41459_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41459_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1U5BU5D_t5213_0_0_0;
extern Il2CppType IEquatable_1U5BU5D_t5213_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7284_ICollection_1_CopyTo_m41460_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1U5BU5D_t5213_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41460_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41460_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7284_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7284_ICollection_1_CopyTo_m41460_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41460_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2285_0_0_0;
static ParameterInfo ICollection_1_t7284_ICollection_1_Remove_m41461_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2285_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41461_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Int32>>::Remove(T)
MethodInfo ICollection_1_Remove_m41461_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7284_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7284_ICollection_1_Remove_m41461_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41461_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7284_MethodInfos[] =
{
	&ICollection_1_get_Count_m41455_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41456_MethodInfo,
	&ICollection_1_Add_m41457_MethodInfo,
	&ICollection_1_Clear_m41458_MethodInfo,
	&ICollection_1_Contains_m41459_MethodInfo,
	&ICollection_1_CopyTo_m41460_MethodInfo,
	&ICollection_1_Remove_m41461_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7286_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7284_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7286_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7284_0_0_0;
extern Il2CppType ICollection_1_t7284_1_0_0;
struct ICollection_1_t7284;
extern Il2CppGenericClass ICollection_1_t7284_GenericClass;
TypeInfo ICollection_1_t7284_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7284_MethodInfos/* methods */
	, ICollection_1_t7284_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7284_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7284_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7284_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7284_0_0_0/* byval_arg */
	, &ICollection_1_t7284_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7284_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Int32>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Int32>>
extern Il2CppType IEnumerator_1_t5740_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41462_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Int32>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41462_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7286_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5740_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41462_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7286_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41462_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7286_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7286_0_0_0;
extern Il2CppType IEnumerable_1_t7286_1_0_0;
struct IEnumerable_1_t7286;
extern Il2CppGenericClass IEnumerable_1_t7286_GenericClass;
TypeInfo IEnumerable_1_t7286_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7286_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7286_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7286_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7286_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7286_0_0_0/* byval_arg */
	, &IEnumerable_1_t7286_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7286_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5740_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Int32>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Int32>>
extern MethodInfo IEnumerator_1_get_Current_m41463_MethodInfo;
static PropertyInfo IEnumerator_1_t5740____Current_PropertyInfo = 
{
	&IEnumerator_1_t5740_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41463_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5740_PropertyInfos[] =
{
	&IEnumerator_1_t5740____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2285_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41463_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Int32>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41463_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5740_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41463_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5740_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41463_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5740_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5740_0_0_0;
extern Il2CppType IEnumerator_1_t5740_1_0_0;
struct IEnumerator_1_t5740;
extern Il2CppGenericClass IEnumerator_1_t5740_GenericClass;
TypeInfo IEnumerator_1_t5740_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5740_MethodInfos/* methods */
	, IEnumerator_1_t5740_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5740_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5740_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5740_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5740_0_0_0/* byval_arg */
	, &IEnumerator_1_t5740_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5740_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEquatable_1_t2285_il2cpp_TypeInfo;



// System.Boolean System.IEquatable`1<System.Int32>::Equals(T)
// Metadata Definition System.IEquatable`1<System.Int32>
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IEquatable_1_t2285_IEquatable_1_Equals_m41464_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEquatable_1_Equals_m41464_GenericMethod;
// System.Boolean System.IEquatable`1<System.Int32>::Equals(T)
MethodInfo IEquatable_1_Equals_m41464_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEquatable_1_t2285_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, IEquatable_1_t2285_IEquatable_1_Equals_m41464_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEquatable_1_Equals_m41464_GenericMethod/* genericMethod */

};
static MethodInfo* IEquatable_1_t2285_MethodInfos[] =
{
	&IEquatable_1_Equals_m41464_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1_t2285_1_0_0;
struct IEquatable_1_t2285;
extern Il2CppGenericClass IEquatable_1_t2285_GenericClass;
TypeInfo IEquatable_1_t2285_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, IEquatable_1_t2285_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t2285_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1_t2285_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1_t2285_0_0_0/* byval_arg */
	, &IEquatable_1_t2285_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEquatable_1_t2285_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_26.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2748_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_26MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m14186_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEquatable_1_t2285_m31740_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.Int32>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.Int32>>(System.Int32)
#define Array_InternalArray__get_Item_TisIEquatable_1_t2285_m31740(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2748____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2748_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2748, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2748____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2748_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2748, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2748_FieldInfos[] =
{
	&InternalEnumerator_1_t2748____array_0_FieldInfo,
	&InternalEnumerator_1_t2748____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14183_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2748____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2748_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14183_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2748____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2748_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14186_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2748_PropertyInfos[] =
{
	&InternalEnumerator_1_t2748____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2748____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2748_InternalEnumerator_1__ctor_m14182_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14182_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14182_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2748_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2748_InternalEnumerator_1__ctor_m14182_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14182_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14183_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14183_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2748_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14183_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14184_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14184_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2748_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14184_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14185_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14185_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2748_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14185_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2285_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14186_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.Int32>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14186_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2748_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14186_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2748_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14182_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14183_MethodInfo,
	&InternalEnumerator_1_Dispose_m14184_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14185_MethodInfo,
	&InternalEnumerator_1_get_Current_m14186_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14185_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14184_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2748_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14183_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14185_MethodInfo,
	&InternalEnumerator_1_Dispose_m14184_MethodInfo,
	&InternalEnumerator_1_get_Current_m14186_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2748_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5740_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2748_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5740_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEquatable_1_t2285_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2748_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14186_MethodInfo/* Method Usage */,
	&IEquatable_1_t2285_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEquatable_1_t2285_m31740_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2748_0_0_0;
extern Il2CppType InternalEnumerator_1_t2748_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2748_GenericClass;
TypeInfo InternalEnumerator_1_t2748_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2748_MethodInfos/* methods */
	, InternalEnumerator_1_t2748_PropertyInfos/* properties */
	, InternalEnumerator_1_t2748_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2748_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2748_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2748_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2748_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2748_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2748_1_0_0/* this_arg */
	, InternalEnumerator_1_t2748_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2748_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2748_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2748)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7285_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.Int32>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Int32>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Int32>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.Int32>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Int32>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Int32>>
extern MethodInfo IList_1_get_Item_m41465_MethodInfo;
extern MethodInfo IList_1_set_Item_m41466_MethodInfo;
static PropertyInfo IList_1_t7285____Item_PropertyInfo = 
{
	&IList_1_t7285_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41465_MethodInfo/* get */
	, &IList_1_set_Item_m41466_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7285_PropertyInfos[] =
{
	&IList_1_t7285____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2285_0_0_0;
static ParameterInfo IList_1_t7285_IList_1_IndexOf_m41467_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2285_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41467_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.Int32>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41467_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7285_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7285_IList_1_IndexOf_m41467_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41467_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEquatable_1_t2285_0_0_0;
static ParameterInfo IList_1_t7285_IList_1_Insert_m41468_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2285_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41468_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Int32>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41468_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7285_IList_1_Insert_m41468_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41468_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7285_IList_1_RemoveAt_m41469_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41469_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Int32>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41469_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7285_IList_1_RemoveAt_m41469_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41469_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7285_IList_1_get_Item_m41465_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEquatable_1_t2285_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41465_GenericMethod;
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.Int32>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41465_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7285_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2285_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7285_IList_1_get_Item_m41465_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41465_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEquatable_1_t2285_0_0_0;
static ParameterInfo IList_1_t7285_IList_1_set_Item_m41466_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2285_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41466_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Int32>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41466_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7285_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7285_IList_1_set_Item_m41466_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41466_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7285_MethodInfos[] =
{
	&IList_1_IndexOf_m41467_MethodInfo,
	&IList_1_Insert_m41468_MethodInfo,
	&IList_1_RemoveAt_m41469_MethodInfo,
	&IList_1_get_Item_m41465_MethodInfo,
	&IList_1_set_Item_m41466_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7285_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7284_il2cpp_TypeInfo,
	&IEnumerable_1_t7286_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7285_0_0_0;
extern Il2CppType IList_1_t7285_1_0_0;
struct IList_1_t7285;
extern Il2CppGenericClass IList_1_t7285_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7285_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7285_MethodInfos/* methods */
	, IList_1_t7285_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7285_il2cpp_TypeInfo/* element_class */
	, IList_1_t7285_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7285_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7285_0_0_0/* byval_arg */
	, &IList_1_t7285_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7285_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5742_il2cpp_TypeInfo;

// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Material>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Material>
extern MethodInfo IEnumerator_1_get_Current_m41470_MethodInfo;
static PropertyInfo IEnumerator_1_t5742____Current_PropertyInfo = 
{
	&IEnumerator_1_t5742_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41470_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5742_PropertyInfos[] =
{
	&IEnumerator_1_t5742____Current_PropertyInfo,
	NULL
};
extern Il2CppType Material_t4_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41470_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Material>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41470_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5742_il2cpp_TypeInfo/* declaring_type */
	, &Material_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41470_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5742_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41470_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5742_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5742_0_0_0;
extern Il2CppType IEnumerator_1_t5742_1_0_0;
struct IEnumerator_1_t5742;
extern Il2CppGenericClass IEnumerator_1_t5742_GenericClass;
TypeInfo IEnumerator_1_t5742_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5742_MethodInfos/* methods */
	, IEnumerator_1_t5742_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5742_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5742_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5742_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5742_0_0_0/* byval_arg */
	, &IEnumerator_1_t5742_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5742_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Material>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_27.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2749_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Material>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_27MethodDeclarations.h"

extern TypeInfo Material_t4_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14191_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMaterial_t4_m31751_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Material>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Material>(System.Int32)
#define Array_InternalArray__get_Item_TisMaterial_t4_m31751(__this, p0, method) (Material_t4 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Material>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Material>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Material>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Material>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Material>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Material>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2749____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2749_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2749, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2749____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2749_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2749, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2749_FieldInfos[] =
{
	&InternalEnumerator_1_t2749____array_0_FieldInfo,
	&InternalEnumerator_1_t2749____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14188_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2749____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2749_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14188_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2749____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2749_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14191_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2749_PropertyInfos[] =
{
	&InternalEnumerator_1_t2749____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2749____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2749_InternalEnumerator_1__ctor_m14187_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14187_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Material>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14187_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2749_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2749_InternalEnumerator_1__ctor_m14187_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14187_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14188_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Material>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14188_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2749_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14188_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14189_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Material>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14189_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2749_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14189_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14190_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Material>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14190_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2749_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14190_GenericMethod/* genericMethod */

};
extern Il2CppType Material_t4_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14191_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Material>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14191_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2749_il2cpp_TypeInfo/* declaring_type */
	, &Material_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14191_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2749_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14187_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14188_MethodInfo,
	&InternalEnumerator_1_Dispose_m14189_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14190_MethodInfo,
	&InternalEnumerator_1_get_Current_m14191_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14190_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14189_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2749_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14188_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14190_MethodInfo,
	&InternalEnumerator_1_Dispose_m14189_MethodInfo,
	&InternalEnumerator_1_get_Current_m14191_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2749_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5742_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2749_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5742_il2cpp_TypeInfo, 7},
};
extern TypeInfo Material_t4_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2749_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14191_MethodInfo/* Method Usage */,
	&Material_t4_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMaterial_t4_m31751_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2749_0_0_0;
extern Il2CppType InternalEnumerator_1_t2749_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2749_GenericClass;
TypeInfo InternalEnumerator_1_t2749_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2749_MethodInfos/* methods */
	, InternalEnumerator_1_t2749_PropertyInfos/* properties */
	, InternalEnumerator_1_t2749_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2749_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2749_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2749_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2749_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2749_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2749_1_0_0/* this_arg */
	, InternalEnumerator_1_t2749_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2749_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2749_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2749)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7287_il2cpp_TypeInfo;

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Material>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Material>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Material>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Material>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Material>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Material>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Material>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Material>
extern MethodInfo ICollection_1_get_Count_m41471_MethodInfo;
static PropertyInfo ICollection_1_t7287____Count_PropertyInfo = 
{
	&ICollection_1_t7287_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41471_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41472_MethodInfo;
static PropertyInfo ICollection_1_t7287____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7287_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41472_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7287_PropertyInfos[] =
{
	&ICollection_1_t7287____Count_PropertyInfo,
	&ICollection_1_t7287____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41471_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Material>::get_Count()
MethodInfo ICollection_1_get_Count_m41471_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7287_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41471_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41472_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Material>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41472_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7287_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41472_GenericMethod/* genericMethod */

};
extern Il2CppType Material_t4_0_0_0;
extern Il2CppType Material_t4_0_0_0;
static ParameterInfo ICollection_1_t7287_ICollection_1_Add_m41473_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Material_t4_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41473_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Material>::Add(T)
MethodInfo ICollection_1_Add_m41473_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7287_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7287_ICollection_1_Add_m41473_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41473_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41474_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Material>::Clear()
MethodInfo ICollection_1_Clear_m41474_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7287_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41474_GenericMethod/* genericMethod */

};
extern Il2CppType Material_t4_0_0_0;
static ParameterInfo ICollection_1_t7287_ICollection_1_Contains_m41475_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Material_t4_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41475_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Material>::Contains(T)
MethodInfo ICollection_1_Contains_m41475_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7287_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7287_ICollection_1_Contains_m41475_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41475_GenericMethod/* genericMethod */

};
extern Il2CppType MaterialU5BU5D_t114_0_0_0;
extern Il2CppType MaterialU5BU5D_t114_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7287_ICollection_1_CopyTo_m41476_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MaterialU5BU5D_t114_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41476_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Material>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41476_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7287_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7287_ICollection_1_CopyTo_m41476_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41476_GenericMethod/* genericMethod */

};
extern Il2CppType Material_t4_0_0_0;
static ParameterInfo ICollection_1_t7287_ICollection_1_Remove_m41477_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Material_t4_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41477_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Material>::Remove(T)
MethodInfo ICollection_1_Remove_m41477_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7287_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7287_ICollection_1_Remove_m41477_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41477_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7287_MethodInfos[] =
{
	&ICollection_1_get_Count_m41471_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41472_MethodInfo,
	&ICollection_1_Add_m41473_MethodInfo,
	&ICollection_1_Clear_m41474_MethodInfo,
	&ICollection_1_Contains_m41475_MethodInfo,
	&ICollection_1_CopyTo_m41476_MethodInfo,
	&ICollection_1_Remove_m41477_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7289_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7287_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7289_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7287_0_0_0;
extern Il2CppType ICollection_1_t7287_1_0_0;
struct ICollection_1_t7287;
extern Il2CppGenericClass ICollection_1_t7287_GenericClass;
TypeInfo ICollection_1_t7287_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7287_MethodInfos/* methods */
	, ICollection_1_t7287_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7287_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7287_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7287_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7287_0_0_0/* byval_arg */
	, &ICollection_1_t7287_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7287_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Material>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Material>
extern Il2CppType IEnumerator_1_t5742_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41478_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Material>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41478_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7289_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5742_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41478_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7289_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41478_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7289_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7289_0_0_0;
extern Il2CppType IEnumerable_1_t7289_1_0_0;
struct IEnumerable_1_t7289;
extern Il2CppGenericClass IEnumerable_1_t7289_GenericClass;
TypeInfo IEnumerable_1_t7289_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7289_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7289_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7289_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7289_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7289_0_0_0/* byval_arg */
	, &IEnumerable_1_t7289_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7289_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7288_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Material>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Material>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Material>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Material>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Material>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Material>
extern MethodInfo IList_1_get_Item_m41479_MethodInfo;
extern MethodInfo IList_1_set_Item_m41480_MethodInfo;
static PropertyInfo IList_1_t7288____Item_PropertyInfo = 
{
	&IList_1_t7288_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41479_MethodInfo/* get */
	, &IList_1_set_Item_m41480_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7288_PropertyInfos[] =
{
	&IList_1_t7288____Item_PropertyInfo,
	NULL
};
extern Il2CppType Material_t4_0_0_0;
static ParameterInfo IList_1_t7288_IList_1_IndexOf_m41481_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Material_t4_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41481_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Material>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41481_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7288_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7288_IList_1_IndexOf_m41481_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41481_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Material_t4_0_0_0;
static ParameterInfo IList_1_t7288_IList_1_Insert_m41482_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Material_t4_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41482_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Material>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41482_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7288_IList_1_Insert_m41482_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41482_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7288_IList_1_RemoveAt_m41483_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41483_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Material>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41483_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7288_IList_1_RemoveAt_m41483_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41483_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7288_IList_1_get_Item_m41479_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Material_t4_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41479_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Material>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41479_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7288_il2cpp_TypeInfo/* declaring_type */
	, &Material_t4_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7288_IList_1_get_Item_m41479_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41479_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Material_t4_0_0_0;
static ParameterInfo IList_1_t7288_IList_1_set_Item_m41480_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Material_t4_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41480_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Material>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41480_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7288_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7288_IList_1_set_Item_m41480_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41480_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7288_MethodInfos[] =
{
	&IList_1_IndexOf_m41481_MethodInfo,
	&IList_1_Insert_m41482_MethodInfo,
	&IList_1_RemoveAt_m41483_MethodInfo,
	&IList_1_get_Item_m41479_MethodInfo,
	&IList_1_set_Item_m41480_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7288_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7287_il2cpp_TypeInfo,
	&IEnumerable_1_t7289_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7288_0_0_0;
extern Il2CppType IList_1_t7288_1_0_0;
struct IList_1_t7288;
extern Il2CppGenericClass IList_1_t7288_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7288_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7288_MethodInfos/* methods */
	, IList_1_t7288_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7288_il2cpp_TypeInfo/* element_class */
	, IList_1_t7288_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7288_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7288_0_0_0/* byval_arg */
	, &IList_1_t7288_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7288_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5744_il2cpp_TypeInfo;

// SetRenderQueueChildren
#include "AssemblyU2DCSharp_SetRenderQueueChildren.h"


// T System.Collections.Generic.IEnumerator`1<SetRenderQueueChildren>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<SetRenderQueueChildren>
extern MethodInfo IEnumerator_1_get_Current_m41484_MethodInfo;
static PropertyInfo IEnumerator_1_t5744____Current_PropertyInfo = 
{
	&IEnumerator_1_t5744_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41484_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5744_PropertyInfos[] =
{
	&IEnumerator_1_t5744____Current_PropertyInfo,
	NULL
};
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41484_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<SetRenderQueueChildren>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41484_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5744_il2cpp_TypeInfo/* declaring_type */
	, &SetRenderQueueChildren_t23_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41484_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5744_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41484_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5744_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5744_0_0_0;
extern Il2CppType IEnumerator_1_t5744_1_0_0;
struct IEnumerator_1_t5744;
extern Il2CppGenericClass IEnumerator_1_t5744_GenericClass;
TypeInfo IEnumerator_1_t5744_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5744_MethodInfos/* methods */
	, IEnumerator_1_t5744_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5744_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5744_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5744_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5744_0_0_0/* byval_arg */
	, &IEnumerator_1_t5744_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5744_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<SetRenderQueueChildren>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_28.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2750_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<SetRenderQueueChildren>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_28MethodDeclarations.h"

extern TypeInfo SetRenderQueueChildren_t23_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14196_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSetRenderQueueChildren_t23_m31762_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<SetRenderQueueChildren>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<SetRenderQueueChildren>(System.Int32)
#define Array_InternalArray__get_Item_TisSetRenderQueueChildren_t23_m31762(__this, p0, method) (SetRenderQueueChildren_t23 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<SetRenderQueueChildren>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<SetRenderQueueChildren>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<SetRenderQueueChildren>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<SetRenderQueueChildren>::MoveNext()
// T System.Array/InternalEnumerator`1<SetRenderQueueChildren>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<SetRenderQueueChildren>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2750____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2750_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2750, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2750____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2750_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2750, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2750_FieldInfos[] =
{
	&InternalEnumerator_1_t2750____array_0_FieldInfo,
	&InternalEnumerator_1_t2750____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14193_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2750____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2750_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14193_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2750____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2750_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14196_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2750_PropertyInfos[] =
{
	&InternalEnumerator_1_t2750____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2750____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2750_InternalEnumerator_1__ctor_m14192_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14192_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<SetRenderQueueChildren>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14192_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2750_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2750_InternalEnumerator_1__ctor_m14192_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14192_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14193_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<SetRenderQueueChildren>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14193_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2750_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14193_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14194_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<SetRenderQueueChildren>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14194_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2750_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14194_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14195_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<SetRenderQueueChildren>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14195_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2750_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14195_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14196_GenericMethod;
// T System.Array/InternalEnumerator`1<SetRenderQueueChildren>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14196_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2750_il2cpp_TypeInfo/* declaring_type */
	, &SetRenderQueueChildren_t23_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14196_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2750_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14192_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14193_MethodInfo,
	&InternalEnumerator_1_Dispose_m14194_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14195_MethodInfo,
	&InternalEnumerator_1_get_Current_m14196_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14195_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14194_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2750_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14193_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14195_MethodInfo,
	&InternalEnumerator_1_Dispose_m14194_MethodInfo,
	&InternalEnumerator_1_get_Current_m14196_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2750_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5744_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2750_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5744_il2cpp_TypeInfo, 7},
};
extern TypeInfo SetRenderQueueChildren_t23_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2750_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14196_MethodInfo/* Method Usage */,
	&SetRenderQueueChildren_t23_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSetRenderQueueChildren_t23_m31762_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2750_0_0_0;
extern Il2CppType InternalEnumerator_1_t2750_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2750_GenericClass;
TypeInfo InternalEnumerator_1_t2750_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2750_MethodInfos/* methods */
	, InternalEnumerator_1_t2750_PropertyInfos/* properties */
	, InternalEnumerator_1_t2750_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2750_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2750_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2750_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2750_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2750_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2750_1_0_0/* this_arg */
	, InternalEnumerator_1_t2750_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2750_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2750_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2750)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7290_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<SetRenderQueueChildren>
extern MethodInfo ICollection_1_get_Count_m41485_MethodInfo;
static PropertyInfo ICollection_1_t7290____Count_PropertyInfo = 
{
	&ICollection_1_t7290_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41485_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41486_MethodInfo;
static PropertyInfo ICollection_1_t7290____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7290_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41486_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7290_PropertyInfos[] =
{
	&ICollection_1_t7290____Count_PropertyInfo,
	&ICollection_1_t7290____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41485_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::get_Count()
MethodInfo ICollection_1_get_Count_m41485_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7290_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41485_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41486_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41486_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7290_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41486_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
static ParameterInfo ICollection_1_t7290_ICollection_1_Add_m41487_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueueChildren_t23_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41487_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::Add(T)
MethodInfo ICollection_1_Add_m41487_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7290_ICollection_1_Add_m41487_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41487_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41488_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::Clear()
MethodInfo ICollection_1_Clear_m41488_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41488_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
static ParameterInfo ICollection_1_t7290_ICollection_1_Contains_m41489_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueueChildren_t23_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41489_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::Contains(T)
MethodInfo ICollection_1_Contains_m41489_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7290_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7290_ICollection_1_Contains_m41489_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41489_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueueChildrenU5BU5D_t5164_0_0_0;
extern Il2CppType SetRenderQueueChildrenU5BU5D_t5164_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7290_ICollection_1_CopyTo_m41490_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueueChildrenU5BU5D_t5164_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41490_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41490_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7290_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7290_ICollection_1_CopyTo_m41490_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41490_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
static ParameterInfo ICollection_1_t7290_ICollection_1_Remove_m41491_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueueChildren_t23_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41491_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<SetRenderQueueChildren>::Remove(T)
MethodInfo ICollection_1_Remove_m41491_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7290_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7290_ICollection_1_Remove_m41491_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41491_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7290_MethodInfos[] =
{
	&ICollection_1_get_Count_m41485_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41486_MethodInfo,
	&ICollection_1_Add_m41487_MethodInfo,
	&ICollection_1_Clear_m41488_MethodInfo,
	&ICollection_1_Contains_m41489_MethodInfo,
	&ICollection_1_CopyTo_m41490_MethodInfo,
	&ICollection_1_Remove_m41491_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7292_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7290_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7292_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7290_0_0_0;
extern Il2CppType ICollection_1_t7290_1_0_0;
struct ICollection_1_t7290;
extern Il2CppGenericClass ICollection_1_t7290_GenericClass;
TypeInfo ICollection_1_t7290_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7290_MethodInfos/* methods */
	, ICollection_1_t7290_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7290_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7290_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7290_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7290_0_0_0/* byval_arg */
	, &ICollection_1_t7290_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7290_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<SetRenderQueueChildren>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<SetRenderQueueChildren>
extern Il2CppType IEnumerator_1_t5744_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41492_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<SetRenderQueueChildren>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41492_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7292_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5744_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41492_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7292_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41492_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7292_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7292_0_0_0;
extern Il2CppType IEnumerable_1_t7292_1_0_0;
struct IEnumerable_1_t7292;
extern Il2CppGenericClass IEnumerable_1_t7292_GenericClass;
TypeInfo IEnumerable_1_t7292_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7292_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7292_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7292_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7292_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7292_0_0_0/* byval_arg */
	, &IEnumerable_1_t7292_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7292_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7291_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<SetRenderQueueChildren>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<SetRenderQueueChildren>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<SetRenderQueueChildren>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<SetRenderQueueChildren>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<SetRenderQueueChildren>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<SetRenderQueueChildren>
extern MethodInfo IList_1_get_Item_m41493_MethodInfo;
extern MethodInfo IList_1_set_Item_m41494_MethodInfo;
static PropertyInfo IList_1_t7291____Item_PropertyInfo = 
{
	&IList_1_t7291_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41493_MethodInfo/* get */
	, &IList_1_set_Item_m41494_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7291_PropertyInfos[] =
{
	&IList_1_t7291____Item_PropertyInfo,
	NULL
};
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
static ParameterInfo IList_1_t7291_IList_1_IndexOf_m41495_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueueChildren_t23_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41495_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<SetRenderQueueChildren>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41495_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7291_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7291_IList_1_IndexOf_m41495_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41495_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
static ParameterInfo IList_1_t7291_IList_1_Insert_m41496_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SetRenderQueueChildren_t23_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41496_GenericMethod;
// System.Void System.Collections.Generic.IList`1<SetRenderQueueChildren>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41496_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7291_IList_1_Insert_m41496_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41496_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7291_IList_1_RemoveAt_m41497_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41497_GenericMethod;
// System.Void System.Collections.Generic.IList`1<SetRenderQueueChildren>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41497_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7291_IList_1_RemoveAt_m41497_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41497_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7291_IList_1_get_Item_m41493_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41493_GenericMethod;
// T System.Collections.Generic.IList`1<SetRenderQueueChildren>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41493_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7291_il2cpp_TypeInfo/* declaring_type */
	, &SetRenderQueueChildren_t23_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7291_IList_1_get_Item_m41493_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41493_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
static ParameterInfo IList_1_t7291_IList_1_set_Item_m41494_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SetRenderQueueChildren_t23_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41494_GenericMethod;
// System.Void System.Collections.Generic.IList`1<SetRenderQueueChildren>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41494_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7291_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7291_IList_1_set_Item_m41494_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41494_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7291_MethodInfos[] =
{
	&IList_1_IndexOf_m41495_MethodInfo,
	&IList_1_Insert_m41496_MethodInfo,
	&IList_1_RemoveAt_m41497_MethodInfo,
	&IList_1_get_Item_m41493_MethodInfo,
	&IList_1_set_Item_m41494_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7291_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7290_il2cpp_TypeInfo,
	&IEnumerable_1_t7292_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7291_0_0_0;
extern Il2CppType IList_1_t7291_1_0_0;
struct IList_1_t7291;
extern Il2CppGenericClass IList_1_t7291_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7291_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7291_MethodInfos/* methods */
	, IList_1_t7291_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7291_il2cpp_TypeInfo/* element_class */
	, IList_1_t7291_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7291_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7291_0_0_0/* byval_arg */
	, &IList_1_t7291_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7291_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<SetRenderQueueChildren>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_10.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2751_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<SetRenderQueueChildren>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_10MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_6.h"
extern TypeInfo InvokableCall_1_t2752_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_6MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14199_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14201_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<SetRenderQueueChildren>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<SetRenderQueueChildren>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<SetRenderQueueChildren>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2751____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2751_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2751, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2751_FieldInfos[] =
{
	&CachedInvokableCall_1_t2751____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2751_CachedInvokableCall_1__ctor_m14197_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &SetRenderQueueChildren_t23_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14197_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<SetRenderQueueChildren>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14197_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2751_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2751_CachedInvokableCall_1__ctor_m14197_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14197_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2751_CachedInvokableCall_1_Invoke_m14198_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14198_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<SetRenderQueueChildren>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14198_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2751_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2751_CachedInvokableCall_1_Invoke_m14198_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14198_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2751_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14197_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14198_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14198_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14202_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2751_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14198_MethodInfo,
	&InvokableCall_1_Find_m14202_MethodInfo,
};
extern Il2CppType UnityAction_1_t2753_0_0_0;
extern TypeInfo UnityAction_1_t2753_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisSetRenderQueueChildren_t23_m31772_MethodInfo;
extern TypeInfo SetRenderQueueChildren_t23_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14204_MethodInfo;
extern TypeInfo SetRenderQueueChildren_t23_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2751_RGCTXData[8] = 
{
	&UnityAction_1_t2753_0_0_0/* Type Usage */,
	&UnityAction_1_t2753_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSetRenderQueueChildren_t23_m31772_MethodInfo/* Method Usage */,
	&SetRenderQueueChildren_t23_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14204_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14199_MethodInfo/* Method Usage */,
	&SetRenderQueueChildren_t23_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14201_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2751_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2751_1_0_0;
struct CachedInvokableCall_1_t2751;
extern Il2CppGenericClass CachedInvokableCall_1_t2751_GenericClass;
TypeInfo CachedInvokableCall_1_t2751_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2751_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2751_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2752_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2751_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2751_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2751_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2751_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2751_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2751_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2751_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2751)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<SetRenderQueueChildren>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_13.h"
extern TypeInfo UnityAction_1_t2753_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<SetRenderQueueChildren>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_13MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<SetRenderQueueChildren>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<SetRenderQueueChildren>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisSetRenderQueueChildren_t23_m31772(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>
extern Il2CppType UnityAction_1_t2753_0_0_1;
FieldInfo InvokableCall_1_t2752____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2753_0_0_1/* type */
	, &InvokableCall_1_t2752_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2752, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2752_FieldInfos[] =
{
	&InvokableCall_1_t2752____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2752_InvokableCall_1__ctor_m14199_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14199_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14199_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2752_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2752_InvokableCall_1__ctor_m14199_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14199_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2753_0_0_0;
static ParameterInfo InvokableCall_1_t2752_InvokableCall_1__ctor_m14200_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2753_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14200_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14200_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2752_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2752_InvokableCall_1__ctor_m14200_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14200_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2752_InvokableCall_1_Invoke_m14201_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14201_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14201_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2752_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2752_InvokableCall_1_Invoke_m14201_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14201_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2752_InvokableCall_1_Find_m14202_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14202_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14202_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2752_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2752_InvokableCall_1_Find_m14202_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14202_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2752_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14199_MethodInfo,
	&InvokableCall_1__ctor_m14200_MethodInfo,
	&InvokableCall_1_Invoke_m14201_MethodInfo,
	&InvokableCall_1_Find_m14202_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2752_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14201_MethodInfo,
	&InvokableCall_1_Find_m14202_MethodInfo,
};
extern TypeInfo UnityAction_1_t2753_il2cpp_TypeInfo;
extern TypeInfo SetRenderQueueChildren_t23_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2752_RGCTXData[5] = 
{
	&UnityAction_1_t2753_0_0_0/* Type Usage */,
	&UnityAction_1_t2753_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisSetRenderQueueChildren_t23_m31772_MethodInfo/* Method Usage */,
	&SetRenderQueueChildren_t23_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14204_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2752_0_0_0;
extern Il2CppType InvokableCall_1_t2752_1_0_0;
struct InvokableCall_1_t2752;
extern Il2CppGenericClass InvokableCall_1_t2752_GenericClass;
TypeInfo InvokableCall_1_t2752_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2752_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2752_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2752_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2752_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2752_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2752_0_0_0/* byval_arg */
	, &InvokableCall_1_t2752_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2752_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2752_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2752)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<SetRenderQueueChildren>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<SetRenderQueueChildren>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<SetRenderQueueChildren>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<SetRenderQueueChildren>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<SetRenderQueueChildren>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2753_UnityAction_1__ctor_m14203_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14203_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<SetRenderQueueChildren>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14203_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2753_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2753_UnityAction_1__ctor_m14203_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14203_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
static ParameterInfo UnityAction_1_t2753_UnityAction_1_Invoke_m14204_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueueChildren_t23_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14204_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<SetRenderQueueChildren>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14204_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2753_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2753_UnityAction_1_Invoke_m14204_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14204_GenericMethod/* genericMethod */

};
extern Il2CppType SetRenderQueueChildren_t23_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2753_UnityAction_1_BeginInvoke_m14205_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &SetRenderQueueChildren_t23_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14205_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<SetRenderQueueChildren>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14205_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2753_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2753_UnityAction_1_BeginInvoke_m14205_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14205_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2753_UnityAction_1_EndInvoke_m14206_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14206_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<SetRenderQueueChildren>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14206_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2753_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2753_UnityAction_1_EndInvoke_m14206_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14206_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2753_MethodInfos[] =
{
	&UnityAction_1__ctor_m14203_MethodInfo,
	&UnityAction_1_Invoke_m14204_MethodInfo,
	&UnityAction_1_BeginInvoke_m14205_MethodInfo,
	&UnityAction_1_EndInvoke_m14206_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14205_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14206_MethodInfo;
static MethodInfo* UnityAction_1_t2753_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14204_MethodInfo,
	&UnityAction_1_BeginInvoke_m14205_MethodInfo,
	&UnityAction_1_EndInvoke_m14206_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2753_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2753_1_0_0;
struct UnityAction_1_t2753;
extern Il2CppGenericClass UnityAction_1_t2753_GenericClass;
TypeInfo UnityAction_1_t2753_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2753_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2753_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2753_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2753_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2753_0_0_0/* byval_arg */
	, &UnityAction_1_t2753_1_0_0/* this_arg */
	, UnityAction_1_t2753_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2753_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2753)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5746_il2cpp_TypeInfo;

// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Renderer>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Renderer>
extern MethodInfo IEnumerator_1_get_Current_m41498_MethodInfo;
static PropertyInfo IEnumerator_1_t5746____Current_PropertyInfo = 
{
	&IEnumerator_1_t5746_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41498_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5746_PropertyInfos[] =
{
	&IEnumerator_1_t5746____Current_PropertyInfo,
	NULL
};
extern Il2CppType Renderer_t7_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41498_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Renderer>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41498_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5746_il2cpp_TypeInfo/* declaring_type */
	, &Renderer_t7_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41498_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5746_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41498_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5746_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5746_0_0_0;
extern Il2CppType IEnumerator_1_t5746_1_0_0;
struct IEnumerator_1_t5746;
extern Il2CppGenericClass IEnumerator_1_t5746_GenericClass;
TypeInfo IEnumerator_1_t5746_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5746_MethodInfos/* methods */
	, IEnumerator_1_t5746_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5746_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5746_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5746_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5746_0_0_0/* byval_arg */
	, &IEnumerator_1_t5746_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5746_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Renderer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_29.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2754_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Renderer>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_29MethodDeclarations.h"

extern TypeInfo Renderer_t7_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14211_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRenderer_t7_m31774_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Renderer>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Renderer>(System.Int32)
#define Array_InternalArray__get_Item_TisRenderer_t7_m31774(__this, p0, method) (Renderer_t7 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Renderer>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Renderer>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Renderer>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Renderer>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Renderer>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Renderer>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2754____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2754, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2754____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2754, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2754_FieldInfos[] =
{
	&InternalEnumerator_1_t2754____array_0_FieldInfo,
	&InternalEnumerator_1_t2754____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14208_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2754____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2754_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14208_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2754____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2754_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14211_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2754_PropertyInfos[] =
{
	&InternalEnumerator_1_t2754____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2754____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2754_InternalEnumerator_1__ctor_m14207_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14207_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Renderer>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14207_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2754_InternalEnumerator_1__ctor_m14207_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14207_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14208_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Renderer>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14208_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14208_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14209_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Renderer>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14209_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14209_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14210_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Renderer>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14210_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14210_GenericMethod/* genericMethod */

};
extern Il2CppType Renderer_t7_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14211_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Renderer>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14211_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* declaring_type */
	, &Renderer_t7_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14211_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2754_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14207_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14208_MethodInfo,
	&InternalEnumerator_1_Dispose_m14209_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14210_MethodInfo,
	&InternalEnumerator_1_get_Current_m14211_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14210_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14209_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2754_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14208_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14210_MethodInfo,
	&InternalEnumerator_1_Dispose_m14209_MethodInfo,
	&InternalEnumerator_1_get_Current_m14211_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2754_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5746_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2754_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5746_il2cpp_TypeInfo, 7},
};
extern TypeInfo Renderer_t7_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2754_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14211_MethodInfo/* Method Usage */,
	&Renderer_t7_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisRenderer_t7_m31774_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2754_0_0_0;
extern Il2CppType InternalEnumerator_1_t2754_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2754_GenericClass;
TypeInfo InternalEnumerator_1_t2754_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2754_MethodInfos/* methods */
	, InternalEnumerator_1_t2754_PropertyInfos/* properties */
	, InternalEnumerator_1_t2754_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2754_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2754_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2754_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2754_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2754_1_0_0/* this_arg */
	, InternalEnumerator_1_t2754_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2754_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2754_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2754)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7293_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Renderer>
extern MethodInfo ICollection_1_get_Count_m41499_MethodInfo;
static PropertyInfo ICollection_1_t7293____Count_PropertyInfo = 
{
	&ICollection_1_t7293_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41499_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41500_MethodInfo;
static PropertyInfo ICollection_1_t7293____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7293_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41500_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7293_PropertyInfos[] =
{
	&ICollection_1_t7293____Count_PropertyInfo,
	&ICollection_1_t7293____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41499_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::get_Count()
MethodInfo ICollection_1_get_Count_m41499_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7293_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41499_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41500_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41500_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7293_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41500_GenericMethod/* genericMethod */

};
extern Il2CppType Renderer_t7_0_0_0;
extern Il2CppType Renderer_t7_0_0_0;
static ParameterInfo ICollection_1_t7293_ICollection_1_Add_m41501_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Renderer_t7_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41501_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Add(T)
MethodInfo ICollection_1_Add_m41501_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7293_ICollection_1_Add_m41501_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41501_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41502_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Clear()
MethodInfo ICollection_1_Clear_m41502_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41502_GenericMethod/* genericMethod */

};
extern Il2CppType Renderer_t7_0_0_0;
static ParameterInfo ICollection_1_t7293_ICollection_1_Contains_m41503_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Renderer_t7_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41503_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Contains(T)
MethodInfo ICollection_1_Contains_m41503_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7293_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7293_ICollection_1_Contains_m41503_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41503_GenericMethod/* genericMethod */

};
extern Il2CppType RendererU5BU5D_t116_0_0_0;
extern Il2CppType RendererU5BU5D_t116_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7293_ICollection_1_CopyTo_m41504_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &RendererU5BU5D_t116_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41504_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41504_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7293_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7293_ICollection_1_CopyTo_m41504_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41504_GenericMethod/* genericMethod */

};
extern Il2CppType Renderer_t7_0_0_0;
static ParameterInfo ICollection_1_t7293_ICollection_1_Remove_m41505_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Renderer_t7_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41505_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Renderer>::Remove(T)
MethodInfo ICollection_1_Remove_m41505_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7293_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7293_ICollection_1_Remove_m41505_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41505_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7293_MethodInfos[] =
{
	&ICollection_1_get_Count_m41499_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41500_MethodInfo,
	&ICollection_1_Add_m41501_MethodInfo,
	&ICollection_1_Clear_m41502_MethodInfo,
	&ICollection_1_Contains_m41503_MethodInfo,
	&ICollection_1_CopyTo_m41504_MethodInfo,
	&ICollection_1_Remove_m41505_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7295_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7293_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7295_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7293_0_0_0;
extern Il2CppType ICollection_1_t7293_1_0_0;
struct ICollection_1_t7293;
extern Il2CppGenericClass ICollection_1_t7293_GenericClass;
TypeInfo ICollection_1_t7293_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7293_MethodInfos/* methods */
	, ICollection_1_t7293_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7293_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7293_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7293_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7293_0_0_0/* byval_arg */
	, &ICollection_1_t7293_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7293_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Renderer>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Renderer>
extern Il2CppType IEnumerator_1_t5746_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41506_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Renderer>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41506_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7295_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5746_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41506_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7295_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41506_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7295_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7295_0_0_0;
extern Il2CppType IEnumerable_1_t7295_1_0_0;
struct IEnumerable_1_t7295;
extern Il2CppGenericClass IEnumerable_1_t7295_GenericClass;
TypeInfo IEnumerable_1_t7295_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7295_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7295_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7295_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7295_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7295_0_0_0/* byval_arg */
	, &IEnumerable_1_t7295_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7295_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7294_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Renderer>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Renderer>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Renderer>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Renderer>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Renderer>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Renderer>
extern MethodInfo IList_1_get_Item_m41507_MethodInfo;
extern MethodInfo IList_1_set_Item_m41508_MethodInfo;
static PropertyInfo IList_1_t7294____Item_PropertyInfo = 
{
	&IList_1_t7294_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41507_MethodInfo/* get */
	, &IList_1_set_Item_m41508_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7294_PropertyInfos[] =
{
	&IList_1_t7294____Item_PropertyInfo,
	NULL
};
extern Il2CppType Renderer_t7_0_0_0;
static ParameterInfo IList_1_t7294_IList_1_IndexOf_m41509_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Renderer_t7_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41509_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Renderer>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41509_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7294_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7294_IList_1_IndexOf_m41509_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41509_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Renderer_t7_0_0_0;
static ParameterInfo IList_1_t7294_IList_1_Insert_m41510_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Renderer_t7_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41510_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Renderer>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41510_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7294_IList_1_Insert_m41510_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41510_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7294_IList_1_RemoveAt_m41511_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41511_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Renderer>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41511_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7294_IList_1_RemoveAt_m41511_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41511_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7294_IList_1_get_Item_m41507_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Renderer_t7_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41507_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Renderer>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41507_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7294_il2cpp_TypeInfo/* declaring_type */
	, &Renderer_t7_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7294_IList_1_get_Item_m41507_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41507_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Renderer_t7_0_0_0;
static ParameterInfo IList_1_t7294_IList_1_set_Item_m41508_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Renderer_t7_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41508_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Renderer>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41508_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7294_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7294_IList_1_set_Item_m41508_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41508_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7294_MethodInfos[] =
{
	&IList_1_IndexOf_m41509_MethodInfo,
	&IList_1_Insert_m41510_MethodInfo,
	&IList_1_RemoveAt_m41511_MethodInfo,
	&IList_1_get_Item_m41507_MethodInfo,
	&IList_1_set_Item_m41508_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7294_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7293_il2cpp_TypeInfo,
	&IEnumerable_1_t7295_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7294_0_0_0;
extern Il2CppType IList_1_t7294_1_0_0;
struct IList_1_t7294;
extern Il2CppGenericClass IList_1_t7294_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7294_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7294_MethodInfos/* methods */
	, IList_1_t7294_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7294_il2cpp_TypeInfo/* element_class */
	, IList_1_t7294_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7294_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7294_0_0_0/* byval_arg */
	, &IList_1_t7294_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7294_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5748_il2cpp_TypeInfo;

// WorldLoop
#include "AssemblyU2DCSharp_WorldLoop.h"


// T System.Collections.Generic.IEnumerator`1<WorldLoop>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<WorldLoop>
extern MethodInfo IEnumerator_1_get_Current_m41512_MethodInfo;
static PropertyInfo IEnumerator_1_t5748____Current_PropertyInfo = 
{
	&IEnumerator_1_t5748_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41512_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5748_PropertyInfos[] =
{
	&IEnumerator_1_t5748____Current_PropertyInfo,
	NULL
};
extern Il2CppType WorldLoop_t24_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41512_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<WorldLoop>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41512_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5748_il2cpp_TypeInfo/* declaring_type */
	, &WorldLoop_t24_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41512_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5748_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41512_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5748_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5748_0_0_0;
extern Il2CppType IEnumerator_1_t5748_1_0_0;
struct IEnumerator_1_t5748;
extern Il2CppGenericClass IEnumerator_1_t5748_GenericClass;
TypeInfo IEnumerator_1_t5748_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5748_MethodInfos/* methods */
	, IEnumerator_1_t5748_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5748_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5748_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5748_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5748_0_0_0/* byval_arg */
	, &IEnumerator_1_t5748_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5748_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<WorldLoop>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_30.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2755_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<WorldLoop>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_30MethodDeclarations.h"

extern TypeInfo WorldLoop_t24_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14216_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisWorldLoop_t24_m31787_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<WorldLoop>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<WorldLoop>(System.Int32)
#define Array_InternalArray__get_Item_TisWorldLoop_t24_m31787(__this, p0, method) (WorldLoop_t24 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<WorldLoop>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<WorldLoop>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<WorldLoop>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<WorldLoop>::MoveNext()
// T System.Array/InternalEnumerator`1<WorldLoop>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<WorldLoop>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2755____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2755, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2755____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2755, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2755_FieldInfos[] =
{
	&InternalEnumerator_1_t2755____array_0_FieldInfo,
	&InternalEnumerator_1_t2755____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14213_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2755____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2755_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14213_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2755____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2755_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14216_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2755_PropertyInfos[] =
{
	&InternalEnumerator_1_t2755____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2755____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2755_InternalEnumerator_1__ctor_m14212_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14212_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<WorldLoop>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14212_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2755_InternalEnumerator_1__ctor_m14212_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14212_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14213_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<WorldLoop>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14213_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14213_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14214_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<WorldLoop>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14214_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14214_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14215_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<WorldLoop>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14215_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14215_GenericMethod/* genericMethod */

};
extern Il2CppType WorldLoop_t24_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14216_GenericMethod;
// T System.Array/InternalEnumerator`1<WorldLoop>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14216_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* declaring_type */
	, &WorldLoop_t24_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14216_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2755_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14212_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14213_MethodInfo,
	&InternalEnumerator_1_Dispose_m14214_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14215_MethodInfo,
	&InternalEnumerator_1_get_Current_m14216_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14215_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14214_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2755_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14213_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14215_MethodInfo,
	&InternalEnumerator_1_Dispose_m14214_MethodInfo,
	&InternalEnumerator_1_get_Current_m14216_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2755_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5748_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2755_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5748_il2cpp_TypeInfo, 7},
};
extern TypeInfo WorldLoop_t24_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2755_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14216_MethodInfo/* Method Usage */,
	&WorldLoop_t24_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisWorldLoop_t24_m31787_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2755_0_0_0;
extern Il2CppType InternalEnumerator_1_t2755_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2755_GenericClass;
TypeInfo InternalEnumerator_1_t2755_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2755_MethodInfos/* methods */
	, InternalEnumerator_1_t2755_PropertyInfos/* properties */
	, InternalEnumerator_1_t2755_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2755_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2755_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2755_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2755_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2755_1_0_0/* this_arg */
	, InternalEnumerator_1_t2755_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2755_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2755_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2755)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7296_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<WorldLoop>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<WorldLoop>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<WorldLoop>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<WorldLoop>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<WorldLoop>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<WorldLoop>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<WorldLoop>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<WorldLoop>
extern MethodInfo ICollection_1_get_Count_m41513_MethodInfo;
static PropertyInfo ICollection_1_t7296____Count_PropertyInfo = 
{
	&ICollection_1_t7296_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41513_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41514_MethodInfo;
static PropertyInfo ICollection_1_t7296____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7296_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41514_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7296_PropertyInfos[] =
{
	&ICollection_1_t7296____Count_PropertyInfo,
	&ICollection_1_t7296____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41513_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<WorldLoop>::get_Count()
MethodInfo ICollection_1_get_Count_m41513_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7296_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41513_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41514_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<WorldLoop>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41514_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7296_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41514_GenericMethod/* genericMethod */

};
extern Il2CppType WorldLoop_t24_0_0_0;
extern Il2CppType WorldLoop_t24_0_0_0;
static ParameterInfo ICollection_1_t7296_ICollection_1_Add_m41515_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldLoop_t24_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41515_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<WorldLoop>::Add(T)
MethodInfo ICollection_1_Add_m41515_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7296_ICollection_1_Add_m41515_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41515_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41516_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<WorldLoop>::Clear()
MethodInfo ICollection_1_Clear_m41516_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41516_GenericMethod/* genericMethod */

};
extern Il2CppType WorldLoop_t24_0_0_0;
static ParameterInfo ICollection_1_t7296_ICollection_1_Contains_m41517_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldLoop_t24_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41517_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<WorldLoop>::Contains(T)
MethodInfo ICollection_1_Contains_m41517_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7296_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7296_ICollection_1_Contains_m41517_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41517_GenericMethod/* genericMethod */

};
extern Il2CppType WorldLoopU5BU5D_t5165_0_0_0;
extern Il2CppType WorldLoopU5BU5D_t5165_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7296_ICollection_1_CopyTo_m41518_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &WorldLoopU5BU5D_t5165_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41518_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<WorldLoop>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41518_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7296_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7296_ICollection_1_CopyTo_m41518_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41518_GenericMethod/* genericMethod */

};
extern Il2CppType WorldLoop_t24_0_0_0;
static ParameterInfo ICollection_1_t7296_ICollection_1_Remove_m41519_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldLoop_t24_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41519_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<WorldLoop>::Remove(T)
MethodInfo ICollection_1_Remove_m41519_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7296_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7296_ICollection_1_Remove_m41519_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41519_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7296_MethodInfos[] =
{
	&ICollection_1_get_Count_m41513_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41514_MethodInfo,
	&ICollection_1_Add_m41515_MethodInfo,
	&ICollection_1_Clear_m41516_MethodInfo,
	&ICollection_1_Contains_m41517_MethodInfo,
	&ICollection_1_CopyTo_m41518_MethodInfo,
	&ICollection_1_Remove_m41519_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7298_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7296_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7298_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7296_0_0_0;
extern Il2CppType ICollection_1_t7296_1_0_0;
struct ICollection_1_t7296;
extern Il2CppGenericClass ICollection_1_t7296_GenericClass;
TypeInfo ICollection_1_t7296_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7296_MethodInfos/* methods */
	, ICollection_1_t7296_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7296_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7296_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7296_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7296_0_0_0/* byval_arg */
	, &ICollection_1_t7296_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7296_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<WorldLoop>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<WorldLoop>
extern Il2CppType IEnumerator_1_t5748_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41520_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<WorldLoop>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41520_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7298_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5748_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41520_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7298_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41520_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7298_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7298_0_0_0;
extern Il2CppType IEnumerable_1_t7298_1_0_0;
struct IEnumerable_1_t7298;
extern Il2CppGenericClass IEnumerable_1_t7298_GenericClass;
TypeInfo IEnumerable_1_t7298_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7298_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7298_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7298_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7298_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7298_0_0_0/* byval_arg */
	, &IEnumerable_1_t7298_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7298_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7297_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<WorldLoop>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<WorldLoop>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<WorldLoop>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<WorldLoop>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<WorldLoop>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<WorldLoop>
extern MethodInfo IList_1_get_Item_m41521_MethodInfo;
extern MethodInfo IList_1_set_Item_m41522_MethodInfo;
static PropertyInfo IList_1_t7297____Item_PropertyInfo = 
{
	&IList_1_t7297_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41521_MethodInfo/* get */
	, &IList_1_set_Item_m41522_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7297_PropertyInfos[] =
{
	&IList_1_t7297____Item_PropertyInfo,
	NULL
};
extern Il2CppType WorldLoop_t24_0_0_0;
static ParameterInfo IList_1_t7297_IList_1_IndexOf_m41523_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &WorldLoop_t24_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41523_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<WorldLoop>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41523_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7297_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7297_IList_1_IndexOf_m41523_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41523_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType WorldLoop_t24_0_0_0;
static ParameterInfo IList_1_t7297_IList_1_Insert_m41524_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &WorldLoop_t24_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41524_GenericMethod;
// System.Void System.Collections.Generic.IList`1<WorldLoop>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41524_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7297_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7297_IList_1_Insert_m41524_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41524_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7297_IList_1_RemoveAt_m41525_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41525_GenericMethod;
// System.Void System.Collections.Generic.IList`1<WorldLoop>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41525_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7297_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7297_IList_1_RemoveAt_m41525_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41525_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7297_IList_1_get_Item_m41521_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType WorldLoop_t24_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41521_GenericMethod;
// T System.Collections.Generic.IList`1<WorldLoop>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41521_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7297_il2cpp_TypeInfo/* declaring_type */
	, &WorldLoop_t24_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7297_IList_1_get_Item_m41521_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41521_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType WorldLoop_t24_0_0_0;
static ParameterInfo IList_1_t7297_IList_1_set_Item_m41522_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &WorldLoop_t24_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41522_GenericMethod;
// System.Void System.Collections.Generic.IList`1<WorldLoop>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41522_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7297_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7297_IList_1_set_Item_m41522_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41522_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7297_MethodInfos[] =
{
	&IList_1_IndexOf_m41523_MethodInfo,
	&IList_1_Insert_m41524_MethodInfo,
	&IList_1_RemoveAt_m41525_MethodInfo,
	&IList_1_get_Item_m41521_MethodInfo,
	&IList_1_set_Item_m41522_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7297_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7296_il2cpp_TypeInfo,
	&IEnumerable_1_t7298_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7297_0_0_0;
extern Il2CppType IList_1_t7297_1_0_0;
struct IList_1_t7297;
extern Il2CppGenericClass IList_1_t7297_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7297_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7297_MethodInfos/* methods */
	, IList_1_t7297_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7297_il2cpp_TypeInfo/* element_class */
	, IList_1_t7297_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7297_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7297_0_0_0/* byval_arg */
	, &IList_1_t7297_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7297_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<WorldLoop>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_11.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2756_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<WorldLoop>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_11MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<WorldLoop>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_7.h"
extern TypeInfo InvokableCall_1_t2757_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<WorldLoop>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_7MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14219_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14221_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<WorldLoop>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<WorldLoop>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<WorldLoop>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2756____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2756_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2756, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2756_FieldInfos[] =
{
	&CachedInvokableCall_1_t2756____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType WorldLoop_t24_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2756_CachedInvokableCall_1__ctor_m14217_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &WorldLoop_t24_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14217_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<WorldLoop>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14217_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2756_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2756_CachedInvokableCall_1__ctor_m14217_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14217_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2756_CachedInvokableCall_1_Invoke_m14218_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14218_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<WorldLoop>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14218_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2756_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2756_CachedInvokableCall_1_Invoke_m14218_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14218_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2756_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14217_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14218_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14218_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14222_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2756_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14218_MethodInfo,
	&InvokableCall_1_Find_m14222_MethodInfo,
};
extern Il2CppType UnityAction_1_t2758_0_0_0;
extern TypeInfo UnityAction_1_t2758_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisWorldLoop_t24_m31797_MethodInfo;
extern TypeInfo WorldLoop_t24_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14224_MethodInfo;
extern TypeInfo WorldLoop_t24_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2756_RGCTXData[8] = 
{
	&UnityAction_1_t2758_0_0_0/* Type Usage */,
	&UnityAction_1_t2758_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWorldLoop_t24_m31797_MethodInfo/* Method Usage */,
	&WorldLoop_t24_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14224_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14219_MethodInfo/* Method Usage */,
	&WorldLoop_t24_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14221_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2756_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2756_1_0_0;
struct CachedInvokableCall_1_t2756;
extern Il2CppGenericClass CachedInvokableCall_1_t2756_GenericClass;
TypeInfo CachedInvokableCall_1_t2756_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2756_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2756_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2757_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2756_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2756_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2756_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2756_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2756_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2756_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2756_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2756)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<WorldLoop>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_14.h"
extern TypeInfo UnityAction_1_t2758_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<WorldLoop>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_14MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<WorldLoop>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<WorldLoop>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisWorldLoop_t24_m31797(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<WorldLoop>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<WorldLoop>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<WorldLoop>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<WorldLoop>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<WorldLoop>
extern Il2CppType UnityAction_1_t2758_0_0_1;
FieldInfo InvokableCall_1_t2757____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2758_0_0_1/* type */
	, &InvokableCall_1_t2757_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2757, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2757_FieldInfos[] =
{
	&InvokableCall_1_t2757____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2757_InvokableCall_1__ctor_m14219_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14219_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<WorldLoop>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14219_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2757_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2757_InvokableCall_1__ctor_m14219_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14219_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2758_0_0_0;
static ParameterInfo InvokableCall_1_t2757_InvokableCall_1__ctor_m14220_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2758_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14220_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<WorldLoop>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14220_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2757_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2757_InvokableCall_1__ctor_m14220_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14220_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2757_InvokableCall_1_Invoke_m14221_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14221_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<WorldLoop>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14221_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2757_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2757_InvokableCall_1_Invoke_m14221_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14221_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2757_InvokableCall_1_Find_m14222_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14222_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<WorldLoop>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14222_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2757_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2757_InvokableCall_1_Find_m14222_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14222_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2757_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14219_MethodInfo,
	&InvokableCall_1__ctor_m14220_MethodInfo,
	&InvokableCall_1_Invoke_m14221_MethodInfo,
	&InvokableCall_1_Find_m14222_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2757_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14221_MethodInfo,
	&InvokableCall_1_Find_m14222_MethodInfo,
};
extern TypeInfo UnityAction_1_t2758_il2cpp_TypeInfo;
extern TypeInfo WorldLoop_t24_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2757_RGCTXData[5] = 
{
	&UnityAction_1_t2758_0_0_0/* Type Usage */,
	&UnityAction_1_t2758_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisWorldLoop_t24_m31797_MethodInfo/* Method Usage */,
	&WorldLoop_t24_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14224_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2757_0_0_0;
extern Il2CppType InvokableCall_1_t2757_1_0_0;
struct InvokableCall_1_t2757;
extern Il2CppGenericClass InvokableCall_1_t2757_GenericClass;
TypeInfo InvokableCall_1_t2757_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2757_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2757_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2757_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2757_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2757_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2757_0_0_0/* byval_arg */
	, &InvokableCall_1_t2757_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2757_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2757_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2757)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<WorldLoop>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<WorldLoop>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<WorldLoop>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<WorldLoop>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<WorldLoop>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2758_UnityAction_1__ctor_m14223_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14223_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<WorldLoop>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14223_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2758_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2758_UnityAction_1__ctor_m14223_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14223_GenericMethod/* genericMethod */

};
extern Il2CppType WorldLoop_t24_0_0_0;
static ParameterInfo UnityAction_1_t2758_UnityAction_1_Invoke_m14224_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WorldLoop_t24_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14224_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<WorldLoop>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14224_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2758_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2758_UnityAction_1_Invoke_m14224_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14224_GenericMethod/* genericMethod */

};
extern Il2CppType WorldLoop_t24_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2758_UnityAction_1_BeginInvoke_m14225_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &WorldLoop_t24_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14225_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<WorldLoop>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14225_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2758_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2758_UnityAction_1_BeginInvoke_m14225_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14225_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2758_UnityAction_1_EndInvoke_m14226_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14226_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<WorldLoop>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14226_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2758_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2758_UnityAction_1_EndInvoke_m14226_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14226_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2758_MethodInfos[] =
{
	&UnityAction_1__ctor_m14223_MethodInfo,
	&UnityAction_1_Invoke_m14224_MethodInfo,
	&UnityAction_1_BeginInvoke_m14225_MethodInfo,
	&UnityAction_1_EndInvoke_m14226_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14225_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14226_MethodInfo;
static MethodInfo* UnityAction_1_t2758_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14224_MethodInfo,
	&UnityAction_1_BeginInvoke_m14225_MethodInfo,
	&UnityAction_1_EndInvoke_m14226_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2758_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2758_1_0_0;
struct UnityAction_1_t2758;
extern Il2CppGenericClass UnityAction_1_t2758_GenericClass;
TypeInfo UnityAction_1_t2758_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2758_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2758_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2758_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2758_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2758_0_0_0/* byval_arg */
	, &UnityAction_1_t2758_1_0_0/* this_arg */
	, UnityAction_1_t2758_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2758_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2758)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<Raycaster>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_2.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2759_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<Raycaster>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_2MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<Raycaster>
extern Il2CppType Raycaster_t20_0_0_6;
FieldInfo CastHelper_1_t2759____t_0_FieldInfo = 
{
	"t"/* name */
	, &Raycaster_t20_0_0_6/* type */
	, &CastHelper_1_t2759_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2759, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2759____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2759_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2759, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2759_FieldInfos[] =
{
	&CastHelper_1_t2759____t_0_FieldInfo,
	&CastHelper_1_t2759____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2759_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2759_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2759_0_0_0;
extern Il2CppType CastHelper_1_t2759_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2759_GenericClass;
TypeInfo CastHelper_1_t2759_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2759_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2759_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2759_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2759_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2759_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2759_0_0_0/* byval_arg */
	, &CastHelper_1_t2759_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2759_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2759)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5750_il2cpp_TypeInfo;

// WorldNav
#include "AssemblyU2DCSharp_WorldNav.h"


// T System.Collections.Generic.IEnumerator`1<WorldNav>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<WorldNav>
extern MethodInfo IEnumerator_1_get_Current_m41526_MethodInfo;
static PropertyInfo IEnumerator_1_t5750____Current_PropertyInfo = 
{
	&IEnumerator_1_t5750_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41526_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5750_PropertyInfos[] =
{
	&IEnumerator_1_t5750____Current_PropertyInfo,
	NULL
};
extern Il2CppType WorldNav_t25_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41526_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<WorldNav>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41526_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5750_il2cpp_TypeInfo/* declaring_type */
	, &WorldNav_t25_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41526_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5750_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41526_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5750_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5750_0_0_0;
extern Il2CppType IEnumerator_1_t5750_1_0_0;
struct IEnumerator_1_t5750;
extern Il2CppGenericClass IEnumerator_1_t5750_GenericClass;
TypeInfo IEnumerator_1_t5750_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5750_MethodInfos/* methods */
	, IEnumerator_1_t5750_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5750_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5750_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5750_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5750_0_0_0/* byval_arg */
	, &IEnumerator_1_t5750_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5750_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
