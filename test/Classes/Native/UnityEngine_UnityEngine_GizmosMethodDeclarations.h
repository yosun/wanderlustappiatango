﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Gizmos
struct Gizmos_t977;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
 void Gizmos_DrawLine_m660 (Object_t * __this/* static, unused */, Vector3_t13  ___from, Vector3_t13  ___to, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawLine(UnityEngine.Vector3&,UnityEngine.Vector3&)
 void Gizmos_INTERNAL_CALL_DrawLine_m5783 (Object_t * __this/* static, unused */, Vector3_t13 * ___from, Vector3_t13 * ___to, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawWireCube(UnityEngine.Vector3,UnityEngine.Vector3)
 void Gizmos_DrawWireCube_m4384 (Object_t * __this/* static, unused */, Vector3_t13  ___center, Vector3_t13  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawWireCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
 void Gizmos_INTERNAL_CALL_DrawWireCube_m5784 (Object_t * __this/* static, unused */, Vector3_t13 * ___center, Vector3_t13 * ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::DrawCube(UnityEngine.Vector3,UnityEngine.Vector3)
 void Gizmos_DrawCube_m4397 (Object_t * __this/* static, unused */, Vector3_t13  ___center, Vector3_t13  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_CALL_DrawCube(UnityEngine.Vector3&,UnityEngine.Vector3&)
 void Gizmos_INTERNAL_CALL_DrawCube_m5785 (Object_t * __this/* static, unused */, Vector3_t13 * ___center, Vector3_t13 * ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_set_color(UnityEngine.Color&)
 void Gizmos_INTERNAL_set_color_m5786 (Object_t * __this/* static, unused */, Color_t19 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
 void Gizmos_set_color_m659 (Object_t * __this/* static, unused */, Color_t19  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::INTERNAL_set_matrix(UnityEngine.Matrix4x4&)
 void Gizmos_INTERNAL_set_matrix_m5787 (Object_t * __this/* static, unused */, Matrix4x4_t176 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gizmos::set_matrix(UnityEngine.Matrix4x4)
 void Gizmos_set_matrix_m658 (Object_t * __this/* static, unused */, Matrix4x4_t176  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
