﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.DataSetTrackableBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_90.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetTrackableBehaviour>
struct CachedInvokableCall_1_t3672  : public InvokableCall_1_t3673
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetTrackableBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
