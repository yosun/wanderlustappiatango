﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.UserDefinedTargetBuildingBehaviour
struct UserDefinedTargetBuildingBehaviour_t81;

// System.Void Vuforia.UserDefinedTargetBuildingBehaviour::.ctor()
 void UserDefinedTargetBuildingBehaviour__ctor_m176 (UserDefinedTargetBuildingBehaviour_t81 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
