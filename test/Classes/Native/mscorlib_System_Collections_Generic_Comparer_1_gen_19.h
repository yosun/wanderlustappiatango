﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.ITrackableEventHandler>
struct Comparer_1_t3669;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.ITrackableEventHandler>
struct Comparer_1_t3669  : public Object_t
{
};
struct Comparer_1_t3669_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ITrackableEventHandler>::_default
	Comparer_1_t3669 * ____default_0;
};
