﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ScriptableObject
struct ScriptableObject_t919;
struct ScriptableObject_t919_marshaled;
// System.String
struct String_t;
// System.Type
struct Type_t;

// System.Void UnityEngine.ScriptableObject::.ctor()
 void ScriptableObject__ctor_m5439 (ScriptableObject_t919 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
 void ScriptableObject_Internal_CreateScriptableObject_m5440 (Object_t * __this/* static, unused */, ScriptableObject_t919 * ___self, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
 ScriptableObject_t919 * ScriptableObject_CreateInstance_m5441 (Object_t * __this/* static, unused */, String_t* ___className, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
 ScriptableObject_t919 * ScriptableObject_CreateInstance_m5442 (Object_t * __this/* static, unused */, Type_t * ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
 ScriptableObject_t919 * ScriptableObject_CreateInstanceFromType_m5443 (Object_t * __this/* static, unused */, Type_t * ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
void ScriptableObject_t919_marshal(const ScriptableObject_t919& unmarshaled, ScriptableObject_t919_marshaled& marshaled);
void ScriptableObject_t919_marshal_back(const ScriptableObject_t919_marshaled& marshaled, ScriptableObject_t919& unmarshaled);
void ScriptableObject_t919_marshal_cleanup(ScriptableObject_t919_marshaled& marshaled);
