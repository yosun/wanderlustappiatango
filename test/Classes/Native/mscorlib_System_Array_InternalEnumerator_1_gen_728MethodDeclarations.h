﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Threading.EventResetMode>
struct InternalEnumerator_1_t5126;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Threading.EventResetMode
#include "mscorlib_System_Threading_EventResetMode.h"

// System.Void System.Array/InternalEnumerator`1<System.Threading.EventResetMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31288 (InternalEnumerator_1_t5126 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Threading.EventResetMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31289 (InternalEnumerator_1_t5126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Threading.EventResetMode>::Dispose()
 void InternalEnumerator_1_Dispose_m31290 (InternalEnumerator_1_t5126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Threading.EventResetMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31291 (InternalEnumerator_1_t5126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Threading.EventResetMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31292 (InternalEnumerator_1_t5126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
