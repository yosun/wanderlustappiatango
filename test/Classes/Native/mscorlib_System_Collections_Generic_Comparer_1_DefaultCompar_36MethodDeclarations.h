﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.ILoadLevelEventHandler>
struct DefaultComparer_t4049;
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t711;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.ILoadLevelEventHandler>::.ctor()
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_0MethodDeclarations.h"
#define DefaultComparer__ctor_m23879(__this, method) (void)DefaultComparer__ctor_m14758_gshared((DefaultComparer_t2862 *)__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.ILoadLevelEventHandler>::Compare(T,T)
#define DefaultComparer_Compare_m23880(__this, ___x, ___y, method) (int32_t)DefaultComparer_Compare_m14759_gshared((DefaultComparer_t2862 *)__this, (Object_t *)___x, (Object_t *)___y, method)
