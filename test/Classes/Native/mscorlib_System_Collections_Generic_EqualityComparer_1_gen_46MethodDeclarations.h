﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>
struct EqualityComparer_1_t4106;
// System.Object
struct Object_t;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t77;

// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>::.ctor()
// System.Collections.Generic.EqualityComparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_genMethodDeclarations.h"
#define EqualityComparer_1__ctor_m24425(__this, method) (void)EqualityComparer_1__ctor_m14712_gshared((EqualityComparer_1_t2853 *)__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>::.cctor()
#define EqualityComparer_1__cctor_m24426(__this/* static, unused */, method) (void)EqualityComparer_1__cctor_m14713_gshared((Object_t *)__this/* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m24427(__this, ___obj, method) (int32_t)EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m14714_gshared((EqualityComparer_1_t2853 *)__this, (Object_t *)___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m24428(__this, ___x, ___y, method) (bool)EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m14715_gshared((EqualityComparer_1_t2853 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.SurfaceAbstractBehaviour>::get_Default()
#define EqualityComparer_1_get_Default_m24429(__this/* static, unused */, method) (EqualityComparer_1_t4106 *)EqualityComparer_1_get_Default_m14716_gshared((Object_t *)__this/* static, unused */, method)
