﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyDelaySignAttribute
struct AssemblyDelaySignAttribute_t1284;

// System.Void System.Reflection.AssemblyDelaySignAttribute::.ctor(System.Boolean)
 void AssemblyDelaySignAttribute__ctor_m6715 (AssemblyDelaySignAttribute_t1284 * __this, bool ___delaySign, MethodInfo* method) IL2CPP_METHOD_ATTR;
