﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>
struct InternalEnumerator_1_t2926;
// System.Object
struct Object_t;
// Vuforia.SmartTerrainTrackerBehaviour
struct SmartTerrainTrackerBehaviour_t75;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15096(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15097(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m15098(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15099(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.SmartTerrainTrackerBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m15100(__this, method) (SmartTerrainTrackerBehaviour_t75 *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
