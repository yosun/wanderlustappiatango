﻿#pragma once
#include <stdint.h>
// UnityEngine.GUIStyle
struct GUIStyle_t953;
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.RectOffset
struct RectOffset_t391  : public Object_t
{
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	IntPtr_t121 ___m_Ptr_0;
	// UnityEngine.GUIStyle UnityEngine.RectOffset::m_SourceStyle
	GUIStyle_t953 * ___m_SourceStyle_1;
};
