﻿#pragma once
#include <stdint.h>
// Vuforia.Word
struct Word_t691;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Word>
struct Comparison_1_t3988  : public MulticastDelegate_t325
{
};
