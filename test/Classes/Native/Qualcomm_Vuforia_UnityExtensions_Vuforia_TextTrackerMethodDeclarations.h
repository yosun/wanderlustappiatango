﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TextTracker
struct TextTracker_t677;
// Vuforia.WordList
struct WordList_t678;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// Vuforia.WordList Vuforia.TextTracker::get_WordList()
// System.Boolean Vuforia.TextTracker::SetRegionOfInterest(UnityEngine.Rect,UnityEngine.Rect)
// System.Boolean Vuforia.TextTracker::GetRegionOfInterest(UnityEngine.Rect&,UnityEngine.Rect&)
// System.Void Vuforia.TextTracker::.ctor()
 void TextTracker__ctor_m3070 (TextTracker_t677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
