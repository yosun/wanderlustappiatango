﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct KeyCollection_t4094;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>
struct Dictionary_2_t715;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3161;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Int32[]
struct Int32U5BU5D_t21;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.SurfaceAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_36.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void KeyCollection__ctor_m24356 (KeyCollection_t4094 * __this, Dictionary_2_t715 * ___dictionary, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
 void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m24357 (KeyCollection_t4094 * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Clear()
 void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m24358 (KeyCollection_t4094 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
 bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m24359 (KeyCollection_t4094 * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
 bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m24360 (KeyCollection_t4094 * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
 Object_t* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m24361 (KeyCollection_t4094 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void KeyCollection_System_Collections_ICollection_CopyTo_m24362 (KeyCollection_t4094 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m24363 (KeyCollection_t4094 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
 bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m24364 (KeyCollection_t4094 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
 bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m24365 (KeyCollection_t4094 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
 Object_t * KeyCollection_System_Collections_ICollection_get_SyncRoot_m24366 (KeyCollection_t4094 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::CopyTo(TKey[],System.Int32)
 void KeyCollection_CopyTo_m24367 (KeyCollection_t4094 * __this, Int32U5BU5D_t21* ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::GetEnumerator()
 Enumerator_t4100  KeyCollection_GetEnumerator_m24368 (KeyCollection_t4094 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.SurfaceAbstractBehaviour>::get_Count()
 int32_t KeyCollection_get_Count_m24369 (KeyCollection_t4094 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
