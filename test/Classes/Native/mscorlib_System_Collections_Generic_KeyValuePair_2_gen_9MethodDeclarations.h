﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct KeyValuePair_2_t3771;
// Vuforia.Image
struct Image_t560;
// System.String
struct String_t;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m21106 (KeyValuePair_2_t3771 * __this, int32_t ___key, Image_t560 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Key()
 int32_t KeyValuePair_2_get_Key_m21107 (KeyValuePair_2_t3771 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m21108 (KeyValuePair_2_t3771 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Value()
 Image_t560 * KeyValuePair_2_get_Value_m21109 (KeyValuePair_2_t3771 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m21110 (KeyValuePair_2_t3771 * __this, Image_t560 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::ToString()
 String_t* KeyValuePair_2_ToString_m21111 (KeyValuePair_2_t3771 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
