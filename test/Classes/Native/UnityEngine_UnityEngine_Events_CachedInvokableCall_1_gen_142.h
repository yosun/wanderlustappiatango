﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.MeshCollider>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_144.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MeshCollider>
struct CachedInvokableCall_1_t4637  : public InvokableCall_1_t4638
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MeshCollider>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
