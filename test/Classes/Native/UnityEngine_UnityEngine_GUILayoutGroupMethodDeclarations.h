﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUILayoutGroup
struct GUILayoutGroup_t956;
// UnityEngine.RectOffset
struct RectOffset_t391;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t960;
// UnityEngine.GUIStyle
struct GUIStyle_t953;
// System.String
struct String_t;

// System.Void UnityEngine.GUILayoutGroup::.ctor()
 void GUILayoutGroup__ctor_m5620 (GUILayoutGroup_t956 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectOffset UnityEngine.GUILayoutGroup::get_margin()
 RectOffset_t391 * GUILayoutGroup_get_margin_m5621 (GUILayoutGroup_t956 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::ApplyOptions(UnityEngine.GUILayoutOption[])
 void GUILayoutGroup_ApplyOptions_m5622 (GUILayoutGroup_t956 * __this, GUILayoutOptionU5BU5D_t960* ___options, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::ApplyStyleSettings(UnityEngine.GUIStyle)
 void GUILayoutGroup_ApplyStyleSettings_m5623 (GUILayoutGroup_t956 * __this, GUIStyle_t953 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::ResetCursor()
 void GUILayoutGroup_ResetCursor_m5624 (GUILayoutGroup_t956 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::CalcWidth()
 void GUILayoutGroup_CalcWidth_m5625 (GUILayoutGroup_t956 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::SetHorizontal(System.Single,System.Single)
 void GUILayoutGroup_SetHorizontal_m5626 (GUILayoutGroup_t956 * __this, float ___x, float ___width, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::CalcHeight()
 void GUILayoutGroup_CalcHeight_m5627 (GUILayoutGroup_t956 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUILayoutGroup::SetVertical(System.Single,System.Single)
 void GUILayoutGroup_SetVertical_m5628 (GUILayoutGroup_t956 * __this, float ___y, float ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUILayoutGroup::ToString()
 String_t* GUILayoutGroup_ToString_m5629 (GUILayoutGroup_t956 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
