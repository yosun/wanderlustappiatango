﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.Image>
struct EqualityComparer_1_t3786;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.Image>
struct EqualityComparer_1_t3786  : public Object_t
{
};
struct EqualityComparer_1_t3786_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.Image>::_default
	EqualityComparer_1_t3786 * ____default_0;
};
