﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ThreadStaticAttribute
struct ThreadStaticAttribute_t2248;

// System.Void System.ThreadStaticAttribute::.ctor()
 void ThreadStaticAttribute__ctor_m13193 (ThreadStaticAttribute_t2248 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
