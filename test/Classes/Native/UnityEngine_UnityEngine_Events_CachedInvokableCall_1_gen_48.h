﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.EventSystem>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_44.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.EventSystem>
struct CachedInvokableCall_1_t3000  : public InvokableCall_1_t3001
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.EventSystem>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
