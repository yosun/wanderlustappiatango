﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.CanvasGroup>
struct UnityAction_1_t4719;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.CanvasGroup>
struct InvokableCall_1_t4718  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.CanvasGroup>::Delegate
	UnityAction_1_t4719 * ___Delegate_0;
};
