﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackerManager
struct TrackerManager_t743;
// Vuforia.StateManager
struct StateManager_t724;

// Vuforia.TrackerManager Vuforia.TrackerManager::get_Instance()
 TrackerManager_t743 * TrackerManager_get_Instance_m4040 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.StateManager Vuforia.TrackerManager::GetStateManager()
// System.Void Vuforia.TrackerManager::.ctor()
 void TrackerManager__ctor_m4041 (TrackerManager_t743 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackerManager::.cctor()
 void TrackerManager__cctor_m4042 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
