﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>
struct KeyValuePair_2_t846;
// Vuforia.Prop
struct Prop_t42;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m24468 (KeyValuePair_2_t846 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::get_Key()
 int32_t KeyValuePair_2_get_Key_m24469 (KeyValuePair_2_t846 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m24470 (KeyValuePair_2_t846 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::get_Value()
 Object_t * KeyValuePair_2_get_Value_m5166 (KeyValuePair_2_t846 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m24471 (KeyValuePair_2_t846 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>::ToString()
 String_t* KeyValuePair_2_ToString_m24472 (KeyValuePair_2_t846 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
