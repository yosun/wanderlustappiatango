﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.ClientActivatedIdentity
struct ClientActivatedIdentity_t2049;
// System.MarshalByRefObject
struct MarshalByRefObject_t1348;

// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::GetServerObject()
 MarshalByRefObject_t1348 * ClientActivatedIdentity_GetServerObject_m11701 (ClientActivatedIdentity_t2049 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
