﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Quaternion
struct Quaternion_t12;
// System.String
struct String_t;
// System.Object
struct Object_t;
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
 void Quaternion__ctor_m242 (Quaternion_t12 * __this, float ___x, float ___y, float ___z, float ___w, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
 Quaternion_t12  Quaternion_get_identity_m4294 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
 float Quaternion_Dot_m5804 (Object_t * __this/* static, unused */, Quaternion_t12  ___a, Quaternion_t12  ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
 Quaternion_t12  Quaternion_AngleAxis_m4282 (Object_t * __this/* static, unused */, float ___angle, Vector3_t13  ___axis, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&)
 Quaternion_t12  Quaternion_INTERNAL_CALL_AngleAxis_m5805 (Object_t * __this/* static, unused */, float ___angle, Vector3_t13 * ___axis, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::ToAngleAxis(System.Single&,UnityEngine.Vector3&)
 void Quaternion_ToAngleAxis_m4493 (Quaternion_t12 * __this, float* ___angle, Vector3_t13 * ___axis, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
 Quaternion_t12  Quaternion_Slerp_m257 (Object_t * __this/* static, unused */, Quaternion_t12  ___from, Quaternion_t12  ___to, float ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single)
 Quaternion_t12  Quaternion_INTERNAL_CALL_Slerp_m5806 (Object_t * __this/* static, unused */, Quaternion_t12 * ___from, Quaternion_t12 * ___to, float ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
 Quaternion_t12  Quaternion_Inverse_m2417 (Object_t * __this/* static, unused */, Quaternion_t12  ___rotation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&)
 Quaternion_t12  Quaternion_INTERNAL_CALL_Inverse_m5807 (Object_t * __this/* static, unused */, Quaternion_t12 * ___rotation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Quaternion::ToString()
 String_t* Quaternion_ToString_m5808 (Quaternion_t12 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Quaternion::ToString(System.String)
 String_t* Quaternion_ToString_m302 (Quaternion_t12 * __this, String_t* ___format, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
 Vector3_t13  Quaternion_get_eulerAngles_m251 (Quaternion_t12 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
 Quaternion_t12  Quaternion_Euler_m287 (Object_t * __this/* static, unused */, float ___x, float ___y, float ___z, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
 Quaternion_t12  Quaternion_Euler_m252 (Object_t * __this/* static, unused */, Vector3_t13  ___euler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
 Vector3_t13  Quaternion_Internal_ToEulerRad_m5809 (Object_t * __this/* static, unused */, Quaternion_t12  ___rotation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&)
 Vector3_t13  Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m5810 (Object_t * __this/* static, unused */, Quaternion_t12 * ___rotation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
 Quaternion_t12  Quaternion_Internal_FromEulerRad_m5811 (Object_t * __this/* static, unused */, Vector3_t13  ___euler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&)
 Quaternion_t12  Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m5812 (Object_t * __this/* static, unused */, Vector3_t13 * ___euler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::Internal_ToAxisAngleRad(UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&)
 void Quaternion_Internal_ToAxisAngleRad_m5813 (Object_t * __this/* static, unused */, Quaternion_t12  ___q, Vector3_t13 * ___axis, float* ___angle, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)
 void Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m5814 (Object_t * __this/* static, unused */, Quaternion_t12 * ___q, Vector3_t13 * ___axis, float* ___angle, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Quaternion::GetHashCode()
 int32_t Quaternion_GetHashCode_m5815 (Quaternion_t12 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
 bool Quaternion_Equals_m5816 (Quaternion_t12 * __this, Object_t * ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
 Quaternion_t12  Quaternion_op_Multiply_m4295 (Object_t * __this/* static, unused */, Quaternion_t12  ___lhs, Quaternion_t12  ___rhs, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
 Vector3_t13  Quaternion_op_Multiply_m329 (Object_t * __this/* static, unused */, Quaternion_t12  ___rotation, Vector3_t13  ___point, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
 bool Quaternion_op_Inequality_m2326 (Object_t * __this/* static, unused */, Quaternion_t12  ___lhs, Quaternion_t12  ___rhs, MethodInfo* method) IL2CPP_METHOD_ATTR;
