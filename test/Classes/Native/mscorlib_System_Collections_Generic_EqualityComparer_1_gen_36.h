﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.SmartTerrainTrackable>
struct EqualityComparer_1_t3923;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.SmartTerrainTrackable>
struct EqualityComparer_1_t3923  : public Object_t
{
};
struct EqualityComparer_1_t3923_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.SmartTerrainTrackable>::_default
	EqualityComparer_1_t3923 * ____default_0;
};
