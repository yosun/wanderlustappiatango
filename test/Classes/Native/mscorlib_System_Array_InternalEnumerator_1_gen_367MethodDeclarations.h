﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Space>
struct InternalEnumerator_1_t4434;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Space
#include "UnityEngine_UnityEngine_Space.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Space>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m27172 (InternalEnumerator_1_t4434 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Space>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27173 (InternalEnumerator_1_t4434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Space>::Dispose()
 void InternalEnumerator_1_Dispose_m27174 (InternalEnumerator_1_t4434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Space>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m27175 (InternalEnumerator_1_t4434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.Space>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m27176 (InternalEnumerator_1_t4434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
