﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>
struct UnityAction_1_t4832;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>
struct InvokableCall_1_t4831  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::Delegate
	UnityAction_1_t4832 * ___Delegate_0;
};
