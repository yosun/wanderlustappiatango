﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordBehaviour
struct WordBehaviour_t92;

// System.Void Vuforia.WordBehaviour::.ctor()
 void WordBehaviour__ctor_m190 (WordBehaviour_t92 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
