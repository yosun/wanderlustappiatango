﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ComponentModel.TypeConverterAttribute
struct TypeConverterAttribute_t1314;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Object
struct Object_t;

// System.Void System.ComponentModel.TypeConverterAttribute::.ctor()
 void TypeConverterAttribute__ctor_m6800 (TypeConverterAttribute_t1314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.TypeConverterAttribute::.ctor(System.Type)
 void TypeConverterAttribute__ctor_m6801 (TypeConverterAttribute_t1314 * __this, Type_t * ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ComponentModel.TypeConverterAttribute::.cctor()
 void TypeConverterAttribute__cctor_m6802 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.TypeConverterAttribute::Equals(System.Object)
 bool TypeConverterAttribute_Equals_m6803 (TypeConverterAttribute_t1314 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.TypeConverterAttribute::GetHashCode()
 int32_t TypeConverterAttribute_GetHashCode_m6804 (TypeConverterAttribute_t1314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.ComponentModel.TypeConverterAttribute::get_ConverterTypeName()
 String_t* TypeConverterAttribute_get_ConverterTypeName_m6805 (TypeConverterAttribute_t1314 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
