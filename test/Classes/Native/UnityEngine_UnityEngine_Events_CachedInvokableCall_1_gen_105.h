﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.QCARAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_107.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARAbstractBehaviour>
struct CachedInvokableCall_1_t4310  : public InvokableCall_1_t4311
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.QCARAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
