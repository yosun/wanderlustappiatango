﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseInputModule>
struct Comparer_1_t3016;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseInputModule>
struct Comparer_1_t3016  : public Object_t
{
};
struct Comparer_1_t3016_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseInputModule>::_default
	Comparer_1_t3016 * ____default_0;
};
