﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// UnityEngine.UserAuthorizationDialog
struct UserAuthorizationDialog_t1093;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>
struct UnityAction_1_t4832  : public MulticastDelegate_t325
{
};
