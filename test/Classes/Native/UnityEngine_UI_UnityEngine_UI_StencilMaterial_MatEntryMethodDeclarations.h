﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.StencilMaterial/MatEntry
struct MatEntry_t362;

// System.Void UnityEngine.UI.StencilMaterial/MatEntry::.ctor()
 void MatEntry__ctor_m1548 (MatEntry_t362 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
