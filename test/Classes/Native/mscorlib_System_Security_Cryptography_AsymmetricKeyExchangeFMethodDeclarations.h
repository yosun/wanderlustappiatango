﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.AsymmetricKeyExchangeFormatter
struct AsymmetricKeyExchangeFormatter_t2089;
// System.Byte[]
struct ByteU5BU5D_t609;

// System.Void System.Security.Cryptography.AsymmetricKeyExchangeFormatter::.ctor()
 void AsymmetricKeyExchangeFormatter__ctor_m11846 (AsymmetricKeyExchangeFormatter_t2089 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.AsymmetricKeyExchangeFormatter::CreateKeyExchange(System.Byte[])
