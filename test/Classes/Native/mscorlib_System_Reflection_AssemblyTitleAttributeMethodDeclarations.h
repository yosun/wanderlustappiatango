﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyTitleAttribute
struct AssemblyTitleAttribute_t529;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyTitleAttribute::.ctor(System.String)
 void AssemblyTitleAttribute__ctor_m2580 (AssemblyTitleAttribute_t529 * __this, String_t* ___title, MethodInfo* method) IL2CPP_METHOD_ATTR;
