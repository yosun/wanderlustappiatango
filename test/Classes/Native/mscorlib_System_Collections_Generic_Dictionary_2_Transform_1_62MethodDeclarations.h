﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Surface,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>>
struct Transform_1_t4088;
// System.Object
struct Object_t;
// Vuforia.Surface
struct Surface_t43;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Surface,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m24289 (Transform_1_t4088 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Surface,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>>::Invoke(TKey,TValue)
 KeyValuePair_2_t848  Transform_1_Invoke_m24290 (Transform_1_t4088 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Surface,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m24291 (Transform_1_t4088 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Surface,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>>::EndInvoke(System.IAsyncResult)
 KeyValuePair_2_t848  Transform_1_EndInvoke_m24292 (Transform_1_t4088 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
