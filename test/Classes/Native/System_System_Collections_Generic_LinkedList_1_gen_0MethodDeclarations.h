﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t4843;
// System.Object
struct Object_t;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t4842;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1066;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t455;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0.h"

// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
 void LinkedList_1__ctor_m29727_gshared (LinkedList_1_t4843 * __this, MethodInfo* method);
#define LinkedList_1__ctor_m29727(__this, method) (void)LinkedList_1__ctor_m29727_gshared((LinkedList_1_t4843 *)__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void LinkedList_1__ctor_m29728_gshared (LinkedList_1_t4843 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___context, MethodInfo* method);
#define LinkedList_1__ctor_m29728(__this, ___info, ___context, method) (void)LinkedList_1__ctor_m29728_gshared((LinkedList_1_t4843 *)__this, (SerializationInfo_t1066 *)___info, (StreamingContext_t1067 )___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
 void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29729_gshared (LinkedList_1_t4843 * __this, Object_t * ___value, MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29729(__this, ___value, method) (void)LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29729_gshared((LinkedList_1_t4843 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void LinkedList_1_System_Collections_ICollection_CopyTo_m29730_gshared (LinkedList_1_t4843 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_CopyTo_m29730(__this, ___array, ___index, method) (void)LinkedList_1_System_Collections_ICollection_CopyTo_m29730_gshared((LinkedList_1_t4843 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
 Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29731_gshared (LinkedList_1_t4843 * __this, MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29731(__this, method) (Object_t*)LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29731_gshared((LinkedList_1_t4843 *)__this, method)
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29732_gshared (LinkedList_1_t4843 * __this, MethodInfo* method);
#define LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29732(__this, method) (Object_t *)LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29732_gshared((LinkedList_1_t4843 *)__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
 bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29733_gshared (LinkedList_1_t4843 * __this, MethodInfo* method);
#define LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29733(__this, method) (bool)LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29733_gshared((LinkedList_1_t4843 *)__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
 bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29734_gshared (LinkedList_1_t4843 * __this, MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29734(__this, method) (bool)LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29734_gshared((LinkedList_1_t4843 *)__this, method)
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
 Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29735_gshared (LinkedList_1_t4843 * __this, MethodInfo* method);
#define LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29735(__this, method) (Object_t *)LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29735_gshared((LinkedList_1_t4843 *)__this, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
 void LinkedList_1_VerifyReferencedNode_m29736_gshared (LinkedList_1_t4843 * __this, LinkedListNode_1_t4842 * ___node, MethodInfo* method);
#define LinkedList_1_VerifyReferencedNode_m29736(__this, ___node, method) (void)LinkedList_1_VerifyReferencedNode_m29736_gshared((LinkedList_1_t4843 *)__this, (LinkedListNode_1_t4842 *)___node, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
 LinkedListNode_1_t4842 * LinkedList_1_AddLast_m29737_gshared (LinkedList_1_t4843 * __this, Object_t * ___value, MethodInfo* method);
#define LinkedList_1_AddLast_m29737(__this, ___value, method) (LinkedListNode_1_t4842 *)LinkedList_1_AddLast_m29737_gshared((LinkedList_1_t4843 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
 void LinkedList_1_Clear_m29738_gshared (LinkedList_1_t4843 * __this, MethodInfo* method);
#define LinkedList_1_Clear_m29738(__this, method) (void)LinkedList_1_Clear_m29738_gshared((LinkedList_1_t4843 *)__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
 bool LinkedList_1_Contains_m29739_gshared (LinkedList_1_t4843 * __this, Object_t * ___value, MethodInfo* method);
#define LinkedList_1_Contains_m29739(__this, ___value, method) (bool)LinkedList_1_Contains_m29739_gshared((LinkedList_1_t4843 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
 void LinkedList_1_CopyTo_m29740_gshared (LinkedList_1_t4843 * __this, ObjectU5BU5D_t115* ___array, int32_t ___index, MethodInfo* method);
#define LinkedList_1_CopyTo_m29740(__this, ___array, ___index, method) (void)LinkedList_1_CopyTo_m29740_gshared((LinkedList_1_t4843 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
 LinkedListNode_1_t4842 * LinkedList_1_Find_m29741_gshared (LinkedList_1_t4843 * __this, Object_t * ___value, MethodInfo* method);
#define LinkedList_1_Find_m29741(__this, ___value, method) (LinkedListNode_1_t4842 *)LinkedList_1_Find_m29741_gshared((LinkedList_1_t4843 *)__this, (Object_t *)___value, method)
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
 Enumerator_t4844  LinkedList_1_GetEnumerator_m29742 (LinkedList_1_t4843 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void LinkedList_1_GetObjectData_m29743_gshared (LinkedList_1_t4843 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___context, MethodInfo* method);
#define LinkedList_1_GetObjectData_m29743(__this, ___info, ___context, method) (void)LinkedList_1_GetObjectData_m29743_gshared((LinkedList_1_t4843 *)__this, (SerializationInfo_t1066 *)___info, (StreamingContext_t1067 )___context, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
 void LinkedList_1_OnDeserialization_m29744_gshared (LinkedList_1_t4843 * __this, Object_t * ___sender, MethodInfo* method);
#define LinkedList_1_OnDeserialization_m29744(__this, ___sender, method) (void)LinkedList_1_OnDeserialization_m29744_gshared((LinkedList_1_t4843 *)__this, (Object_t *)___sender, method)
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
 bool LinkedList_1_Remove_m29745_gshared (LinkedList_1_t4843 * __this, Object_t * ___value, MethodInfo* method);
#define LinkedList_1_Remove_m29745(__this, ___value, method) (bool)LinkedList_1_Remove_m29745_gshared((LinkedList_1_t4843 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
 void LinkedList_1_Remove_m29746_gshared (LinkedList_1_t4843 * __this, LinkedListNode_1_t4842 * ___node, MethodInfo* method);
#define LinkedList_1_Remove_m29746(__this, ___node, method) (void)LinkedList_1_Remove_m29746_gshared((LinkedList_1_t4843 *)__this, (LinkedListNode_1_t4842 *)___node, method)
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
 int32_t LinkedList_1_get_Count_m29747_gshared (LinkedList_1_t4843 * __this, MethodInfo* method);
#define LinkedList_1_get_Count_m29747(__this, method) (int32_t)LinkedList_1_get_Count_m29747_gshared((LinkedList_1_t4843 *)__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
 LinkedListNode_1_t4842 * LinkedList_1_get_First_m29748_gshared (LinkedList_1_t4843 * __this, MethodInfo* method);
#define LinkedList_1_get_First_m29748(__this, method) (LinkedListNode_1_t4842 *)LinkedList_1_get_First_m29748_gshared((LinkedList_1_t4843 *)__this, method)
