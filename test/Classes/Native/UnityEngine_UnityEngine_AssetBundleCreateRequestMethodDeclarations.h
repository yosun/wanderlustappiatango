﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t908;
// UnityEngine.AssetBundle
struct AssetBundle_t910;

// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
 void AssetBundleCreateRequest__ctor_m5421 (AssetBundleCreateRequest_t908 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
 AssetBundle_t910 * AssetBundleCreateRequest_get_assetBundle_m5422 (AssetBundleCreateRequest_t908 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
 void AssetBundleCreateRequest_DisableCompatibilityChecks_m5423 (AssetBundleCreateRequest_t908 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
