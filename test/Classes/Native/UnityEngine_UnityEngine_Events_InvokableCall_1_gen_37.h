﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.VideoBackgroundBehaviour>
struct UnityAction_1_t2964;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>
struct InvokableCall_1_t2963  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.VideoBackgroundBehaviour>::Delegate
	UnityAction_1_t2964 * ___Delegate_0;
};
