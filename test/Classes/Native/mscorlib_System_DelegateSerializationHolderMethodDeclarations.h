﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.DelegateSerializationHolder
struct DelegateSerializationHolder_t2204;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1066;
// System.Delegate
struct Delegate_t153;
// System.Object
struct Object_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.DelegateSerializationHolder::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void DelegateSerializationHolder__ctor_m12886 (DelegateSerializationHolder_t2204 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___ctx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DelegateSerializationHolder::GetDelegateData(System.Delegate,System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void DelegateSerializationHolder_GetDelegateData_m12887 (Object_t * __this/* static, unused */, Delegate_t153 * ___instance, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___ctx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.DelegateSerializationHolder::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void DelegateSerializationHolder_GetObjectData_m12888 (DelegateSerializationHolder_t2204 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.DelegateSerializationHolder::GetRealObject(System.Runtime.Serialization.StreamingContext)
 Object_t * DelegateSerializationHolder_GetRealObject_m12889 (DelegateSerializationHolder_t2204 * __this, StreamingContext_t1067  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
