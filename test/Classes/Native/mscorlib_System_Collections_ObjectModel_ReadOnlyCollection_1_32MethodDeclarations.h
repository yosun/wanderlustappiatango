﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>
struct ReadOnlyCollection_1_t3928;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t72;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<Vuforia.ReconstructionAbstractBehaviour>
struct IList_1_t3931;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// Vuforia.ReconstructionAbstractBehaviour[]
struct ReconstructionAbstractBehaviourU5BU5D_t821;
// System.Collections.Generic.IEnumerator`1<Vuforia.ReconstructionAbstractBehaviour>
struct IEnumerator_1_t785;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_0MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m22707(__this, ___list, method) (void)ReadOnlyCollection_1__ctor_m14646_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t*)___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22708(__this, ___item, method) (void)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14647_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22709(__this, method) (void)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14648_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22710(__this, ___index, ___item, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14649_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22711(__this, ___item, method) (bool)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14650_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22712(__this, ___index, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14651_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22713(__this, ___index, method) (ReconstructionAbstractBehaviour_t72 *)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14652_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22714(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14653_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22715(__this, method) (bool)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14654_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22716(__this, ___array, ___index, method) (void)ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14655_gshared((ReadOnlyCollection_1_t2849 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22717(__this, method) (Object_t *)ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14656_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m22718(__this, ___value, method) (int32_t)ReadOnlyCollection_1_System_Collections_IList_Add_m14657_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m22719(__this, method) (void)ReadOnlyCollection_1_System_Collections_IList_Clear_m14658_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m22720(__this, ___value, method) (bool)ReadOnlyCollection_1_System_Collections_IList_Contains_m14659_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22721(__this, ___value, method) (int32_t)ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14660_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m22722(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_Insert_m14661_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m22723(__this, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_Remove_m14662_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22724(__this, ___index, method) (void)ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14663_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22725(__this, method) (bool)ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14664_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22726(__this, method) (Object_t *)ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14665_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22727(__this, method) (bool)ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14666_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22728(__this, method) (bool)ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14667_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m22729(__this, ___index, method) (Object_t *)ReadOnlyCollection_1_System_Collections_IList_get_Item_m14668_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m22730(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_set_Item_m14669_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::Contains(T)
#define ReadOnlyCollection_1_Contains_m22731(__this, ___value, method) (bool)ReadOnlyCollection_1_Contains_m14670_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m22732(__this, ___array, ___index, method) (void)ReadOnlyCollection_1_CopyTo_m14671_gshared((ReadOnlyCollection_1_t2849 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m22733(__this, method) (Object_t*)ReadOnlyCollection_1_GetEnumerator_m14672_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m22734(__this, ___value, method) (int32_t)ReadOnlyCollection_1_IndexOf_m14673_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::get_Count()
#define ReadOnlyCollection_1_get_Count_m22735(__this, method) (int32_t)ReadOnlyCollection_1_get_Count_m14674_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.ReconstructionAbstractBehaviour>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m22736(__this, ___index, method) (ReconstructionAbstractBehaviour_t72 *)ReadOnlyCollection_1_get_Item_m14675_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, method)
