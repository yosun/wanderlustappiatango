﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Enumerator_t825;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t697;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Dictionary_2_t698;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_33MethodDeclarations.h"
#define Enumerator__ctor_m23703(__this, ___host, method) (void)Enumerator__ctor_m17701_gshared((Enumerator_t3287 *)__this, (Dictionary_2_t3275 *)___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23704(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m17702_gshared((Enumerator_t3287 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Dispose()
#define Enumerator_Dispose_m23705(__this, method) (void)Enumerator_Dispose_m17703_gshared((Enumerator_t3287 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::MoveNext()
#define Enumerator_MoveNext_m4973(__this, method) (bool)Enumerator_MoveNext_m17704_gshared((Enumerator_t3287 *)__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Current()
#define Enumerator_get_Current_m4971(__this, method) (List_1_t697 *)Enumerator_get_Current_m17705_gshared((Enumerator_t3287 *)__this, method)
