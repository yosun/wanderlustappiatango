﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.DSAParameters
struct DSAParameters_t1509;
struct DSAParameters_t1509_marshaled;

void DSAParameters_t1509_marshal(const DSAParameters_t1509& unmarshaled, DSAParameters_t1509_marshaled& marshaled);
void DSAParameters_t1509_marshal_back(const DSAParameters_t1509_marshaled& marshaled, DSAParameters_t1509& unmarshaled);
void DSAParameters_t1509_marshal_cleanup(DSAParameters_t1509_marshaled& marshaled);
