﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoBehaviour>
struct UnityAction_1_t2784;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>
struct InvokableCall_1_t2783  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoBehaviour>::Delegate
	UnityAction_1_t2784 * ___Delegate_0;
};
