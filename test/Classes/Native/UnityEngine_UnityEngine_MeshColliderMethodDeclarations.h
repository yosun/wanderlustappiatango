﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.MeshCollider
struct MeshCollider_t582;
// UnityEngine.Mesh
struct Mesh_t174;

// System.Void UnityEngine.MeshCollider::set_sharedMesh(UnityEngine.Mesh)
 void MeshCollider_set_sharedMesh_m4513 (MeshCollider_t582 * __this, Mesh_t174 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
