﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct KeyValuePair_2_t3281;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m17641_gshared (KeyValuePair_2_t3281 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define KeyValuePair_2__ctor_m17641(__this, ___key, ___value, method) (void)KeyValuePair_2__ctor_m17641_gshared((KeyValuePair_2_t3281 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
 Object_t * KeyValuePair_2_get_Key_m17642_gshared (KeyValuePair_2_t3281 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Key_m17642(__this, method) (Object_t *)KeyValuePair_2_get_Key_m17642_gshared((KeyValuePair_2_t3281 *)__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m17643_gshared (KeyValuePair_2_t3281 * __this, Object_t * ___value, MethodInfo* method);
#define KeyValuePair_2_set_Key_m17643(__this, ___value, method) (void)KeyValuePair_2_set_Key_m17643_gshared((KeyValuePair_2_t3281 *)__this, (Object_t *)___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
 Object_t * KeyValuePair_2_get_Value_m17644_gshared (KeyValuePair_2_t3281 * __this, MethodInfo* method);
#define KeyValuePair_2_get_Value_m17644(__this, method) (Object_t *)KeyValuePair_2_get_Value_m17644_gshared((KeyValuePair_2_t3281 *)__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m17645_gshared (KeyValuePair_2_t3281 * __this, Object_t * ___value, MethodInfo* method);
#define KeyValuePair_2_set_Value_m17645(__this, ___value, method) (void)KeyValuePair_2_set_Value_m17645_gshared((KeyValuePair_2_t3281 *)__this, (Object_t *)___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
 String_t* KeyValuePair_2_ToString_m17646_gshared (KeyValuePair_2_t3281 * __this, MethodInfo* method);
#define KeyValuePair_2_ToString_m17646(__this, method) (String_t*)KeyValuePair_2_ToString_m17646_gshared((KeyValuePair_2_t3281 *)__this, method)
