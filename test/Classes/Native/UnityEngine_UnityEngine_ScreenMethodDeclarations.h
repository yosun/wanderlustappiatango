﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Screen
struct Screen_t942;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"

// System.Int32 UnityEngine.Screen::get_width()
 int32_t Screen_get_width_m273 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_height()
 int32_t Screen_get_height_m274 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Screen::get_dpi()
 float Screen_get_dpi_m2483 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Screen::get_autorotateToPortrait()
 bool Screen_get_autorotateToPortrait_m4849 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Screen::set_autorotateToPortrait(System.Boolean)
 void Screen_set_autorotateToPortrait_m4853 (Object_t * __this/* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Screen::get_autorotateToPortraitUpsideDown()
 bool Screen_get_autorotateToPortraitUpsideDown_m4850 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Screen::set_autorotateToPortraitUpsideDown(System.Boolean)
 void Screen_set_autorotateToPortraitUpsideDown_m4854 (Object_t * __this/* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Screen::get_autorotateToLandscapeLeft()
 bool Screen_get_autorotateToLandscapeLeft_m4847 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Screen::set_autorotateToLandscapeLeft(System.Boolean)
 void Screen_set_autorotateToLandscapeLeft_m4851 (Object_t * __this/* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Screen::get_autorotateToLandscapeRight()
 bool Screen_get_autorotateToLandscapeRight_m4848 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Screen::set_autorotateToLandscapeRight(System.Boolean)
 void Screen_set_autorotateToLandscapeRight_m4852 (Object_t * __this/* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
 int32_t Screen_get_orientation_m443 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)
 void Screen_set_orientation_m5349 (Object_t * __this/* static, unused */, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Screen::set_sleepTimeout(System.Int32)
 void Screen_set_sleepTimeout_m244 (Object_t * __this/* static, unused */, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
