﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Raycaster
struct Raycaster_t20;
// UnityEngine.Camera
struct Camera_t3;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void Raycaster::.ctor()
 void Raycaster__ctor_m53 (Raycaster_t20 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raycaster::RaycastIt(UnityEngine.Camera,UnityEngine.Vector2,System.Single)
 void Raycaster_RaycastIt_m54 (Raycaster_t20 * __this, Camera_t3 * ___cam, Vector2_t9  ___pos, float ___distance, MethodInfo* method) IL2CPP_METHOD_ATTR;
