﻿#pragma once
#include <stdint.h>
// Mono.Math.BigInteger
struct BigInteger_t1537;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t1538  : public Object_t
{
	// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::mod
	BigInteger_t1537 * ___mod_0;
	// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::constant
	BigInteger_t1537 * ___constant_1;
};
