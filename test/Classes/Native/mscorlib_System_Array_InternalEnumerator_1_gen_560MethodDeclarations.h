﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.UIntPtr>
struct InternalEnumerator_1_t4938;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.UIntPtr
#include "mscorlib_System_UIntPtr.h"

// System.Void System.Array/InternalEnumerator`1<System.UIntPtr>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30272 (InternalEnumerator_1_t4938 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.UIntPtr>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30273 (InternalEnumerator_1_t4938 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.UIntPtr>::Dispose()
 void InternalEnumerator_1_Dispose_m30274 (InternalEnumerator_1_t4938 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.UIntPtr>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30275 (InternalEnumerator_1_t4938 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.UIntPtr>::get_Current()
 UIntPtr_t1690  InternalEnumerator_1_get_Current_m30276 (InternalEnumerator_1_t4938 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
