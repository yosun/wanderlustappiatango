﻿#pragma once
#include <stdint.h>
// Vuforia.VirtualButton
struct VirtualButton_t595;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.VirtualButton>
struct Comparison_1_t3754  : public MulticastDelegate_t325
{
};
