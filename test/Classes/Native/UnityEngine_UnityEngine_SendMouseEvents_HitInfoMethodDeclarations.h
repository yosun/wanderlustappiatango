﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SendMouseEvents/HitInfo
struct HitInfo_t1056;
// System.String
struct String_t;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"

// System.Void UnityEngine.SendMouseEvents/HitInfo::SendMessage(System.String)
 void HitInfo_SendMessage_m6307 (HitInfo_t1056 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::Compare(UnityEngine.SendMouseEvents/HitInfo,UnityEngine.SendMouseEvents/HitInfo)
 bool HitInfo_Compare_m6308 (Object_t * __this/* static, unused */, HitInfo_t1056  ___lhs, HitInfo_t1056  ___rhs, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SendMouseEvents/HitInfo::op_Implicit(UnityEngine.SendMouseEvents/HitInfo)
 bool HitInfo_op_Implicit_m6309 (Object_t * __this/* static, unused */, HitInfo_t1056  ___exists, MethodInfo* method) IL2CPP_METHOD_ATTR;
