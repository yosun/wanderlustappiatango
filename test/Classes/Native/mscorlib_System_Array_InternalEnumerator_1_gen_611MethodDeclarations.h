﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.IO.FileAttributes>
struct InternalEnumerator_1_t4994;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributes.h"

// System.Void System.Array/InternalEnumerator`1<System.IO.FileAttributes>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30552 (InternalEnumerator_1_t4994 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.IO.FileAttributes>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30553 (InternalEnumerator_1_t4994 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileAttributes>::Dispose()
 void InternalEnumerator_1_Dispose_m30554 (InternalEnumerator_1_t4994 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileAttributes>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30555 (InternalEnumerator_1_t4994 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.IO.FileAttributes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30556 (InternalEnumerator_1_t4994 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
