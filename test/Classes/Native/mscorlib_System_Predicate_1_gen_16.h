﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t293;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.UI.Graphic>
struct Predicate_1_t3371  : public MulticastDelegate_t325
{
};
