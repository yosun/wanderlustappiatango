﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerator_1_t5695_il2cpp_TypeInfo;

// ARVRModes
#include "AssemblyU2DCSharp_ARVRModes.h"

// System.Array
#include "mscorlib_System_Array.h"

// T System.Collections.Generic.IEnumerator`1<ARVRModes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<ARVRModes>
extern MethodInfo IEnumerator_1_get_Current_m41083_MethodInfo;
static PropertyInfo IEnumerator_1_t5695____Current_PropertyInfo = 
{
	&IEnumerator_1_t5695_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41083_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5695_PropertyInfos[] =
{
	&IEnumerator_1_t5695____Current_PropertyInfo,
	NULL
};
extern Il2CppType ARVRModes_t5_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41083_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<ARVRModes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41083_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5695_il2cpp_TypeInfo/* declaring_type */
	, &ARVRModes_t5_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41083_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5695_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41083_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t5695_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5695_0_0_0;
extern Il2CppType IEnumerator_1_t5695_1_0_0;
struct IEnumerator_1_t5695;
extern Il2CppGenericClass IEnumerator_1_t5695_GenericClass;
TypeInfo IEnumerator_1_t5695_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5695_MethodInfos/* methods */
	, IEnumerator_1_t5695_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5695_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5695_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5695_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5695_0_0_0/* byval_arg */
	, &IEnumerator_1_t5695_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5695_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t455_il2cpp_TypeInfo;

// System.Object
#include "mscorlib_System_Object.h"


// T System.Collections.Generic.IEnumerator`1<System.Object>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Object>
extern MethodInfo IEnumerator_1_get_Current_m35062_MethodInfo;
static PropertyInfo IEnumerator_1_t455____Current_PropertyInfo = 
{
	&IEnumerator_1_t455_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m35062_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t455_PropertyInfos[] =
{
	&IEnumerator_1_t455____Current_PropertyInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m35062_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Object>::get_Current()
MethodInfo IEnumerator_1_get_Current_m35062_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t455_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m35062_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t455_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m35062_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t455_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t455_0_0_0;
extern Il2CppType IEnumerator_1_t455_1_0_0;
struct IEnumerator_1_t455;
extern Il2CppGenericClass IEnumerator_1_t455_GenericClass;
TypeInfo IEnumerator_1_t455_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t455_MethodInfos/* methods */
	, IEnumerator_1_t455_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t455_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t455_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t455_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t455_0_0_0/* byval_arg */
	, &IEnumerator_1_t455_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t455_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<ARVRModes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2697_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<ARVRModes>
#include "mscorlib_System_Array_InternalEnumerator_1_genMethodDeclarations.h"

// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo ARVRModes_t5_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1493_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m13985_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7651_MethodInfo;
extern MethodInfo Array_get_Length_m7656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisARVRModes_t5_m31438_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m31436_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m31436(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<ARVRModes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<ARVRModes>(System.Int32)
#define Array_InternalArray__get_Item_TisARVRModes_t5_m31438(__this, p0, method) (ARVRModes_t5 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<ARVRModes>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<ARVRModes>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<ARVRModes>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<ARVRModes>::MoveNext()
// T System.Array/InternalEnumerator`1<ARVRModes>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<ARVRModes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2697____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2697_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2697, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2697____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2697_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2697, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2697_FieldInfos[] =
{
	&InternalEnumerator_1_t2697____array_0_FieldInfo,
	&InternalEnumerator_1_t2697____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13979_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2697____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2697_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13979_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2697____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2697_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m13985_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2697_PropertyInfos[] =
{
	&InternalEnumerator_1_t2697____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2697____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2697_InternalEnumerator_1__ctor_m13977_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m13977_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ARVRModes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m13977_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2697_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2697_InternalEnumerator_1__ctor_m13977_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m13977_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13979_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<ARVRModes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13979_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2697_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13979_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m13981_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ARVRModes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m13981_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2697_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m13981_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m13983_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<ARVRModes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m13983_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2697_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m13983_GenericMethod/* genericMethod */

};
extern Il2CppType ARVRModes_t5_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m13985_GenericMethod;
// T System.Array/InternalEnumerator`1<ARVRModes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m13985_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2697_il2cpp_TypeInfo/* declaring_type */
	, &ARVRModes_t5_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m13985_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2697_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m13977_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13979_MethodInfo,
	&InternalEnumerator_1_Dispose_m13981_MethodInfo,
	&InternalEnumerator_1_MoveNext_m13983_MethodInfo,
	&InternalEnumerator_1_get_Current_m13985_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m13983_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m13981_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2697_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13979_MethodInfo,
	&InternalEnumerator_1_MoveNext_m13983_MethodInfo,
	&InternalEnumerator_1_Dispose_m13981_MethodInfo,
	&InternalEnumerator_1_get_Current_m13985_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2697_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5695_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2697_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5695_il2cpp_TypeInfo, 7},
};
extern TypeInfo ARVRModes_t5_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2697_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m13985_MethodInfo/* Method Usage */,
	&ARVRModes_t5_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisARVRModes_t5_m31438_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2697_0_0_0;
extern Il2CppType InternalEnumerator_1_t2697_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t2697_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t2697_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2697_MethodInfos/* methods */
	, InternalEnumerator_1_t2697_PropertyInfos/* properties */
	, InternalEnumerator_1_t2697_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2697_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2697_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2697_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2697_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2697_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2697_1_0_0/* this_arg */
	, InternalEnumerator_1_t2697_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2697_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2697_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2697)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2698_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

extern TypeInfo Object_t_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m13986_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo;


// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m13978_MethodInfo;
 void InternalEnumerator_1__ctor_m13978_gshared (InternalEnumerator_1_t2698 * __this, Array_t * ___array, MethodInfo* method)
{
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared (InternalEnumerator_1_t2698 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (( Object_t * (*) (InternalEnumerator_1_t2698 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Object_t * L_1 = L_0;
		return ((Object_t *)L_1);
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m13982_MethodInfo;
 void InternalEnumerator_1_Dispose_m13982_gshared (InternalEnumerator_1_t2698 * __this, MethodInfo* method)
{
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m13984_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m13984_gshared (InternalEnumerator_1_t2698 * __this, MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
 Object_t * InternalEnumerator_1_get_Current_m13986_gshared (InternalEnumerator_1_t2698 * __this, MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		Object_t * L_8 = (( Object_t * (*) (Array_t * __this, int32_t p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Object>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2698____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2698_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2698, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2698____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2698_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2698, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2698_FieldInfos[] =
{
	&InternalEnumerator_1_t2698____array_0_FieldInfo,
	&InternalEnumerator_1_t2698____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t2698____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2698_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2698____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2698_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m13986_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2698_PropertyInfos[] =
{
	&InternalEnumerator_1_t2698____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2698____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2698_InternalEnumerator_1__ctor_m13978_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m13978_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m13978_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2698_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2698_InternalEnumerator_1__ctor_m13978_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m13978_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2698_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m13982_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m13982_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2698_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m13982_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m13984_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m13984_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2698_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m13984_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m13986_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m13986_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2698_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m13986_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2698_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m13978_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_MethodInfo,
	&InternalEnumerator_1_Dispose_m13982_MethodInfo,
	&InternalEnumerator_1_MoveNext_m13984_MethodInfo,
	&InternalEnumerator_1_get_Current_m13986_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t2698_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_MethodInfo,
	&InternalEnumerator_1_MoveNext_m13984_MethodInfo,
	&InternalEnumerator_1_Dispose_m13982_MethodInfo,
	&InternalEnumerator_1_get_Current_m13986_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2698_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t455_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2698_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t455_il2cpp_TypeInfo, 7},
};
extern TypeInfo Object_t_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2698_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m13986_MethodInfo/* Method Usage */,
	&Object_t_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisObject_t_m31436_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2698_0_0_0;
extern Il2CppType InternalEnumerator_1_t2698_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2698_GenericClass;
TypeInfo InternalEnumerator_1_t2698_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2698_MethodInfos/* methods */
	, InternalEnumerator_1_t2698_PropertyInfos/* properties */
	, InternalEnumerator_1_t2698_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2698_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2698_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2698_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2698_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2698_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2698_1_0_0/* this_arg */
	, InternalEnumerator_1_t2698_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2698_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2698_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2698)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t2848_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Object>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Object>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Object>
extern MethodInfo ICollection_1_get_Count_m35608_MethodInfo;
static PropertyInfo ICollection_1_t2848____Count_PropertyInfo = 
{
	&ICollection_1_t2848_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m35608_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41084_MethodInfo;
static PropertyInfo ICollection_1_t2848____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t2848_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41084_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t2848_PropertyInfos[] =
{
	&ICollection_1_t2848____Count_PropertyInfo,
	&ICollection_1_t2848____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m35608_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Object>::get_Count()
MethodInfo ICollection_1_get_Count_m35608_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t2848_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m35608_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41084_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Object>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41084_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t2848_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41084_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ICollection_1_t2848_ICollection_1_Add_m41085_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41085_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Object>::Add(T)
MethodInfo ICollection_1_Add_m41085_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t2848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t2848_ICollection_1_Add_m41085_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41085_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41086_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Object>::Clear()
MethodInfo ICollection_1_Clear_m41086_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t2848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41086_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ICollection_1_t2848_ICollection_1_Contains_m35060_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m35060_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Contains(T)
MethodInfo ICollection_1_Contains_m35060_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t2848_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t2848_ICollection_1_Contains_m35060_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m35060_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t2848_ICollection_1_CopyTo_m35904_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m35904_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Object>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m35904_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t2848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t2848_ICollection_1_CopyTo_m35904_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m35904_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo ICollection_1_t2848_ICollection_1_Remove_m41087_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41087_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Object>::Remove(T)
MethodInfo ICollection_1_Remove_m41087_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t2848_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t2848_ICollection_1_Remove_m41087_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41087_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t2848_MethodInfos[] =
{
	&ICollection_1_get_Count_m35608_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41084_MethodInfo,
	&ICollection_1_Add_m41085_MethodInfo,
	&ICollection_1_Clear_m41086_MethodInfo,
	&ICollection_1_Contains_m35060_MethodInfo,
	&ICollection_1_CopyTo_m35904_MethodInfo,
	&ICollection_1_Remove_m41087_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t2847_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t2848_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t2848_0_0_0;
extern Il2CppType ICollection_1_t2848_1_0_0;
struct ICollection_1_t2848;
extern Il2CppGenericClass ICollection_1_t2848_GenericClass;
TypeInfo ICollection_1_t2848_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t2848_MethodInfos/* methods */
	, ICollection_1_t2848_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t2848_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t2848_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t2848_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t2848_0_0_0/* byval_arg */
	, &ICollection_1_t2848_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t2848_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Object>
extern Il2CppType IEnumerator_1_t455_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m35061_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m35061_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t2847_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t455_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m35061_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t2847_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m35061_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t2847_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t2847_0_0_0;
extern Il2CppType IEnumerable_1_t2847_1_0_0;
struct IEnumerable_1_t2847;
extern Il2CppGenericClass IEnumerable_1_t2847_GenericClass;
TypeInfo IEnumerable_1_t2847_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t2847_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t2847_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t2847_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t2847_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t2847_0_0_0/* byval_arg */
	, &IEnumerable_1_t2847_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t2847_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t2851_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Object>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Object>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Object>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Object>
extern MethodInfo IList_1_get_Item_m35907_MethodInfo;
extern MethodInfo IList_1_set_Item_m41088_MethodInfo;
static PropertyInfo IList_1_t2851____Item_PropertyInfo = 
{
	&IList_1_t2851_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m35907_MethodInfo/* get */
	, &IList_1_set_Item_m41088_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t2851_PropertyInfos[] =
{
	&IList_1_t2851____Item_PropertyInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IList_1_t2851_IList_1_IndexOf_m41089_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41089_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Object>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41089_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t2851_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t2851_IList_1_IndexOf_m41089_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41089_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IList_1_t2851_IList_1_Insert_m41090_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41090_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Object>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41090_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t2851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t2851_IList_1_Insert_m41090_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41090_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t2851_IList_1_RemoveAt_m41091_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41091_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Object>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41091_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t2851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t2851_IList_1_RemoveAt_m41091_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41091_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t2851_IList_1_get_Item_m35907_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m35907_GenericMethod;
// T System.Collections.Generic.IList`1<System.Object>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m35907_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t2851_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t2851_IList_1_get_Item_m35907_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m35907_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo IList_1_t2851_IList_1_set_Item_m41088_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41088_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Object>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41088_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t2851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t2851_IList_1_set_Item_m41088_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41088_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t2851_MethodInfos[] =
{
	&IList_1_IndexOf_m41089_MethodInfo,
	&IList_1_Insert_m41090_MethodInfo,
	&IList_1_RemoveAt_m41091_MethodInfo,
	&IList_1_get_Item_m35907_MethodInfo,
	&IList_1_set_Item_m41088_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t2851_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t2851_0_0_0;
extern Il2CppType IList_1_t2851_1_0_0;
struct IList_1_t2851;
extern Il2CppGenericClass IList_1_t2851_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t2851_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t2851_MethodInfos/* methods */
	, IList_1_t2851_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t2851_il2cpp_TypeInfo/* element_class */
	, IList_1_t2851_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t2851_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t2851_0_0_0/* byval_arg */
	, &IList_1_t2851_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t2851_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7216_il2cpp_TypeInfo;

#include "Assembly-CSharp_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<ARVRModes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<ARVRModes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<ARVRModes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<ARVRModes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<ARVRModes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<ARVRModes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<ARVRModes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<ARVRModes>
extern MethodInfo ICollection_1_get_Count_m41092_MethodInfo;
static PropertyInfo ICollection_1_t7216____Count_PropertyInfo = 
{
	&ICollection_1_t7216_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41092_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41093_MethodInfo;
static PropertyInfo ICollection_1_t7216____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7216_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41093_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7216_PropertyInfos[] =
{
	&ICollection_1_t7216____Count_PropertyInfo,
	&ICollection_1_t7216____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41092_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<ARVRModes>::get_Count()
MethodInfo ICollection_1_get_Count_m41092_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7216_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41092_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41093_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ARVRModes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41093_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7216_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41093_GenericMethod/* genericMethod */

};
extern Il2CppType ARVRModes_t5_0_0_0;
extern Il2CppType ARVRModes_t5_0_0_0;
static ParameterInfo ICollection_1_t7216_ICollection_1_Add_m41094_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ARVRModes_t5_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41094_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ARVRModes>::Add(T)
MethodInfo ICollection_1_Add_m41094_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7216_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7216_ICollection_1_Add_m41094_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41094_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41095_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ARVRModes>::Clear()
MethodInfo ICollection_1_Clear_m41095_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7216_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41095_GenericMethod/* genericMethod */

};
extern Il2CppType ARVRModes_t5_0_0_0;
static ParameterInfo ICollection_1_t7216_ICollection_1_Contains_m41096_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ARVRModes_t5_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41096_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ARVRModes>::Contains(T)
MethodInfo ICollection_1_Contains_m41096_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7216_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7216_ICollection_1_Contains_m41096_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41096_GenericMethod/* genericMethod */

};
extern Il2CppType ARVRModesU5BU5D_t5157_0_0_0;
extern Il2CppType ARVRModesU5BU5D_t5157_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7216_ICollection_1_CopyTo_m41097_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ARVRModesU5BU5D_t5157_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41097_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ARVRModes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41097_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7216_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7216_ICollection_1_CopyTo_m41097_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41097_GenericMethod/* genericMethod */

};
extern Il2CppType ARVRModes_t5_0_0_0;
static ParameterInfo ICollection_1_t7216_ICollection_1_Remove_m41098_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ARVRModes_t5_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41098_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ARVRModes>::Remove(T)
MethodInfo ICollection_1_Remove_m41098_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7216_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7216_ICollection_1_Remove_m41098_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41098_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7216_MethodInfos[] =
{
	&ICollection_1_get_Count_m41092_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41093_MethodInfo,
	&ICollection_1_Add_m41094_MethodInfo,
	&ICollection_1_Clear_m41095_MethodInfo,
	&ICollection_1_Contains_m41096_MethodInfo,
	&ICollection_1_CopyTo_m41097_MethodInfo,
	&ICollection_1_Remove_m41098_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7218_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7216_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7218_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7216_0_0_0;
extern Il2CppType ICollection_1_t7216_1_0_0;
struct ICollection_1_t7216;
extern Il2CppGenericClass ICollection_1_t7216_GenericClass;
TypeInfo ICollection_1_t7216_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7216_MethodInfos/* methods */
	, ICollection_1_t7216_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7216_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7216_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7216_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7216_0_0_0/* byval_arg */
	, &ICollection_1_t7216_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7216_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ARVRModes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<ARVRModes>
extern Il2CppType IEnumerator_1_t5695_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41099_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ARVRModes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41099_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7218_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5695_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41099_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7218_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41099_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7218_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7218_0_0_0;
extern Il2CppType IEnumerable_1_t7218_1_0_0;
struct IEnumerable_1_t7218;
extern Il2CppGenericClass IEnumerable_1_t7218_GenericClass;
TypeInfo IEnumerable_1_t7218_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7218_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7218_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7218_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7218_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7218_0_0_0/* byval_arg */
	, &IEnumerable_1_t7218_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7218_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7217_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<ARVRModes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<ARVRModes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<ARVRModes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<ARVRModes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<ARVRModes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<ARVRModes>
extern MethodInfo IList_1_get_Item_m41100_MethodInfo;
extern MethodInfo IList_1_set_Item_m41101_MethodInfo;
static PropertyInfo IList_1_t7217____Item_PropertyInfo = 
{
	&IList_1_t7217_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41100_MethodInfo/* get */
	, &IList_1_set_Item_m41101_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7217_PropertyInfos[] =
{
	&IList_1_t7217____Item_PropertyInfo,
	NULL
};
extern Il2CppType ARVRModes_t5_0_0_0;
static ParameterInfo IList_1_t7217_IList_1_IndexOf_m41102_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ARVRModes_t5_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41102_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<ARVRModes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41102_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7217_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7217_IList_1_IndexOf_m41102_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41102_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ARVRModes_t5_0_0_0;
static ParameterInfo IList_1_t7217_IList_1_Insert_m41103_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ARVRModes_t5_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41103_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ARVRModes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41103_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7217_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7217_IList_1_Insert_m41103_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41103_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7217_IList_1_RemoveAt_m41104_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41104_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ARVRModes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41104_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7217_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7217_IList_1_RemoveAt_m41104_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41104_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7217_IList_1_get_Item_m41100_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ARVRModes_t5_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41100_GenericMethod;
// T System.Collections.Generic.IList`1<ARVRModes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41100_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7217_il2cpp_TypeInfo/* declaring_type */
	, &ARVRModes_t5_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7217_IList_1_get_Item_m41100_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41100_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ARVRModes_t5_0_0_0;
static ParameterInfo IList_1_t7217_IList_1_set_Item_m41101_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ARVRModes_t5_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41101_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ARVRModes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41101_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7217_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7217_IList_1_set_Item_m41101_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41101_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7217_MethodInfos[] =
{
	&IList_1_IndexOf_m41102_MethodInfo,
	&IList_1_Insert_m41103_MethodInfo,
	&IList_1_RemoveAt_m41104_MethodInfo,
	&IList_1_get_Item_m41100_MethodInfo,
	&IList_1_set_Item_m41101_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7217_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7216_il2cpp_TypeInfo,
	&IEnumerable_1_t7218_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7217_0_0_0;
extern Il2CppType IList_1_t7217_1_0_0;
struct IList_1_t7217;
extern Il2CppGenericClass IList_1_t7217_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7217_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7217_MethodInfos/* methods */
	, IList_1_t7217_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7217_il2cpp_TypeInfo/* element_class */
	, IList_1_t7217_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7217_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7217_0_0_0/* byval_arg */
	, &IList_1_t7217_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7217_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7219_il2cpp_TypeInfo;

// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>
extern MethodInfo ICollection_1_get_Count_m41105_MethodInfo;
static PropertyInfo ICollection_1_t7219____Count_PropertyInfo = 
{
	&ICollection_1_t7219_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41105_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41106_MethodInfo;
static PropertyInfo ICollection_1_t7219____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7219_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41106_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7219_PropertyInfos[] =
{
	&ICollection_1_t7219____Count_PropertyInfo,
	&ICollection_1_t7219____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41105_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41105_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7219_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41105_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41106_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41106_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7219_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41106_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviour_t6_0_0_0;
extern Il2CppType MonoBehaviour_t6_0_0_0;
static ParameterInfo ICollection_1_t7219_ICollection_1_Add_m41107_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t6_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41107_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41107_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7219_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7219_ICollection_1_Add_m41107_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41107_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41108_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41108_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7219_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41108_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviour_t6_0_0_0;
static ParameterInfo ICollection_1_t7219_ICollection_1_Contains_m41109_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t6_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41109_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41109_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7219_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7219_ICollection_1_Contains_m41109_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41109_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviourU5BU5D_t5409_0_0_0;
extern Il2CppType MonoBehaviourU5BU5D_t5409_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7219_ICollection_1_CopyTo_m41110_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviourU5BU5D_t5409_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41110_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41110_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7219_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7219_ICollection_1_CopyTo_m41110_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41110_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviour_t6_0_0_0;
static ParameterInfo ICollection_1_t7219_ICollection_1_Remove_m41111_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t6_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41111_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.MonoBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41111_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7219_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7219_ICollection_1_Remove_m41111_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41111_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7219_MethodInfos[] =
{
	&ICollection_1_get_Count_m41105_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41106_MethodInfo,
	&ICollection_1_Add_m41107_MethodInfo,
	&ICollection_1_Clear_m41108_MethodInfo,
	&ICollection_1_Contains_m41109_MethodInfo,
	&ICollection_1_CopyTo_m41110_MethodInfo,
	&ICollection_1_Remove_m41111_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7221_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7219_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7221_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7219_0_0_0;
extern Il2CppType ICollection_1_t7219_1_0_0;
struct ICollection_1_t7219;
extern Il2CppGenericClass ICollection_1_t7219_GenericClass;
TypeInfo ICollection_1_t7219_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7219_MethodInfos/* methods */
	, ICollection_1_t7219_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7219_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7219_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7219_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7219_0_0_0/* byval_arg */
	, &ICollection_1_t7219_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7219_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.MonoBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.MonoBehaviour>
extern Il2CppType IEnumerator_1_t5697_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41112_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.MonoBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41112_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7221_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5697_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41112_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7221_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41112_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7221_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7221_0_0_0;
extern Il2CppType IEnumerable_1_t7221_1_0_0;
struct IEnumerable_1_t7221;
extern Il2CppGenericClass IEnumerable_1_t7221_GenericClass;
TypeInfo IEnumerable_1_t7221_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7221_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7221_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7221_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7221_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7221_0_0_0/* byval_arg */
	, &IEnumerable_1_t7221_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7221_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5697_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.MonoBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.MonoBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41113_MethodInfo;
static PropertyInfo IEnumerator_1_t5697____Current_PropertyInfo = 
{
	&IEnumerator_1_t5697_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41113_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5697_PropertyInfos[] =
{
	&IEnumerator_1_t5697____Current_PropertyInfo,
	NULL
};
extern Il2CppType MonoBehaviour_t6_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41113_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.MonoBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41113_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5697_il2cpp_TypeInfo/* declaring_type */
	, &MonoBehaviour_t6_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41113_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5697_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41113_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5697_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5697_0_0_0;
extern Il2CppType IEnumerator_1_t5697_1_0_0;
struct IEnumerator_1_t5697;
extern Il2CppGenericClass IEnumerator_1_t5697_GenericClass;
TypeInfo IEnumerator_1_t5697_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5697_MethodInfos/* methods */
	, IEnumerator_1_t5697_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5697_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5697_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5697_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5697_0_0_0/* byval_arg */
	, &IEnumerator_1_t5697_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5697_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_1.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2699_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_1MethodDeclarations.h"

extern TypeInfo MonoBehaviour_t6_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m13991_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMonoBehaviour_t6_m31458_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.MonoBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.MonoBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisMonoBehaviour_t6_m31458(__this, p0, method) (MonoBehaviour_t6 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2699____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2699_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2699, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2699____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2699_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2699, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2699_FieldInfos[] =
{
	&InternalEnumerator_1_t2699____array_0_FieldInfo,
	&InternalEnumerator_1_t2699____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13988_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2699____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2699_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13988_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2699____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2699_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m13991_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2699_PropertyInfos[] =
{
	&InternalEnumerator_1_t2699____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2699____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2699_InternalEnumerator_1__ctor_m13987_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m13987_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m13987_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2699_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2699_InternalEnumerator_1__ctor_m13987_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m13987_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13988_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13988_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2699_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13988_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m13989_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m13989_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2699_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m13989_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m13990_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m13990_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2699_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m13990_GenericMethod/* genericMethod */

};
extern Il2CppType MonoBehaviour_t6_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m13991_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.MonoBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m13991_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2699_il2cpp_TypeInfo/* declaring_type */
	, &MonoBehaviour_t6_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m13991_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2699_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m13987_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13988_MethodInfo,
	&InternalEnumerator_1_Dispose_m13989_MethodInfo,
	&InternalEnumerator_1_MoveNext_m13990_MethodInfo,
	&InternalEnumerator_1_get_Current_m13991_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m13990_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m13989_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2699_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13988_MethodInfo,
	&InternalEnumerator_1_MoveNext_m13990_MethodInfo,
	&InternalEnumerator_1_Dispose_m13989_MethodInfo,
	&InternalEnumerator_1_get_Current_m13991_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2699_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5697_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2699_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5697_il2cpp_TypeInfo, 7},
};
extern TypeInfo MonoBehaviour_t6_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2699_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m13991_MethodInfo/* Method Usage */,
	&MonoBehaviour_t6_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMonoBehaviour_t6_m31458_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2699_0_0_0;
extern Il2CppType InternalEnumerator_1_t2699_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2699_GenericClass;
TypeInfo InternalEnumerator_1_t2699_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2699_MethodInfos/* methods */
	, InternalEnumerator_1_t2699_PropertyInfos/* properties */
	, InternalEnumerator_1_t2699_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2699_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2699_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2699_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2699_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2699_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2699_1_0_0/* this_arg */
	, InternalEnumerator_1_t2699_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2699_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2699_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2699)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7220_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>
extern MethodInfo IList_1_get_Item_m41114_MethodInfo;
extern MethodInfo IList_1_set_Item_m41115_MethodInfo;
static PropertyInfo IList_1_t7220____Item_PropertyInfo = 
{
	&IList_1_t7220_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41114_MethodInfo/* get */
	, &IList_1_set_Item_m41115_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7220_PropertyInfos[] =
{
	&IList_1_t7220____Item_PropertyInfo,
	NULL
};
extern Il2CppType MonoBehaviour_t6_0_0_0;
static ParameterInfo IList_1_t7220_IList_1_IndexOf_m41116_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t6_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41116_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41116_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7220_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7220_IList_1_IndexOf_m41116_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41116_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MonoBehaviour_t6_0_0_0;
static ParameterInfo IList_1_t7220_IList_1_Insert_m41117_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t6_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41117_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41117_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7220_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7220_IList_1_Insert_m41117_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41117_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7220_IList_1_RemoveAt_m41118_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41118_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41118_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7220_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7220_IList_1_RemoveAt_m41118_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41118_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7220_IList_1_get_Item_m41114_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType MonoBehaviour_t6_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41114_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41114_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7220_il2cpp_TypeInfo/* declaring_type */
	, &MonoBehaviour_t6_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7220_IList_1_get_Item_m41114_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41114_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MonoBehaviour_t6_0_0_0;
static ParameterInfo IList_1_t7220_IList_1_set_Item_m41115_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MonoBehaviour_t6_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41115_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.MonoBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41115_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7220_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7220_IList_1_set_Item_m41115_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41115_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7220_MethodInfos[] =
{
	&IList_1_IndexOf_m41116_MethodInfo,
	&IList_1_Insert_m41117_MethodInfo,
	&IList_1_RemoveAt_m41118_MethodInfo,
	&IList_1_get_Item_m41114_MethodInfo,
	&IList_1_set_Item_m41115_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7220_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7219_il2cpp_TypeInfo,
	&IEnumerable_1_t7221_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7220_0_0_0;
extern Il2CppType IList_1_t7220_1_0_0;
struct IList_1_t7220;
extern Il2CppGenericClass IList_1_t7220_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7220_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7220_MethodInfos/* methods */
	, IList_1_t7220_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7220_il2cpp_TypeInfo/* element_class */
	, IList_1_t7220_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7220_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7220_0_0_0/* byval_arg */
	, &IList_1_t7220_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7220_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7222_il2cpp_TypeInfo;

// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_Behaviour.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>
extern MethodInfo ICollection_1_get_Count_m41119_MethodInfo;
static PropertyInfo ICollection_1_t7222____Count_PropertyInfo = 
{
	&ICollection_1_t7222_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41119_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41120_MethodInfo;
static PropertyInfo ICollection_1_t7222____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7222_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41120_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7222_PropertyInfos[] =
{
	&ICollection_1_t7222____Count_PropertyInfo,
	&ICollection_1_t7222____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41119_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41119_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7222_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41119_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41120_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41120_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7222_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41120_GenericMethod/* genericMethod */

};
extern Il2CppType Behaviour_t515_0_0_0;
extern Il2CppType Behaviour_t515_0_0_0;
static ParameterInfo ICollection_1_t7222_ICollection_1_Add_m41121_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Behaviour_t515_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41121_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Add(T)
MethodInfo ICollection_1_Add_m41121_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7222_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7222_ICollection_1_Add_m41121_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41121_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41122_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Clear()
MethodInfo ICollection_1_Clear_m41122_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7222_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41122_GenericMethod/* genericMethod */

};
extern Il2CppType Behaviour_t515_0_0_0;
static ParameterInfo ICollection_1_t7222_ICollection_1_Contains_m41123_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Behaviour_t515_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41123_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41123_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7222_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7222_ICollection_1_Contains_m41123_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41123_GenericMethod/* genericMethod */

};
extern Il2CppType BehaviourU5BU5D_t5410_0_0_0;
extern Il2CppType BehaviourU5BU5D_t5410_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7222_ICollection_1_CopyTo_m41124_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &BehaviourU5BU5D_t5410_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41124_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41124_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7222_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7222_ICollection_1_CopyTo_m41124_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41124_GenericMethod/* genericMethod */

};
extern Il2CppType Behaviour_t515_0_0_0;
static ParameterInfo ICollection_1_t7222_ICollection_1_Remove_m41125_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Behaviour_t515_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41125_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Behaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41125_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7222_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7222_ICollection_1_Remove_m41125_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41125_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7222_MethodInfos[] =
{
	&ICollection_1_get_Count_m41119_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41120_MethodInfo,
	&ICollection_1_Add_m41121_MethodInfo,
	&ICollection_1_Clear_m41122_MethodInfo,
	&ICollection_1_Contains_m41123_MethodInfo,
	&ICollection_1_CopyTo_m41124_MethodInfo,
	&ICollection_1_Remove_m41125_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7224_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7222_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7224_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7222_0_0_0;
extern Il2CppType ICollection_1_t7222_1_0_0;
struct ICollection_1_t7222;
extern Il2CppGenericClass ICollection_1_t7222_GenericClass;
TypeInfo ICollection_1_t7222_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7222_MethodInfos/* methods */
	, ICollection_1_t7222_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7222_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7222_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7222_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7222_0_0_0/* byval_arg */
	, &ICollection_1_t7222_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7222_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Behaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Behaviour>
extern Il2CppType IEnumerator_1_t5699_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41126_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Behaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41126_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7224_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5699_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41126_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7224_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41126_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7224_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7224_0_0_0;
extern Il2CppType IEnumerable_1_t7224_1_0_0;
struct IEnumerable_1_t7224;
extern Il2CppGenericClass IEnumerable_1_t7224_GenericClass;
TypeInfo IEnumerable_1_t7224_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7224_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7224_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7224_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7224_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7224_0_0_0/* byval_arg */
	, &IEnumerable_1_t7224_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7224_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5699_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.Behaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Behaviour>
extern MethodInfo IEnumerator_1_get_Current_m41127_MethodInfo;
static PropertyInfo IEnumerator_1_t5699____Current_PropertyInfo = 
{
	&IEnumerator_1_t5699_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41127_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5699_PropertyInfos[] =
{
	&IEnumerator_1_t5699____Current_PropertyInfo,
	NULL
};
extern Il2CppType Behaviour_t515_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41127_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Behaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41127_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5699_il2cpp_TypeInfo/* declaring_type */
	, &Behaviour_t515_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41127_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5699_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41127_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5699_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5699_0_0_0;
extern Il2CppType IEnumerator_1_t5699_1_0_0;
struct IEnumerator_1_t5699;
extern Il2CppGenericClass IEnumerator_1_t5699_GenericClass;
TypeInfo IEnumerator_1_t5699_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5699_MethodInfos/* methods */
	, IEnumerator_1_t5699_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5699_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5699_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5699_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5699_0_0_0/* byval_arg */
	, &IEnumerator_1_t5699_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5699_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Behaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_2.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2700_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Behaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_2MethodDeclarations.h"

extern TypeInfo Behaviour_t515_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m13996_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisBehaviour_t515_m31469_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Behaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Behaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisBehaviour_t515_m31469(__this, p0, method) (Behaviour_t515 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Behaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2700____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2700_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2700, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2700____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2700_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2700, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2700_FieldInfos[] =
{
	&InternalEnumerator_1_t2700____array_0_FieldInfo,
	&InternalEnumerator_1_t2700____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13993_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2700____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2700_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13993_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2700____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2700_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m13996_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2700_PropertyInfos[] =
{
	&InternalEnumerator_1_t2700____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2700____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2700_InternalEnumerator_1__ctor_m13992_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m13992_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m13992_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2700_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2700_InternalEnumerator_1__ctor_m13992_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m13992_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13993_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13993_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2700_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13993_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m13994_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m13994_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2700_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m13994_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m13995_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m13995_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2700_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m13995_GenericMethod/* genericMethod */

};
extern Il2CppType Behaviour_t515_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m13996_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Behaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m13996_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2700_il2cpp_TypeInfo/* declaring_type */
	, &Behaviour_t515_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m13996_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2700_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m13992_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13993_MethodInfo,
	&InternalEnumerator_1_Dispose_m13994_MethodInfo,
	&InternalEnumerator_1_MoveNext_m13995_MethodInfo,
	&InternalEnumerator_1_get_Current_m13996_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m13995_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m13994_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2700_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13993_MethodInfo,
	&InternalEnumerator_1_MoveNext_m13995_MethodInfo,
	&InternalEnumerator_1_Dispose_m13994_MethodInfo,
	&InternalEnumerator_1_get_Current_m13996_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2700_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5699_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2700_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5699_il2cpp_TypeInfo, 7},
};
extern TypeInfo Behaviour_t515_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2700_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m13996_MethodInfo/* Method Usage */,
	&Behaviour_t515_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisBehaviour_t515_m31469_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2700_0_0_0;
extern Il2CppType InternalEnumerator_1_t2700_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2700_GenericClass;
TypeInfo InternalEnumerator_1_t2700_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2700_MethodInfos/* methods */
	, InternalEnumerator_1_t2700_PropertyInfos/* properties */
	, InternalEnumerator_1_t2700_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2700_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2700_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2700_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2700_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2700_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2700_1_0_0/* this_arg */
	, InternalEnumerator_1_t2700_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2700_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2700_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2700)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7223_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Behaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Behaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Behaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Behaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Behaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Behaviour>
extern MethodInfo IList_1_get_Item_m41128_MethodInfo;
extern MethodInfo IList_1_set_Item_m41129_MethodInfo;
static PropertyInfo IList_1_t7223____Item_PropertyInfo = 
{
	&IList_1_t7223_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41128_MethodInfo/* get */
	, &IList_1_set_Item_m41129_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7223_PropertyInfos[] =
{
	&IList_1_t7223____Item_PropertyInfo,
	NULL
};
extern Il2CppType Behaviour_t515_0_0_0;
static ParameterInfo IList_1_t7223_IList_1_IndexOf_m41130_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Behaviour_t515_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41130_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Behaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41130_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7223_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7223_IList_1_IndexOf_m41130_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41130_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Behaviour_t515_0_0_0;
static ParameterInfo IList_1_t7223_IList_1_Insert_m41131_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Behaviour_t515_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41131_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Behaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41131_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7223_IList_1_Insert_m41131_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41131_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7223_IList_1_RemoveAt_m41132_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41132_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Behaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41132_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7223_IList_1_RemoveAt_m41132_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41132_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7223_IList_1_get_Item_m41128_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Behaviour_t515_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41128_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Behaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41128_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7223_il2cpp_TypeInfo/* declaring_type */
	, &Behaviour_t515_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7223_IList_1_get_Item_m41128_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41128_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Behaviour_t515_0_0_0;
static ParameterInfo IList_1_t7223_IList_1_set_Item_m41129_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Behaviour_t515_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41129_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Behaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41129_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7223_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7223_IList_1_set_Item_m41129_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41129_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7223_MethodInfos[] =
{
	&IList_1_IndexOf_m41130_MethodInfo,
	&IList_1_Insert_m41131_MethodInfo,
	&IList_1_RemoveAt_m41132_MethodInfo,
	&IList_1_get_Item_m41128_MethodInfo,
	&IList_1_set_Item_m41129_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7223_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7222_il2cpp_TypeInfo,
	&IEnumerable_1_t7224_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7223_0_0_0;
extern Il2CppType IList_1_t7223_1_0_0;
struct IList_1_t7223;
extern Il2CppGenericClass IList_1_t7223_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7223_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7223_MethodInfos/* methods */
	, IList_1_t7223_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7223_il2cpp_TypeInfo/* element_class */
	, IList_1_t7223_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7223_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7223_0_0_0/* byval_arg */
	, &IList_1_t7223_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7223_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t3046_il2cpp_TypeInfo;

// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Component>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Component>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Component>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Component>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Component>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Component>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Component>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Component>
extern MethodInfo ICollection_1_get_Count_m41133_MethodInfo;
static PropertyInfo ICollection_1_t3046____Count_PropertyInfo = 
{
	&ICollection_1_t3046_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41133_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41134_MethodInfo;
static PropertyInfo ICollection_1_t3046____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t3046_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41134_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t3046_PropertyInfos[] =
{
	&ICollection_1_t3046____Count_PropertyInfo,
	&ICollection_1_t3046____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41133_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Component>::get_Count()
MethodInfo ICollection_1_get_Count_m41133_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t3046_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41133_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41134_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Component>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41134_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t3046_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41134_GenericMethod/* genericMethod */

};
extern Il2CppType Component_t100_0_0_0;
extern Il2CppType Component_t100_0_0_0;
static ParameterInfo ICollection_1_t3046_ICollection_1_Add_m41135_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Component_t100_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41135_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Component>::Add(T)
MethodInfo ICollection_1_Add_m41135_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t3046_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t3046_ICollection_1_Add_m41135_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41135_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41136_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Component>::Clear()
MethodInfo ICollection_1_Clear_m41136_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t3046_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41136_GenericMethod/* genericMethod */

};
extern Il2CppType Component_t100_0_0_0;
static ParameterInfo ICollection_1_t3046_ICollection_1_Contains_m41137_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Component_t100_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41137_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Component>::Contains(T)
MethodInfo ICollection_1_Contains_m41137_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t3046_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t3046_ICollection_1_Contains_m41137_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41137_GenericMethod/* genericMethod */

};
extern Il2CppType ComponentU5BU5D_t3043_0_0_0;
extern Il2CppType ComponentU5BU5D_t3043_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t3046_ICollection_1_CopyTo_m41138_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ComponentU5BU5D_t3043_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41138_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Component>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41138_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t3046_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t3046_ICollection_1_CopyTo_m41138_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41138_GenericMethod/* genericMethod */

};
extern Il2CppType Component_t100_0_0_0;
static ParameterInfo ICollection_1_t3046_ICollection_1_Remove_m41139_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Component_t100_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41139_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Component>::Remove(T)
MethodInfo ICollection_1_Remove_m41139_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t3046_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t3046_ICollection_1_Remove_m41139_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41139_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t3046_MethodInfos[] =
{
	&ICollection_1_get_Count_m41133_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41134_MethodInfo,
	&ICollection_1_Add_m41135_MethodInfo,
	&ICollection_1_Clear_m41136_MethodInfo,
	&ICollection_1_Contains_m41137_MethodInfo,
	&ICollection_1_CopyTo_m41138_MethodInfo,
	&ICollection_1_Remove_m41139_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t3044_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t3046_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t3044_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t3046_0_0_0;
extern Il2CppType ICollection_1_t3046_1_0_0;
struct ICollection_1_t3046;
extern Il2CppGenericClass ICollection_1_t3046_GenericClass;
TypeInfo ICollection_1_t3046_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t3046_MethodInfos/* methods */
	, ICollection_1_t3046_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t3046_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t3046_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t3046_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t3046_0_0_0/* byval_arg */
	, &ICollection_1_t3046_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t3046_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Component>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Component>
extern Il2CppType IEnumerator_1_t3045_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41140_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Component>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41140_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t3044_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3045_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41140_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t3044_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41140_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t3044_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t3044_0_0_0;
extern Il2CppType IEnumerable_1_t3044_1_0_0;
struct IEnumerable_1_t3044;
extern Il2CppGenericClass IEnumerable_1_t3044_GenericClass;
TypeInfo IEnumerable_1_t3044_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t3044_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t3044_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t3044_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t3044_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t3044_0_0_0/* byval_arg */
	, &IEnumerable_1_t3044_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t3044_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t3045_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.Component>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Component>
extern MethodInfo IEnumerator_1_get_Current_m41141_MethodInfo;
static PropertyInfo IEnumerator_1_t3045____Current_PropertyInfo = 
{
	&IEnumerator_1_t3045_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41141_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t3045_PropertyInfos[] =
{
	&IEnumerator_1_t3045____Current_PropertyInfo,
	NULL
};
extern Il2CppType Component_t100_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41141_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Component>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41141_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t3045_il2cpp_TypeInfo/* declaring_type */
	, &Component_t100_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41141_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t3045_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41141_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t3045_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t3045_0_0_0;
extern Il2CppType IEnumerator_1_t3045_1_0_0;
struct IEnumerator_1_t3045;
extern Il2CppGenericClass IEnumerator_1_t3045_GenericClass;
TypeInfo IEnumerator_1_t3045_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t3045_MethodInfos/* methods */
	, IEnumerator_1_t3045_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t3045_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t3045_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t3045_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t3045_0_0_0/* byval_arg */
	, &IEnumerator_1_t3045_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t3045_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Component>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_3.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2701_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Component>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_3MethodDeclarations.h"

extern TypeInfo Component_t100_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14001_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisComponent_t100_m31480_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Component>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Component>(System.Int32)
#define Array_InternalArray__get_Item_TisComponent_t100_m31480(__this, p0, method) (Component_t100 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Component>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Component>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Component>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Component>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Component>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Component>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2701____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2701_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2701, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2701____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2701_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2701, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2701_FieldInfos[] =
{
	&InternalEnumerator_1_t2701____array_0_FieldInfo,
	&InternalEnumerator_1_t2701____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13998_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2701____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2701_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13998_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2701____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2701_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14001_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2701_PropertyInfos[] =
{
	&InternalEnumerator_1_t2701____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2701____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2701_InternalEnumerator_1__ctor_m13997_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m13997_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Component>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m13997_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2701_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2701_InternalEnumerator_1__ctor_m13997_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m13997_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13998_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Component>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13998_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2701_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13998_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m13999_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Component>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m13999_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2701_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m13999_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14000_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Component>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14000_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2701_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14000_GenericMethod/* genericMethod */

};
extern Il2CppType Component_t100_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14001_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Component>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14001_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2701_il2cpp_TypeInfo/* declaring_type */
	, &Component_t100_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14001_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2701_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m13997_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13998_MethodInfo,
	&InternalEnumerator_1_Dispose_m13999_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14000_MethodInfo,
	&InternalEnumerator_1_get_Current_m14001_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14000_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m13999_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2701_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13998_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14000_MethodInfo,
	&InternalEnumerator_1_Dispose_m13999_MethodInfo,
	&InternalEnumerator_1_get_Current_m14001_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2701_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t3045_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2701_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t3045_il2cpp_TypeInfo, 7},
};
extern TypeInfo Component_t100_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2701_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14001_MethodInfo/* Method Usage */,
	&Component_t100_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisComponent_t100_m31480_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2701_0_0_0;
extern Il2CppType InternalEnumerator_1_t2701_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2701_GenericClass;
TypeInfo InternalEnumerator_1_t2701_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2701_MethodInfos/* methods */
	, InternalEnumerator_1_t2701_PropertyInfos/* properties */
	, InternalEnumerator_1_t2701_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2701_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2701_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2701_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2701_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2701_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2701_1_0_0/* this_arg */
	, InternalEnumerator_1_t2701_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2701_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2701_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2701)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t3050_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Component>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Component>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Component>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Component>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Component>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Component>
extern MethodInfo IList_1_get_Item_m41142_MethodInfo;
extern MethodInfo IList_1_set_Item_m41143_MethodInfo;
static PropertyInfo IList_1_t3050____Item_PropertyInfo = 
{
	&IList_1_t3050_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41142_MethodInfo/* get */
	, &IList_1_set_Item_m41143_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t3050_PropertyInfos[] =
{
	&IList_1_t3050____Item_PropertyInfo,
	NULL
};
extern Il2CppType Component_t100_0_0_0;
static ParameterInfo IList_1_t3050_IList_1_IndexOf_m41144_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Component_t100_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41144_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Component>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41144_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t3050_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t3050_IList_1_IndexOf_m41144_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41144_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Component_t100_0_0_0;
static ParameterInfo IList_1_t3050_IList_1_Insert_m41145_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Component_t100_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41145_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Component>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41145_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t3050_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t3050_IList_1_Insert_m41145_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41145_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t3050_IList_1_RemoveAt_m41146_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41146_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Component>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41146_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t3050_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t3050_IList_1_RemoveAt_m41146_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41146_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t3050_IList_1_get_Item_m41142_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Component_t100_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41142_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Component>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41142_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t3050_il2cpp_TypeInfo/* declaring_type */
	, &Component_t100_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t3050_IList_1_get_Item_m41142_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41142_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Component_t100_0_0_0;
static ParameterInfo IList_1_t3050_IList_1_set_Item_m41143_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Component_t100_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41143_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Component>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41143_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t3050_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t3050_IList_1_set_Item_m41143_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41143_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t3050_MethodInfos[] =
{
	&IList_1_IndexOf_m41144_MethodInfo,
	&IList_1_Insert_m41145_MethodInfo,
	&IList_1_RemoveAt_m41146_MethodInfo,
	&IList_1_get_Item_m41142_MethodInfo,
	&IList_1_set_Item_m41143_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t3050_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t3046_il2cpp_TypeInfo,
	&IEnumerable_1_t3044_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t3050_0_0_0;
extern Il2CppType IList_1_t3050_1_0_0;
struct IList_1_t3050;
extern Il2CppGenericClass IList_1_t3050_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t3050_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t3050_MethodInfos/* methods */
	, IList_1_t3050_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t3050_il2cpp_TypeInfo/* element_class */
	, IList_1_t3050_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t3050_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t3050_0_0_0/* byval_arg */
	, &IList_1_t3050_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t3050_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7225_il2cpp_TypeInfo;

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Object>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Object>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Object>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Object>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Object>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Object>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Object>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Object>
extern MethodInfo ICollection_1_get_Count_m41147_MethodInfo;
static PropertyInfo ICollection_1_t7225____Count_PropertyInfo = 
{
	&ICollection_1_t7225_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41147_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41148_MethodInfo;
static PropertyInfo ICollection_1_t7225____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7225_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41148_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7225_PropertyInfos[] =
{
	&ICollection_1_t7225____Count_PropertyInfo,
	&ICollection_1_t7225____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41147_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Object>::get_Count()
MethodInfo ICollection_1_get_Count_m41147_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7225_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41147_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41148_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Object>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41148_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7225_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41148_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType Object_t117_0_0_0;
static ParameterInfo ICollection_1_t7225_ICollection_1_Add_m41149_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41149_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Object>::Add(T)
MethodInfo ICollection_1_Add_m41149_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7225_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7225_ICollection_1_Add_m41149_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41149_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41150_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Object>::Clear()
MethodInfo ICollection_1_Clear_m41150_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7225_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41150_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t117_0_0_0;
static ParameterInfo ICollection_1_t7225_ICollection_1_Contains_m41151_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41151_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Object>::Contains(T)
MethodInfo ICollection_1_Contains_m41151_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7225_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7225_ICollection_1_Contains_m41151_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41151_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t816_0_0_0;
extern Il2CppType ObjectU5BU5D_t816_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7225_ICollection_1_CopyTo_m41152_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t816_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41152_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Object>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41152_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7225_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7225_ICollection_1_CopyTo_m41152_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41152_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t117_0_0_0;
static ParameterInfo ICollection_1_t7225_ICollection_1_Remove_m41153_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41153_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Object>::Remove(T)
MethodInfo ICollection_1_Remove_m41153_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7225_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7225_ICollection_1_Remove_m41153_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41153_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7225_MethodInfos[] =
{
	&ICollection_1_get_Count_m41147_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41148_MethodInfo,
	&ICollection_1_Add_m41149_MethodInfo,
	&ICollection_1_Clear_m41150_MethodInfo,
	&ICollection_1_Contains_m41151_MethodInfo,
	&ICollection_1_CopyTo_m41152_MethodInfo,
	&ICollection_1_Remove_m41153_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7227_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7225_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7225_0_0_0;
extern Il2CppType ICollection_1_t7225_1_0_0;
struct ICollection_1_t7225;
extern Il2CppGenericClass ICollection_1_t7225_GenericClass;
TypeInfo ICollection_1_t7225_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7225_MethodInfos/* methods */
	, ICollection_1_t7225_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7225_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7225_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7225_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7225_0_0_0/* byval_arg */
	, &ICollection_1_t7225_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7225_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Object>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Object>
extern Il2CppType IEnumerator_1_t5702_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41154_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Object>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41154_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7227_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5702_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41154_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7227_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41154_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7227_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7227_0_0_0;
extern Il2CppType IEnumerable_1_t7227_1_0_0;
struct IEnumerable_1_t7227;
extern Il2CppGenericClass IEnumerable_1_t7227_GenericClass;
TypeInfo IEnumerable_1_t7227_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7227_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7227_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7227_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7227_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7227_0_0_0/* byval_arg */
	, &IEnumerable_1_t7227_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7227_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5702_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.Object>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Object>
extern MethodInfo IEnumerator_1_get_Current_m41155_MethodInfo;
static PropertyInfo IEnumerator_1_t5702____Current_PropertyInfo = 
{
	&IEnumerator_1_t5702_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41155_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5702_PropertyInfos[] =
{
	&IEnumerator_1_t5702____Current_PropertyInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41155_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Object>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41155_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5702_il2cpp_TypeInfo/* declaring_type */
	, &Object_t117_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41155_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5702_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41155_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5702_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5702_0_0_0;
extern Il2CppType IEnumerator_1_t5702_1_0_0;
struct IEnumerator_1_t5702;
extern Il2CppGenericClass IEnumerator_1_t5702_GenericClass;
TypeInfo IEnumerator_1_t5702_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5702_MethodInfos/* methods */
	, IEnumerator_1_t5702_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5702_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5702_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5702_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5702_0_0_0/* byval_arg */
	, &IEnumerator_1_t5702_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5702_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_4.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2702_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_4MethodDeclarations.h"

extern TypeInfo Object_t117_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14006_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Object>(System.Int32)
#define Array_InternalArray__get_Item_TisObject_t117_m31491(__this, p0, method) (Object_t117 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Object>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Object>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Object>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Object>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Object>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Object>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2702____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2702_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2702, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2702____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2702_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2702, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2702_FieldInfos[] =
{
	&InternalEnumerator_1_t2702____array_0_FieldInfo,
	&InternalEnumerator_1_t2702____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14003_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2702____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2702_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14003_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2702____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2702_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14006_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2702_PropertyInfos[] =
{
	&InternalEnumerator_1_t2702____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2702____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2702_InternalEnumerator_1__ctor_m14002_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14002_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Object>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14002_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2702_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2702_InternalEnumerator_1__ctor_m14002_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14002_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14003_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Object>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14003_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2702_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14003_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14004_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Object>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14004_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2702_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14004_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14005_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Object>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14005_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2702_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14005_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t117_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14006_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Object>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14006_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2702_il2cpp_TypeInfo/* declaring_type */
	, &Object_t117_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14006_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2702_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14002_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14003_MethodInfo,
	&InternalEnumerator_1_Dispose_m14004_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14005_MethodInfo,
	&InternalEnumerator_1_get_Current_m14006_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14005_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14004_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2702_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14003_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14005_MethodInfo,
	&InternalEnumerator_1_Dispose_m14004_MethodInfo,
	&InternalEnumerator_1_get_Current_m14006_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2702_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5702_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2702_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5702_il2cpp_TypeInfo, 7},
};
extern TypeInfo Object_t117_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2702_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14006_MethodInfo/* Method Usage */,
	&Object_t117_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisObject_t117_m31491_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2702_0_0_0;
extern Il2CppType InternalEnumerator_1_t2702_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2702_GenericClass;
TypeInfo InternalEnumerator_1_t2702_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2702_MethodInfos/* methods */
	, InternalEnumerator_1_t2702_PropertyInfos/* properties */
	, InternalEnumerator_1_t2702_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2702_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2702_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2702_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2702_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2702_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2702_1_0_0/* this_arg */
	, InternalEnumerator_1_t2702_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2702_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2702_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2702)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7226_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Object>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Object>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Object>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Object>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Object>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Object>
extern MethodInfo IList_1_get_Item_m41156_MethodInfo;
extern MethodInfo IList_1_set_Item_m41157_MethodInfo;
static PropertyInfo IList_1_t7226____Item_PropertyInfo = 
{
	&IList_1_t7226_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41156_MethodInfo/* get */
	, &IList_1_set_Item_m41157_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7226_PropertyInfos[] =
{
	&IList_1_t7226____Item_PropertyInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
static ParameterInfo IList_1_t7226_IList_1_IndexOf_m41158_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41158_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Object>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41158_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7226_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7226_IList_1_IndexOf_m41158_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41158_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Object_t117_0_0_0;
static ParameterInfo IList_1_t7226_IList_1_Insert_m41159_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41159_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Object>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41159_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7226_IList_1_Insert_m41159_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41159_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7226_IList_1_RemoveAt_m41160_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41160_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Object>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41160_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7226_IList_1_RemoveAt_m41160_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41160_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7226_IList_1_get_Item_m41156_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Object_t117_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41156_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Object>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41156_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7226_il2cpp_TypeInfo/* declaring_type */
	, &Object_t117_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7226_IList_1_get_Item_m41156_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41156_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Object_t117_0_0_0;
static ParameterInfo IList_1_t7226_IList_1_set_Item_m41157_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41157_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Object>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41157_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7226_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7226_IList_1_set_Item_m41157_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41157_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7226_MethodInfos[] =
{
	&IList_1_IndexOf_m41158_MethodInfo,
	&IList_1_Insert_m41159_MethodInfo,
	&IList_1_RemoveAt_m41160_MethodInfo,
	&IList_1_get_Item_m41156_MethodInfo,
	&IList_1_set_Item_m41157_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7226_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7225_il2cpp_TypeInfo,
	&IEnumerable_1_t7227_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7226_0_0_0;
extern Il2CppType IList_1_t7226_1_0_0;
struct IList_1_t7226;
extern Il2CppGenericClass IList_1_t7226_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7226_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7226_MethodInfos/* methods */
	, IList_1_t7226_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7226_il2cpp_TypeInfo/* element_class */
	, IList_1_t7226_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7226_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7226_0_0_0/* byval_arg */
	, &IList_1_t7226_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7226_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<ARVRModes>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_3.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2703_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<ARVRModes>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_3MethodDeclarations.h"

// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// UnityEngine.Events.InvokableCall`1<ARVRModes>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen.h"
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t2704_il2cpp_TypeInfo;
extern TypeInfo Void_t99_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<ARVRModes>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_genMethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14019_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14021_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<ARVRModes>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<ARVRModes>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<ARVRModes>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2703____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2703_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2703, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2703_FieldInfos[] =
{
	&CachedInvokableCall_1_t2703____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType ARVRModes_t5_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2703_CachedInvokableCall_1__ctor_m14007_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &ARVRModes_t5_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14007_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<ARVRModes>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14007_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2703_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2703_CachedInvokableCall_1__ctor_m14007_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14007_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2703_CachedInvokableCall_1_Invoke_m14009_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14009_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<ARVRModes>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14009_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2703_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2703_CachedInvokableCall_1_Invoke_m14009_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14009_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2703_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14007_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14009_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
extern MethodInfo Object_ToString_m306_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m14009_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14022_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2703_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14009_MethodInfo,
	&InvokableCall_1_Find_m14022_MethodInfo,
};
extern Il2CppType UnityAction_1_t2708_0_0_0;
extern TypeInfo UnityAction_1_t2708_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisARVRModes_t5_m31502_MethodInfo;
extern TypeInfo ARVRModes_t5_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14024_MethodInfo;
extern TypeInfo ARVRModes_t5_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2703_RGCTXData[8] = 
{
	&UnityAction_1_t2708_0_0_0/* Type Usage */,
	&UnityAction_1_t2708_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisARVRModes_t5_m31502_MethodInfo/* Method Usage */,
	&ARVRModes_t5_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14024_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14019_MethodInfo/* Method Usage */,
	&ARVRModes_t5_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14021_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2703_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2703_1_0_0;
struct CachedInvokableCall_1_t2703;
extern Il2CppGenericClass CachedInvokableCall_1_t2703_GenericClass;
TypeInfo CachedInvokableCall_1_t2703_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2703_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2703_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2704_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2703_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2703_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2703_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2703_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2703_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2703_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2703_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2703)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_4.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2705_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_4MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_0.h"
extern TypeInfo InvokableCall_1_t2706_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_0MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14011_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14013_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern MethodInfo CachedInvokableCall_1__ctor_m14008_MethodInfo;
 void CachedInvokableCall_1__ctor_m14008_gshared (CachedInvokableCall_1_t2705 * __this, Object_t117 * ___target, MethodInfo_t142 * ___theFunction, Object_t * ___argument, MethodInfo* method)
{
	{
		__this->___m_Arg1_1 = ((ObjectU5BU5D_t115*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t115_il2cpp_TypeInfo), 1));
		(( void (*) (InvokableCall_1_t2706 * __this, Object_t * p0, MethodInfo_t142 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(__this, ___target, ___theFunction, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		ObjectU5BU5D_t115* L_0 = (__this->___m_Arg1_1);
		Object_t * L_1 = ___argument;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, ((Object_t *)L_1));
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0)) = (Object_t *)((Object_t *)L_1);
		return;
	}
}
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::Invoke(System.Object[])
extern MethodInfo CachedInvokableCall_1_Invoke_m14010_MethodInfo;
 void CachedInvokableCall_1_Invoke_m14010_gshared (CachedInvokableCall_1_t2705 * __this, ObjectU5BU5D_t115* ___args, MethodInfo* method)
{
	{
		ObjectU5BU5D_t115* L_0 = (__this->___m_Arg1_1);
		(( void (*) (InvokableCall_1_t2706 * __this, ObjectU5BU5D_t115* p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7)->method)(__this, L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		return;
	}
}
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<System.Object>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2705____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2705_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2705, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2705_FieldInfos[] =
{
	&CachedInvokableCall_1_t2705____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2705_CachedInvokableCall_1__ctor_m14008_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14008_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14008_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2705_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2705_CachedInvokableCall_1__ctor_m14008_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14008_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2705_CachedInvokableCall_1_Invoke_m14010_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14010_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14010_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2705_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2705_CachedInvokableCall_1_Invoke_m14010_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14010_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2705_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14008_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14010_MethodInfo,
	NULL
};
extern MethodInfo InvokableCall_1_Find_m14014_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2705_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14010_MethodInfo,
	&InvokableCall_1_Find_m14014_MethodInfo,
};
extern Il2CppType UnityAction_1_t2707_0_0_0;
extern TypeInfo UnityAction_1_t2707_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_MethodInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14016_MethodInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2705_RGCTXData[8] = 
{
	&UnityAction_1_t2707_0_0_0/* Type Usage */,
	&UnityAction_1_t2707_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_MethodInfo/* Method Usage */,
	&Object_t_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14016_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14011_MethodInfo/* Method Usage */,
	&Object_t_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14013_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2705_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2705_1_0_0;
struct CachedInvokableCall_1_t2705;
extern Il2CppGenericClass CachedInvokableCall_1_t2705_GenericClass;
TypeInfo CachedInvokableCall_1_t2705_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2705_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2705_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2706_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2705_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2705_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2705_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2705_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2705_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2705_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2705_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2705)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_6.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t2707_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t507_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<System.Object>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_6MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6358_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m395_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m456_MethodInfo;
extern MethodInfo Delegate_Combine_m2149_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6357_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2468_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6359_MethodInfo;
extern MethodInfo Delegate_get_Target_m6536_MethodInfo;
extern MethodInfo Delegate_get_Method_m6534_MethodInfo;
struct BaseInvokableCall_t1075;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
 void InvokableCall_1__ctor_m14011_gshared (InvokableCall_1_t2706 * __this, Object_t * ___target, MethodInfo_t142 * ___theFunction, MethodInfo* method)
{
	{
		BaseInvokableCall__ctor_m6358(__this, ___target, ___theFunction, /*hidden argument*/&BaseInvokableCall__ctor_m6358_MethodInfo);
		UnityAction_1_t2707 * L_0 = (__this->___Delegate_0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		Delegate_t153 * L_2 = Delegate_CreateDelegate_m456(NULL /*static, unused*/, L_1, ___target, ___theFunction, /*hidden argument*/&Delegate_CreateDelegate_m456_MethodInfo);
		Delegate_t153 * L_3 = Delegate_Combine_m2149(NULL /*static, unused*/, L_0, ((UnityAction_1_t2707 *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1))), /*hidden argument*/&Delegate_Combine_m2149_MethodInfo);
		__this->___Delegate_0 = ((UnityAction_1_t2707 *)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern MethodInfo InvokableCall_1__ctor_m14012_MethodInfo;
 void InvokableCall_1__ctor_m14012_gshared (InvokableCall_1_t2706 * __this, UnityAction_1_t2707 * ___callback, MethodInfo* method)
{
	{
		BaseInvokableCall__ctor_m6357(__this, /*hidden argument*/&BaseInvokableCall__ctor_m6357_MethodInfo);
		UnityAction_1_t2707 * L_0 = (__this->___Delegate_0);
		Delegate_t153 * L_1 = Delegate_Combine_m2149(NULL /*static, unused*/, L_0, ___callback, /*hidden argument*/&Delegate_Combine_m2149_MethodInfo);
		__this->___Delegate_0 = ((UnityAction_1_t2707 *)Castclass(L_1, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)));
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
 void InvokableCall_1_Invoke_m14013_gshared (InvokableCall_1_t2706 * __this, ObjectU5BU5D_t115* ___args, MethodInfo* method)
{
	{
		NullCheck(___args);
		if ((((int32_t)(((int32_t)(((Array_t *)___args)->max_length)))) == ((int32_t)1)))
		{
			goto IL_0014;
		}
	}
	{
		ArgumentException_t507 * L_0 = (ArgumentException_t507 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t507_il2cpp_TypeInfo));
		ArgumentException__ctor_m2468(L_0, (String_t*) &_stringLiteral440, /*hidden argument*/&ArgumentException__ctor_m2468_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0014:
	{
		NullCheck(___args);
		IL2CPP_ARRAY_BOUNDS_CHECK(___args, 0);
		int32_t L_1 = 0;
		(( void (*) (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(NULL /*static, unused*/, (*(Object_t **)(Object_t **)SZArrayLdElema(___args, L_1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		UnityAction_1_t2707 * L_2 = (__this->___Delegate_0);
		bool L_3 = BaseInvokableCall_AllowInvoke_m6359(NULL /*static, unused*/, L_2, /*hidden argument*/&BaseInvokableCall_AllowInvoke_m6359_MethodInfo);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		UnityAction_1_t2707 * L_4 = (__this->___Delegate_0);
		NullCheck(___args);
		IL2CPP_ARRAY_BOUNDS_CHECK(___args, 0);
		int32_t L_5 = 0;
		NullCheck(L_4);
		VirtActionInvoker1< Object_t * >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4), L_4, ((Object_t *)Castclass((*(Object_t **)(Object_t **)SZArrayLdElema(___args, L_5)), IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3))));
	}

IL_003f:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Object>::Find(System.Object,System.Reflection.MethodInfo)
 bool InvokableCall_1_Find_m14014_gshared (InvokableCall_1_t2706 * __this, Object_t * ___targetObj, MethodInfo_t142 * ___method, MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UnityAction_1_t2707 * L_0 = (__this->___Delegate_0);
		NullCheck(L_0);
		Object_t * L_1 = Delegate_get_Target_m6536(L_0, /*hidden argument*/&Delegate_get_Target_m6536_MethodInfo);
		if ((((Object_t *)L_1) != ((Object_t *)___targetObj)))
		{
			goto IL_0021;
		}
	}
	{
		UnityAction_1_t2707 * L_2 = (__this->___Delegate_0);
		NullCheck(L_2);
		MethodInfo_t142 * L_3 = Delegate_get_Method_m6534(L_2, /*hidden argument*/&Delegate_get_Method_m6534_MethodInfo);
		G_B3_0 = ((((MethodInfo_t142 *)L_3) == ((MethodInfo_t142 *)___method))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 0;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// Metadata Definition UnityEngine.Events.InvokableCall`1<System.Object>
extern Il2CppType UnityAction_1_t2707_0_0_1;
FieldInfo InvokableCall_1_t2706____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2707_0_0_1/* type */
	, &InvokableCall_1_t2706_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2706, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2706_FieldInfos[] =
{
	&InvokableCall_1_t2706____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2706_InvokableCall_1__ctor_m14011_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14011_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14011_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2706_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2706_InvokableCall_1__ctor_m14011_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14011_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2707_0_0_0;
static ParameterInfo InvokableCall_1_t2706_InvokableCall_1__ctor_m14012_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2707_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14012_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14012_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2706_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2706_InvokableCall_1__ctor_m14012_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14012_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2706_InvokableCall_1_Invoke_m14013_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14013_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14013_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2706_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2706_InvokableCall_1_Invoke_m14013_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14013_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2706_InvokableCall_1_Find_m14014_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14014_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Object>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14014_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2706_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2706_InvokableCall_1_Find_m14014_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14014_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2706_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14011_MethodInfo,
	&InvokableCall_1__ctor_m14012_MethodInfo,
	&InvokableCall_1_Invoke_m14013_MethodInfo,
	&InvokableCall_1_Find_m14014_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2706_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14013_MethodInfo,
	&InvokableCall_1_Find_m14014_MethodInfo,
};
extern TypeInfo UnityAction_1_t2707_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2706_RGCTXData[5] = 
{
	&UnityAction_1_t2707_0_0_0/* Type Usage */,
	&UnityAction_1_t2707_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_MethodInfo/* Method Usage */,
	&Object_t_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14016_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2706_0_0_0;
extern Il2CppType InvokableCall_1_t2706_1_0_0;
extern TypeInfo BaseInvokableCall_t1075_il2cpp_TypeInfo;
struct InvokableCall_1_t2706;
extern Il2CppGenericClass InvokableCall_1_t2706_GenericClass;
TypeInfo InvokableCall_1_t2706_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2706_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2706_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2706_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2706_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2706_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2706_0_0_0/* byval_arg */
	, &InvokableCall_1_t2706_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2706_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2706_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2706)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern MethodInfo UnityAction_1__ctor_m14015_MethodInfo;
 void UnityAction_1__ctor_m14015_gshared (UnityAction_1_t2707 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
 void UnityAction_1_Invoke_m14016_gshared (UnityAction_1_t2707 * __this, Object_t * ___arg0, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		UnityAction_1_Invoke_m14016((UnityAction_1_t2707 *)__this->___prev_9,___arg0, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___arg0, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg0,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___arg0, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg0,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___arg0,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Object>::BeginInvoke(T0,System.AsyncCallback,System.Object)
extern MethodInfo UnityAction_1_BeginInvoke_m14017_MethodInfo;
 Object_t * UnityAction_1_BeginInvoke_m14017_gshared (UnityAction_1_t2707 * __this, Object_t * ___arg0, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg0;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::EndInvoke(System.IAsyncResult)
extern MethodInfo UnityAction_1_EndInvoke_m14018_MethodInfo;
 void UnityAction_1_EndInvoke_m14018_gshared (UnityAction_1_t2707 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Metadata Definition UnityEngine.Events.UnityAction`1<System.Object>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2707_UnityAction_1__ctor_m14015_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14015_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14015_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2707_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2707_UnityAction_1__ctor_m14015_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14015_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2707_UnityAction_1_Invoke_m14016_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14016_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14016_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2707_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2707_UnityAction_1_Invoke_m14016_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14016_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2707_UnityAction_1_BeginInvoke_m14017_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14017_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Object>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14017_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2707_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2707_UnityAction_1_BeginInvoke_m14017_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14017_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2707_UnityAction_1_EndInvoke_m14018_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14018_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14018_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2707_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2707_UnityAction_1_EndInvoke_m14018_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14018_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2707_MethodInfos[] =
{
	&UnityAction_1__ctor_m14015_MethodInfo,
	&UnityAction_1_Invoke_m14016_MethodInfo,
	&UnityAction_1_BeginInvoke_m14017_MethodInfo,
	&UnityAction_1_EndInvoke_m14018_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2241_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2242_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2243_MethodInfo;
extern MethodInfo Delegate_Clone_m2244_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2245_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2246_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2247_MethodInfo;
static MethodInfo* UnityAction_1_t2707_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14016_MethodInfo,
	&UnityAction_1_BeginInvoke_m14017_MethodInfo,
	&UnityAction_1_EndInvoke_m14018_MethodInfo,
};
extern TypeInfo ICloneable_t481_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t482_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t2707_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2707_1_0_0;
extern TypeInfo MulticastDelegate_t325_il2cpp_TypeInfo;
struct UnityAction_1_t2707;
extern Il2CppGenericClass UnityAction_1_t2707_GenericClass;
TypeInfo UnityAction_1_t2707_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2707_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2707_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2707_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2707_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2707_0_0_0/* byval_arg */
	, &UnityAction_1_t2707_1_0_0/* this_arg */
	, UnityAction_1_t2707_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2707_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2707)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<ARVRModes>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_7.h"
extern TypeInfo UnityAction_1_t2708_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<ARVRModes>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_7MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<ARVRModes>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<ARVRModes>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisARVRModes_t5_m31502(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<ARVRModes>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<ARVRModes>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<ARVRModes>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<ARVRModes>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<ARVRModes>
extern Il2CppType UnityAction_1_t2708_0_0_1;
FieldInfo InvokableCall_1_t2704____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2708_0_0_1/* type */
	, &InvokableCall_1_t2704_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2704, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2704_FieldInfos[] =
{
	&InvokableCall_1_t2704____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2704_InvokableCall_1__ctor_m14019_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14019_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<ARVRModes>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14019_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2704_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2704_InvokableCall_1__ctor_m14019_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14019_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2708_0_0_0;
static ParameterInfo InvokableCall_1_t2704_InvokableCall_1__ctor_m14020_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2708_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14020_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<ARVRModes>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14020_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2704_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2704_InvokableCall_1__ctor_m14020_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14020_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2704_InvokableCall_1_Invoke_m14021_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14021_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<ARVRModes>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14021_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2704_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2704_InvokableCall_1_Invoke_m14021_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14021_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2704_InvokableCall_1_Find_m14022_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14022_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<ARVRModes>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14022_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2704_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2704_InvokableCall_1_Find_m14022_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14022_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2704_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14019_MethodInfo,
	&InvokableCall_1__ctor_m14020_MethodInfo,
	&InvokableCall_1_Invoke_m14021_MethodInfo,
	&InvokableCall_1_Find_m14022_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2704_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14021_MethodInfo,
	&InvokableCall_1_Find_m14022_MethodInfo,
};
extern TypeInfo UnityAction_1_t2708_il2cpp_TypeInfo;
extern TypeInfo ARVRModes_t5_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2704_RGCTXData[5] = 
{
	&UnityAction_1_t2708_0_0_0/* Type Usage */,
	&UnityAction_1_t2708_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisARVRModes_t5_m31502_MethodInfo/* Method Usage */,
	&ARVRModes_t5_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14024_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2704_0_0_0;
extern Il2CppType InvokableCall_1_t2704_1_0_0;
struct InvokableCall_1_t2704;
extern Il2CppGenericClass InvokableCall_1_t2704_GenericClass;
TypeInfo InvokableCall_1_t2704_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2704_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2704_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2704_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2704_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2704_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2704_0_0_0/* byval_arg */
	, &InvokableCall_1_t2704_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2704_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2704_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2704)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<ARVRModes>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<ARVRModes>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<ARVRModes>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<ARVRModes>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<ARVRModes>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2708_UnityAction_1__ctor_m14023_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14023_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<ARVRModes>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14023_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2708_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2708_UnityAction_1__ctor_m14023_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14023_GenericMethod/* genericMethod */

};
extern Il2CppType ARVRModes_t5_0_0_0;
static ParameterInfo UnityAction_1_t2708_UnityAction_1_Invoke_m14024_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ARVRModes_t5_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14024_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<ARVRModes>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14024_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2708_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2708_UnityAction_1_Invoke_m14024_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14024_GenericMethod/* genericMethod */

};
extern Il2CppType ARVRModes_t5_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2708_UnityAction_1_BeginInvoke_m14025_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &ARVRModes_t5_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14025_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<ARVRModes>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14025_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2708_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2708_UnityAction_1_BeginInvoke_m14025_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14025_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2708_UnityAction_1_EndInvoke_m14026_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14026_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<ARVRModes>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14026_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2708_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2708_UnityAction_1_EndInvoke_m14026_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14026_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2708_MethodInfos[] =
{
	&UnityAction_1__ctor_m14023_MethodInfo,
	&UnityAction_1_Invoke_m14024_MethodInfo,
	&UnityAction_1_BeginInvoke_m14025_MethodInfo,
	&UnityAction_1_EndInvoke_m14026_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14025_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14026_MethodInfo;
static MethodInfo* UnityAction_1_t2708_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14024_MethodInfo,
	&UnityAction_1_BeginInvoke_m14025_MethodInfo,
	&UnityAction_1_EndInvoke_m14026_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2708_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2708_1_0_0;
struct UnityAction_1_t2708;
extern Il2CppGenericClass UnityAction_1_t2708_GenericClass;
TypeInfo UnityAction_1_t2708_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2708_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2708_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2708_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2708_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2708_0_0_0/* byval_arg */
	, &UnityAction_1_t2708_1_0_0/* this_arg */
	, UnityAction_1_t2708_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2708_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2708)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5704_il2cpp_TypeInfo;

// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentMode.h"


// T System.Collections.Generic.IEnumerator`1<ARVRModes/TheCurrentMode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<ARVRModes/TheCurrentMode>
extern MethodInfo IEnumerator_1_get_Current_m41161_MethodInfo;
static PropertyInfo IEnumerator_1_t5704____Current_PropertyInfo = 
{
	&IEnumerator_1_t5704_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41161_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5704_PropertyInfos[] =
{
	&IEnumerator_1_t5704____Current_PropertyInfo,
	NULL
};
extern Il2CppType TheCurrentMode_t1_0_0_0;
extern void* RuntimeInvoker_TheCurrentMode_t1 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41161_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<ARVRModes/TheCurrentMode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41161_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5704_il2cpp_TypeInfo/* declaring_type */
	, &TheCurrentMode_t1_0_0_0/* return_type */
	, RuntimeInvoker_TheCurrentMode_t1/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41161_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5704_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41161_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5704_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5704_0_0_0;
extern Il2CppType IEnumerator_1_t5704_1_0_0;
struct IEnumerator_1_t5704;
extern Il2CppGenericClass IEnumerator_1_t5704_GenericClass;
TypeInfo IEnumerator_1_t5704_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5704_MethodInfos/* methods */
	, IEnumerator_1_t5704_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5704_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5704_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5704_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5704_0_0_0/* byval_arg */
	, &IEnumerator_1_t5704_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5704_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_5.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2709_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_5MethodDeclarations.h"

extern TypeInfo TheCurrentMode_t1_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14031_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTheCurrentMode_t1_m31504_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<ARVRModes/TheCurrentMode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<ARVRModes/TheCurrentMode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTheCurrentMode_t1_m31504 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m14027_MethodInfo;
 void InternalEnumerator_1__ctor_m14027 (InternalEnumerator_1_t2709 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14028_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14028 (InternalEnumerator_1_t2709 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m14031(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m14031_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&TheCurrentMode_t1_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m14029_MethodInfo;
 void InternalEnumerator_1_Dispose_m14029 (InternalEnumerator_1_t2709 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m14030_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m14030 (InternalEnumerator_1_t2709 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m14031 (InternalEnumerator_1_t2709 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisTheCurrentMode_t1_m31504(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisTheCurrentMode_t1_m31504_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2709____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2709_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2709, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2709____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2709_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2709, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2709_FieldInfos[] =
{
	&InternalEnumerator_1_t2709____array_0_FieldInfo,
	&InternalEnumerator_1_t2709____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t2709____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2709_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14028_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2709____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2709_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14031_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2709_PropertyInfos[] =
{
	&InternalEnumerator_1_t2709____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2709____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2709_InternalEnumerator_1__ctor_m14027_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14027_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14027_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m14027/* method */
	, &InternalEnumerator_1_t2709_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2709_InternalEnumerator_1__ctor_m14027_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14027_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14028_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14028_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14028/* method */
	, &InternalEnumerator_1_t2709_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14028_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14029_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14029_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m14029/* method */
	, &InternalEnumerator_1_t2709_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14029_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14030_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14030_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m14030/* method */
	, &InternalEnumerator_1_t2709_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14030_GenericMethod/* genericMethod */

};
extern Il2CppType TheCurrentMode_t1_0_0_0;
extern void* RuntimeInvoker_TheCurrentMode_t1 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14031_GenericMethod;
// T System.Array/InternalEnumerator`1<ARVRModes/TheCurrentMode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14031_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m14031/* method */
	, &InternalEnumerator_1_t2709_il2cpp_TypeInfo/* declaring_type */
	, &TheCurrentMode_t1_0_0_0/* return_type */
	, RuntimeInvoker_TheCurrentMode_t1/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14031_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2709_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14027_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14028_MethodInfo,
	&InternalEnumerator_1_Dispose_m14029_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14030_MethodInfo,
	&InternalEnumerator_1_get_Current_m14031_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t2709_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14028_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14030_MethodInfo,
	&InternalEnumerator_1_Dispose_m14029_MethodInfo,
	&InternalEnumerator_1_get_Current_m14031_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2709_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5704_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2709_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5704_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2709_0_0_0;
extern Il2CppType InternalEnumerator_1_t2709_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2709_GenericClass;
TypeInfo InternalEnumerator_1_t2709_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2709_MethodInfos/* methods */
	, InternalEnumerator_1_t2709_PropertyInfos/* properties */
	, InternalEnumerator_1_t2709_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2709_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2709_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2709_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2709_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2709_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2709_1_0_0/* this_arg */
	, InternalEnumerator_1_t2709_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2709_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2709)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7228_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>
extern MethodInfo ICollection_1_get_Count_m41162_MethodInfo;
static PropertyInfo ICollection_1_t7228____Count_PropertyInfo = 
{
	&ICollection_1_t7228_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41162_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41163_MethodInfo;
static PropertyInfo ICollection_1_t7228____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7228_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41163_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7228_PropertyInfos[] =
{
	&ICollection_1_t7228____Count_PropertyInfo,
	&ICollection_1_t7228____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41162_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::get_Count()
MethodInfo ICollection_1_get_Count_m41162_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7228_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41162_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41163_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41163_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7228_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41163_GenericMethod/* genericMethod */

};
extern Il2CppType TheCurrentMode_t1_0_0_0;
extern Il2CppType TheCurrentMode_t1_0_0_0;
static ParameterInfo ICollection_1_t7228_ICollection_1_Add_m41164_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TheCurrentMode_t1_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41164_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::Add(T)
MethodInfo ICollection_1_Add_m41164_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7228_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7228_ICollection_1_Add_m41164_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41164_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41165_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::Clear()
MethodInfo ICollection_1_Clear_m41165_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7228_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41165_GenericMethod/* genericMethod */

};
extern Il2CppType TheCurrentMode_t1_0_0_0;
static ParameterInfo ICollection_1_t7228_ICollection_1_Contains_m41166_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TheCurrentMode_t1_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41166_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::Contains(T)
MethodInfo ICollection_1_Contains_m41166_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7228_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7228_ICollection_1_Contains_m41166_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41166_GenericMethod/* genericMethod */

};
extern Il2CppType TheCurrentModeU5BU5D_t5158_0_0_0;
extern Il2CppType TheCurrentModeU5BU5D_t5158_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7228_ICollection_1_CopyTo_m41167_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TheCurrentModeU5BU5D_t5158_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41167_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41167_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7228_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7228_ICollection_1_CopyTo_m41167_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41167_GenericMethod/* genericMethod */

};
extern Il2CppType TheCurrentMode_t1_0_0_0;
static ParameterInfo ICollection_1_t7228_ICollection_1_Remove_m41168_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TheCurrentMode_t1_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41168_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<ARVRModes/TheCurrentMode>::Remove(T)
MethodInfo ICollection_1_Remove_m41168_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7228_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7228_ICollection_1_Remove_m41168_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41168_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7228_MethodInfos[] =
{
	&ICollection_1_get_Count_m41162_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41163_MethodInfo,
	&ICollection_1_Add_m41164_MethodInfo,
	&ICollection_1_Clear_m41165_MethodInfo,
	&ICollection_1_Contains_m41166_MethodInfo,
	&ICollection_1_CopyTo_m41167_MethodInfo,
	&ICollection_1_Remove_m41168_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7230_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7228_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7230_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7228_0_0_0;
extern Il2CppType ICollection_1_t7228_1_0_0;
struct ICollection_1_t7228;
extern Il2CppGenericClass ICollection_1_t7228_GenericClass;
TypeInfo ICollection_1_t7228_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7228_MethodInfos/* methods */
	, ICollection_1_t7228_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7228_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7228_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7228_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7228_0_0_0/* byval_arg */
	, &ICollection_1_t7228_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7228_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ARVRModes/TheCurrentMode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<ARVRModes/TheCurrentMode>
extern Il2CppType IEnumerator_1_t5704_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41169_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<ARVRModes/TheCurrentMode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41169_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7230_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5704_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41169_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7230_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41169_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7230_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7230_0_0_0;
extern Il2CppType IEnumerable_1_t7230_1_0_0;
struct IEnumerable_1_t7230;
extern Il2CppGenericClass IEnumerable_1_t7230_GenericClass;
TypeInfo IEnumerable_1_t7230_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7230_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7230_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7230_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7230_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7230_0_0_0/* byval_arg */
	, &IEnumerable_1_t7230_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7230_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7229_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<ARVRModes/TheCurrentMode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<ARVRModes/TheCurrentMode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<ARVRModes/TheCurrentMode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<ARVRModes/TheCurrentMode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<ARVRModes/TheCurrentMode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<ARVRModes/TheCurrentMode>
extern MethodInfo IList_1_get_Item_m41170_MethodInfo;
extern MethodInfo IList_1_set_Item_m41171_MethodInfo;
static PropertyInfo IList_1_t7229____Item_PropertyInfo = 
{
	&IList_1_t7229_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41170_MethodInfo/* get */
	, &IList_1_set_Item_m41171_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7229_PropertyInfos[] =
{
	&IList_1_t7229____Item_PropertyInfo,
	NULL
};
extern Il2CppType TheCurrentMode_t1_0_0_0;
static ParameterInfo IList_1_t7229_IList_1_IndexOf_m41172_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TheCurrentMode_t1_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41172_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<ARVRModes/TheCurrentMode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41172_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7229_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7229_IList_1_IndexOf_m41172_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41172_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TheCurrentMode_t1_0_0_0;
static ParameterInfo IList_1_t7229_IList_1_Insert_m41173_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TheCurrentMode_t1_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41173_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ARVRModes/TheCurrentMode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41173_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7229_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7229_IList_1_Insert_m41173_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41173_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7229_IList_1_RemoveAt_m41174_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41174_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ARVRModes/TheCurrentMode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41174_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7229_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7229_IList_1_RemoveAt_m41174_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41174_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7229_IList_1_get_Item_m41170_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType TheCurrentMode_t1_0_0_0;
extern void* RuntimeInvoker_TheCurrentMode_t1_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41170_GenericMethod;
// T System.Collections.Generic.IList`1<ARVRModes/TheCurrentMode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41170_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7229_il2cpp_TypeInfo/* declaring_type */
	, &TheCurrentMode_t1_0_0_0/* return_type */
	, RuntimeInvoker_TheCurrentMode_t1_Int32_t93/* invoker_method */
	, IList_1_t7229_IList_1_get_Item_m41170_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41170_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TheCurrentMode_t1_0_0_0;
static ParameterInfo IList_1_t7229_IList_1_set_Item_m41171_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TheCurrentMode_t1_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41171_GenericMethod;
// System.Void System.Collections.Generic.IList`1<ARVRModes/TheCurrentMode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41171_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7229_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7229_IList_1_set_Item_m41171_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41171_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7229_MethodInfos[] =
{
	&IList_1_IndexOf_m41172_MethodInfo,
	&IList_1_Insert_m41173_MethodInfo,
	&IList_1_RemoveAt_m41174_MethodInfo,
	&IList_1_get_Item_m41170_MethodInfo,
	&IList_1_set_Item_m41171_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7229_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7228_il2cpp_TypeInfo,
	&IEnumerable_1_t7230_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7229_0_0_0;
extern Il2CppType IList_1_t7229_1_0_0;
struct IList_1_t7229;
extern Il2CppGenericClass IList_1_t7229_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7229_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7229_MethodInfos/* methods */
	, IList_1_t7229_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7229_il2cpp_TypeInfo/* element_class */
	, IList_1_t7229_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7229_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7229_0_0_0/* byval_arg */
	, &IList_1_t7229_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7229_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7231_il2cpp_TypeInfo;

// System.Enum
#include "mscorlib_System_Enum.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Enum>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Enum>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Enum>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Enum>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Enum>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Enum>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Enum>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Enum>
extern MethodInfo ICollection_1_get_Count_m41175_MethodInfo;
static PropertyInfo ICollection_1_t7231____Count_PropertyInfo = 
{
	&ICollection_1_t7231_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41175_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41176_MethodInfo;
static PropertyInfo ICollection_1_t7231____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7231_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41176_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7231_PropertyInfos[] =
{
	&ICollection_1_t7231____Count_PropertyInfo,
	&ICollection_1_t7231____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41175_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Enum>::get_Count()
MethodInfo ICollection_1_get_Count_m41175_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7231_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41175_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41176_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Enum>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41176_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7231_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41176_GenericMethod/* genericMethod */

};
extern Il2CppType Enum_t97_0_0_0;
extern Il2CppType Enum_t97_0_0_0;
static ParameterInfo ICollection_1_t7231_ICollection_1_Add_m41177_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Enum_t97_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41177_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Enum>::Add(T)
MethodInfo ICollection_1_Add_m41177_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7231_ICollection_1_Add_m41177_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41177_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41178_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Enum>::Clear()
MethodInfo ICollection_1_Clear_m41178_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41178_GenericMethod/* genericMethod */

};
extern Il2CppType Enum_t97_0_0_0;
static ParameterInfo ICollection_1_t7231_ICollection_1_Contains_m41179_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Enum_t97_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41179_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Enum>::Contains(T)
MethodInfo ICollection_1_Contains_m41179_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7231_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7231_ICollection_1_Contains_m41179_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41179_GenericMethod/* genericMethod */

};
extern Il2CppType EnumU5BU5D_t5201_0_0_0;
extern Il2CppType EnumU5BU5D_t5201_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7231_ICollection_1_CopyTo_m41180_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EnumU5BU5D_t5201_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41180_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Enum>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41180_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7231_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7231_ICollection_1_CopyTo_m41180_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41180_GenericMethod/* genericMethod */

};
extern Il2CppType Enum_t97_0_0_0;
static ParameterInfo ICollection_1_t7231_ICollection_1_Remove_m41181_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Enum_t97_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41181_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Enum>::Remove(T)
MethodInfo ICollection_1_Remove_m41181_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7231_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7231_ICollection_1_Remove_m41181_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41181_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7231_MethodInfos[] =
{
	&ICollection_1_get_Count_m41175_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41176_MethodInfo,
	&ICollection_1_Add_m41177_MethodInfo,
	&ICollection_1_Clear_m41178_MethodInfo,
	&ICollection_1_Contains_m41179_MethodInfo,
	&ICollection_1_CopyTo_m41180_MethodInfo,
	&ICollection_1_Remove_m41181_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7233_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7231_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7231_0_0_0;
extern Il2CppType ICollection_1_t7231_1_0_0;
struct ICollection_1_t7231;
extern Il2CppGenericClass ICollection_1_t7231_GenericClass;
TypeInfo ICollection_1_t7231_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7231_MethodInfos/* methods */
	, ICollection_1_t7231_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7231_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7231_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7231_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7231_0_0_0/* byval_arg */
	, &ICollection_1_t7231_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7231_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Enum>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Enum>
extern Il2CppType IEnumerator_1_t5706_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41182_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Enum>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41182_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7233_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5706_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41182_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7233_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41182_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7233_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7233_0_0_0;
extern Il2CppType IEnumerable_1_t7233_1_0_0;
struct IEnumerable_1_t7233;
extern Il2CppGenericClass IEnumerable_1_t7233_GenericClass;
TypeInfo IEnumerable_1_t7233_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7233_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7233_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7233_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7233_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7233_0_0_0/* byval_arg */
	, &IEnumerable_1_t7233_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7233_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5706_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Enum>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Enum>
extern MethodInfo IEnumerator_1_get_Current_m41183_MethodInfo;
static PropertyInfo IEnumerator_1_t5706____Current_PropertyInfo = 
{
	&IEnumerator_1_t5706_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41183_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5706_PropertyInfos[] =
{
	&IEnumerator_1_t5706____Current_PropertyInfo,
	NULL
};
extern Il2CppType Enum_t97_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41183_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Enum>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41183_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5706_il2cpp_TypeInfo/* declaring_type */
	, &Enum_t97_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41183_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5706_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41183_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5706_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5706_0_0_0;
extern Il2CppType IEnumerator_1_t5706_1_0_0;
struct IEnumerator_1_t5706;
extern Il2CppGenericClass IEnumerator_1_t5706_GenericClass;
TypeInfo IEnumerator_1_t5706_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5706_MethodInfos/* methods */
	, IEnumerator_1_t5706_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5706_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5706_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5706_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5706_0_0_0/* byval_arg */
	, &IEnumerator_1_t5706_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5706_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Enum>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_6.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2710_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Enum>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_6MethodDeclarations.h"

extern TypeInfo Enum_t97_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14036_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Enum>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Enum>(System.Int32)
#define Array_InternalArray__get_Item_TisEnum_t97_m31515(__this, p0, method) (Enum_t97 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Enum>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Enum>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Enum>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Enum>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Enum>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Enum>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2710____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2710_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2710, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2710____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2710_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2710, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2710_FieldInfos[] =
{
	&InternalEnumerator_1_t2710____array_0_FieldInfo,
	&InternalEnumerator_1_t2710____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14033_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2710____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2710_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14033_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2710____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2710_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14036_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2710_PropertyInfos[] =
{
	&InternalEnumerator_1_t2710____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2710____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2710_InternalEnumerator_1__ctor_m14032_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14032_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Enum>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14032_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2710_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2710_InternalEnumerator_1__ctor_m14032_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14032_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14033_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Enum>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14033_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2710_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14033_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14034_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Enum>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14034_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2710_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14034_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14035_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Enum>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14035_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2710_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14035_GenericMethod/* genericMethod */

};
extern Il2CppType Enum_t97_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14036_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Enum>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14036_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2710_il2cpp_TypeInfo/* declaring_type */
	, &Enum_t97_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14036_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2710_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14032_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14033_MethodInfo,
	&InternalEnumerator_1_Dispose_m14034_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14035_MethodInfo,
	&InternalEnumerator_1_get_Current_m14036_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14035_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14034_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2710_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14033_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14035_MethodInfo,
	&InternalEnumerator_1_Dispose_m14034_MethodInfo,
	&InternalEnumerator_1_get_Current_m14036_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2710_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5706_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2710_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5706_il2cpp_TypeInfo, 7},
};
extern TypeInfo Enum_t97_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2710_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14036_MethodInfo/* Method Usage */,
	&Enum_t97_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisEnum_t97_m31515_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2710_0_0_0;
extern Il2CppType InternalEnumerator_1_t2710_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2710_GenericClass;
TypeInfo InternalEnumerator_1_t2710_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2710_MethodInfos/* methods */
	, InternalEnumerator_1_t2710_PropertyInfos/* properties */
	, InternalEnumerator_1_t2710_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2710_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2710_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2710_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2710_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2710_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2710_1_0_0/* this_arg */
	, InternalEnumerator_1_t2710_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2710_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2710_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2710)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7232_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Enum>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Enum>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Enum>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Enum>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Enum>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Enum>
extern MethodInfo IList_1_get_Item_m41184_MethodInfo;
extern MethodInfo IList_1_set_Item_m41185_MethodInfo;
static PropertyInfo IList_1_t7232____Item_PropertyInfo = 
{
	&IList_1_t7232_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41184_MethodInfo/* get */
	, &IList_1_set_Item_m41185_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7232_PropertyInfos[] =
{
	&IList_1_t7232____Item_PropertyInfo,
	NULL
};
extern Il2CppType Enum_t97_0_0_0;
static ParameterInfo IList_1_t7232_IList_1_IndexOf_m41186_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Enum_t97_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41186_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Enum>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41186_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7232_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7232_IList_1_IndexOf_m41186_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41186_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Enum_t97_0_0_0;
static ParameterInfo IList_1_t7232_IList_1_Insert_m41187_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Enum_t97_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41187_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Enum>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41187_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7232_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7232_IList_1_Insert_m41187_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41187_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7232_IList_1_RemoveAt_m41188_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41188_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Enum>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41188_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7232_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7232_IList_1_RemoveAt_m41188_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41188_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7232_IList_1_get_Item_m41184_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Enum_t97_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41184_GenericMethod;
// T System.Collections.Generic.IList`1<System.Enum>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41184_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7232_il2cpp_TypeInfo/* declaring_type */
	, &Enum_t97_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7232_IList_1_get_Item_m41184_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41184_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Enum_t97_0_0_0;
static ParameterInfo IList_1_t7232_IList_1_set_Item_m41185_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Enum_t97_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41185_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Enum>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41185_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7232_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7232_IList_1_set_Item_m41185_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41185_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7232_MethodInfos[] =
{
	&IList_1_IndexOf_m41186_MethodInfo,
	&IList_1_Insert_m41187_MethodInfo,
	&IList_1_RemoveAt_m41188_MethodInfo,
	&IList_1_get_Item_m41184_MethodInfo,
	&IList_1_set_Item_m41185_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7232_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7231_il2cpp_TypeInfo,
	&IEnumerable_1_t7233_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7232_0_0_0;
extern Il2CppType IList_1_t7232_1_0_0;
struct IList_1_t7232;
extern Il2CppGenericClass IList_1_t7232_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7232_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7232_MethodInfos/* methods */
	, IList_1_t7232_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7232_il2cpp_TypeInfo/* element_class */
	, IList_1_t7232_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7232_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7232_0_0_0/* byval_arg */
	, &IList_1_t7232_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7232_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7234_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IFormattable>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IFormattable>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IFormattable>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IFormattable>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IFormattable>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IFormattable>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IFormattable>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IFormattable>
extern MethodInfo ICollection_1_get_Count_m41189_MethodInfo;
static PropertyInfo ICollection_1_t7234____Count_PropertyInfo = 
{
	&ICollection_1_t7234_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41189_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41190_MethodInfo;
static PropertyInfo ICollection_1_t7234____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7234_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41190_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7234_PropertyInfos[] =
{
	&ICollection_1_t7234____Count_PropertyInfo,
	&ICollection_1_t7234____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41189_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IFormattable>::get_Count()
MethodInfo ICollection_1_get_Count_m41189_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7234_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41189_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41190_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IFormattable>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41190_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7234_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41190_GenericMethod/* genericMethod */

};
extern Il2CppType IFormattable_t94_0_0_0;
extern Il2CppType IFormattable_t94_0_0_0;
static ParameterInfo ICollection_1_t7234_ICollection_1_Add_m41191_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IFormattable_t94_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41191_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IFormattable>::Add(T)
MethodInfo ICollection_1_Add_m41191_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7234_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7234_ICollection_1_Add_m41191_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41191_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41192_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IFormattable>::Clear()
MethodInfo ICollection_1_Clear_m41192_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7234_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41192_GenericMethod/* genericMethod */

};
extern Il2CppType IFormattable_t94_0_0_0;
static ParameterInfo ICollection_1_t7234_ICollection_1_Contains_m41193_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IFormattable_t94_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41193_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IFormattable>::Contains(T)
MethodInfo ICollection_1_Contains_m41193_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7234_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7234_ICollection_1_Contains_m41193_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41193_GenericMethod/* genericMethod */

};
extern Il2CppType IFormattableU5BU5D_t5202_0_0_0;
extern Il2CppType IFormattableU5BU5D_t5202_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7234_ICollection_1_CopyTo_m41194_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IFormattableU5BU5D_t5202_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41194_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IFormattable>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41194_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7234_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7234_ICollection_1_CopyTo_m41194_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41194_GenericMethod/* genericMethod */

};
extern Il2CppType IFormattable_t94_0_0_0;
static ParameterInfo ICollection_1_t7234_ICollection_1_Remove_m41195_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IFormattable_t94_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41195_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IFormattable>::Remove(T)
MethodInfo ICollection_1_Remove_m41195_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7234_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7234_ICollection_1_Remove_m41195_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41195_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7234_MethodInfos[] =
{
	&ICollection_1_get_Count_m41189_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41190_MethodInfo,
	&ICollection_1_Add_m41191_MethodInfo,
	&ICollection_1_Clear_m41192_MethodInfo,
	&ICollection_1_Contains_m41193_MethodInfo,
	&ICollection_1_CopyTo_m41194_MethodInfo,
	&ICollection_1_Remove_m41195_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7236_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7234_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7234_0_0_0;
extern Il2CppType ICollection_1_t7234_1_0_0;
struct ICollection_1_t7234;
extern Il2CppGenericClass ICollection_1_t7234_GenericClass;
TypeInfo ICollection_1_t7234_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7234_MethodInfos/* methods */
	, ICollection_1_t7234_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7234_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7234_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7234_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7234_0_0_0/* byval_arg */
	, &ICollection_1_t7234_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7234_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IFormattable>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IFormattable>
extern Il2CppType IEnumerator_1_t5708_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41196_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IFormattable>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41196_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7236_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5708_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41196_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7236_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41196_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7236_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7236_0_0_0;
extern Il2CppType IEnumerable_1_t7236_1_0_0;
struct IEnumerable_1_t7236;
extern Il2CppGenericClass IEnumerable_1_t7236_GenericClass;
TypeInfo IEnumerable_1_t7236_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7236_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7236_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7236_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7236_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7236_0_0_0/* byval_arg */
	, &IEnumerable_1_t7236_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7236_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5708_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IFormattable>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IFormattable>
extern MethodInfo IEnumerator_1_get_Current_m41197_MethodInfo;
static PropertyInfo IEnumerator_1_t5708____Current_PropertyInfo = 
{
	&IEnumerator_1_t5708_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41197_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5708_PropertyInfos[] =
{
	&IEnumerator_1_t5708____Current_PropertyInfo,
	NULL
};
extern Il2CppType IFormattable_t94_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41197_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IFormattable>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41197_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5708_il2cpp_TypeInfo/* declaring_type */
	, &IFormattable_t94_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41197_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5708_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41197_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5708_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5708_0_0_0;
extern Il2CppType IEnumerator_1_t5708_1_0_0;
struct IEnumerator_1_t5708;
extern Il2CppGenericClass IEnumerator_1_t5708_GenericClass;
TypeInfo IEnumerator_1_t5708_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5708_MethodInfos/* methods */
	, IEnumerator_1_t5708_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5708_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5708_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5708_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5708_0_0_0/* byval_arg */
	, &IEnumerator_1_t5708_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5708_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IFormattable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_7.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2711_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IFormattable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_7MethodDeclarations.h"

extern TypeInfo IFormattable_t94_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14041_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IFormattable>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IFormattable>(System.Int32)
#define Array_InternalArray__get_Item_TisIFormattable_t94_m31526(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IFormattable>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IFormattable>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IFormattable>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IFormattable>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IFormattable>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IFormattable>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2711____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2711_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2711, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2711____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2711_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2711, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2711_FieldInfos[] =
{
	&InternalEnumerator_1_t2711____array_0_FieldInfo,
	&InternalEnumerator_1_t2711____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14038_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2711____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2711_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14038_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2711____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2711_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14041_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2711_PropertyInfos[] =
{
	&InternalEnumerator_1_t2711____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2711____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2711_InternalEnumerator_1__ctor_m14037_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14037_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IFormattable>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14037_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2711_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2711_InternalEnumerator_1__ctor_m14037_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14037_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14038_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IFormattable>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14038_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2711_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14038_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14039_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IFormattable>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14039_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2711_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14039_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14040_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IFormattable>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14040_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2711_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14040_GenericMethod/* genericMethod */

};
extern Il2CppType IFormattable_t94_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14041_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IFormattable>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14041_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2711_il2cpp_TypeInfo/* declaring_type */
	, &IFormattable_t94_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14041_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2711_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14037_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14038_MethodInfo,
	&InternalEnumerator_1_Dispose_m14039_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14040_MethodInfo,
	&InternalEnumerator_1_get_Current_m14041_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14040_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14039_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2711_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14038_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14040_MethodInfo,
	&InternalEnumerator_1_Dispose_m14039_MethodInfo,
	&InternalEnumerator_1_get_Current_m14041_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2711_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5708_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2711_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5708_il2cpp_TypeInfo, 7},
};
extern TypeInfo IFormattable_t94_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2711_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14041_MethodInfo/* Method Usage */,
	&IFormattable_t94_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIFormattable_t94_m31526_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2711_0_0_0;
extern Il2CppType InternalEnumerator_1_t2711_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2711_GenericClass;
TypeInfo InternalEnumerator_1_t2711_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2711_MethodInfos/* methods */
	, InternalEnumerator_1_t2711_PropertyInfos/* properties */
	, InternalEnumerator_1_t2711_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2711_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2711_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2711_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2711_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2711_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2711_1_0_0/* this_arg */
	, InternalEnumerator_1_t2711_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2711_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2711_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2711)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7235_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IFormattable>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IFormattable>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IFormattable>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IFormattable>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IFormattable>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IFormattable>
extern MethodInfo IList_1_get_Item_m41198_MethodInfo;
extern MethodInfo IList_1_set_Item_m41199_MethodInfo;
static PropertyInfo IList_1_t7235____Item_PropertyInfo = 
{
	&IList_1_t7235_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41198_MethodInfo/* get */
	, &IList_1_set_Item_m41199_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7235_PropertyInfos[] =
{
	&IList_1_t7235____Item_PropertyInfo,
	NULL
};
extern Il2CppType IFormattable_t94_0_0_0;
static ParameterInfo IList_1_t7235_IList_1_IndexOf_m41200_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IFormattable_t94_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41200_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IFormattable>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41200_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7235_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7235_IList_1_IndexOf_m41200_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41200_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IFormattable_t94_0_0_0;
static ParameterInfo IList_1_t7235_IList_1_Insert_m41201_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IFormattable_t94_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41201_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IFormattable>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41201_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7235_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7235_IList_1_Insert_m41201_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41201_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7235_IList_1_RemoveAt_m41202_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41202_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IFormattable>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41202_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7235_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7235_IList_1_RemoveAt_m41202_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41202_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7235_IList_1_get_Item_m41198_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IFormattable_t94_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41198_GenericMethod;
// T System.Collections.Generic.IList`1<System.IFormattable>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41198_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7235_il2cpp_TypeInfo/* declaring_type */
	, &IFormattable_t94_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7235_IList_1_get_Item_m41198_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41198_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IFormattable_t94_0_0_0;
static ParameterInfo IList_1_t7235_IList_1_set_Item_m41199_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IFormattable_t94_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41199_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IFormattable>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41199_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7235_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7235_IList_1_set_Item_m41199_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41199_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7235_MethodInfos[] =
{
	&IList_1_IndexOf_m41200_MethodInfo,
	&IList_1_Insert_m41201_MethodInfo,
	&IList_1_RemoveAt_m41202_MethodInfo,
	&IList_1_get_Item_m41198_MethodInfo,
	&IList_1_set_Item_m41199_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7235_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7234_il2cpp_TypeInfo,
	&IEnumerable_1_t7236_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7235_0_0_0;
extern Il2CppType IList_1_t7235_1_0_0;
struct IList_1_t7235;
extern Il2CppGenericClass IList_1_t7235_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7235_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7235_MethodInfos/* methods */
	, IList_1_t7235_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7235_il2cpp_TypeInfo/* element_class */
	, IList_1_t7235_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7235_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7235_0_0_0/* byval_arg */
	, &IList_1_t7235_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7235_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7237_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IConvertible>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IConvertible>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IConvertible>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IConvertible>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IConvertible>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IConvertible>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IConvertible>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IConvertible>
extern MethodInfo ICollection_1_get_Count_m41203_MethodInfo;
static PropertyInfo ICollection_1_t7237____Count_PropertyInfo = 
{
	&ICollection_1_t7237_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41203_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41204_MethodInfo;
static PropertyInfo ICollection_1_t7237____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7237_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41204_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7237_PropertyInfos[] =
{
	&ICollection_1_t7237____Count_PropertyInfo,
	&ICollection_1_t7237____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41203_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IConvertible>::get_Count()
MethodInfo ICollection_1_get_Count_m41203_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7237_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41203_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41204_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IConvertible>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41204_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7237_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41204_GenericMethod/* genericMethod */

};
extern Il2CppType IConvertible_t95_0_0_0;
extern Il2CppType IConvertible_t95_0_0_0;
static ParameterInfo ICollection_1_t7237_ICollection_1_Add_m41205_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IConvertible_t95_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41205_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IConvertible>::Add(T)
MethodInfo ICollection_1_Add_m41205_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7237_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7237_ICollection_1_Add_m41205_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41205_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41206_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IConvertible>::Clear()
MethodInfo ICollection_1_Clear_m41206_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7237_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41206_GenericMethod/* genericMethod */

};
extern Il2CppType IConvertible_t95_0_0_0;
static ParameterInfo ICollection_1_t7237_ICollection_1_Contains_m41207_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IConvertible_t95_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41207_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IConvertible>::Contains(T)
MethodInfo ICollection_1_Contains_m41207_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7237_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7237_ICollection_1_Contains_m41207_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41207_GenericMethod/* genericMethod */

};
extern Il2CppType IConvertibleU5BU5D_t5203_0_0_0;
extern Il2CppType IConvertibleU5BU5D_t5203_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7237_ICollection_1_CopyTo_m41208_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IConvertibleU5BU5D_t5203_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41208_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IConvertible>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41208_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7237_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7237_ICollection_1_CopyTo_m41208_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41208_GenericMethod/* genericMethod */

};
extern Il2CppType IConvertible_t95_0_0_0;
static ParameterInfo ICollection_1_t7237_ICollection_1_Remove_m41209_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IConvertible_t95_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41209_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IConvertible>::Remove(T)
MethodInfo ICollection_1_Remove_m41209_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7237_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7237_ICollection_1_Remove_m41209_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41209_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7237_MethodInfos[] =
{
	&ICollection_1_get_Count_m41203_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41204_MethodInfo,
	&ICollection_1_Add_m41205_MethodInfo,
	&ICollection_1_Clear_m41206_MethodInfo,
	&ICollection_1_Contains_m41207_MethodInfo,
	&ICollection_1_CopyTo_m41208_MethodInfo,
	&ICollection_1_Remove_m41209_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7239_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7237_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7237_0_0_0;
extern Il2CppType ICollection_1_t7237_1_0_0;
struct ICollection_1_t7237;
extern Il2CppGenericClass ICollection_1_t7237_GenericClass;
TypeInfo ICollection_1_t7237_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7237_MethodInfos/* methods */
	, ICollection_1_t7237_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7237_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7237_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7237_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7237_0_0_0/* byval_arg */
	, &ICollection_1_t7237_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7237_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IConvertible>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IConvertible>
extern Il2CppType IEnumerator_1_t5710_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41210_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IConvertible>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41210_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7239_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5710_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41210_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7239_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41210_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7239_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7239_0_0_0;
extern Il2CppType IEnumerable_1_t7239_1_0_0;
struct IEnumerable_1_t7239;
extern Il2CppGenericClass IEnumerable_1_t7239_GenericClass;
TypeInfo IEnumerable_1_t7239_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7239_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7239_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7239_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7239_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7239_0_0_0/* byval_arg */
	, &IEnumerable_1_t7239_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7239_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5710_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IConvertible>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IConvertible>
extern MethodInfo IEnumerator_1_get_Current_m41211_MethodInfo;
static PropertyInfo IEnumerator_1_t5710____Current_PropertyInfo = 
{
	&IEnumerator_1_t5710_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41211_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5710_PropertyInfos[] =
{
	&IEnumerator_1_t5710____Current_PropertyInfo,
	NULL
};
extern Il2CppType IConvertible_t95_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41211_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IConvertible>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41211_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5710_il2cpp_TypeInfo/* declaring_type */
	, &IConvertible_t95_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41211_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5710_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41211_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5710_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5710_0_0_0;
extern Il2CppType IEnumerator_1_t5710_1_0_0;
struct IEnumerator_1_t5710;
extern Il2CppGenericClass IEnumerator_1_t5710_GenericClass;
TypeInfo IEnumerator_1_t5710_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5710_MethodInfos/* methods */
	, IEnumerator_1_t5710_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5710_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5710_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5710_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5710_0_0_0/* byval_arg */
	, &IEnumerator_1_t5710_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5710_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IConvertible>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_8.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2712_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IConvertible>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_8MethodDeclarations.h"

extern TypeInfo IConvertible_t95_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14046_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IConvertible>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IConvertible>(System.Int32)
#define Array_InternalArray__get_Item_TisIConvertible_t95_m31537(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IConvertible>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IConvertible>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IConvertible>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IConvertible>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IConvertible>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IConvertible>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2712____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2712_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2712, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2712____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2712_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2712, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2712_FieldInfos[] =
{
	&InternalEnumerator_1_t2712____array_0_FieldInfo,
	&InternalEnumerator_1_t2712____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14043_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2712____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2712_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14043_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2712____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2712_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14046_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2712_PropertyInfos[] =
{
	&InternalEnumerator_1_t2712____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2712____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2712_InternalEnumerator_1__ctor_m14042_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14042_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IConvertible>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14042_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2712_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2712_InternalEnumerator_1__ctor_m14042_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14042_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14043_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IConvertible>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14043_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2712_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14043_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14044_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IConvertible>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14044_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2712_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14044_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14045_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IConvertible>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14045_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2712_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14045_GenericMethod/* genericMethod */

};
extern Il2CppType IConvertible_t95_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14046_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IConvertible>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14046_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2712_il2cpp_TypeInfo/* declaring_type */
	, &IConvertible_t95_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14046_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2712_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14042_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14043_MethodInfo,
	&InternalEnumerator_1_Dispose_m14044_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14045_MethodInfo,
	&InternalEnumerator_1_get_Current_m14046_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14045_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14044_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2712_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14043_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14045_MethodInfo,
	&InternalEnumerator_1_Dispose_m14044_MethodInfo,
	&InternalEnumerator_1_get_Current_m14046_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2712_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5710_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2712_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5710_il2cpp_TypeInfo, 7},
};
extern TypeInfo IConvertible_t95_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2712_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14046_MethodInfo/* Method Usage */,
	&IConvertible_t95_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIConvertible_t95_m31537_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2712_0_0_0;
extern Il2CppType InternalEnumerator_1_t2712_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2712_GenericClass;
TypeInfo InternalEnumerator_1_t2712_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2712_MethodInfos/* methods */
	, InternalEnumerator_1_t2712_PropertyInfos/* properties */
	, InternalEnumerator_1_t2712_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2712_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2712_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2712_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2712_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2712_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2712_1_0_0/* this_arg */
	, InternalEnumerator_1_t2712_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2712_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2712_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2712)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7238_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IConvertible>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IConvertible>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IConvertible>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IConvertible>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IConvertible>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IConvertible>
extern MethodInfo IList_1_get_Item_m41212_MethodInfo;
extern MethodInfo IList_1_set_Item_m41213_MethodInfo;
static PropertyInfo IList_1_t7238____Item_PropertyInfo = 
{
	&IList_1_t7238_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41212_MethodInfo/* get */
	, &IList_1_set_Item_m41213_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7238_PropertyInfos[] =
{
	&IList_1_t7238____Item_PropertyInfo,
	NULL
};
extern Il2CppType IConvertible_t95_0_0_0;
static ParameterInfo IList_1_t7238_IList_1_IndexOf_m41214_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IConvertible_t95_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41214_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IConvertible>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41214_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7238_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7238_IList_1_IndexOf_m41214_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41214_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IConvertible_t95_0_0_0;
static ParameterInfo IList_1_t7238_IList_1_Insert_m41215_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IConvertible_t95_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41215_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IConvertible>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41215_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7238_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7238_IList_1_Insert_m41215_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41215_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7238_IList_1_RemoveAt_m41216_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41216_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IConvertible>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41216_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7238_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7238_IList_1_RemoveAt_m41216_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41216_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7238_IList_1_get_Item_m41212_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IConvertible_t95_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41212_GenericMethod;
// T System.Collections.Generic.IList`1<System.IConvertible>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41212_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7238_il2cpp_TypeInfo/* declaring_type */
	, &IConvertible_t95_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7238_IList_1_get_Item_m41212_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41212_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IConvertible_t95_0_0_0;
static ParameterInfo IList_1_t7238_IList_1_set_Item_m41213_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IConvertible_t95_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41213_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IConvertible>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41213_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7238_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7238_IList_1_set_Item_m41213_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41213_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7238_MethodInfos[] =
{
	&IList_1_IndexOf_m41214_MethodInfo,
	&IList_1_Insert_m41215_MethodInfo,
	&IList_1_RemoveAt_m41216_MethodInfo,
	&IList_1_get_Item_m41212_MethodInfo,
	&IList_1_set_Item_m41213_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7238_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7237_il2cpp_TypeInfo,
	&IEnumerable_1_t7239_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7238_0_0_0;
extern Il2CppType IList_1_t7238_1_0_0;
struct IList_1_t7238;
extern Il2CppGenericClass IList_1_t7238_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7238_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7238_MethodInfos/* methods */
	, IList_1_t7238_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7238_il2cpp_TypeInfo/* element_class */
	, IList_1_t7238_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7238_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7238_0_0_0/* byval_arg */
	, &IList_1_t7238_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7238_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7240_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IComparable>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable>
extern MethodInfo ICollection_1_get_Count_m41217_MethodInfo;
static PropertyInfo ICollection_1_t7240____Count_PropertyInfo = 
{
	&ICollection_1_t7240_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41217_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41218_MethodInfo;
static PropertyInfo ICollection_1_t7240____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7240_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41218_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7240_PropertyInfos[] =
{
	&ICollection_1_t7240____Count_PropertyInfo,
	&ICollection_1_t7240____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41217_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable>::get_Count()
MethodInfo ICollection_1_get_Count_m41217_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7240_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41217_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41218_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41218_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7240_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41218_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_t96_0_0_0;
extern Il2CppType IComparable_t96_0_0_0;
static ParameterInfo ICollection_1_t7240_ICollection_1_Add_m41219_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_t96_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41219_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable>::Add(T)
MethodInfo ICollection_1_Add_m41219_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7240_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7240_ICollection_1_Add_m41219_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41219_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41220_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable>::Clear()
MethodInfo ICollection_1_Clear_m41220_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7240_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41220_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_t96_0_0_0;
static ParameterInfo ICollection_1_t7240_ICollection_1_Contains_m41221_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_t96_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41221_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable>::Contains(T)
MethodInfo ICollection_1_Contains_m41221_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7240_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7240_ICollection_1_Contains_m41221_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41221_GenericMethod/* genericMethod */

};
extern Il2CppType IComparableU5BU5D_t5204_0_0_0;
extern Il2CppType IComparableU5BU5D_t5204_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7240_ICollection_1_CopyTo_m41222_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IComparableU5BU5D_t5204_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41222_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41222_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7240_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7240_ICollection_1_CopyTo_m41222_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41222_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_t96_0_0_0;
static ParameterInfo ICollection_1_t7240_ICollection_1_Remove_m41223_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_t96_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41223_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable>::Remove(T)
MethodInfo ICollection_1_Remove_m41223_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7240_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7240_ICollection_1_Remove_m41223_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41223_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7240_MethodInfos[] =
{
	&ICollection_1_get_Count_m41217_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41218_MethodInfo,
	&ICollection_1_Add_m41219_MethodInfo,
	&ICollection_1_Clear_m41220_MethodInfo,
	&ICollection_1_Contains_m41221_MethodInfo,
	&ICollection_1_CopyTo_m41222_MethodInfo,
	&ICollection_1_Remove_m41223_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7242_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7240_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7240_0_0_0;
extern Il2CppType ICollection_1_t7240_1_0_0;
struct ICollection_1_t7240;
extern Il2CppGenericClass ICollection_1_t7240_GenericClass;
TypeInfo ICollection_1_t7240_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7240_MethodInfos/* methods */
	, ICollection_1_t7240_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7240_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7240_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7240_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7240_0_0_0/* byval_arg */
	, &ICollection_1_t7240_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7240_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable>
extern Il2CppType IEnumerator_1_t5712_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41224_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41224_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7242_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5712_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41224_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7242_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41224_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7242_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7242_0_0_0;
extern Il2CppType IEnumerable_1_t7242_1_0_0;
struct IEnumerable_1_t7242;
extern Il2CppGenericClass IEnumerable_1_t7242_GenericClass;
TypeInfo IEnumerable_1_t7242_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7242_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7242_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7242_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7242_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7242_0_0_0/* byval_arg */
	, &IEnumerable_1_t7242_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7242_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5712_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IComparable>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable>
extern MethodInfo IEnumerator_1_get_Current_m41225_MethodInfo;
static PropertyInfo IEnumerator_1_t5712____Current_PropertyInfo = 
{
	&IEnumerator_1_t5712_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41225_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5712_PropertyInfos[] =
{
	&IEnumerator_1_t5712____Current_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_t96_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41225_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IComparable>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41225_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5712_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_t96_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41225_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5712_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41225_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5712_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5712_0_0_0;
extern Il2CppType IEnumerator_1_t5712_1_0_0;
struct IEnumerator_1_t5712;
extern Il2CppGenericClass IEnumerator_1_t5712_GenericClass;
TypeInfo IEnumerator_1_t5712_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5712_MethodInfos/* methods */
	, IEnumerator_1_t5712_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5712_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5712_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5712_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5712_0_0_0/* byval_arg */
	, &IEnumerator_1_t5712_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5712_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IComparable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_9.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2713_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IComparable>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_9MethodDeclarations.h"

extern TypeInfo IComparable_t96_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14051_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IComparable>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IComparable>(System.Int32)
#define Array_InternalArray__get_Item_TisIComparable_t96_m31548(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IComparable>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IComparable>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IComparable>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IComparable>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2713____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2713_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2713, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2713____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2713_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2713, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2713_FieldInfos[] =
{
	&InternalEnumerator_1_t2713____array_0_FieldInfo,
	&InternalEnumerator_1_t2713____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14048_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2713____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2713_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14048_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2713____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2713_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14051_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2713_PropertyInfos[] =
{
	&InternalEnumerator_1_t2713____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2713____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2713_InternalEnumerator_1__ctor_m14047_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14047_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14047_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2713_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2713_InternalEnumerator_1__ctor_m14047_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14047_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14048_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IComparable>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14048_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2713_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14048_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14049_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14049_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2713_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14049_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14050_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14050_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2713_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14050_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_t96_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14051_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IComparable>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14051_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2713_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_t96_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14051_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2713_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14047_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14048_MethodInfo,
	&InternalEnumerator_1_Dispose_m14049_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14050_MethodInfo,
	&InternalEnumerator_1_get_Current_m14051_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14050_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14049_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2713_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14048_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14050_MethodInfo,
	&InternalEnumerator_1_Dispose_m14049_MethodInfo,
	&InternalEnumerator_1_get_Current_m14051_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2713_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5712_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2713_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5712_il2cpp_TypeInfo, 7},
};
extern TypeInfo IComparable_t96_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2713_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14051_MethodInfo/* Method Usage */,
	&IComparable_t96_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIComparable_t96_m31548_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2713_0_0_0;
extern Il2CppType InternalEnumerator_1_t2713_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2713_GenericClass;
TypeInfo InternalEnumerator_1_t2713_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2713_MethodInfos/* methods */
	, InternalEnumerator_1_t2713_PropertyInfos/* properties */
	, InternalEnumerator_1_t2713_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2713_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2713_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2713_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2713_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2713_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2713_1_0_0/* this_arg */
	, InternalEnumerator_1_t2713_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2713_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2713_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2713)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7241_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IComparable>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IComparable>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IComparable>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IComparable>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IComparable>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IComparable>
extern MethodInfo IList_1_get_Item_m41226_MethodInfo;
extern MethodInfo IList_1_set_Item_m41227_MethodInfo;
static PropertyInfo IList_1_t7241____Item_PropertyInfo = 
{
	&IList_1_t7241_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41226_MethodInfo/* get */
	, &IList_1_set_Item_m41227_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7241_PropertyInfos[] =
{
	&IList_1_t7241____Item_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_t96_0_0_0;
static ParameterInfo IList_1_t7241_IList_1_IndexOf_m41228_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_t96_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41228_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IComparable>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41228_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7241_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7241_IList_1_IndexOf_m41228_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41228_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IComparable_t96_0_0_0;
static ParameterInfo IList_1_t7241_IList_1_Insert_m41229_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_t96_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41229_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41229_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7241_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7241_IList_1_Insert_m41229_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41229_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7241_IList_1_RemoveAt_m41230_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41230_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41230_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7241_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7241_IList_1_RemoveAt_m41230_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41230_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7241_IList_1_get_Item_m41226_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IComparable_t96_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41226_GenericMethod;
// T System.Collections.Generic.IList`1<System.IComparable>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41226_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7241_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_t96_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7241_IList_1_get_Item_m41226_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41226_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IComparable_t96_0_0_0;
static ParameterInfo IList_1_t7241_IList_1_set_Item_m41227_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_t96_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41227_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41227_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7241_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7241_IList_1_set_Item_m41227_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41227_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7241_MethodInfos[] =
{
	&IList_1_IndexOf_m41228_MethodInfo,
	&IList_1_Insert_m41229_MethodInfo,
	&IList_1_RemoveAt_m41230_MethodInfo,
	&IList_1_get_Item_m41226_MethodInfo,
	&IList_1_set_Item_m41227_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7241_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7240_il2cpp_TypeInfo,
	&IEnumerable_1_t7242_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7241_0_0_0;
extern Il2CppType IList_1_t7241_1_0_0;
struct IList_1_t7241;
extern Il2CppGenericClass IList_1_t7241_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7241_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7241_MethodInfos/* methods */
	, IList_1_t7241_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7241_il2cpp_TypeInfo/* element_class */
	, IList_1_t7241_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7241_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7241_0_0_0/* byval_arg */
	, &IList_1_t7241_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7241_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7243_il2cpp_TypeInfo;

// System.ValueType
#include "mscorlib_System_ValueType.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.ValueType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.ValueType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.ValueType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.ValueType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.ValueType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.ValueType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.ValueType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.ValueType>
extern MethodInfo ICollection_1_get_Count_m41231_MethodInfo;
static PropertyInfo ICollection_1_t7243____Count_PropertyInfo = 
{
	&ICollection_1_t7243_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41231_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41232_MethodInfo;
static PropertyInfo ICollection_1_t7243____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7243_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41232_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7243_PropertyInfos[] =
{
	&ICollection_1_t7243____Count_PropertyInfo,
	&ICollection_1_t7243____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41231_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.ValueType>::get_Count()
MethodInfo ICollection_1_get_Count_m41231_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7243_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41231_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41232_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ValueType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41232_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7243_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41232_GenericMethod/* genericMethod */

};
extern Il2CppType ValueType_t436_0_0_0;
extern Il2CppType ValueType_t436_0_0_0;
static ParameterInfo ICollection_1_t7243_ICollection_1_Add_m41233_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ValueType_t436_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41233_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ValueType>::Add(T)
MethodInfo ICollection_1_Add_m41233_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7243_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7243_ICollection_1_Add_m41233_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41233_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41234_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ValueType>::Clear()
MethodInfo ICollection_1_Clear_m41234_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7243_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41234_GenericMethod/* genericMethod */

};
extern Il2CppType ValueType_t436_0_0_0;
static ParameterInfo ICollection_1_t7243_ICollection_1_Contains_m41235_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ValueType_t436_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41235_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ValueType>::Contains(T)
MethodInfo ICollection_1_Contains_m41235_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7243_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7243_ICollection_1_Contains_m41235_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41235_GenericMethod/* genericMethod */

};
extern Il2CppType ValueTypeU5BU5D_t5205_0_0_0;
extern Il2CppType ValueTypeU5BU5D_t5205_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7243_ICollection_1_CopyTo_m41236_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ValueTypeU5BU5D_t5205_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41236_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ValueType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41236_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7243_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7243_ICollection_1_CopyTo_m41236_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41236_GenericMethod/* genericMethod */

};
extern Il2CppType ValueType_t436_0_0_0;
static ParameterInfo ICollection_1_t7243_ICollection_1_Remove_m41237_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ValueType_t436_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41237_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ValueType>::Remove(T)
MethodInfo ICollection_1_Remove_m41237_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7243_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7243_ICollection_1_Remove_m41237_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41237_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7243_MethodInfos[] =
{
	&ICollection_1_get_Count_m41231_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41232_MethodInfo,
	&ICollection_1_Add_m41233_MethodInfo,
	&ICollection_1_Clear_m41234_MethodInfo,
	&ICollection_1_Contains_m41235_MethodInfo,
	&ICollection_1_CopyTo_m41236_MethodInfo,
	&ICollection_1_Remove_m41237_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7245_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7243_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7243_0_0_0;
extern Il2CppType ICollection_1_t7243_1_0_0;
struct ICollection_1_t7243;
extern Il2CppGenericClass ICollection_1_t7243_GenericClass;
TypeInfo ICollection_1_t7243_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7243_MethodInfos/* methods */
	, ICollection_1_t7243_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7243_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7243_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7243_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7243_0_0_0/* byval_arg */
	, &ICollection_1_t7243_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7243_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ValueType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ValueType>
extern Il2CppType IEnumerator_1_t5714_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41238_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ValueType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41238_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7245_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5714_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41238_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7245_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41238_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7245_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7245_0_0_0;
extern Il2CppType IEnumerable_1_t7245_1_0_0;
struct IEnumerable_1_t7245;
extern Il2CppGenericClass IEnumerable_1_t7245_GenericClass;
TypeInfo IEnumerable_1_t7245_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7245_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7245_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7245_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7245_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7245_0_0_0/* byval_arg */
	, &IEnumerable_1_t7245_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7245_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5714_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.ValueType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ValueType>
extern MethodInfo IEnumerator_1_get_Current_m41239_MethodInfo;
static PropertyInfo IEnumerator_1_t5714____Current_PropertyInfo = 
{
	&IEnumerator_1_t5714_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41239_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5714_PropertyInfos[] =
{
	&IEnumerator_1_t5714____Current_PropertyInfo,
	NULL
};
extern Il2CppType ValueType_t436_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41239_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.ValueType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41239_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5714_il2cpp_TypeInfo/* declaring_type */
	, &ValueType_t436_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41239_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5714_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41239_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5714_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5714_0_0_0;
extern Il2CppType IEnumerator_1_t5714_1_0_0;
struct IEnumerator_1_t5714;
extern Il2CppGenericClass IEnumerator_1_t5714_GenericClass;
TypeInfo IEnumerator_1_t5714_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5714_MethodInfos/* methods */
	, IEnumerator_1_t5714_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5714_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5714_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5714_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5714_0_0_0/* byval_arg */
	, &IEnumerator_1_t5714_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5714_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.ValueType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_10.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2714_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.ValueType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_10MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m14056_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.ValueType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.ValueType>(System.Int32)
#define Array_InternalArray__get_Item_TisValueType_t436_m31559(__this, p0, method) (ValueType_t436 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.ValueType>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.ValueType>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.ValueType>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.ValueType>::MoveNext()
// T System.Array/InternalEnumerator`1<System.ValueType>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.ValueType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2714____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2714_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2714, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2714____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2714_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2714, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2714_FieldInfos[] =
{
	&InternalEnumerator_1_t2714____array_0_FieldInfo,
	&InternalEnumerator_1_t2714____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14053_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2714____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2714_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14053_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2714____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2714_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14056_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2714_PropertyInfos[] =
{
	&InternalEnumerator_1_t2714____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2714____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2714_InternalEnumerator_1__ctor_m14052_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14052_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ValueType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14052_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2714_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2714_InternalEnumerator_1__ctor_m14052_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14052_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14053_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.ValueType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14053_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2714_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14053_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14054_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ValueType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14054_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2714_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14054_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14055_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.ValueType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14055_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2714_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14055_GenericMethod/* genericMethod */

};
extern Il2CppType ValueType_t436_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14056_GenericMethod;
// T System.Array/InternalEnumerator`1<System.ValueType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14056_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2714_il2cpp_TypeInfo/* declaring_type */
	, &ValueType_t436_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14056_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2714_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14053_MethodInfo,
	&InternalEnumerator_1_Dispose_m14054_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14055_MethodInfo,
	&InternalEnumerator_1_get_Current_m14056_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14055_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14054_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2714_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14053_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14055_MethodInfo,
	&InternalEnumerator_1_Dispose_m14054_MethodInfo,
	&InternalEnumerator_1_get_Current_m14056_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2714_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5714_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2714_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5714_il2cpp_TypeInfo, 7},
};
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2714_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14056_MethodInfo/* Method Usage */,
	&ValueType_t436_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisValueType_t436_m31559_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2714_0_0_0;
extern Il2CppType InternalEnumerator_1_t2714_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2714_GenericClass;
TypeInfo InternalEnumerator_1_t2714_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2714_MethodInfos/* methods */
	, InternalEnumerator_1_t2714_PropertyInfos/* properties */
	, InternalEnumerator_1_t2714_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2714_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2714_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2714_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2714_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2714_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2714_1_0_0/* this_arg */
	, InternalEnumerator_1_t2714_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2714_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2714_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2714)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7244_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.ValueType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.ValueType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.ValueType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.ValueType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.ValueType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.ValueType>
extern MethodInfo IList_1_get_Item_m41240_MethodInfo;
extern MethodInfo IList_1_set_Item_m41241_MethodInfo;
static PropertyInfo IList_1_t7244____Item_PropertyInfo = 
{
	&IList_1_t7244_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41240_MethodInfo/* get */
	, &IList_1_set_Item_m41241_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7244_PropertyInfos[] =
{
	&IList_1_t7244____Item_PropertyInfo,
	NULL
};
extern Il2CppType ValueType_t436_0_0_0;
static ParameterInfo IList_1_t7244_IList_1_IndexOf_m41242_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ValueType_t436_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41242_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.ValueType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41242_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7244_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7244_IList_1_IndexOf_m41242_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41242_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ValueType_t436_0_0_0;
static ParameterInfo IList_1_t7244_IList_1_Insert_m41243_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ValueType_t436_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41243_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ValueType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41243_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7244_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7244_IList_1_Insert_m41243_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41243_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7244_IList_1_RemoveAt_m41244_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41244_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ValueType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41244_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7244_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7244_IList_1_RemoveAt_m41244_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41244_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7244_IList_1_get_Item_m41240_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ValueType_t436_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41240_GenericMethod;
// T System.Collections.Generic.IList`1<System.ValueType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41240_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7244_il2cpp_TypeInfo/* declaring_type */
	, &ValueType_t436_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7244_IList_1_get_Item_m41240_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41240_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ValueType_t436_0_0_0;
static ParameterInfo IList_1_t7244_IList_1_set_Item_m41241_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ValueType_t436_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41241_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ValueType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41241_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7244_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7244_IList_1_set_Item_m41241_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41241_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7244_MethodInfos[] =
{
	&IList_1_IndexOf_m41242_MethodInfo,
	&IList_1_Insert_m41243_MethodInfo,
	&IList_1_RemoveAt_m41244_MethodInfo,
	&IList_1_get_Item_m41240_MethodInfo,
	&IList_1_set_Item_m41241_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7244_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7243_il2cpp_TypeInfo,
	&IEnumerable_1_t7245_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7244_0_0_0;
extern Il2CppType IList_1_t7244_1_0_0;
struct IList_1_t7244;
extern Il2CppGenericClass IList_1_t7244_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7244_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7244_MethodInfos/* methods */
	, IList_1_t7244_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7244_il2cpp_TypeInfo/* element_class */
	, IList_1_t7244_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7244_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7244_0_0_0/* byval_arg */
	, &IList_1_t7244_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7244_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5716_il2cpp_TypeInfo;

// AnimateTexture
#include "AssemblyU2DCSharp_AnimateTexture.h"


// T System.Collections.Generic.IEnumerator`1<AnimateTexture>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<AnimateTexture>
extern MethodInfo IEnumerator_1_get_Current_m41245_MethodInfo;
static PropertyInfo IEnumerator_1_t5716____Current_PropertyInfo = 
{
	&IEnumerator_1_t5716_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41245_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5716_PropertyInfos[] =
{
	&IEnumerator_1_t5716____Current_PropertyInfo,
	NULL
};
extern Il2CppType AnimateTexture_t8_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41245_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<AnimateTexture>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41245_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5716_il2cpp_TypeInfo/* declaring_type */
	, &AnimateTexture_t8_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41245_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5716_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41245_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5716_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5716_0_0_0;
extern Il2CppType IEnumerator_1_t5716_1_0_0;
struct IEnumerator_1_t5716;
extern Il2CppGenericClass IEnumerator_1_t5716_GenericClass;
TypeInfo IEnumerator_1_t5716_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5716_MethodInfos/* methods */
	, IEnumerator_1_t5716_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5716_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5716_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5716_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5716_0_0_0/* byval_arg */
	, &IEnumerator_1_t5716_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5716_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<AnimateTexture>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_11.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2715_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<AnimateTexture>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_11MethodDeclarations.h"

extern TypeInfo AnimateTexture_t8_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14061_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAnimateTexture_t8_m31570_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<AnimateTexture>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<AnimateTexture>(System.Int32)
#define Array_InternalArray__get_Item_TisAnimateTexture_t8_m31570(__this, p0, method) (AnimateTexture_t8 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<AnimateTexture>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<AnimateTexture>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<AnimateTexture>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<AnimateTexture>::MoveNext()
// T System.Array/InternalEnumerator`1<AnimateTexture>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<AnimateTexture>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2715____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2715_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2715, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2715____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2715_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2715, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2715_FieldInfos[] =
{
	&InternalEnumerator_1_t2715____array_0_FieldInfo,
	&InternalEnumerator_1_t2715____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14058_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2715____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2715_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14058_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2715____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2715_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14061_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2715_PropertyInfos[] =
{
	&InternalEnumerator_1_t2715____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2715____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2715_InternalEnumerator_1__ctor_m14057_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14057_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<AnimateTexture>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14057_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2715_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2715_InternalEnumerator_1__ctor_m14057_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14057_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14058_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<AnimateTexture>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14058_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2715_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14058_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14059_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<AnimateTexture>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14059_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2715_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14059_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14060_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<AnimateTexture>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14060_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2715_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14060_GenericMethod/* genericMethod */

};
extern Il2CppType AnimateTexture_t8_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14061_GenericMethod;
// T System.Array/InternalEnumerator`1<AnimateTexture>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14061_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2715_il2cpp_TypeInfo/* declaring_type */
	, &AnimateTexture_t8_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14061_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2715_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14057_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14058_MethodInfo,
	&InternalEnumerator_1_Dispose_m14059_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14060_MethodInfo,
	&InternalEnumerator_1_get_Current_m14061_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14060_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14059_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2715_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14058_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14060_MethodInfo,
	&InternalEnumerator_1_Dispose_m14059_MethodInfo,
	&InternalEnumerator_1_get_Current_m14061_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2715_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5716_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2715_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5716_il2cpp_TypeInfo, 7},
};
extern TypeInfo AnimateTexture_t8_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2715_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14061_MethodInfo/* Method Usage */,
	&AnimateTexture_t8_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAnimateTexture_t8_m31570_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2715_0_0_0;
extern Il2CppType InternalEnumerator_1_t2715_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2715_GenericClass;
TypeInfo InternalEnumerator_1_t2715_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2715_MethodInfos/* methods */
	, InternalEnumerator_1_t2715_PropertyInfos/* properties */
	, InternalEnumerator_1_t2715_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2715_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2715_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2715_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2715_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2715_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2715_1_0_0/* this_arg */
	, InternalEnumerator_1_t2715_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2715_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2715_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2715)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7246_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<AnimateTexture>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<AnimateTexture>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<AnimateTexture>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<AnimateTexture>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<AnimateTexture>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<AnimateTexture>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<AnimateTexture>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<AnimateTexture>
extern MethodInfo ICollection_1_get_Count_m41246_MethodInfo;
static PropertyInfo ICollection_1_t7246____Count_PropertyInfo = 
{
	&ICollection_1_t7246_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41246_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41247_MethodInfo;
static PropertyInfo ICollection_1_t7246____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7246_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41247_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7246_PropertyInfos[] =
{
	&ICollection_1_t7246____Count_PropertyInfo,
	&ICollection_1_t7246____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41246_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<AnimateTexture>::get_Count()
MethodInfo ICollection_1_get_Count_m41246_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7246_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41246_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41247_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<AnimateTexture>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41247_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7246_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41247_GenericMethod/* genericMethod */

};
extern Il2CppType AnimateTexture_t8_0_0_0;
extern Il2CppType AnimateTexture_t8_0_0_0;
static ParameterInfo ICollection_1_t7246_ICollection_1_Add_m41248_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AnimateTexture_t8_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41248_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<AnimateTexture>::Add(T)
MethodInfo ICollection_1_Add_m41248_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7246_ICollection_1_Add_m41248_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41248_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41249_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<AnimateTexture>::Clear()
MethodInfo ICollection_1_Clear_m41249_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41249_GenericMethod/* genericMethod */

};
extern Il2CppType AnimateTexture_t8_0_0_0;
static ParameterInfo ICollection_1_t7246_ICollection_1_Contains_m41250_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AnimateTexture_t8_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41250_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<AnimateTexture>::Contains(T)
MethodInfo ICollection_1_Contains_m41250_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7246_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7246_ICollection_1_Contains_m41250_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41250_GenericMethod/* genericMethod */

};
extern Il2CppType AnimateTextureU5BU5D_t5159_0_0_0;
extern Il2CppType AnimateTextureU5BU5D_t5159_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7246_ICollection_1_CopyTo_m41251_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AnimateTextureU5BU5D_t5159_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41251_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<AnimateTexture>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41251_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7246_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7246_ICollection_1_CopyTo_m41251_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41251_GenericMethod/* genericMethod */

};
extern Il2CppType AnimateTexture_t8_0_0_0;
static ParameterInfo ICollection_1_t7246_ICollection_1_Remove_m41252_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AnimateTexture_t8_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41252_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<AnimateTexture>::Remove(T)
MethodInfo ICollection_1_Remove_m41252_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7246_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7246_ICollection_1_Remove_m41252_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41252_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7246_MethodInfos[] =
{
	&ICollection_1_get_Count_m41246_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41247_MethodInfo,
	&ICollection_1_Add_m41248_MethodInfo,
	&ICollection_1_Clear_m41249_MethodInfo,
	&ICollection_1_Contains_m41250_MethodInfo,
	&ICollection_1_CopyTo_m41251_MethodInfo,
	&ICollection_1_Remove_m41252_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7248_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7246_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7248_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7246_0_0_0;
extern Il2CppType ICollection_1_t7246_1_0_0;
struct ICollection_1_t7246;
extern Il2CppGenericClass ICollection_1_t7246_GenericClass;
TypeInfo ICollection_1_t7246_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7246_MethodInfos/* methods */
	, ICollection_1_t7246_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7246_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7246_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7246_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7246_0_0_0/* byval_arg */
	, &ICollection_1_t7246_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7246_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<AnimateTexture>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<AnimateTexture>
extern Il2CppType IEnumerator_1_t5716_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41253_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<AnimateTexture>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41253_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7248_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5716_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41253_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7248_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41253_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7248_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7248_0_0_0;
extern Il2CppType IEnumerable_1_t7248_1_0_0;
struct IEnumerable_1_t7248;
extern Il2CppGenericClass IEnumerable_1_t7248_GenericClass;
TypeInfo IEnumerable_1_t7248_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7248_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7248_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7248_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7248_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7248_0_0_0/* byval_arg */
	, &IEnumerable_1_t7248_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7248_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7247_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<AnimateTexture>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<AnimateTexture>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<AnimateTexture>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<AnimateTexture>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<AnimateTexture>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<AnimateTexture>
extern MethodInfo IList_1_get_Item_m41254_MethodInfo;
extern MethodInfo IList_1_set_Item_m41255_MethodInfo;
static PropertyInfo IList_1_t7247____Item_PropertyInfo = 
{
	&IList_1_t7247_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41254_MethodInfo/* get */
	, &IList_1_set_Item_m41255_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7247_PropertyInfos[] =
{
	&IList_1_t7247____Item_PropertyInfo,
	NULL
};
extern Il2CppType AnimateTexture_t8_0_0_0;
static ParameterInfo IList_1_t7247_IList_1_IndexOf_m41256_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AnimateTexture_t8_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41256_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<AnimateTexture>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41256_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7247_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7247_IList_1_IndexOf_m41256_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41256_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AnimateTexture_t8_0_0_0;
static ParameterInfo IList_1_t7247_IList_1_Insert_m41257_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AnimateTexture_t8_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41257_GenericMethod;
// System.Void System.Collections.Generic.IList`1<AnimateTexture>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41257_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7247_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7247_IList_1_Insert_m41257_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41257_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7247_IList_1_RemoveAt_m41258_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41258_GenericMethod;
// System.Void System.Collections.Generic.IList`1<AnimateTexture>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41258_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7247_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7247_IList_1_RemoveAt_m41258_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41258_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7247_IList_1_get_Item_m41254_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AnimateTexture_t8_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41254_GenericMethod;
// T System.Collections.Generic.IList`1<AnimateTexture>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41254_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7247_il2cpp_TypeInfo/* declaring_type */
	, &AnimateTexture_t8_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7247_IList_1_get_Item_m41254_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41254_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AnimateTexture_t8_0_0_0;
static ParameterInfo IList_1_t7247_IList_1_set_Item_m41255_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AnimateTexture_t8_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41255_GenericMethod;
// System.Void System.Collections.Generic.IList`1<AnimateTexture>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41255_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7247_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7247_IList_1_set_Item_m41255_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41255_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7247_MethodInfos[] =
{
	&IList_1_IndexOf_m41256_MethodInfo,
	&IList_1_Insert_m41257_MethodInfo,
	&IList_1_RemoveAt_m41258_MethodInfo,
	&IList_1_get_Item_m41254_MethodInfo,
	&IList_1_set_Item_m41255_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7247_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7246_il2cpp_TypeInfo,
	&IEnumerable_1_t7248_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7247_0_0_0;
extern Il2CppType IList_1_t7247_1_0_0;
struct IList_1_t7247;
extern Il2CppGenericClass IList_1_t7247_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7247_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7247_MethodInfos/* methods */
	, IList_1_t7247_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7247_il2cpp_TypeInfo/* element_class */
	, IList_1_t7247_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7247_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7247_0_0_0/* byval_arg */
	, &IList_1_t7247_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7247_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<AnimateTexture>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_5.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2716_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<AnimateTexture>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_5MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<AnimateTexture>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_1.h"
extern TypeInfo InvokableCall_1_t2717_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<AnimateTexture>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_1MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14064_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14066_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<AnimateTexture>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<AnimateTexture>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<AnimateTexture>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2716____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2716_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2716, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2716_FieldInfos[] =
{
	&CachedInvokableCall_1_t2716____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType AnimateTexture_t8_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2716_CachedInvokableCall_1__ctor_m14062_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &AnimateTexture_t8_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14062_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<AnimateTexture>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14062_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2716_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2716_CachedInvokableCall_1__ctor_m14062_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14062_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2716_CachedInvokableCall_1_Invoke_m14063_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14063_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<AnimateTexture>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14063_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2716_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2716_CachedInvokableCall_1_Invoke_m14063_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14063_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2716_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14062_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14063_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14063_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14067_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2716_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14063_MethodInfo,
	&InvokableCall_1_Find_m14067_MethodInfo,
};
extern Il2CppType UnityAction_1_t2718_0_0_0;
extern TypeInfo UnityAction_1_t2718_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisAnimateTexture_t8_m31580_MethodInfo;
extern TypeInfo AnimateTexture_t8_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14069_MethodInfo;
extern TypeInfo AnimateTexture_t8_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2716_RGCTXData[8] = 
{
	&UnityAction_1_t2718_0_0_0/* Type Usage */,
	&UnityAction_1_t2718_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisAnimateTexture_t8_m31580_MethodInfo/* Method Usage */,
	&AnimateTexture_t8_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14069_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14064_MethodInfo/* Method Usage */,
	&AnimateTexture_t8_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14066_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2716_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2716_1_0_0;
struct CachedInvokableCall_1_t2716;
extern Il2CppGenericClass CachedInvokableCall_1_t2716_GenericClass;
TypeInfo CachedInvokableCall_1_t2716_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2716_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2716_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2717_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2716_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2716_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2716_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2716_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2716_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2716_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2716_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2716)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<AnimateTexture>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_8.h"
extern TypeInfo UnityAction_1_t2718_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<AnimateTexture>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_8MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<AnimateTexture>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<AnimateTexture>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisAnimateTexture_t8_m31580(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<AnimateTexture>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<AnimateTexture>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<AnimateTexture>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<AnimateTexture>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<AnimateTexture>
extern Il2CppType UnityAction_1_t2718_0_0_1;
FieldInfo InvokableCall_1_t2717____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2718_0_0_1/* type */
	, &InvokableCall_1_t2717_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2717, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2717_FieldInfos[] =
{
	&InvokableCall_1_t2717____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2717_InvokableCall_1__ctor_m14064_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14064_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<AnimateTexture>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14064_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2717_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2717_InvokableCall_1__ctor_m14064_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14064_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2718_0_0_0;
static ParameterInfo InvokableCall_1_t2717_InvokableCall_1__ctor_m14065_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2718_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14065_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<AnimateTexture>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14065_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2717_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2717_InvokableCall_1__ctor_m14065_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14065_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2717_InvokableCall_1_Invoke_m14066_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14066_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<AnimateTexture>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14066_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2717_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2717_InvokableCall_1_Invoke_m14066_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14066_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2717_InvokableCall_1_Find_m14067_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14067_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<AnimateTexture>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14067_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2717_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2717_InvokableCall_1_Find_m14067_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14067_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2717_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14064_MethodInfo,
	&InvokableCall_1__ctor_m14065_MethodInfo,
	&InvokableCall_1_Invoke_m14066_MethodInfo,
	&InvokableCall_1_Find_m14067_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2717_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14066_MethodInfo,
	&InvokableCall_1_Find_m14067_MethodInfo,
};
extern TypeInfo UnityAction_1_t2718_il2cpp_TypeInfo;
extern TypeInfo AnimateTexture_t8_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2717_RGCTXData[5] = 
{
	&UnityAction_1_t2718_0_0_0/* Type Usage */,
	&UnityAction_1_t2718_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisAnimateTexture_t8_m31580_MethodInfo/* Method Usage */,
	&AnimateTexture_t8_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14069_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2717_0_0_0;
extern Il2CppType InvokableCall_1_t2717_1_0_0;
struct InvokableCall_1_t2717;
extern Il2CppGenericClass InvokableCall_1_t2717_GenericClass;
TypeInfo InvokableCall_1_t2717_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2717_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2717_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2717_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2717_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2717_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2717_0_0_0/* byval_arg */
	, &InvokableCall_1_t2717_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2717_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2717_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2717)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<AnimateTexture>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<AnimateTexture>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<AnimateTexture>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<AnimateTexture>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<AnimateTexture>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2718_UnityAction_1__ctor_m14068_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14068_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<AnimateTexture>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14068_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2718_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2718_UnityAction_1__ctor_m14068_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14068_GenericMethod/* genericMethod */

};
extern Il2CppType AnimateTexture_t8_0_0_0;
static ParameterInfo UnityAction_1_t2718_UnityAction_1_Invoke_m14069_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &AnimateTexture_t8_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14069_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<AnimateTexture>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14069_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2718_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2718_UnityAction_1_Invoke_m14069_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14069_GenericMethod/* genericMethod */

};
extern Il2CppType AnimateTexture_t8_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2718_UnityAction_1_BeginInvoke_m14070_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &AnimateTexture_t8_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14070_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<AnimateTexture>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14070_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2718_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2718_UnityAction_1_BeginInvoke_m14070_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14070_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2718_UnityAction_1_EndInvoke_m14071_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14071_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<AnimateTexture>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14071_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2718_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2718_UnityAction_1_EndInvoke_m14071_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14071_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2718_MethodInfos[] =
{
	&UnityAction_1__ctor_m14068_MethodInfo,
	&UnityAction_1_Invoke_m14069_MethodInfo,
	&UnityAction_1_BeginInvoke_m14070_MethodInfo,
	&UnityAction_1_EndInvoke_m14071_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14070_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14071_MethodInfo;
static MethodInfo* UnityAction_1_t2718_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14069_MethodInfo,
	&UnityAction_1_BeginInvoke_m14070_MethodInfo,
	&UnityAction_1_EndInvoke_m14071_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2718_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2718_1_0_0;
struct UnityAction_1_t2718;
extern Il2CppGenericClass UnityAction_1_t2718_GenericClass;
TypeInfo UnityAction_1_t2718_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2718_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2718_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2718_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2718_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2718_0_0_0/* byval_arg */
	, &UnityAction_1_t2718_1_0_0/* this_arg */
	, UnityAction_1_t2718_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2718_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2718)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<System.Object>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_0.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2719_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<System.Object>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_0MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<System.Object>
extern Il2CppType Object_t_0_0_6;
FieldInfo CastHelper_1_t2719____t_0_FieldInfo = 
{
	"t"/* name */
	, &Object_t_0_0_6/* type */
	, &CastHelper_1_t2719_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2719, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2719____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2719_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2719, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2719_FieldInfos[] =
{
	&CastHelper_1_t2719____t_0_FieldInfo,
	&CastHelper_1_t2719____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2719_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2719_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2719_0_0_0;
extern Il2CppType CastHelper_1_t2719_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2719_GenericClass;
TypeInfo CastHelper_1_t2719_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2719_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2719_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2719_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2719_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2719_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2719_0_0_0/* byval_arg */
	, &CastHelper_1_t2719_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2719_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2719)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.Renderer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2720_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.Renderer>
#include "UnityEngine_UnityEngine_CastHelper_1_genMethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.Renderer>
extern Il2CppType Renderer_t7_0_0_6;
FieldInfo CastHelper_1_t2720____t_0_FieldInfo = 
{
	"t"/* name */
	, &Renderer_t7_0_0_6/* type */
	, &CastHelper_1_t2720_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2720, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2720____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2720_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2720, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2720_FieldInfos[] =
{
	&CastHelper_1_t2720____t_0_FieldInfo,
	&CastHelper_1_t2720____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2720_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2720_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2720_0_0_0;
extern Il2CppType CastHelper_1_t2720_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2720_GenericClass;
TypeInfo CastHelper_1_t2720_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2720_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2720_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2720_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2720_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2720_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2720_0_0_0/* byval_arg */
	, &CastHelper_1_t2720_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2720_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2720)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5718_il2cpp_TypeInfo;

// GyroCameraYo
#include "AssemblyU2DCSharp_GyroCameraYo.h"


// T System.Collections.Generic.IEnumerator`1<GyroCameraYo>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<GyroCameraYo>
extern MethodInfo IEnumerator_1_get_Current_m41259_MethodInfo;
static PropertyInfo IEnumerator_1_t5718____Current_PropertyInfo = 
{
	&IEnumerator_1_t5718_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41259_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5718_PropertyInfos[] =
{
	&IEnumerator_1_t5718____Current_PropertyInfo,
	NULL
};
extern Il2CppType GyroCameraYo_t11_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41259_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<GyroCameraYo>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41259_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5718_il2cpp_TypeInfo/* declaring_type */
	, &GyroCameraYo_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41259_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5718_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41259_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5718_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5718_0_0_0;
extern Il2CppType IEnumerator_1_t5718_1_0_0;
struct IEnumerator_1_t5718;
extern Il2CppGenericClass IEnumerator_1_t5718_GenericClass;
TypeInfo IEnumerator_1_t5718_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5718_MethodInfos/* methods */
	, IEnumerator_1_t5718_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5718_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5718_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5718_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5718_0_0_0/* byval_arg */
	, &IEnumerator_1_t5718_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5718_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<GyroCameraYo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_12.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2721_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<GyroCameraYo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_12MethodDeclarations.h"

extern TypeInfo GyroCameraYo_t11_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14076_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGyroCameraYo_t11_m31582_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<GyroCameraYo>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<GyroCameraYo>(System.Int32)
#define Array_InternalArray__get_Item_TisGyroCameraYo_t11_m31582(__this, p0, method) (GyroCameraYo_t11 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<GyroCameraYo>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<GyroCameraYo>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<GyroCameraYo>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<GyroCameraYo>::MoveNext()
// T System.Array/InternalEnumerator`1<GyroCameraYo>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<GyroCameraYo>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2721____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2721_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2721, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2721____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2721_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2721, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2721_FieldInfos[] =
{
	&InternalEnumerator_1_t2721____array_0_FieldInfo,
	&InternalEnumerator_1_t2721____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14073_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2721____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2721_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14073_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2721____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2721_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14076_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2721_PropertyInfos[] =
{
	&InternalEnumerator_1_t2721____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2721____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2721_InternalEnumerator_1__ctor_m14072_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14072_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<GyroCameraYo>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14072_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2721_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2721_InternalEnumerator_1__ctor_m14072_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14072_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14073_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<GyroCameraYo>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14073_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2721_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14073_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14074_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<GyroCameraYo>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14074_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2721_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14074_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14075_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<GyroCameraYo>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14075_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2721_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14075_GenericMethod/* genericMethod */

};
extern Il2CppType GyroCameraYo_t11_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14076_GenericMethod;
// T System.Array/InternalEnumerator`1<GyroCameraYo>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14076_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2721_il2cpp_TypeInfo/* declaring_type */
	, &GyroCameraYo_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14076_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2721_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14072_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14073_MethodInfo,
	&InternalEnumerator_1_Dispose_m14074_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14075_MethodInfo,
	&InternalEnumerator_1_get_Current_m14076_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14075_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14074_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2721_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14073_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14075_MethodInfo,
	&InternalEnumerator_1_Dispose_m14074_MethodInfo,
	&InternalEnumerator_1_get_Current_m14076_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2721_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5718_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2721_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5718_il2cpp_TypeInfo, 7},
};
extern TypeInfo GyroCameraYo_t11_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2721_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14076_MethodInfo/* Method Usage */,
	&GyroCameraYo_t11_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisGyroCameraYo_t11_m31582_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2721_0_0_0;
extern Il2CppType InternalEnumerator_1_t2721_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2721_GenericClass;
TypeInfo InternalEnumerator_1_t2721_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2721_MethodInfos/* methods */
	, InternalEnumerator_1_t2721_PropertyInfos/* properties */
	, InternalEnumerator_1_t2721_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2721_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2721_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2721_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2721_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2721_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2721_1_0_0/* this_arg */
	, InternalEnumerator_1_t2721_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2721_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2721_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2721)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7249_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<GyroCameraYo>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<GyroCameraYo>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<GyroCameraYo>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<GyroCameraYo>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<GyroCameraYo>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<GyroCameraYo>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<GyroCameraYo>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<GyroCameraYo>
extern MethodInfo ICollection_1_get_Count_m41260_MethodInfo;
static PropertyInfo ICollection_1_t7249____Count_PropertyInfo = 
{
	&ICollection_1_t7249_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41260_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41261_MethodInfo;
static PropertyInfo ICollection_1_t7249____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7249_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41261_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7249_PropertyInfos[] =
{
	&ICollection_1_t7249____Count_PropertyInfo,
	&ICollection_1_t7249____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41260_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<GyroCameraYo>::get_Count()
MethodInfo ICollection_1_get_Count_m41260_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7249_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41260_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41261_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GyroCameraYo>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41261_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7249_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41261_GenericMethod/* genericMethod */

};
extern Il2CppType GyroCameraYo_t11_0_0_0;
extern Il2CppType GyroCameraYo_t11_0_0_0;
static ParameterInfo ICollection_1_t7249_ICollection_1_Add_m41262_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GyroCameraYo_t11_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41262_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GyroCameraYo>::Add(T)
MethodInfo ICollection_1_Add_m41262_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7249_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7249_ICollection_1_Add_m41262_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41262_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41263_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GyroCameraYo>::Clear()
MethodInfo ICollection_1_Clear_m41263_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7249_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41263_GenericMethod/* genericMethod */

};
extern Il2CppType GyroCameraYo_t11_0_0_0;
static ParameterInfo ICollection_1_t7249_ICollection_1_Contains_m41264_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GyroCameraYo_t11_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41264_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GyroCameraYo>::Contains(T)
MethodInfo ICollection_1_Contains_m41264_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7249_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7249_ICollection_1_Contains_m41264_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41264_GenericMethod/* genericMethod */

};
extern Il2CppType GyroCameraYoU5BU5D_t5160_0_0_0;
extern Il2CppType GyroCameraYoU5BU5D_t5160_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7249_ICollection_1_CopyTo_m41265_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GyroCameraYoU5BU5D_t5160_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41265_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<GyroCameraYo>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41265_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7249_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7249_ICollection_1_CopyTo_m41265_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41265_GenericMethod/* genericMethod */

};
extern Il2CppType GyroCameraYo_t11_0_0_0;
static ParameterInfo ICollection_1_t7249_ICollection_1_Remove_m41266_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GyroCameraYo_t11_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41266_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<GyroCameraYo>::Remove(T)
MethodInfo ICollection_1_Remove_m41266_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7249_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7249_ICollection_1_Remove_m41266_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41266_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7249_MethodInfos[] =
{
	&ICollection_1_get_Count_m41260_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41261_MethodInfo,
	&ICollection_1_Add_m41262_MethodInfo,
	&ICollection_1_Clear_m41263_MethodInfo,
	&ICollection_1_Contains_m41264_MethodInfo,
	&ICollection_1_CopyTo_m41265_MethodInfo,
	&ICollection_1_Remove_m41266_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7251_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7249_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7251_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7249_0_0_0;
extern Il2CppType ICollection_1_t7249_1_0_0;
struct ICollection_1_t7249;
extern Il2CppGenericClass ICollection_1_t7249_GenericClass;
TypeInfo ICollection_1_t7249_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7249_MethodInfos/* methods */
	, ICollection_1_t7249_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7249_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7249_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7249_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7249_0_0_0/* byval_arg */
	, &ICollection_1_t7249_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7249_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<GyroCameraYo>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<GyroCameraYo>
extern Il2CppType IEnumerator_1_t5718_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41267_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<GyroCameraYo>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41267_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7251_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5718_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41267_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7251_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41267_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7251_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7251_0_0_0;
extern Il2CppType IEnumerable_1_t7251_1_0_0;
struct IEnumerable_1_t7251;
extern Il2CppGenericClass IEnumerable_1_t7251_GenericClass;
TypeInfo IEnumerable_1_t7251_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7251_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7251_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7251_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7251_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7251_0_0_0/* byval_arg */
	, &IEnumerable_1_t7251_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7251_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7250_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<GyroCameraYo>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<GyroCameraYo>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<GyroCameraYo>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<GyroCameraYo>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<GyroCameraYo>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<GyroCameraYo>
extern MethodInfo IList_1_get_Item_m41268_MethodInfo;
extern MethodInfo IList_1_set_Item_m41269_MethodInfo;
static PropertyInfo IList_1_t7250____Item_PropertyInfo = 
{
	&IList_1_t7250_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41268_MethodInfo/* get */
	, &IList_1_set_Item_m41269_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7250_PropertyInfos[] =
{
	&IList_1_t7250____Item_PropertyInfo,
	NULL
};
extern Il2CppType GyroCameraYo_t11_0_0_0;
static ParameterInfo IList_1_t7250_IList_1_IndexOf_m41270_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GyroCameraYo_t11_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41270_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<GyroCameraYo>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41270_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7250_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7250_IList_1_IndexOf_m41270_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41270_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType GyroCameraYo_t11_0_0_0;
static ParameterInfo IList_1_t7250_IList_1_Insert_m41271_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GyroCameraYo_t11_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41271_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GyroCameraYo>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41271_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7250_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7250_IList_1_Insert_m41271_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41271_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7250_IList_1_RemoveAt_m41272_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41272_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GyroCameraYo>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41272_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7250_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7250_IList_1_RemoveAt_m41272_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41272_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7250_IList_1_get_Item_m41268_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType GyroCameraYo_t11_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41268_GenericMethod;
// T System.Collections.Generic.IList`1<GyroCameraYo>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41268_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7250_il2cpp_TypeInfo/* declaring_type */
	, &GyroCameraYo_t11_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7250_IList_1_get_Item_m41268_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41268_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType GyroCameraYo_t11_0_0_0;
static ParameterInfo IList_1_t7250_IList_1_set_Item_m41269_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GyroCameraYo_t11_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41269_GenericMethod;
// System.Void System.Collections.Generic.IList`1<GyroCameraYo>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41269_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7250_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7250_IList_1_set_Item_m41269_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41269_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7250_MethodInfos[] =
{
	&IList_1_IndexOf_m41270_MethodInfo,
	&IList_1_Insert_m41271_MethodInfo,
	&IList_1_RemoveAt_m41272_MethodInfo,
	&IList_1_get_Item_m41268_MethodInfo,
	&IList_1_set_Item_m41269_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7250_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7249_il2cpp_TypeInfo,
	&IEnumerable_1_t7251_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7250_0_0_0;
extern Il2CppType IList_1_t7250_1_0_0;
struct IList_1_t7250;
extern Il2CppGenericClass IList_1_t7250_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7250_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7250_MethodInfos/* methods */
	, IList_1_t7250_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7250_il2cpp_TypeInfo/* element_class */
	, IList_1_t7250_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7250_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7250_0_0_0/* byval_arg */
	, &IList_1_t7250_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7250_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<GyroCameraYo>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_6.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2722_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<GyroCameraYo>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_6MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<GyroCameraYo>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_2.h"
extern TypeInfo InvokableCall_1_t2723_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<GyroCameraYo>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_2MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14079_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14081_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<GyroCameraYo>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<GyroCameraYo>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<GyroCameraYo>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2722____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2722_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2722, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2722_FieldInfos[] =
{
	&CachedInvokableCall_1_t2722____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType GyroCameraYo_t11_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2722_CachedInvokableCall_1__ctor_m14077_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &GyroCameraYo_t11_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14077_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<GyroCameraYo>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14077_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2722_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2722_CachedInvokableCall_1__ctor_m14077_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14077_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2722_CachedInvokableCall_1_Invoke_m14078_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14078_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<GyroCameraYo>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14078_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2722_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2722_CachedInvokableCall_1_Invoke_m14078_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14078_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2722_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14077_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14078_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14078_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14082_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2722_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14078_MethodInfo,
	&InvokableCall_1_Find_m14082_MethodInfo,
};
extern Il2CppType UnityAction_1_t2724_0_0_0;
extern TypeInfo UnityAction_1_t2724_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisGyroCameraYo_t11_m31592_MethodInfo;
extern TypeInfo GyroCameraYo_t11_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14084_MethodInfo;
extern TypeInfo GyroCameraYo_t11_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2722_RGCTXData[8] = 
{
	&UnityAction_1_t2724_0_0_0/* Type Usage */,
	&UnityAction_1_t2724_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGyroCameraYo_t11_m31592_MethodInfo/* Method Usage */,
	&GyroCameraYo_t11_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14084_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14079_MethodInfo/* Method Usage */,
	&GyroCameraYo_t11_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14081_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2722_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2722_1_0_0;
struct CachedInvokableCall_1_t2722;
extern Il2CppGenericClass CachedInvokableCall_1_t2722_GenericClass;
TypeInfo CachedInvokableCall_1_t2722_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2722_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2722_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2723_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2722_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2722_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2722_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2722_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2722_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2722_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2722_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2722)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<GyroCameraYo>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_9.h"
extern TypeInfo UnityAction_1_t2724_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<GyroCameraYo>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_9MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<GyroCameraYo>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<GyroCameraYo>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisGyroCameraYo_t11_m31592(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<GyroCameraYo>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<GyroCameraYo>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<GyroCameraYo>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<GyroCameraYo>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<GyroCameraYo>
extern Il2CppType UnityAction_1_t2724_0_0_1;
FieldInfo InvokableCall_1_t2723____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2724_0_0_1/* type */
	, &InvokableCall_1_t2723_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2723, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2723_FieldInfos[] =
{
	&InvokableCall_1_t2723____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2723_InvokableCall_1__ctor_m14079_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14079_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<GyroCameraYo>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14079_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2723_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2723_InvokableCall_1__ctor_m14079_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14079_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2724_0_0_0;
static ParameterInfo InvokableCall_1_t2723_InvokableCall_1__ctor_m14080_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2724_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14080_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<GyroCameraYo>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14080_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2723_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2723_InvokableCall_1__ctor_m14080_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14080_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2723_InvokableCall_1_Invoke_m14081_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14081_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<GyroCameraYo>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14081_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2723_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2723_InvokableCall_1_Invoke_m14081_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14081_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2723_InvokableCall_1_Find_m14082_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14082_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<GyroCameraYo>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14082_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2723_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2723_InvokableCall_1_Find_m14082_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14082_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2723_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14079_MethodInfo,
	&InvokableCall_1__ctor_m14080_MethodInfo,
	&InvokableCall_1_Invoke_m14081_MethodInfo,
	&InvokableCall_1_Find_m14082_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2723_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14081_MethodInfo,
	&InvokableCall_1_Find_m14082_MethodInfo,
};
extern TypeInfo UnityAction_1_t2724_il2cpp_TypeInfo;
extern TypeInfo GyroCameraYo_t11_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2723_RGCTXData[5] = 
{
	&UnityAction_1_t2724_0_0_0/* Type Usage */,
	&UnityAction_1_t2724_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGyroCameraYo_t11_m31592_MethodInfo/* Method Usage */,
	&GyroCameraYo_t11_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14084_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2723_0_0_0;
extern Il2CppType InvokableCall_1_t2723_1_0_0;
struct InvokableCall_1_t2723;
extern Il2CppGenericClass InvokableCall_1_t2723_GenericClass;
TypeInfo InvokableCall_1_t2723_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2723_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2723_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2723_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2723_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2723_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2723_0_0_0/* byval_arg */
	, &InvokableCall_1_t2723_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2723_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2723_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2723)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<GyroCameraYo>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<GyroCameraYo>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<GyroCameraYo>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<GyroCameraYo>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<GyroCameraYo>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2724_UnityAction_1__ctor_m14083_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14083_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<GyroCameraYo>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14083_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2724_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2724_UnityAction_1__ctor_m14083_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14083_GenericMethod/* genericMethod */

};
extern Il2CppType GyroCameraYo_t11_0_0_0;
static ParameterInfo UnityAction_1_t2724_UnityAction_1_Invoke_m14084_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GyroCameraYo_t11_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14084_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<GyroCameraYo>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14084_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2724_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2724_UnityAction_1_Invoke_m14084_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14084_GenericMethod/* genericMethod */

};
extern Il2CppType GyroCameraYo_t11_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2724_UnityAction_1_BeginInvoke_m14085_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GyroCameraYo_t11_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14085_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<GyroCameraYo>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14085_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2724_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2724_UnityAction_1_BeginInvoke_m14085_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14085_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2724_UnityAction_1_EndInvoke_m14086_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14086_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<GyroCameraYo>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14086_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2724_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2724_UnityAction_1_EndInvoke_m14086_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14086_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2724_MethodInfos[] =
{
	&UnityAction_1__ctor_m14083_MethodInfo,
	&UnityAction_1_Invoke_m14084_MethodInfo,
	&UnityAction_1_BeginInvoke_m14085_MethodInfo,
	&UnityAction_1_EndInvoke_m14086_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14085_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14086_MethodInfo;
static MethodInfo* UnityAction_1_t2724_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14084_MethodInfo,
	&UnityAction_1_BeginInvoke_m14085_MethodInfo,
	&UnityAction_1_EndInvoke_m14086_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2724_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2724_1_0_0;
struct UnityAction_1_t2724;
extern Il2CppGenericClass UnityAction_1_t2724_GenericClass;
TypeInfo UnityAction_1_t2724_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2724_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2724_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2724_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2724_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2724_0_0_0/* byval_arg */
	, &UnityAction_1_t2724_1_0_0/* this_arg */
	, UnityAction_1_t2724_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2724_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2724)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<UnityEngine.Transform>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_1.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2725_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<UnityEngine.Transform>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_1MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<UnityEngine.Transform>
extern Il2CppType Transform_t10_0_0_6;
FieldInfo CastHelper_1_t2725____t_0_FieldInfo = 
{
	"t"/* name */
	, &Transform_t10_0_0_6/* type */
	, &CastHelper_1_t2725_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2725, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2725____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2725_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2725, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2725_FieldInfos[] =
{
	&CastHelper_1_t2725____t_0_FieldInfo,
	&CastHelper_1_t2725____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2725_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2725_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2725_0_0_0;
extern Il2CppType CastHelper_1_t2725_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2725_GenericClass;
TypeInfo CastHelper_1_t2725_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2725_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2725_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2725_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2725_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2725_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2725_0_0_0/* byval_arg */
	, &CastHelper_1_t2725_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2725_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2725)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5720_il2cpp_TypeInfo;

// Input2
#include "AssemblyU2DCSharp_Input2.h"


// T System.Collections.Generic.IEnumerator`1<Input2>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Input2>
extern MethodInfo IEnumerator_1_get_Current_m41273_MethodInfo;
static PropertyInfo IEnumerator_1_t5720____Current_PropertyInfo = 
{
	&IEnumerator_1_t5720_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41273_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5720_PropertyInfos[] =
{
	&IEnumerator_1_t5720____Current_PropertyInfo,
	NULL
};
extern Il2CppType Input2_t14_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41273_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Input2>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41273_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5720_il2cpp_TypeInfo/* declaring_type */
	, &Input2_t14_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41273_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5720_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41273_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5720_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5720_0_0_0;
extern Il2CppType IEnumerator_1_t5720_1_0_0;
struct IEnumerator_1_t5720;
extern Il2CppGenericClass IEnumerator_1_t5720_GenericClass;
TypeInfo IEnumerator_1_t5720_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5720_MethodInfos/* methods */
	, IEnumerator_1_t5720_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5720_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5720_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5720_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5720_0_0_0/* byval_arg */
	, &IEnumerator_1_t5720_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5720_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Input2>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_13.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2726_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Input2>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_13MethodDeclarations.h"

extern TypeInfo Input2_t14_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14091_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisInput2_t14_m31594_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Input2>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Input2>(System.Int32)
#define Array_InternalArray__get_Item_TisInput2_t14_m31594(__this, p0, method) (Input2_t14 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Input2>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Input2>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Input2>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Input2>::MoveNext()
// T System.Array/InternalEnumerator`1<Input2>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Input2>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2726____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2726_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2726, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2726____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2726_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2726, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2726_FieldInfos[] =
{
	&InternalEnumerator_1_t2726____array_0_FieldInfo,
	&InternalEnumerator_1_t2726____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14088_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2726____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2726_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14088_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2726____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2726_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14091_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2726_PropertyInfos[] =
{
	&InternalEnumerator_1_t2726____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2726____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2726_InternalEnumerator_1__ctor_m14087_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14087_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Input2>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14087_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2726_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2726_InternalEnumerator_1__ctor_m14087_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14087_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14088_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Input2>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14088_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2726_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14088_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14089_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Input2>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14089_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2726_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14089_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14090_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Input2>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14090_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2726_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14090_GenericMethod/* genericMethod */

};
extern Il2CppType Input2_t14_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14091_GenericMethod;
// T System.Array/InternalEnumerator`1<Input2>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14091_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2726_il2cpp_TypeInfo/* declaring_type */
	, &Input2_t14_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14091_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2726_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14087_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14088_MethodInfo,
	&InternalEnumerator_1_Dispose_m14089_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14090_MethodInfo,
	&InternalEnumerator_1_get_Current_m14091_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14090_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14089_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2726_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14088_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14090_MethodInfo,
	&InternalEnumerator_1_Dispose_m14089_MethodInfo,
	&InternalEnumerator_1_get_Current_m14091_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2726_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5720_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2726_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5720_il2cpp_TypeInfo, 7},
};
extern TypeInfo Input2_t14_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2726_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14091_MethodInfo/* Method Usage */,
	&Input2_t14_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisInput2_t14_m31594_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2726_0_0_0;
extern Il2CppType InternalEnumerator_1_t2726_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2726_GenericClass;
TypeInfo InternalEnumerator_1_t2726_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2726_MethodInfos/* methods */
	, InternalEnumerator_1_t2726_PropertyInfos/* properties */
	, InternalEnumerator_1_t2726_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2726_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2726_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2726_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2726_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2726_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2726_1_0_0/* this_arg */
	, InternalEnumerator_1_t2726_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2726_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2726_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2726)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7252_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Input2>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Input2>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Input2>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Input2>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Input2>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Input2>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Input2>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Input2>
extern MethodInfo ICollection_1_get_Count_m41274_MethodInfo;
static PropertyInfo ICollection_1_t7252____Count_PropertyInfo = 
{
	&ICollection_1_t7252_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41274_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41275_MethodInfo;
static PropertyInfo ICollection_1_t7252____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7252_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41275_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7252_PropertyInfos[] =
{
	&ICollection_1_t7252____Count_PropertyInfo,
	&ICollection_1_t7252____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41274_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Input2>::get_Count()
MethodInfo ICollection_1_get_Count_m41274_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7252_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41274_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41275_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Input2>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41275_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7252_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41275_GenericMethod/* genericMethod */

};
extern Il2CppType Input2_t14_0_0_0;
extern Il2CppType Input2_t14_0_0_0;
static ParameterInfo ICollection_1_t7252_ICollection_1_Add_m41276_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Input2_t14_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41276_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Input2>::Add(T)
MethodInfo ICollection_1_Add_m41276_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7252_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7252_ICollection_1_Add_m41276_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41276_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41277_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Input2>::Clear()
MethodInfo ICollection_1_Clear_m41277_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7252_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41277_GenericMethod/* genericMethod */

};
extern Il2CppType Input2_t14_0_0_0;
static ParameterInfo ICollection_1_t7252_ICollection_1_Contains_m41278_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Input2_t14_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41278_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Input2>::Contains(T)
MethodInfo ICollection_1_Contains_m41278_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7252_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7252_ICollection_1_Contains_m41278_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41278_GenericMethod/* genericMethod */

};
extern Il2CppType Input2U5BU5D_t5161_0_0_0;
extern Il2CppType Input2U5BU5D_t5161_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7252_ICollection_1_CopyTo_m41279_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Input2U5BU5D_t5161_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41279_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Input2>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41279_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7252_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7252_ICollection_1_CopyTo_m41279_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41279_GenericMethod/* genericMethod */

};
extern Il2CppType Input2_t14_0_0_0;
static ParameterInfo ICollection_1_t7252_ICollection_1_Remove_m41280_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Input2_t14_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41280_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Input2>::Remove(T)
MethodInfo ICollection_1_Remove_m41280_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7252_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7252_ICollection_1_Remove_m41280_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41280_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7252_MethodInfos[] =
{
	&ICollection_1_get_Count_m41274_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41275_MethodInfo,
	&ICollection_1_Add_m41276_MethodInfo,
	&ICollection_1_Clear_m41277_MethodInfo,
	&ICollection_1_Contains_m41278_MethodInfo,
	&ICollection_1_CopyTo_m41279_MethodInfo,
	&ICollection_1_Remove_m41280_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7254_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7252_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7254_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7252_0_0_0;
extern Il2CppType ICollection_1_t7252_1_0_0;
struct ICollection_1_t7252;
extern Il2CppGenericClass ICollection_1_t7252_GenericClass;
TypeInfo ICollection_1_t7252_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7252_MethodInfos/* methods */
	, ICollection_1_t7252_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7252_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7252_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7252_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7252_0_0_0/* byval_arg */
	, &ICollection_1_t7252_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7252_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Input2>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Input2>
extern Il2CppType IEnumerator_1_t5720_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41281_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Input2>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41281_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7254_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5720_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41281_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7254_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41281_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7254_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7254_0_0_0;
extern Il2CppType IEnumerable_1_t7254_1_0_0;
struct IEnumerable_1_t7254;
extern Il2CppGenericClass IEnumerable_1_t7254_GenericClass;
TypeInfo IEnumerable_1_t7254_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7254_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7254_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7254_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7254_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7254_0_0_0/* byval_arg */
	, &IEnumerable_1_t7254_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7254_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7253_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Input2>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Input2>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Input2>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Input2>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Input2>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Input2>
extern MethodInfo IList_1_get_Item_m41282_MethodInfo;
extern MethodInfo IList_1_set_Item_m41283_MethodInfo;
static PropertyInfo IList_1_t7253____Item_PropertyInfo = 
{
	&IList_1_t7253_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41282_MethodInfo/* get */
	, &IList_1_set_Item_m41283_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7253_PropertyInfos[] =
{
	&IList_1_t7253____Item_PropertyInfo,
	NULL
};
extern Il2CppType Input2_t14_0_0_0;
static ParameterInfo IList_1_t7253_IList_1_IndexOf_m41284_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Input2_t14_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41284_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Input2>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41284_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7253_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7253_IList_1_IndexOf_m41284_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41284_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Input2_t14_0_0_0;
static ParameterInfo IList_1_t7253_IList_1_Insert_m41285_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Input2_t14_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41285_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Input2>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41285_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7253_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7253_IList_1_Insert_m41285_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41285_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7253_IList_1_RemoveAt_m41286_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41286_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Input2>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41286_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7253_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7253_IList_1_RemoveAt_m41286_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41286_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7253_IList_1_get_Item_m41282_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Input2_t14_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41282_GenericMethod;
// T System.Collections.Generic.IList`1<Input2>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41282_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7253_il2cpp_TypeInfo/* declaring_type */
	, &Input2_t14_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7253_IList_1_get_Item_m41282_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41282_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Input2_t14_0_0_0;
static ParameterInfo IList_1_t7253_IList_1_set_Item_m41283_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Input2_t14_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41283_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Input2>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41283_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7253_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7253_IList_1_set_Item_m41283_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41283_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7253_MethodInfos[] =
{
	&IList_1_IndexOf_m41284_MethodInfo,
	&IList_1_Insert_m41285_MethodInfo,
	&IList_1_RemoveAt_m41286_MethodInfo,
	&IList_1_get_Item_m41282_MethodInfo,
	&IList_1_set_Item_m41283_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7253_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7252_il2cpp_TypeInfo,
	&IEnumerable_1_t7254_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7253_0_0_0;
extern Il2CppType IList_1_t7253_1_0_0;
struct IList_1_t7253;
extern Il2CppGenericClass IList_1_t7253_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7253_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7253_MethodInfos/* methods */
	, IList_1_t7253_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7253_il2cpp_TypeInfo/* element_class */
	, IList_1_t7253_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7253_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7253_0_0_0/* byval_arg */
	, &IList_1_t7253_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7253_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Input2>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_7.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2727_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Input2>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_7MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Input2>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_3.h"
extern TypeInfo InvokableCall_1_t2728_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Input2>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_3MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14094_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14096_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Input2>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Input2>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Input2>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2727____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2727_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2727, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2727_FieldInfos[] =
{
	&CachedInvokableCall_1_t2727____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType Input2_t14_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2727_CachedInvokableCall_1__ctor_m14092_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &Input2_t14_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14092_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Input2>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14092_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2727_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2727_CachedInvokableCall_1__ctor_m14092_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14092_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2727_CachedInvokableCall_1_Invoke_m14093_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14093_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Input2>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14093_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2727_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2727_CachedInvokableCall_1_Invoke_m14093_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14093_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2727_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14092_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14093_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14093_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14097_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2727_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14093_MethodInfo,
	&InvokableCall_1_Find_m14097_MethodInfo,
};
extern Il2CppType UnityAction_1_t2729_0_0_0;
extern TypeInfo UnityAction_1_t2729_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisInput2_t14_m31604_MethodInfo;
extern TypeInfo Input2_t14_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14099_MethodInfo;
extern TypeInfo Input2_t14_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2727_RGCTXData[8] = 
{
	&UnityAction_1_t2729_0_0_0/* Type Usage */,
	&UnityAction_1_t2729_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisInput2_t14_m31604_MethodInfo/* Method Usage */,
	&Input2_t14_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14099_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14094_MethodInfo/* Method Usage */,
	&Input2_t14_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14096_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2727_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2727_1_0_0;
struct CachedInvokableCall_1_t2727;
extern Il2CppGenericClass CachedInvokableCall_1_t2727_GenericClass;
TypeInfo CachedInvokableCall_1_t2727_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2727_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2727_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2728_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2727_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2727_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2727_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2727_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2727_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2727_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2727_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2727)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Input2>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_10.h"
extern TypeInfo UnityAction_1_t2729_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Input2>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_10MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Input2>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Input2>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisInput2_t14_m31604(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Input2>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Input2>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Input2>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Input2>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Input2>
extern Il2CppType UnityAction_1_t2729_0_0_1;
FieldInfo InvokableCall_1_t2728____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2729_0_0_1/* type */
	, &InvokableCall_1_t2728_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2728, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2728_FieldInfos[] =
{
	&InvokableCall_1_t2728____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2728_InvokableCall_1__ctor_m14094_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14094_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Input2>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14094_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2728_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2728_InvokableCall_1__ctor_m14094_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14094_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2729_0_0_0;
static ParameterInfo InvokableCall_1_t2728_InvokableCall_1__ctor_m14095_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2729_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14095_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Input2>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14095_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2728_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2728_InvokableCall_1__ctor_m14095_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14095_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2728_InvokableCall_1_Invoke_m14096_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14096_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Input2>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14096_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2728_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2728_InvokableCall_1_Invoke_m14096_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14096_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2728_InvokableCall_1_Find_m14097_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14097_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Input2>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14097_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2728_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2728_InvokableCall_1_Find_m14097_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14097_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2728_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14094_MethodInfo,
	&InvokableCall_1__ctor_m14095_MethodInfo,
	&InvokableCall_1_Invoke_m14096_MethodInfo,
	&InvokableCall_1_Find_m14097_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2728_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14096_MethodInfo,
	&InvokableCall_1_Find_m14097_MethodInfo,
};
extern TypeInfo UnityAction_1_t2729_il2cpp_TypeInfo;
extern TypeInfo Input2_t14_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2728_RGCTXData[5] = 
{
	&UnityAction_1_t2729_0_0_0/* Type Usage */,
	&UnityAction_1_t2729_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisInput2_t14_m31604_MethodInfo/* Method Usage */,
	&Input2_t14_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14099_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2728_0_0_0;
extern Il2CppType InvokableCall_1_t2728_1_0_0;
struct InvokableCall_1_t2728;
extern Il2CppGenericClass InvokableCall_1_t2728_GenericClass;
TypeInfo InvokableCall_1_t2728_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2728_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2728_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2728_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2728_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2728_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2728_0_0_0/* byval_arg */
	, &InvokableCall_1_t2728_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2728_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2728_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2728)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Input2>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Input2>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Input2>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Input2>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Input2>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2729_UnityAction_1__ctor_m14098_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14098_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Input2>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14098_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2729_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2729_UnityAction_1__ctor_m14098_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14098_GenericMethod/* genericMethod */

};
extern Il2CppType Input2_t14_0_0_0;
static ParameterInfo UnityAction_1_t2729_UnityAction_1_Invoke_m14099_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Input2_t14_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14099_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Input2>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14099_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2729_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2729_UnityAction_1_Invoke_m14099_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14099_GenericMethod/* genericMethod */

};
extern Il2CppType Input2_t14_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2729_UnityAction_1_BeginInvoke_m14100_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &Input2_t14_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14100_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Input2>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14100_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2729_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2729_UnityAction_1_BeginInvoke_m14100_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14100_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2729_UnityAction_1_EndInvoke_m14101_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14101_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Input2>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14101_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2729_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2729_UnityAction_1_EndInvoke_m14101_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14101_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2729_MethodInfos[] =
{
	&UnityAction_1__ctor_m14098_MethodInfo,
	&UnityAction_1_Invoke_m14099_MethodInfo,
	&UnityAction_1_BeginInvoke_m14100_MethodInfo,
	&UnityAction_1_EndInvoke_m14101_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14100_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14101_MethodInfo;
static MethodInfo* UnityAction_1_t2729_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14099_MethodInfo,
	&UnityAction_1_BeginInvoke_m14100_MethodInfo,
	&UnityAction_1_EndInvoke_m14101_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2729_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2729_1_0_0;
struct UnityAction_1_t2729;
extern Il2CppGenericClass UnityAction_1_t2729_GenericClass;
TypeInfo UnityAction_1_t2729_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2729_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2729_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2729_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2729_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2729_0_0_0/* byval_arg */
	, &UnityAction_1_t2729_1_0_0/* this_arg */
	, UnityAction_1_t2729_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2729_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2729)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t3738_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.String>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.String>
extern MethodInfo IEnumerator_1_get_Current_m41287_MethodInfo;
static PropertyInfo IEnumerator_1_t3738____Current_PropertyInfo = 
{
	&IEnumerator_1_t3738_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41287_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t3738_PropertyInfos[] =
{
	&IEnumerator_1_t3738____Current_PropertyInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41287_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.String>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41287_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t3738_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41287_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t3738_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41287_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t3738_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t3738_0_0_0;
extern Il2CppType IEnumerator_1_t3738_1_0_0;
struct IEnumerator_1_t3738;
extern Il2CppGenericClass IEnumerator_1_t3738_GenericClass;
TypeInfo IEnumerator_1_t3738_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t3738_MethodInfos/* methods */
	, IEnumerator_1_t3738_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t3738_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t3738_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t3738_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t3738_0_0_0/* byval_arg */
	, &IEnumerator_1_t3738_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t3738_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.String>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_14.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2730_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.String>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_14MethodDeclarations.h"

extern TypeInfo String_t_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14106_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisString_t_m31606_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.String>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.String>(System.Int32)
#define Array_InternalArray__get_Item_TisString_t_m31606(__this, p0, method) (String_t*)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.String>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.String>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.String>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.String>::MoveNext()
// T System.Array/InternalEnumerator`1<System.String>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.String>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2730____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2730_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2730, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2730____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2730_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2730, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2730_FieldInfos[] =
{
	&InternalEnumerator_1_t2730____array_0_FieldInfo,
	&InternalEnumerator_1_t2730____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14103_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2730____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2730_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14103_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2730____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2730_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14106_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2730_PropertyInfos[] =
{
	&InternalEnumerator_1_t2730____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2730____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2730_InternalEnumerator_1__ctor_m14102_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14102_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.String>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14102_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2730_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2730_InternalEnumerator_1__ctor_m14102_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14102_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14103_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.String>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14103_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2730_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14103_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14104_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.String>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14104_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2730_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14104_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14105_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.String>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14105_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2730_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14105_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14106_GenericMethod;
// T System.Array/InternalEnumerator`1<System.String>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14106_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2730_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14106_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2730_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14102_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14103_MethodInfo,
	&InternalEnumerator_1_Dispose_m14104_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14105_MethodInfo,
	&InternalEnumerator_1_get_Current_m14106_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14105_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14104_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2730_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14103_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14105_MethodInfo,
	&InternalEnumerator_1_Dispose_m14104_MethodInfo,
	&InternalEnumerator_1_get_Current_m14106_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2730_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t3738_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2730_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t3738_il2cpp_TypeInfo, 7},
};
extern TypeInfo String_t_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2730_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14106_MethodInfo/* Method Usage */,
	&String_t_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisString_t_m31606_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2730_0_0_0;
extern Il2CppType InternalEnumerator_1_t2730_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2730_GenericClass;
TypeInfo InternalEnumerator_1_t2730_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2730_MethodInfos/* methods */
	, InternalEnumerator_1_t2730_PropertyInfos/* properties */
	, InternalEnumerator_1_t2730_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2730_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2730_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2730_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2730_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2730_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2730_1_0_0/* this_arg */
	, InternalEnumerator_1_t2730_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2730_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2730_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2730)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t3739_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.String>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.String>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.String>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.String>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.String>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.String>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.String>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.String>
extern MethodInfo ICollection_1_get_Count_m35902_MethodInfo;
static PropertyInfo ICollection_1_t3739____Count_PropertyInfo = 
{
	&ICollection_1_t3739_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m35902_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41288_MethodInfo;
static PropertyInfo ICollection_1_t3739____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t3739_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41288_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t3739_PropertyInfos[] =
{
	&ICollection_1_t3739____Count_PropertyInfo,
	&ICollection_1_t3739____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m35902_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.String>::get_Count()
MethodInfo ICollection_1_get_Count_m35902_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t3739_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m35902_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41288_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.String>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41288_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t3739_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41288_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo ICollection_1_t3739_ICollection_1_Add_m41289_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41289_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.String>::Add(T)
MethodInfo ICollection_1_Add_m41289_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t3739_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t3739_ICollection_1_Add_m41289_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41289_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41290_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.String>::Clear()
MethodInfo ICollection_1_Clear_m41290_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t3739_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41290_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo ICollection_1_t3739_ICollection_1_Contains_m41291_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41291_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.String>::Contains(T)
MethodInfo ICollection_1_Contains_m41291_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t3739_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t3739_ICollection_1_Contains_m41291_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41291_GenericMethod/* genericMethod */

};
extern Il2CppType StringU5BU5D_t112_0_0_0;
extern Il2CppType StringU5BU5D_t112_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t3739_ICollection_1_CopyTo_m35903_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &StringU5BU5D_t112_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m35903_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.String>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m35903_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t3739_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t3739_ICollection_1_CopyTo_m35903_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m35903_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo ICollection_1_t3739_ICollection_1_Remove_m41292_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41292_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.String>::Remove(T)
MethodInfo ICollection_1_Remove_m41292_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t3739_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t3739_ICollection_1_Remove_m41292_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41292_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t3739_MethodInfos[] =
{
	&ICollection_1_get_Count_m35902_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41288_MethodInfo,
	&ICollection_1_Add_m41289_MethodInfo,
	&ICollection_1_Clear_m41290_MethodInfo,
	&ICollection_1_Contains_m41291_MethodInfo,
	&ICollection_1_CopyTo_m35903_MethodInfo,
	&ICollection_1_Remove_m41292_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t3737_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t3739_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t3737_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t3739_0_0_0;
extern Il2CppType ICollection_1_t3739_1_0_0;
struct ICollection_1_t3739;
extern Il2CppGenericClass ICollection_1_t3739_GenericClass;
TypeInfo ICollection_1_t3739_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t3739_MethodInfos/* methods */
	, ICollection_1_t3739_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t3739_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t3739_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t3739_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t3739_0_0_0/* byval_arg */
	, &ICollection_1_t3739_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t3739_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.String>
extern Il2CppType IEnumerator_1_t3738_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41293_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41293_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t3737_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3738_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41293_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t3737_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41293_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t3737_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t3737_0_0_0;
extern Il2CppType IEnumerable_1_t3737_1_0_0;
struct IEnumerable_1_t3737;
extern Il2CppGenericClass IEnumerable_1_t3737_GenericClass;
TypeInfo IEnumerable_1_t3737_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t3737_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t3737_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t3737_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t3737_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t3737_0_0_0/* byval_arg */
	, &IEnumerable_1_t3737_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t3737_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t3743_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.String>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.String>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.String>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.String>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.String>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.String>
extern MethodInfo IList_1_get_Item_m41294_MethodInfo;
extern MethodInfo IList_1_set_Item_m41295_MethodInfo;
static PropertyInfo IList_1_t3743____Item_PropertyInfo = 
{
	&IList_1_t3743_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41294_MethodInfo/* get */
	, &IList_1_set_Item_m41295_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t3743_PropertyInfos[] =
{
	&IList_1_t3743____Item_PropertyInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
static ParameterInfo IList_1_t3743_IList_1_IndexOf_m41296_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41296_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.String>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41296_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t3743_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t3743_IList_1_IndexOf_m41296_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41296_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo IList_1_t3743_IList_1_Insert_m41297_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41297_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.String>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41297_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t3743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t3743_IList_1_Insert_m41297_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41297_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t3743_IList_1_RemoveAt_m41298_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41298_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.String>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41298_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t3743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t3743_IList_1_RemoveAt_m41298_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41298_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t3743_IList_1_get_Item_m41294_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41294_GenericMethod;
// T System.Collections.Generic.IList`1<System.String>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41294_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t3743_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t3743_IList_1_get_Item_m41294_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41294_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo IList_1_t3743_IList_1_set_Item_m41295_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41295_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.String>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41295_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t3743_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t3743_IList_1_set_Item_m41295_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41295_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t3743_MethodInfos[] =
{
	&IList_1_IndexOf_m41296_MethodInfo,
	&IList_1_Insert_m41297_MethodInfo,
	&IList_1_RemoveAt_m41298_MethodInfo,
	&IList_1_get_Item_m41294_MethodInfo,
	&IList_1_set_Item_m41295_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t3743_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t3739_il2cpp_TypeInfo,
	&IEnumerable_1_t3737_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t3743_0_0_0;
extern Il2CppType IList_1_t3743_1_0_0;
struct IList_1_t3743;
extern Il2CppGenericClass IList_1_t3743_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t3743_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t3743_MethodInfos/* methods */
	, IList_1_t3743_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t3743_il2cpp_TypeInfo/* element_class */
	, IList_1_t3743_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t3743_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t3743_0_0_0/* byval_arg */
	, &IList_1_t3743_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t3743_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7255_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>
extern MethodInfo ICollection_1_get_Count_m41299_MethodInfo;
static PropertyInfo ICollection_1_t7255____Count_PropertyInfo = 
{
	&ICollection_1_t7255_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41299_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41300_MethodInfo;
static PropertyInfo ICollection_1_t7255____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7255_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41300_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7255_PropertyInfos[] =
{
	&ICollection_1_t7255____Count_PropertyInfo,
	&ICollection_1_t7255____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41299_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::get_Count()
MethodInfo ICollection_1_get_Count_m41299_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7255_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41299_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41300_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41300_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7255_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41300_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_t1126_0_0_0;
extern Il2CppType IEnumerable_t1126_0_0_0;
static ParameterInfo ICollection_1_t7255_ICollection_1_Add_m41301_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_t1126_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41301_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Add(T)
MethodInfo ICollection_1_Add_m41301_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7255_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7255_ICollection_1_Add_m41301_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41301_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41302_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Clear()
MethodInfo ICollection_1_Clear_m41302_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7255_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41302_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_t1126_0_0_0;
static ParameterInfo ICollection_1_t7255_ICollection_1_Contains_m41303_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_t1126_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41303_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Contains(T)
MethodInfo ICollection_1_Contains_m41303_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7255_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7255_ICollection_1_Contains_m41303_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41303_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerableU5BU5D_t5206_0_0_0;
extern Il2CppType IEnumerableU5BU5D_t5206_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7255_ICollection_1_CopyTo_m41304_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerableU5BU5D_t5206_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41304_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41304_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7255_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7255_ICollection_1_CopyTo_m41304_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41304_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerable_t1126_0_0_0;
static ParameterInfo ICollection_1_t7255_ICollection_1_Remove_m41305_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEnumerable_t1126_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41305_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Collections.IEnumerable>::Remove(T)
MethodInfo ICollection_1_Remove_m41305_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7255_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7255_ICollection_1_Remove_m41305_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41305_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7255_MethodInfos[] =
{
	&ICollection_1_get_Count_m41299_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41300_MethodInfo,
	&ICollection_1_Add_m41301_MethodInfo,
	&ICollection_1_Clear_m41302_MethodInfo,
	&ICollection_1_Contains_m41303_MethodInfo,
	&ICollection_1_CopyTo_m41304_MethodInfo,
	&ICollection_1_Remove_m41305_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7257_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7255_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7257_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7255_0_0_0;
extern Il2CppType ICollection_1_t7255_1_0_0;
struct ICollection_1_t7255;
extern Il2CppGenericClass ICollection_1_t7255_GenericClass;
TypeInfo ICollection_1_t7255_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7255_MethodInfos/* methods */
	, ICollection_1_t7255_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7255_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7255_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7255_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7255_0_0_0/* byval_arg */
	, &ICollection_1_t7255_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7255_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>
extern Il2CppType IEnumerator_1_t5722_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41306_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerable>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41306_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7257_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5722_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41306_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7257_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41306_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7257_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7257_0_0_0;
extern Il2CppType IEnumerable_1_t7257_1_0_0;
struct IEnumerable_1_t7257;
extern Il2CppGenericClass IEnumerable_1_t7257_GenericClass;
TypeInfo IEnumerable_1_t7257_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7257_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7257_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7257_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7257_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7257_0_0_0/* byval_arg */
	, &IEnumerable_1_t7257_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7257_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
