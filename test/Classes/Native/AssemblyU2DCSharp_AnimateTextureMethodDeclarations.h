﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// AnimateTexture
struct AnimateTexture_t8;

// System.Void AnimateTexture::.ctor()
 void AnimateTexture__ctor_m5 (AnimateTexture_t8 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimateTexture::Awake()
 void AnimateTexture_Awake_m6 (AnimateTexture_t8 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimateTexture::Update()
 void AnimateTexture_Update_m7 (AnimateTexture_t8 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
