﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ContextBoundObject
struct ContextBoundObject_t2195;

// System.Void System.ContextBoundObject::.ctor()
 void ContextBoundObject__ctor_m12570 (ContextBoundObject_t2195 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
