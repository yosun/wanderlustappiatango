﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamImpl
struct WebCamImpl_t604;
// UnityEngine.Camera[]
struct CameraU5BU5D_t172;
// System.String
struct String_t;
// UnityEngine.Color32[]
struct Color32U5BU5D_t610;
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"
// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"
// Vuforia.QCARRenderer/VideoTextureInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoT.h"

// System.Boolean Vuforia.WebCamImpl::get_DidUpdateThisFrame()
 bool WebCamImpl_get_DidUpdateThisFrame_m4054 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamImpl::get_IsPlaying()
 bool WebCamImpl_get_IsPlaying_m4055 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WebCamImpl::get_ActualWidth()
 int32_t WebCamImpl_get_ActualWidth_m4056 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WebCamImpl::get_ActualHeight()
 int32_t WebCamImpl_get_ActualHeight_m4057 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamImpl::get_IsTextureSizeAvailable()
 bool WebCamImpl_get_IsTextureSizeAvailable_m4058 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::set_IsTextureSizeAvailable(System.Boolean)
 void WebCamImpl_set_IsTextureSizeAvailable_m4059 (WebCamImpl_t604 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamImpl::get_FlipHorizontally()
 bool WebCamImpl_get_FlipHorizontally_m4060 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/Vec2I Vuforia.WebCamImpl::get_ResampledTextureSize()
 Vec2I_t630  WebCamImpl_get_ResampledTextureSize_m4061 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::ComputeResampledTextureSize()
 void WebCamImpl_ComputeResampledTextureSize_m4062 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::.ctor(UnityEngine.Camera[],System.Int32,System.String,System.Boolean)
 void WebCamImpl__ctor_m4063 (WebCamImpl_t604 * __this, CameraU5BU5D_t172* ___arCameras, int32_t ___renderTextureLayer, String_t* ___webcamDeviceName, bool ___flipHorizontally, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::StartCamera()
 void WebCamImpl_StartCamera_m4064 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::StopCamera()
 void WebCamImpl_StopCamera_m4065 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::ResetPlaying()
 void WebCamImpl_ResetPlaying_m4066 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] Vuforia.WebCamImpl::GetPixels32AndBufferFrame()
 Color32U5BU5D_t610* WebCamImpl_GetPixels32AndBufferFrame_m4067 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::RenderFrame(System.Int32)
 void WebCamImpl_RenderFrame_m4068 (WebCamImpl_t604 * __this, int32_t ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/VideoModeData Vuforia.WebCamImpl::GetVideoMode()
 VideoModeData_t558  WebCamImpl_GetVideoMode_m4069 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/VideoTextureInfo Vuforia.WebCamImpl::GetVideoTextureInfo()
 VideoTextureInfo_t544  WebCamImpl_GetVideoTextureInfo_m4070 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamImpl::IsRendererDirty()
 bool WebCamImpl_IsRendererDirty_m4071 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::OnDestroy()
 void WebCamImpl_OnDestroy_m4072 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamImpl::Update()
 void WebCamImpl_Update_m4073 (WebCamImpl_t604 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
