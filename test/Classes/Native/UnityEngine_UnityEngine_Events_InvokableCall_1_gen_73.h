﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.ToggleGroup>
struct UnityAction_1_t3533;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.ToggleGroup>
struct InvokableCall_1_t3532  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.UI.ToggleGroup>::Delegate
	UnityAction_1_t3533 * ___Delegate_0;
};
