﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<OrbitCamera>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_9.h"
// UnityEngine.Events.CachedInvokableCall`1<OrbitCamera>
struct CachedInvokableCall_1_t2766  : public InvokableCall_1_t2767
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<OrbitCamera>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
