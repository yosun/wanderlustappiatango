﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$52
struct $ArrayType$52_t2280;
struct $ArrayType$52_t2280_marshaled;

void $ArrayType$52_t2280_marshal(const $ArrayType$52_t2280& unmarshaled, $ArrayType$52_t2280_marshaled& marshaled);
void $ArrayType$52_t2280_marshal_back(const $ArrayType$52_t2280_marshaled& marshaled, $ArrayType$52_t2280& unmarshaled);
void $ArrayType$52_t2280_marshal_cleanup($ArrayType$52_t2280_marshaled& marshaled);
