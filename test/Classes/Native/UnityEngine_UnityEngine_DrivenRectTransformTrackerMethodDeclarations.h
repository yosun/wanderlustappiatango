﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.DrivenRectTransformTracker
struct DrivenRectTransformTracker_t345;
// UnityEngine.Object
struct Object_t117;
struct Object_t117_marshaled;
// UnityEngine.RectTransform
struct RectTransform_t287;
// UnityEngine.DrivenTransformProperties
#include "UnityEngine_UnityEngine_DrivenTransformProperties.h"

// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
 void DrivenRectTransformTracker_Add_m2367 (DrivenRectTransformTracker_t345 * __this, Object_t117 * ___driver, RectTransform_t287 * ___rectTransform, int32_t ___drivenProperties, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
 void DrivenRectTransformTracker_Clear_m2365 (DrivenRectTransformTracker_t345 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
