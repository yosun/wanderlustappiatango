﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// UnityEngine.Object
struct Object_t117;
struct Object_t117_marshaled;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Object>
struct UnityAction_1_t4616  : public MulticastDelegate_t325
{
};
