﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.MatchCollection/Enumerator
struct Enumerator_t1408;
// System.Object
struct Object_t;
// System.Text.RegularExpressions.MatchCollection
struct MatchCollection_t1407;

// System.Void System.Text.RegularExpressions.MatchCollection/Enumerator::.ctor(System.Text.RegularExpressions.MatchCollection)
 void Enumerator__ctor_m7190 (Enumerator_t1408 * __this, MatchCollection_t1407 * ___coll, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Text.RegularExpressions.MatchCollection/Enumerator::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m7191 (Enumerator_t1408 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.MatchCollection/Enumerator::System.Collections.IEnumerator.MoveNext()
 bool Enumerator_System_Collections_IEnumerator_MoveNext_m7192 (Enumerator_t1408 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
