﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.FormatterServices
struct FormatterServices_t2071;
// System.Object
struct Object_t;
// System.Type
struct Type_t;

// System.Object System.Runtime.Serialization.FormatterServices::GetUninitializedObject(System.Type)
 Object_t * FormatterServices_GetUninitializedObject_m11762 (Object_t * __this/* static, unused */, Type_t * ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.FormatterServices::GetSafeUninitializedObject(System.Type)
 Object_t * FormatterServices_GetSafeUninitializedObject_m11763 (Object_t * __this/* static, unused */, Type_t * ___type, MethodInfo* method) IL2CPP_METHOD_ATTR;
