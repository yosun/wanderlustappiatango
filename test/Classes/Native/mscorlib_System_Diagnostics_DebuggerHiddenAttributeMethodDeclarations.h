﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_t452;

// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
 void DebuggerHiddenAttribute__ctor_m2059 (DebuggerHiddenAttribute_t452 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
