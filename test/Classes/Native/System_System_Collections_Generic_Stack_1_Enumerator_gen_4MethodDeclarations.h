﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<System.Type>
struct Enumerator_t4728;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t1159;

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m29012(__this, ___t, method) (void)Enumerator__ctor_m15728_gshared((Enumerator_t3036 *)__this, (Stack_1_t3035 *)___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m29013(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m15729_gshared((Enumerator_t3036 *)__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m29014(__this, method) (void)Enumerator_Dispose_m15730_gshared((Enumerator_t3036 *)__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m29015(__this, method) (bool)Enumerator_MoveNext_m15731_gshared((Enumerator_t3036 *)__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m29016(__this, method) (Type_t *)Enumerator_get_Current_m15732_gshared((Enumerator_t3036 *)__this, method)
