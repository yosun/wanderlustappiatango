﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IList_1_t7759_il2cpp_TypeInfo;

// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Void
#include "mscorlib_System_Void.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Vector2>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector2>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Vector2>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Vector2>
extern MethodInfo IList_1_get_Item_m44081_MethodInfo;
extern MethodInfo IList_1_set_Item_m44082_MethodInfo;
static PropertyInfo IList_1_t7759____Item_PropertyInfo = 
{
	&IList_1_t7759_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44081_MethodInfo/* get */
	, &IList_1_set_Item_m44082_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7759_PropertyInfos[] =
{
	&IList_1_t7759____Item_PropertyInfo,
	NULL
};
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType Vector2_t9_0_0_0;
static ParameterInfo IList_1_t7759_IList_1_IndexOf_m44083_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Vector2_t9 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44083_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Vector2>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44083_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7759_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Vector2_t9/* invoker_method */
	, IList_1_t7759_IList_1_IndexOf_m44083_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44083_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Vector2_t9_0_0_0;
static ParameterInfo IList_1_t7759_IList_1_Insert_m44084_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Vector2_t9 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44084_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector2>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44084_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7759_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Vector2_t9/* invoker_method */
	, IList_1_t7759_IList_1_Insert_m44084_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44084_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7759_IList_1_RemoveAt_m44085_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44085_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44085_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7759_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7759_IList_1_RemoveAt_m44085_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44085_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7759_IList_1_get_Item_m44081_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Vector2_t9_0_0_0;
extern void* RuntimeInvoker_Vector2_t9_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44081_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Vector2>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44081_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7759_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t9_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t9_Int32_t93/* invoker_method */
	, IList_1_t7759_IList_1_get_Item_m44081_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44081_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Vector2_t9_0_0_0;
static ParameterInfo IList_1_t7759_IList_1_set_Item_m44082_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Vector2_t9 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44082_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44082_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7759_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Vector2_t9/* invoker_method */
	, IList_1_t7759_IList_1_set_Item_m44082_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44082_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7759_MethodInfos[] =
{
	&IList_1_IndexOf_m44083_MethodInfo,
	&IList_1_Insert_m44084_MethodInfo,
	&IList_1_RemoveAt_m44085_MethodInfo,
	&IList_1_get_Item_m44081_MethodInfo,
	&IList_1_set_Item_m44082_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t7758_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7760_il2cpp_TypeInfo;
static TypeInfo* IList_1_t7759_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7758_il2cpp_TypeInfo,
	&IEnumerable_1_t7760_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7759_0_0_0;
extern Il2CppType IList_1_t7759_1_0_0;
struct IList_1_t7759;
extern Il2CppGenericClass IList_1_t7759_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7759_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7759_MethodInfos/* methods */
	, IList_1_t7759_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7759_il2cpp_TypeInfo/* element_class */
	, IList_1_t7759_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7759_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7759_0_0_0/* byval_arg */
	, &IList_1_t7759_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7759_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6100_il2cpp_TypeInfo;

// UnityEngine.UI.Image/Type
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Type>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Type>
extern MethodInfo IEnumerator_1_get_Current_m44086_MethodInfo;
static PropertyInfo IEnumerator_1_t6100____Current_PropertyInfo = 
{
	&IEnumerator_1_t6100_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44086_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6100_PropertyInfos[] =
{
	&IEnumerator_1_t6100____Current_PropertyInfo,
	NULL
};
extern Il2CppType Type_t303_0_0_0;
extern void* RuntimeInvoker_Type_t303 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44086_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Type>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44086_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6100_il2cpp_TypeInfo/* declaring_type */
	, &Type_t303_0_0_0/* return_type */
	, RuntimeInvoker_Type_t303/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44086_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6100_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44086_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6100_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6100_0_0_0;
extern Il2CppType IEnumerator_1_t6100_1_0_0;
struct IEnumerator_1_t6100;
extern Il2CppGenericClass IEnumerator_1_t6100_GenericClass;
TypeInfo IEnumerator_1_t6100_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6100_MethodInfos/* methods */
	, IEnumerator_1_t6100_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6100_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6100_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6100_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6100_0_0_0/* byval_arg */
	, &IEnumerator_1_t6100_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6100_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_206.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3421_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_206MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo Type_t303_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1493_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m18796_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7651_MethodInfo;
extern MethodInfo Array_get_Length_m7656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisType_t303_m34122_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/Type>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/Type>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisType_t303_m34122 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18792_MethodInfo;
 void InternalEnumerator_1__ctor_m18792 (InternalEnumerator_1_t3421 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18793_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18793 (InternalEnumerator_1_t3421 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m18796(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18796_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Type_t303_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18794_MethodInfo;
 void InternalEnumerator_1_Dispose_m18794 (InternalEnumerator_1_t3421 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18795_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18795 (InternalEnumerator_1_t3421 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18796 (InternalEnumerator_1_t3421 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisType_t303_m34122(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisType_t303_m34122_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3421____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3421_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3421, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3421____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3421_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3421, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3421_FieldInfos[] =
{
	&InternalEnumerator_1_t3421____array_0_FieldInfo,
	&InternalEnumerator_1_t3421____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3421____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3421_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18793_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3421____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3421_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18796_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3421_PropertyInfos[] =
{
	&InternalEnumerator_1_t3421____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3421____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3421_InternalEnumerator_1__ctor_m18792_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18792_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18792_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18792/* method */
	, &InternalEnumerator_1_t3421_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3421_InternalEnumerator_1__ctor_m18792_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18792_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18793_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18793_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18793/* method */
	, &InternalEnumerator_1_t3421_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18793_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18794_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18794_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18794/* method */
	, &InternalEnumerator_1_t3421_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18794_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18795_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18795_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18795/* method */
	, &InternalEnumerator_1_t3421_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18795_GenericMethod/* genericMethod */

};
extern Il2CppType Type_t303_0_0_0;
extern void* RuntimeInvoker_Type_t303 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18796_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Type>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18796_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18796/* method */
	, &InternalEnumerator_1_t3421_il2cpp_TypeInfo/* declaring_type */
	, &Type_t303_0_0_0/* return_type */
	, RuntimeInvoker_Type_t303/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18796_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3421_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18792_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18793_MethodInfo,
	&InternalEnumerator_1_Dispose_m18794_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18795_MethodInfo,
	&InternalEnumerator_1_get_Current_m18796_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3421_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18793_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18795_MethodInfo,
	&InternalEnumerator_1_Dispose_m18794_MethodInfo,
	&InternalEnumerator_1_get_Current_m18796_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3421_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6100_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3421_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6100_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3421_0_0_0;
extern Il2CppType InternalEnumerator_1_t3421_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t3421_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t3421_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3421_MethodInfos/* methods */
	, InternalEnumerator_1_t3421_PropertyInfos/* properties */
	, InternalEnumerator_1_t3421_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3421_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3421_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3421_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3421_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3421_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3421_1_0_0/* this_arg */
	, InternalEnumerator_1_t3421_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3421_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3421)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7761_il2cpp_TypeInfo;

#include "UnityEngine.UI_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>
extern MethodInfo ICollection_1_get_Count_m44087_MethodInfo;
static PropertyInfo ICollection_1_t7761____Count_PropertyInfo = 
{
	&ICollection_1_t7761_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44087_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44088_MethodInfo;
static PropertyInfo ICollection_1_t7761____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7761_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44088_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7761_PropertyInfos[] =
{
	&ICollection_1_t7761____Count_PropertyInfo,
	&ICollection_1_t7761____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44087_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::get_Count()
MethodInfo ICollection_1_get_Count_m44087_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7761_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44087_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44088_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44088_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7761_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44088_GenericMethod/* genericMethod */

};
extern Il2CppType Type_t303_0_0_0;
extern Il2CppType Type_t303_0_0_0;
static ParameterInfo ICollection_1_t7761_ICollection_1_Add_m44089_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Type_t303_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44089_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::Add(T)
MethodInfo ICollection_1_Add_m44089_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7761_ICollection_1_Add_m44089_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44089_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44090_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::Clear()
MethodInfo ICollection_1_Clear_m44090_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44090_GenericMethod/* genericMethod */

};
extern Il2CppType Type_t303_0_0_0;
static ParameterInfo ICollection_1_t7761_ICollection_1_Contains_m44091_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Type_t303_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44091_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::Contains(T)
MethodInfo ICollection_1_Contains_m44091_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7761_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7761_ICollection_1_Contains_m44091_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44091_GenericMethod/* genericMethod */

};
extern Il2CppType TypeU5BU5D_t5591_0_0_0;
extern Il2CppType TypeU5BU5D_t5591_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7761_ICollection_1_CopyTo_m44092_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TypeU5BU5D_t5591_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44092_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44092_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7761_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7761_ICollection_1_CopyTo_m44092_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44092_GenericMethod/* genericMethod */

};
extern Il2CppType Type_t303_0_0_0;
static ParameterInfo ICollection_1_t7761_ICollection_1_Remove_m44093_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Type_t303_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44093_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Type>::Remove(T)
MethodInfo ICollection_1_Remove_m44093_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7761_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7761_ICollection_1_Remove_m44093_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44093_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7761_MethodInfos[] =
{
	&ICollection_1_get_Count_m44087_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44088_MethodInfo,
	&ICollection_1_Add_m44089_MethodInfo,
	&ICollection_1_Clear_m44090_MethodInfo,
	&ICollection_1_Contains_m44091_MethodInfo,
	&ICollection_1_CopyTo_m44092_MethodInfo,
	&ICollection_1_Remove_m44093_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7763_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7761_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7763_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7761_0_0_0;
extern Il2CppType ICollection_1_t7761_1_0_0;
struct ICollection_1_t7761;
extern Il2CppGenericClass ICollection_1_t7761_GenericClass;
TypeInfo ICollection_1_t7761_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7761_MethodInfos/* methods */
	, ICollection_1_t7761_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7761_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7761_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7761_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7761_0_0_0/* byval_arg */
	, &ICollection_1_t7761_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7761_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Type>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Type>
extern Il2CppType IEnumerator_1_t6100_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44094_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Type>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44094_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7763_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6100_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44094_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7763_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44094_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7763_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7763_0_0_0;
extern Il2CppType IEnumerable_1_t7763_1_0_0;
struct IEnumerable_1_t7763;
extern Il2CppGenericClass IEnumerable_1_t7763_GenericClass;
TypeInfo IEnumerable_1_t7763_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7763_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7763_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7763_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7763_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7763_0_0_0/* byval_arg */
	, &IEnumerable_1_t7763_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7763_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7762_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/Type>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Type>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Type>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/Type>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Type>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/Type>
extern MethodInfo IList_1_get_Item_m44095_MethodInfo;
extern MethodInfo IList_1_set_Item_m44096_MethodInfo;
static PropertyInfo IList_1_t7762____Item_PropertyInfo = 
{
	&IList_1_t7762_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44095_MethodInfo/* get */
	, &IList_1_set_Item_m44096_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7762_PropertyInfos[] =
{
	&IList_1_t7762____Item_PropertyInfo,
	NULL
};
extern Il2CppType Type_t303_0_0_0;
static ParameterInfo IList_1_t7762_IList_1_IndexOf_m44097_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Type_t303_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44097_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/Type>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44097_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7762_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7762_IList_1_IndexOf_m44097_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44097_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Type_t303_0_0_0;
static ParameterInfo IList_1_t7762_IList_1_Insert_m44098_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Type_t303_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44098_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Type>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44098_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7762_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7762_IList_1_Insert_m44098_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44098_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7762_IList_1_RemoveAt_m44099_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44099_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Type>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44099_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7762_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7762_IList_1_RemoveAt_m44099_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44099_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7762_IList_1_get_Item_m44095_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Type_t303_0_0_0;
extern void* RuntimeInvoker_Type_t303_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44095_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/Type>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44095_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7762_il2cpp_TypeInfo/* declaring_type */
	, &Type_t303_0_0_0/* return_type */
	, RuntimeInvoker_Type_t303_Int32_t93/* invoker_method */
	, IList_1_t7762_IList_1_get_Item_m44095_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44095_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Type_t303_0_0_0;
static ParameterInfo IList_1_t7762_IList_1_set_Item_m44096_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Type_t303_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44096_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Type>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44096_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7762_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7762_IList_1_set_Item_m44096_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44096_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7762_MethodInfos[] =
{
	&IList_1_IndexOf_m44097_MethodInfo,
	&IList_1_Insert_m44098_MethodInfo,
	&IList_1_RemoveAt_m44099_MethodInfo,
	&IList_1_get_Item_m44095_MethodInfo,
	&IList_1_set_Item_m44096_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7762_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7761_il2cpp_TypeInfo,
	&IEnumerable_1_t7763_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7762_0_0_0;
extern Il2CppType IList_1_t7762_1_0_0;
struct IList_1_t7762;
extern Il2CppGenericClass IList_1_t7762_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7762_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7762_MethodInfos/* methods */
	, IList_1_t7762_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7762_il2cpp_TypeInfo/* element_class */
	, IList_1_t7762_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7762_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7762_0_0_0/* byval_arg */
	, &IList_1_t7762_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7762_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6101_il2cpp_TypeInfo;

// UnityEngine.UI.Image/FillMethod
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/FillMethod>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/FillMethod>
extern MethodInfo IEnumerator_1_get_Current_m44100_MethodInfo;
static PropertyInfo IEnumerator_1_t6101____Current_PropertyInfo = 
{
	&IEnumerator_1_t6101_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44100_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6101_PropertyInfos[] =
{
	&IEnumerator_1_t6101____Current_PropertyInfo,
	NULL
};
extern Il2CppType FillMethod_t304_0_0_0;
extern void* RuntimeInvoker_FillMethod_t304 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44100_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/FillMethod>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44100_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6101_il2cpp_TypeInfo/* declaring_type */
	, &FillMethod_t304_0_0_0/* return_type */
	, RuntimeInvoker_FillMethod_t304/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44100_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6101_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44100_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6101_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6101_0_0_0;
extern Il2CppType IEnumerator_1_t6101_1_0_0;
struct IEnumerator_1_t6101;
extern Il2CppGenericClass IEnumerator_1_t6101_GenericClass;
TypeInfo IEnumerator_1_t6101_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6101_MethodInfos/* methods */
	, IEnumerator_1_t6101_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6101_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6101_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6101_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6101_0_0_0/* byval_arg */
	, &IEnumerator_1_t6101_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6101_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_207.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3422_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_207MethodDeclarations.h"

extern TypeInfo FillMethod_t304_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18801_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFillMethod_t304_m34133_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/FillMethod>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/FillMethod>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisFillMethod_t304_m34133 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18797_MethodInfo;
 void InternalEnumerator_1__ctor_m18797 (InternalEnumerator_1_t3422 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18798_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18798 (InternalEnumerator_1_t3422 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m18801(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18801_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&FillMethod_t304_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18799_MethodInfo;
 void InternalEnumerator_1_Dispose_m18799 (InternalEnumerator_1_t3422 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18800_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18800 (InternalEnumerator_1_t3422 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18801 (InternalEnumerator_1_t3422 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisFillMethod_t304_m34133(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisFillMethod_t304_m34133_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3422____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3422_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3422, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3422____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3422_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3422, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3422_FieldInfos[] =
{
	&InternalEnumerator_1_t3422____array_0_FieldInfo,
	&InternalEnumerator_1_t3422____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3422____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3422_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18798_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3422____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3422_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18801_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3422_PropertyInfos[] =
{
	&InternalEnumerator_1_t3422____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3422____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3422_InternalEnumerator_1__ctor_m18797_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18797_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18797_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18797/* method */
	, &InternalEnumerator_1_t3422_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3422_InternalEnumerator_1__ctor_m18797_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18797_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18798_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18798_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18798/* method */
	, &InternalEnumerator_1_t3422_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18798_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18799_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18799_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18799/* method */
	, &InternalEnumerator_1_t3422_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18799_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18800_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18800_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18800/* method */
	, &InternalEnumerator_1_t3422_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18800_GenericMethod/* genericMethod */

};
extern Il2CppType FillMethod_t304_0_0_0;
extern void* RuntimeInvoker_FillMethod_t304 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18801_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/FillMethod>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18801_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18801/* method */
	, &InternalEnumerator_1_t3422_il2cpp_TypeInfo/* declaring_type */
	, &FillMethod_t304_0_0_0/* return_type */
	, RuntimeInvoker_FillMethod_t304/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18801_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3422_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18797_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18798_MethodInfo,
	&InternalEnumerator_1_Dispose_m18799_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18800_MethodInfo,
	&InternalEnumerator_1_get_Current_m18801_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3422_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18798_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18800_MethodInfo,
	&InternalEnumerator_1_Dispose_m18799_MethodInfo,
	&InternalEnumerator_1_get_Current_m18801_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3422_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6101_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3422_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6101_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3422_0_0_0;
extern Il2CppType InternalEnumerator_1_t3422_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3422_GenericClass;
TypeInfo InternalEnumerator_1_t3422_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3422_MethodInfos/* methods */
	, InternalEnumerator_1_t3422_PropertyInfos/* properties */
	, InternalEnumerator_1_t3422_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3422_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3422_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3422_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3422_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3422_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3422_1_0_0/* this_arg */
	, InternalEnumerator_1_t3422_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3422_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3422)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7764_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>
extern MethodInfo ICollection_1_get_Count_m44101_MethodInfo;
static PropertyInfo ICollection_1_t7764____Count_PropertyInfo = 
{
	&ICollection_1_t7764_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44101_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44102_MethodInfo;
static PropertyInfo ICollection_1_t7764____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7764_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44102_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7764_PropertyInfos[] =
{
	&ICollection_1_t7764____Count_PropertyInfo,
	&ICollection_1_t7764____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44101_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::get_Count()
MethodInfo ICollection_1_get_Count_m44101_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7764_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44101_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44102_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44102_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7764_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44102_GenericMethod/* genericMethod */

};
extern Il2CppType FillMethod_t304_0_0_0;
extern Il2CppType FillMethod_t304_0_0_0;
static ParameterInfo ICollection_1_t7764_ICollection_1_Add_m44103_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FillMethod_t304_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44103_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::Add(T)
MethodInfo ICollection_1_Add_m44103_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7764_ICollection_1_Add_m44103_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44103_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44104_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::Clear()
MethodInfo ICollection_1_Clear_m44104_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44104_GenericMethod/* genericMethod */

};
extern Il2CppType FillMethod_t304_0_0_0;
static ParameterInfo ICollection_1_t7764_ICollection_1_Contains_m44105_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FillMethod_t304_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44105_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::Contains(T)
MethodInfo ICollection_1_Contains_m44105_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7764_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7764_ICollection_1_Contains_m44105_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44105_GenericMethod/* genericMethod */

};
extern Il2CppType FillMethodU5BU5D_t5592_0_0_0;
extern Il2CppType FillMethodU5BU5D_t5592_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7764_ICollection_1_CopyTo_m44106_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FillMethodU5BU5D_t5592_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44106_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44106_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7764_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7764_ICollection_1_CopyTo_m44106_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44106_GenericMethod/* genericMethod */

};
extern Il2CppType FillMethod_t304_0_0_0;
static ParameterInfo ICollection_1_t7764_ICollection_1_Remove_m44107_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FillMethod_t304_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44107_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/FillMethod>::Remove(T)
MethodInfo ICollection_1_Remove_m44107_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7764_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7764_ICollection_1_Remove_m44107_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44107_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7764_MethodInfos[] =
{
	&ICollection_1_get_Count_m44101_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44102_MethodInfo,
	&ICollection_1_Add_m44103_MethodInfo,
	&ICollection_1_Clear_m44104_MethodInfo,
	&ICollection_1_Contains_m44105_MethodInfo,
	&ICollection_1_CopyTo_m44106_MethodInfo,
	&ICollection_1_Remove_m44107_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7766_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7764_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7766_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7764_0_0_0;
extern Il2CppType ICollection_1_t7764_1_0_0;
struct ICollection_1_t7764;
extern Il2CppGenericClass ICollection_1_t7764_GenericClass;
TypeInfo ICollection_1_t7764_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7764_MethodInfos/* methods */
	, ICollection_1_t7764_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7764_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7764_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7764_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7764_0_0_0/* byval_arg */
	, &ICollection_1_t7764_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7764_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/FillMethod>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/FillMethod>
extern Il2CppType IEnumerator_1_t6101_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44108_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/FillMethod>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44108_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7766_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6101_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44108_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7766_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44108_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7766_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7766_0_0_0;
extern Il2CppType IEnumerable_1_t7766_1_0_0;
struct IEnumerable_1_t7766;
extern Il2CppGenericClass IEnumerable_1_t7766_GenericClass;
TypeInfo IEnumerable_1_t7766_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7766_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7766_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7766_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7766_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7766_0_0_0/* byval_arg */
	, &IEnumerable_1_t7766_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7766_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7765_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/FillMethod>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/FillMethod>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/FillMethod>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/FillMethod>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/FillMethod>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/FillMethod>
extern MethodInfo IList_1_get_Item_m44109_MethodInfo;
extern MethodInfo IList_1_set_Item_m44110_MethodInfo;
static PropertyInfo IList_1_t7765____Item_PropertyInfo = 
{
	&IList_1_t7765_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44109_MethodInfo/* get */
	, &IList_1_set_Item_m44110_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7765_PropertyInfos[] =
{
	&IList_1_t7765____Item_PropertyInfo,
	NULL
};
extern Il2CppType FillMethod_t304_0_0_0;
static ParameterInfo IList_1_t7765_IList_1_IndexOf_m44111_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FillMethod_t304_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44111_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/FillMethod>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44111_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7765_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7765_IList_1_IndexOf_m44111_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44111_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FillMethod_t304_0_0_0;
static ParameterInfo IList_1_t7765_IList_1_Insert_m44112_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FillMethod_t304_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44112_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/FillMethod>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44112_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7765_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7765_IList_1_Insert_m44112_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44112_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7765_IList_1_RemoveAt_m44113_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44113_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/FillMethod>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44113_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7765_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7765_IList_1_RemoveAt_m44113_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44113_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7765_IList_1_get_Item_m44109_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType FillMethod_t304_0_0_0;
extern void* RuntimeInvoker_FillMethod_t304_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44109_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/FillMethod>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44109_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7765_il2cpp_TypeInfo/* declaring_type */
	, &FillMethod_t304_0_0_0/* return_type */
	, RuntimeInvoker_FillMethod_t304_Int32_t93/* invoker_method */
	, IList_1_t7765_IList_1_get_Item_m44109_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44109_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FillMethod_t304_0_0_0;
static ParameterInfo IList_1_t7765_IList_1_set_Item_m44110_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FillMethod_t304_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44110_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/FillMethod>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44110_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7765_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7765_IList_1_set_Item_m44110_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44110_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7765_MethodInfos[] =
{
	&IList_1_IndexOf_m44111_MethodInfo,
	&IList_1_Insert_m44112_MethodInfo,
	&IList_1_RemoveAt_m44113_MethodInfo,
	&IList_1_get_Item_m44109_MethodInfo,
	&IList_1_set_Item_m44110_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7765_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7764_il2cpp_TypeInfo,
	&IEnumerable_1_t7766_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7765_0_0_0;
extern Il2CppType IList_1_t7765_1_0_0;
struct IList_1_t7765;
extern Il2CppGenericClass IList_1_t7765_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7765_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7765_MethodInfos/* methods */
	, IList_1_t7765_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7765_il2cpp_TypeInfo/* element_class */
	, IList_1_t7765_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7765_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7765_0_0_0/* byval_arg */
	, &IList_1_t7765_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7765_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6103_il2cpp_TypeInfo;

// UnityEngine.UI.Image/OriginHorizontal
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginHorizontal.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>
extern MethodInfo IEnumerator_1_get_Current_m44114_MethodInfo;
static PropertyInfo IEnumerator_1_t6103____Current_PropertyInfo = 
{
	&IEnumerator_1_t6103_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44114_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6103_PropertyInfos[] =
{
	&IEnumerator_1_t6103____Current_PropertyInfo,
	NULL
};
extern Il2CppType OriginHorizontal_t305_0_0_0;
extern void* RuntimeInvoker_OriginHorizontal_t305 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44114_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44114_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6103_il2cpp_TypeInfo/* declaring_type */
	, &OriginHorizontal_t305_0_0_0/* return_type */
	, RuntimeInvoker_OriginHorizontal_t305/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44114_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6103_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44114_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6103_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6103_0_0_0;
extern Il2CppType IEnumerator_1_t6103_1_0_0;
struct IEnumerator_1_t6103;
extern Il2CppGenericClass IEnumerator_1_t6103_GenericClass;
TypeInfo IEnumerator_1_t6103_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6103_MethodInfos/* methods */
	, IEnumerator_1_t6103_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6103_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6103_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6103_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6103_0_0_0/* byval_arg */
	, &IEnumerator_1_t6103_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6103_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_208.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3423_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_208MethodDeclarations.h"

extern TypeInfo OriginHorizontal_t305_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18806_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisOriginHorizontal_t305_m34144_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/OriginHorizontal>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/OriginHorizontal>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisOriginHorizontal_t305_m34144 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18802_MethodInfo;
 void InternalEnumerator_1__ctor_m18802 (InternalEnumerator_1_t3423 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18803_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18803 (InternalEnumerator_1_t3423 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m18806(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18806_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&OriginHorizontal_t305_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18804_MethodInfo;
 void InternalEnumerator_1_Dispose_m18804 (InternalEnumerator_1_t3423 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18805_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18805 (InternalEnumerator_1_t3423 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18806 (InternalEnumerator_1_t3423 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisOriginHorizontal_t305_m34144(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisOriginHorizontal_t305_m34144_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3423____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3423_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3423, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3423____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3423_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3423, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3423_FieldInfos[] =
{
	&InternalEnumerator_1_t3423____array_0_FieldInfo,
	&InternalEnumerator_1_t3423____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3423____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3423_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18803_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3423____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3423_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18806_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3423_PropertyInfos[] =
{
	&InternalEnumerator_1_t3423____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3423____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3423_InternalEnumerator_1__ctor_m18802_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18802_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18802_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18802/* method */
	, &InternalEnumerator_1_t3423_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3423_InternalEnumerator_1__ctor_m18802_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18802_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18803_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18803_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18803/* method */
	, &InternalEnumerator_1_t3423_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18803_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18804_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18804_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18804/* method */
	, &InternalEnumerator_1_t3423_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18804_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18805_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18805_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18805/* method */
	, &InternalEnumerator_1_t3423_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18805_GenericMethod/* genericMethod */

};
extern Il2CppType OriginHorizontal_t305_0_0_0;
extern void* RuntimeInvoker_OriginHorizontal_t305 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18806_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginHorizontal>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18806_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18806/* method */
	, &InternalEnumerator_1_t3423_il2cpp_TypeInfo/* declaring_type */
	, &OriginHorizontal_t305_0_0_0/* return_type */
	, RuntimeInvoker_OriginHorizontal_t305/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18806_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3423_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18802_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18803_MethodInfo,
	&InternalEnumerator_1_Dispose_m18804_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18805_MethodInfo,
	&InternalEnumerator_1_get_Current_m18806_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3423_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18803_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18805_MethodInfo,
	&InternalEnumerator_1_Dispose_m18804_MethodInfo,
	&InternalEnumerator_1_get_Current_m18806_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3423_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6103_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3423_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6103_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3423_0_0_0;
extern Il2CppType InternalEnumerator_1_t3423_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3423_GenericClass;
TypeInfo InternalEnumerator_1_t3423_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3423_MethodInfos/* methods */
	, InternalEnumerator_1_t3423_PropertyInfos/* properties */
	, InternalEnumerator_1_t3423_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3423_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3423_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3423_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3423_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3423_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3423_1_0_0/* this_arg */
	, InternalEnumerator_1_t3423_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3423_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3423)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7767_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>
extern MethodInfo ICollection_1_get_Count_m44115_MethodInfo;
static PropertyInfo ICollection_1_t7767____Count_PropertyInfo = 
{
	&ICollection_1_t7767_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44115_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44116_MethodInfo;
static PropertyInfo ICollection_1_t7767____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7767_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44116_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7767_PropertyInfos[] =
{
	&ICollection_1_t7767____Count_PropertyInfo,
	&ICollection_1_t7767____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44115_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::get_Count()
MethodInfo ICollection_1_get_Count_m44115_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7767_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44115_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44116_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44116_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7767_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44116_GenericMethod/* genericMethod */

};
extern Il2CppType OriginHorizontal_t305_0_0_0;
extern Il2CppType OriginHorizontal_t305_0_0_0;
static ParameterInfo ICollection_1_t7767_ICollection_1_Add_m44117_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &OriginHorizontal_t305_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44117_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::Add(T)
MethodInfo ICollection_1_Add_m44117_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7767_ICollection_1_Add_m44117_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44117_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44118_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::Clear()
MethodInfo ICollection_1_Clear_m44118_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44118_GenericMethod/* genericMethod */

};
extern Il2CppType OriginHorizontal_t305_0_0_0;
static ParameterInfo ICollection_1_t7767_ICollection_1_Contains_m44119_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &OriginHorizontal_t305_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44119_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::Contains(T)
MethodInfo ICollection_1_Contains_m44119_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7767_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7767_ICollection_1_Contains_m44119_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44119_GenericMethod/* genericMethod */

};
extern Il2CppType OriginHorizontalU5BU5D_t5593_0_0_0;
extern Il2CppType OriginHorizontalU5BU5D_t5593_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7767_ICollection_1_CopyTo_m44120_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &OriginHorizontalU5BU5D_t5593_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44120_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44120_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7767_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7767_ICollection_1_CopyTo_m44120_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44120_GenericMethod/* genericMethod */

};
extern Il2CppType OriginHorizontal_t305_0_0_0;
static ParameterInfo ICollection_1_t7767_ICollection_1_Remove_m44121_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &OriginHorizontal_t305_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44121_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginHorizontal>::Remove(T)
MethodInfo ICollection_1_Remove_m44121_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7767_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7767_ICollection_1_Remove_m44121_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44121_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7767_MethodInfos[] =
{
	&ICollection_1_get_Count_m44115_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44116_MethodInfo,
	&ICollection_1_Add_m44117_MethodInfo,
	&ICollection_1_Clear_m44118_MethodInfo,
	&ICollection_1_Contains_m44119_MethodInfo,
	&ICollection_1_CopyTo_m44120_MethodInfo,
	&ICollection_1_Remove_m44121_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7769_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7767_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7769_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7767_0_0_0;
extern Il2CppType ICollection_1_t7767_1_0_0;
struct ICollection_1_t7767;
extern Il2CppGenericClass ICollection_1_t7767_GenericClass;
TypeInfo ICollection_1_t7767_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7767_MethodInfos/* methods */
	, ICollection_1_t7767_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7767_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7767_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7767_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7767_0_0_0/* byval_arg */
	, &ICollection_1_t7767_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7767_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/OriginHorizontal>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/OriginHorizontal>
extern Il2CppType IEnumerator_1_t6103_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44122_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/OriginHorizontal>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44122_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7769_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6103_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44122_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7769_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44122_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7769_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7769_0_0_0;
extern Il2CppType IEnumerable_1_t7769_1_0_0;
struct IEnumerable_1_t7769;
extern Il2CppGenericClass IEnumerable_1_t7769_GenericClass;
TypeInfo IEnumerable_1_t7769_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7769_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7769_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7769_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7769_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7769_0_0_0/* byval_arg */
	, &IEnumerable_1_t7769_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7769_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7768_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginHorizontal>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginHorizontal>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginHorizontal>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginHorizontal>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginHorizontal>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginHorizontal>
extern MethodInfo IList_1_get_Item_m44123_MethodInfo;
extern MethodInfo IList_1_set_Item_m44124_MethodInfo;
static PropertyInfo IList_1_t7768____Item_PropertyInfo = 
{
	&IList_1_t7768_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44123_MethodInfo/* get */
	, &IList_1_set_Item_m44124_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7768_PropertyInfos[] =
{
	&IList_1_t7768____Item_PropertyInfo,
	NULL
};
extern Il2CppType OriginHorizontal_t305_0_0_0;
static ParameterInfo IList_1_t7768_IList_1_IndexOf_m44125_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &OriginHorizontal_t305_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44125_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginHorizontal>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44125_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7768_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7768_IList_1_IndexOf_m44125_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44125_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType OriginHorizontal_t305_0_0_0;
static ParameterInfo IList_1_t7768_IList_1_Insert_m44126_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &OriginHorizontal_t305_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44126_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginHorizontal>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44126_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7768_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7768_IList_1_Insert_m44126_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44126_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7768_IList_1_RemoveAt_m44127_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44127_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginHorizontal>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44127_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7768_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7768_IList_1_RemoveAt_m44127_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44127_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7768_IList_1_get_Item_m44123_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType OriginHorizontal_t305_0_0_0;
extern void* RuntimeInvoker_OriginHorizontal_t305_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44123_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginHorizontal>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44123_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7768_il2cpp_TypeInfo/* declaring_type */
	, &OriginHorizontal_t305_0_0_0/* return_type */
	, RuntimeInvoker_OriginHorizontal_t305_Int32_t93/* invoker_method */
	, IList_1_t7768_IList_1_get_Item_m44123_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44123_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType OriginHorizontal_t305_0_0_0;
static ParameterInfo IList_1_t7768_IList_1_set_Item_m44124_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &OriginHorizontal_t305_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44124_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginHorizontal>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44124_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7768_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7768_IList_1_set_Item_m44124_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44124_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7768_MethodInfos[] =
{
	&IList_1_IndexOf_m44125_MethodInfo,
	&IList_1_Insert_m44126_MethodInfo,
	&IList_1_RemoveAt_m44127_MethodInfo,
	&IList_1_get_Item_m44123_MethodInfo,
	&IList_1_set_Item_m44124_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7768_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7767_il2cpp_TypeInfo,
	&IEnumerable_1_t7769_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7768_0_0_0;
extern Il2CppType IList_1_t7768_1_0_0;
struct IList_1_t7768;
extern Il2CppGenericClass IList_1_t7768_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7768_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7768_MethodInfos/* methods */
	, IList_1_t7768_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7768_il2cpp_TypeInfo/* element_class */
	, IList_1_t7768_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7768_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7768_0_0_0/* byval_arg */
	, &IList_1_t7768_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7768_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6105_il2cpp_TypeInfo;

// UnityEngine.UI.Image/OriginVertical
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginVertical.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/OriginVertical>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/OriginVertical>
extern MethodInfo IEnumerator_1_get_Current_m44128_MethodInfo;
static PropertyInfo IEnumerator_1_t6105____Current_PropertyInfo = 
{
	&IEnumerator_1_t6105_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44128_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6105_PropertyInfos[] =
{
	&IEnumerator_1_t6105____Current_PropertyInfo,
	NULL
};
extern Il2CppType OriginVertical_t306_0_0_0;
extern void* RuntimeInvoker_OriginVertical_t306 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44128_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/OriginVertical>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44128_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6105_il2cpp_TypeInfo/* declaring_type */
	, &OriginVertical_t306_0_0_0/* return_type */
	, RuntimeInvoker_OriginVertical_t306/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44128_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6105_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44128_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6105_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6105_0_0_0;
extern Il2CppType IEnumerator_1_t6105_1_0_0;
struct IEnumerator_1_t6105;
extern Il2CppGenericClass IEnumerator_1_t6105_GenericClass;
TypeInfo IEnumerator_1_t6105_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6105_MethodInfos/* methods */
	, IEnumerator_1_t6105_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6105_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6105_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6105_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6105_0_0_0/* byval_arg */
	, &IEnumerator_1_t6105_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6105_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_209.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3424_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_209MethodDeclarations.h"

extern TypeInfo OriginVertical_t306_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18811_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisOriginVertical_t306_m34155_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/OriginVertical>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/OriginVertical>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisOriginVertical_t306_m34155 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18807_MethodInfo;
 void InternalEnumerator_1__ctor_m18807 (InternalEnumerator_1_t3424 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18808_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18808 (InternalEnumerator_1_t3424 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m18811(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18811_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&OriginVertical_t306_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18809_MethodInfo;
 void InternalEnumerator_1_Dispose_m18809 (InternalEnumerator_1_t3424 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18810_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18810 (InternalEnumerator_1_t3424 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18811 (InternalEnumerator_1_t3424 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisOriginVertical_t306_m34155(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisOriginVertical_t306_m34155_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3424____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3424_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3424, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3424____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3424_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3424, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3424_FieldInfos[] =
{
	&InternalEnumerator_1_t3424____array_0_FieldInfo,
	&InternalEnumerator_1_t3424____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3424____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3424_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18808_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3424____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3424_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18811_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3424_PropertyInfos[] =
{
	&InternalEnumerator_1_t3424____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3424____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3424_InternalEnumerator_1__ctor_m18807_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18807_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18807_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18807/* method */
	, &InternalEnumerator_1_t3424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3424_InternalEnumerator_1__ctor_m18807_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18807_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18808_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18808_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18808/* method */
	, &InternalEnumerator_1_t3424_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18808_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18809_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18809_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18809/* method */
	, &InternalEnumerator_1_t3424_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18809_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18810_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18810_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18810/* method */
	, &InternalEnumerator_1_t3424_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18810_GenericMethod/* genericMethod */

};
extern Il2CppType OriginVertical_t306_0_0_0;
extern void* RuntimeInvoker_OriginVertical_t306 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18811_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/OriginVertical>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18811_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18811/* method */
	, &InternalEnumerator_1_t3424_il2cpp_TypeInfo/* declaring_type */
	, &OriginVertical_t306_0_0_0/* return_type */
	, RuntimeInvoker_OriginVertical_t306/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18811_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3424_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18807_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18808_MethodInfo,
	&InternalEnumerator_1_Dispose_m18809_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18810_MethodInfo,
	&InternalEnumerator_1_get_Current_m18811_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3424_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18808_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18810_MethodInfo,
	&InternalEnumerator_1_Dispose_m18809_MethodInfo,
	&InternalEnumerator_1_get_Current_m18811_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3424_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6105_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3424_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6105_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3424_0_0_0;
extern Il2CppType InternalEnumerator_1_t3424_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3424_GenericClass;
TypeInfo InternalEnumerator_1_t3424_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3424_MethodInfos/* methods */
	, InternalEnumerator_1_t3424_PropertyInfos/* properties */
	, InternalEnumerator_1_t3424_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3424_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3424_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3424_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3424_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3424_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3424_1_0_0/* this_arg */
	, InternalEnumerator_1_t3424_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3424_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3424)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7770_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>
extern MethodInfo ICollection_1_get_Count_m44129_MethodInfo;
static PropertyInfo ICollection_1_t7770____Count_PropertyInfo = 
{
	&ICollection_1_t7770_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44129_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44130_MethodInfo;
static PropertyInfo ICollection_1_t7770____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7770_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44130_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7770_PropertyInfos[] =
{
	&ICollection_1_t7770____Count_PropertyInfo,
	&ICollection_1_t7770____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44129_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::get_Count()
MethodInfo ICollection_1_get_Count_m44129_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7770_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44129_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44130_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44130_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7770_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44130_GenericMethod/* genericMethod */

};
extern Il2CppType OriginVertical_t306_0_0_0;
extern Il2CppType OriginVertical_t306_0_0_0;
static ParameterInfo ICollection_1_t7770_ICollection_1_Add_m44131_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &OriginVertical_t306_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44131_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::Add(T)
MethodInfo ICollection_1_Add_m44131_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7770_ICollection_1_Add_m44131_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44131_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44132_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::Clear()
MethodInfo ICollection_1_Clear_m44132_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44132_GenericMethod/* genericMethod */

};
extern Il2CppType OriginVertical_t306_0_0_0;
static ParameterInfo ICollection_1_t7770_ICollection_1_Contains_m44133_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &OriginVertical_t306_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44133_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::Contains(T)
MethodInfo ICollection_1_Contains_m44133_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7770_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7770_ICollection_1_Contains_m44133_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44133_GenericMethod/* genericMethod */

};
extern Il2CppType OriginVerticalU5BU5D_t5594_0_0_0;
extern Il2CppType OriginVerticalU5BU5D_t5594_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7770_ICollection_1_CopyTo_m44134_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &OriginVerticalU5BU5D_t5594_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44134_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44134_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7770_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7770_ICollection_1_CopyTo_m44134_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44134_GenericMethod/* genericMethod */

};
extern Il2CppType OriginVertical_t306_0_0_0;
static ParameterInfo ICollection_1_t7770_ICollection_1_Remove_m44135_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &OriginVertical_t306_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44135_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/OriginVertical>::Remove(T)
MethodInfo ICollection_1_Remove_m44135_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7770_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7770_ICollection_1_Remove_m44135_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44135_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7770_MethodInfos[] =
{
	&ICollection_1_get_Count_m44129_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44130_MethodInfo,
	&ICollection_1_Add_m44131_MethodInfo,
	&ICollection_1_Clear_m44132_MethodInfo,
	&ICollection_1_Contains_m44133_MethodInfo,
	&ICollection_1_CopyTo_m44134_MethodInfo,
	&ICollection_1_Remove_m44135_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7772_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7770_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7772_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7770_0_0_0;
extern Il2CppType ICollection_1_t7770_1_0_0;
struct ICollection_1_t7770;
extern Il2CppGenericClass ICollection_1_t7770_GenericClass;
TypeInfo ICollection_1_t7770_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7770_MethodInfos/* methods */
	, ICollection_1_t7770_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7770_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7770_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7770_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7770_0_0_0/* byval_arg */
	, &ICollection_1_t7770_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7770_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/OriginVertical>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/OriginVertical>
extern Il2CppType IEnumerator_1_t6105_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44136_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/OriginVertical>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44136_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7772_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6105_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44136_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7772_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44136_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7772_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7772_0_0_0;
extern Il2CppType IEnumerable_1_t7772_1_0_0;
struct IEnumerable_1_t7772;
extern Il2CppGenericClass IEnumerable_1_t7772_GenericClass;
TypeInfo IEnumerable_1_t7772_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7772_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7772_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7772_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7772_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7772_0_0_0/* byval_arg */
	, &IEnumerable_1_t7772_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7772_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7771_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginVertical>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginVertical>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginVertical>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginVertical>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginVertical>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginVertical>
extern MethodInfo IList_1_get_Item_m44137_MethodInfo;
extern MethodInfo IList_1_set_Item_m44138_MethodInfo;
static PropertyInfo IList_1_t7771____Item_PropertyInfo = 
{
	&IList_1_t7771_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44137_MethodInfo/* get */
	, &IList_1_set_Item_m44138_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7771_PropertyInfos[] =
{
	&IList_1_t7771____Item_PropertyInfo,
	NULL
};
extern Il2CppType OriginVertical_t306_0_0_0;
static ParameterInfo IList_1_t7771_IList_1_IndexOf_m44139_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &OriginVertical_t306_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44139_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginVertical>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44139_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7771_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7771_IList_1_IndexOf_m44139_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44139_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType OriginVertical_t306_0_0_0;
static ParameterInfo IList_1_t7771_IList_1_Insert_m44140_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &OriginVertical_t306_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44140_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginVertical>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44140_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7771_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7771_IList_1_Insert_m44140_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44140_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7771_IList_1_RemoveAt_m44141_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44141_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginVertical>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44141_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7771_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7771_IList_1_RemoveAt_m44141_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44141_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7771_IList_1_get_Item_m44137_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType OriginVertical_t306_0_0_0;
extern void* RuntimeInvoker_OriginVertical_t306_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44137_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginVertical>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44137_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7771_il2cpp_TypeInfo/* declaring_type */
	, &OriginVertical_t306_0_0_0/* return_type */
	, RuntimeInvoker_OriginVertical_t306_Int32_t93/* invoker_method */
	, IList_1_t7771_IList_1_get_Item_m44137_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44137_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType OriginVertical_t306_0_0_0;
static ParameterInfo IList_1_t7771_IList_1_set_Item_m44138_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &OriginVertical_t306_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44138_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/OriginVertical>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44138_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7771_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7771_IList_1_set_Item_m44138_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44138_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7771_MethodInfos[] =
{
	&IList_1_IndexOf_m44139_MethodInfo,
	&IList_1_Insert_m44140_MethodInfo,
	&IList_1_RemoveAt_m44141_MethodInfo,
	&IList_1_get_Item_m44137_MethodInfo,
	&IList_1_set_Item_m44138_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7771_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7770_il2cpp_TypeInfo,
	&IEnumerable_1_t7772_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7771_0_0_0;
extern Il2CppType IList_1_t7771_1_0_0;
struct IList_1_t7771;
extern Il2CppGenericClass IList_1_t7771_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7771_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7771_MethodInfos/* methods */
	, IList_1_t7771_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7771_il2cpp_TypeInfo/* element_class */
	, IList_1_t7771_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7771_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7771_0_0_0/* byval_arg */
	, &IList_1_t7771_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7771_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6107_il2cpp_TypeInfo;

// UnityEngine.UI.Image/Origin90
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin90.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Origin90>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Origin90>
extern MethodInfo IEnumerator_1_get_Current_m44142_MethodInfo;
static PropertyInfo IEnumerator_1_t6107____Current_PropertyInfo = 
{
	&IEnumerator_1_t6107_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44142_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6107_PropertyInfos[] =
{
	&IEnumerator_1_t6107____Current_PropertyInfo,
	NULL
};
extern Il2CppType Origin90_t307_0_0_0;
extern void* RuntimeInvoker_Origin90_t307 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44142_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Origin90>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44142_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6107_il2cpp_TypeInfo/* declaring_type */
	, &Origin90_t307_0_0_0/* return_type */
	, RuntimeInvoker_Origin90_t307/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44142_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6107_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44142_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6107_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6107_0_0_0;
extern Il2CppType IEnumerator_1_t6107_1_0_0;
struct IEnumerator_1_t6107;
extern Il2CppGenericClass IEnumerator_1_t6107_GenericClass;
TypeInfo IEnumerator_1_t6107_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6107_MethodInfos/* methods */
	, IEnumerator_1_t6107_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6107_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6107_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6107_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6107_0_0_0/* byval_arg */
	, &IEnumerator_1_t6107_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6107_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_210.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3425_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_210MethodDeclarations.h"

extern TypeInfo Origin90_t307_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18816_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisOrigin90_t307_m34166_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/Origin90>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/Origin90>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisOrigin90_t307_m34166 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18812_MethodInfo;
 void InternalEnumerator_1__ctor_m18812 (InternalEnumerator_1_t3425 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18813_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18813 (InternalEnumerator_1_t3425 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m18816(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18816_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Origin90_t307_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18814_MethodInfo;
 void InternalEnumerator_1_Dispose_m18814 (InternalEnumerator_1_t3425 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18815_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18815 (InternalEnumerator_1_t3425 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18816 (InternalEnumerator_1_t3425 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisOrigin90_t307_m34166(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisOrigin90_t307_m34166_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3425____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3425_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3425, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3425____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3425_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3425, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3425_FieldInfos[] =
{
	&InternalEnumerator_1_t3425____array_0_FieldInfo,
	&InternalEnumerator_1_t3425____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3425____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3425_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18813_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3425____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3425_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18816_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3425_PropertyInfos[] =
{
	&InternalEnumerator_1_t3425____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3425____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3425_InternalEnumerator_1__ctor_m18812_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18812_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18812_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18812/* method */
	, &InternalEnumerator_1_t3425_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3425_InternalEnumerator_1__ctor_m18812_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18812_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18813_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18813_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18813/* method */
	, &InternalEnumerator_1_t3425_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18813_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18814_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18814_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18814/* method */
	, &InternalEnumerator_1_t3425_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18814_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18815_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18815_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18815/* method */
	, &InternalEnumerator_1_t3425_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18815_GenericMethod/* genericMethod */

};
extern Il2CppType Origin90_t307_0_0_0;
extern void* RuntimeInvoker_Origin90_t307 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18816_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin90>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18816_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18816/* method */
	, &InternalEnumerator_1_t3425_il2cpp_TypeInfo/* declaring_type */
	, &Origin90_t307_0_0_0/* return_type */
	, RuntimeInvoker_Origin90_t307/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18816_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3425_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18812_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18813_MethodInfo,
	&InternalEnumerator_1_Dispose_m18814_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18815_MethodInfo,
	&InternalEnumerator_1_get_Current_m18816_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3425_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18813_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18815_MethodInfo,
	&InternalEnumerator_1_Dispose_m18814_MethodInfo,
	&InternalEnumerator_1_get_Current_m18816_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3425_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6107_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3425_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6107_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3425_0_0_0;
extern Il2CppType InternalEnumerator_1_t3425_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3425_GenericClass;
TypeInfo InternalEnumerator_1_t3425_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3425_MethodInfos/* methods */
	, InternalEnumerator_1_t3425_PropertyInfos/* properties */
	, InternalEnumerator_1_t3425_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3425_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3425_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3425_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3425_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3425_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3425_1_0_0/* this_arg */
	, InternalEnumerator_1_t3425_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3425_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3425)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7773_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>
extern MethodInfo ICollection_1_get_Count_m44143_MethodInfo;
static PropertyInfo ICollection_1_t7773____Count_PropertyInfo = 
{
	&ICollection_1_t7773_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44143_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44144_MethodInfo;
static PropertyInfo ICollection_1_t7773____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7773_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44144_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7773_PropertyInfos[] =
{
	&ICollection_1_t7773____Count_PropertyInfo,
	&ICollection_1_t7773____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44143_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::get_Count()
MethodInfo ICollection_1_get_Count_m44143_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7773_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44143_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44144_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44144_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7773_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44144_GenericMethod/* genericMethod */

};
extern Il2CppType Origin90_t307_0_0_0;
extern Il2CppType Origin90_t307_0_0_0;
static ParameterInfo ICollection_1_t7773_ICollection_1_Add_m44145_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Origin90_t307_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44145_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::Add(T)
MethodInfo ICollection_1_Add_m44145_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7773_ICollection_1_Add_m44145_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44145_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44146_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::Clear()
MethodInfo ICollection_1_Clear_m44146_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44146_GenericMethod/* genericMethod */

};
extern Il2CppType Origin90_t307_0_0_0;
static ParameterInfo ICollection_1_t7773_ICollection_1_Contains_m44147_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Origin90_t307_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44147_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::Contains(T)
MethodInfo ICollection_1_Contains_m44147_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7773_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7773_ICollection_1_Contains_m44147_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44147_GenericMethod/* genericMethod */

};
extern Il2CppType Origin90U5BU5D_t5595_0_0_0;
extern Il2CppType Origin90U5BU5D_t5595_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7773_ICollection_1_CopyTo_m44148_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Origin90U5BU5D_t5595_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44148_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44148_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7773_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7773_ICollection_1_CopyTo_m44148_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44148_GenericMethod/* genericMethod */

};
extern Il2CppType Origin90_t307_0_0_0;
static ParameterInfo ICollection_1_t7773_ICollection_1_Remove_m44149_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Origin90_t307_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44149_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin90>::Remove(T)
MethodInfo ICollection_1_Remove_m44149_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7773_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7773_ICollection_1_Remove_m44149_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44149_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7773_MethodInfos[] =
{
	&ICollection_1_get_Count_m44143_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44144_MethodInfo,
	&ICollection_1_Add_m44145_MethodInfo,
	&ICollection_1_Clear_m44146_MethodInfo,
	&ICollection_1_Contains_m44147_MethodInfo,
	&ICollection_1_CopyTo_m44148_MethodInfo,
	&ICollection_1_Remove_m44149_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7775_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7773_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7775_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7773_0_0_0;
extern Il2CppType ICollection_1_t7773_1_0_0;
struct ICollection_1_t7773;
extern Il2CppGenericClass ICollection_1_t7773_GenericClass;
TypeInfo ICollection_1_t7773_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7773_MethodInfos/* methods */
	, ICollection_1_t7773_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7773_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7773_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7773_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7773_0_0_0/* byval_arg */
	, &ICollection_1_t7773_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7773_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Origin90>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Origin90>
extern Il2CppType IEnumerator_1_t6107_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44150_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Origin90>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44150_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7775_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6107_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44150_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7775_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44150_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7775_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7775_0_0_0;
extern Il2CppType IEnumerable_1_t7775_1_0_0;
struct IEnumerable_1_t7775;
extern Il2CppGenericClass IEnumerable_1_t7775_GenericClass;
TypeInfo IEnumerable_1_t7775_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7775_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7775_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7775_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7775_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7775_0_0_0/* byval_arg */
	, &IEnumerable_1_t7775_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7775_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7774_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin90>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin90>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin90>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin90>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin90>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin90>
extern MethodInfo IList_1_get_Item_m44151_MethodInfo;
extern MethodInfo IList_1_set_Item_m44152_MethodInfo;
static PropertyInfo IList_1_t7774____Item_PropertyInfo = 
{
	&IList_1_t7774_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44151_MethodInfo/* get */
	, &IList_1_set_Item_m44152_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7774_PropertyInfos[] =
{
	&IList_1_t7774____Item_PropertyInfo,
	NULL
};
extern Il2CppType Origin90_t307_0_0_0;
static ParameterInfo IList_1_t7774_IList_1_IndexOf_m44153_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Origin90_t307_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44153_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin90>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44153_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7774_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7774_IList_1_IndexOf_m44153_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44153_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Origin90_t307_0_0_0;
static ParameterInfo IList_1_t7774_IList_1_Insert_m44154_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Origin90_t307_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44154_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin90>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44154_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7774_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7774_IList_1_Insert_m44154_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44154_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7774_IList_1_RemoveAt_m44155_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44155_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin90>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44155_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7774_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7774_IList_1_RemoveAt_m44155_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44155_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7774_IList_1_get_Item_m44151_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Origin90_t307_0_0_0;
extern void* RuntimeInvoker_Origin90_t307_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44151_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin90>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44151_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7774_il2cpp_TypeInfo/* declaring_type */
	, &Origin90_t307_0_0_0/* return_type */
	, RuntimeInvoker_Origin90_t307_Int32_t93/* invoker_method */
	, IList_1_t7774_IList_1_get_Item_m44151_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44151_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Origin90_t307_0_0_0;
static ParameterInfo IList_1_t7774_IList_1_set_Item_m44152_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Origin90_t307_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44152_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin90>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44152_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7774_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7774_IList_1_set_Item_m44152_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44152_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7774_MethodInfos[] =
{
	&IList_1_IndexOf_m44153_MethodInfo,
	&IList_1_Insert_m44154_MethodInfo,
	&IList_1_RemoveAt_m44155_MethodInfo,
	&IList_1_get_Item_m44151_MethodInfo,
	&IList_1_set_Item_m44152_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7774_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7773_il2cpp_TypeInfo,
	&IEnumerable_1_t7775_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7774_0_0_0;
extern Il2CppType IList_1_t7774_1_0_0;
struct IList_1_t7774;
extern Il2CppGenericClass IList_1_t7774_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7774_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7774_MethodInfos/* methods */
	, IList_1_t7774_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7774_il2cpp_TypeInfo/* element_class */
	, IList_1_t7774_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7774_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7774_0_0_0/* byval_arg */
	, &IList_1_t7774_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7774_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6109_il2cpp_TypeInfo;

// UnityEngine.UI.Image/Origin180
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin180.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Origin180>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Origin180>
extern MethodInfo IEnumerator_1_get_Current_m44156_MethodInfo;
static PropertyInfo IEnumerator_1_t6109____Current_PropertyInfo = 
{
	&IEnumerator_1_t6109_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44156_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6109_PropertyInfos[] =
{
	&IEnumerator_1_t6109____Current_PropertyInfo,
	NULL
};
extern Il2CppType Origin180_t308_0_0_0;
extern void* RuntimeInvoker_Origin180_t308 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44156_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Origin180>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44156_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6109_il2cpp_TypeInfo/* declaring_type */
	, &Origin180_t308_0_0_0/* return_type */
	, RuntimeInvoker_Origin180_t308/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44156_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6109_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44156_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6109_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6109_0_0_0;
extern Il2CppType IEnumerator_1_t6109_1_0_0;
struct IEnumerator_1_t6109;
extern Il2CppGenericClass IEnumerator_1_t6109_GenericClass;
TypeInfo IEnumerator_1_t6109_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6109_MethodInfos/* methods */
	, IEnumerator_1_t6109_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6109_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6109_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6109_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6109_0_0_0/* byval_arg */
	, &IEnumerator_1_t6109_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6109_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_211.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3426_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_211MethodDeclarations.h"

extern TypeInfo Origin180_t308_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18821_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisOrigin180_t308_m34177_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/Origin180>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/Origin180>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisOrigin180_t308_m34177 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18817_MethodInfo;
 void InternalEnumerator_1__ctor_m18817 (InternalEnumerator_1_t3426 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18818_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18818 (InternalEnumerator_1_t3426 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m18821(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18821_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Origin180_t308_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18819_MethodInfo;
 void InternalEnumerator_1_Dispose_m18819 (InternalEnumerator_1_t3426 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18820_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18820 (InternalEnumerator_1_t3426 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18821 (InternalEnumerator_1_t3426 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisOrigin180_t308_m34177(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisOrigin180_t308_m34177_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3426____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3426_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3426, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3426____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3426_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3426, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3426_FieldInfos[] =
{
	&InternalEnumerator_1_t3426____array_0_FieldInfo,
	&InternalEnumerator_1_t3426____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3426____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3426_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18818_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3426____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3426_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18821_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3426_PropertyInfos[] =
{
	&InternalEnumerator_1_t3426____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3426____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3426_InternalEnumerator_1__ctor_m18817_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18817_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18817_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18817/* method */
	, &InternalEnumerator_1_t3426_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3426_InternalEnumerator_1__ctor_m18817_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18817_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18818_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18818_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18818/* method */
	, &InternalEnumerator_1_t3426_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18818_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18819_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18819_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18819/* method */
	, &InternalEnumerator_1_t3426_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18819_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18820_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18820_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18820/* method */
	, &InternalEnumerator_1_t3426_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18820_GenericMethod/* genericMethod */

};
extern Il2CppType Origin180_t308_0_0_0;
extern void* RuntimeInvoker_Origin180_t308 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18821_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin180>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18821_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18821/* method */
	, &InternalEnumerator_1_t3426_il2cpp_TypeInfo/* declaring_type */
	, &Origin180_t308_0_0_0/* return_type */
	, RuntimeInvoker_Origin180_t308/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18821_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3426_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18817_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18818_MethodInfo,
	&InternalEnumerator_1_Dispose_m18819_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18820_MethodInfo,
	&InternalEnumerator_1_get_Current_m18821_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3426_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18818_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18820_MethodInfo,
	&InternalEnumerator_1_Dispose_m18819_MethodInfo,
	&InternalEnumerator_1_get_Current_m18821_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3426_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6109_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3426_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6109_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3426_0_0_0;
extern Il2CppType InternalEnumerator_1_t3426_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3426_GenericClass;
TypeInfo InternalEnumerator_1_t3426_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3426_MethodInfos/* methods */
	, InternalEnumerator_1_t3426_PropertyInfos/* properties */
	, InternalEnumerator_1_t3426_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3426_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3426_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3426_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3426_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3426_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3426_1_0_0/* this_arg */
	, InternalEnumerator_1_t3426_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3426_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3426)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7776_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>
extern MethodInfo ICollection_1_get_Count_m44157_MethodInfo;
static PropertyInfo ICollection_1_t7776____Count_PropertyInfo = 
{
	&ICollection_1_t7776_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44157_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44158_MethodInfo;
static PropertyInfo ICollection_1_t7776____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7776_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44158_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7776_PropertyInfos[] =
{
	&ICollection_1_t7776____Count_PropertyInfo,
	&ICollection_1_t7776____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44157_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::get_Count()
MethodInfo ICollection_1_get_Count_m44157_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7776_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44157_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44158_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44158_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7776_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44158_GenericMethod/* genericMethod */

};
extern Il2CppType Origin180_t308_0_0_0;
extern Il2CppType Origin180_t308_0_0_0;
static ParameterInfo ICollection_1_t7776_ICollection_1_Add_m44159_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Origin180_t308_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44159_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::Add(T)
MethodInfo ICollection_1_Add_m44159_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7776_ICollection_1_Add_m44159_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44159_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44160_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::Clear()
MethodInfo ICollection_1_Clear_m44160_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44160_GenericMethod/* genericMethod */

};
extern Il2CppType Origin180_t308_0_0_0;
static ParameterInfo ICollection_1_t7776_ICollection_1_Contains_m44161_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Origin180_t308_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44161_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::Contains(T)
MethodInfo ICollection_1_Contains_m44161_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7776_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7776_ICollection_1_Contains_m44161_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44161_GenericMethod/* genericMethod */

};
extern Il2CppType Origin180U5BU5D_t5596_0_0_0;
extern Il2CppType Origin180U5BU5D_t5596_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7776_ICollection_1_CopyTo_m44162_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Origin180U5BU5D_t5596_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44162_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44162_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7776_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7776_ICollection_1_CopyTo_m44162_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44162_GenericMethod/* genericMethod */

};
extern Il2CppType Origin180_t308_0_0_0;
static ParameterInfo ICollection_1_t7776_ICollection_1_Remove_m44163_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Origin180_t308_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44163_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin180>::Remove(T)
MethodInfo ICollection_1_Remove_m44163_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7776_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7776_ICollection_1_Remove_m44163_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44163_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7776_MethodInfos[] =
{
	&ICollection_1_get_Count_m44157_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44158_MethodInfo,
	&ICollection_1_Add_m44159_MethodInfo,
	&ICollection_1_Clear_m44160_MethodInfo,
	&ICollection_1_Contains_m44161_MethodInfo,
	&ICollection_1_CopyTo_m44162_MethodInfo,
	&ICollection_1_Remove_m44163_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7778_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7776_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7778_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7776_0_0_0;
extern Il2CppType ICollection_1_t7776_1_0_0;
struct ICollection_1_t7776;
extern Il2CppGenericClass ICollection_1_t7776_GenericClass;
TypeInfo ICollection_1_t7776_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7776_MethodInfos/* methods */
	, ICollection_1_t7776_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7776_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7776_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7776_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7776_0_0_0/* byval_arg */
	, &ICollection_1_t7776_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7776_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Origin180>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Origin180>
extern Il2CppType IEnumerator_1_t6109_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44164_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Origin180>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44164_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7778_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6109_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44164_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7778_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44164_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7778_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7778_0_0_0;
extern Il2CppType IEnumerable_1_t7778_1_0_0;
struct IEnumerable_1_t7778;
extern Il2CppGenericClass IEnumerable_1_t7778_GenericClass;
TypeInfo IEnumerable_1_t7778_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7778_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7778_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7778_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7778_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7778_0_0_0/* byval_arg */
	, &IEnumerable_1_t7778_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7778_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7777_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin180>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin180>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin180>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin180>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin180>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin180>
extern MethodInfo IList_1_get_Item_m44165_MethodInfo;
extern MethodInfo IList_1_set_Item_m44166_MethodInfo;
static PropertyInfo IList_1_t7777____Item_PropertyInfo = 
{
	&IList_1_t7777_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44165_MethodInfo/* get */
	, &IList_1_set_Item_m44166_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7777_PropertyInfos[] =
{
	&IList_1_t7777____Item_PropertyInfo,
	NULL
};
extern Il2CppType Origin180_t308_0_0_0;
static ParameterInfo IList_1_t7777_IList_1_IndexOf_m44167_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Origin180_t308_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44167_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin180>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44167_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7777_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7777_IList_1_IndexOf_m44167_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44167_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Origin180_t308_0_0_0;
static ParameterInfo IList_1_t7777_IList_1_Insert_m44168_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Origin180_t308_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44168_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin180>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44168_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7777_IList_1_Insert_m44168_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44168_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7777_IList_1_RemoveAt_m44169_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44169_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin180>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44169_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7777_IList_1_RemoveAt_m44169_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44169_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7777_IList_1_get_Item_m44165_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Origin180_t308_0_0_0;
extern void* RuntimeInvoker_Origin180_t308_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44165_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin180>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44165_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7777_il2cpp_TypeInfo/* declaring_type */
	, &Origin180_t308_0_0_0/* return_type */
	, RuntimeInvoker_Origin180_t308_Int32_t93/* invoker_method */
	, IList_1_t7777_IList_1_get_Item_m44165_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44165_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Origin180_t308_0_0_0;
static ParameterInfo IList_1_t7777_IList_1_set_Item_m44166_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Origin180_t308_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44166_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin180>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44166_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7777_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7777_IList_1_set_Item_m44166_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44166_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7777_MethodInfos[] =
{
	&IList_1_IndexOf_m44167_MethodInfo,
	&IList_1_Insert_m44168_MethodInfo,
	&IList_1_RemoveAt_m44169_MethodInfo,
	&IList_1_get_Item_m44165_MethodInfo,
	&IList_1_set_Item_m44166_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7777_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7776_il2cpp_TypeInfo,
	&IEnumerable_1_t7778_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7777_0_0_0;
extern Il2CppType IList_1_t7777_1_0_0;
struct IList_1_t7777;
extern Il2CppGenericClass IList_1_t7777_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7777_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7777_MethodInfos/* methods */
	, IList_1_t7777_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7777_il2cpp_TypeInfo/* element_class */
	, IList_1_t7777_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7777_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7777_0_0_0/* byval_arg */
	, &IList_1_t7777_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7777_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6111_il2cpp_TypeInfo;

// UnityEngine.UI.Image/Origin360
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin360.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Origin360>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Origin360>
extern MethodInfo IEnumerator_1_get_Current_m44170_MethodInfo;
static PropertyInfo IEnumerator_1_t6111____Current_PropertyInfo = 
{
	&IEnumerator_1_t6111_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44170_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6111_PropertyInfos[] =
{
	&IEnumerator_1_t6111____Current_PropertyInfo,
	NULL
};
extern Il2CppType Origin360_t309_0_0_0;
extern void* RuntimeInvoker_Origin360_t309 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44170_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Image/Origin360>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44170_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6111_il2cpp_TypeInfo/* declaring_type */
	, &Origin360_t309_0_0_0/* return_type */
	, RuntimeInvoker_Origin360_t309/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44170_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6111_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44170_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6111_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6111_0_0_0;
extern Il2CppType IEnumerator_1_t6111_1_0_0;
struct IEnumerator_1_t6111;
extern Il2CppGenericClass IEnumerator_1_t6111_GenericClass;
TypeInfo IEnumerator_1_t6111_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6111_MethodInfos/* methods */
	, IEnumerator_1_t6111_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6111_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6111_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6111_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6111_0_0_0/* byval_arg */
	, &IEnumerator_1_t6111_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6111_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_212.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3427_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_212MethodDeclarations.h"

extern TypeInfo Origin360_t309_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18826_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisOrigin360_t309_m34188_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/Origin360>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Image/Origin360>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisOrigin360_t309_m34188 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18822_MethodInfo;
 void InternalEnumerator_1__ctor_m18822 (InternalEnumerator_1_t3427 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18823_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18823 (InternalEnumerator_1_t3427 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m18826(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18826_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Origin360_t309_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18824_MethodInfo;
 void InternalEnumerator_1_Dispose_m18824 (InternalEnumerator_1_t3427 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18825_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18825 (InternalEnumerator_1_t3427 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18826 (InternalEnumerator_1_t3427 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisOrigin360_t309_m34188(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisOrigin360_t309_m34188_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3427____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3427_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3427, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3427____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3427_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3427, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3427_FieldInfos[] =
{
	&InternalEnumerator_1_t3427____array_0_FieldInfo,
	&InternalEnumerator_1_t3427____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3427____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3427_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18823_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3427____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3427_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18826_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3427_PropertyInfos[] =
{
	&InternalEnumerator_1_t3427____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3427____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3427_InternalEnumerator_1__ctor_m18822_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18822_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18822_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18822/* method */
	, &InternalEnumerator_1_t3427_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3427_InternalEnumerator_1__ctor_m18822_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18822_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18823_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18823_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18823/* method */
	, &InternalEnumerator_1_t3427_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18823_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18824_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18824_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18824/* method */
	, &InternalEnumerator_1_t3427_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18824_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18825_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18825_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18825/* method */
	, &InternalEnumerator_1_t3427_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18825_GenericMethod/* genericMethod */

};
extern Il2CppType Origin360_t309_0_0_0;
extern void* RuntimeInvoker_Origin360_t309 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18826_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18826_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18826/* method */
	, &InternalEnumerator_1_t3427_il2cpp_TypeInfo/* declaring_type */
	, &Origin360_t309_0_0_0/* return_type */
	, RuntimeInvoker_Origin360_t309/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18826_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3427_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18822_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18823_MethodInfo,
	&InternalEnumerator_1_Dispose_m18824_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18825_MethodInfo,
	&InternalEnumerator_1_get_Current_m18826_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3427_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18823_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18825_MethodInfo,
	&InternalEnumerator_1_Dispose_m18824_MethodInfo,
	&InternalEnumerator_1_get_Current_m18826_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3427_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6111_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3427_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6111_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3427_0_0_0;
extern Il2CppType InternalEnumerator_1_t3427_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3427_GenericClass;
TypeInfo InternalEnumerator_1_t3427_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3427_MethodInfos/* methods */
	, InternalEnumerator_1_t3427_PropertyInfos/* properties */
	, InternalEnumerator_1_t3427_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3427_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3427_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3427_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3427_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3427_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3427_1_0_0/* this_arg */
	, InternalEnumerator_1_t3427_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3427_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3427)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7779_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>
extern MethodInfo ICollection_1_get_Count_m44171_MethodInfo;
static PropertyInfo ICollection_1_t7779____Count_PropertyInfo = 
{
	&ICollection_1_t7779_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44171_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44172_MethodInfo;
static PropertyInfo ICollection_1_t7779____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7779_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44172_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7779_PropertyInfos[] =
{
	&ICollection_1_t7779____Count_PropertyInfo,
	&ICollection_1_t7779____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44171_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::get_Count()
MethodInfo ICollection_1_get_Count_m44171_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7779_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44171_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44172_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44172_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7779_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44172_GenericMethod/* genericMethod */

};
extern Il2CppType Origin360_t309_0_0_0;
extern Il2CppType Origin360_t309_0_0_0;
static ParameterInfo ICollection_1_t7779_ICollection_1_Add_m44173_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Origin360_t309_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44173_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::Add(T)
MethodInfo ICollection_1_Add_m44173_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7779_ICollection_1_Add_m44173_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44173_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44174_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::Clear()
MethodInfo ICollection_1_Clear_m44174_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44174_GenericMethod/* genericMethod */

};
extern Il2CppType Origin360_t309_0_0_0;
static ParameterInfo ICollection_1_t7779_ICollection_1_Contains_m44175_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Origin360_t309_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44175_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::Contains(T)
MethodInfo ICollection_1_Contains_m44175_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7779_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7779_ICollection_1_Contains_m44175_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44175_GenericMethod/* genericMethod */

};
extern Il2CppType Origin360U5BU5D_t5597_0_0_0;
extern Il2CppType Origin360U5BU5D_t5597_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7779_ICollection_1_CopyTo_m44176_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Origin360U5BU5D_t5597_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44176_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44176_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7779_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7779_ICollection_1_CopyTo_m44176_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44176_GenericMethod/* genericMethod */

};
extern Il2CppType Origin360_t309_0_0_0;
static ParameterInfo ICollection_1_t7779_ICollection_1_Remove_m44177_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Origin360_t309_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44177_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Image/Origin360>::Remove(T)
MethodInfo ICollection_1_Remove_m44177_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7779_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7779_ICollection_1_Remove_m44177_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44177_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7779_MethodInfos[] =
{
	&ICollection_1_get_Count_m44171_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44172_MethodInfo,
	&ICollection_1_Add_m44173_MethodInfo,
	&ICollection_1_Clear_m44174_MethodInfo,
	&ICollection_1_Contains_m44175_MethodInfo,
	&ICollection_1_CopyTo_m44176_MethodInfo,
	&ICollection_1_Remove_m44177_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7781_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7779_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7781_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7779_0_0_0;
extern Il2CppType ICollection_1_t7779_1_0_0;
struct ICollection_1_t7779;
extern Il2CppGenericClass ICollection_1_t7779_GenericClass;
TypeInfo ICollection_1_t7779_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7779_MethodInfos/* methods */
	, ICollection_1_t7779_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7779_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7779_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7779_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7779_0_0_0/* byval_arg */
	, &ICollection_1_t7779_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7779_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Origin360>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Origin360>
extern Il2CppType IEnumerator_1_t6111_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44178_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Image/Origin360>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44178_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7781_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6111_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44178_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7781_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44178_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7781_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7781_0_0_0;
extern Il2CppType IEnumerable_1_t7781_1_0_0;
struct IEnumerable_1_t7781;
extern Il2CppGenericClass IEnumerable_1_t7781_GenericClass;
TypeInfo IEnumerable_1_t7781_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7781_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7781_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7781_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7781_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7781_0_0_0/* byval_arg */
	, &IEnumerable_1_t7781_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7781_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7780_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin360>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin360>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin360>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin360>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin360>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin360>
extern MethodInfo IList_1_get_Item_m44179_MethodInfo;
extern MethodInfo IList_1_set_Item_m44180_MethodInfo;
static PropertyInfo IList_1_t7780____Item_PropertyInfo = 
{
	&IList_1_t7780_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44179_MethodInfo/* get */
	, &IList_1_set_Item_m44180_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7780_PropertyInfos[] =
{
	&IList_1_t7780____Item_PropertyInfo,
	NULL
};
extern Il2CppType Origin360_t309_0_0_0;
static ParameterInfo IList_1_t7780_IList_1_IndexOf_m44181_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Origin360_t309_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44181_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin360>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44181_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7780_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7780_IList_1_IndexOf_m44181_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44181_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Origin360_t309_0_0_0;
static ParameterInfo IList_1_t7780_IList_1_Insert_m44182_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Origin360_t309_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44182_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin360>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44182_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7780_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7780_IList_1_Insert_m44182_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44182_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7780_IList_1_RemoveAt_m44183_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44183_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin360>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44183_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7780_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7780_IList_1_RemoveAt_m44183_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44183_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7780_IList_1_get_Item_m44179_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Origin360_t309_0_0_0;
extern void* RuntimeInvoker_Origin360_t309_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44179_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin360>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44179_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7780_il2cpp_TypeInfo/* declaring_type */
	, &Origin360_t309_0_0_0/* return_type */
	, RuntimeInvoker_Origin360_t309_Int32_t93/* invoker_method */
	, IList_1_t7780_IList_1_get_Item_m44179_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44179_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Origin360_t309_0_0_0;
static ParameterInfo IList_1_t7780_IList_1_set_Item_m44180_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Origin360_t309_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44180_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Image/Origin360>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44180_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7780_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7780_IList_1_set_Item_m44180_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44180_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7780_MethodInfos[] =
{
	&IList_1_IndexOf_m44181_MethodInfo,
	&IList_1_Insert_m44182_MethodInfo,
	&IList_1_RemoveAt_m44183_MethodInfo,
	&IList_1_get_Item_m44179_MethodInfo,
	&IList_1_set_Item_m44180_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7780_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7779_il2cpp_TypeInfo,
	&IEnumerable_1_t7781_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7780_0_0_0;
extern Il2CppType IList_1_t7780_1_0_0;
struct IList_1_t7780;
extern Il2CppGenericClass IList_1_t7780_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7780_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7780_MethodInfos/* methods */
	, IList_1_t7780_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7780_il2cpp_TypeInfo/* element_class */
	, IList_1_t7780_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7780_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7780_0_0_0/* byval_arg */
	, &IList_1_t7780_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7780_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6113_il2cpp_TypeInfo;

// UnityEngine.UI.InputField
#include "UnityEngine_UI_UnityEngine_UI_InputField.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField>
extern MethodInfo IEnumerator_1_get_Current_m44184_MethodInfo;
static PropertyInfo IEnumerator_1_t6113____Current_PropertyInfo = 
{
	&IEnumerator_1_t6113_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44184_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6113_PropertyInfos[] =
{
	&IEnumerator_1_t6113____Current_PropertyInfo,
	NULL
};
extern Il2CppType InputField_t326_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44184_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44184_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6113_il2cpp_TypeInfo/* declaring_type */
	, &InputField_t326_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44184_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6113_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44184_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6113_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6113_0_0_0;
extern Il2CppType IEnumerator_1_t6113_1_0_0;
struct IEnumerator_1_t6113;
extern Il2CppGenericClass IEnumerator_1_t6113_GenericClass;
TypeInfo IEnumerator_1_t6113_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6113_MethodInfos/* methods */
	, IEnumerator_1_t6113_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6113_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6113_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6113_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6113_0_0_0/* byval_arg */
	, &IEnumerator_1_t6113_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6113_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_213.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3428_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_213MethodDeclarations.h"

extern TypeInfo InputField_t326_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18831_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisInputField_t326_m34199_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m31436_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m31436(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField>(System.Int32)
#define Array_InternalArray__get_Item_TisInputField_t326_m34199(__this, p0, method) (InputField_t326 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3428____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3428_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3428, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3428____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3428_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3428, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3428_FieldInfos[] =
{
	&InternalEnumerator_1_t3428____array_0_FieldInfo,
	&InternalEnumerator_1_t3428____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18828_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3428____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3428_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18828_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3428____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3428_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18831_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3428_PropertyInfos[] =
{
	&InternalEnumerator_1_t3428____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3428____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3428_InternalEnumerator_1__ctor_m18827_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18827_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18827_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3428_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3428_InternalEnumerator_1__ctor_m18827_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18827_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18828_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18828_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3428_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18828_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18829_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18829_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3428_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18829_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18830_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18830_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3428_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18830_GenericMethod/* genericMethod */

};
extern Il2CppType InputField_t326_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18831_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18831_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3428_il2cpp_TypeInfo/* declaring_type */
	, &InputField_t326_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18831_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3428_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18827_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18828_MethodInfo,
	&InternalEnumerator_1_Dispose_m18829_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18830_MethodInfo,
	&InternalEnumerator_1_get_Current_m18831_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m18830_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m18829_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3428_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18828_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18830_MethodInfo,
	&InternalEnumerator_1_Dispose_m18829_MethodInfo,
	&InternalEnumerator_1_get_Current_m18831_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3428_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6113_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3428_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6113_il2cpp_TypeInfo, 7},
};
extern TypeInfo InputField_t326_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3428_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m18831_MethodInfo/* Method Usage */,
	&InputField_t326_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisInputField_t326_m34199_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3428_0_0_0;
extern Il2CppType InternalEnumerator_1_t3428_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3428_GenericClass;
TypeInfo InternalEnumerator_1_t3428_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3428_MethodInfos/* methods */
	, InternalEnumerator_1_t3428_PropertyInfos/* properties */
	, InternalEnumerator_1_t3428_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3428_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3428_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3428_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3428_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3428_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3428_1_0_0/* this_arg */
	, InternalEnumerator_1_t3428_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3428_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3428_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3428)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7782_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>
extern MethodInfo ICollection_1_get_Count_m44185_MethodInfo;
static PropertyInfo ICollection_1_t7782____Count_PropertyInfo = 
{
	&ICollection_1_t7782_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44185_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44186_MethodInfo;
static PropertyInfo ICollection_1_t7782____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7782_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44186_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7782_PropertyInfos[] =
{
	&ICollection_1_t7782____Count_PropertyInfo,
	&ICollection_1_t7782____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44185_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::get_Count()
MethodInfo ICollection_1_get_Count_m44185_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7782_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44185_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44186_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44186_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7782_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44186_GenericMethod/* genericMethod */

};
extern Il2CppType InputField_t326_0_0_0;
extern Il2CppType InputField_t326_0_0_0;
static ParameterInfo ICollection_1_t7782_ICollection_1_Add_m44187_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &InputField_t326_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44187_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::Add(T)
MethodInfo ICollection_1_Add_m44187_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7782_ICollection_1_Add_m44187_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44187_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44188_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::Clear()
MethodInfo ICollection_1_Clear_m44188_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44188_GenericMethod/* genericMethod */

};
extern Il2CppType InputField_t326_0_0_0;
static ParameterInfo ICollection_1_t7782_ICollection_1_Contains_m44189_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &InputField_t326_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44189_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::Contains(T)
MethodInfo ICollection_1_Contains_m44189_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7782_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7782_ICollection_1_Contains_m44189_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44189_GenericMethod/* genericMethod */

};
extern Il2CppType InputFieldU5BU5D_t5598_0_0_0;
extern Il2CppType InputFieldU5BU5D_t5598_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7782_ICollection_1_CopyTo_m44190_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &InputFieldU5BU5D_t5598_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44190_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44190_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7782_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7782_ICollection_1_CopyTo_m44190_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44190_GenericMethod/* genericMethod */

};
extern Il2CppType InputField_t326_0_0_0;
static ParameterInfo ICollection_1_t7782_ICollection_1_Remove_m44191_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &InputField_t326_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44191_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField>::Remove(T)
MethodInfo ICollection_1_Remove_m44191_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7782_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7782_ICollection_1_Remove_m44191_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44191_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7782_MethodInfos[] =
{
	&ICollection_1_get_Count_m44185_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44186_MethodInfo,
	&ICollection_1_Add_m44187_MethodInfo,
	&ICollection_1_Clear_m44188_MethodInfo,
	&ICollection_1_Contains_m44189_MethodInfo,
	&ICollection_1_CopyTo_m44190_MethodInfo,
	&ICollection_1_Remove_m44191_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7784_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7782_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7784_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7782_0_0_0;
extern Il2CppType ICollection_1_t7782_1_0_0;
struct ICollection_1_t7782;
extern Il2CppGenericClass ICollection_1_t7782_GenericClass;
TypeInfo ICollection_1_t7782_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7782_MethodInfos/* methods */
	, ICollection_1_t7782_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7782_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7782_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7782_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7782_0_0_0/* byval_arg */
	, &ICollection_1_t7782_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7782_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField>
extern Il2CppType IEnumerator_1_t6113_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44192_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44192_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7784_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6113_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44192_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7784_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44192_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7784_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7784_0_0_0;
extern Il2CppType IEnumerable_1_t7784_1_0_0;
struct IEnumerable_1_t7784;
extern Il2CppGenericClass IEnumerable_1_t7784_GenericClass;
TypeInfo IEnumerable_1_t7784_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7784_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7784_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7784_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7784_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7784_0_0_0/* byval_arg */
	, &IEnumerable_1_t7784_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7784_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7783_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.InputField>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.InputField>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.InputField>
extern MethodInfo IList_1_get_Item_m44193_MethodInfo;
extern MethodInfo IList_1_set_Item_m44194_MethodInfo;
static PropertyInfo IList_1_t7783____Item_PropertyInfo = 
{
	&IList_1_t7783_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44193_MethodInfo/* get */
	, &IList_1_set_Item_m44194_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7783_PropertyInfos[] =
{
	&IList_1_t7783____Item_PropertyInfo,
	NULL
};
extern Il2CppType InputField_t326_0_0_0;
static ParameterInfo IList_1_t7783_IList_1_IndexOf_m44195_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &InputField_t326_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44195_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.InputField>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44195_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7783_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7783_IList_1_IndexOf_m44195_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44195_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType InputField_t326_0_0_0;
static ParameterInfo IList_1_t7783_IList_1_Insert_m44196_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &InputField_t326_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44196_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44196_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7783_IList_1_Insert_m44196_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44196_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7783_IList_1_RemoveAt_m44197_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44197_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44197_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7783_IList_1_RemoveAt_m44197_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44197_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7783_IList_1_get_Item_m44193_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType InputField_t326_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44193_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.InputField>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44193_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7783_il2cpp_TypeInfo/* declaring_type */
	, &InputField_t326_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7783_IList_1_get_Item_m44193_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44193_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType InputField_t326_0_0_0;
static ParameterInfo IList_1_t7783_IList_1_set_Item_m44194_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &InputField_t326_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44194_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44194_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7783_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7783_IList_1_set_Item_m44194_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44194_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7783_MethodInfos[] =
{
	&IList_1_IndexOf_m44195_MethodInfo,
	&IList_1_Insert_m44196_MethodInfo,
	&IList_1_RemoveAt_m44197_MethodInfo,
	&IList_1_get_Item_m44193_MethodInfo,
	&IList_1_set_Item_m44194_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7783_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7782_il2cpp_TypeInfo,
	&IEnumerable_1_t7784_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7783_0_0_0;
extern Il2CppType IList_1_t7783_1_0_0;
struct IList_1_t7783;
extern Il2CppGenericClass IList_1_t7783_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7783_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7783_MethodInfos/* methods */
	, IList_1_t7783_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7783_il2cpp_TypeInfo/* element_class */
	, IList_1_t7783_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7783_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7783_0_0_0/* byval_arg */
	, &IList_1_t7783_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7783_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.InputField>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_62.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3429_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.InputField>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_62MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_ArrayTypes.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.InputField>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_60.h"
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t3430_il2cpp_TypeInfo;
extern TypeInfo Void_t99_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.InputField>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_60MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m18834_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m18836_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.InputField>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.InputField>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.InputField>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t3429____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t3429_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3429, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3429_FieldInfos[] =
{
	&CachedInvokableCall_1_t3429____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType InputField_t326_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3429_CachedInvokableCall_1__ctor_m18832_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &InputField_t326_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m18832_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.InputField>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m18832_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t3429_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3429_CachedInvokableCall_1__ctor_m18832_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m18832_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3429_CachedInvokableCall_1_Invoke_m18833_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m18833_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.InputField>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m18833_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t3429_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3429_CachedInvokableCall_1_Invoke_m18833_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m18833_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3429_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m18832_MethodInfo,
	&CachedInvokableCall_1_Invoke_m18833_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
extern MethodInfo Object_ToString_m306_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m18833_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m18837_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3429_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m18833_MethodInfo,
	&InvokableCall_1_Find_m18837_MethodInfo,
};
extern Il2CppType UnityAction_1_t3431_0_0_0;
extern TypeInfo UnityAction_1_t3431_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisInputField_t326_m34209_MethodInfo;
extern TypeInfo InputField_t326_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m18839_MethodInfo;
extern TypeInfo InputField_t326_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3429_RGCTXData[8] = 
{
	&UnityAction_1_t3431_0_0_0/* Type Usage */,
	&UnityAction_1_t3431_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisInputField_t326_m34209_MethodInfo/* Method Usage */,
	&InputField_t326_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m18839_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m18834_MethodInfo/* Method Usage */,
	&InputField_t326_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m18836_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3429_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3429_1_0_0;
struct CachedInvokableCall_1_t3429;
extern Il2CppGenericClass CachedInvokableCall_1_t3429_GenericClass;
TypeInfo CachedInvokableCall_1_t3429_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3429_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3429_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3430_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3429_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3429_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3429_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3429_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3429_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3429_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3429_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3429)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.UI.InputField>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_66.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t3431_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t507_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.InputField>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_66MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6358_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m395_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m456_MethodInfo;
extern MethodInfo Delegate_Combine_m2149_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6357_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2468_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6359_MethodInfo;
extern MethodInfo Delegate_get_Target_m6536_MethodInfo;
extern MethodInfo Delegate_get_Method_m6534_MethodInfo;
struct BaseInvokableCall_t1075;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.InputField>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.InputField>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisInputField_t326_m34209(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.InputField>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.InputField>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.InputField>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.InputField>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.InputField>
extern Il2CppType UnityAction_1_t3431_0_0_1;
FieldInfo InvokableCall_1_t3430____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3431_0_0_1/* type */
	, &InvokableCall_1_t3430_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3430, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3430_FieldInfos[] =
{
	&InvokableCall_1_t3430____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3430_InvokableCall_1__ctor_m18834_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m18834_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.InputField>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m18834_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t3430_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3430_InvokableCall_1__ctor_m18834_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m18834_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3431_0_0_0;
static ParameterInfo InvokableCall_1_t3430_InvokableCall_1__ctor_m18835_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3431_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m18835_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.InputField>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m18835_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t3430_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3430_InvokableCall_1__ctor_m18835_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m18835_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t3430_InvokableCall_1_Invoke_m18836_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m18836_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.InputField>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m18836_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t3430_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3430_InvokableCall_1_Invoke_m18836_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m18836_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3430_InvokableCall_1_Find_m18837_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m18837_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.InputField>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m18837_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t3430_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3430_InvokableCall_1_Find_m18837_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m18837_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3430_MethodInfos[] =
{
	&InvokableCall_1__ctor_m18834_MethodInfo,
	&InvokableCall_1__ctor_m18835_MethodInfo,
	&InvokableCall_1_Invoke_m18836_MethodInfo,
	&InvokableCall_1_Find_m18837_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3430_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m18836_MethodInfo,
	&InvokableCall_1_Find_m18837_MethodInfo,
};
extern TypeInfo UnityAction_1_t3431_il2cpp_TypeInfo;
extern TypeInfo InputField_t326_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3430_RGCTXData[5] = 
{
	&UnityAction_1_t3431_0_0_0/* Type Usage */,
	&UnityAction_1_t3431_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisInputField_t326_m34209_MethodInfo/* Method Usage */,
	&InputField_t326_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m18839_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3430_0_0_0;
extern Il2CppType InvokableCall_1_t3430_1_0_0;
extern TypeInfo BaseInvokableCall_t1075_il2cpp_TypeInfo;
struct InvokableCall_1_t3430;
extern Il2CppGenericClass InvokableCall_1_t3430_GenericClass;
TypeInfo InvokableCall_1_t3430_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3430_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3430_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3430_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3430_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3430_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3430_0_0_0/* byval_arg */
	, &InvokableCall_1_t3430_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3430_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3430_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3430)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.InputField>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.InputField>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.InputField>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.InputField>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.InputField>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3431_UnityAction_1__ctor_m18838_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m18838_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.InputField>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m18838_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t3431_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3431_UnityAction_1__ctor_m18838_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m18838_GenericMethod/* genericMethod */

};
extern Il2CppType InputField_t326_0_0_0;
static ParameterInfo UnityAction_1_t3431_UnityAction_1_Invoke_m18839_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &InputField_t326_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m18839_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.InputField>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m18839_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t3431_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3431_UnityAction_1_Invoke_m18839_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m18839_GenericMethod/* genericMethod */

};
extern Il2CppType InputField_t326_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3431_UnityAction_1_BeginInvoke_m18840_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &InputField_t326_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m18840_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.InputField>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m18840_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t3431_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3431_UnityAction_1_BeginInvoke_m18840_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m18840_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t3431_UnityAction_1_EndInvoke_m18841_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m18841_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.InputField>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m18841_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t3431_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3431_UnityAction_1_EndInvoke_m18841_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m18841_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3431_MethodInfos[] =
{
	&UnityAction_1__ctor_m18838_MethodInfo,
	&UnityAction_1_Invoke_m18839_MethodInfo,
	&UnityAction_1_BeginInvoke_m18840_MethodInfo,
	&UnityAction_1_EndInvoke_m18841_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2241_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2242_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2243_MethodInfo;
extern MethodInfo Delegate_Clone_m2244_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2245_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2246_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2247_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m18840_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m18841_MethodInfo;
static MethodInfo* UnityAction_1_t3431_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m18839_MethodInfo,
	&UnityAction_1_BeginInvoke_m18840_MethodInfo,
	&UnityAction_1_EndInvoke_m18841_MethodInfo,
};
extern TypeInfo ICloneable_t481_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t482_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t3431_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3431_1_0_0;
extern TypeInfo MulticastDelegate_t325_il2cpp_TypeInfo;
struct UnityAction_1_t3431;
extern Il2CppGenericClass UnityAction_1_t3431_GenericClass;
TypeInfo UnityAction_1_t3431_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3431_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3431_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3431_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3431_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3431_0_0_0/* byval_arg */
	, &UnityAction_1_t3431_1_0_0/* this_arg */
	, UnityAction_1_t3431_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3431_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3431)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6119_il2cpp_TypeInfo;

// UnityEngine.UI.InputField/ContentType
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/ContentType>
extern MethodInfo IEnumerator_1_get_Current_m44198_MethodInfo;
static PropertyInfo IEnumerator_1_t6119____Current_PropertyInfo = 
{
	&IEnumerator_1_t6119_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44198_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6119_PropertyInfos[] =
{
	&IEnumerator_1_t6119____Current_PropertyInfo,
	NULL
};
extern Il2CppType ContentType_t316_0_0_0;
extern void* RuntimeInvoker_ContentType_t316 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44198_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44198_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6119_il2cpp_TypeInfo/* declaring_type */
	, &ContentType_t316_0_0_0/* return_type */
	, RuntimeInvoker_ContentType_t316/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44198_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6119_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44198_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6119_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6119_0_0_0;
extern Il2CppType IEnumerator_1_t6119_1_0_0;
struct IEnumerator_1_t6119;
extern Il2CppGenericClass IEnumerator_1_t6119_GenericClass;
TypeInfo IEnumerator_1_t6119_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6119_MethodInfos/* methods */
	, IEnumerator_1_t6119_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6119_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6119_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6119_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6119_0_0_0/* byval_arg */
	, &IEnumerator_1_t6119_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6119_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_214.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3432_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_214MethodDeclarations.h"

extern TypeInfo ContentType_t316_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18846_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisContentType_t316_m34211_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisContentType_t316_m34211 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18842_MethodInfo;
 void InternalEnumerator_1__ctor_m18842 (InternalEnumerator_1_t3432 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18843_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18843 (InternalEnumerator_1_t3432 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m18846(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18846_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ContentType_t316_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18844_MethodInfo;
 void InternalEnumerator_1_Dispose_m18844 (InternalEnumerator_1_t3432 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18845_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18845 (InternalEnumerator_1_t3432 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18846 (InternalEnumerator_1_t3432 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisContentType_t316_m34211(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisContentType_t316_m34211_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3432____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3432_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3432, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3432____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3432_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3432, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3432_FieldInfos[] =
{
	&InternalEnumerator_1_t3432____array_0_FieldInfo,
	&InternalEnumerator_1_t3432____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3432____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3432_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18843_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3432____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3432_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18846_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3432_PropertyInfos[] =
{
	&InternalEnumerator_1_t3432____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3432____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3432_InternalEnumerator_1__ctor_m18842_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18842_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18842_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18842/* method */
	, &InternalEnumerator_1_t3432_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3432_InternalEnumerator_1__ctor_m18842_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18842_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18843_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18843_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18843/* method */
	, &InternalEnumerator_1_t3432_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18843_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18844_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18844_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18844/* method */
	, &InternalEnumerator_1_t3432_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18844_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18845_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18845_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18845/* method */
	, &InternalEnumerator_1_t3432_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18845_GenericMethod/* genericMethod */

};
extern Il2CppType ContentType_t316_0_0_0;
extern void* RuntimeInvoker_ContentType_t316 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18846_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18846_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18846/* method */
	, &InternalEnumerator_1_t3432_il2cpp_TypeInfo/* declaring_type */
	, &ContentType_t316_0_0_0/* return_type */
	, RuntimeInvoker_ContentType_t316/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18846_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3432_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18842_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18843_MethodInfo,
	&InternalEnumerator_1_Dispose_m18844_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18845_MethodInfo,
	&InternalEnumerator_1_get_Current_m18846_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3432_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18843_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18845_MethodInfo,
	&InternalEnumerator_1_Dispose_m18844_MethodInfo,
	&InternalEnumerator_1_get_Current_m18846_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3432_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6119_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3432_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6119_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3432_0_0_0;
extern Il2CppType InternalEnumerator_1_t3432_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3432_GenericClass;
TypeInfo InternalEnumerator_1_t3432_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3432_MethodInfos/* methods */
	, InternalEnumerator_1_t3432_PropertyInfos/* properties */
	, InternalEnumerator_1_t3432_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3432_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3432_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3432_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3432_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3432_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3432_1_0_0/* this_arg */
	, InternalEnumerator_1_t3432_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3432_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3432)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7785_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>
extern MethodInfo ICollection_1_get_Count_m44199_MethodInfo;
static PropertyInfo ICollection_1_t7785____Count_PropertyInfo = 
{
	&ICollection_1_t7785_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44199_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44200_MethodInfo;
static PropertyInfo ICollection_1_t7785____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7785_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44200_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7785_PropertyInfos[] =
{
	&ICollection_1_t7785____Count_PropertyInfo,
	&ICollection_1_t7785____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44199_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::get_Count()
MethodInfo ICollection_1_get_Count_m44199_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7785_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44199_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44200_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44200_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7785_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44200_GenericMethod/* genericMethod */

};
extern Il2CppType ContentType_t316_0_0_0;
extern Il2CppType ContentType_t316_0_0_0;
static ParameterInfo ICollection_1_t7785_ICollection_1_Add_m44201_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContentType_t316_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44201_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::Add(T)
MethodInfo ICollection_1_Add_m44201_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7785_ICollection_1_Add_m44201_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44201_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44202_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::Clear()
MethodInfo ICollection_1_Clear_m44202_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44202_GenericMethod/* genericMethod */

};
extern Il2CppType ContentType_t316_0_0_0;
static ParameterInfo ICollection_1_t7785_ICollection_1_Contains_m44203_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContentType_t316_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44203_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::Contains(T)
MethodInfo ICollection_1_Contains_m44203_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7785_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7785_ICollection_1_Contains_m44203_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44203_GenericMethod/* genericMethod */

};
extern Il2CppType ContentTypeU5BU5D_t334_0_0_0;
extern Il2CppType ContentTypeU5BU5D_t334_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7785_ICollection_1_CopyTo_m44204_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ContentTypeU5BU5D_t334_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44204_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44204_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7785_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7785_ICollection_1_CopyTo_m44204_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44204_GenericMethod/* genericMethod */

};
extern Il2CppType ContentType_t316_0_0_0;
static ParameterInfo ICollection_1_t7785_ICollection_1_Remove_m44205_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContentType_t316_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44205_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/ContentType>::Remove(T)
MethodInfo ICollection_1_Remove_m44205_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7785_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7785_ICollection_1_Remove_m44205_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44205_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7785_MethodInfos[] =
{
	&ICollection_1_get_Count_m44199_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44200_MethodInfo,
	&ICollection_1_Add_m44201_MethodInfo,
	&ICollection_1_Clear_m44202_MethodInfo,
	&ICollection_1_Contains_m44203_MethodInfo,
	&ICollection_1_CopyTo_m44204_MethodInfo,
	&ICollection_1_Remove_m44205_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7787_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7785_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7787_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7785_0_0_0;
extern Il2CppType ICollection_1_t7785_1_0_0;
struct ICollection_1_t7785;
extern Il2CppGenericClass ICollection_1_t7785_GenericClass;
TypeInfo ICollection_1_t7785_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7785_MethodInfos/* methods */
	, ICollection_1_t7785_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7785_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7785_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7785_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7785_0_0_0/* byval_arg */
	, &ICollection_1_t7785_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7785_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/ContentType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/ContentType>
extern Il2CppType IEnumerator_1_t6119_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44206_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/ContentType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44206_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7787_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6119_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44206_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7787_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44206_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7787_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7787_0_0_0;
extern Il2CppType IEnumerable_1_t7787_1_0_0;
struct IEnumerable_1_t7787;
extern Il2CppGenericClass IEnumerable_1_t7787_GenericClass;
TypeInfo IEnumerable_1_t7787_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7787_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7787_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7787_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7787_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7787_0_0_0/* byval_arg */
	, &IEnumerable_1_t7787_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7787_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7786_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.InputField/ContentType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/ContentType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/ContentType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.InputField/ContentType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/ContentType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.InputField/ContentType>
extern MethodInfo IList_1_get_Item_m44207_MethodInfo;
extern MethodInfo IList_1_set_Item_m44208_MethodInfo;
static PropertyInfo IList_1_t7786____Item_PropertyInfo = 
{
	&IList_1_t7786_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44207_MethodInfo/* get */
	, &IList_1_set_Item_m44208_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7786_PropertyInfos[] =
{
	&IList_1_t7786____Item_PropertyInfo,
	NULL
};
extern Il2CppType ContentType_t316_0_0_0;
static ParameterInfo IList_1_t7786_IList_1_IndexOf_m44209_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContentType_t316_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44209_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.InputField/ContentType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44209_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7786_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7786_IList_1_IndexOf_m44209_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44209_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ContentType_t316_0_0_0;
static ParameterInfo IList_1_t7786_IList_1_Insert_m44210_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ContentType_t316_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44210_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/ContentType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44210_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7786_IList_1_Insert_m44210_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44210_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7786_IList_1_RemoveAt_m44211_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44211_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/ContentType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44211_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7786_IList_1_RemoveAt_m44211_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44211_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7786_IList_1_get_Item_m44207_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ContentType_t316_0_0_0;
extern void* RuntimeInvoker_ContentType_t316_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44207_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.InputField/ContentType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44207_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7786_il2cpp_TypeInfo/* declaring_type */
	, &ContentType_t316_0_0_0/* return_type */
	, RuntimeInvoker_ContentType_t316_Int32_t93/* invoker_method */
	, IList_1_t7786_IList_1_get_Item_m44207_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44207_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ContentType_t316_0_0_0;
static ParameterInfo IList_1_t7786_IList_1_set_Item_m44208_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ContentType_t316_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44208_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/ContentType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44208_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7786_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7786_IList_1_set_Item_m44208_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44208_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7786_MethodInfos[] =
{
	&IList_1_IndexOf_m44209_MethodInfo,
	&IList_1_Insert_m44210_MethodInfo,
	&IList_1_RemoveAt_m44211_MethodInfo,
	&IList_1_get_Item_m44207_MethodInfo,
	&IList_1_set_Item_m44208_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7786_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7785_il2cpp_TypeInfo,
	&IEnumerable_1_t7787_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7786_0_0_0;
extern Il2CppType IList_1_t7786_1_0_0;
struct IList_1_t7786;
extern Il2CppGenericClass IList_1_t7786_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7786_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7786_MethodInfos/* methods */
	, IList_1_t7786_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7786_il2cpp_TypeInfo/* element_class */
	, IList_1_t7786_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7786_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7786_0_0_0/* byval_arg */
	, &IList_1_t7786_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7786_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t486_il2cpp_TypeInfo;

// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"


// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UILineInfo>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
extern MethodInfo IList_1_get_Item_m2288_MethodInfo;
extern MethodInfo IList_1_set_Item_m44212_MethodInfo;
static PropertyInfo IList_1_t486____Item_PropertyInfo = 
{
	&IList_1_t486_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m2288_MethodInfo/* get */
	, &IList_1_set_Item_m44212_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t486_PropertyInfos[] =
{
	&IList_1_t486____Item_PropertyInfo,
	NULL
};
extern Il2CppType UILineInfo_t487_0_0_0;
extern Il2CppType UILineInfo_t487_0_0_0;
static ParameterInfo IList_1_t486_IList_1_IndexOf_m44213_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UILineInfo_t487_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_UILineInfo_t487 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44213_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UILineInfo>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44213_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t486_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_UILineInfo_t487/* invoker_method */
	, IList_1_t486_IList_1_IndexOf_m44213_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44213_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType UILineInfo_t487_0_0_0;
static ParameterInfo IList_1_t486_IList_1_Insert_m44214_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UILineInfo_t487_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_UILineInfo_t487 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44214_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44214_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t486_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_UILineInfo_t487/* invoker_method */
	, IList_1_t486_IList_1_Insert_m44214_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44214_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t486_IList_1_RemoveAt_m44215_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44215_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44215_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t486_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t486_IList_1_RemoveAt_m44215_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44215_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t486_IList_1_get_Item_m2288_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType UILineInfo_t487_0_0_0;
extern void* RuntimeInvoker_UILineInfo_t487_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m2288_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m2288_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t486_il2cpp_TypeInfo/* declaring_type */
	, &UILineInfo_t487_0_0_0/* return_type */
	, RuntimeInvoker_UILineInfo_t487_Int32_t93/* invoker_method */
	, IList_1_t486_IList_1_get_Item_m2288_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m2288_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType UILineInfo_t487_0_0_0;
static ParameterInfo IList_1_t486_IList_1_set_Item_m44212_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UILineInfo_t487_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_UILineInfo_t487 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44212_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44212_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t486_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_UILineInfo_t487/* invoker_method */
	, IList_1_t486_IList_1_set_Item_m44212_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44212_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t486_MethodInfos[] =
{
	&IList_1_IndexOf_m44213_MethodInfo,
	&IList_1_Insert_m44214_MethodInfo,
	&IList_1_RemoveAt_m44215_MethodInfo,
	&IList_1_get_Item_m2288_MethodInfo,
	&IList_1_set_Item_m44212_MethodInfo,
	NULL
};
extern TypeInfo ICollection_1_t490_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t4702_il2cpp_TypeInfo;
static TypeInfo* IList_1_t486_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t490_il2cpp_TypeInfo,
	&IEnumerable_1_t4702_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t486_0_0_0;
extern Il2CppType IList_1_t486_1_0_0;
struct IList_1_t486;
extern Il2CppGenericClass IList_1_t486_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t486_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t486_MethodInfos/* methods */
	, IList_1_t486_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t486_il2cpp_TypeInfo/* element_class */
	, IList_1_t486_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t486_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t486_0_0_0/* byval_arg */
	, &IList_1_t486_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t486_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>
extern MethodInfo ICollection_1_get_Count_m2313_MethodInfo;
static PropertyInfo ICollection_1_t490____Count_PropertyInfo = 
{
	&ICollection_1_t490_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m2313_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44216_MethodInfo;
static PropertyInfo ICollection_1_t490____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t490_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44216_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t490_PropertyInfos[] =
{
	&ICollection_1_t490____Count_PropertyInfo,
	&ICollection_1_t490____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m2313_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::get_Count()
MethodInfo ICollection_1_get_Count_m2313_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t490_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m2313_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44216_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44216_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t490_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44216_GenericMethod/* genericMethod */

};
extern Il2CppType UILineInfo_t487_0_0_0;
static ParameterInfo ICollection_1_t490_ICollection_1_Add_m44217_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UILineInfo_t487_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_UILineInfo_t487 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44217_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::Add(T)
MethodInfo ICollection_1_Add_m44217_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t490_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_UILineInfo_t487/* invoker_method */
	, ICollection_1_t490_ICollection_1_Add_m44217_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44217_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44218_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::Clear()
MethodInfo ICollection_1_Clear_m44218_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t490_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44218_GenericMethod/* genericMethod */

};
extern Il2CppType UILineInfo_t487_0_0_0;
static ParameterInfo ICollection_1_t490_ICollection_1_Contains_m44219_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UILineInfo_t487_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_UILineInfo_t487 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44219_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::Contains(T)
MethodInfo ICollection_1_Contains_m44219_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t490_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_UILineInfo_t487/* invoker_method */
	, ICollection_1_t490_ICollection_1_Contains_m44219_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44219_GenericMethod/* genericMethod */

};
extern Il2CppType UILineInfoU5BU5D_t1031_0_0_0;
extern Il2CppType UILineInfoU5BU5D_t1031_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t490_ICollection_1_CopyTo_m44220_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UILineInfoU5BU5D_t1031_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44220_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44220_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t490_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t490_ICollection_1_CopyTo_m44220_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44220_GenericMethod/* genericMethod */

};
extern Il2CppType UILineInfo_t487_0_0_0;
static ParameterInfo ICollection_1_t490_ICollection_1_Remove_m44221_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UILineInfo_t487_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_UILineInfo_t487 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44221_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UILineInfo>::Remove(T)
MethodInfo ICollection_1_Remove_m44221_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t490_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_UILineInfo_t487/* invoker_method */
	, ICollection_1_t490_ICollection_1_Remove_m44221_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44221_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t490_MethodInfos[] =
{
	&ICollection_1_get_Count_m2313_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44216_MethodInfo,
	&ICollection_1_Add_m44217_MethodInfo,
	&ICollection_1_Clear_m44218_MethodInfo,
	&ICollection_1_Contains_m44219_MethodInfo,
	&ICollection_1_CopyTo_m44220_MethodInfo,
	&ICollection_1_Remove_m44221_MethodInfo,
	NULL
};
static TypeInfo* ICollection_1_t490_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t4702_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t490_0_0_0;
extern Il2CppType ICollection_1_t490_1_0_0;
struct ICollection_1_t490;
extern Il2CppGenericClass ICollection_1_t490_GenericClass;
TypeInfo ICollection_1_t490_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t490_MethodInfos/* methods */
	, ICollection_1_t490_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t490_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t490_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t490_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t490_0_0_0/* byval_arg */
	, &ICollection_1_t490_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t490_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UILineInfo>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UILineInfo>
extern Il2CppType IEnumerator_1_t4703_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44222_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UILineInfo>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44222_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t4702_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t4703_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44222_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t4702_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44222_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t4702_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t4702_0_0_0;
extern Il2CppType IEnumerable_1_t4702_1_0_0;
struct IEnumerable_1_t4702;
extern Il2CppGenericClass IEnumerable_1_t4702_GenericClass;
TypeInfo IEnumerable_1_t4702_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t4702_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t4702_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t4702_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t4702_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t4702_0_0_0/* byval_arg */
	, &IEnumerable_1_t4702_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t4702_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t4703_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
extern MethodInfo IEnumerator_1_get_Current_m44223_MethodInfo;
static PropertyInfo IEnumerator_1_t4703____Current_PropertyInfo = 
{
	&IEnumerator_1_t4703_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44223_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t4703_PropertyInfos[] =
{
	&IEnumerator_1_t4703____Current_PropertyInfo,
	NULL
};
extern Il2CppType UILineInfo_t487_0_0_0;
extern void* RuntimeInvoker_UILineInfo_t487 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44223_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44223_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t4703_il2cpp_TypeInfo/* declaring_type */
	, &UILineInfo_t487_0_0_0/* return_type */
	, RuntimeInvoker_UILineInfo_t487/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44223_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t4703_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44223_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t4703_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t4703_0_0_0;
extern Il2CppType IEnumerator_1_t4703_1_0_0;
struct IEnumerator_1_t4703;
extern Il2CppGenericClass IEnumerator_1_t4703_GenericClass;
TypeInfo IEnumerator_1_t4703_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t4703_MethodInfos/* methods */
	, IEnumerator_1_t4703_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t4703_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t4703_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t4703_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t4703_0_0_0/* byval_arg */
	, &IEnumerator_1_t4703_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t4703_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_215.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3433_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_215MethodDeclarations.h"

extern TypeInfo UILineInfo_t487_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18851_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUILineInfo_t487_m34222_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
 UILineInfo_t487  Array_InternalArray__get_Item_TisUILineInfo_t487_m34222 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18847_MethodInfo;
 void InternalEnumerator_1__ctor_m18847 (InternalEnumerator_1_t3433 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18848_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18848 (InternalEnumerator_1_t3433 * __this, MethodInfo* method){
	{
		UILineInfo_t487  L_0 = InternalEnumerator_1_get_Current_m18851(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18851_MethodInfo);
		UILineInfo_t487  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&UILineInfo_t487_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18849_MethodInfo;
 void InternalEnumerator_1_Dispose_m18849 (InternalEnumerator_1_t3433 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18850_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18850 (InternalEnumerator_1_t3433 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::get_Current()
 UILineInfo_t487  InternalEnumerator_1_get_Current_m18851 (InternalEnumerator_1_t3433 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		UILineInfo_t487  L_8 = Array_InternalArray__get_Item_TisUILineInfo_t487_m34222(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisUILineInfo_t487_m34222_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3433____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3433_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3433, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3433____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3433_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3433, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3433_FieldInfos[] =
{
	&InternalEnumerator_1_t3433____array_0_FieldInfo,
	&InternalEnumerator_1_t3433____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3433____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3433_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18848_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3433____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3433_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18851_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3433_PropertyInfos[] =
{
	&InternalEnumerator_1_t3433____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3433____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3433_InternalEnumerator_1__ctor_m18847_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18847_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18847_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18847/* method */
	, &InternalEnumerator_1_t3433_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3433_InternalEnumerator_1__ctor_m18847_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18847_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18848_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18848_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18848/* method */
	, &InternalEnumerator_1_t3433_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18848_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18849_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18849_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18849/* method */
	, &InternalEnumerator_1_t3433_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18849_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18850_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18850_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18850/* method */
	, &InternalEnumerator_1_t3433_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18850_GenericMethod/* genericMethod */

};
extern Il2CppType UILineInfo_t487_0_0_0;
extern void* RuntimeInvoker_UILineInfo_t487 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18851_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18851_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18851/* method */
	, &InternalEnumerator_1_t3433_il2cpp_TypeInfo/* declaring_type */
	, &UILineInfo_t487_0_0_0/* return_type */
	, RuntimeInvoker_UILineInfo_t487/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18851_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3433_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18847_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18848_MethodInfo,
	&InternalEnumerator_1_Dispose_m18849_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18850_MethodInfo,
	&InternalEnumerator_1_get_Current_m18851_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3433_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18848_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18850_MethodInfo,
	&InternalEnumerator_1_Dispose_m18849_MethodInfo,
	&InternalEnumerator_1_get_Current_m18851_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3433_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t4703_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3433_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t4703_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3433_0_0_0;
extern Il2CppType InternalEnumerator_1_t3433_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3433_GenericClass;
TypeInfo InternalEnumerator_1_t3433_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3433_MethodInfos/* methods */
	, InternalEnumerator_1_t3433_PropertyInfos/* properties */
	, InternalEnumerator_1_t3433_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3433_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3433_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3433_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3433_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3433_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3433_1_0_0/* this_arg */
	, InternalEnumerator_1_t3433_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3433_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3433)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t488_il2cpp_TypeInfo;

// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"


// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UICharInfo>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
extern MethodInfo IList_1_get_Item_m2292_MethodInfo;
extern MethodInfo IList_1_set_Item_m44224_MethodInfo;
static PropertyInfo IList_1_t488____Item_PropertyInfo = 
{
	&IList_1_t488_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m2292_MethodInfo/* get */
	, &IList_1_set_Item_m44224_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t488_PropertyInfos[] =
{
	&IList_1_t488____Item_PropertyInfo,
	NULL
};
extern Il2CppType UICharInfo_t489_0_0_0;
extern Il2CppType UICharInfo_t489_0_0_0;
static ParameterInfo IList_1_t488_IList_1_IndexOf_m44225_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UICharInfo_t489_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_UICharInfo_t489 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44225_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UICharInfo>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44225_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t488_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_UICharInfo_t489/* invoker_method */
	, IList_1_t488_IList_1_IndexOf_m44225_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44225_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType UICharInfo_t489_0_0_0;
static ParameterInfo IList_1_t488_IList_1_Insert_m44226_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UICharInfo_t489_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_UICharInfo_t489 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44226_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44226_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_UICharInfo_t489/* invoker_method */
	, IList_1_t488_IList_1_Insert_m44226_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44226_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t488_IList_1_RemoveAt_m44227_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44227_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44227_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t488_IList_1_RemoveAt_m44227_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44227_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t488_IList_1_get_Item_m2292_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType UICharInfo_t489_0_0_0;
extern void* RuntimeInvoker_UICharInfo_t489_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m2292_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m2292_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t488_il2cpp_TypeInfo/* declaring_type */
	, &UICharInfo_t489_0_0_0/* return_type */
	, RuntimeInvoker_UICharInfo_t489_Int32_t93/* invoker_method */
	, IList_1_t488_IList_1_get_Item_m2292_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m2292_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType UICharInfo_t489_0_0_0;
static ParameterInfo IList_1_t488_IList_1_set_Item_m44224_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UICharInfo_t489_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_UICharInfo_t489 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44224_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44224_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_UICharInfo_t489/* invoker_method */
	, IList_1_t488_IList_1_set_Item_m44224_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44224_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t488_MethodInfos[] =
{
	&IList_1_IndexOf_m44225_MethodInfo,
	&IList_1_Insert_m44226_MethodInfo,
	&IList_1_RemoveAt_m44227_MethodInfo,
	&IList_1_get_Item_m2292_MethodInfo,
	&IList_1_set_Item_m44224_MethodInfo,
	NULL
};
extern TypeInfo ICollection_1_t4692_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t4690_il2cpp_TypeInfo;
static TypeInfo* IList_1_t488_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t4692_il2cpp_TypeInfo,
	&IEnumerable_1_t4690_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t488_0_0_0;
extern Il2CppType IList_1_t488_1_0_0;
struct IList_1_t488;
extern Il2CppGenericClass IList_1_t488_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t488_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t488_MethodInfos/* methods */
	, IList_1_t488_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t488_il2cpp_TypeInfo/* element_class */
	, IList_1_t488_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t488_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t488_0_0_0/* byval_arg */
	, &IList_1_t488_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t488_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>
extern MethodInfo ICollection_1_get_Count_m44228_MethodInfo;
static PropertyInfo ICollection_1_t4692____Count_PropertyInfo = 
{
	&ICollection_1_t4692_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44228_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44229_MethodInfo;
static PropertyInfo ICollection_1_t4692____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t4692_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44229_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t4692_PropertyInfos[] =
{
	&ICollection_1_t4692____Count_PropertyInfo,
	&ICollection_1_t4692____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44228_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::get_Count()
MethodInfo ICollection_1_get_Count_m44228_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t4692_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44228_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44229_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44229_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t4692_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44229_GenericMethod/* genericMethod */

};
extern Il2CppType UICharInfo_t489_0_0_0;
static ParameterInfo ICollection_1_t4692_ICollection_1_Add_m44230_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UICharInfo_t489_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_UICharInfo_t489 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44230_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::Add(T)
MethodInfo ICollection_1_Add_m44230_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t4692_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_UICharInfo_t489/* invoker_method */
	, ICollection_1_t4692_ICollection_1_Add_m44230_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44230_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44231_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::Clear()
MethodInfo ICollection_1_Clear_m44231_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t4692_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44231_GenericMethod/* genericMethod */

};
extern Il2CppType UICharInfo_t489_0_0_0;
static ParameterInfo ICollection_1_t4692_ICollection_1_Contains_m44232_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UICharInfo_t489_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_UICharInfo_t489 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44232_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::Contains(T)
MethodInfo ICollection_1_Contains_m44232_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t4692_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_UICharInfo_t489/* invoker_method */
	, ICollection_1_t4692_ICollection_1_Contains_m44232_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44232_GenericMethod/* genericMethod */

};
extern Il2CppType UICharInfoU5BU5D_t1030_0_0_0;
extern Il2CppType UICharInfoU5BU5D_t1030_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t4692_ICollection_1_CopyTo_m44233_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UICharInfoU5BU5D_t1030_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44233_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44233_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t4692_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t4692_ICollection_1_CopyTo_m44233_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44233_GenericMethod/* genericMethod */

};
extern Il2CppType UICharInfo_t489_0_0_0;
static ParameterInfo ICollection_1_t4692_ICollection_1_Remove_m44234_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UICharInfo_t489_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_UICharInfo_t489 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44234_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>::Remove(T)
MethodInfo ICollection_1_Remove_m44234_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t4692_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_UICharInfo_t489/* invoker_method */
	, ICollection_1_t4692_ICollection_1_Remove_m44234_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44234_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t4692_MethodInfos[] =
{
	&ICollection_1_get_Count_m44228_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44229_MethodInfo,
	&ICollection_1_Add_m44230_MethodInfo,
	&ICollection_1_Clear_m44231_MethodInfo,
	&ICollection_1_Contains_m44232_MethodInfo,
	&ICollection_1_CopyTo_m44233_MethodInfo,
	&ICollection_1_Remove_m44234_MethodInfo,
	NULL
};
static TypeInfo* ICollection_1_t4692_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t4690_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t4692_0_0_0;
extern Il2CppType ICollection_1_t4692_1_0_0;
struct ICollection_1_t4692;
extern Il2CppGenericClass ICollection_1_t4692_GenericClass;
TypeInfo ICollection_1_t4692_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t4692_MethodInfos/* methods */
	, ICollection_1_t4692_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t4692_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t4692_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t4692_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t4692_0_0_0/* byval_arg */
	, &ICollection_1_t4692_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t4692_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UICharInfo>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UICharInfo>
extern Il2CppType IEnumerator_1_t4691_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44235_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UICharInfo>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44235_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t4690_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t4691_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44235_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t4690_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44235_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t4690_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t4690_0_0_0;
extern Il2CppType IEnumerable_1_t4690_1_0_0;
struct IEnumerable_1_t4690;
extern Il2CppGenericClass IEnumerable_1_t4690_GenericClass;
TypeInfo IEnumerable_1_t4690_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t4690_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t4690_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t4690_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t4690_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t4690_0_0_0/* byval_arg */
	, &IEnumerable_1_t4690_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t4690_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t4691_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
extern MethodInfo IEnumerator_1_get_Current_m44236_MethodInfo;
static PropertyInfo IEnumerator_1_t4691____Current_PropertyInfo = 
{
	&IEnumerator_1_t4691_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44236_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t4691_PropertyInfos[] =
{
	&IEnumerator_1_t4691____Current_PropertyInfo,
	NULL
};
extern Il2CppType UICharInfo_t489_0_0_0;
extern void* RuntimeInvoker_UICharInfo_t489 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44236_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44236_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t4691_il2cpp_TypeInfo/* declaring_type */
	, &UICharInfo_t489_0_0_0/* return_type */
	, RuntimeInvoker_UICharInfo_t489/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44236_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t4691_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44236_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t4691_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t4691_0_0_0;
extern Il2CppType IEnumerator_1_t4691_1_0_0;
struct IEnumerator_1_t4691;
extern Il2CppGenericClass IEnumerator_1_t4691_GenericClass;
TypeInfo IEnumerator_1_t4691_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t4691_MethodInfos/* methods */
	, IEnumerator_1_t4691_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t4691_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t4691_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t4691_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t4691_0_0_0/* byval_arg */
	, &IEnumerator_1_t4691_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t4691_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_216.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3434_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_216MethodDeclarations.h"

extern TypeInfo UICharInfo_t489_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18856_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUICharInfo_t489_m34233_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
 UICharInfo_t489  Array_InternalArray__get_Item_TisUICharInfo_t489_m34233 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18852_MethodInfo;
 void InternalEnumerator_1__ctor_m18852 (InternalEnumerator_1_t3434 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18853_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18853 (InternalEnumerator_1_t3434 * __this, MethodInfo* method){
	{
		UICharInfo_t489  L_0 = InternalEnumerator_1_get_Current_m18856(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18856_MethodInfo);
		UICharInfo_t489  L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&UICharInfo_t489_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18854_MethodInfo;
 void InternalEnumerator_1_Dispose_m18854 (InternalEnumerator_1_t3434 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18855_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18855 (InternalEnumerator_1_t3434 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
 UICharInfo_t489  InternalEnumerator_1_get_Current_m18856 (InternalEnumerator_1_t3434 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		UICharInfo_t489  L_8 = Array_InternalArray__get_Item_TisUICharInfo_t489_m34233(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisUICharInfo_t489_m34233_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3434____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3434_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3434, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3434____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3434_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3434, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3434_FieldInfos[] =
{
	&InternalEnumerator_1_t3434____array_0_FieldInfo,
	&InternalEnumerator_1_t3434____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3434____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3434_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18853_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3434____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3434_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18856_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3434_PropertyInfos[] =
{
	&InternalEnumerator_1_t3434____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3434____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3434_InternalEnumerator_1__ctor_m18852_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18852_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18852_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18852/* method */
	, &InternalEnumerator_1_t3434_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3434_InternalEnumerator_1__ctor_m18852_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18852_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18853_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18853_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18853/* method */
	, &InternalEnumerator_1_t3434_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18853_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18854_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18854_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18854/* method */
	, &InternalEnumerator_1_t3434_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18854_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18855_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18855_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18855/* method */
	, &InternalEnumerator_1_t3434_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18855_GenericMethod/* genericMethod */

};
extern Il2CppType UICharInfo_t489_0_0_0;
extern void* RuntimeInvoker_UICharInfo_t489 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18856_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18856_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18856/* method */
	, &InternalEnumerator_1_t3434_il2cpp_TypeInfo/* declaring_type */
	, &UICharInfo_t489_0_0_0/* return_type */
	, RuntimeInvoker_UICharInfo_t489/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18856_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3434_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18852_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18853_MethodInfo,
	&InternalEnumerator_1_Dispose_m18854_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18855_MethodInfo,
	&InternalEnumerator_1_get_Current_m18856_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3434_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18853_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18855_MethodInfo,
	&InternalEnumerator_1_Dispose_m18854_MethodInfo,
	&InternalEnumerator_1_get_Current_m18856_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3434_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t4691_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3434_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t4691_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3434_0_0_0;
extern Il2CppType InternalEnumerator_1_t3434_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3434_GenericClass;
TypeInfo InternalEnumerator_1_t3434_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3434_MethodInfos/* methods */
	, InternalEnumerator_1_t3434_PropertyInfos/* properties */
	, InternalEnumerator_1_t3434_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3434_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3434_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3434_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3434_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3434_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3434_1_0_0/* this_arg */
	, InternalEnumerator_1_t3434_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3434_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3434)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`1<System.String>
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen_1.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityEvent_1_t321_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent`1<System.String>
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen_1MethodDeclarations.h"

// UnityEngine.Events.UnityAction`1<System.String>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_67.h"
// UnityEngine.Events.InvokableCall`1<System.String>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_61.h"
extern TypeInfo TypeU5BU5D_t878_il2cpp_TypeInfo;
extern TypeInfo String_t_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t479_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
// UnityEngine.Events.InvokableCall`1<System.String>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_61MethodDeclarations.h"
extern Il2CppType String_t_0_0_0;
extern MethodInfo UnityEventBase__ctor_m6379_MethodInfo;
extern MethodInfo UnityEvent_1_GetDelegate_m2238_MethodInfo;
extern MethodInfo UnityEventBase_AddCall_m6384_MethodInfo;
extern MethodInfo UnityEventBase_RemoveListener_m6385_MethodInfo;
extern MethodInfo UnityEventBase_GetValidMethodInfo_m6387_MethodInfo;
extern MethodInfo InvokableCall_1__ctor_m2239_MethodInfo;
extern MethodInfo InvokableCall_1__ctor_m2240_MethodInfo;
extern MethodInfo UnityEventBase_Invoke_m6386_MethodInfo;


// System.Void UnityEngine.Events.UnityEvent`1<System.String>::.ctor()
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.String>::FindMethod_Impl(System.String,System.Object)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.String>::GetDelegate(System.Object,System.Reflection.MethodInfo)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.String>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::Invoke(T0)
// Metadata Definition UnityEngine.Events.UnityEvent`1<System.String>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo UnityEvent_1_t321____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &UnityEvent_1_t321_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEvent_1_t321, ___m_InvokeArray_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_1_t321_FieldInfos[] =
{
	&UnityEvent_1_t321____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_1__ctor_m2235_GenericMethod;
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::.ctor()
MethodInfo UnityEvent_1__ctor_m2235_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent_1__ctor_m16438_gshared/* method */
	, &UnityEvent_1_t321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_1__ctor_m2235_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3435_0_0_0;
extern Il2CppType UnityAction_1_t3435_0_0_0;
static ParameterInfo UnityEvent_1_t321_UnityEvent_1_AddListener_m18857_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3435_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_1_AddListener_m18857_GenericMethod;
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
MethodInfo UnityEvent_1_AddListener_m18857_MethodInfo = 
{
	"AddListener"/* name */
	, (methodPointerType)&UnityEvent_1_AddListener_m16440_gshared/* method */
	, &UnityEvent_1_t321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityEvent_1_t321_UnityEvent_1_AddListener_m18857_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_1_AddListener_m18857_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3435_0_0_0;
static ParameterInfo UnityEvent_1_t321_UnityEvent_1_RemoveListener_m18858_ParameterInfos[] = 
{
	{"call", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3435_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_1_RemoveListener_m18858_GenericMethod;
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
MethodInfo UnityEvent_1_RemoveListener_m18858_MethodInfo = 
{
	"RemoveListener"/* name */
	, (methodPointerType)&UnityEvent_1_RemoveListener_m16442_gshared/* method */
	, &UnityEvent_1_t321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityEvent_1_t321_UnityEvent_1_RemoveListener_m18858_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_1_RemoveListener_m18858_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_1_t321_UnityEvent_1_FindMethod_Impl_m2236_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t142_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_1_FindMethod_Impl_m2236_GenericMethod;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<System.String>::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_1_FindMethod_Impl_m2236_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_1_FindMethod_Impl_m16443_gshared/* method */
	, &UnityEvent_1_t321_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t142_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_1_t321_UnityEvent_1_FindMethod_Impl_m2236_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_1_FindMethod_Impl_m2236_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo UnityEvent_1_t321_UnityEvent_1_GetDelegate_m2237_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_1_GetDelegate_m2237_GenericMethod;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.String>::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_1_GetDelegate_m2237_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_1_GetDelegate_m16444_gshared/* method */
	, &UnityEvent_1_t321_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_1_t321_UnityEvent_1_GetDelegate_m2237_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_1_GetDelegate_m2237_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3435_0_0_0;
static ParameterInfo UnityEvent_1_t321_UnityEvent_1_GetDelegate_m2238_ParameterInfos[] = 
{
	{"action", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3435_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_1_GetDelegate_m2238_GenericMethod;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<System.String>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
MethodInfo UnityEvent_1_GetDelegate_m2238_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_1_GetDelegate_m16445_gshared/* method */
	, &UnityEvent_1_t321_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, UnityEvent_1_t321_UnityEvent_1_GetDelegate_m2238_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_1_GetDelegate_m2238_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityEvent_1_t321_UnityEvent_1_Invoke_m2308_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_1_Invoke_m2308_GenericMethod;
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::Invoke(T0)
MethodInfo UnityEvent_1_Invoke_m2308_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityEvent_1_Invoke_m16446_gshared/* method */
	, &UnityEvent_1_t321_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityEvent_1_t321_UnityEvent_1_Invoke_m2308_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_1_Invoke_m2308_GenericMethod/* genericMethod */

};
static MethodInfo* UnityEvent_1_t321_MethodInfos[] =
{
	&UnityEvent_1__ctor_m2235_MethodInfo,
	&UnityEvent_1_AddListener_m18857_MethodInfo,
	&UnityEvent_1_RemoveListener_m18858_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m2236_MethodInfo,
	&UnityEvent_1_GetDelegate_m2237_MethodInfo,
	&UnityEvent_1_GetDelegate_m2238_MethodInfo,
	&UnityEvent_1_Invoke_m2308_MethodInfo,
	NULL
};
extern MethodInfo UnityEventBase_ToString_m1903_MethodInfo;
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1904_MethodInfo;
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1905_MethodInfo;
extern MethodInfo UnityEvent_1_FindMethod_Impl_m2236_MethodInfo;
extern MethodInfo UnityEvent_1_GetDelegate_m2237_MethodInfo;
static MethodInfo* UnityEvent_1_t321_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&UnityEventBase_ToString_m1903_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1904_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1905_MethodInfo,
	&UnityEvent_1_FindMethod_Impl_m2236_MethodInfo,
	&UnityEvent_1_GetDelegate_m2237_MethodInfo,
};
extern TypeInfo ISerializationCallbackReceiver_t422_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityEvent_1_t321_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t422_il2cpp_TypeInfo, 4},
};
extern TypeInfo InvokableCall_1_t479_il2cpp_TypeInfo;
extern TypeInfo String_t_il2cpp_TypeInfo;
static Il2CppRGCTXData UnityEvent_1_t321_RGCTXData[6] = 
{
	&UnityEvent_1_GetDelegate_m2238_MethodInfo/* Method Usage */,
	&String_t_0_0_0/* Type Usage */,
	&InvokableCall_1_t479_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1__ctor_m2239_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m2240_MethodInfo/* Method Usage */,
	&String_t_il2cpp_TypeInfo/* Class Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_1_t321_0_0_0;
extern Il2CppType UnityEvent_1_t321_1_0_0;
extern TypeInfo UnityEventBase_t1084_il2cpp_TypeInfo;
struct UnityEvent_1_t321;
extern Il2CppGenericClass UnityEvent_1_t321_GenericClass;
TypeInfo UnityEvent_1_t321_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_1_t321_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_1_t321_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_1_t321_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityEvent_1_t321_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityEvent_1_t321_il2cpp_TypeInfo/* cast_class */
	, &UnityEvent_1_t321_0_0_0/* byval_arg */
	, &UnityEvent_1_t321_1_0_0/* this_arg */
	, UnityEvent_1_t321_InterfacesOffsets/* interface_offsets */
	, &UnityEvent_1_t321_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, UnityEvent_1_t321_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_1_t321)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityAction_1_t3435_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<System.String>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_67MethodDeclarations.h"



// System.Void UnityEngine.Events.UnityAction`1<System.String>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<System.String>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.String>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<System.String>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<System.String>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3435_UnityAction_1__ctor_m18859_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m18859_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.String>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m18859_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t3435_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3435_UnityAction_1__ctor_m18859_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m18859_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo UnityAction_1_t3435_UnityAction_1_Invoke_m18860_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m18860_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.String>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m18860_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t3435_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3435_UnityAction_1_Invoke_m18860_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m18860_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3435_UnityAction_1_BeginInvoke_m18861_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m18861_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.String>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m18861_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t3435_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3435_UnityAction_1_BeginInvoke_m18861_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m18861_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t3435_UnityAction_1_EndInvoke_m18862_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m18862_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<System.String>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m18862_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t3435_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3435_UnityAction_1_EndInvoke_m18862_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m18862_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3435_MethodInfos[] =
{
	&UnityAction_1__ctor_m18859_MethodInfo,
	&UnityAction_1_Invoke_m18860_MethodInfo,
	&UnityAction_1_BeginInvoke_m18861_MethodInfo,
	&UnityAction_1_EndInvoke_m18862_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_Invoke_m18860_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m18861_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m18862_MethodInfo;
static MethodInfo* UnityAction_1_t3435_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m18860_MethodInfo,
	&UnityAction_1_BeginInvoke_m18861_MethodInfo,
	&UnityAction_1_EndInvoke_m18862_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3435_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3435_1_0_0;
struct UnityAction_1_t3435;
extern Il2CppGenericClass UnityAction_1_t3435_GenericClass;
TypeInfo UnityAction_1_t3435_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3435_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3435_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3435_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3435_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3435_0_0_0/* byval_arg */
	, &UnityAction_1_t3435_1_0_0/* this_arg */
	, UnityAction_1_t3435_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3435_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3435)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisString_t_m34243_MethodInfo;
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.String>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.String>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisString_t_m34243(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<System.String>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<System.String>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<System.String>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<System.String>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<System.String>
extern Il2CppType UnityAction_1_t3435_0_0_1;
FieldInfo InvokableCall_1_t479____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3435_0_0_1/* type */
	, &InvokableCall_1_t479_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t479, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t479_FieldInfos[] =
{
	&InvokableCall_1_t479____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t479_InvokableCall_1__ctor_m2239_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m2239_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<System.String>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m2239_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t479_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t479_InvokableCall_1__ctor_m2239_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m2239_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3435_0_0_0;
static ParameterInfo InvokableCall_1_t479_InvokableCall_1__ctor_m2240_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3435_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m2240_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<System.String>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m2240_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t479_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t479_InvokableCall_1__ctor_m2240_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m2240_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t479_InvokableCall_1_Invoke_m18863_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m18863_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<System.String>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m18863_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t479_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t479_InvokableCall_1_Invoke_m18863_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m18863_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t479_InvokableCall_1_Find_m18864_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m18864_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<System.String>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m18864_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t479_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t479_InvokableCall_1_Find_m18864_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m18864_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t479_MethodInfos[] =
{
	&InvokableCall_1__ctor_m2239_MethodInfo,
	&InvokableCall_1__ctor_m2240_MethodInfo,
	&InvokableCall_1_Invoke_m18863_MethodInfo,
	&InvokableCall_1_Find_m18864_MethodInfo,
	NULL
};
extern MethodInfo InvokableCall_1_Invoke_m18863_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m18864_MethodInfo;
static MethodInfo* InvokableCall_1_t479_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m18863_MethodInfo,
	&InvokableCall_1_Find_m18864_MethodInfo,
};
extern TypeInfo UnityAction_1_t3435_il2cpp_TypeInfo;
extern TypeInfo String_t_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t479_RGCTXData[5] = 
{
	&UnityAction_1_t3435_0_0_0/* Type Usage */,
	&UnityAction_1_t3435_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisString_t_m34243_MethodInfo/* Method Usage */,
	&String_t_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m18860_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t479_0_0_0;
extern Il2CppType InvokableCall_1_t479_1_0_0;
struct InvokableCall_1_t479;
extern Il2CppGenericClass InvokableCall_1_t479_GenericClass;
TypeInfo InvokableCall_1_t479_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t479_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t479_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t479_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t479_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t479_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t479_0_0_0/* byval_arg */
	, &InvokableCall_1_t479_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t479_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t479_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t479)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6125_il2cpp_TypeInfo;

// UnityEngine.UI.InputField/InputType
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/InputType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/InputType>
extern MethodInfo IEnumerator_1_get_Current_m44237_MethodInfo;
static PropertyInfo IEnumerator_1_t6125____Current_PropertyInfo = 
{
	&IEnumerator_1_t6125_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44237_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6125_PropertyInfos[] =
{
	&IEnumerator_1_t6125____Current_PropertyInfo,
	NULL
};
extern Il2CppType InputType_t317_0_0_0;
extern void* RuntimeInvoker_InputType_t317 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44237_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/InputType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44237_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6125_il2cpp_TypeInfo/* declaring_type */
	, &InputType_t317_0_0_0/* return_type */
	, RuntimeInvoker_InputType_t317/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44237_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6125_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44237_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6125_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6125_0_0_0;
extern Il2CppType IEnumerator_1_t6125_1_0_0;
struct IEnumerator_1_t6125;
extern Il2CppGenericClass IEnumerator_1_t6125_GenericClass;
TypeInfo IEnumerator_1_t6125_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6125_MethodInfos/* methods */
	, IEnumerator_1_t6125_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6125_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6125_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6125_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6125_0_0_0/* byval_arg */
	, &IEnumerator_1_t6125_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6125_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_217.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3436_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_217MethodDeclarations.h"

extern TypeInfo InputType_t317_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18869_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisInputType_t317_m34245_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/InputType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/InputType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisInputType_t317_m34245 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18865_MethodInfo;
 void InternalEnumerator_1__ctor_m18865 (InternalEnumerator_1_t3436 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18866_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18866 (InternalEnumerator_1_t3436 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m18869(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18869_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&InputType_t317_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18867_MethodInfo;
 void InternalEnumerator_1_Dispose_m18867 (InternalEnumerator_1_t3436 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18868_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18868 (InternalEnumerator_1_t3436 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18869 (InternalEnumerator_1_t3436 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisInputType_t317_m34245(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisInputType_t317_m34245_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3436____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3436_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3436, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3436____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3436_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3436, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3436_FieldInfos[] =
{
	&InternalEnumerator_1_t3436____array_0_FieldInfo,
	&InternalEnumerator_1_t3436____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3436____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3436_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18866_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3436____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3436_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18869_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3436_PropertyInfos[] =
{
	&InternalEnumerator_1_t3436____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3436____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3436_InternalEnumerator_1__ctor_m18865_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18865_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18865_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18865/* method */
	, &InternalEnumerator_1_t3436_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3436_InternalEnumerator_1__ctor_m18865_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18865_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18866_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18866_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18866/* method */
	, &InternalEnumerator_1_t3436_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18866_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18867_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18867_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18867/* method */
	, &InternalEnumerator_1_t3436_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18867_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18868_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18868_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18868/* method */
	, &InternalEnumerator_1_t3436_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18868_GenericMethod/* genericMethod */

};
extern Il2CppType InputType_t317_0_0_0;
extern void* RuntimeInvoker_InputType_t317 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18869_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/InputType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18869_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18869/* method */
	, &InternalEnumerator_1_t3436_il2cpp_TypeInfo/* declaring_type */
	, &InputType_t317_0_0_0/* return_type */
	, RuntimeInvoker_InputType_t317/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18869_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3436_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18865_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18866_MethodInfo,
	&InternalEnumerator_1_Dispose_m18867_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18868_MethodInfo,
	&InternalEnumerator_1_get_Current_m18869_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3436_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18866_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18868_MethodInfo,
	&InternalEnumerator_1_Dispose_m18867_MethodInfo,
	&InternalEnumerator_1_get_Current_m18869_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3436_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6125_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3436_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6125_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3436_0_0_0;
extern Il2CppType InternalEnumerator_1_t3436_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3436_GenericClass;
TypeInfo InternalEnumerator_1_t3436_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3436_MethodInfos/* methods */
	, InternalEnumerator_1_t3436_PropertyInfos/* properties */
	, InternalEnumerator_1_t3436_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3436_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3436_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3436_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3436_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3436_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3436_1_0_0/* this_arg */
	, InternalEnumerator_1_t3436_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3436_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3436)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7788_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>
extern MethodInfo ICollection_1_get_Count_m44238_MethodInfo;
static PropertyInfo ICollection_1_t7788____Count_PropertyInfo = 
{
	&ICollection_1_t7788_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44238_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44239_MethodInfo;
static PropertyInfo ICollection_1_t7788____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7788_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44239_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7788_PropertyInfos[] =
{
	&ICollection_1_t7788____Count_PropertyInfo,
	&ICollection_1_t7788____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44238_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::get_Count()
MethodInfo ICollection_1_get_Count_m44238_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7788_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44238_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44239_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44239_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44239_GenericMethod/* genericMethod */

};
extern Il2CppType InputType_t317_0_0_0;
extern Il2CppType InputType_t317_0_0_0;
static ParameterInfo ICollection_1_t7788_ICollection_1_Add_m44240_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &InputType_t317_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44240_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::Add(T)
MethodInfo ICollection_1_Add_m44240_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7788_ICollection_1_Add_m44240_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44240_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44241_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::Clear()
MethodInfo ICollection_1_Clear_m44241_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44241_GenericMethod/* genericMethod */

};
extern Il2CppType InputType_t317_0_0_0;
static ParameterInfo ICollection_1_t7788_ICollection_1_Contains_m44242_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &InputType_t317_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44242_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::Contains(T)
MethodInfo ICollection_1_Contains_m44242_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7788_ICollection_1_Contains_m44242_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44242_GenericMethod/* genericMethod */

};
extern Il2CppType InputTypeU5BU5D_t5599_0_0_0;
extern Il2CppType InputTypeU5BU5D_t5599_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7788_ICollection_1_CopyTo_m44243_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &InputTypeU5BU5D_t5599_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44243_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44243_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7788_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7788_ICollection_1_CopyTo_m44243_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44243_GenericMethod/* genericMethod */

};
extern Il2CppType InputType_t317_0_0_0;
static ParameterInfo ICollection_1_t7788_ICollection_1_Remove_m44244_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &InputType_t317_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44244_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/InputType>::Remove(T)
MethodInfo ICollection_1_Remove_m44244_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7788_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7788_ICollection_1_Remove_m44244_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44244_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7788_MethodInfos[] =
{
	&ICollection_1_get_Count_m44238_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44239_MethodInfo,
	&ICollection_1_Add_m44240_MethodInfo,
	&ICollection_1_Clear_m44241_MethodInfo,
	&ICollection_1_Contains_m44242_MethodInfo,
	&ICollection_1_CopyTo_m44243_MethodInfo,
	&ICollection_1_Remove_m44244_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7790_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7788_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7790_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7788_0_0_0;
extern Il2CppType ICollection_1_t7788_1_0_0;
struct ICollection_1_t7788;
extern Il2CppGenericClass ICollection_1_t7788_GenericClass;
TypeInfo ICollection_1_t7788_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7788_MethodInfos/* methods */
	, ICollection_1_t7788_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7788_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7788_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7788_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7788_0_0_0/* byval_arg */
	, &ICollection_1_t7788_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7788_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/InputType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/InputType>
extern Il2CppType IEnumerator_1_t6125_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44245_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/InputType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44245_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7790_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6125_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44245_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7790_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44245_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7790_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7790_0_0_0;
extern Il2CppType IEnumerable_1_t7790_1_0_0;
struct IEnumerable_1_t7790;
extern Il2CppGenericClass IEnumerable_1_t7790_GenericClass;
TypeInfo IEnumerable_1_t7790_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7790_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7790_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7790_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7790_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7790_0_0_0/* byval_arg */
	, &IEnumerable_1_t7790_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7790_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7789_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.InputField/InputType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/InputType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/InputType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.InputField/InputType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/InputType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.InputField/InputType>
extern MethodInfo IList_1_get_Item_m44246_MethodInfo;
extern MethodInfo IList_1_set_Item_m44247_MethodInfo;
static PropertyInfo IList_1_t7789____Item_PropertyInfo = 
{
	&IList_1_t7789_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44246_MethodInfo/* get */
	, &IList_1_set_Item_m44247_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7789_PropertyInfos[] =
{
	&IList_1_t7789____Item_PropertyInfo,
	NULL
};
extern Il2CppType InputType_t317_0_0_0;
static ParameterInfo IList_1_t7789_IList_1_IndexOf_m44248_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &InputType_t317_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44248_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.InputField/InputType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44248_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7789_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7789_IList_1_IndexOf_m44248_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44248_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType InputType_t317_0_0_0;
static ParameterInfo IList_1_t7789_IList_1_Insert_m44249_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &InputType_t317_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44249_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/InputType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44249_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7789_IList_1_Insert_m44249_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44249_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7789_IList_1_RemoveAt_m44250_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44250_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/InputType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44250_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7789_IList_1_RemoveAt_m44250_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44250_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7789_IList_1_get_Item_m44246_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType InputType_t317_0_0_0;
extern void* RuntimeInvoker_InputType_t317_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44246_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.InputField/InputType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44246_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7789_il2cpp_TypeInfo/* declaring_type */
	, &InputType_t317_0_0_0/* return_type */
	, RuntimeInvoker_InputType_t317_Int32_t93/* invoker_method */
	, IList_1_t7789_IList_1_get_Item_m44246_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44246_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType InputType_t317_0_0_0;
static ParameterInfo IList_1_t7789_IList_1_set_Item_m44247_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &InputType_t317_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44247_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/InputType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44247_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7789_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7789_IList_1_set_Item_m44247_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44247_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7789_MethodInfos[] =
{
	&IList_1_IndexOf_m44248_MethodInfo,
	&IList_1_Insert_m44249_MethodInfo,
	&IList_1_RemoveAt_m44250_MethodInfo,
	&IList_1_get_Item_m44246_MethodInfo,
	&IList_1_set_Item_m44247_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7789_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7788_il2cpp_TypeInfo,
	&IEnumerable_1_t7790_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7789_0_0_0;
extern Il2CppType IList_1_t7789_1_0_0;
struct IList_1_t7789;
extern Il2CppGenericClass IList_1_t7789_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7789_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7789_MethodInfos/* methods */
	, IList_1_t7789_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7789_il2cpp_TypeInfo/* element_class */
	, IList_1_t7789_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7789_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7789_0_0_0/* byval_arg */
	, &IList_1_t7789_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7789_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6126_il2cpp_TypeInfo;

// UnityEngine.UI.InputField/CharacterValidation
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidation.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>
extern MethodInfo IEnumerator_1_get_Current_m44251_MethodInfo;
static PropertyInfo IEnumerator_1_t6126____Current_PropertyInfo = 
{
	&IEnumerator_1_t6126_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44251_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6126_PropertyInfos[] =
{
	&IEnumerator_1_t6126____Current_PropertyInfo,
	NULL
};
extern Il2CppType CharacterValidation_t318_0_0_0;
extern void* RuntimeInvoker_CharacterValidation_t318 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44251_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44251_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6126_il2cpp_TypeInfo/* declaring_type */
	, &CharacterValidation_t318_0_0_0/* return_type */
	, RuntimeInvoker_CharacterValidation_t318/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44251_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6126_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44251_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6126_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6126_0_0_0;
extern Il2CppType IEnumerator_1_t6126_1_0_0;
struct IEnumerator_1_t6126;
extern Il2CppGenericClass IEnumerator_1_t6126_GenericClass;
TypeInfo IEnumerator_1_t6126_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6126_MethodInfos/* methods */
	, IEnumerator_1_t6126_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6126_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6126_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6126_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6126_0_0_0/* byval_arg */
	, &IEnumerator_1_t6126_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6126_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_218.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3437_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_218MethodDeclarations.h"

extern TypeInfo CharacterValidation_t318_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18874_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCharacterValidation_t318_m34256_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/CharacterValidation>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/CharacterValidation>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisCharacterValidation_t318_m34256 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18870_MethodInfo;
 void InternalEnumerator_1__ctor_m18870 (InternalEnumerator_1_t3437 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18871_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18871 (InternalEnumerator_1_t3437 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m18874(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18874_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&CharacterValidation_t318_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18872_MethodInfo;
 void InternalEnumerator_1_Dispose_m18872 (InternalEnumerator_1_t3437 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18873_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18873 (InternalEnumerator_1_t3437 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18874 (InternalEnumerator_1_t3437 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisCharacterValidation_t318_m34256(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisCharacterValidation_t318_m34256_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3437____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3437_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3437, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3437____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3437_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3437, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3437_FieldInfos[] =
{
	&InternalEnumerator_1_t3437____array_0_FieldInfo,
	&InternalEnumerator_1_t3437____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3437____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3437_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18871_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3437____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3437_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18874_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3437_PropertyInfos[] =
{
	&InternalEnumerator_1_t3437____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3437____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3437_InternalEnumerator_1__ctor_m18870_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18870_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18870_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18870/* method */
	, &InternalEnumerator_1_t3437_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3437_InternalEnumerator_1__ctor_m18870_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18870_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18871_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18871_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18871/* method */
	, &InternalEnumerator_1_t3437_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18871_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18872_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18872_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18872/* method */
	, &InternalEnumerator_1_t3437_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18872_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18873_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18873_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18873/* method */
	, &InternalEnumerator_1_t3437_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18873_GenericMethod/* genericMethod */

};
extern Il2CppType CharacterValidation_t318_0_0_0;
extern void* RuntimeInvoker_CharacterValidation_t318 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18874_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/CharacterValidation>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18874_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18874/* method */
	, &InternalEnumerator_1_t3437_il2cpp_TypeInfo/* declaring_type */
	, &CharacterValidation_t318_0_0_0/* return_type */
	, RuntimeInvoker_CharacterValidation_t318/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18874_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3437_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18870_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18871_MethodInfo,
	&InternalEnumerator_1_Dispose_m18872_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18873_MethodInfo,
	&InternalEnumerator_1_get_Current_m18874_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3437_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18871_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18873_MethodInfo,
	&InternalEnumerator_1_Dispose_m18872_MethodInfo,
	&InternalEnumerator_1_get_Current_m18874_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3437_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6126_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3437_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6126_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3437_0_0_0;
extern Il2CppType InternalEnumerator_1_t3437_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3437_GenericClass;
TypeInfo InternalEnumerator_1_t3437_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3437_MethodInfos/* methods */
	, InternalEnumerator_1_t3437_PropertyInfos/* properties */
	, InternalEnumerator_1_t3437_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3437_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3437_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3437_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3437_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3437_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3437_1_0_0/* this_arg */
	, InternalEnumerator_1_t3437_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3437_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3437)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7791_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>
extern MethodInfo ICollection_1_get_Count_m44252_MethodInfo;
static PropertyInfo ICollection_1_t7791____Count_PropertyInfo = 
{
	&ICollection_1_t7791_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44252_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44253_MethodInfo;
static PropertyInfo ICollection_1_t7791____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7791_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44253_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7791_PropertyInfos[] =
{
	&ICollection_1_t7791____Count_PropertyInfo,
	&ICollection_1_t7791____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44252_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::get_Count()
MethodInfo ICollection_1_get_Count_m44252_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7791_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44252_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44253_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44253_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7791_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44253_GenericMethod/* genericMethod */

};
extern Il2CppType CharacterValidation_t318_0_0_0;
extern Il2CppType CharacterValidation_t318_0_0_0;
static ParameterInfo ICollection_1_t7791_ICollection_1_Add_m44254_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CharacterValidation_t318_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44254_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::Add(T)
MethodInfo ICollection_1_Add_m44254_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7791_ICollection_1_Add_m44254_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44254_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44255_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::Clear()
MethodInfo ICollection_1_Clear_m44255_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44255_GenericMethod/* genericMethod */

};
extern Il2CppType CharacterValidation_t318_0_0_0;
static ParameterInfo ICollection_1_t7791_ICollection_1_Contains_m44256_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CharacterValidation_t318_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44256_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::Contains(T)
MethodInfo ICollection_1_Contains_m44256_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7791_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7791_ICollection_1_Contains_m44256_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44256_GenericMethod/* genericMethod */

};
extern Il2CppType CharacterValidationU5BU5D_t5600_0_0_0;
extern Il2CppType CharacterValidationU5BU5D_t5600_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7791_ICollection_1_CopyTo_m44257_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CharacterValidationU5BU5D_t5600_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44257_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44257_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7791_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7791_ICollection_1_CopyTo_m44257_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44257_GenericMethod/* genericMethod */

};
extern Il2CppType CharacterValidation_t318_0_0_0;
static ParameterInfo ICollection_1_t7791_ICollection_1_Remove_m44258_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CharacterValidation_t318_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44258_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/CharacterValidation>::Remove(T)
MethodInfo ICollection_1_Remove_m44258_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7791_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7791_ICollection_1_Remove_m44258_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44258_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7791_MethodInfos[] =
{
	&ICollection_1_get_Count_m44252_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44253_MethodInfo,
	&ICollection_1_Add_m44254_MethodInfo,
	&ICollection_1_Clear_m44255_MethodInfo,
	&ICollection_1_Contains_m44256_MethodInfo,
	&ICollection_1_CopyTo_m44257_MethodInfo,
	&ICollection_1_Remove_m44258_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7793_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7791_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7793_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7791_0_0_0;
extern Il2CppType ICollection_1_t7791_1_0_0;
struct ICollection_1_t7791;
extern Il2CppGenericClass ICollection_1_t7791_GenericClass;
TypeInfo ICollection_1_t7791_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7791_MethodInfos/* methods */
	, ICollection_1_t7791_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7791_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7791_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7791_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7791_0_0_0/* byval_arg */
	, &ICollection_1_t7791_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7791_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/CharacterValidation>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/CharacterValidation>
extern Il2CppType IEnumerator_1_t6126_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44259_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/CharacterValidation>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44259_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7793_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6126_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44259_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7793_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44259_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7793_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7793_0_0_0;
extern Il2CppType IEnumerable_1_t7793_1_0_0;
struct IEnumerable_1_t7793;
extern Il2CppGenericClass IEnumerable_1_t7793_GenericClass;
TypeInfo IEnumerable_1_t7793_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7793_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7793_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7793_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7793_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7793_0_0_0/* byval_arg */
	, &IEnumerable_1_t7793_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7793_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7792_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.InputField/CharacterValidation>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/CharacterValidation>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/CharacterValidation>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.InputField/CharacterValidation>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/CharacterValidation>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.InputField/CharacterValidation>
extern MethodInfo IList_1_get_Item_m44260_MethodInfo;
extern MethodInfo IList_1_set_Item_m44261_MethodInfo;
static PropertyInfo IList_1_t7792____Item_PropertyInfo = 
{
	&IList_1_t7792_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44260_MethodInfo/* get */
	, &IList_1_set_Item_m44261_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7792_PropertyInfos[] =
{
	&IList_1_t7792____Item_PropertyInfo,
	NULL
};
extern Il2CppType CharacterValidation_t318_0_0_0;
static ParameterInfo IList_1_t7792_IList_1_IndexOf_m44262_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CharacterValidation_t318_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44262_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.InputField/CharacterValidation>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44262_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7792_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7792_IList_1_IndexOf_m44262_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44262_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CharacterValidation_t318_0_0_0;
static ParameterInfo IList_1_t7792_IList_1_Insert_m44263_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CharacterValidation_t318_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44263_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/CharacterValidation>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44263_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7792_IList_1_Insert_m44263_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44263_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7792_IList_1_RemoveAt_m44264_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44264_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/CharacterValidation>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44264_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7792_IList_1_RemoveAt_m44264_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44264_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7792_IList_1_get_Item_m44260_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType CharacterValidation_t318_0_0_0;
extern void* RuntimeInvoker_CharacterValidation_t318_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44260_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.InputField/CharacterValidation>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44260_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7792_il2cpp_TypeInfo/* declaring_type */
	, &CharacterValidation_t318_0_0_0/* return_type */
	, RuntimeInvoker_CharacterValidation_t318_Int32_t93/* invoker_method */
	, IList_1_t7792_IList_1_get_Item_m44260_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44260_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CharacterValidation_t318_0_0_0;
static ParameterInfo IList_1_t7792_IList_1_set_Item_m44261_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CharacterValidation_t318_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44261_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/CharacterValidation>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44261_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7792_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7792_IList_1_set_Item_m44261_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44261_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7792_MethodInfos[] =
{
	&IList_1_IndexOf_m44262_MethodInfo,
	&IList_1_Insert_m44263_MethodInfo,
	&IList_1_RemoveAt_m44264_MethodInfo,
	&IList_1_get_Item_m44260_MethodInfo,
	&IList_1_set_Item_m44261_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7792_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7791_il2cpp_TypeInfo,
	&IEnumerable_1_t7793_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7792_0_0_0;
extern Il2CppType IList_1_t7792_1_0_0;
struct IList_1_t7792;
extern Il2CppGenericClass IList_1_t7792_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7792_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7792_MethodInfos/* methods */
	, IList_1_t7792_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7792_il2cpp_TypeInfo/* element_class */
	, IList_1_t7792_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7792_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7792_0_0_0/* byval_arg */
	, &IList_1_t7792_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7792_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6127_il2cpp_TypeInfo;

// UnityEngine.UI.InputField/LineType
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/LineType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/LineType>
extern MethodInfo IEnumerator_1_get_Current_m44265_MethodInfo;
static PropertyInfo IEnumerator_1_t6127____Current_PropertyInfo = 
{
	&IEnumerator_1_t6127_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44265_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6127_PropertyInfos[] =
{
	&IEnumerator_1_t6127____Current_PropertyInfo,
	NULL
};
extern Il2CppType LineType_t319_0_0_0;
extern void* RuntimeInvoker_LineType_t319 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44265_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/LineType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44265_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6127_il2cpp_TypeInfo/* declaring_type */
	, &LineType_t319_0_0_0/* return_type */
	, RuntimeInvoker_LineType_t319/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44265_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6127_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44265_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6127_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6127_0_0_0;
extern Il2CppType IEnumerator_1_t6127_1_0_0;
struct IEnumerator_1_t6127;
extern Il2CppGenericClass IEnumerator_1_t6127_GenericClass;
TypeInfo IEnumerator_1_t6127_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6127_MethodInfos/* methods */
	, IEnumerator_1_t6127_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6127_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6127_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6127_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6127_0_0_0/* byval_arg */
	, &IEnumerator_1_t6127_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6127_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_219.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3438_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_219MethodDeclarations.h"

extern TypeInfo LineType_t319_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18879_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisLineType_t319_m34267_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/LineType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/LineType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisLineType_t319_m34267 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18875_MethodInfo;
 void InternalEnumerator_1__ctor_m18875 (InternalEnumerator_1_t3438 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18876_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18876 (InternalEnumerator_1_t3438 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m18879(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18879_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&LineType_t319_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18877_MethodInfo;
 void InternalEnumerator_1_Dispose_m18877 (InternalEnumerator_1_t3438 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18878_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18878 (InternalEnumerator_1_t3438 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18879 (InternalEnumerator_1_t3438 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisLineType_t319_m34267(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisLineType_t319_m34267_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3438____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3438_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3438, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3438____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3438_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3438, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3438_FieldInfos[] =
{
	&InternalEnumerator_1_t3438____array_0_FieldInfo,
	&InternalEnumerator_1_t3438____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3438____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3438_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18876_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3438____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3438_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18879_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3438_PropertyInfos[] =
{
	&InternalEnumerator_1_t3438____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3438____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3438_InternalEnumerator_1__ctor_m18875_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18875_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18875_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18875/* method */
	, &InternalEnumerator_1_t3438_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3438_InternalEnumerator_1__ctor_m18875_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18875_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18876_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18876_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18876/* method */
	, &InternalEnumerator_1_t3438_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18876_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18877_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18877_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18877/* method */
	, &InternalEnumerator_1_t3438_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18877_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18878_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18878_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18878/* method */
	, &InternalEnumerator_1_t3438_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18878_GenericMethod/* genericMethod */

};
extern Il2CppType LineType_t319_0_0_0;
extern void* RuntimeInvoker_LineType_t319 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18879_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/LineType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18879_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18879/* method */
	, &InternalEnumerator_1_t3438_il2cpp_TypeInfo/* declaring_type */
	, &LineType_t319_0_0_0/* return_type */
	, RuntimeInvoker_LineType_t319/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18879_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3438_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18875_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18876_MethodInfo,
	&InternalEnumerator_1_Dispose_m18877_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18878_MethodInfo,
	&InternalEnumerator_1_get_Current_m18879_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3438_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18876_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18878_MethodInfo,
	&InternalEnumerator_1_Dispose_m18877_MethodInfo,
	&InternalEnumerator_1_get_Current_m18879_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3438_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6127_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3438_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6127_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3438_0_0_0;
extern Il2CppType InternalEnumerator_1_t3438_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3438_GenericClass;
TypeInfo InternalEnumerator_1_t3438_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3438_MethodInfos/* methods */
	, InternalEnumerator_1_t3438_PropertyInfos/* properties */
	, InternalEnumerator_1_t3438_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3438_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3438_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3438_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3438_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3438_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3438_1_0_0/* this_arg */
	, InternalEnumerator_1_t3438_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3438_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3438)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7794_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>
extern MethodInfo ICollection_1_get_Count_m44266_MethodInfo;
static PropertyInfo ICollection_1_t7794____Count_PropertyInfo = 
{
	&ICollection_1_t7794_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44266_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44267_MethodInfo;
static PropertyInfo ICollection_1_t7794____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7794_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44267_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7794_PropertyInfos[] =
{
	&ICollection_1_t7794____Count_PropertyInfo,
	&ICollection_1_t7794____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44266_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::get_Count()
MethodInfo ICollection_1_get_Count_m44266_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7794_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44266_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44267_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44267_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7794_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44267_GenericMethod/* genericMethod */

};
extern Il2CppType LineType_t319_0_0_0;
extern Il2CppType LineType_t319_0_0_0;
static ParameterInfo ICollection_1_t7794_ICollection_1_Add_m44268_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &LineType_t319_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44268_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::Add(T)
MethodInfo ICollection_1_Add_m44268_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7794_ICollection_1_Add_m44268_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44268_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44269_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::Clear()
MethodInfo ICollection_1_Clear_m44269_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44269_GenericMethod/* genericMethod */

};
extern Il2CppType LineType_t319_0_0_0;
static ParameterInfo ICollection_1_t7794_ICollection_1_Contains_m44270_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &LineType_t319_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44270_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::Contains(T)
MethodInfo ICollection_1_Contains_m44270_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7794_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7794_ICollection_1_Contains_m44270_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44270_GenericMethod/* genericMethod */

};
extern Il2CppType LineTypeU5BU5D_t5601_0_0_0;
extern Il2CppType LineTypeU5BU5D_t5601_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7794_ICollection_1_CopyTo_m44271_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &LineTypeU5BU5D_t5601_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44271_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44271_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7794_ICollection_1_CopyTo_m44271_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44271_GenericMethod/* genericMethod */

};
extern Il2CppType LineType_t319_0_0_0;
static ParameterInfo ICollection_1_t7794_ICollection_1_Remove_m44272_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &LineType_t319_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44272_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/LineType>::Remove(T)
MethodInfo ICollection_1_Remove_m44272_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7794_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7794_ICollection_1_Remove_m44272_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44272_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7794_MethodInfos[] =
{
	&ICollection_1_get_Count_m44266_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44267_MethodInfo,
	&ICollection_1_Add_m44268_MethodInfo,
	&ICollection_1_Clear_m44269_MethodInfo,
	&ICollection_1_Contains_m44270_MethodInfo,
	&ICollection_1_CopyTo_m44271_MethodInfo,
	&ICollection_1_Remove_m44272_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7796_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7794_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7796_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7794_0_0_0;
extern Il2CppType ICollection_1_t7794_1_0_0;
struct ICollection_1_t7794;
extern Il2CppGenericClass ICollection_1_t7794_GenericClass;
TypeInfo ICollection_1_t7794_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7794_MethodInfos/* methods */
	, ICollection_1_t7794_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7794_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7794_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7794_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7794_0_0_0/* byval_arg */
	, &ICollection_1_t7794_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7794_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/LineType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/LineType>
extern Il2CppType IEnumerator_1_t6127_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44273_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/LineType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44273_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7796_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6127_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44273_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7796_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44273_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7796_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7796_0_0_0;
extern Il2CppType IEnumerable_1_t7796_1_0_0;
struct IEnumerable_1_t7796;
extern Il2CppGenericClass IEnumerable_1_t7796_GenericClass;
TypeInfo IEnumerable_1_t7796_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7796_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7796_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7796_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7796_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7796_0_0_0/* byval_arg */
	, &IEnumerable_1_t7796_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7796_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7795_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.InputField/LineType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/LineType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/LineType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.InputField/LineType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/LineType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.InputField/LineType>
extern MethodInfo IList_1_get_Item_m44274_MethodInfo;
extern MethodInfo IList_1_set_Item_m44275_MethodInfo;
static PropertyInfo IList_1_t7795____Item_PropertyInfo = 
{
	&IList_1_t7795_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44274_MethodInfo/* get */
	, &IList_1_set_Item_m44275_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7795_PropertyInfos[] =
{
	&IList_1_t7795____Item_PropertyInfo,
	NULL
};
extern Il2CppType LineType_t319_0_0_0;
static ParameterInfo IList_1_t7795_IList_1_IndexOf_m44276_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &LineType_t319_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44276_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.InputField/LineType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44276_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7795_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7795_IList_1_IndexOf_m44276_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44276_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType LineType_t319_0_0_0;
static ParameterInfo IList_1_t7795_IList_1_Insert_m44277_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &LineType_t319_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44277_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/LineType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44277_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7795_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7795_IList_1_Insert_m44277_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44277_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7795_IList_1_RemoveAt_m44278_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44278_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/LineType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44278_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7795_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7795_IList_1_RemoveAt_m44278_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44278_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7795_IList_1_get_Item_m44274_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType LineType_t319_0_0_0;
extern void* RuntimeInvoker_LineType_t319_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44274_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.InputField/LineType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44274_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7795_il2cpp_TypeInfo/* declaring_type */
	, &LineType_t319_0_0_0/* return_type */
	, RuntimeInvoker_LineType_t319_Int32_t93/* invoker_method */
	, IList_1_t7795_IList_1_get_Item_m44274_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44274_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType LineType_t319_0_0_0;
static ParameterInfo IList_1_t7795_IList_1_set_Item_m44275_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &LineType_t319_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44275_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/LineType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44275_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7795_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7795_IList_1_set_Item_m44275_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44275_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7795_MethodInfos[] =
{
	&IList_1_IndexOf_m44276_MethodInfo,
	&IList_1_Insert_m44277_MethodInfo,
	&IList_1_RemoveAt_m44278_MethodInfo,
	&IList_1_get_Item_m44274_MethodInfo,
	&IList_1_set_Item_m44275_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7795_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7794_il2cpp_TypeInfo,
	&IEnumerable_1_t7796_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7795_0_0_0;
extern Il2CppType IList_1_t7795_1_0_0;
struct IList_1_t7795;
extern Il2CppGenericClass IList_1_t7795_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7795_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7795_MethodInfos/* methods */
	, IList_1_t7795_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7795_il2cpp_TypeInfo/* element_class */
	, IList_1_t7795_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7795_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7795_0_0_0/* byval_arg */
	, &IList_1_t7795_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7795_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6129_il2cpp_TypeInfo;

// UnityEngine.UI.InputField/EditState
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/EditState>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/EditState>
extern MethodInfo IEnumerator_1_get_Current_m44279_MethodInfo;
static PropertyInfo IEnumerator_1_t6129____Current_PropertyInfo = 
{
	&IEnumerator_1_t6129_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44279_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6129_PropertyInfos[] =
{
	&IEnumerator_1_t6129____Current_PropertyInfo,
	NULL
};
extern Il2CppType EditState_t323_0_0_0;
extern void* RuntimeInvoker_EditState_t323 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44279_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.InputField/EditState>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44279_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6129_il2cpp_TypeInfo/* declaring_type */
	, &EditState_t323_0_0_0/* return_type */
	, RuntimeInvoker_EditState_t323/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44279_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6129_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44279_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6129_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6129_0_0_0;
extern Il2CppType IEnumerator_1_t6129_1_0_0;
struct IEnumerator_1_t6129;
extern Il2CppGenericClass IEnumerator_1_t6129_GenericClass;
TypeInfo IEnumerator_1_t6129_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6129_MethodInfos/* methods */
	, IEnumerator_1_t6129_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6129_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6129_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6129_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6129_0_0_0/* byval_arg */
	, &IEnumerator_1_t6129_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6129_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_220.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3439_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_220MethodDeclarations.h"

extern TypeInfo EditState_t323_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18884_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEditState_t323_m34278_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/EditState>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/EditState>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisEditState_t323_m34278 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18880_MethodInfo;
 void InternalEnumerator_1__ctor_m18880 (InternalEnumerator_1_t3439 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18881_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18881 (InternalEnumerator_1_t3439 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m18884(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18884_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&EditState_t323_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18882_MethodInfo;
 void InternalEnumerator_1_Dispose_m18882 (InternalEnumerator_1_t3439 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18883_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18883 (InternalEnumerator_1_t3439 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18884 (InternalEnumerator_1_t3439 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisEditState_t323_m34278(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisEditState_t323_m34278_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3439____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3439_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3439, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3439____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3439_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3439, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3439_FieldInfos[] =
{
	&InternalEnumerator_1_t3439____array_0_FieldInfo,
	&InternalEnumerator_1_t3439____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3439____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3439_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18881_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3439____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3439_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18884_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3439_PropertyInfos[] =
{
	&InternalEnumerator_1_t3439____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3439____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3439_InternalEnumerator_1__ctor_m18880_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18880_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18880_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18880/* method */
	, &InternalEnumerator_1_t3439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3439_InternalEnumerator_1__ctor_m18880_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18880_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18881_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18881_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18881/* method */
	, &InternalEnumerator_1_t3439_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18881_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18882_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18882_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18882/* method */
	, &InternalEnumerator_1_t3439_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18882_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18883_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18883_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18883/* method */
	, &InternalEnumerator_1_t3439_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18883_GenericMethod/* genericMethod */

};
extern Il2CppType EditState_t323_0_0_0;
extern void* RuntimeInvoker_EditState_t323 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18884_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/EditState>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18884_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18884/* method */
	, &InternalEnumerator_1_t3439_il2cpp_TypeInfo/* declaring_type */
	, &EditState_t323_0_0_0/* return_type */
	, RuntimeInvoker_EditState_t323/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18884_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3439_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18880_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18881_MethodInfo,
	&InternalEnumerator_1_Dispose_m18882_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18883_MethodInfo,
	&InternalEnumerator_1_get_Current_m18884_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3439_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18881_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18883_MethodInfo,
	&InternalEnumerator_1_Dispose_m18882_MethodInfo,
	&InternalEnumerator_1_get_Current_m18884_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3439_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6129_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3439_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6129_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3439_0_0_0;
extern Il2CppType InternalEnumerator_1_t3439_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3439_GenericClass;
TypeInfo InternalEnumerator_1_t3439_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3439_MethodInfos/* methods */
	, InternalEnumerator_1_t3439_PropertyInfos/* properties */
	, InternalEnumerator_1_t3439_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3439_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3439_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3439_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3439_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3439_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3439_1_0_0/* this_arg */
	, InternalEnumerator_1_t3439_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3439_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3439)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7797_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>
extern MethodInfo ICollection_1_get_Count_m44280_MethodInfo;
static PropertyInfo ICollection_1_t7797____Count_PropertyInfo = 
{
	&ICollection_1_t7797_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44280_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44281_MethodInfo;
static PropertyInfo ICollection_1_t7797____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7797_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44281_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7797_PropertyInfos[] =
{
	&ICollection_1_t7797____Count_PropertyInfo,
	&ICollection_1_t7797____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44280_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::get_Count()
MethodInfo ICollection_1_get_Count_m44280_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7797_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44280_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44281_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44281_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7797_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44281_GenericMethod/* genericMethod */

};
extern Il2CppType EditState_t323_0_0_0;
extern Il2CppType EditState_t323_0_0_0;
static ParameterInfo ICollection_1_t7797_ICollection_1_Add_m44282_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditState_t323_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44282_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::Add(T)
MethodInfo ICollection_1_Add_m44282_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7797_ICollection_1_Add_m44282_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44282_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44283_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::Clear()
MethodInfo ICollection_1_Clear_m44283_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44283_GenericMethod/* genericMethod */

};
extern Il2CppType EditState_t323_0_0_0;
static ParameterInfo ICollection_1_t7797_ICollection_1_Contains_m44284_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditState_t323_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44284_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::Contains(T)
MethodInfo ICollection_1_Contains_m44284_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7797_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7797_ICollection_1_Contains_m44284_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44284_GenericMethod/* genericMethod */

};
extern Il2CppType EditStateU5BU5D_t5602_0_0_0;
extern Il2CppType EditStateU5BU5D_t5602_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7797_ICollection_1_CopyTo_m44285_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EditStateU5BU5D_t5602_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44285_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44285_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7797_ICollection_1_CopyTo_m44285_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44285_GenericMethod/* genericMethod */

};
extern Il2CppType EditState_t323_0_0_0;
static ParameterInfo ICollection_1_t7797_ICollection_1_Remove_m44286_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditState_t323_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44286_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.InputField/EditState>::Remove(T)
MethodInfo ICollection_1_Remove_m44286_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7797_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7797_ICollection_1_Remove_m44286_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44286_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7797_MethodInfos[] =
{
	&ICollection_1_get_Count_m44280_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44281_MethodInfo,
	&ICollection_1_Add_m44282_MethodInfo,
	&ICollection_1_Clear_m44283_MethodInfo,
	&ICollection_1_Contains_m44284_MethodInfo,
	&ICollection_1_CopyTo_m44285_MethodInfo,
	&ICollection_1_Remove_m44286_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7799_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7797_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7799_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7797_0_0_0;
extern Il2CppType ICollection_1_t7797_1_0_0;
struct ICollection_1_t7797;
extern Il2CppGenericClass ICollection_1_t7797_GenericClass;
TypeInfo ICollection_1_t7797_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7797_MethodInfos/* methods */
	, ICollection_1_t7797_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7797_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7797_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7797_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7797_0_0_0/* byval_arg */
	, &ICollection_1_t7797_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7797_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/EditState>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/EditState>
extern Il2CppType IEnumerator_1_t6129_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44287_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.InputField/EditState>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44287_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7799_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6129_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44287_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7799_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44287_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7799_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7799_0_0_0;
extern Il2CppType IEnumerable_1_t7799_1_0_0;
struct IEnumerable_1_t7799;
extern Il2CppGenericClass IEnumerable_1_t7799_GenericClass;
TypeInfo IEnumerable_1_t7799_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7799_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7799_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7799_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7799_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7799_0_0_0/* byval_arg */
	, &IEnumerable_1_t7799_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7799_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7798_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.InputField/EditState>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/EditState>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/EditState>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.InputField/EditState>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/EditState>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.InputField/EditState>
extern MethodInfo IList_1_get_Item_m44288_MethodInfo;
extern MethodInfo IList_1_set_Item_m44289_MethodInfo;
static PropertyInfo IList_1_t7798____Item_PropertyInfo = 
{
	&IList_1_t7798_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44288_MethodInfo/* get */
	, &IList_1_set_Item_m44289_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7798_PropertyInfos[] =
{
	&IList_1_t7798____Item_PropertyInfo,
	NULL
};
extern Il2CppType EditState_t323_0_0_0;
static ParameterInfo IList_1_t7798_IList_1_IndexOf_m44290_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditState_t323_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44290_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.InputField/EditState>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44290_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7798_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7798_IList_1_IndexOf_m44290_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44290_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType EditState_t323_0_0_0;
static ParameterInfo IList_1_t7798_IList_1_Insert_m44291_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &EditState_t323_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44291_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/EditState>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44291_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7798_IList_1_Insert_m44291_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44291_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7798_IList_1_RemoveAt_m44292_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44292_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/EditState>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44292_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7798_IList_1_RemoveAt_m44292_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44292_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7798_IList_1_get_Item_m44288_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType EditState_t323_0_0_0;
extern void* RuntimeInvoker_EditState_t323_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44288_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.InputField/EditState>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44288_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7798_il2cpp_TypeInfo/* declaring_type */
	, &EditState_t323_0_0_0/* return_type */
	, RuntimeInvoker_EditState_t323_Int32_t93/* invoker_method */
	, IList_1_t7798_IList_1_get_Item_m44288_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44288_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType EditState_t323_0_0_0;
static ParameterInfo IList_1_t7798_IList_1_set_Item_m44289_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &EditState_t323_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44289_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.InputField/EditState>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44289_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7798_IList_1_set_Item_m44289_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44289_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7798_MethodInfos[] =
{
	&IList_1_IndexOf_m44290_MethodInfo,
	&IList_1_Insert_m44291_MethodInfo,
	&IList_1_RemoveAt_m44292_MethodInfo,
	&IList_1_get_Item_m44288_MethodInfo,
	&IList_1_set_Item_m44289_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7798_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7797_il2cpp_TypeInfo,
	&IEnumerable_1_t7799_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7798_0_0_0;
extern Il2CppType IList_1_t7798_1_0_0;
struct IList_1_t7798;
extern Il2CppGenericClass IList_1_t7798_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7798_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7798_MethodInfos/* methods */
	, IList_1_t7798_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7798_il2cpp_TypeInfo/* element_class */
	, IList_1_t7798_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7798_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7798_0_0_0/* byval_arg */
	, &IList_1_t7798_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7798_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.MaskableGraphic>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_63.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3440_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.MaskableGraphic>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_63MethodDeclarations.h"

// UnityEngine.UI.MaskableGraphic
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_62.h"
extern TypeInfo MaskableGraphic_t313_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t3441_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_62MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m18887_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m18889_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.MaskableGraphic>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.MaskableGraphic>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.MaskableGraphic>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t3440____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t3440_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3440, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3440_FieldInfos[] =
{
	&CachedInvokableCall_1_t3440____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MaskableGraphic_t313_0_0_0;
extern Il2CppType MaskableGraphic_t313_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3440_CachedInvokableCall_1__ctor_m18885_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &MaskableGraphic_t313_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m18885_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.MaskableGraphic>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m18885_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t3440_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3440_CachedInvokableCall_1__ctor_m18885_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m18885_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3440_CachedInvokableCall_1_Invoke_m18886_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m18886_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.MaskableGraphic>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m18886_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t3440_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3440_CachedInvokableCall_1_Invoke_m18886_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m18886_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3440_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m18885_MethodInfo,
	&CachedInvokableCall_1_Invoke_m18886_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m18886_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m18890_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3440_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m18886_MethodInfo,
	&InvokableCall_1_Find_m18890_MethodInfo,
};
extern Il2CppType UnityAction_1_t3442_0_0_0;
extern TypeInfo UnityAction_1_t3442_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisMaskableGraphic_t313_m34288_MethodInfo;
extern TypeInfo MaskableGraphic_t313_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m18892_MethodInfo;
extern TypeInfo MaskableGraphic_t313_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3440_RGCTXData[8] = 
{
	&UnityAction_1_t3442_0_0_0/* Type Usage */,
	&UnityAction_1_t3442_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMaskableGraphic_t313_m34288_MethodInfo/* Method Usage */,
	&MaskableGraphic_t313_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m18892_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m18887_MethodInfo/* Method Usage */,
	&MaskableGraphic_t313_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m18889_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3440_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3440_1_0_0;
struct CachedInvokableCall_1_t3440;
extern Il2CppGenericClass CachedInvokableCall_1_t3440_GenericClass;
TypeInfo CachedInvokableCall_1_t3440_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3440_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3440_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3441_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3440_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3440_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3440_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3440_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3440_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3440_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3440_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3440)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.UI.MaskableGraphic>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_68.h"
extern TypeInfo UnityAction_1_t3442_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.MaskableGraphic>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_68MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.MaskableGraphic>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.MaskableGraphic>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisMaskableGraphic_t313_m34288(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>
extern Il2CppType UnityAction_1_t3442_0_0_1;
FieldInfo InvokableCall_1_t3441____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3442_0_0_1/* type */
	, &InvokableCall_1_t3441_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3441, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3441_FieldInfos[] =
{
	&InvokableCall_1_t3441____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3441_InvokableCall_1__ctor_m18887_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m18887_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m18887_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t3441_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3441_InvokableCall_1__ctor_m18887_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m18887_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3442_0_0_0;
static ParameterInfo InvokableCall_1_t3441_InvokableCall_1__ctor_m18888_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3442_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m18888_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m18888_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t3441_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3441_InvokableCall_1__ctor_m18888_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m18888_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t3441_InvokableCall_1_Invoke_m18889_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m18889_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m18889_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t3441_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3441_InvokableCall_1_Invoke_m18889_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m18889_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3441_InvokableCall_1_Find_m18890_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m18890_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m18890_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t3441_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3441_InvokableCall_1_Find_m18890_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m18890_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3441_MethodInfos[] =
{
	&InvokableCall_1__ctor_m18887_MethodInfo,
	&InvokableCall_1__ctor_m18888_MethodInfo,
	&InvokableCall_1_Invoke_m18889_MethodInfo,
	&InvokableCall_1_Find_m18890_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3441_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m18889_MethodInfo,
	&InvokableCall_1_Find_m18890_MethodInfo,
};
extern TypeInfo UnityAction_1_t3442_il2cpp_TypeInfo;
extern TypeInfo MaskableGraphic_t313_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3441_RGCTXData[5] = 
{
	&UnityAction_1_t3442_0_0_0/* Type Usage */,
	&UnityAction_1_t3442_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisMaskableGraphic_t313_m34288_MethodInfo/* Method Usage */,
	&MaskableGraphic_t313_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m18892_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3441_0_0_0;
extern Il2CppType InvokableCall_1_t3441_1_0_0;
struct InvokableCall_1_t3441;
extern Il2CppGenericClass InvokableCall_1_t3441_GenericClass;
TypeInfo InvokableCall_1_t3441_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3441_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3441_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3441_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3441_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3441_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3441_0_0_0/* byval_arg */
	, &InvokableCall_1_t3441_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3441_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3441_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3441)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.MaskableGraphic>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.MaskableGraphic>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.MaskableGraphic>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.MaskableGraphic>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.MaskableGraphic>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3442_UnityAction_1__ctor_m18891_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m18891_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.MaskableGraphic>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m18891_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t3442_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3442_UnityAction_1__ctor_m18891_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m18891_GenericMethod/* genericMethod */

};
extern Il2CppType MaskableGraphic_t313_0_0_0;
static ParameterInfo UnityAction_1_t3442_UnityAction_1_Invoke_m18892_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MaskableGraphic_t313_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m18892_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.MaskableGraphic>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m18892_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t3442_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3442_UnityAction_1_Invoke_m18892_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m18892_GenericMethod/* genericMethod */

};
extern Il2CppType MaskableGraphic_t313_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3442_UnityAction_1_BeginInvoke_m18893_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &MaskableGraphic_t313_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m18893_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.MaskableGraphic>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m18893_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t3442_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3442_UnityAction_1_BeginInvoke_m18893_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m18893_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t3442_UnityAction_1_EndInvoke_m18894_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m18894_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.MaskableGraphic>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m18894_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t3442_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3442_UnityAction_1_EndInvoke_m18894_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m18894_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3442_MethodInfos[] =
{
	&UnityAction_1__ctor_m18891_MethodInfo,
	&UnityAction_1_Invoke_m18892_MethodInfo,
	&UnityAction_1_BeginInvoke_m18893_MethodInfo,
	&UnityAction_1_EndInvoke_m18894_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m18893_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m18894_MethodInfo;
static MethodInfo* UnityAction_1_t3442_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m18892_MethodInfo,
	&UnityAction_1_BeginInvoke_m18893_MethodInfo,
	&UnityAction_1_EndInvoke_m18894_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3442_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3442_1_0_0;
struct UnityAction_1_t3442;
extern Il2CppGenericClass UnityAction_1_t3442_GenericClass;
TypeInfo UnityAction_1_t3442_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3442_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3442_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3442_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3442_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3442_0_0_0/* byval_arg */
	, &UnityAction_1_t3442_1_0_0/* this_arg */
	, UnityAction_1_t3442_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3442_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3442)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6131_il2cpp_TypeInfo;

// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Navigation/Mode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Navigation/Mode>
extern MethodInfo IEnumerator_1_get_Current_m44293_MethodInfo;
static PropertyInfo IEnumerator_1_t6131____Current_PropertyInfo = 
{
	&IEnumerator_1_t6131_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44293_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6131_PropertyInfos[] =
{
	&IEnumerator_1_t6131____Current_PropertyInfo,
	NULL
};
extern Il2CppType Mode_t336_0_0_0;
extern void* RuntimeInvoker_Mode_t336 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44293_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Navigation/Mode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44293_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6131_il2cpp_TypeInfo/* declaring_type */
	, &Mode_t336_0_0_0/* return_type */
	, RuntimeInvoker_Mode_t336/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44293_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6131_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44293_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6131_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6131_0_0_0;
extern Il2CppType IEnumerator_1_t6131_1_0_0;
struct IEnumerator_1_t6131;
extern Il2CppGenericClass IEnumerator_1_t6131_GenericClass;
TypeInfo IEnumerator_1_t6131_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6131_MethodInfos/* methods */
	, IEnumerator_1_t6131_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6131_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6131_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6131_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6131_0_0_0/* byval_arg */
	, &IEnumerator_1_t6131_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6131_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_221.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3443_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_221MethodDeclarations.h"

extern TypeInfo Mode_t336_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18899_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMode_t336_m34290_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Navigation/Mode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Navigation/Mode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisMode_t336_m34290 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m18895_MethodInfo;
 void InternalEnumerator_1__ctor_m18895 (InternalEnumerator_1_t3443 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18896_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18896 (InternalEnumerator_1_t3443 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m18899(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m18899_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&Mode_t336_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m18897_MethodInfo;
 void InternalEnumerator_1_Dispose_m18897 (InternalEnumerator_1_t3443 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m18898_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m18898 (InternalEnumerator_1_t3443 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18899 (InternalEnumerator_1_t3443 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisMode_t336_m34290(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisMode_t336_m34290_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3443____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3443_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3443, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3443____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3443_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3443, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3443_FieldInfos[] =
{
	&InternalEnumerator_1_t3443____array_0_FieldInfo,
	&InternalEnumerator_1_t3443____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t3443____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3443_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18896_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3443____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3443_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18899_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3443_PropertyInfos[] =
{
	&InternalEnumerator_1_t3443____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3443____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3443_InternalEnumerator_1__ctor_m18895_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18895_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18895_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m18895/* method */
	, &InternalEnumerator_1_t3443_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3443_InternalEnumerator_1__ctor_m18895_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18895_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18896_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18896_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18896/* method */
	, &InternalEnumerator_1_t3443_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18896_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18897_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18897_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m18897/* method */
	, &InternalEnumerator_1_t3443_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18897_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18898_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18898_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m18898/* method */
	, &InternalEnumerator_1_t3443_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18898_GenericMethod/* genericMethod */

};
extern Il2CppType Mode_t336_0_0_0;
extern void* RuntimeInvoker_Mode_t336 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18899_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Navigation/Mode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18899_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m18899/* method */
	, &InternalEnumerator_1_t3443_il2cpp_TypeInfo/* declaring_type */
	, &Mode_t336_0_0_0/* return_type */
	, RuntimeInvoker_Mode_t336/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18899_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3443_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18895_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18896_MethodInfo,
	&InternalEnumerator_1_Dispose_m18897_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18898_MethodInfo,
	&InternalEnumerator_1_get_Current_m18899_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t3443_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18896_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18898_MethodInfo,
	&InternalEnumerator_1_Dispose_m18897_MethodInfo,
	&InternalEnumerator_1_get_Current_m18899_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3443_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6131_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3443_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6131_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3443_0_0_0;
extern Il2CppType InternalEnumerator_1_t3443_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3443_GenericClass;
TypeInfo InternalEnumerator_1_t3443_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3443_MethodInfos/* methods */
	, InternalEnumerator_1_t3443_PropertyInfos/* properties */
	, InternalEnumerator_1_t3443_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3443_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3443_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3443_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3443_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3443_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3443_1_0_0/* this_arg */
	, InternalEnumerator_1_t3443_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3443_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3443)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7800_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>
extern MethodInfo ICollection_1_get_Count_m44294_MethodInfo;
static PropertyInfo ICollection_1_t7800____Count_PropertyInfo = 
{
	&ICollection_1_t7800_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44294_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44295_MethodInfo;
static PropertyInfo ICollection_1_t7800____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7800_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44295_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7800_PropertyInfos[] =
{
	&ICollection_1_t7800____Count_PropertyInfo,
	&ICollection_1_t7800____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44294_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::get_Count()
MethodInfo ICollection_1_get_Count_m44294_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7800_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44294_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44295_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44295_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7800_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44295_GenericMethod/* genericMethod */

};
extern Il2CppType Mode_t336_0_0_0;
extern Il2CppType Mode_t336_0_0_0;
static ParameterInfo ICollection_1_t7800_ICollection_1_Add_m44296_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Mode_t336_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44296_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::Add(T)
MethodInfo ICollection_1_Add_m44296_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t7800_ICollection_1_Add_m44296_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44296_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44297_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::Clear()
MethodInfo ICollection_1_Clear_m44297_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44297_GenericMethod/* genericMethod */

};
extern Il2CppType Mode_t336_0_0_0;
static ParameterInfo ICollection_1_t7800_ICollection_1_Contains_m44298_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Mode_t336_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44298_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::Contains(T)
MethodInfo ICollection_1_Contains_m44298_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7800_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7800_ICollection_1_Contains_m44298_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44298_GenericMethod/* genericMethod */

};
extern Il2CppType ModeU5BU5D_t5603_0_0_0;
extern Il2CppType ModeU5BU5D_t5603_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7800_ICollection_1_CopyTo_m44299_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ModeU5BU5D_t5603_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44299_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44299_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7800_ICollection_1_CopyTo_m44299_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44299_GenericMethod/* genericMethod */

};
extern Il2CppType Mode_t336_0_0_0;
static ParameterInfo ICollection_1_t7800_ICollection_1_Remove_m44300_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Mode_t336_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44300_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.Navigation/Mode>::Remove(T)
MethodInfo ICollection_1_Remove_m44300_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7800_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t7800_ICollection_1_Remove_m44300_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44300_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7800_MethodInfos[] =
{
	&ICollection_1_get_Count_m44294_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44295_MethodInfo,
	&ICollection_1_Add_m44296_MethodInfo,
	&ICollection_1_Clear_m44297_MethodInfo,
	&ICollection_1_Contains_m44298_MethodInfo,
	&ICollection_1_CopyTo_m44299_MethodInfo,
	&ICollection_1_Remove_m44300_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7802_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7800_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7802_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7800_0_0_0;
extern Il2CppType ICollection_1_t7800_1_0_0;
struct ICollection_1_t7800;
extern Il2CppGenericClass ICollection_1_t7800_GenericClass;
TypeInfo ICollection_1_t7800_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7800_MethodInfos/* methods */
	, ICollection_1_t7800_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7800_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7800_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7800_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7800_0_0_0/* byval_arg */
	, &ICollection_1_t7800_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7800_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Navigation/Mode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Navigation/Mode>
extern Il2CppType IEnumerator_1_t6131_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44301_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Navigation/Mode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44301_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7802_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6131_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44301_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7802_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44301_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7802_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7802_0_0_0;
extern Il2CppType IEnumerable_1_t7802_1_0_0;
struct IEnumerable_1_t7802;
extern Il2CppGenericClass IEnumerable_1_t7802_GenericClass;
TypeInfo IEnumerable_1_t7802_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7802_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7802_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7802_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7802_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7802_0_0_0/* byval_arg */
	, &IEnumerable_1_t7802_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7802_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7801_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Navigation/Mode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Navigation/Mode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Navigation/Mode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.Navigation/Mode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Navigation/Mode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.Navigation/Mode>
extern MethodInfo IList_1_get_Item_m44302_MethodInfo;
extern MethodInfo IList_1_set_Item_m44303_MethodInfo;
static PropertyInfo IList_1_t7801____Item_PropertyInfo = 
{
	&IList_1_t7801_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44302_MethodInfo/* get */
	, &IList_1_set_Item_m44303_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7801_PropertyInfos[] =
{
	&IList_1_t7801____Item_PropertyInfo,
	NULL
};
extern Il2CppType Mode_t336_0_0_0;
static ParameterInfo IList_1_t7801_IList_1_IndexOf_m44304_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Mode_t336_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44304_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.Navigation/Mode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44304_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7801_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7801_IList_1_IndexOf_m44304_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44304_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Mode_t336_0_0_0;
static ParameterInfo IList_1_t7801_IList_1_Insert_m44305_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Mode_t336_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44305_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Navigation/Mode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44305_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7801_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7801_IList_1_Insert_m44305_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44305_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7801_IList_1_RemoveAt_m44306_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44306_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Navigation/Mode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44306_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7801_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7801_IList_1_RemoveAt_m44306_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44306_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7801_IList_1_get_Item_m44302_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Mode_t336_0_0_0;
extern void* RuntimeInvoker_Mode_t336_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44302_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.Navigation/Mode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44302_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7801_il2cpp_TypeInfo/* declaring_type */
	, &Mode_t336_0_0_0/* return_type */
	, RuntimeInvoker_Mode_t336_Int32_t93/* invoker_method */
	, IList_1_t7801_IList_1_get_Item_m44302_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44302_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Mode_t336_0_0_0;
static ParameterInfo IList_1_t7801_IList_1_set_Item_m44303_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Mode_t336_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44303_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.Navigation/Mode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44303_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7801_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t7801_IList_1_set_Item_m44303_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44303_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7801_MethodInfos[] =
{
	&IList_1_IndexOf_m44304_MethodInfo,
	&IList_1_Insert_m44305_MethodInfo,
	&IList_1_RemoveAt_m44306_MethodInfo,
	&IList_1_get_Item_m44302_MethodInfo,
	&IList_1_set_Item_m44303_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7801_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7800_il2cpp_TypeInfo,
	&IEnumerable_1_t7802_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7801_0_0_0;
extern Il2CppType IList_1_t7801_1_0_0;
struct IList_1_t7801;
extern Il2CppGenericClass IList_1_t7801_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7801_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7801_MethodInfos/* methods */
	, IList_1_t7801_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7801_il2cpp_TypeInfo/* element_class */
	, IList_1_t7801_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7801_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7801_0_0_0/* byval_arg */
	, &IList_1_t7801_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7801_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6133_il2cpp_TypeInfo;

// UnityEngine.UI.RawImage
#include "UnityEngine_UI_UnityEngine_UI_RawImage.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.RawImage>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.RawImage>
extern MethodInfo IEnumerator_1_get_Current_m44307_MethodInfo;
static PropertyInfo IEnumerator_1_t6133____Current_PropertyInfo = 
{
	&IEnumerator_1_t6133_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44307_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6133_PropertyInfos[] =
{
	&IEnumerator_1_t6133____Current_PropertyInfo,
	NULL
};
extern Il2CppType RawImage_t338_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44307_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.RawImage>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44307_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6133_il2cpp_TypeInfo/* declaring_type */
	, &RawImage_t338_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44307_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6133_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44307_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6133_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6133_0_0_0;
extern Il2CppType IEnumerator_1_t6133_1_0_0;
struct IEnumerator_1_t6133;
extern Il2CppGenericClass IEnumerator_1_t6133_GenericClass;
TypeInfo IEnumerator_1_t6133_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6133_MethodInfos/* methods */
	, IEnumerator_1_t6133_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6133_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6133_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6133_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6133_0_0_0/* byval_arg */
	, &IEnumerator_1_t6133_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6133_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_222.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3444_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_222MethodDeclarations.h"

extern TypeInfo RawImage_t338_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18904_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisRawImage_t338_m34301_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.RawImage>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.RawImage>(System.Int32)
#define Array_InternalArray__get_Item_TisRawImage_t338_m34301(__this, p0, method) (RawImage_t338 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3444____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3444_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3444, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3444____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3444_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3444, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3444_FieldInfos[] =
{
	&InternalEnumerator_1_t3444____array_0_FieldInfo,
	&InternalEnumerator_1_t3444____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18901_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3444____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3444_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18901_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3444____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3444_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18904_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3444_PropertyInfos[] =
{
	&InternalEnumerator_1_t3444____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3444____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3444_InternalEnumerator_1__ctor_m18900_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18900_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18900_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3444_InternalEnumerator_1__ctor_m18900_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18900_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18901_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18901_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3444_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18901_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18902_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18902_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3444_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18902_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18903_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18903_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3444_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18903_GenericMethod/* genericMethod */

};
extern Il2CppType RawImage_t338_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18904_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.RawImage>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18904_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3444_il2cpp_TypeInfo/* declaring_type */
	, &RawImage_t338_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18904_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3444_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18900_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18901_MethodInfo,
	&InternalEnumerator_1_Dispose_m18902_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18903_MethodInfo,
	&InternalEnumerator_1_get_Current_m18904_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m18903_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m18902_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3444_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18901_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18903_MethodInfo,
	&InternalEnumerator_1_Dispose_m18902_MethodInfo,
	&InternalEnumerator_1_get_Current_m18904_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3444_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6133_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3444_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6133_il2cpp_TypeInfo, 7},
};
extern TypeInfo RawImage_t338_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3444_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m18904_MethodInfo/* Method Usage */,
	&RawImage_t338_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisRawImage_t338_m34301_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3444_0_0_0;
extern Il2CppType InternalEnumerator_1_t3444_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3444_GenericClass;
TypeInfo InternalEnumerator_1_t3444_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3444_MethodInfos/* methods */
	, InternalEnumerator_1_t3444_PropertyInfos/* properties */
	, InternalEnumerator_1_t3444_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3444_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3444_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3444_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3444_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3444_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3444_1_0_0/* this_arg */
	, InternalEnumerator_1_t3444_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3444_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3444_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3444)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7803_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>
extern MethodInfo ICollection_1_get_Count_m44308_MethodInfo;
static PropertyInfo ICollection_1_t7803____Count_PropertyInfo = 
{
	&ICollection_1_t7803_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m44308_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m44309_MethodInfo;
static PropertyInfo ICollection_1_t7803____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7803_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m44309_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7803_PropertyInfos[] =
{
	&ICollection_1_t7803____Count_PropertyInfo,
	&ICollection_1_t7803____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m44308_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::get_Count()
MethodInfo ICollection_1_get_Count_m44308_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7803_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m44308_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m44309_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m44309_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7803_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m44309_GenericMethod/* genericMethod */

};
extern Il2CppType RawImage_t338_0_0_0;
extern Il2CppType RawImage_t338_0_0_0;
static ParameterInfo ICollection_1_t7803_ICollection_1_Add_m44310_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RawImage_t338_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m44310_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::Add(T)
MethodInfo ICollection_1_Add_m44310_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7803_ICollection_1_Add_m44310_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m44310_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m44311_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::Clear()
MethodInfo ICollection_1_Clear_m44311_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m44311_GenericMethod/* genericMethod */

};
extern Il2CppType RawImage_t338_0_0_0;
static ParameterInfo ICollection_1_t7803_ICollection_1_Contains_m44312_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RawImage_t338_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m44312_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::Contains(T)
MethodInfo ICollection_1_Contains_m44312_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7803_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7803_ICollection_1_Contains_m44312_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m44312_GenericMethod/* genericMethod */

};
extern Il2CppType RawImageU5BU5D_t5604_0_0_0;
extern Il2CppType RawImageU5BU5D_t5604_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7803_ICollection_1_CopyTo_m44313_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &RawImageU5BU5D_t5604_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m44313_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m44313_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7803_ICollection_1_CopyTo_m44313_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m44313_GenericMethod/* genericMethod */

};
extern Il2CppType RawImage_t338_0_0_0;
static ParameterInfo ICollection_1_t7803_ICollection_1_Remove_m44314_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RawImage_t338_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m44314_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UI.RawImage>::Remove(T)
MethodInfo ICollection_1_Remove_m44314_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7803_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7803_ICollection_1_Remove_m44314_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m44314_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7803_MethodInfos[] =
{
	&ICollection_1_get_Count_m44308_MethodInfo,
	&ICollection_1_get_IsReadOnly_m44309_MethodInfo,
	&ICollection_1_Add_m44310_MethodInfo,
	&ICollection_1_Clear_m44311_MethodInfo,
	&ICollection_1_Contains_m44312_MethodInfo,
	&ICollection_1_CopyTo_m44313_MethodInfo,
	&ICollection_1_Remove_m44314_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7805_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7803_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7805_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7803_0_0_0;
extern Il2CppType ICollection_1_t7803_1_0_0;
struct ICollection_1_t7803;
extern Il2CppGenericClass ICollection_1_t7803_GenericClass;
TypeInfo ICollection_1_t7803_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7803_MethodInfos/* methods */
	, ICollection_1_t7803_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7803_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7803_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7803_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7803_0_0_0/* byval_arg */
	, &ICollection_1_t7803_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7803_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.RawImage>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UI.RawImage>
extern Il2CppType IEnumerator_1_t6133_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m44315_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UI.RawImage>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m44315_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7805_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6133_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m44315_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7805_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m44315_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7805_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7805_0_0_0;
extern Il2CppType IEnumerable_1_t7805_1_0_0;
struct IEnumerable_1_t7805;
extern Il2CppGenericClass IEnumerable_1_t7805_GenericClass;
TypeInfo IEnumerable_1_t7805_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7805_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7805_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7805_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7805_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7805_0_0_0/* byval_arg */
	, &IEnumerable_1_t7805_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7805_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7804_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.RawImage>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.RawImage>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.RawImage>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UI.RawImage>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.RawImage>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UI.RawImage>
extern MethodInfo IList_1_get_Item_m44316_MethodInfo;
extern MethodInfo IList_1_set_Item_m44317_MethodInfo;
static PropertyInfo IList_1_t7804____Item_PropertyInfo = 
{
	&IList_1_t7804_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m44316_MethodInfo/* get */
	, &IList_1_set_Item_m44317_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7804_PropertyInfos[] =
{
	&IList_1_t7804____Item_PropertyInfo,
	NULL
};
extern Il2CppType RawImage_t338_0_0_0;
static ParameterInfo IList_1_t7804_IList_1_IndexOf_m44318_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &RawImage_t338_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m44318_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UI.RawImage>::IndexOf(T)
MethodInfo IList_1_IndexOf_m44318_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7804_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7804_IList_1_IndexOf_m44318_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m44318_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType RawImage_t338_0_0_0;
static ParameterInfo IList_1_t7804_IList_1_Insert_m44319_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &RawImage_t338_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m44319_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.RawImage>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m44319_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7804_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7804_IList_1_Insert_m44319_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m44319_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7804_IList_1_RemoveAt_m44320_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m44320_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.RawImage>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m44320_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7804_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7804_IList_1_RemoveAt_m44320_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m44320_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7804_IList_1_get_Item_m44316_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType RawImage_t338_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m44316_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UI.RawImage>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m44316_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7804_il2cpp_TypeInfo/* declaring_type */
	, &RawImage_t338_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7804_IList_1_get_Item_m44316_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m44316_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType RawImage_t338_0_0_0;
static ParameterInfo IList_1_t7804_IList_1_set_Item_m44317_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &RawImage_t338_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m44317_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UI.RawImage>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m44317_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7804_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7804_IList_1_set_Item_m44317_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m44317_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7804_MethodInfos[] =
{
	&IList_1_IndexOf_m44318_MethodInfo,
	&IList_1_Insert_m44319_MethodInfo,
	&IList_1_RemoveAt_m44320_MethodInfo,
	&IList_1_get_Item_m44316_MethodInfo,
	&IList_1_set_Item_m44317_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7804_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7803_il2cpp_TypeInfo,
	&IEnumerable_1_t7805_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7804_0_0_0;
extern Il2CppType IList_1_t7804_1_0_0;
struct IList_1_t7804;
extern Il2CppGenericClass IList_1_t7804_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7804_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7804_MethodInfos/* methods */
	, IList_1_t7804_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7804_il2cpp_TypeInfo/* element_class */
	, IList_1_t7804_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7804_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7804_0_0_0/* byval_arg */
	, &IList_1_t7804_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7804_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.RawImage>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_64.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t3445_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.RawImage>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_64MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_63.h"
extern TypeInfo InvokableCall_1_t3446_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_63MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m18907_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m18909_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.RawImage>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.RawImage>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.RawImage>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t3445____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t3445_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t3445, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t3445_FieldInfos[] =
{
	&CachedInvokableCall_1_t3445____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType RawImage_t338_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3445_CachedInvokableCall_1__ctor_m18905_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &RawImage_t338_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m18905_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.RawImage>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m18905_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t3445_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3445_CachedInvokableCall_1__ctor_m18905_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m18905_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t3445_CachedInvokableCall_1_Invoke_m18906_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m18906_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.RawImage>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m18906_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t3445_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t3445_CachedInvokableCall_1_Invoke_m18906_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m18906_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t3445_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m18905_MethodInfo,
	&CachedInvokableCall_1_Invoke_m18906_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m18906_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m18910_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t3445_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m18906_MethodInfo,
	&InvokableCall_1_Find_m18910_MethodInfo,
};
extern Il2CppType UnityAction_1_t3447_0_0_0;
extern TypeInfo UnityAction_1_t3447_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisRawImage_t338_m34311_MethodInfo;
extern TypeInfo RawImage_t338_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m18912_MethodInfo;
extern TypeInfo RawImage_t338_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t3445_RGCTXData[8] = 
{
	&UnityAction_1_t3447_0_0_0/* Type Usage */,
	&UnityAction_1_t3447_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRawImage_t338_m34311_MethodInfo/* Method Usage */,
	&RawImage_t338_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m18912_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m18907_MethodInfo/* Method Usage */,
	&RawImage_t338_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m18909_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t3445_0_0_0;
extern Il2CppType CachedInvokableCall_1_t3445_1_0_0;
struct CachedInvokableCall_1_t3445;
extern Il2CppGenericClass CachedInvokableCall_1_t3445_GenericClass;
TypeInfo CachedInvokableCall_1_t3445_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t3445_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t3445_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t3446_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t3445_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t3445_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t3445_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t3445_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t3445_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t3445_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t3445_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t3445)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.UI.RawImage>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_69.h"
extern TypeInfo UnityAction_1_t3447_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.RawImage>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_69MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.RawImage>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UI.RawImage>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisRawImage_t338_m34311(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>
extern Il2CppType UnityAction_1_t3447_0_0_1;
FieldInfo InvokableCall_1_t3446____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t3447_0_0_1/* type */
	, &InvokableCall_1_t3446_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t3446, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t3446_FieldInfos[] =
{
	&InvokableCall_1_t3446____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3446_InvokableCall_1__ctor_m18907_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m18907_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m18907_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t3446_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3446_InvokableCall_1__ctor_m18907_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m18907_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t3447_0_0_0;
static ParameterInfo InvokableCall_1_t3446_InvokableCall_1__ctor_m18908_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t3447_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m18908_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m18908_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t3446_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3446_InvokableCall_1__ctor_m18908_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m18908_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t3446_InvokableCall_1_Invoke_m18909_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m18909_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m18909_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t3446_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t3446_InvokableCall_1_Invoke_m18909_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m18909_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t3446_InvokableCall_1_Find_m18910_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m18910_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.RawImage>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m18910_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t3446_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t3446_InvokableCall_1_Find_m18910_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m18910_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t3446_MethodInfos[] =
{
	&InvokableCall_1__ctor_m18907_MethodInfo,
	&InvokableCall_1__ctor_m18908_MethodInfo,
	&InvokableCall_1_Invoke_m18909_MethodInfo,
	&InvokableCall_1_Find_m18910_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t3446_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m18909_MethodInfo,
	&InvokableCall_1_Find_m18910_MethodInfo,
};
extern TypeInfo UnityAction_1_t3447_il2cpp_TypeInfo;
extern TypeInfo RawImage_t338_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t3446_RGCTXData[5] = 
{
	&UnityAction_1_t3447_0_0_0/* Type Usage */,
	&UnityAction_1_t3447_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisRawImage_t338_m34311_MethodInfo/* Method Usage */,
	&RawImage_t338_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m18912_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t3446_0_0_0;
extern Il2CppType InvokableCall_1_t3446_1_0_0;
struct InvokableCall_1_t3446;
extern Il2CppGenericClass InvokableCall_1_t3446_GenericClass;
TypeInfo InvokableCall_1_t3446_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t3446_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t3446_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t3446_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t3446_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t3446_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t3446_0_0_0/* byval_arg */
	, &InvokableCall_1_t3446_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t3446_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t3446_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t3446)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.RawImage>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.RawImage>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.RawImage>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.RawImage>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UI.RawImage>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t3447_UnityAction_1__ctor_m18911_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m18911_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.RawImage>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m18911_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t3447_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t3447_UnityAction_1__ctor_m18911_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m18911_GenericMethod/* genericMethod */

};
extern Il2CppType RawImage_t338_0_0_0;
static ParameterInfo UnityAction_1_t3447_UnityAction_1_Invoke_m18912_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &RawImage_t338_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m18912_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.RawImage>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m18912_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t3447_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3447_UnityAction_1_Invoke_m18912_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m18912_GenericMethod/* genericMethod */

};
extern Il2CppType RawImage_t338_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t3447_UnityAction_1_BeginInvoke_m18913_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &RawImage_t338_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m18913_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UI.RawImage>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m18913_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t3447_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t3447_UnityAction_1_BeginInvoke_m18913_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m18913_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t3447_UnityAction_1_EndInvoke_m18914_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m18914_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UI.RawImage>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m18914_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t3447_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t3447_UnityAction_1_EndInvoke_m18914_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m18914_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t3447_MethodInfos[] =
{
	&UnityAction_1__ctor_m18911_MethodInfo,
	&UnityAction_1_Invoke_m18912_MethodInfo,
	&UnityAction_1_BeginInvoke_m18913_MethodInfo,
	&UnityAction_1_EndInvoke_m18914_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m18913_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m18914_MethodInfo;
static MethodInfo* UnityAction_1_t3447_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m18912_MethodInfo,
	&UnityAction_1_BeginInvoke_m18913_MethodInfo,
	&UnityAction_1_EndInvoke_m18914_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t3447_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t3447_1_0_0;
struct UnityAction_1_t3447;
extern Il2CppGenericClass UnityAction_1_t3447_GenericClass;
TypeInfo UnityAction_1_t3447_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t3447_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t3447_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t3447_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t3447_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t3447_0_0_0/* byval_arg */
	, &UnityAction_1_t3447_1_0_0/* this_arg */
	, UnityAction_1_t3447_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t3447_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t3447)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6135_il2cpp_TypeInfo;

// UnityEngine.UI.Scrollbar
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Scrollbar>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Scrollbar>
extern MethodInfo IEnumerator_1_get_Current_m44321_MethodInfo;
static PropertyInfo IEnumerator_1_t6135____Current_PropertyInfo = 
{
	&IEnumerator_1_t6135_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m44321_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6135_PropertyInfos[] =
{
	&IEnumerator_1_t6135____Current_PropertyInfo,
	NULL
};
extern Il2CppType Scrollbar_t343_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m44321_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Scrollbar>::get_Current()
MethodInfo IEnumerator_1_get_Current_m44321_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6135_il2cpp_TypeInfo/* declaring_type */
	, &Scrollbar_t343_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m44321_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6135_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m44321_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6135_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6135_0_0_0;
extern Il2CppType IEnumerator_1_t6135_1_0_0;
struct IEnumerator_1_t6135;
extern Il2CppGenericClass IEnumerator_1_t6135_GenericClass;
TypeInfo IEnumerator_1_t6135_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6135_MethodInfos/* methods */
	, IEnumerator_1_t6135_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6135_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6135_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6135_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6135_0_0_0/* byval_arg */
	, &IEnumerator_1_t6135_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6135_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_223.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t3448_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_223MethodDeclarations.h"

extern TypeInfo Scrollbar_t343_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m18919_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisScrollbar_t343_m34313_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Scrollbar>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.Scrollbar>(System.Int32)
#define Array_InternalArray__get_Item_TisScrollbar_t343_m34313(__this, p0, method) (Scrollbar_t343 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t3448____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t3448_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3448, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t3448____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t3448_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t3448, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t3448_FieldInfos[] =
{
	&InternalEnumerator_1_t3448____array_0_FieldInfo,
	&InternalEnumerator_1_t3448____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18916_MethodInfo;
static PropertyInfo InternalEnumerator_1_t3448____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3448_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18916_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t3448____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t3448_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m18919_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t3448_PropertyInfos[] =
{
	&InternalEnumerator_1_t3448____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t3448____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t3448_InternalEnumerator_1__ctor_m18915_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m18915_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m18915_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t3448_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t3448_InternalEnumerator_1__ctor_m18915_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m18915_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18916_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18916_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t3448_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18916_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m18917_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m18917_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t3448_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m18917_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m18918_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m18918_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t3448_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m18918_GenericMethod/* genericMethod */

};
extern Il2CppType Scrollbar_t343_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m18919_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Scrollbar>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m18919_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t3448_il2cpp_TypeInfo/* declaring_type */
	, &Scrollbar_t343_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m18919_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t3448_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m18915_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18916_MethodInfo,
	&InternalEnumerator_1_Dispose_m18917_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18918_MethodInfo,
	&InternalEnumerator_1_get_Current_m18919_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m18918_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m18917_MethodInfo;
static MethodInfo* InternalEnumerator_1_t3448_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18916_MethodInfo,
	&InternalEnumerator_1_MoveNext_m18918_MethodInfo,
	&InternalEnumerator_1_Dispose_m18917_MethodInfo,
	&InternalEnumerator_1_get_Current_m18919_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t3448_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6135_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t3448_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6135_il2cpp_TypeInfo, 7},
};
extern TypeInfo Scrollbar_t343_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t3448_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m18919_MethodInfo/* Method Usage */,
	&Scrollbar_t343_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisScrollbar_t343_m34313_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t3448_0_0_0;
extern Il2CppType InternalEnumerator_1_t3448_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t3448_GenericClass;
TypeInfo InternalEnumerator_1_t3448_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t3448_MethodInfos/* methods */
	, InternalEnumerator_1_t3448_PropertyInfos/* properties */
	, InternalEnumerator_1_t3448_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t3448_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t3448_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t3448_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t3448_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t3448_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t3448_1_0_0/* this_arg */
	, InternalEnumerator_1_t3448_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t3448_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t3448_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t3448)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
