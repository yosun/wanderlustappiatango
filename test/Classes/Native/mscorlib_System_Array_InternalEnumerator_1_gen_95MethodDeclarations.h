﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>
struct InternalEnumerator_1_t2915;
// System.Object
struct Object_t;
// Vuforia.ReconstructionBehaviour
struct ReconstructionBehaviour_t38;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m15051(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15052(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m15053(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15054(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.ReconstructionBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m15055(__this, method) (ReconstructionBehaviour_t38 *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
