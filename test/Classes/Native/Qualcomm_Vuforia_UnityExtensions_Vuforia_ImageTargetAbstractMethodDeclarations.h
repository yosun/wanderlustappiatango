﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t50;
// Vuforia.ImageTarget
struct ImageTarget_t572;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t553;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t56;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t2;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonAbstractBehaviour>
struct IEnumerable_1_t751;
// Vuforia.VirtualButton
struct VirtualButton_t595;
// UnityEngine.Transform
struct Transform_t10;
// Vuforia.ImageTargetType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetType.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// Vuforia.ImageTarget Vuforia.ImageTargetAbstractBehaviour::get_ImageTarget()
 Object_t * ImageTargetAbstractBehaviour_get_ImageTarget_m4079 (ImageTargetAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::.ctor()
 void ImageTargetAbstractBehaviour__ctor_m421 (ImageTargetAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetAbstractBehaviour::CorrectScaleImpl()
 bool ImageTargetAbstractBehaviour_CorrectScaleImpl_m428 (ImageTargetAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::InternalUnregisterTrackable()
 void ImageTargetAbstractBehaviour_InternalUnregisterTrackable_m427 (ImageTargetAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
 void ImageTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m429 (ImageTargetAbstractBehaviour_t50 * __this, Vector3_t13 * ___boundsMin, Vector3_t13 * ___boundsMax, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget)
 void ImageTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m430 (ImageTargetAbstractBehaviour_t50 * __this, Object_t * ___reconstructionFromTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.ImageTargetAbstractBehaviour::CreateVirtualButton(System.String,UnityEngine.Vector2,UnityEngine.Vector2)
 VirtualButtonAbstractBehaviour_t56 * ImageTargetAbstractBehaviour_CreateVirtualButton_m4080 (ImageTargetAbstractBehaviour_t50 * __this, String_t* ___vbName, Vector2_t9  ___position, Vector2_t9  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.ImageTargetAbstractBehaviour::CreateVirtualButton(System.String,UnityEngine.Vector2,UnityEngine.GameObject)
 VirtualButtonAbstractBehaviour_t56 * ImageTargetAbstractBehaviour_CreateVirtualButton_m4081 (Object_t * __this/* static, unused */, String_t* ___vbName, Vector2_t9  ___localScale, GameObject_t2 * ___immediateParent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButtonAbstractBehaviour> Vuforia.ImageTargetAbstractBehaviour::GetVirtualButtonBehaviours()
 Object_t* ImageTargetAbstractBehaviour_GetVirtualButtonBehaviours_m4082 (ImageTargetAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::DestroyVirtualButton(System.String)
 void ImageTargetAbstractBehaviour_DestroyVirtualButton_m4083 (ImageTargetAbstractBehaviour_t50 * __this, String_t* ___vbName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Vuforia.ImageTargetAbstractBehaviour::GetSize()
 Vector2_t9  ImageTargetAbstractBehaviour_GetSize_m435 (ImageTargetAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::SetWidth(System.Single)
 void ImageTargetAbstractBehaviour_SetWidth_m436 (ImageTargetAbstractBehaviour_t50 * __this, float ___width, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::SetHeight(System.Single)
 void ImageTargetAbstractBehaviour_SetHeight_m437 (ImageTargetAbstractBehaviour_t50 * __this, float ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.get_AspectRatio()
 float ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_AspectRatio_m431 (ImageTargetAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetType Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.get_ImageTargetType()
 int32_t ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_get_ImageTargetType_m432 (ImageTargetAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.SetAspectRatio(System.Single)
 bool ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetAspectRatio_m433 (ImageTargetAbstractBehaviour_t50 * __this, float ___aspectRatio, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.SetImageTargetType(Vuforia.ImageTargetType)
 bool ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_SetImageTargetType_m434 (ImageTargetAbstractBehaviour_t50 * __this, int32_t ___imageTargetType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.InitializeImageTarget(Vuforia.ImageTarget)
 void ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_InitializeImageTarget_m438 (ImageTargetAbstractBehaviour_t50 * __this, Object_t * ___imageTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.AssociateExistingVirtualButtonBehaviour(Vuforia.VirtualButtonAbstractBehaviour)
 void ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_AssociateExistingVirtualButtonBehaviour_m441 (ImageTargetAbstractBehaviour_t50 * __this, VirtualButtonAbstractBehaviour_t56 * ___virtualButtonBehaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.CreateMissingVirtualButtonBehaviours()
 void ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_CreateMissingVirtualButtonBehaviours_m439 (ImageTargetAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorImageTargetBehaviour.TryGetVirtualButtonBehaviourByID(System.Int32,Vuforia.VirtualButtonAbstractBehaviour&)
 bool ImageTargetAbstractBehaviour_Vuforia_IEditorImageTargetBehaviour_TryGetVirtualButtonBehaviourByID_m440 (ImageTargetAbstractBehaviour_t50 * __this, int32_t ___id, VirtualButtonAbstractBehaviour_t56 ** ___virtualButtonBehaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::CreateVirtualButtonFromNative(Vuforia.VirtualButton)
 void ImageTargetAbstractBehaviour_CreateVirtualButtonFromNative_m4084 (ImageTargetAbstractBehaviour_t50 * __this, VirtualButton_t595 * ___virtualButton, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetAbstractBehaviour::CreateNewVirtualButtonFromBehaviour(Vuforia.VirtualButtonAbstractBehaviour)
 bool ImageTargetAbstractBehaviour_CreateNewVirtualButtonFromBehaviour_m4085 (ImageTargetAbstractBehaviour_t50 * __this, VirtualButtonAbstractBehaviour_t56 * ___newVBB, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m422 (ImageTargetAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m423 (ImageTargetAbstractBehaviour_t50 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t10 * ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m424 (ImageTargetAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.ImageTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t2 * ImageTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m425 (ImageTargetAbstractBehaviour_t50 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
