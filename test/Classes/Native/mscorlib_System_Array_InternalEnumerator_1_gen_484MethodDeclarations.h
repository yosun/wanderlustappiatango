﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>
struct InternalEnumerator_1_t4845;
// System.Object
struct Object_t;
// System.ComponentModel.EditorBrowsableAttribute
struct EditorBrowsableAttribute_t1128;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m29760(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29761(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::Dispose()
#define InternalEnumerator_1_Dispose_m29762(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::MoveNext()
#define InternalEnumerator_1_MoveNext_m29763(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::get_Current()
#define InternalEnumerator_1_get_Current_m29764(__this, method) (EditorBrowsableAttribute_t1128 *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
