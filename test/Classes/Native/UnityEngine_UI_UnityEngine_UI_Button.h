﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t268;
// UnityEngine.UI.Selectable
#include "UnityEngine_UI_UnityEngine_UI_Selectable.h"
// UnityEngine.UI.Button
struct Button_t270  : public Selectable_t272
{
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t268 * ___m_OnClick_16;
};
