﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory
struct NullBehaviourComponentFactory_t592;
// Vuforia.MaskOutAbstractBehaviour
struct MaskOutAbstractBehaviour_t55;
// UnityEngine.GameObject
struct GameObject_t2;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t56;
// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t57;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t50;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t58;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t59;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t34;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t60;
// Vuforia.TextRecoAbstractBehaviour
struct TextRecoAbstractBehaviour_t61;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t62;

// Vuforia.MaskOutAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMaskOutBehaviour(UnityEngine.GameObject)
 MaskOutAbstractBehaviour_t55 * NullBehaviourComponentFactory_AddMaskOutBehaviour_m2804 (NullBehaviourComponentFactory_t592 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButtonAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddVirtualButtonBehaviour(UnityEngine.GameObject)
 VirtualButtonAbstractBehaviour_t56 * NullBehaviourComponentFactory_AddVirtualButtonBehaviour_m2805 (NullBehaviourComponentFactory_t592 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TurnOffAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTurnOffBehaviour(UnityEngine.GameObject)
 TurnOffAbstractBehaviour_t57 * NullBehaviourComponentFactory_AddTurnOffBehaviour_m2806 (NullBehaviourComponentFactory_t592 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddImageTargetBehaviour(UnityEngine.GameObject)
 ImageTargetAbstractBehaviour_t50 * NullBehaviourComponentFactory_AddImageTargetBehaviour_m2807 (NullBehaviourComponentFactory_t592 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMarkerBehaviour(UnityEngine.GameObject)
 MarkerAbstractBehaviour_t58 * NullBehaviourComponentFactory_AddMarkerBehaviour_m2808 (NullBehaviourComponentFactory_t592 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddMultiTargetBehaviour(UnityEngine.GameObject)
 MultiTargetAbstractBehaviour_t59 * NullBehaviourComponentFactory_AddMultiTargetBehaviour_m2809 (NullBehaviourComponentFactory_t592 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddCylinderTargetBehaviour(UnityEngine.GameObject)
 CylinderTargetAbstractBehaviour_t34 * NullBehaviourComponentFactory_AddCylinderTargetBehaviour_m2810 (NullBehaviourComponentFactory_t592 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject)
 WordAbstractBehaviour_t60 * NullBehaviourComponentFactory_AddWordBehaviour_m2811 (NullBehaviourComponentFactory_t592 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TextRecoAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddTextRecoBehaviour(UnityEngine.GameObject)
 TextRecoAbstractBehaviour_t61 * NullBehaviourComponentFactory_AddTextRecoBehaviour_m2812 (NullBehaviourComponentFactory_t592 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::AddObjectTargetBehaviour(UnityEngine.GameObject)
 ObjectTargetAbstractBehaviour_t62 * NullBehaviourComponentFactory_AddObjectTargetBehaviour_m2813 (NullBehaviourComponentFactory_t592 * __this, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.BehaviourComponentFactory/NullBehaviourComponentFactory::.ctor()
 void NullBehaviourComponentFactory__ctor_m2814 (NullBehaviourComponentFactory_t592 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
