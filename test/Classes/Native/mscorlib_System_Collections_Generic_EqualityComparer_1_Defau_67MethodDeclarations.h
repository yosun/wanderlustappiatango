﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t4699;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
 void DefaultComparer__ctor_m28785 (DefaultComparer_t4699 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::GetHashCode(T)
 int32_t DefaultComparer_GetHashCode_m28786 (DefaultComparer_t4699 * __this, UICharInfo_t489  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::Equals(T,T)
 bool DefaultComparer_Equals_m28787 (DefaultComparer_t4699 * __this, UICharInfo_t489  ___x, UICharInfo_t489  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
