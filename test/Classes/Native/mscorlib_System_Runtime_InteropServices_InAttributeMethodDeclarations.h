﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.InAttribute
struct InAttribute_t1711;

// System.Void System.Runtime.InteropServices.InAttribute::.ctor()
 void InAttribute__ctor_m9737 (InAttribute_t1711 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
