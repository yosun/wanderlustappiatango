﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_t968  : public MulticastDelegate_t325
{
};
