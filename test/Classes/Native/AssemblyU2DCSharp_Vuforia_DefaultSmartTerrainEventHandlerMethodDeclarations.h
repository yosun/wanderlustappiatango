﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DefaultSmartTerrainEventHandler
struct DefaultSmartTerrainEventHandler_t41;
// Vuforia.Prop
struct Prop_t42;
// Vuforia.Surface
struct Surface_t43;

// System.Void Vuforia.DefaultSmartTerrainEventHandler::.ctor()
 void DefaultSmartTerrainEventHandler__ctor_m98 (DefaultSmartTerrainEventHandler_t41 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::Start()
 void DefaultSmartTerrainEventHandler_Start_m99 (DefaultSmartTerrainEventHandler_t41 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnDestroy()
 void DefaultSmartTerrainEventHandler_OnDestroy_m100 (DefaultSmartTerrainEventHandler_t41 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnPropCreated(Vuforia.Prop)
 void DefaultSmartTerrainEventHandler_OnPropCreated_m101 (DefaultSmartTerrainEventHandler_t41 * __this, Object_t * ___prop, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultSmartTerrainEventHandler::OnSurfaceCreated(Vuforia.Surface)
 void DefaultSmartTerrainEventHandler_OnSurfaceCreated_m102 (DefaultSmartTerrainEventHandler_t41 * __this, Object_t * ___surface, MethodInfo* method) IL2CPP_METHOD_ATTR;
