﻿#pragma once
#include <stdint.h>
// UnityEngine.Renderer
struct Renderer_t7;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// AnimateTexture
struct AnimateTexture_t8  : public MonoBehaviour_t6
{
	// UnityEngine.Vector2 AnimateTexture::uvOffset
	Vector2_t9  ___uvOffset_2;
	// UnityEngine.Vector2 AnimateTexture::uvAnimationRate
	Vector2_t9  ___uvAnimationRate_3;
	// UnityEngine.Renderer AnimateTexture::renderer
	Renderer_t7 * ___renderer_4;
};
