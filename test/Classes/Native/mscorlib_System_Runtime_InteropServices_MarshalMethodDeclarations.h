﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.Marshal
struct Marshal_t776;
// System.Array
struct Array_t;
// System.Byte[]
struct ByteU5BU5D_t609;
// System.Char[]
struct CharU5BU5D_t108;
// System.Single[]
struct SingleU5BU5D_t578;
// System.String
struct String_t;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Runtime.InteropServices.Marshal::.cctor()
 void Marshal__cctor_m11419 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::AllocHGlobal(System.IntPtr)
 IntPtr_t121 Marshal_AllocHGlobal_m11420 (Object_t * __this/* static, unused */, IntPtr_t121 ___cb, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::AllocHGlobal(System.Int32)
 IntPtr_t121 Marshal_AllocHGlobal_m4268 (Object_t * __this/* static, unused */, int32_t ___cb, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::copy_to_unmanaged(System.Array,System.Int32,System.IntPtr,System.Int32)
 void Marshal_copy_to_unmanaged_m11421 (Object_t * __this/* static, unused */, Array_t * ___source, int32_t ___startIndex, IntPtr_t121 ___destination, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::copy_from_unmanaged(System.IntPtr,System.Int32,System.Array,System.Int32)
 void Marshal_copy_from_unmanaged_m11422 (Object_t * __this/* static, unused */, IntPtr_t121 ___source, int32_t ___startIndex, Array_t * ___destination, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.Byte[],System.Int32,System.IntPtr,System.Int32)
 void Marshal_Copy_m4733 (Object_t * __this/* static, unused */, ByteU5BU5D_t609* ___source, int32_t ___startIndex, IntPtr_t121 ___destination, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Byte[],System.Int32,System.Int32)
 void Marshal_Copy_m4730 (Object_t * __this/* static, unused */, IntPtr_t121 ___source, ByteU5BU5D_t609* ___destination, int32_t ___startIndex, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Char[],System.Int32,System.Int32)
 void Marshal_Copy_m11423 (Object_t * __this/* static, unused */, IntPtr_t121 ___source, CharU5BU5D_t108* ___destination, int32_t ___startIndex, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Single[],System.Int32,System.Int32)
 void Marshal_Copy_m4270 (Object_t * __this/* static, unused */, IntPtr_t121 ___source, SingleU5BU5D_t578* ___destination, int32_t ___startIndex, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::FreeHGlobal(System.IntPtr)
 void Marshal_FreeHGlobal_m4273 (Object_t * __this/* static, unused */, IntPtr_t121 ___hglobal, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::PtrToStringAnsi(System.IntPtr)
 String_t* Marshal_PtrToStringAnsi_m5267 (Object_t * __this/* static, unused */, IntPtr_t121 ___ptr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.InteropServices.Marshal::PtrToStringUni(System.IntPtr)
 String_t* Marshal_PtrToStringUni_m4277 (Object_t * __this/* static, unused */, IntPtr_t121 ___ptr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.InteropServices.Marshal::PtrToStructure(System.IntPtr,System.Type)
 Object_t * Marshal_PtrToStructure_m4461 (Object_t * __this/* static, unused */, IntPtr_t121 ___ptr, Type_t * ___structureType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::ReadInt32(System.IntPtr)
 int32_t Marshal_ReadInt32_m5270 (Object_t * __this/* static, unused */, IntPtr_t121 ___ptr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::ReadInt32(System.IntPtr,System.Int32)
 int32_t Marshal_ReadInt32_m11424 (Object_t * __this/* static, unused */, IntPtr_t121 ___ptr, int32_t ___ofs, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Runtime.InteropServices.Marshal::SizeOf(System.Type)
 int32_t Marshal_SizeOf_m4267 (Object_t * __this/* static, unused */, Type_t * ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr System.Runtime.InteropServices.Marshal::StringToHGlobalUni(System.String)
 IntPtr_t121 Marshal_StringToHGlobalUni_m4278 (Object_t * __this/* static, unused */, String_t* ___s, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.InteropServices.Marshal::StructureToPtr(System.Object,System.IntPtr,System.Boolean)
 void Marshal_StructureToPtr_m4360 (Object_t * __this/* static, unused */, Object_t * ___structure, IntPtr_t121 ___ptr, bool ___fDeleteOld, MethodInfo* method) IL2CPP_METHOD_ATTR;
