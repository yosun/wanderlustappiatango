﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// UnityEngine.EventSystems.EventHandle[]
// UnityEngine.EventSystems.EventHandle[]
struct EventHandleU5BU5D_t5550  : public Array_t
{
};
// UnityEngine.EventSystems.EventSystem[]
// UnityEngine.EventSystems.EventSystem[]
struct EventSystemU5BU5D_t5551  : public Array_t
{
};
struct EventSystemU5BU5D_t5551_StaticFields{
};
// UnityEngine.EventSystems.UIBehaviour[]
// UnityEngine.EventSystems.UIBehaviour[]
struct UIBehaviourU5BU5D_t5552  : public Array_t
{
};
// UnityEngine.EventSystems.BaseInputModule[]
// UnityEngine.EventSystems.BaseInputModule[]
struct BaseInputModuleU5BU5D_t3003  : public Array_t
{
};
// UnityEngine.EventSystems.IEventSystemHandler[]
// UnityEngine.EventSystems.IEventSystemHandler[]
struct IEventSystemHandlerU5BU5D_t3019  : public Array_t
{
};
// UnityEngine.EventSystems.RaycastResult[]
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t3056  : public Array_t
{
};
// UnityEngine.EventSystems.BaseRaycaster[]
// UnityEngine.EventSystems.BaseRaycaster[]
struct BaseRaycasterU5BU5D_t3070  : public Array_t
{
};
// UnityEngine.EventSystems.EventTrigger[]
// UnityEngine.EventSystems.EventTrigger[]
struct EventTriggerU5BU5D_t5553  : public Array_t
{
};
// UnityEngine.EventSystems.IPointerEnterHandler[]
// UnityEngine.EventSystems.IPointerEnterHandler[]
struct IPointerEnterHandlerU5BU5D_t5554  : public Array_t
{
};
// UnityEngine.EventSystems.IPointerExitHandler[]
// UnityEngine.EventSystems.IPointerExitHandler[]
struct IPointerExitHandlerU5BU5D_t5555  : public Array_t
{
};
// UnityEngine.EventSystems.IPointerDownHandler[]
// UnityEngine.EventSystems.IPointerDownHandler[]
struct IPointerDownHandlerU5BU5D_t5556  : public Array_t
{
};
// UnityEngine.EventSystems.IPointerUpHandler[]
// UnityEngine.EventSystems.IPointerUpHandler[]
struct IPointerUpHandlerU5BU5D_t5557  : public Array_t
{
};
// UnityEngine.EventSystems.IPointerClickHandler[]
// UnityEngine.EventSystems.IPointerClickHandler[]
struct IPointerClickHandlerU5BU5D_t5558  : public Array_t
{
};
// UnityEngine.EventSystems.IBeginDragHandler[]
// UnityEngine.EventSystems.IBeginDragHandler[]
struct IBeginDragHandlerU5BU5D_t5559  : public Array_t
{
};
// UnityEngine.EventSystems.IInitializePotentialDragHandler[]
// UnityEngine.EventSystems.IInitializePotentialDragHandler[]
struct IInitializePotentialDragHandlerU5BU5D_t5560  : public Array_t
{
};
// UnityEngine.EventSystems.IDragHandler[]
// UnityEngine.EventSystems.IDragHandler[]
struct IDragHandlerU5BU5D_t5561  : public Array_t
{
};
// UnityEngine.EventSystems.IEndDragHandler[]
// UnityEngine.EventSystems.IEndDragHandler[]
struct IEndDragHandlerU5BU5D_t5562  : public Array_t
{
};
// UnityEngine.EventSystems.IDropHandler[]
// UnityEngine.EventSystems.IDropHandler[]
struct IDropHandlerU5BU5D_t5563  : public Array_t
{
};
// UnityEngine.EventSystems.IScrollHandler[]
// UnityEngine.EventSystems.IScrollHandler[]
struct IScrollHandlerU5BU5D_t5564  : public Array_t
{
};
// UnityEngine.EventSystems.IUpdateSelectedHandler[]
// UnityEngine.EventSystems.IUpdateSelectedHandler[]
struct IUpdateSelectedHandlerU5BU5D_t5565  : public Array_t
{
};
// UnityEngine.EventSystems.ISelectHandler[]
// UnityEngine.EventSystems.ISelectHandler[]
struct ISelectHandlerU5BU5D_t5566  : public Array_t
{
};
// UnityEngine.EventSystems.IDeselectHandler[]
// UnityEngine.EventSystems.IDeselectHandler[]
struct IDeselectHandlerU5BU5D_t5567  : public Array_t
{
};
// UnityEngine.EventSystems.IMoveHandler[]
// UnityEngine.EventSystems.IMoveHandler[]
struct IMoveHandlerU5BU5D_t5568  : public Array_t
{
};
// UnityEngine.EventSystems.ISubmitHandler[]
// UnityEngine.EventSystems.ISubmitHandler[]
struct ISubmitHandlerU5BU5D_t5569  : public Array_t
{
};
// UnityEngine.EventSystems.ICancelHandler[]
// UnityEngine.EventSystems.ICancelHandler[]
struct ICancelHandlerU5BU5D_t5570  : public Array_t
{
};
// UnityEngine.EventSystems.EventTrigger/Entry[]
// UnityEngine.EventSystems.EventTrigger/Entry[]
struct EntryU5BU5D_t3106  : public Array_t
{
};
// UnityEngine.EventSystems.EventTriggerType[]
// UnityEngine.EventSystems.EventTriggerType[]
struct EventTriggerTypeU5BU5D_t5571  : public Array_t
{
};
// UnityEngine.EventSystems.MoveDirection[]
// UnityEngine.EventSystems.MoveDirection[]
struct MoveDirectionU5BU5D_t5572  : public Array_t
{
};
// UnityEngine.EventSystems.PointerEventData/InputButton[]
// UnityEngine.EventSystems.PointerEventData/InputButton[]
struct InputButtonU5BU5D_t5573  : public Array_t
{
};
// UnityEngine.EventSystems.PointerEventData/FramePressState[]
// UnityEngine.EventSystems.PointerEventData/FramePressState[]
struct FramePressStateU5BU5D_t5574  : public Array_t
{
};
// UnityEngine.EventSystems.PointerInputModule[]
// UnityEngine.EventSystems.PointerInputModule[]
struct PointerInputModuleU5BU5D_t5575  : public Array_t
{
};
// UnityEngine.EventSystems.PointerEventData[]
// UnityEngine.EventSystems.PointerEventData[]
struct PointerEventDataU5BU5D_t3151  : public Array_t
{
};
// UnityEngine.EventSystems.BaseEventData[]
// UnityEngine.EventSystems.BaseEventData[]
struct BaseEventDataU5BU5D_t5576  : public Array_t
{
};
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
struct ButtonStateU5BU5D_t3174  : public Array_t
{
};
// UnityEngine.EventSystems.StandaloneInputModule[]
// UnityEngine.EventSystems.StandaloneInputModule[]
struct StandaloneInputModuleU5BU5D_t5577  : public Array_t
{
};
// UnityEngine.EventSystems.StandaloneInputModule/InputMode[]
// UnityEngine.EventSystems.StandaloneInputModule/InputMode[]
struct InputModeU5BU5D_t5578  : public Array_t
{
};
// UnityEngine.EventSystems.TouchInputModule[]
// UnityEngine.EventSystems.TouchInputModule[]
struct TouchInputModuleU5BU5D_t5579  : public Array_t
{
};
// UnityEngine.EventSystems.Physics2DRaycaster[]
// UnityEngine.EventSystems.Physics2DRaycaster[]
struct Physics2DRaycasterU5BU5D_t5580  : public Array_t
{
};
// UnityEngine.EventSystems.PhysicsRaycaster[]
// UnityEngine.EventSystems.PhysicsRaycaster[]
struct PhysicsRaycasterU5BU5D_t5581  : public Array_t
{
};
struct PhysicsRaycasterU5BU5D_t5581_StaticFields{
};
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode[]
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode[]
struct ColorTweenModeU5BU5D_t5582  : public Array_t
{
};
// UnityEngine.UI.Button[]
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t5583  : public Array_t
{
};
// UnityEngine.UI.Selectable[]
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t3465  : public Array_t
{
};
struct SelectableU5BU5D_t3465_StaticFields{
};
// UnityEngine.UI.CanvasUpdate[]
// UnityEngine.UI.CanvasUpdate[]
struct CanvasUpdateU5BU5D_t5584  : public Array_t
{
};
// UnityEngine.UI.ICanvasElement[]
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t3222  : public Array_t
{
};
// UnityEngine.UI.Text[]
// UnityEngine.UI.Text[]
struct TextU5BU5D_t3290  : public Array_t
{
};
struct TextU5BU5D_t3290_StaticFields{
};
// UnityEngine.UI.ILayoutElement[]
// UnityEngine.UI.ILayoutElement[]
struct ILayoutElementU5BU5D_t5585  : public Array_t
{
};
// UnityEngine.UI.MaskableGraphic[]
// UnityEngine.UI.MaskableGraphic[]
struct MaskableGraphicU5BU5D_t5586  : public Array_t
{
};
// UnityEngine.UI.IMaskable[]
// UnityEngine.UI.IMaskable[]
struct IMaskableU5BU5D_t5587  : public Array_t
{
};
// UnityEngine.UI.Graphic[]
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t3367  : public Array_t
{
};
struct GraphicU5BU5D_t3367_StaticFields{
};
// UnityEngine.UI.GraphicRaycaster[]
// UnityEngine.UI.GraphicRaycaster[]
struct GraphicRaycasterU5BU5D_t5588  : public Array_t
{
};
struct GraphicRaycasterU5BU5D_t5588_StaticFields{
};
// UnityEngine.UI.GraphicRaycaster/BlockingObjects[]
// UnityEngine.UI.GraphicRaycaster/BlockingObjects[]
struct BlockingObjectsU5BU5D_t5589  : public Array_t
{
};
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
struct IndexedSet_1U5BU5D_t3379  : public Array_t
{
};
// UnityEngine.UI.Image[]
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t5590  : public Array_t
{
};
struct ImageU5BU5D_t5590_StaticFields{
};
// UnityEngine.UI.Image/Type[]
// UnityEngine.UI.Image/Type[]
struct TypeU5BU5D_t5591  : public Array_t
{
};
// UnityEngine.UI.Image/FillMethod[]
// UnityEngine.UI.Image/FillMethod[]
struct FillMethodU5BU5D_t5592  : public Array_t
{
};
// UnityEngine.UI.Image/OriginHorizontal[]
// UnityEngine.UI.Image/OriginHorizontal[]
struct OriginHorizontalU5BU5D_t5593  : public Array_t
{
};
// UnityEngine.UI.Image/OriginVertical[]
// UnityEngine.UI.Image/OriginVertical[]
struct OriginVerticalU5BU5D_t5594  : public Array_t
{
};
// UnityEngine.UI.Image/Origin90[]
// UnityEngine.UI.Image/Origin90[]
struct Origin90U5BU5D_t5595  : public Array_t
{
};
// UnityEngine.UI.Image/Origin180[]
// UnityEngine.UI.Image/Origin180[]
struct Origin180U5BU5D_t5596  : public Array_t
{
};
// UnityEngine.UI.Image/Origin360[]
// UnityEngine.UI.Image/Origin360[]
struct Origin360U5BU5D_t5597  : public Array_t
{
};
// UnityEngine.UI.InputField[]
// UnityEngine.UI.InputField[]
struct InputFieldU5BU5D_t5598  : public Array_t
{
};
struct InputFieldU5BU5D_t5598_StaticFields{
};
// UnityEngine.UI.InputField/ContentType[]
// UnityEngine.UI.InputField/ContentType[]
struct ContentTypeU5BU5D_t334  : public Array_t
{
};
// UnityEngine.UI.InputField/InputType[]
// UnityEngine.UI.InputField/InputType[]
struct InputTypeU5BU5D_t5599  : public Array_t
{
};
// UnityEngine.UI.InputField/CharacterValidation[]
// UnityEngine.UI.InputField/CharacterValidation[]
struct CharacterValidationU5BU5D_t5600  : public Array_t
{
};
// UnityEngine.UI.InputField/LineType[]
// UnityEngine.UI.InputField/LineType[]
struct LineTypeU5BU5D_t5601  : public Array_t
{
};
// UnityEngine.UI.InputField/EditState[]
// UnityEngine.UI.InputField/EditState[]
struct EditStateU5BU5D_t5602  : public Array_t
{
};
// UnityEngine.UI.Navigation/Mode[]
// UnityEngine.UI.Navigation/Mode[]
struct ModeU5BU5D_t5603  : public Array_t
{
};
// UnityEngine.UI.RawImage[]
// UnityEngine.UI.RawImage[]
struct RawImageU5BU5D_t5604  : public Array_t
{
};
// UnityEngine.UI.Scrollbar[]
// UnityEngine.UI.Scrollbar[]
struct ScrollbarU5BU5D_t5605  : public Array_t
{
};
// UnityEngine.UI.Scrollbar/Direction[]
// UnityEngine.UI.Scrollbar/Direction[]
struct DirectionU5BU5D_t5606  : public Array_t
{
};
// UnityEngine.UI.Scrollbar/Axis[]
// UnityEngine.UI.Scrollbar/Axis[]
struct AxisU5BU5D_t5607  : public Array_t
{
};
// UnityEngine.UI.ScrollRect[]
// UnityEngine.UI.ScrollRect[]
struct ScrollRectU5BU5D_t5608  : public Array_t
{
};
// UnityEngine.UI.ScrollRect/MovementType[]
// UnityEngine.UI.ScrollRect/MovementType[]
struct MovementTypeU5BU5D_t5609  : public Array_t
{
};
// UnityEngine.UI.Selectable/Transition[]
// UnityEngine.UI.Selectable/Transition[]
struct TransitionU5BU5D_t5610  : public Array_t
{
};
// UnityEngine.UI.Selectable/SelectionState[]
// UnityEngine.UI.Selectable/SelectionState[]
struct SelectionStateU5BU5D_t5611  : public Array_t
{
};
// UnityEngine.UI.Slider[]
// UnityEngine.UI.Slider[]
struct SliderU5BU5D_t5612  : public Array_t
{
};
// UnityEngine.UI.Slider/Direction[]
// UnityEngine.UI.Slider/Direction[]
struct DirectionU5BU5D_t5613  : public Array_t
{
};
// UnityEngine.UI.Slider/Axis[]
// UnityEngine.UI.Slider/Axis[]
struct AxisU5BU5D_t5614  : public Array_t
{
};
// UnityEngine.UI.StencilMaterial/MatEntry[]
// UnityEngine.UI.StencilMaterial/MatEntry[]
struct MatEntryU5BU5D_t3505  : public Array_t
{
};
// UnityEngine.UI.Toggle[]
// UnityEngine.UI.Toggle[]
struct ToggleU5BU5D_t3534  : public Array_t
{
};
// UnityEngine.UI.Toggle/ToggleTransition[]
// UnityEngine.UI.Toggle/ToggleTransition[]
struct ToggleTransitionU5BU5D_t5615  : public Array_t
{
};
// UnityEngine.UI.ToggleGroup[]
// UnityEngine.UI.ToggleGroup[]
struct ToggleGroupU5BU5D_t5616  : public Array_t
{
};
struct ToggleGroupU5BU5D_t5616_StaticFields{
};
// UnityEngine.UI.AspectRatioFitter[]
// UnityEngine.UI.AspectRatioFitter[]
struct AspectRatioFitterU5BU5D_t5617  : public Array_t
{
};
// UnityEngine.UI.ILayoutController[]
// UnityEngine.UI.ILayoutController[]
struct ILayoutControllerU5BU5D_t5618  : public Array_t
{
};
// UnityEngine.UI.ILayoutSelfController[]
// UnityEngine.UI.ILayoutSelfController[]
struct ILayoutSelfControllerU5BU5D_t5619  : public Array_t
{
};
// UnityEngine.UI.AspectRatioFitter/AspectMode[]
// UnityEngine.UI.AspectRatioFitter/AspectMode[]
struct AspectModeU5BU5D_t5620  : public Array_t
{
};
// UnityEngine.UI.CanvasScaler[]
// UnityEngine.UI.CanvasScaler[]
struct CanvasScalerU5BU5D_t5621  : public Array_t
{
};
// UnityEngine.UI.CanvasScaler/ScaleMode[]
// UnityEngine.UI.CanvasScaler/ScaleMode[]
struct ScaleModeU5BU5D_t5622  : public Array_t
{
};
// UnityEngine.UI.CanvasScaler/ScreenMatchMode[]
// UnityEngine.UI.CanvasScaler/ScreenMatchMode[]
struct ScreenMatchModeU5BU5D_t5623  : public Array_t
{
};
// UnityEngine.UI.CanvasScaler/Unit[]
// UnityEngine.UI.CanvasScaler/Unit[]
struct UnitU5BU5D_t5624  : public Array_t
{
};
// UnityEngine.UI.ContentSizeFitter[]
// UnityEngine.UI.ContentSizeFitter[]
struct ContentSizeFitterU5BU5D_t5625  : public Array_t
{
};
// UnityEngine.UI.ContentSizeFitter/FitMode[]
// UnityEngine.UI.ContentSizeFitter/FitMode[]
struct FitModeU5BU5D_t5626  : public Array_t
{
};
// UnityEngine.UI.GridLayoutGroup[]
// UnityEngine.UI.GridLayoutGroup[]
struct GridLayoutGroupU5BU5D_t5627  : public Array_t
{
};
// UnityEngine.UI.LayoutGroup[]
// UnityEngine.UI.LayoutGroup[]
struct LayoutGroupU5BU5D_t5628  : public Array_t
{
};
// UnityEngine.UI.ILayoutGroup[]
// UnityEngine.UI.ILayoutGroup[]
struct ILayoutGroupU5BU5D_t5629  : public Array_t
{
};
// UnityEngine.UI.GridLayoutGroup/Corner[]
// UnityEngine.UI.GridLayoutGroup/Corner[]
struct CornerU5BU5D_t5630  : public Array_t
{
};
// UnityEngine.UI.GridLayoutGroup/Axis[]
// UnityEngine.UI.GridLayoutGroup/Axis[]
struct AxisU5BU5D_t5631  : public Array_t
{
};
// UnityEngine.UI.GridLayoutGroup/Constraint[]
// UnityEngine.UI.GridLayoutGroup/Constraint[]
struct ConstraintU5BU5D_t5632  : public Array_t
{
};
// UnityEngine.UI.HorizontalLayoutGroup[]
// UnityEngine.UI.HorizontalLayoutGroup[]
struct HorizontalLayoutGroupU5BU5D_t5633  : public Array_t
{
};
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup[]
// UnityEngine.UI.HorizontalOrVerticalLayoutGroup[]
struct HorizontalOrVerticalLayoutGroupU5BU5D_t5634  : public Array_t
{
};
// UnityEngine.UI.LayoutElement[]
// UnityEngine.UI.LayoutElement[]
struct LayoutElementU5BU5D_t5635  : public Array_t
{
};
// UnityEngine.UI.ILayoutIgnorer[]
// UnityEngine.UI.ILayoutIgnorer[]
struct ILayoutIgnorerU5BU5D_t5636  : public Array_t
{
};
// UnityEngine.UI.VerticalLayoutGroup[]
// UnityEngine.UI.VerticalLayoutGroup[]
struct VerticalLayoutGroupU5BU5D_t5637  : public Array_t
{
};
// UnityEngine.UI.Mask[]
// UnityEngine.UI.Mask[]
struct MaskU5BU5D_t5638  : public Array_t
{
};
// UnityEngine.UI.IGraphicEnabledDisabled[]
// UnityEngine.UI.IGraphicEnabledDisabled[]
struct IGraphicEnabledDisabledU5BU5D_t5639  : public Array_t
{
};
// UnityEngine.UI.IMask[]
// UnityEngine.UI.IMask[]
struct IMaskU5BU5D_t5640  : public Array_t
{
};
// UnityEngine.UI.IMaterialModifier[]
// UnityEngine.UI.IMaterialModifier[]
struct IMaterialModifierU5BU5D_t5641  : public Array_t
{
};
// UnityEngine.UI.BaseVertexEffect[]
// UnityEngine.UI.BaseVertexEffect[]
struct BaseVertexEffectU5BU5D_t5642  : public Array_t
{
};
// UnityEngine.UI.IVertexModifier[]
// UnityEngine.UI.IVertexModifier[]
struct IVertexModifierU5BU5D_t5643  : public Array_t
{
};
// UnityEngine.UI.Outline[]
// UnityEngine.UI.Outline[]
struct OutlineU5BU5D_t5644  : public Array_t
{
};
// UnityEngine.UI.Shadow[]
// UnityEngine.UI.Shadow[]
struct ShadowU5BU5D_t5645  : public Array_t
{
};
// UnityEngine.UI.PositionAsUV1[]
// UnityEngine.UI.PositionAsUV1[]
struct PositionAsUV1U5BU5D_t5646  : public Array_t
{
};
