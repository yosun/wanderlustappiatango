﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<ARVRModes>
struct UnityAction_1_t2708;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<ARVRModes>
struct InvokableCall_1_t2704  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<ARVRModes>::Delegate
	UnityAction_1_t2708 * ___Delegate_0;
};
