﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t506;

// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
 void TextAreaAttribute__ctor_m2460 (TextAreaAttribute_t506 * __this, int32_t ___minLines, int32_t ___maxLines, MethodInfo* method) IL2CPP_METHOD_ATTR;
