﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t603;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerable_1_t3788;
// System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerator_1_t3777;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ICollection_1_t3789;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ReadOnlyCollection_1_t3790;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t3765;
// System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>
struct Predicate_1_t3791;
// System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparison_1_t3792;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_45.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
 void List_1__ctor_m4602 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
 void List_1__ctor_m21212 (List_1_t603 * __this, Object_t* ___collection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Int32)
 void List_1__ctor_m21213 (List_1_t603 * __this, int32_t ___capacity, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.cctor()
 void List_1__cctor_m21214 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
 Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m21215 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void List_1_System_Collections_ICollection_CopyTo_m21216 (List_1_t603 * __this, Array_t * ___array, int32_t ___arrayIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m21217 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Add(System.Object)
 int32_t List_1_System_Collections_IList_Add_m21218 (List_1_t603 * __this, Object_t * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Contains(System.Object)
 bool List_1_System_Collections_IList_Contains_m21219 (List_1_t603 * __this, Object_t * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.IndexOf(System.Object)
 int32_t List_1_System_Collections_IList_IndexOf_m21220 (List_1_t603 * __this, Object_t * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Insert(System.Int32,System.Object)
 void List_1_System_Collections_IList_Insert_m21221 (List_1_t603 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Remove(System.Object)
 void List_1_System_Collections_IList_Remove_m21222 (List_1_t603 * __this, Object_t * ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
 bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21223 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_IsSynchronized()
 bool List_1_System_Collections_ICollection_get_IsSynchronized_m21224 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_SyncRoot()
 Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m21225 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsFixedSize()
 bool List_1_System_Collections_IList_get_IsFixedSize_m21226 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsReadOnly()
 bool List_1_System_Collections_IList_get_IsReadOnly_m21227 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_Item(System.Int32)
 Object_t * List_1_System_Collections_IList_get_Item_m21228 (List_1_t603 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.set_Item(System.Int32,System.Object)
 void List_1_System_Collections_IList_set_Item_m21229 (List_1_t603 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Add(T)
 void List_1_Add_m4604 (List_1_t603 * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GrowIfNeeded(System.Int32)
 void List_1_GrowIfNeeded_m21230 (List_1_t603 * __this, int32_t ___newCount, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddCollection(System.Collections.Generic.ICollection`1<T>)
 void List_1_AddCollection_m21231 (List_1_t603 * __this, Object_t* ___collection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
 void List_1_AddEnumerable_m21232 (List_1_t603 * __this, Object_t* ___enumerable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
 void List_1_AddRange_m21233 (List_1_t603 * __this, Object_t* ___collection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AsReadOnly()
 ReadOnlyCollection_1_t3790 * List_1_AsReadOnly_m21234 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Clear()
 void List_1_Clear_m21235 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(T)
 bool List_1_Contains_m4597 (List_1_t603 * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32)
 void List_1_CopyTo_m21236 (List_1_t603 * __this, PIXEL_FORMATU5BU5D_t3765* ___array, int32_t ___arrayIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Find(System.Predicate`1<T>)
 int32_t List_1_Find_m21237 (List_1_t603 * __this, Predicate_1_t3791 * ___match, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckMatch(System.Predicate`1<T>)
 void List_1_CheckMatch_m21238 (Object_t * __this/* static, unused */, Predicate_1_t3791 * ___match, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
 int32_t List_1_GetIndex_m21239 (List_1_t603 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t3791 * ___match, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator()
 Enumerator_t3793  List_1_GetEnumerator_m21240 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T)
 int32_t List_1_IndexOf_m21241 (List_1_t603 * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Shift(System.Int32,System.Int32)
 void List_1_Shift_m21242 (List_1_t603 * __this, int32_t ___start, int32_t ___delta, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckIndex(System.Int32)
 void List_1_CheckIndex_m21243 (List_1_t603 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Insert(System.Int32,T)
 void List_1_Insert_m21244 (List_1_t603 * __this, int32_t ___index, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
 void List_1_CheckCollection_m21245 (List_1_t603 * __this, Object_t* ___collection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Remove(T)
 bool List_1_Remove_m4603 (List_1_t603 * __this, int32_t ___item, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAll(System.Predicate`1<T>)
 int32_t List_1_RemoveAll_m21246 (List_1_t603 * __this, Predicate_1_t3791 * ___match, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAt(System.Int32)
 void List_1_RemoveAt_m21247 (List_1_t603 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Reverse()
 void List_1_Reverse_m21248 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort()
 void List_1_Sort_m21249 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort(System.Comparison`1<T>)
 void List_1_Sort_m21250 (List_1_t603 * __this, Comparison_1_t3792 * ___comparison, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T[] System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::ToArray()
 PIXEL_FORMATU5BU5D_t3765* List_1_ToArray_m21251 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::TrimExcess()
 void List_1_TrimExcess_m21252 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Capacity()
 int32_t List_1_get_Capacity_m21253 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Capacity(System.Int32)
 void List_1_set_Capacity_m21254 (List_1_t603 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Count()
 int32_t List_1_get_Count_m21255 (List_1_t603 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Item(System.Int32)
 int32_t List_1_get_Item_m21256 (List_1_t603 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Item(System.Int32,T)
 void List_1_set_Item_m21257 (List_1_t603 * __this, int32_t ___index, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
