﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_t3346;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
 void U3CStartU3Ec__Iterator0__ctor_m18173 (U3CStartU3Ec__Iterator0_t3346 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
 Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m18174 (U3CStartU3Ec__Iterator0_t3346 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
 Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m18175 (U3CStartU3Ec__Iterator0_t3346 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
 bool U3CStartU3Ec__Iterator0_MoveNext_m18176 (U3CStartU3Ec__Iterator0_t3346 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
 void U3CStartU3Ec__Iterator0_Dispose_m18177 (U3CStartU3Ec__Iterator0_t3346 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
 void U3CStartU3Ec__Iterator0_Reset_m18178 (U3CStartU3Ec__Iterator0_t3346 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
