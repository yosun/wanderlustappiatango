﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.ITextRecoEventHandler>
struct Comparer_1_t4359;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.ITextRecoEventHandler>
struct Comparer_1_t4359  : public Object_t
{
};
struct Comparer_1_t4359_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ITextRecoEventHandler>::_default
	Comparer_1_t4359 * ____default_0;
};
