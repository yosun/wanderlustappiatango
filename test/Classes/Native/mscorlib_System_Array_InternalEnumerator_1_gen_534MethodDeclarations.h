﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
struct InternalEnumerator_1_t4912;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Mono.Security.Protocol.Tls.CipherAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30142 (InternalEnumerator_1_t4912 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30143 (InternalEnumerator_1_t4912 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Dispose()
 void InternalEnumerator_1_Dispose_m30144 (InternalEnumerator_1_t4912 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30145 (InternalEnumerator_1_t4912 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30146 (InternalEnumerator_1_t4912 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
