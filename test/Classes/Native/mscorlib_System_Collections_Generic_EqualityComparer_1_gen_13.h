﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Font>
struct EqualityComparer_1_t3320;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Font>
struct EqualityComparer_1_t3320  : public Object_t
{
};
struct EqualityComparer_1_t3320_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Font>::_default
	EqualityComparer_1_t3320 * ____default_0;
};
