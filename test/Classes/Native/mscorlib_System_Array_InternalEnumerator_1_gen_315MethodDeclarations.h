﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.UInt16>
struct InternalEnumerator_1_t3945;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m22845 (InternalEnumerator_1_t3945 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22846 (InternalEnumerator_1_t3945 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
 void InternalEnumerator_1_Dispose_m22847 (InternalEnumerator_1_t3945 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m22848 (InternalEnumerator_1_t3945 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
 uint16_t InternalEnumerator_1_get_Current_m22849 (InternalEnumerator_1_t3945 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
