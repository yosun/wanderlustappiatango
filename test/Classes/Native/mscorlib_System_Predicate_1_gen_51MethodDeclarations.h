﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.GUILayoutEntry>
struct Predicate_1_t4527;
// System.Object
struct Object_t;
// UnityEngine.GUILayoutEntry
struct GUILayoutEntry_t961;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<UnityEngine.GUILayoutEntry>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m27815(__this, ___object, ___method, method) (void)Predicate_1__ctor_m14750_gshared((Predicate_1_t2845 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// System.Boolean System.Predicate`1<UnityEngine.GUILayoutEntry>::Invoke(T)
#define Predicate_1_Invoke_m27816(__this, ___obj, method) (bool)Predicate_1_Invoke_m14751_gshared((Predicate_1_t2845 *)__this, (Object_t *)___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.GUILayoutEntry>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m27817(__this, ___obj, ___callback, ___object, method) (Object_t *)Predicate_1_BeginInvoke_m14752_gshared((Predicate_1_t2845 *)__this, (Object_t *)___obj, (AsyncCallback_t200 *)___callback, (Object_t *)___object, method)
// System.Boolean System.Predicate`1<UnityEngine.GUILayoutEntry>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m27818(__this, ___result, method) (bool)Predicate_1_EndInvoke_m14753_gshared((Predicate_1_t2845 *)__this, (Object_t *)___result, method)
