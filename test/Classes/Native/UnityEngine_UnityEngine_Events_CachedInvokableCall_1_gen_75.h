﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.GridLayoutGroup>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_77.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.GridLayoutGroup>
struct CachedInvokableCall_1_t3570  : public InvokableCall_1_t3571
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.GridLayoutGroup>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
