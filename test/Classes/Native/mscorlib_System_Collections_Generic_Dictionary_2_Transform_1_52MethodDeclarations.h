﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordAbstractBehaviour,Vuforia.WordAbstractBehaviour>
struct Transform_1_t4003;
// System.Object
struct Object_t;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t60;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordAbstractBehaviour,Vuforia.WordAbstractBehaviour>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m23452 (Transform_1_t4003 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordAbstractBehaviour,Vuforia.WordAbstractBehaviour>::Invoke(TKey,TValue)
 WordAbstractBehaviour_t60 * Transform_1_Invoke_m23453 (Transform_1_t4003 * __this, int32_t ___key, WordAbstractBehaviour_t60 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordAbstractBehaviour,Vuforia.WordAbstractBehaviour>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m23454 (Transform_1_t4003 * __this, int32_t ___key, WordAbstractBehaviour_t60 * ___value, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.WordAbstractBehaviour,Vuforia.WordAbstractBehaviour>::EndInvoke(System.IAsyncResult)
 WordAbstractBehaviour_t60 * Transform_1_EndInvoke_m23455 (Transform_1_t4003 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
