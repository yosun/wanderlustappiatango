﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<UnityEngine.EventSystems.BaseInputModule>
struct Comparison_1_t3009;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t183;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<UnityEngine.EventSystems.BaseInputModule>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m15542(__this, ___object, ___method, method) (void)Comparison_1__ctor_m14775_gshared((Comparison_1_t2846 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.BaseInputModule>::Invoke(T,T)
#define Comparison_1_Invoke_m15543(__this, ___x, ___y, method) (int32_t)Comparison_1_Invoke_m14776_gshared((Comparison_1_t2846 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.EventSystems.BaseInputModule>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m15544(__this, ___x, ___y, ___callback, ___object, method) (Object_t *)Comparison_1_BeginInvoke_m14777_gshared((Comparison_1_t2846 *)__this, (Object_t *)___x, (Object_t *)___y, (AsyncCallback_t200 *)___callback, (Object_t *)___object, method)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.BaseInputModule>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m15545(__this, ___result, method) (int32_t)Comparison_1_EndInvoke_m14778_gshared((Comparison_1_t2846 *)__this, (Object_t *)___result, method)
