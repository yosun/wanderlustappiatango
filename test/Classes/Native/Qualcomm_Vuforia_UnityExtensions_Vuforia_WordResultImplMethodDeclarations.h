﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordResultImpl
struct WordResultImpl_t703;
// Vuforia.Word
struct Word_t691;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox.h"
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"

// System.Void Vuforia.WordResultImpl::.ctor(Vuforia.Word)
 void WordResultImpl__ctor_m3119 (WordResultImpl_t703 * __this, Object_t * ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Word Vuforia.WordResultImpl::get_Word()
 Object_t * WordResultImpl_get_Word_m3120 (WordResultImpl_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.WordResultImpl::get_Position()
 Vector3_t13  WordResultImpl_get_Position_m3121 (WordResultImpl_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Vuforia.WordResultImpl::get_Orientation()
 Quaternion_t12  WordResultImpl_get_Orientation_m3122 (WordResultImpl_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.OrientedBoundingBox Vuforia.WordResultImpl::get_Obb()
 OrientedBoundingBox_t590  WordResultImpl_get_Obb_m3123 (WordResultImpl_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TrackableBehaviour/Status Vuforia.WordResultImpl::get_CurrentStatus()
 int32_t WordResultImpl_get_CurrentStatus_m3124 (WordResultImpl_t703 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordResultImpl::SetPose(UnityEngine.Vector3,UnityEngine.Quaternion)
 void WordResultImpl_SetPose_m3125 (WordResultImpl_t703 * __this, Vector3_t13  ___position, Quaternion_t12  ___orientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordResultImpl::SetObb(Vuforia.OrientedBoundingBox)
 void WordResultImpl_SetObb_m3126 (WordResultImpl_t703 * __this, OrientedBoundingBox_t590  ___obb, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordResultImpl::SetStatus(Vuforia.TrackableBehaviour/Status)
 void WordResultImpl_SetStatus_m3127 (WordResultImpl_t703 * __this, int32_t ___status, MethodInfo* method) IL2CPP_METHOD_ATTR;
