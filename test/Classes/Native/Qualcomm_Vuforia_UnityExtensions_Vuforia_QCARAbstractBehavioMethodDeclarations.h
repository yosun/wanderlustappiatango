﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARAbstractBehaviour
struct QCARAbstractBehaviour_t71;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t44;
// UnityEngine.Camera
struct Camera_t3;
// System.String
struct String_t;
// System.Action`1<Vuforia.QCARUnity/InitError>
struct Action_1_t127;
// System.Action
struct Action_t148;
// System.Action`1<System.Boolean>
struct Action_1_t758;
// Vuforia.ITrackerEventHandler
struct ITrackerEventHandler_t761;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t122;
// UnityEngine.GameObject
struct GameObject_t2;
// Vuforia.IUnityPlayer
struct IUnityPlayer_t140;
// Vuforia.QCARAbstractBehaviour/WorldCenterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio_0.h"
// Vuforia.CameraDevice/CameraDeviceMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// Vuforia.CameraDevice/CameraDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0.h"
// Vuforia.QCARRenderer/VideoBackgroundReflection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_VideoB.h"

// System.Boolean Vuforia.QCARAbstractBehaviour::get_AutoAdjustStereoCameraSkewing()
 bool QCARAbstractBehaviour_get_AutoAdjustStereoCameraSkewing_m4093 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARAbstractBehaviour::get_SceneScaleFactor()
 float QCARAbstractBehaviour_get_SceneScaleFactor_m4094 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_SceneScaleFactor(System.Single)
 void QCARAbstractBehaviour_set_SceneScaleFactor_m4095 (QCARAbstractBehaviour_t71 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARAbstractBehaviour::get_CameraOffset()
 float QCARAbstractBehaviour_get_CameraOffset_m4096 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_CameraOffset(System.Single)
 void QCARAbstractBehaviour_set_CameraOffset_m4097 (QCARAbstractBehaviour_t71 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARAbstractBehaviour/WorldCenterMode Vuforia.QCARAbstractBehaviour::get_WorldCenterModeSetting()
 int32_t QCARAbstractBehaviour_get_WorldCenterModeSetting_m4098 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.TrackableBehaviour Vuforia.QCARAbstractBehaviour::get_WorldCenter()
 TrackableBehaviour_t44 * QCARAbstractBehaviour_get_WorldCenter_m4099 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::get_VideoBackGroundMirrored()
 bool QCARAbstractBehaviour_get_VideoBackGroundMirrored_m4100 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_VideoBackGroundMirrored(System.Boolean)
 void QCARAbstractBehaviour_set_VideoBackGroundMirrored_m4101 (QCARAbstractBehaviour_t71 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/CameraDeviceMode Vuforia.QCARAbstractBehaviour::get_CameraDeviceMode()
 int32_t QCARAbstractBehaviour_get_CameraDeviceMode_m4102 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::get_HasStarted()
 bool QCARAbstractBehaviour_get_HasStarted_m4103 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::get_IsStereoRendering()
 bool QCARAbstractBehaviour_get_IsStereoRendering_m4104 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera Vuforia.QCARAbstractBehaviour::get_PrimaryCamera()
 Camera_t3 * QCARAbstractBehaviour_get_PrimaryCamera_m4105 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_PrimaryCamera(UnityEngine.Camera)
 void QCARAbstractBehaviour_set_PrimaryCamera_m4106 (QCARAbstractBehaviour_t71 * __this, Camera_t3 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera Vuforia.QCARAbstractBehaviour::get_SecondaryCamera()
 Camera_t3 * QCARAbstractBehaviour_get_SecondaryCamera_m4107 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::set_SecondaryCamera(UnityEngine.Camera)
 void QCARAbstractBehaviour_set_SecondaryCamera_m4108 (QCARAbstractBehaviour_t71 * __this, Camera_t3 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.QCARAbstractBehaviour::get_AppLicenseKey()
 String_t* QCARAbstractBehaviour_get_AppLicenseKey_m4109 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetAutoAdjustStereoCameraSkewing(System.Boolean)
 void QCARAbstractBehaviour_SetAutoAdjustStereoCameraSkewing_m4110 (QCARAbstractBehaviour_t71 * __this, bool ___setSkewing, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetSceneScaleFactor(System.Single)
 void QCARAbstractBehaviour_SetSceneScaleFactor_m4111 (QCARAbstractBehaviour_t71 * __this, float ___Scale, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterQCARInitErrorCallback(System.Action`1<Vuforia.QCARUnity/InitError>)
 void QCARAbstractBehaviour_RegisterQCARInitErrorCallback_m399 (QCARAbstractBehaviour_t71 * __this, Action_1_t127 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterQCARInitErrorCallback(System.Action`1<Vuforia.QCARUnity/InitError>)
 void QCARAbstractBehaviour_UnregisterQCARInitErrorCallback_m400 (QCARAbstractBehaviour_t71 * __this, Action_1_t127 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterQCARInitializedCallback(System.Action)
 void QCARAbstractBehaviour_RegisterQCARInitializedCallback_m4112 (QCARAbstractBehaviour_t71 * __this, Action_t148 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterQCARInitializedCallback(System.Action)
 void QCARAbstractBehaviour_UnregisterQCARInitializedCallback_m4113 (QCARAbstractBehaviour_t71 * __this, Action_t148 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterQCARStartedCallback(System.Action)
 void QCARAbstractBehaviour_RegisterQCARStartedCallback_m4114 (QCARAbstractBehaviour_t71 * __this, Action_t148 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterQCARStartedCallback(System.Action)
 void QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4115 (QCARAbstractBehaviour_t71 * __this, Action_t148 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterTrackablesUpdatedCallback(System.Action)
 void QCARAbstractBehaviour_RegisterTrackablesUpdatedCallback_m4116 (QCARAbstractBehaviour_t71 * __this, Action_t148 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterTrackablesUpdatedCallback(System.Action)
 void QCARAbstractBehaviour_UnregisterTrackablesUpdatedCallback_m4117 (QCARAbstractBehaviour_t71 * __this, Action_t148 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterOnPauseCallback(System.Action`1<System.Boolean>)
 void QCARAbstractBehaviour_RegisterOnPauseCallback_m4118 (QCARAbstractBehaviour_t71 * __this, Action_1_t758 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterOnPauseCallback(System.Action`1<System.Boolean>)
 void QCARAbstractBehaviour_UnregisterOnPauseCallback_m4119 (QCARAbstractBehaviour_t71 * __this, Action_1_t758 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetEditorValues(System.Single)
 void QCARAbstractBehaviour_SetEditorValues_m4120 (QCARAbstractBehaviour_t71 * __this, float ___Offset, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterTrackerEventHandler(Vuforia.ITrackerEventHandler)
 void QCARAbstractBehaviour_RegisterTrackerEventHandler_m4121 (QCARAbstractBehaviour_t71 * __this, Object_t * ___trackerEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::UnregisterTrackerEventHandler(Vuforia.ITrackerEventHandler)
 bool QCARAbstractBehaviour_UnregisterTrackerEventHandler_m4122 (QCARAbstractBehaviour_t71 * __this, Object_t * ___trackerEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterVideoBgEventHandler(Vuforia.IVideoBackgroundEventHandler)
 void QCARAbstractBehaviour_RegisterVideoBgEventHandler_m4123 (QCARAbstractBehaviour_t71 * __this, Object_t * ___videoBgEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::UnregisterVideoBgEventHandler(Vuforia.IVideoBackgroundEventHandler)
 bool QCARAbstractBehaviour_UnregisterVideoBgEventHandler_m4124 (QCARAbstractBehaviour_t71 * __this, Object_t * ___videoBgEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetWorldCenterMode(Vuforia.QCARAbstractBehaviour/WorldCenterMode)
 void QCARAbstractBehaviour_SetWorldCenterMode_m4125 (QCARAbstractBehaviour_t71 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetWorldCenter(Vuforia.TrackableBehaviour)
 void QCARAbstractBehaviour_SetWorldCenter_m4126 (QCARAbstractBehaviour_t71 * __this, TrackableBehaviour_t44 * ___trackable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetAppLicenseKey(System.String)
 void QCARAbstractBehaviour_SetAppLicenseKey_m4127 (QCARAbstractBehaviour_t71 * __this, String_t* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.QCARAbstractBehaviour::GetViewportRectangle()
 Rect_t118  QCARAbstractBehaviour_GetViewportRectangle_m4128 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation Vuforia.QCARAbstractBehaviour::GetSurfaceOrientation()
 int32_t QCARAbstractBehaviour_GetSurfaceOrientation_m4129 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ConfigureVideoBackground(System.Boolean)
 void QCARAbstractBehaviour_ConfigureVideoBackground_m4130 (QCARAbstractBehaviour_t71 * __this, bool ___forceReflectionSetting, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
 void QCARAbstractBehaviour_ResetBackgroundPlane_m4131 (QCARAbstractBehaviour_t71 * __this, bool ___disable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::RegisterRenderOnUpdateCallback(System.Action)
 void QCARAbstractBehaviour_RegisterRenderOnUpdateCallback_m4132 (QCARAbstractBehaviour_t71 * __this, Action_t148 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UnregisterRenderOnUpdateCallback(System.Action)
 void QCARAbstractBehaviour_UnregisterRenderOnUpdateCallback_m4133 (QCARAbstractBehaviour_t71 * __this, Action_t148 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ConfigureView()
 void QCARAbstractBehaviour_ConfigureView_m4134 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::EnableObjectRenderer(UnityEngine.GameObject,System.Boolean)
 void QCARAbstractBehaviour_EnableObjectRenderer_m4135 (QCARAbstractBehaviour_t71 * __this, GameObject_t2 * ___go, bool ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Start()
 void QCARAbstractBehaviour_Start_m4136 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::OnEnable()
 void QCARAbstractBehaviour_OnEnable_m4137 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UpdateView()
 void QCARAbstractBehaviour_UpdateView_m4138 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Update()
 void QCARAbstractBehaviour_Update_m4139 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::OnApplicationPause(System.Boolean)
 void QCARAbstractBehaviour_OnApplicationPause_m4140 (QCARAbstractBehaviour_t71 * __this, bool ___pause, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::OnDisable()
 void QCARAbstractBehaviour_OnDisable_m4141 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::OnDestroy()
 void QCARAbstractBehaviour_OnDestroy_m4142 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::SetUnityPlayerImplementation(Vuforia.IUnityPlayer)
 void QCARAbstractBehaviour_SetUnityPlayerImplementation_m542 (QCARAbstractBehaviour_t71 * __this, Object_t * ___implementation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::StartQCAR(System.Boolean,System.Boolean)
 bool QCARAbstractBehaviour_StartQCAR_m4143 (QCARAbstractBehaviour_t71 * __this, bool ___startObjectTracker, bool ___startMarkerTracker, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::StopQCAR()
 bool QCARAbstractBehaviour_StopQCAR_m4144 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UpdateStereoDepth()
 void QCARAbstractBehaviour_UpdateStereoDepth_m4145 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ProjectionMatricesUpdated()
 void QCARAbstractBehaviour_ProjectionMatricesUpdated_m4146 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::ApplyMatrix(UnityEngine.Camera,UnityEngine.Matrix4x4)
 void QCARAbstractBehaviour_ApplyMatrix_m4147 (QCARAbstractBehaviour_t71 * __this, Camera_t3 * ___cam, Matrix4x4_t176  ___inputMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::UpdateProjection(UnityEngine.ScreenOrientation)
 void QCARAbstractBehaviour_UpdateProjection_m4148 (QCARAbstractBehaviour_t71 * __this, int32_t ___orientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::DeinitRequestedTrackers()
 void QCARAbstractBehaviour_DeinitRequestedTrackers_m4149 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::CheckSceneScaleFactor()
 void QCARAbstractBehaviour_CheckSceneScaleFactor_m4150 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::CheckForSurfaceChanges()
 void QCARAbstractBehaviour_CheckForSurfaceChanges_m4151 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetCameraDeviceMode(Vuforia.CameraDevice/CameraDeviceMode)
 void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDeviceMode_m544 (QCARAbstractBehaviour_t71 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetMaximumSimultaneousImageTargets()
 int32_t QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousImageTargets_m545 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetMaximumSimultaneousImageTargets(System.Int32)
 void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousImageTargets_m546 (QCARAbstractBehaviour_t71 * __this, int32_t ___max, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetMaximumSimultaneousObjectTargets()
 int32_t QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMaximumSimultaneousObjectTargets_m547 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetMaximumSimultaneousObjectTargets(System.Int32)
 void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMaximumSimultaneousObjectTargets_m548 (QCARAbstractBehaviour_t71 * __this, int32_t ___max, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetDelayedLoadingObjectTargets()
 bool QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetDelayedLoadingObjectTargets_m549 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetUseDelayedLoadingObjectTargets(System.Boolean)
 void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetUseDelayedLoadingObjectTargets_m550 (QCARAbstractBehaviour_t71 * __this, bool ___useDelayedLoading, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/CameraDirection Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetCameraDirection()
 int32_t QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetCameraDirection_m551 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetCameraDirection(Vuforia.CameraDevice/CameraDirection)
 void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetCameraDirection_m552 (QCARAbstractBehaviour_t71 * __this, int32_t ___cameraDirection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARRenderer/VideoBackgroundReflection Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.GetMirrorVideoBackground()
 int32_t QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_GetMirrorVideoBackground_m553 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::Vuforia.IEditorQCARBehaviour.SetMirrorVideoBackground(Vuforia.QCARRenderer/VideoBackgroundReflection)
 void QCARAbstractBehaviour_Vuforia_IEditorQCARBehaviour_SetMirrorVideoBackground_m554 (QCARAbstractBehaviour_t71 * __this, int32_t ___reflection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARAbstractBehaviour::.ctor()
 void QCARAbstractBehaviour__ctor_m537 (QCARAbstractBehaviour_t71 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
