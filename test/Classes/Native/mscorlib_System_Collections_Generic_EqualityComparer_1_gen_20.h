﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.CanvasGroup>
struct EqualityComparer_1_t3491;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.CanvasGroup>
struct EqualityComparer_1_t3491  : public Object_t
{
};
struct EqualityComparer_1_t3491_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.CanvasGroup>::_default
	EqualityComparer_1_t3491 * ____default_0;
};
