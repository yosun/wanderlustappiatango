﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.DayOfWeek>
struct InternalEnumerator_1_t5139;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.DayOfWeek
#include "mscorlib_System_DayOfWeek.h"

// System.Void System.Array/InternalEnumerator`1<System.DayOfWeek>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31351 (InternalEnumerator_1_t5139 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.DayOfWeek>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31352 (InternalEnumerator_1_t5139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.DayOfWeek>::Dispose()
 void InternalEnumerator_1_Dispose_m31353 (InternalEnumerator_1_t5139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.DayOfWeek>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31354 (InternalEnumerator_1_t5139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.DayOfWeek>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31355 (InternalEnumerator_1_t5139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
