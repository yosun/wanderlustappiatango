﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>
struct GenericComparer_1_t2663;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::.ctor()
 void GenericComparer_1__ctor_m13948 (GenericComparer_1_t2663 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTimeOffset>::Compare(T,T)
 int32_t GenericComparer_1_Compare_m31330 (GenericComparer_1_t2663 * __this, DateTimeOffset_t2198  ___x, DateTimeOffset_t2198  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
