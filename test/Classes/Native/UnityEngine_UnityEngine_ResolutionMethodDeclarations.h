﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Resolution
struct Resolution_t941;

// System.Int32 UnityEngine.Resolution::get_width()
 int32_t Resolution_get_width_m5539 (Resolution_t941 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Resolution::set_width(System.Int32)
 void Resolution_set_width_m5540 (Resolution_t941 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_height()
 int32_t Resolution_get_height_m5541 (Resolution_t941 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Resolution::set_height(System.Int32)
 void Resolution_set_height_m5542 (Resolution_t941 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_refreshRate()
 int32_t Resolution_get_refreshRate_m5543 (Resolution_t941 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Resolution::set_refreshRate(System.Int32)
 void Resolution_set_refreshRate_m5544 (Resolution_t941 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
