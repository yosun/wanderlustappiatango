﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
struct List_1_t1007;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Physics2D
struct Physics2D_t444  : public Object_t
{
};
struct Physics2D_t444_StaticFields{
	// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D> UnityEngine.Physics2D::m_LastDisabledRigidbody2D
	List_1_t1007 * ___m_LastDisabledRigidbody2D_0;
};
