﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DefaultInitializationErrorHandler
struct DefaultInitializationErrorHandler_t37;
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void Vuforia.DefaultInitializationErrorHandler::.ctor()
 void DefaultInitializationErrorHandler__ctor_m90 (DefaultInitializationErrorHandler_t37 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::Awake()
 void DefaultInitializationErrorHandler_Awake_m91 (DefaultInitializationErrorHandler_t37 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnGUI()
 void DefaultInitializationErrorHandler_OnGUI_m92 (DefaultInitializationErrorHandler_t37 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnDestroy()
 void DefaultInitializationErrorHandler_OnDestroy_m93 (DefaultInitializationErrorHandler_t37 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::DrawWindowContent(System.Int32)
 void DefaultInitializationErrorHandler_DrawWindowContent_m94 (DefaultInitializationErrorHandler_t37 * __this, int32_t ___id, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorCode(Vuforia.QCARUnity/InitError)
 void DefaultInitializationErrorHandler_SetErrorCode_m95 (DefaultInitializationErrorHandler_t37 * __this, int32_t ___errorCode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::SetErrorOccurred(System.Boolean)
 void DefaultInitializationErrorHandler_SetErrorOccurred_m96 (DefaultInitializationErrorHandler_t37 * __this, bool ___errorOccurred, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultInitializationErrorHandler::OnQCARInitializationError(Vuforia.QCARUnity/InitError)
 void DefaultInitializationErrorHandler_OnQCARInitializationError_m97 (DefaultInitializationErrorHandler_t37 * __this, int32_t ___initError, MethodInfo* method) IL2CPP_METHOD_ATTR;
