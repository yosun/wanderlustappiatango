﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_101.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionAbstractBehaviour>
struct CachedInvokableCall_1_t4061  : public InvokableCall_1_t4062
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
