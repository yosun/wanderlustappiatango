﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.WrapperlessIcall
struct WrapperlessIcall_t1033;

// System.Void UnityEngine.WrapperlessIcall::.ctor()
 void WrapperlessIcall__ctor_m6234 (WrapperlessIcall_t1033 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
