﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VideoTextureRendererAbstractBehaviour
struct VideoTextureRendererAbstractBehaviour_t86;

// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Awake()
 void VideoTextureRendererAbstractBehaviour_Awake_m4211 (VideoTextureRendererAbstractBehaviour_t86 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Start()
 void VideoTextureRendererAbstractBehaviour_Start_m4212 (VideoTextureRendererAbstractBehaviour_t86 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Update()
 void VideoTextureRendererAbstractBehaviour_Update_m4213 (VideoTextureRendererAbstractBehaviour_t86 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnDestroy()
 void VideoTextureRendererAbstractBehaviour_OnDestroy_m4214 (VideoTextureRendererAbstractBehaviour_t86 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnVideoBackgroundConfigChanged()
 void VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m614 (VideoTextureRendererAbstractBehaviour_t86 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::.ctor()
 void VideoTextureRendererAbstractBehaviour__ctor_m613 (VideoTextureRendererAbstractBehaviour_t86 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
