﻿#pragma once
#include <stdint.h>
// UnityEngine.Camera
struct Camera_t3;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.Camera>
struct CastHelper_1_t2764 
{
	// T UnityEngine.CastHelper`1<UnityEngine.Camera>::t
	Camera_t3 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.Camera>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
