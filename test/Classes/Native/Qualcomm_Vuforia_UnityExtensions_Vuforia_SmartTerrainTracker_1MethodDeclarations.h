﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackerImpl
struct SmartTerrainTrackerImpl_t676;
// Vuforia.SmartTerrainBuilder
struct SmartTerrainBuilder_t574;

// System.Single Vuforia.SmartTerrainTrackerImpl::get_ScaleToMillimeter()
 float SmartTerrainTrackerImpl_get_ScaleToMillimeter_m3064 (SmartTerrainTrackerImpl_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerImpl::SetScaleToMillimeter(System.Single)
 bool SmartTerrainTrackerImpl_SetScaleToMillimeter_m3065 (SmartTerrainTrackerImpl_t676 * __this, float ___scaleFactor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SmartTerrainBuilder Vuforia.SmartTerrainTrackerImpl::get_SmartTerrainBuilder()
 SmartTerrainBuilder_t574 * SmartTerrainTrackerImpl_get_SmartTerrainBuilder_m3066 (SmartTerrainTrackerImpl_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerImpl::Start()
 bool SmartTerrainTrackerImpl_Start_m3067 (SmartTerrainTrackerImpl_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::Stop()
 void SmartTerrainTrackerImpl_Stop_m3068 (SmartTerrainTrackerImpl_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerImpl::.ctor()
 void SmartTerrainTrackerImpl__ctor_m3069 (SmartTerrainTrackerImpl_t676 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
