﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>
struct InternalEnumerator_1_t5032;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.CallingConventions
#include "mscorlib_System_Reflection_CallingConventions.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30742 (InternalEnumerator_1_t5032 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30743 (InternalEnumerator_1_t5032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::Dispose()
 void InternalEnumerator_1_Dispose_m30744 (InternalEnumerator_1_t5032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30745 (InternalEnumerator_1_t5032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Reflection.CallingConventions>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30746 (InternalEnumerator_1_t5032 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
