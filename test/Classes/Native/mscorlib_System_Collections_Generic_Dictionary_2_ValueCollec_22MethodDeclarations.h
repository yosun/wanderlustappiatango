﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>
struct Enumerator_t875;
// System.Object
struct Object_t;
// Vuforia.ImageTarget
struct ImageTarget_t572;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ImageTarget>
struct Dictionary_2_t738;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void Enumerator__ctor_m25759 (Enumerator_t875 * __this, Dictionary_2_t738 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25760 (Enumerator_t875 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::Dispose()
 void Enumerator_Dispose_m25761 (Enumerator_t875 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::MoveNext()
 bool Enumerator_MoveNext_m5276 (Enumerator_t875 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::get_Current()
 Object_t * Enumerator_get_Current_m5275 (Enumerator_t875 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
