﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.FontUpdateTracker
struct FontUpdateTracker_t283;
// UnityEngine.UI.Text
struct Text_t284;
// UnityEngine.Font
struct Font_t280;

// System.Void UnityEngine.UI.FontUpdateTracker::.cctor()
 void FontUpdateTracker__cctor_m1042 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FontUpdateTracker::TrackText(UnityEngine.UI.Text)
 void FontUpdateTracker_TrackText_m1043 (Object_t * __this/* static, unused */, Text_t284 * ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FontUpdateTracker::RebuildForFont(UnityEngine.Font)
 void FontUpdateTracker_RebuildForFont_m1044 (Object_t * __this/* static, unused */, Font_t280 * ___f, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.FontUpdateTracker::UntrackText(UnityEngine.UI.Text)
 void FontUpdateTracker_UntrackText_m1045 (Object_t * __this/* static, unused */, Text_t284 * ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
