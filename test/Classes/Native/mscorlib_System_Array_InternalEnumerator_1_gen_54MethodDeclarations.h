﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>
struct InternalEnumerator_1_t2813;
// System.Object
struct Object_t;
// Vuforia.DefaultTrackableEventHandler
struct DefaultTrackableEventHandler_t45;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m14442(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14443(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::Dispose()
#define InternalEnumerator_1_Dispose_m14444(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14445(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::get_Current()
#define InternalEnumerator_1_get_Current_m14446(__this, method) (DefaultTrackableEventHandler_t45 *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
