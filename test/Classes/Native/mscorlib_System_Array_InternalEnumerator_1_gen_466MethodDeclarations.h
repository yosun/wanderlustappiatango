﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>
struct InternalEnumerator_1_t4771;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29218 (InternalEnumerator_1_t4771 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29219 (InternalEnumerator_1_t4771 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::Dispose()
 void InternalEnumerator_1_Dispose_m29220 (InternalEnumerator_1_t4771 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29221 (InternalEnumerator_1_t4771 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29222 (InternalEnumerator_1_t4771 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
