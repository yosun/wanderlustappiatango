﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AudioClip
struct AudioClip_t1011;
// System.Single[]
struct SingleU5BU5D_t578;

// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
 void AudioClip_InvokePCMReaderCallback_Internal_m6115 (AudioClip_t1011 * __this, SingleU5BU5D_t578* ___data, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
 void AudioClip_InvokePCMSetPositionCallback_Internal_m6116 (AudioClip_t1011 * __this, int32_t ___position, MethodInfo* method) IL2CPP_METHOD_ATTR;
