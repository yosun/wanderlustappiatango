﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.DSA
struct DSA_t1355;
// System.Security.Cryptography.AsymmetricSignatureDeformatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureDef.h"
// System.Security.Cryptography.DSASignatureDeformatter
struct DSASignatureDeformatter_t1669  : public AsymmetricSignatureDeformatter_t1619
{
	// System.Security.Cryptography.DSA System.Security.Cryptography.DSASignatureDeformatter::dsa
	DSA_t1355 * ___dsa_0;
};
