﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUILayout
struct GUILayout_t954;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t955;

// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Width(System.Single)
 GUILayoutOption_t955 * GUILayout_Width_m5593 (Object_t * __this/* static, unused */, float ___width, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::Height(System.Single)
 GUILayoutOption_t955 * GUILayout_Height_m5594 (Object_t * __this/* static, unused */, float ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
