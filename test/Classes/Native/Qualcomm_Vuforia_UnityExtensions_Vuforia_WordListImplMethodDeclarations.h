﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordListImpl
struct WordListImpl_t704;
// System.String
struct String_t;
// Vuforia.DataSet/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSet_StorageType.h"
// Vuforia.QCARUnity/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_StorageTy.h"
// Vuforia.WordFilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterMode.h"

// System.Boolean Vuforia.WordListImpl::LoadWordListFile(System.String)
 bool WordListImpl_LoadWordListFile_m3129 (WordListImpl_t704 * __this, String_t* ___relativePath, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordListImpl::LoadWordListFile(System.String,Vuforia.DataSet/StorageType)
 bool WordListImpl_LoadWordListFile_m3130 (WordListImpl_t704 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordListImpl::LoadWordListFile(System.String,Vuforia.QCARUnity/StorageType)
 bool WordListImpl_LoadWordListFile_m3131 (WordListImpl_t704 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WordListImpl::AddWordsFromFile(System.String)
 int32_t WordListImpl_AddWordsFromFile_m3132 (WordListImpl_t704 * __this, String_t* ___relativePath, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WordListImpl::AddWordsFromFile(System.String,Vuforia.DataSet/StorageType)
 int32_t WordListImpl_AddWordsFromFile_m3133 (WordListImpl_t704 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WordListImpl::AddWordsFromFile(System.String,Vuforia.QCARUnity/StorageType)
 int32_t WordListImpl_AddWordsFromFile_m3134 (WordListImpl_t704 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordListImpl::AddWord(System.String)
 bool WordListImpl_AddWord_m3135 (WordListImpl_t704 * __this, String_t* ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordListImpl::RemoveWord(System.String)
 bool WordListImpl_RemoveWord_m3136 (WordListImpl_t704 * __this, String_t* ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordListImpl::ContainsWord(System.String)
 bool WordListImpl_ContainsWord_m3137 (WordListImpl_t704 * __this, String_t* ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordListImpl::UnloadAllLists()
 bool WordListImpl_UnloadAllLists_m3138 (WordListImpl_t704 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordFilterMode Vuforia.WordListImpl::GetFilterMode()
 int32_t WordListImpl_GetFilterMode_m3139 (WordListImpl_t704 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordListImpl::SetFilterMode(Vuforia.WordFilterMode)
 bool WordListImpl_SetFilterMode_m3140 (WordListImpl_t704 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordListImpl::LoadFilterListFile(System.String)
 bool WordListImpl_LoadFilterListFile_m3141 (WordListImpl_t704 * __this, String_t* ___relativePath, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordListImpl::LoadFilterListFile(System.String,Vuforia.DataSet/StorageType)
 bool WordListImpl_LoadFilterListFile_m3142 (WordListImpl_t704 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordListImpl::LoadFilterListFile(System.String,Vuforia.QCARUnity/StorageType)
 bool WordListImpl_LoadFilterListFile_m3143 (WordListImpl_t704 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordListImpl::AddWordToFilterList(System.String)
 bool WordListImpl_AddWordToFilterList_m3144 (WordListImpl_t704 * __this, String_t* ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordListImpl::RemoveWordFromFilterList(System.String)
 bool WordListImpl_RemoveWordFromFilterList_m3145 (WordListImpl_t704 * __this, String_t* ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordListImpl::ClearFilterList()
 bool WordListImpl_ClearFilterList_m3146 (WordListImpl_t704 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.WordListImpl::GetFilterListWordCount()
 int32_t WordListImpl_GetFilterListWordCount_m3147 (WordListImpl_t704 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.WordListImpl::GetFilterListWord(System.Int32)
 String_t* WordListImpl_GetFilterListWord_m3148 (WordListImpl_t704 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordListImpl::.ctor()
 void WordListImpl__ctor_m3149 (WordListImpl_t704 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
