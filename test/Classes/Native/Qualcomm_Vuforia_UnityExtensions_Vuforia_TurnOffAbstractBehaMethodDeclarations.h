﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TurnOffAbstractBehaviour
struct TurnOffAbstractBehaviour_t57;

// System.Void Vuforia.TurnOffAbstractBehaviour::.ctor()
 void TurnOffAbstractBehaviour__ctor_m606 (TurnOffAbstractBehaviour_t57 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
