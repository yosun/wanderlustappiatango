﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t244;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.EventSystems.BaseRaycaster>
struct Comparison_1_t3076  : public MulticastDelegate_t325
{
};
