﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Scrollbar>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_64.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Scrollbar>
struct CachedInvokableCall_1_t3449  : public InvokableCall_1_t3450
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Scrollbar>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
