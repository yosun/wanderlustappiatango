﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.IUserDefinedTargetEventHandler>
struct EqualityComparer_1_t4377;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.IUserDefinedTargetEventHandler>
struct EqualityComparer_1_t4377  : public Object_t
{
};
struct EqualityComparer_1_t4377_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.IUserDefinedTargetEventHandler>::_default
	EqualityComparer_1_t4377 * ____default_0;
};
