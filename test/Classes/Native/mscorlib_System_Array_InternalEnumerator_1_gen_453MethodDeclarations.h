﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>
struct InternalEnumerator_1_t4745;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SocialPlatforms.TimeScope
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29092 (InternalEnumerator_1_t4745 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29093 (InternalEnumerator_1_t4745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::Dispose()
 void InternalEnumerator_1_Dispose_m29094 (InternalEnumerator_1_t4745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29095 (InternalEnumerator_1_t4745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.TimeScope>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29096 (InternalEnumerator_1_t4745 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
