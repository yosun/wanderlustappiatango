﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.TripleDESCryptoServiceProvider
struct TripleDESCryptoServiceProvider_t2126;
// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t1555;
// System.Byte[]
struct ByteU5BU5D_t609;

// System.Void System.Security.Cryptography.TripleDESCryptoServiceProvider::.ctor()
 void TripleDESCryptoServiceProvider__ctor_m12138 (TripleDESCryptoServiceProvider_t2126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.TripleDESCryptoServiceProvider::GenerateIV()
 void TripleDESCryptoServiceProvider_GenerateIV_m12139 (TripleDESCryptoServiceProvider_t2126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.TripleDESCryptoServiceProvider::GenerateKey()
 void TripleDESCryptoServiceProvider_GenerateKey_m12140 (TripleDESCryptoServiceProvider_t2126 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.TripleDESCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
 Object_t * TripleDESCryptoServiceProvider_CreateDecryptor_m12141 (TripleDESCryptoServiceProvider_t2126 * __this, ByteU5BU5D_t609* ___rgbKey, ByteU5BU5D_t609* ___rgbIV, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.TripleDESCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
 Object_t * TripleDESCryptoServiceProvider_CreateEncryptor_m12142 (TripleDESCryptoServiceProvider_t2126 * __this, ByteU5BU5D_t609* ___rgbKey, ByteU5BU5D_t609* ___rgbIV, MethodInfo* method) IL2CPP_METHOD_ATTR;
