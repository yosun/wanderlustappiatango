﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.X509.X509Crl/X509CrlEntry
struct X509CrlEntry_t1379;
// System.Byte[]
struct ByteU5BU5D_t609;
// Mono.Security.X509.X509ExtensionCollection
struct X509ExtensionCollection_t1519;
// Mono.Security.ASN1
struct ASN1_t1365;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void Mono.Security.X509.X509Crl/X509CrlEntry::.ctor(Mono.Security.ASN1)
 void X509CrlEntry__ctor_m8210 (X509CrlEntry_t1379 * __this, ASN1_t1365 * ___entry, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.X509Crl/X509CrlEntry::get_SerialNumber()
 ByteU5BU5D_t609* X509CrlEntry_get_SerialNumber_m8211 (X509CrlEntry_t1379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Mono.Security.X509.X509Crl/X509CrlEntry::get_RevocationDate()
 DateTime_t110  X509CrlEntry_get_RevocationDate_m7849 (X509CrlEntry_t1379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Security.X509.X509ExtensionCollection Mono.Security.X509.X509Crl/X509CrlEntry::get_Extensions()
 X509ExtensionCollection_t1519 * X509CrlEntry_get_Extensions_m7855 (X509CrlEntry_t1379 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
