﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_14.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>
struct CachedInvokableCall_1_t2798  : public InvokableCall_1_t2799
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
