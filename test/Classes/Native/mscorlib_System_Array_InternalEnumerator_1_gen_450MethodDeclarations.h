﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>
struct InternalEnumerator_1_t4741;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SendMouseEvents/HitInfo
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29077 (InternalEnumerator_1_t4741 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29078 (InternalEnumerator_1_t4741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
 void InternalEnumerator_1_Dispose_m29079 (InternalEnumerator_1_t4741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29080 (InternalEnumerator_1_t4741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
 HitInfo_t1056  InternalEnumerator_1_get_Current_m29081 (InternalEnumerator_1_t4741 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
