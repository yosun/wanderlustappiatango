﻿#pragma once
#include <stdint.h>
// Vuforia.DataSetImpl
struct DataSetImpl_t566;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.DataSetImpl>
struct Predicate_1_t3846  : public MulticastDelegate_t325
{
};
