﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$32
struct $ArrayType$32_t2267;
struct $ArrayType$32_t2267_marshaled;

void $ArrayType$32_t2267_marshal(const $ArrayType$32_t2267& unmarshaled, $ArrayType$32_t2267_marshaled& marshaled);
void $ArrayType$32_t2267_marshal_back(const $ArrayType$32_t2267_marshaled& marshaled, $ArrayType$32_t2267& unmarshaled);
void $ArrayType$32_t2267_marshal_cleanup($ArrayType$32_t2267_marshaled& marshaled);
