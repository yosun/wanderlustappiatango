﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.ICanvasElement>
struct Enumerator_t3244;
// System.Object
struct Object_t;
// UnityEngine.UI.ICanvasElement
struct ICanvasElement_t278;
// System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>
struct List_1_t3220;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.ICanvasElement>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m17334(__this, ___l, method) (void)Enumerator__ctor_m14640_gshared((Enumerator_t2850 *)__this, (List_1_t150 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17335(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14641_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.ICanvasElement>::Dispose()
#define Enumerator_Dispose_m17336(__this, method) (void)Enumerator_Dispose_m14642_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.ICanvasElement>::VerifyState()
#define Enumerator_VerifyState_m17337(__this, method) (void)Enumerator_VerifyState_m14643_gshared((Enumerator_t2850 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.ICanvasElement>::MoveNext()
#define Enumerator_MoveNext_m17338(__this, method) (bool)Enumerator_MoveNext_m14644_gshared((Enumerator_t2850 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.ICanvasElement>::get_Current()
#define Enumerator_get_Current_m17339(__this, method) (Object_t *)Enumerator_get_Current_m14645_gshared((Enumerator_t2850 *)__this, method)
