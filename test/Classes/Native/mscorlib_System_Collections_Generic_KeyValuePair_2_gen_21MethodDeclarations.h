﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>
struct KeyValuePair_2_t4235;
// Vuforia.ImageTarget
struct ImageTarget_t572;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m25685 (KeyValuePair_2_t4235 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::get_Key()
 int32_t KeyValuePair_2_get_Key_m25686 (KeyValuePair_2_t4235 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m25687 (KeyValuePair_2_t4235 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::get_Value()
 Object_t * KeyValuePair_2_get_Value_m25688 (KeyValuePair_2_t4235 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m25689 (KeyValuePair_2_t4235 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::ToString()
 String_t* KeyValuePair_2_ToString_m25690 (KeyValuePair_2_t4235 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
