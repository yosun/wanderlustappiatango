﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.SHA384
struct SHA384_t2116;

// System.Void System.Security.Cryptography.SHA384::.ctor()
 void SHA384__ctor_m12083 (SHA384_t2116 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
