﻿#pragma once
#include <stdint.h>
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Object
#include "mscorlib_System_Object.h"
// System.Runtime.Serialization.SerializationInfoEnumerator
struct SerializationInfoEnumerator_t2087  : public Object_t
{
	// System.Collections.IEnumerator System.Runtime.Serialization.SerializationInfoEnumerator::enumerator
	Object_t * ___enumerator_0;
};
