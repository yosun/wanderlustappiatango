﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>
struct DefaultComparer_t3797;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::.ctor()
 void DefaultComparer__ctor_m21338 (DefaultComparer_t3797 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Vuforia.Image/PIXEL_FORMAT>::Compare(T,T)
 int32_t DefaultComparer_Compare_m21339 (DefaultComparer_t3797 * __this, int32_t ___x, int32_t ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
