﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARUnity/StorageType>
struct InternalEnumerator_1_t4309;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARUnity/StorageType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_StorageTy.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARUnity/StorageType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m26250 (InternalEnumerator_1_t4309 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARUnity/StorageType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26251 (InternalEnumerator_1_t4309 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARUnity/StorageType>::Dispose()
 void InternalEnumerator_1_Dispose_m26252 (InternalEnumerator_1_t4309 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARUnity/StorageType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m26253 (InternalEnumerator_1_t4309 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.QCARUnity/StorageType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m26254 (InternalEnumerator_1_t4309 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
