﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>
struct ShimEnumerator_t3954;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Type,System.UInt16>
struct Dictionary_2_t681;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m22926 (ShimEnumerator_t3954 * __this, Dictionary_2_t681 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>::MoveNext()
 bool ShimEnumerator_MoveNext_m22927 (ShimEnumerator_t3954 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m22928 (ShimEnumerator_t3954 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>::get_Key()
 Object_t * ShimEnumerator_get_Key_m22929 (ShimEnumerator_t3954 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>::get_Value()
 Object_t * ShimEnumerator_get_Value_m22930 (ShimEnumerator_t3954 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Type,System.UInt16>::get_Current()
 Object_t * ShimEnumerator_get_Current_m22931 (ShimEnumerator_t3954 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
