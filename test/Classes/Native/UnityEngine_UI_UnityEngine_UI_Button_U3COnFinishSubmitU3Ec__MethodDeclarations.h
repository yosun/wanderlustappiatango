﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1
struct U3COnFinishSubmitU3Ec__Iterator1_t271;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1::.ctor()
 void U3COnFinishSubmitU3Ec__Iterator1__ctor_m972 (U3COnFinishSubmitU3Ec__Iterator1_t271 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
 Object_t * U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m973 (U3COnFinishSubmitU3Ec__Iterator1_t271 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1::System.Collections.IEnumerator.get_Current()
 Object_t * U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m974 (U3COnFinishSubmitU3Ec__Iterator1_t271 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1::MoveNext()
 bool U3COnFinishSubmitU3Ec__Iterator1_MoveNext_m975 (U3COnFinishSubmitU3Ec__Iterator1_t271 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1::Dispose()
 void U3COnFinishSubmitU3Ec__Iterator1_Dispose_m976 (U3COnFinishSubmitU3Ec__Iterator1_t271 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button/<OnFinishSubmit>c__Iterator1::Reset()
 void U3COnFinishSubmitU3Ec__Iterator1_Reset_m977 (U3COnFinishSubmitU3Ec__Iterator1_t271 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
