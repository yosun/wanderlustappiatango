﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.CanvasScaler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_75.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.CanvasScaler>
struct CachedInvokableCall_1_t3556  : public InvokableCall_1_t3557
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.CanvasScaler>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
