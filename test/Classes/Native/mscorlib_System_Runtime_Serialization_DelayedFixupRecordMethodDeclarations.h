﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.DelayedFixupRecord
struct DelayedFixupRecord_t2077;
// System.Runtime.Serialization.ObjectRecord
struct ObjectRecord_t2072;
// System.String
struct String_t;
// System.Runtime.Serialization.ObjectManager
struct ObjectManager_t2065;

// System.Void System.Runtime.Serialization.DelayedFixupRecord::.ctor(System.Runtime.Serialization.ObjectRecord,System.String,System.Runtime.Serialization.ObjectRecord)
 void DelayedFixupRecord__ctor_m11786 (DelayedFixupRecord_t2077 * __this, ObjectRecord_t2072 * ___objectToBeFixed, String_t* ___memberName, ObjectRecord_t2072 * ___objectRequired, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.DelayedFixupRecord::FixupImpl(System.Runtime.Serialization.ObjectManager)
 void DelayedFixupRecord_FixupImpl_m11787 (DelayedFixupRecord_t2077 * __this, ObjectManager_t2065 * ___manager, MethodInfo* method) IL2CPP_METHOD_ATTR;
