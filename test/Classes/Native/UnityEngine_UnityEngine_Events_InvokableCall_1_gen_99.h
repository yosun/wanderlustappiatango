﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadAbstractBehaviour>
struct UnityAction_1_t3736;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadAbstractBehaviour>
struct InvokableCall_1_t3735  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadAbstractBehaviour>::Delegate
	UnityAction_1_t3736 * ___Delegate_0;
};
