﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerable_1_t8859_il2cpp_TypeInfo;


// System.Array
#include "mscorlib_System_Array.h"

// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Diagnostics.StackFrame>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Diagnostics.StackFrame>
extern Il2CppType IEnumerator_1_t6939_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50028_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Diagnostics.StackFrame>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50028_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8859_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6939_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50028_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8859_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50028_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
static TypeInfo* IEnumerable_1_t8859_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8859_0_0_0;
extern Il2CppType IEnumerable_1_t8859_1_0_0;
struct IEnumerable_1_t8859;
extern Il2CppGenericClass IEnumerable_1_t8859_GenericClass;
TypeInfo IEnumerable_1_t8859_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8859_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8859_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8859_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8859_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8859_0_0_0/* byval_arg */
	, &IEnumerable_1_t8859_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8859_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8858_il2cpp_TypeInfo;

// System.Diagnostics.StackFrame
#include "mscorlib_System_Diagnostics_StackFrame.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Void
#include "mscorlib_System_Void.h"


// System.Int32 System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>
extern MethodInfo IList_1_get_Item_m50029_MethodInfo;
extern MethodInfo IList_1_set_Item_m50030_MethodInfo;
static PropertyInfo IList_1_t8858____Item_PropertyInfo = 
{
	&IList_1_t8858_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50029_MethodInfo/* get */
	, &IList_1_set_Item_m50030_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8858_PropertyInfos[] =
{
	&IList_1_t8858____Item_PropertyInfo,
	NULL
};
extern Il2CppType StackFrame_t1166_0_0_0;
extern Il2CppType StackFrame_t1166_0_0_0;
static ParameterInfo IList_1_t8858_IList_1_IndexOf_m50031_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &StackFrame_t1166_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50031_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50031_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8858_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8858_IList_1_IndexOf_m50031_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50031_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType StackFrame_t1166_0_0_0;
static ParameterInfo IList_1_t8858_IList_1_Insert_m50032_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &StackFrame_t1166_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50032_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50032_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8858_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8858_IList_1_Insert_m50032_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50032_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8858_IList_1_RemoveAt_m50033_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50033_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50033_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8858_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8858_IList_1_RemoveAt_m50033_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50033_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8858_IList_1_get_Item_m50029_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType StackFrame_t1166_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50029_GenericMethod;
// T System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50029_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8858_il2cpp_TypeInfo/* declaring_type */
	, &StackFrame_t1166_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8858_IList_1_get_Item_m50029_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50029_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType StackFrame_t1166_0_0_0;
static ParameterInfo IList_1_t8858_IList_1_set_Item_m50030_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &StackFrame_t1166_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50030_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Diagnostics.StackFrame>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50030_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8858_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8858_IList_1_set_Item_m50030_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50030_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8858_MethodInfos[] =
{
	&IList_1_IndexOf_m50031_MethodInfo,
	&IList_1_Insert_m50032_MethodInfo,
	&IList_1_RemoveAt_m50033_MethodInfo,
	&IList_1_get_Item_m50029_MethodInfo,
	&IList_1_set_Item_m50030_MethodInfo,
	NULL
};
extern TypeInfo ICollection_1_t8857_il2cpp_TypeInfo;
static TypeInfo* IList_1_t8858_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8857_il2cpp_TypeInfo,
	&IEnumerable_1_t8859_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8858_0_0_0;
extern Il2CppType IList_1_t8858_1_0_0;
struct IList_1_t8858;
extern Il2CppGenericClass IList_1_t8858_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8858_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8858_MethodInfos/* methods */
	, IList_1_t8858_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8858_il2cpp_TypeInfo/* element_class */
	, IList_1_t8858_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8858_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8858_0_0_0/* byval_arg */
	, &IList_1_t8858_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8858_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6941_il2cpp_TypeInfo;

// System.Globalization.CompareOptions
#include "mscorlib_System_Globalization_CompareOptions.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.CompareOptions>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.CompareOptions>
extern MethodInfo IEnumerator_1_get_Current_m50034_MethodInfo;
static PropertyInfo IEnumerator_1_t6941____Current_PropertyInfo = 
{
	&IEnumerator_1_t6941_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50034_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6941_PropertyInfos[] =
{
	&IEnumerator_1_t6941____Current_PropertyInfo,
	NULL
};
extern Il2CppType CompareOptions_t1681_0_0_0;
extern void* RuntimeInvoker_CompareOptions_t1681 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50034_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.CompareOptions>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50034_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6941_il2cpp_TypeInfo/* declaring_type */
	, &CompareOptions_t1681_0_0_0/* return_type */
	, RuntimeInvoker_CompareOptions_t1681/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50034_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6941_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50034_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6941_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6941_0_0_0;
extern Il2CppType IEnumerator_1_t6941_1_0_0;
struct IEnumerator_1_t6941;
extern Il2CppGenericClass IEnumerator_1_t6941_GenericClass;
TypeInfo IEnumerator_1_t6941_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6941_MethodInfos/* methods */
	, IEnumerator_1_t6941_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6941_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6941_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6941_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6941_0_0_0/* byval_arg */
	, &IEnumerator_1_t6941_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6941_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_604.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4987_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_604MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo CompareOptions_t1681_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1493_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m30521_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7651_MethodInfo;
extern MethodInfo Array_get_Length_m7656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCompareOptions_t1681_m39524_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.CompareOptions>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.CompareOptions>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisCompareOptions_t1681_m39524 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30517_MethodInfo;
 void InternalEnumerator_1__ctor_m30517 (InternalEnumerator_1_t4987 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30518_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30518 (InternalEnumerator_1_t4987 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30521(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30521_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&CompareOptions_t1681_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30519_MethodInfo;
 void InternalEnumerator_1_Dispose_m30519 (InternalEnumerator_1_t4987 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30520_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30520 (InternalEnumerator_1_t4987 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30521 (InternalEnumerator_1_t4987 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisCompareOptions_t1681_m39524(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisCompareOptions_t1681_m39524_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4987____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4987_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4987, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4987____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4987_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4987, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4987_FieldInfos[] =
{
	&InternalEnumerator_1_t4987____array_0_FieldInfo,
	&InternalEnumerator_1_t4987____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4987____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4987_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30518_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4987____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4987_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30521_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4987_PropertyInfos[] =
{
	&InternalEnumerator_1_t4987____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4987____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4987_InternalEnumerator_1__ctor_m30517_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30517_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30517_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30517/* method */
	, &InternalEnumerator_1_t4987_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4987_InternalEnumerator_1__ctor_m30517_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30517_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30518_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30518_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30518/* method */
	, &InternalEnumerator_1_t4987_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30518_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30519_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30519_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30519/* method */
	, &InternalEnumerator_1_t4987_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30519_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30520_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30520_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30520/* method */
	, &InternalEnumerator_1_t4987_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30520_GenericMethod/* genericMethod */

};
extern Il2CppType CompareOptions_t1681_0_0_0;
extern void* RuntimeInvoker_CompareOptions_t1681 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30521_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.CompareOptions>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30521_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30521/* method */
	, &InternalEnumerator_1_t4987_il2cpp_TypeInfo/* declaring_type */
	, &CompareOptions_t1681_0_0_0/* return_type */
	, RuntimeInvoker_CompareOptions_t1681/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30521_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4987_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30517_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30518_MethodInfo,
	&InternalEnumerator_1_Dispose_m30519_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30520_MethodInfo,
	&InternalEnumerator_1_get_Current_m30521_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4987_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30518_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30520_MethodInfo,
	&InternalEnumerator_1_Dispose_m30519_MethodInfo,
	&InternalEnumerator_1_get_Current_m30521_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4987_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6941_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4987_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6941_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4987_0_0_0;
extern Il2CppType InternalEnumerator_1_t4987_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t4987_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t4987_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4987_MethodInfos/* methods */
	, InternalEnumerator_1_t4987_PropertyInfos/* properties */
	, InternalEnumerator_1_t4987_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4987_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4987_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4987_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4987_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4987_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4987_1_0_0/* this_arg */
	, InternalEnumerator_1_t4987_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4987_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4987)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8860_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>
extern MethodInfo ICollection_1_get_Count_m50035_MethodInfo;
static PropertyInfo ICollection_1_t8860____Count_PropertyInfo = 
{
	&ICollection_1_t8860_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50035_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50036_MethodInfo;
static PropertyInfo ICollection_1_t8860____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8860_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50036_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8860_PropertyInfos[] =
{
	&ICollection_1_t8860____Count_PropertyInfo,
	&ICollection_1_t8860____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50035_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::get_Count()
MethodInfo ICollection_1_get_Count_m50035_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8860_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50035_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50036_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50036_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8860_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50036_GenericMethod/* genericMethod */

};
extern Il2CppType CompareOptions_t1681_0_0_0;
extern Il2CppType CompareOptions_t1681_0_0_0;
static ParameterInfo ICollection_1_t8860_ICollection_1_Add_m50037_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompareOptions_t1681_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50037_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Add(T)
MethodInfo ICollection_1_Add_m50037_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8860_ICollection_1_Add_m50037_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50037_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50038_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Clear()
MethodInfo ICollection_1_Clear_m50038_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50038_GenericMethod/* genericMethod */

};
extern Il2CppType CompareOptions_t1681_0_0_0;
static ParameterInfo ICollection_1_t8860_ICollection_1_Contains_m50039_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompareOptions_t1681_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50039_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Contains(T)
MethodInfo ICollection_1_Contains_m50039_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8860_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8860_ICollection_1_Contains_m50039_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50039_GenericMethod/* genericMethod */

};
extern Il2CppType CompareOptionsU5BU5D_t5287_0_0_0;
extern Il2CppType CompareOptionsU5BU5D_t5287_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8860_ICollection_1_CopyTo_m50040_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CompareOptionsU5BU5D_t5287_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50040_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50040_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8860_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8860_ICollection_1_CopyTo_m50040_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50040_GenericMethod/* genericMethod */

};
extern Il2CppType CompareOptions_t1681_0_0_0;
static ParameterInfo ICollection_1_t8860_ICollection_1_Remove_m50041_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompareOptions_t1681_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50041_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.CompareOptions>::Remove(T)
MethodInfo ICollection_1_Remove_m50041_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8860_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8860_ICollection_1_Remove_m50041_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50041_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8860_MethodInfos[] =
{
	&ICollection_1_get_Count_m50035_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50036_MethodInfo,
	&ICollection_1_Add_m50037_MethodInfo,
	&ICollection_1_Clear_m50038_MethodInfo,
	&ICollection_1_Contains_m50039_MethodInfo,
	&ICollection_1_CopyTo_m50040_MethodInfo,
	&ICollection_1_Remove_m50041_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8862_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8860_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8862_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8860_0_0_0;
extern Il2CppType ICollection_1_t8860_1_0_0;
struct ICollection_1_t8860;
extern Il2CppGenericClass ICollection_1_t8860_GenericClass;
TypeInfo ICollection_1_t8860_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8860_MethodInfos/* methods */
	, ICollection_1_t8860_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8860_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8860_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8860_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8860_0_0_0/* byval_arg */
	, &ICollection_1_t8860_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8860_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.CompareOptions>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.CompareOptions>
extern Il2CppType IEnumerator_1_t6941_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50042_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.CompareOptions>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50042_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8862_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6941_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50042_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8862_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50042_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8862_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8862_0_0_0;
extern Il2CppType IEnumerable_1_t8862_1_0_0;
struct IEnumerable_1_t8862;
extern Il2CppGenericClass IEnumerable_1_t8862_GenericClass;
TypeInfo IEnumerable_1_t8862_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8862_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8862_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8862_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8862_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8862_0_0_0/* byval_arg */
	, &IEnumerable_1_t8862_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8862_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8861_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.CompareOptions>
extern MethodInfo IList_1_get_Item_m50043_MethodInfo;
extern MethodInfo IList_1_set_Item_m50044_MethodInfo;
static PropertyInfo IList_1_t8861____Item_PropertyInfo = 
{
	&IList_1_t8861_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50043_MethodInfo/* get */
	, &IList_1_set_Item_m50044_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8861_PropertyInfos[] =
{
	&IList_1_t8861____Item_PropertyInfo,
	NULL
};
extern Il2CppType CompareOptions_t1681_0_0_0;
static ParameterInfo IList_1_t8861_IList_1_IndexOf_m50045_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CompareOptions_t1681_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50045_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50045_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8861_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8861_IList_1_IndexOf_m50045_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50045_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CompareOptions_t1681_0_0_0;
static ParameterInfo IList_1_t8861_IList_1_Insert_m50046_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CompareOptions_t1681_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50046_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50046_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8861_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8861_IList_1_Insert_m50046_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50046_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8861_IList_1_RemoveAt_m50047_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50047_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50047_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8861_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8861_IList_1_RemoveAt_m50047_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50047_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8861_IList_1_get_Item_m50043_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType CompareOptions_t1681_0_0_0;
extern void* RuntimeInvoker_CompareOptions_t1681_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50043_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50043_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8861_il2cpp_TypeInfo/* declaring_type */
	, &CompareOptions_t1681_0_0_0/* return_type */
	, RuntimeInvoker_CompareOptions_t1681_Int32_t93/* invoker_method */
	, IList_1_t8861_IList_1_get_Item_m50043_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50043_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CompareOptions_t1681_0_0_0;
static ParameterInfo IList_1_t8861_IList_1_set_Item_m50044_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CompareOptions_t1681_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50044_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.CompareOptions>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50044_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8861_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8861_IList_1_set_Item_m50044_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50044_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8861_MethodInfos[] =
{
	&IList_1_IndexOf_m50045_MethodInfo,
	&IList_1_Insert_m50046_MethodInfo,
	&IList_1_RemoveAt_m50047_MethodInfo,
	&IList_1_get_Item_m50043_MethodInfo,
	&IList_1_set_Item_m50044_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8861_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8860_il2cpp_TypeInfo,
	&IEnumerable_1_t8862_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8861_0_0_0;
extern Il2CppType IList_1_t8861_1_0_0;
struct IList_1_t8861;
extern Il2CppGenericClass IList_1_t8861_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8861_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8861_MethodInfos/* methods */
	, IList_1_t8861_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8861_il2cpp_TypeInfo/* element_class */
	, IList_1_t8861_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8861_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8861_0_0_0/* byval_arg */
	, &IList_1_t8861_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8861_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6943_il2cpp_TypeInfo;

// System.Globalization.Calendar
#include "mscorlib_System_Globalization_Calendar.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.Calendar>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.Calendar>
extern MethodInfo IEnumerator_1_get_Current_m50048_MethodInfo;
static PropertyInfo IEnumerator_1_t6943____Current_PropertyInfo = 
{
	&IEnumerator_1_t6943_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50048_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6943_PropertyInfos[] =
{
	&IEnumerator_1_t6943____Current_PropertyInfo,
	NULL
};
extern Il2CppType Calendar_t1848_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50048_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.Calendar>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50048_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6943_il2cpp_TypeInfo/* declaring_type */
	, &Calendar_t1848_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50048_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6943_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50048_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6943_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6943_0_0_0;
extern Il2CppType IEnumerator_1_t6943_1_0_0;
struct IEnumerator_1_t6943;
extern Il2CppGenericClass IEnumerator_1_t6943_GenericClass;
TypeInfo IEnumerator_1_t6943_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6943_MethodInfos/* methods */
	, IEnumerator_1_t6943_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6943_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6943_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6943_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6943_0_0_0/* byval_arg */
	, &IEnumerator_1_t6943_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6943_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.Calendar>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_605.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4988_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.Calendar>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_605MethodDeclarations.h"

extern TypeInfo Calendar_t1848_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30526_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCalendar_t1848_m39535_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m31436_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m31436(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.Calendar>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.Calendar>(System.Int32)
#define Array_InternalArray__get_Item_TisCalendar_t1848_m39535(__this, p0, method) (Calendar_t1848 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Globalization.Calendar>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Globalization.Calendar>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Globalization.Calendar>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.Calendar>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Globalization.Calendar>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.Calendar>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4988____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4988_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4988, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4988____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4988_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4988, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4988_FieldInfos[] =
{
	&InternalEnumerator_1_t4988____array_0_FieldInfo,
	&InternalEnumerator_1_t4988____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30523_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4988____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4988_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30523_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4988____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4988_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30526_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4988_PropertyInfos[] =
{
	&InternalEnumerator_1_t4988____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4988____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4988_InternalEnumerator_1__ctor_m30522_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30522_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.Calendar>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30522_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4988_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4988_InternalEnumerator_1__ctor_m30522_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30522_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30523_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.Calendar>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30523_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4988_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30523_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30524_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.Calendar>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30524_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4988_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30524_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30525_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.Calendar>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30525_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4988_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30525_GenericMethod/* genericMethod */

};
extern Il2CppType Calendar_t1848_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30526_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.Calendar>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30526_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4988_il2cpp_TypeInfo/* declaring_type */
	, &Calendar_t1848_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30526_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4988_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30522_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30523_MethodInfo,
	&InternalEnumerator_1_Dispose_m30524_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30525_MethodInfo,
	&InternalEnumerator_1_get_Current_m30526_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30525_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30524_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4988_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30523_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30525_MethodInfo,
	&InternalEnumerator_1_Dispose_m30524_MethodInfo,
	&InternalEnumerator_1_get_Current_m30526_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4988_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6943_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4988_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6943_il2cpp_TypeInfo, 7},
};
extern TypeInfo Calendar_t1848_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4988_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30526_MethodInfo/* Method Usage */,
	&Calendar_t1848_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCalendar_t1848_m39535_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4988_0_0_0;
extern Il2CppType InternalEnumerator_1_t4988_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4988_GenericClass;
TypeInfo InternalEnumerator_1_t4988_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4988_MethodInfos/* methods */
	, InternalEnumerator_1_t4988_PropertyInfos/* properties */
	, InternalEnumerator_1_t4988_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4988_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4988_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4988_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4988_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4988_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4988_1_0_0/* this_arg */
	, InternalEnumerator_1_t4988_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4988_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4988_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4988)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8863_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.Calendar>
extern MethodInfo ICollection_1_get_Count_m50049_MethodInfo;
static PropertyInfo ICollection_1_t8863____Count_PropertyInfo = 
{
	&ICollection_1_t8863_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50049_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50050_MethodInfo;
static PropertyInfo ICollection_1_t8863____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8863_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50050_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8863_PropertyInfos[] =
{
	&ICollection_1_t8863____Count_PropertyInfo,
	&ICollection_1_t8863____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50049_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::get_Count()
MethodInfo ICollection_1_get_Count_m50049_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8863_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50049_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50050_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50050_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8863_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50050_GenericMethod/* genericMethod */

};
extern Il2CppType Calendar_t1848_0_0_0;
extern Il2CppType Calendar_t1848_0_0_0;
static ParameterInfo ICollection_1_t8863_ICollection_1_Add_m50051_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Calendar_t1848_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50051_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Add(T)
MethodInfo ICollection_1_Add_m50051_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8863_ICollection_1_Add_m50051_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50051_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50052_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Clear()
MethodInfo ICollection_1_Clear_m50052_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50052_GenericMethod/* genericMethod */

};
extern Il2CppType Calendar_t1848_0_0_0;
static ParameterInfo ICollection_1_t8863_ICollection_1_Contains_m50053_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Calendar_t1848_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50053_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Contains(T)
MethodInfo ICollection_1_Contains_m50053_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8863_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8863_ICollection_1_Contains_m50053_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50053_GenericMethod/* genericMethod */

};
extern Il2CppType CalendarU5BU5D_t1853_0_0_0;
extern Il2CppType CalendarU5BU5D_t1853_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8863_ICollection_1_CopyTo_m50054_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CalendarU5BU5D_t1853_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50054_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50054_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8863_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8863_ICollection_1_CopyTo_m50054_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50054_GenericMethod/* genericMethod */

};
extern Il2CppType Calendar_t1848_0_0_0;
static ParameterInfo ICollection_1_t8863_ICollection_1_Remove_m50055_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Calendar_t1848_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50055_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.Calendar>::Remove(T)
MethodInfo ICollection_1_Remove_m50055_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8863_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8863_ICollection_1_Remove_m50055_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50055_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8863_MethodInfos[] =
{
	&ICollection_1_get_Count_m50049_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50050_MethodInfo,
	&ICollection_1_Add_m50051_MethodInfo,
	&ICollection_1_Clear_m50052_MethodInfo,
	&ICollection_1_Contains_m50053_MethodInfo,
	&ICollection_1_CopyTo_m50054_MethodInfo,
	&ICollection_1_Remove_m50055_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8865_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8863_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8865_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8863_0_0_0;
extern Il2CppType ICollection_1_t8863_1_0_0;
struct ICollection_1_t8863;
extern Il2CppGenericClass ICollection_1_t8863_GenericClass;
TypeInfo ICollection_1_t8863_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8863_MethodInfos/* methods */
	, ICollection_1_t8863_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8863_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8863_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8863_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8863_0_0_0/* byval_arg */
	, &ICollection_1_t8863_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8863_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.Calendar>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.Calendar>
extern Il2CppType IEnumerator_1_t6943_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50056_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.Calendar>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50056_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8865_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6943_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50056_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8865_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50056_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8865_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8865_0_0_0;
extern Il2CppType IEnumerable_1_t8865_1_0_0;
struct IEnumerable_1_t8865;
extern Il2CppGenericClass IEnumerable_1_t8865_GenericClass;
TypeInfo IEnumerable_1_t8865_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8865_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8865_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8865_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8865_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8865_0_0_0/* byval_arg */
	, &IEnumerable_1_t8865_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8865_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8864_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.Calendar>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.Calendar>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.Calendar>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.Calendar>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.Calendar>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.Calendar>
extern MethodInfo IList_1_get_Item_m50057_MethodInfo;
extern MethodInfo IList_1_set_Item_m50058_MethodInfo;
static PropertyInfo IList_1_t8864____Item_PropertyInfo = 
{
	&IList_1_t8864_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50057_MethodInfo/* get */
	, &IList_1_set_Item_m50058_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8864_PropertyInfos[] =
{
	&IList_1_t8864____Item_PropertyInfo,
	NULL
};
extern Il2CppType Calendar_t1848_0_0_0;
static ParameterInfo IList_1_t8864_IList_1_IndexOf_m50059_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Calendar_t1848_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50059_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.Calendar>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50059_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8864_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8864_IList_1_IndexOf_m50059_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50059_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Calendar_t1848_0_0_0;
static ParameterInfo IList_1_t8864_IList_1_Insert_m50060_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Calendar_t1848_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50060_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.Calendar>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50060_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8864_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8864_IList_1_Insert_m50060_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50060_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8864_IList_1_RemoveAt_m50061_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50061_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.Calendar>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50061_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8864_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8864_IList_1_RemoveAt_m50061_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50061_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8864_IList_1_get_Item_m50057_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Calendar_t1848_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50057_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.Calendar>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50057_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8864_il2cpp_TypeInfo/* declaring_type */
	, &Calendar_t1848_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8864_IList_1_get_Item_m50057_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50057_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Calendar_t1848_0_0_0;
static ParameterInfo IList_1_t8864_IList_1_set_Item_m50058_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Calendar_t1848_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50058_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.Calendar>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50058_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8864_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8864_IList_1_set_Item_m50058_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50058_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8864_MethodInfos[] =
{
	&IList_1_IndexOf_m50059_MethodInfo,
	&IList_1_Insert_m50060_MethodInfo,
	&IList_1_RemoveAt_m50061_MethodInfo,
	&IList_1_get_Item_m50057_MethodInfo,
	&IList_1_set_Item_m50058_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8864_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8863_il2cpp_TypeInfo,
	&IEnumerable_1_t8865_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8864_0_0_0;
extern Il2CppType IList_1_t8864_1_0_0;
struct IList_1_t8864;
extern Il2CppGenericClass IList_1_t8864_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8864_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8864_MethodInfos/* methods */
	, IList_1_t8864_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8864_il2cpp_TypeInfo/* element_class */
	, IList_1_t8864_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8864_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8864_0_0_0/* byval_arg */
	, &IList_1_t8864_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8864_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6945_il2cpp_TypeInfo;

// System.Globalization.DateTimeFormatFlags
#include "mscorlib_System_Globalization_DateTimeFormatFlags.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeFormatFlags>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeFormatFlags>
extern MethodInfo IEnumerator_1_get_Current_m50062_MethodInfo;
static PropertyInfo IEnumerator_1_t6945____Current_PropertyInfo = 
{
	&IEnumerator_1_t6945_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50062_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6945_PropertyInfos[] =
{
	&IEnumerator_1_t6945____Current_PropertyInfo,
	NULL
};
extern Il2CppType DateTimeFormatFlags_t1854_0_0_0;
extern void* RuntimeInvoker_DateTimeFormatFlags_t1854 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50062_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeFormatFlags>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50062_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6945_il2cpp_TypeInfo/* declaring_type */
	, &DateTimeFormatFlags_t1854_0_0_0/* return_type */
	, RuntimeInvoker_DateTimeFormatFlags_t1854/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50062_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6945_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50062_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6945_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6945_0_0_0;
extern Il2CppType IEnumerator_1_t6945_1_0_0;
struct IEnumerator_1_t6945;
extern Il2CppGenericClass IEnumerator_1_t6945_GenericClass;
TypeInfo IEnumerator_1_t6945_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6945_MethodInfos/* methods */
	, IEnumerator_1_t6945_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6945_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6945_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6945_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6945_0_0_0/* byval_arg */
	, &IEnumerator_1_t6945_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6945_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_606.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4989_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_606MethodDeclarations.h"

extern TypeInfo DateTimeFormatFlags_t1854_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30531_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDateTimeFormatFlags_t1854_m39546_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.DateTimeFormatFlags>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.DateTimeFormatFlags>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisDateTimeFormatFlags_t1854_m39546 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30527_MethodInfo;
 void InternalEnumerator_1__ctor_m30527 (InternalEnumerator_1_t4989 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30528_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30528 (InternalEnumerator_1_t4989 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30531(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30531_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&DateTimeFormatFlags_t1854_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30529_MethodInfo;
 void InternalEnumerator_1_Dispose_m30529 (InternalEnumerator_1_t4989 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30530_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30530 (InternalEnumerator_1_t4989 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30531 (InternalEnumerator_1_t4989 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisDateTimeFormatFlags_t1854_m39546(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisDateTimeFormatFlags_t1854_m39546_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4989____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4989_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4989, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4989____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4989_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4989, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4989_FieldInfos[] =
{
	&InternalEnumerator_1_t4989____array_0_FieldInfo,
	&InternalEnumerator_1_t4989____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4989____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4989_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30528_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4989____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4989_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30531_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4989_PropertyInfos[] =
{
	&InternalEnumerator_1_t4989____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4989____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4989_InternalEnumerator_1__ctor_m30527_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30527_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30527_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30527/* method */
	, &InternalEnumerator_1_t4989_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4989_InternalEnumerator_1__ctor_m30527_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30527_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30528_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30528_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30528/* method */
	, &InternalEnumerator_1_t4989_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30528_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30529_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30529_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30529/* method */
	, &InternalEnumerator_1_t4989_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30529_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30530_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30530_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30530/* method */
	, &InternalEnumerator_1_t4989_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30530_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeFormatFlags_t1854_0_0_0;
extern void* RuntimeInvoker_DateTimeFormatFlags_t1854 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30531_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.DateTimeFormatFlags>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30531_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30531/* method */
	, &InternalEnumerator_1_t4989_il2cpp_TypeInfo/* declaring_type */
	, &DateTimeFormatFlags_t1854_0_0_0/* return_type */
	, RuntimeInvoker_DateTimeFormatFlags_t1854/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30531_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4989_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30527_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30528_MethodInfo,
	&InternalEnumerator_1_Dispose_m30529_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30530_MethodInfo,
	&InternalEnumerator_1_get_Current_m30531_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4989_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30528_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30530_MethodInfo,
	&InternalEnumerator_1_Dispose_m30529_MethodInfo,
	&InternalEnumerator_1_get_Current_m30531_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4989_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6945_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4989_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6945_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4989_0_0_0;
extern Il2CppType InternalEnumerator_1_t4989_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4989_GenericClass;
TypeInfo InternalEnumerator_1_t4989_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4989_MethodInfos/* methods */
	, InternalEnumerator_1_t4989_PropertyInfos/* properties */
	, InternalEnumerator_1_t4989_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4989_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4989_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4989_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4989_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4989_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4989_1_0_0/* this_arg */
	, InternalEnumerator_1_t4989_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4989_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4989)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8866_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>
extern MethodInfo ICollection_1_get_Count_m50063_MethodInfo;
static PropertyInfo ICollection_1_t8866____Count_PropertyInfo = 
{
	&ICollection_1_t8866_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50063_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50064_MethodInfo;
static PropertyInfo ICollection_1_t8866____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8866_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50064_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8866_PropertyInfos[] =
{
	&ICollection_1_t8866____Count_PropertyInfo,
	&ICollection_1_t8866____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50063_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::get_Count()
MethodInfo ICollection_1_get_Count_m50063_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8866_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50063_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50064_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50064_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8866_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50064_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeFormatFlags_t1854_0_0_0;
extern Il2CppType DateTimeFormatFlags_t1854_0_0_0;
static ParameterInfo ICollection_1_t8866_ICollection_1_Add_m50065_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlags_t1854_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50065_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Add(T)
MethodInfo ICollection_1_Add_m50065_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8866_ICollection_1_Add_m50065_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50065_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50066_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Clear()
MethodInfo ICollection_1_Clear_m50066_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50066_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeFormatFlags_t1854_0_0_0;
static ParameterInfo ICollection_1_t8866_ICollection_1_Contains_m50067_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlags_t1854_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50067_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Contains(T)
MethodInfo ICollection_1_Contains_m50067_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8866_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8866_ICollection_1_Contains_m50067_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50067_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeFormatFlagsU5BU5D_t5288_0_0_0;
extern Il2CppType DateTimeFormatFlagsU5BU5D_t5288_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8866_ICollection_1_CopyTo_m50068_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlagsU5BU5D_t5288_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50068_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50068_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8866_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8866_ICollection_1_CopyTo_m50068_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50068_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeFormatFlags_t1854_0_0_0;
static ParameterInfo ICollection_1_t8866_ICollection_1_Remove_m50069_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlags_t1854_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50069_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeFormatFlags>::Remove(T)
MethodInfo ICollection_1_Remove_m50069_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8866_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8866_ICollection_1_Remove_m50069_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50069_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8866_MethodInfos[] =
{
	&ICollection_1_get_Count_m50063_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50064_MethodInfo,
	&ICollection_1_Add_m50065_MethodInfo,
	&ICollection_1_Clear_m50066_MethodInfo,
	&ICollection_1_Contains_m50067_MethodInfo,
	&ICollection_1_CopyTo_m50068_MethodInfo,
	&ICollection_1_Remove_m50069_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8868_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8866_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8868_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8866_0_0_0;
extern Il2CppType ICollection_1_t8866_1_0_0;
struct ICollection_1_t8866;
extern Il2CppGenericClass ICollection_1_t8866_GenericClass;
TypeInfo ICollection_1_t8866_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8866_MethodInfos/* methods */
	, ICollection_1_t8866_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8866_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8866_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8866_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8866_0_0_0/* byval_arg */
	, &ICollection_1_t8866_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8866_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeFormatFlags>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeFormatFlags>
extern Il2CppType IEnumerator_1_t6945_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50070_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeFormatFlags>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50070_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8868_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6945_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50070_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8868_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50070_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8868_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8868_0_0_0;
extern Il2CppType IEnumerable_1_t8868_1_0_0;
struct IEnumerable_1_t8868;
extern Il2CppGenericClass IEnumerable_1_t8868_GenericClass;
TypeInfo IEnumerable_1_t8868_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8868_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8868_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8868_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8868_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8868_0_0_0/* byval_arg */
	, &IEnumerable_1_t8868_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8868_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8867_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>
extern MethodInfo IList_1_get_Item_m50071_MethodInfo;
extern MethodInfo IList_1_set_Item_m50072_MethodInfo;
static PropertyInfo IList_1_t8867____Item_PropertyInfo = 
{
	&IList_1_t8867_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50071_MethodInfo/* get */
	, &IList_1_set_Item_m50072_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8867_PropertyInfos[] =
{
	&IList_1_t8867____Item_PropertyInfo,
	NULL
};
extern Il2CppType DateTimeFormatFlags_t1854_0_0_0;
static ParameterInfo IList_1_t8867_IList_1_IndexOf_m50073_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlags_t1854_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50073_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50073_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8867_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8867_IList_1_IndexOf_m50073_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50073_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DateTimeFormatFlags_t1854_0_0_0;
static ParameterInfo IList_1_t8867_IList_1_Insert_m50074_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlags_t1854_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50074_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50074_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8867_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8867_IList_1_Insert_m50074_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50074_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8867_IList_1_RemoveAt_m50075_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50075_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50075_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8867_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8867_IList_1_RemoveAt_m50075_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50075_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8867_IList_1_get_Item_m50071_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType DateTimeFormatFlags_t1854_0_0_0;
extern void* RuntimeInvoker_DateTimeFormatFlags_t1854_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50071_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50071_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8867_il2cpp_TypeInfo/* declaring_type */
	, &DateTimeFormatFlags_t1854_0_0_0/* return_type */
	, RuntimeInvoker_DateTimeFormatFlags_t1854_Int32_t93/* invoker_method */
	, IList_1_t8867_IList_1_get_Item_m50071_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50071_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DateTimeFormatFlags_t1854_0_0_0;
static ParameterInfo IList_1_t8867_IList_1_set_Item_m50072_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DateTimeFormatFlags_t1854_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50072_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeFormatFlags>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50072_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8867_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8867_IList_1_set_Item_m50072_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50072_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8867_MethodInfos[] =
{
	&IList_1_IndexOf_m50073_MethodInfo,
	&IList_1_Insert_m50074_MethodInfo,
	&IList_1_RemoveAt_m50075_MethodInfo,
	&IList_1_get_Item_m50071_MethodInfo,
	&IList_1_set_Item_m50072_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8867_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8866_il2cpp_TypeInfo,
	&IEnumerable_1_t8868_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8867_0_0_0;
extern Il2CppType IList_1_t8867_1_0_0;
struct IList_1_t8867;
extern Il2CppGenericClass IList_1_t8867_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8867_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8867_MethodInfos/* methods */
	, IList_1_t8867_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8867_il2cpp_TypeInfo/* element_class */
	, IList_1_t8867_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8867_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8867_0_0_0/* byval_arg */
	, &IList_1_t8867_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8867_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6947_il2cpp_TypeInfo;

// System.Globalization.DateTimeStyles
#include "mscorlib_System_Globalization_DateTimeStyles.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeStyles>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeStyles>
extern MethodInfo IEnumerator_1_get_Current_m50076_MethodInfo;
static PropertyInfo IEnumerator_1_t6947____Current_PropertyInfo = 
{
	&IEnumerator_1_t6947_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50076_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6947_PropertyInfos[] =
{
	&IEnumerator_1_t6947____Current_PropertyInfo,
	NULL
};
extern Il2CppType DateTimeStyles_t1855_0_0_0;
extern void* RuntimeInvoker_DateTimeStyles_t1855 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50076_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.DateTimeStyles>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50076_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6947_il2cpp_TypeInfo/* declaring_type */
	, &DateTimeStyles_t1855_0_0_0/* return_type */
	, RuntimeInvoker_DateTimeStyles_t1855/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50076_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6947_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50076_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6947_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6947_0_0_0;
extern Il2CppType IEnumerator_1_t6947_1_0_0;
struct IEnumerator_1_t6947;
extern Il2CppGenericClass IEnumerator_1_t6947_GenericClass;
TypeInfo IEnumerator_1_t6947_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6947_MethodInfos/* methods */
	, IEnumerator_1_t6947_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6947_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6947_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6947_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6947_0_0_0/* byval_arg */
	, &IEnumerator_1_t6947_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6947_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_607.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4990_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_607MethodDeclarations.h"

extern TypeInfo DateTimeStyles_t1855_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30536_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDateTimeStyles_t1855_m39557_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.DateTimeStyles>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.DateTimeStyles>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisDateTimeStyles_t1855_m39557 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30532_MethodInfo;
 void InternalEnumerator_1__ctor_m30532 (InternalEnumerator_1_t4990 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30533_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30533 (InternalEnumerator_1_t4990 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30536(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30536_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&DateTimeStyles_t1855_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30534_MethodInfo;
 void InternalEnumerator_1_Dispose_m30534 (InternalEnumerator_1_t4990 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30535_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30535 (InternalEnumerator_1_t4990 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30536 (InternalEnumerator_1_t4990 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisDateTimeStyles_t1855_m39557(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisDateTimeStyles_t1855_m39557_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4990____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4990_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4990, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4990____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4990_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4990, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4990_FieldInfos[] =
{
	&InternalEnumerator_1_t4990____array_0_FieldInfo,
	&InternalEnumerator_1_t4990____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4990____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4990_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30533_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4990____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4990_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30536_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4990_PropertyInfos[] =
{
	&InternalEnumerator_1_t4990____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4990____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4990_InternalEnumerator_1__ctor_m30532_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30532_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30532_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30532/* method */
	, &InternalEnumerator_1_t4990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4990_InternalEnumerator_1__ctor_m30532_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30532_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30533_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30533_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30533/* method */
	, &InternalEnumerator_1_t4990_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30533_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30534_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30534_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30534/* method */
	, &InternalEnumerator_1_t4990_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30534_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30535_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30535_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30535/* method */
	, &InternalEnumerator_1_t4990_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30535_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeStyles_t1855_0_0_0;
extern void* RuntimeInvoker_DateTimeStyles_t1855 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30536_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.DateTimeStyles>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30536_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30536/* method */
	, &InternalEnumerator_1_t4990_il2cpp_TypeInfo/* declaring_type */
	, &DateTimeStyles_t1855_0_0_0/* return_type */
	, RuntimeInvoker_DateTimeStyles_t1855/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30536_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4990_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30532_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30533_MethodInfo,
	&InternalEnumerator_1_Dispose_m30534_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30535_MethodInfo,
	&InternalEnumerator_1_get_Current_m30536_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4990_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30533_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30535_MethodInfo,
	&InternalEnumerator_1_Dispose_m30534_MethodInfo,
	&InternalEnumerator_1_get_Current_m30536_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4990_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6947_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4990_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6947_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4990_0_0_0;
extern Il2CppType InternalEnumerator_1_t4990_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4990_GenericClass;
TypeInfo InternalEnumerator_1_t4990_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4990_MethodInfos/* methods */
	, InternalEnumerator_1_t4990_PropertyInfos/* properties */
	, InternalEnumerator_1_t4990_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4990_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4990_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4990_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4990_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4990_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4990_1_0_0/* this_arg */
	, InternalEnumerator_1_t4990_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4990_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4990)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8869_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>
extern MethodInfo ICollection_1_get_Count_m50077_MethodInfo;
static PropertyInfo ICollection_1_t8869____Count_PropertyInfo = 
{
	&ICollection_1_t8869_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50077_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50078_MethodInfo;
static PropertyInfo ICollection_1_t8869____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8869_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50078_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8869_PropertyInfos[] =
{
	&ICollection_1_t8869____Count_PropertyInfo,
	&ICollection_1_t8869____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50077_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::get_Count()
MethodInfo ICollection_1_get_Count_m50077_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8869_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50077_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50078_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50078_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8869_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50078_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeStyles_t1855_0_0_0;
extern Il2CppType DateTimeStyles_t1855_0_0_0;
static ParameterInfo ICollection_1_t8869_ICollection_1_Add_m50079_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeStyles_t1855_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50079_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Add(T)
MethodInfo ICollection_1_Add_m50079_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8869_ICollection_1_Add_m50079_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50079_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50080_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Clear()
MethodInfo ICollection_1_Clear_m50080_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50080_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeStyles_t1855_0_0_0;
static ParameterInfo ICollection_1_t8869_ICollection_1_Contains_m50081_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeStyles_t1855_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50081_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Contains(T)
MethodInfo ICollection_1_Contains_m50081_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8869_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8869_ICollection_1_Contains_m50081_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50081_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeStylesU5BU5D_t5289_0_0_0;
extern Il2CppType DateTimeStylesU5BU5D_t5289_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8869_ICollection_1_CopyTo_m50082_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeStylesU5BU5D_t5289_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50082_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50082_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8869_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8869_ICollection_1_CopyTo_m50082_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50082_GenericMethod/* genericMethod */

};
extern Il2CppType DateTimeStyles_t1855_0_0_0;
static ParameterInfo ICollection_1_t8869_ICollection_1_Remove_m50083_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeStyles_t1855_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50083_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.DateTimeStyles>::Remove(T)
MethodInfo ICollection_1_Remove_m50083_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8869_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8869_ICollection_1_Remove_m50083_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50083_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8869_MethodInfos[] =
{
	&ICollection_1_get_Count_m50077_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50078_MethodInfo,
	&ICollection_1_Add_m50079_MethodInfo,
	&ICollection_1_Clear_m50080_MethodInfo,
	&ICollection_1_Contains_m50081_MethodInfo,
	&ICollection_1_CopyTo_m50082_MethodInfo,
	&ICollection_1_Remove_m50083_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8871_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8869_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8871_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8869_0_0_0;
extern Il2CppType ICollection_1_t8869_1_0_0;
struct ICollection_1_t8869;
extern Il2CppGenericClass ICollection_1_t8869_GenericClass;
TypeInfo ICollection_1_t8869_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8869_MethodInfos/* methods */
	, ICollection_1_t8869_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8869_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8869_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8869_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8869_0_0_0/* byval_arg */
	, &ICollection_1_t8869_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8869_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeStyles>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeStyles>
extern Il2CppType IEnumerator_1_t6947_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50084_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.DateTimeStyles>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50084_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8871_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6947_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50084_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8871_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50084_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8871_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8871_0_0_0;
extern Il2CppType IEnumerable_1_t8871_1_0_0;
struct IEnumerable_1_t8871;
extern Il2CppGenericClass IEnumerable_1_t8871_GenericClass;
TypeInfo IEnumerable_1_t8871_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8871_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8871_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8871_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8871_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8871_0_0_0/* byval_arg */
	, &IEnumerable_1_t8871_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8871_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8870_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>
extern MethodInfo IList_1_get_Item_m50085_MethodInfo;
extern MethodInfo IList_1_set_Item_m50086_MethodInfo;
static PropertyInfo IList_1_t8870____Item_PropertyInfo = 
{
	&IList_1_t8870_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50085_MethodInfo/* get */
	, &IList_1_set_Item_m50086_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8870_PropertyInfos[] =
{
	&IList_1_t8870____Item_PropertyInfo,
	NULL
};
extern Il2CppType DateTimeStyles_t1855_0_0_0;
static ParameterInfo IList_1_t8870_IList_1_IndexOf_m50087_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DateTimeStyles_t1855_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50087_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50087_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8870_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8870_IList_1_IndexOf_m50087_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50087_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DateTimeStyles_t1855_0_0_0;
static ParameterInfo IList_1_t8870_IList_1_Insert_m50088_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DateTimeStyles_t1855_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50088_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50088_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8870_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8870_IList_1_Insert_m50088_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50088_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8870_IList_1_RemoveAt_m50089_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50089_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50089_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8870_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8870_IList_1_RemoveAt_m50089_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50089_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8870_IList_1_get_Item_m50085_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType DateTimeStyles_t1855_0_0_0;
extern void* RuntimeInvoker_DateTimeStyles_t1855_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50085_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50085_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8870_il2cpp_TypeInfo/* declaring_type */
	, &DateTimeStyles_t1855_0_0_0/* return_type */
	, RuntimeInvoker_DateTimeStyles_t1855_Int32_t93/* invoker_method */
	, IList_1_t8870_IList_1_get_Item_m50085_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50085_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DateTimeStyles_t1855_0_0_0;
static ParameterInfo IList_1_t8870_IList_1_set_Item_m50086_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DateTimeStyles_t1855_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50086_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.DateTimeStyles>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50086_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8870_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8870_IList_1_set_Item_m50086_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50086_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8870_MethodInfos[] =
{
	&IList_1_IndexOf_m50087_MethodInfo,
	&IList_1_Insert_m50088_MethodInfo,
	&IList_1_RemoveAt_m50089_MethodInfo,
	&IList_1_get_Item_m50085_MethodInfo,
	&IList_1_set_Item_m50086_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8870_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8869_il2cpp_TypeInfo,
	&IEnumerable_1_t8871_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8870_0_0_0;
extern Il2CppType IList_1_t8870_1_0_0;
struct IList_1_t8870;
extern Il2CppGenericClass IList_1_t8870_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8870_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8870_MethodInfos/* methods */
	, IList_1_t8870_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8870_il2cpp_TypeInfo/* element_class */
	, IList_1_t8870_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8870_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8870_0_0_0/* byval_arg */
	, &IList_1_t8870_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8870_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6949_il2cpp_TypeInfo;

// System.Globalization.GregorianCalendarTypes
#include "mscorlib_System_Globalization_GregorianCalendarTypes.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.GregorianCalendarTypes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.GregorianCalendarTypes>
extern MethodInfo IEnumerator_1_get_Current_m50090_MethodInfo;
static PropertyInfo IEnumerator_1_t6949____Current_PropertyInfo = 
{
	&IEnumerator_1_t6949_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50090_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6949_PropertyInfos[] =
{
	&IEnumerator_1_t6949____Current_PropertyInfo,
	NULL
};
extern Il2CppType GregorianCalendarTypes_t1858_0_0_0;
extern void* RuntimeInvoker_GregorianCalendarTypes_t1858 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50090_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.GregorianCalendarTypes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50090_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6949_il2cpp_TypeInfo/* declaring_type */
	, &GregorianCalendarTypes_t1858_0_0_0/* return_type */
	, RuntimeInvoker_GregorianCalendarTypes_t1858/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50090_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6949_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50090_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6949_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6949_0_0_0;
extern Il2CppType IEnumerator_1_t6949_1_0_0;
struct IEnumerator_1_t6949;
extern Il2CppGenericClass IEnumerator_1_t6949_GenericClass;
TypeInfo IEnumerator_1_t6949_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6949_MethodInfos/* methods */
	, IEnumerator_1_t6949_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6949_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6949_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6949_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6949_0_0_0/* byval_arg */
	, &IEnumerator_1_t6949_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6949_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_608.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4991_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_608MethodDeclarations.h"

extern TypeInfo GregorianCalendarTypes_t1858_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30541_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGregorianCalendarTypes_t1858_m39568_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.GregorianCalendarTypes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.GregorianCalendarTypes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisGregorianCalendarTypes_t1858_m39568 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30537_MethodInfo;
 void InternalEnumerator_1__ctor_m30537 (InternalEnumerator_1_t4991 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30538_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30538 (InternalEnumerator_1_t4991 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30541(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30541_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&GregorianCalendarTypes_t1858_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30539_MethodInfo;
 void InternalEnumerator_1_Dispose_m30539 (InternalEnumerator_1_t4991 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30540_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30540 (InternalEnumerator_1_t4991 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30541 (InternalEnumerator_1_t4991 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisGregorianCalendarTypes_t1858_m39568(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisGregorianCalendarTypes_t1858_m39568_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4991____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4991_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4991, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4991____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4991_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4991, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4991_FieldInfos[] =
{
	&InternalEnumerator_1_t4991____array_0_FieldInfo,
	&InternalEnumerator_1_t4991____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4991____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4991_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30538_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4991____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4991_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30541_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4991_PropertyInfos[] =
{
	&InternalEnumerator_1_t4991____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4991____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4991_InternalEnumerator_1__ctor_m30537_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30537_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30537_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30537/* method */
	, &InternalEnumerator_1_t4991_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4991_InternalEnumerator_1__ctor_m30537_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30537_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30538_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30538_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30538/* method */
	, &InternalEnumerator_1_t4991_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30538_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30539_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30539_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30539/* method */
	, &InternalEnumerator_1_t4991_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30539_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30540_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30540_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30540/* method */
	, &InternalEnumerator_1_t4991_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30540_GenericMethod/* genericMethod */

};
extern Il2CppType GregorianCalendarTypes_t1858_0_0_0;
extern void* RuntimeInvoker_GregorianCalendarTypes_t1858 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30541_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.GregorianCalendarTypes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30541_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30541/* method */
	, &InternalEnumerator_1_t4991_il2cpp_TypeInfo/* declaring_type */
	, &GregorianCalendarTypes_t1858_0_0_0/* return_type */
	, RuntimeInvoker_GregorianCalendarTypes_t1858/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30541_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4991_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30537_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30538_MethodInfo,
	&InternalEnumerator_1_Dispose_m30539_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30540_MethodInfo,
	&InternalEnumerator_1_get_Current_m30541_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4991_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30538_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30540_MethodInfo,
	&InternalEnumerator_1_Dispose_m30539_MethodInfo,
	&InternalEnumerator_1_get_Current_m30541_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4991_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6949_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4991_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6949_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4991_0_0_0;
extern Il2CppType InternalEnumerator_1_t4991_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4991_GenericClass;
TypeInfo InternalEnumerator_1_t4991_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4991_MethodInfos/* methods */
	, InternalEnumerator_1_t4991_PropertyInfos/* properties */
	, InternalEnumerator_1_t4991_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4991_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4991_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4991_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4991_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4991_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4991_1_0_0/* this_arg */
	, InternalEnumerator_1_t4991_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4991_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4991)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8872_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>
extern MethodInfo ICollection_1_get_Count_m50091_MethodInfo;
static PropertyInfo ICollection_1_t8872____Count_PropertyInfo = 
{
	&ICollection_1_t8872_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50091_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50092_MethodInfo;
static PropertyInfo ICollection_1_t8872____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8872_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50092_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8872_PropertyInfos[] =
{
	&ICollection_1_t8872____Count_PropertyInfo,
	&ICollection_1_t8872____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50091_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::get_Count()
MethodInfo ICollection_1_get_Count_m50091_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8872_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50091_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50092_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50092_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8872_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50092_GenericMethod/* genericMethod */

};
extern Il2CppType GregorianCalendarTypes_t1858_0_0_0;
extern Il2CppType GregorianCalendarTypes_t1858_0_0_0;
static ParameterInfo ICollection_1_t8872_ICollection_1_Add_m50093_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypes_t1858_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50093_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Add(T)
MethodInfo ICollection_1_Add_m50093_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8872_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8872_ICollection_1_Add_m50093_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50093_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50094_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Clear()
MethodInfo ICollection_1_Clear_m50094_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8872_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50094_GenericMethod/* genericMethod */

};
extern Il2CppType GregorianCalendarTypes_t1858_0_0_0;
static ParameterInfo ICollection_1_t8872_ICollection_1_Contains_m50095_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypes_t1858_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50095_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Contains(T)
MethodInfo ICollection_1_Contains_m50095_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8872_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8872_ICollection_1_Contains_m50095_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50095_GenericMethod/* genericMethod */

};
extern Il2CppType GregorianCalendarTypesU5BU5D_t5290_0_0_0;
extern Il2CppType GregorianCalendarTypesU5BU5D_t5290_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8872_ICollection_1_CopyTo_m50096_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypesU5BU5D_t5290_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50096_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50096_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8872_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8872_ICollection_1_CopyTo_m50096_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50096_GenericMethod/* genericMethod */

};
extern Il2CppType GregorianCalendarTypes_t1858_0_0_0;
static ParameterInfo ICollection_1_t8872_ICollection_1_Remove_m50097_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypes_t1858_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50097_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.GregorianCalendarTypes>::Remove(T)
MethodInfo ICollection_1_Remove_m50097_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8872_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8872_ICollection_1_Remove_m50097_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50097_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8872_MethodInfos[] =
{
	&ICollection_1_get_Count_m50091_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50092_MethodInfo,
	&ICollection_1_Add_m50093_MethodInfo,
	&ICollection_1_Clear_m50094_MethodInfo,
	&ICollection_1_Contains_m50095_MethodInfo,
	&ICollection_1_CopyTo_m50096_MethodInfo,
	&ICollection_1_Remove_m50097_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8874_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8872_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8874_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8872_0_0_0;
extern Il2CppType ICollection_1_t8872_1_0_0;
struct ICollection_1_t8872;
extern Il2CppGenericClass ICollection_1_t8872_GenericClass;
TypeInfo ICollection_1_t8872_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8872_MethodInfos/* methods */
	, ICollection_1_t8872_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8872_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8872_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8872_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8872_0_0_0/* byval_arg */
	, &ICollection_1_t8872_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8872_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.GregorianCalendarTypes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.GregorianCalendarTypes>
extern Il2CppType IEnumerator_1_t6949_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50098_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.GregorianCalendarTypes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50098_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8874_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6949_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50098_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8874_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50098_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8874_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8874_0_0_0;
extern Il2CppType IEnumerable_1_t8874_1_0_0;
struct IEnumerable_1_t8874;
extern Il2CppGenericClass IEnumerable_1_t8874_GenericClass;
TypeInfo IEnumerable_1_t8874_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8874_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8874_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8874_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8874_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8874_0_0_0/* byval_arg */
	, &IEnumerable_1_t8874_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8874_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8873_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>
extern MethodInfo IList_1_get_Item_m50099_MethodInfo;
extern MethodInfo IList_1_set_Item_m50100_MethodInfo;
static PropertyInfo IList_1_t8873____Item_PropertyInfo = 
{
	&IList_1_t8873_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50099_MethodInfo/* get */
	, &IList_1_set_Item_m50100_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8873_PropertyInfos[] =
{
	&IList_1_t8873____Item_PropertyInfo,
	NULL
};
extern Il2CppType GregorianCalendarTypes_t1858_0_0_0;
static ParameterInfo IList_1_t8873_IList_1_IndexOf_m50101_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypes_t1858_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50101_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50101_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8873_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8873_IList_1_IndexOf_m50101_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50101_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType GregorianCalendarTypes_t1858_0_0_0;
static ParameterInfo IList_1_t8873_IList_1_Insert_m50102_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypes_t1858_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50102_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50102_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8873_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8873_IList_1_Insert_m50102_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50102_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8873_IList_1_RemoveAt_m50103_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50103_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50103_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8873_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8873_IList_1_RemoveAt_m50103_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50103_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8873_IList_1_get_Item_m50099_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType GregorianCalendarTypes_t1858_0_0_0;
extern void* RuntimeInvoker_GregorianCalendarTypes_t1858_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50099_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50099_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8873_il2cpp_TypeInfo/* declaring_type */
	, &GregorianCalendarTypes_t1858_0_0_0/* return_type */
	, RuntimeInvoker_GregorianCalendarTypes_t1858_Int32_t93/* invoker_method */
	, IList_1_t8873_IList_1_get_Item_m50099_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50099_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType GregorianCalendarTypes_t1858_0_0_0;
static ParameterInfo IList_1_t8873_IList_1_set_Item_m50100_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GregorianCalendarTypes_t1858_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50100_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.GregorianCalendarTypes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50100_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8873_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8873_IList_1_set_Item_m50100_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50100_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8873_MethodInfos[] =
{
	&IList_1_IndexOf_m50101_MethodInfo,
	&IList_1_Insert_m50102_MethodInfo,
	&IList_1_RemoveAt_m50103_MethodInfo,
	&IList_1_get_Item_m50099_MethodInfo,
	&IList_1_set_Item_m50100_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8873_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8872_il2cpp_TypeInfo,
	&IEnumerable_1_t8874_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8873_0_0_0;
extern Il2CppType IList_1_t8873_1_0_0;
struct IList_1_t8873;
extern Il2CppGenericClass IList_1_t8873_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8873_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8873_MethodInfos/* methods */
	, IList_1_t8873_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8873_il2cpp_TypeInfo/* element_class */
	, IList_1_t8873_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8873_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8873_0_0_0/* byval_arg */
	, &IList_1_t8873_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8873_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6951_il2cpp_TypeInfo;

// System.Globalization.NumberStyles
#include "mscorlib_System_Globalization_NumberStyles.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.NumberStyles>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.NumberStyles>
extern MethodInfo IEnumerator_1_get_Current_m50104_MethodInfo;
static PropertyInfo IEnumerator_1_t6951____Current_PropertyInfo = 
{
	&IEnumerator_1_t6951_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50104_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6951_PropertyInfos[] =
{
	&IEnumerator_1_t6951____Current_PropertyInfo,
	NULL
};
extern Il2CppType NumberStyles_t1859_0_0_0;
extern void* RuntimeInvoker_NumberStyles_t1859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50104_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.NumberStyles>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50104_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6951_il2cpp_TypeInfo/* declaring_type */
	, &NumberStyles_t1859_0_0_0/* return_type */
	, RuntimeInvoker_NumberStyles_t1859/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50104_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6951_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50104_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6951_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6951_0_0_0;
extern Il2CppType IEnumerator_1_t6951_1_0_0;
struct IEnumerator_1_t6951;
extern Il2CppGenericClass IEnumerator_1_t6951_GenericClass;
TypeInfo IEnumerator_1_t6951_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6951_MethodInfos/* methods */
	, IEnumerator_1_t6951_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6951_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6951_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6951_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6951_0_0_0/* byval_arg */
	, &IEnumerator_1_t6951_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6951_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_609.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4992_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_609MethodDeclarations.h"

extern TypeInfo NumberStyles_t1859_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30546_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisNumberStyles_t1859_m39579_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.NumberStyles>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.NumberStyles>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisNumberStyles_t1859_m39579 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30542_MethodInfo;
 void InternalEnumerator_1__ctor_m30542 (InternalEnumerator_1_t4992 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30543_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30543 (InternalEnumerator_1_t4992 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30546(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30546_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&NumberStyles_t1859_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30544_MethodInfo;
 void InternalEnumerator_1_Dispose_m30544 (InternalEnumerator_1_t4992 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30545_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30545 (InternalEnumerator_1_t4992 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30546 (InternalEnumerator_1_t4992 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisNumberStyles_t1859_m39579(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisNumberStyles_t1859_m39579_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4992____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4992_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4992, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4992____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4992_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4992, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4992_FieldInfos[] =
{
	&InternalEnumerator_1_t4992____array_0_FieldInfo,
	&InternalEnumerator_1_t4992____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4992____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4992_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30543_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4992____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4992_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30546_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4992_PropertyInfos[] =
{
	&InternalEnumerator_1_t4992____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4992____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4992_InternalEnumerator_1__ctor_m30542_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30542_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30542_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30542/* method */
	, &InternalEnumerator_1_t4992_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4992_InternalEnumerator_1__ctor_m30542_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30542_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30543_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30543_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30543/* method */
	, &InternalEnumerator_1_t4992_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30543_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30544_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30544_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30544/* method */
	, &InternalEnumerator_1_t4992_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30544_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30545_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30545_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30545/* method */
	, &InternalEnumerator_1_t4992_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30545_GenericMethod/* genericMethod */

};
extern Il2CppType NumberStyles_t1859_0_0_0;
extern void* RuntimeInvoker_NumberStyles_t1859 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30546_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.NumberStyles>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30546_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30546/* method */
	, &InternalEnumerator_1_t4992_il2cpp_TypeInfo/* declaring_type */
	, &NumberStyles_t1859_0_0_0/* return_type */
	, RuntimeInvoker_NumberStyles_t1859/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30546_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4992_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30542_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30543_MethodInfo,
	&InternalEnumerator_1_Dispose_m30544_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30545_MethodInfo,
	&InternalEnumerator_1_get_Current_m30546_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4992_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30543_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30545_MethodInfo,
	&InternalEnumerator_1_Dispose_m30544_MethodInfo,
	&InternalEnumerator_1_get_Current_m30546_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4992_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6951_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4992_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6951_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4992_0_0_0;
extern Il2CppType InternalEnumerator_1_t4992_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4992_GenericClass;
TypeInfo InternalEnumerator_1_t4992_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4992_MethodInfos/* methods */
	, InternalEnumerator_1_t4992_PropertyInfos/* properties */
	, InternalEnumerator_1_t4992_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4992_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4992_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4992_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4992_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4992_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4992_1_0_0/* this_arg */
	, InternalEnumerator_1_t4992_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4992_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4992)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8875_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>
extern MethodInfo ICollection_1_get_Count_m50105_MethodInfo;
static PropertyInfo ICollection_1_t8875____Count_PropertyInfo = 
{
	&ICollection_1_t8875_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50105_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50106_MethodInfo;
static PropertyInfo ICollection_1_t8875____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8875_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50106_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8875_PropertyInfos[] =
{
	&ICollection_1_t8875____Count_PropertyInfo,
	&ICollection_1_t8875____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50105_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::get_Count()
MethodInfo ICollection_1_get_Count_m50105_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8875_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50105_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50106_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50106_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8875_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50106_GenericMethod/* genericMethod */

};
extern Il2CppType NumberStyles_t1859_0_0_0;
extern Il2CppType NumberStyles_t1859_0_0_0;
static ParameterInfo ICollection_1_t8875_ICollection_1_Add_m50107_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NumberStyles_t1859_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50107_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Add(T)
MethodInfo ICollection_1_Add_m50107_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8875_ICollection_1_Add_m50107_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50107_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50108_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Clear()
MethodInfo ICollection_1_Clear_m50108_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50108_GenericMethod/* genericMethod */

};
extern Il2CppType NumberStyles_t1859_0_0_0;
static ParameterInfo ICollection_1_t8875_ICollection_1_Contains_m50109_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NumberStyles_t1859_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50109_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Contains(T)
MethodInfo ICollection_1_Contains_m50109_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8875_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8875_ICollection_1_Contains_m50109_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50109_GenericMethod/* genericMethod */

};
extern Il2CppType NumberStylesU5BU5D_t5291_0_0_0;
extern Il2CppType NumberStylesU5BU5D_t5291_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8875_ICollection_1_CopyTo_m50110_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &NumberStylesU5BU5D_t5291_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50110_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50110_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8875_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8875_ICollection_1_CopyTo_m50110_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50110_GenericMethod/* genericMethod */

};
extern Il2CppType NumberStyles_t1859_0_0_0;
static ParameterInfo ICollection_1_t8875_ICollection_1_Remove_m50111_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NumberStyles_t1859_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50111_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.NumberStyles>::Remove(T)
MethodInfo ICollection_1_Remove_m50111_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8875_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8875_ICollection_1_Remove_m50111_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50111_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8875_MethodInfos[] =
{
	&ICollection_1_get_Count_m50105_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50106_MethodInfo,
	&ICollection_1_Add_m50107_MethodInfo,
	&ICollection_1_Clear_m50108_MethodInfo,
	&ICollection_1_Contains_m50109_MethodInfo,
	&ICollection_1_CopyTo_m50110_MethodInfo,
	&ICollection_1_Remove_m50111_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8877_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8875_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8877_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8875_0_0_0;
extern Il2CppType ICollection_1_t8875_1_0_0;
struct ICollection_1_t8875;
extern Il2CppGenericClass ICollection_1_t8875_GenericClass;
TypeInfo ICollection_1_t8875_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8875_MethodInfos/* methods */
	, ICollection_1_t8875_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8875_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8875_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8875_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8875_0_0_0/* byval_arg */
	, &ICollection_1_t8875_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8875_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.NumberStyles>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.NumberStyles>
extern Il2CppType IEnumerator_1_t6951_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50112_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.NumberStyles>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50112_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8877_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6951_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50112_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8877_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50112_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8877_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8877_0_0_0;
extern Il2CppType IEnumerable_1_t8877_1_0_0;
struct IEnumerable_1_t8877;
extern Il2CppGenericClass IEnumerable_1_t8877_GenericClass;
TypeInfo IEnumerable_1_t8877_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8877_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8877_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8877_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8877_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8877_0_0_0/* byval_arg */
	, &IEnumerable_1_t8877_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8877_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8876_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.NumberStyles>
extern MethodInfo IList_1_get_Item_m50113_MethodInfo;
extern MethodInfo IList_1_set_Item_m50114_MethodInfo;
static PropertyInfo IList_1_t8876____Item_PropertyInfo = 
{
	&IList_1_t8876_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50113_MethodInfo/* get */
	, &IList_1_set_Item_m50114_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8876_PropertyInfos[] =
{
	&IList_1_t8876____Item_PropertyInfo,
	NULL
};
extern Il2CppType NumberStyles_t1859_0_0_0;
static ParameterInfo IList_1_t8876_IList_1_IndexOf_m50115_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &NumberStyles_t1859_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50115_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50115_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8876_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8876_IList_1_IndexOf_m50115_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50115_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType NumberStyles_t1859_0_0_0;
static ParameterInfo IList_1_t8876_IList_1_Insert_m50116_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &NumberStyles_t1859_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50116_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50116_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8876_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8876_IList_1_Insert_m50116_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50116_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8876_IList_1_RemoveAt_m50117_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50117_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50117_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8876_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8876_IList_1_RemoveAt_m50117_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50117_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8876_IList_1_get_Item_m50113_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType NumberStyles_t1859_0_0_0;
extern void* RuntimeInvoker_NumberStyles_t1859_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50113_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50113_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8876_il2cpp_TypeInfo/* declaring_type */
	, &NumberStyles_t1859_0_0_0/* return_type */
	, RuntimeInvoker_NumberStyles_t1859_Int32_t93/* invoker_method */
	, IList_1_t8876_IList_1_get_Item_m50113_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50113_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType NumberStyles_t1859_0_0_0;
static ParameterInfo IList_1_t8876_IList_1_set_Item_m50114_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &NumberStyles_t1859_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50114_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.NumberStyles>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50114_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8876_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8876_IList_1_set_Item_m50114_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50114_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8876_MethodInfos[] =
{
	&IList_1_IndexOf_m50115_MethodInfo,
	&IList_1_Insert_m50116_MethodInfo,
	&IList_1_RemoveAt_m50117_MethodInfo,
	&IList_1_get_Item_m50113_MethodInfo,
	&IList_1_set_Item_m50114_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8876_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8875_il2cpp_TypeInfo,
	&IEnumerable_1_t8877_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8876_0_0_0;
extern Il2CppType IList_1_t8876_1_0_0;
struct IList_1_t8876;
extern Il2CppGenericClass IList_1_t8876_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8876_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8876_MethodInfos/* methods */
	, IList_1_t8876_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8876_il2cpp_TypeInfo/* element_class */
	, IList_1_t8876_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8876_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8876_0_0_0/* byval_arg */
	, &IList_1_t8876_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8876_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6953_il2cpp_TypeInfo;

// System.Globalization.UnicodeCategory
#include "mscorlib_System_Globalization_UnicodeCategory.h"


// T System.Collections.Generic.IEnumerator`1<System.Globalization.UnicodeCategory>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Globalization.UnicodeCategory>
extern MethodInfo IEnumerator_1_get_Current_m50118_MethodInfo;
static PropertyInfo IEnumerator_1_t6953____Current_PropertyInfo = 
{
	&IEnumerator_1_t6953_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50118_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6953_PropertyInfos[] =
{
	&IEnumerator_1_t6953____Current_PropertyInfo,
	NULL
};
extern Il2CppType UnicodeCategory_t1523_0_0_0;
extern void* RuntimeInvoker_UnicodeCategory_t1523 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50118_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Globalization.UnicodeCategory>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50118_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6953_il2cpp_TypeInfo/* declaring_type */
	, &UnicodeCategory_t1523_0_0_0/* return_type */
	, RuntimeInvoker_UnicodeCategory_t1523/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50118_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6953_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50118_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6953_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6953_0_0_0;
extern Il2CppType IEnumerator_1_t6953_1_0_0;
struct IEnumerator_1_t6953;
extern Il2CppGenericClass IEnumerator_1_t6953_GenericClass;
TypeInfo IEnumerator_1_t6953_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6953_MethodInfos/* methods */
	, IEnumerator_1_t6953_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6953_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6953_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6953_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6953_0_0_0/* byval_arg */
	, &IEnumerator_1_t6953_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6953_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_610.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4993_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_610MethodDeclarations.h"

extern TypeInfo UnicodeCategory_t1523_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30551_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUnicodeCategory_t1523_m39590_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Globalization.UnicodeCategory>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Globalization.UnicodeCategory>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisUnicodeCategory_t1523_m39590 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30547_MethodInfo;
 void InternalEnumerator_1__ctor_m30547 (InternalEnumerator_1_t4993 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30548_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30548 (InternalEnumerator_1_t4993 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30551(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30551_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&UnicodeCategory_t1523_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30549_MethodInfo;
 void InternalEnumerator_1_Dispose_m30549 (InternalEnumerator_1_t4993 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30550_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30550 (InternalEnumerator_1_t4993 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30551 (InternalEnumerator_1_t4993 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisUnicodeCategory_t1523_m39590(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisUnicodeCategory_t1523_m39590_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4993____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4993_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4993, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4993____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4993_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4993, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4993_FieldInfos[] =
{
	&InternalEnumerator_1_t4993____array_0_FieldInfo,
	&InternalEnumerator_1_t4993____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4993____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4993_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30548_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4993____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4993_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30551_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4993_PropertyInfos[] =
{
	&InternalEnumerator_1_t4993____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4993____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4993_InternalEnumerator_1__ctor_m30547_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30547_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30547_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30547/* method */
	, &InternalEnumerator_1_t4993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4993_InternalEnumerator_1__ctor_m30547_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30547_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30548_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30548_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30548/* method */
	, &InternalEnumerator_1_t4993_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30548_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30549_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30549_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30549/* method */
	, &InternalEnumerator_1_t4993_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30549_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30550_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30550_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30550/* method */
	, &InternalEnumerator_1_t4993_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30550_GenericMethod/* genericMethod */

};
extern Il2CppType UnicodeCategory_t1523_0_0_0;
extern void* RuntimeInvoker_UnicodeCategory_t1523 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30551_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Globalization.UnicodeCategory>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30551_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30551/* method */
	, &InternalEnumerator_1_t4993_il2cpp_TypeInfo/* declaring_type */
	, &UnicodeCategory_t1523_0_0_0/* return_type */
	, RuntimeInvoker_UnicodeCategory_t1523/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30551_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4993_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30547_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30548_MethodInfo,
	&InternalEnumerator_1_Dispose_m30549_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30550_MethodInfo,
	&InternalEnumerator_1_get_Current_m30551_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4993_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30548_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30550_MethodInfo,
	&InternalEnumerator_1_Dispose_m30549_MethodInfo,
	&InternalEnumerator_1_get_Current_m30551_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4993_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6953_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4993_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6953_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4993_0_0_0;
extern Il2CppType InternalEnumerator_1_t4993_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4993_GenericClass;
TypeInfo InternalEnumerator_1_t4993_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4993_MethodInfos/* methods */
	, InternalEnumerator_1_t4993_PropertyInfos/* properties */
	, InternalEnumerator_1_t4993_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4993_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4993_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4993_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4993_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4993_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4993_1_0_0/* this_arg */
	, InternalEnumerator_1_t4993_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4993_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4993)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8878_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>
extern MethodInfo ICollection_1_get_Count_m50119_MethodInfo;
static PropertyInfo ICollection_1_t8878____Count_PropertyInfo = 
{
	&ICollection_1_t8878_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50119_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50120_MethodInfo;
static PropertyInfo ICollection_1_t8878____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8878_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50120_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8878_PropertyInfos[] =
{
	&ICollection_1_t8878____Count_PropertyInfo,
	&ICollection_1_t8878____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50119_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::get_Count()
MethodInfo ICollection_1_get_Count_m50119_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8878_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50119_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50120_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50120_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8878_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50120_GenericMethod/* genericMethod */

};
extern Il2CppType UnicodeCategory_t1523_0_0_0;
extern Il2CppType UnicodeCategory_t1523_0_0_0;
static ParameterInfo ICollection_1_t8878_ICollection_1_Add_m50121_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnicodeCategory_t1523_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50121_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Add(T)
MethodInfo ICollection_1_Add_m50121_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8878_ICollection_1_Add_m50121_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50121_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50122_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Clear()
MethodInfo ICollection_1_Clear_m50122_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50122_GenericMethod/* genericMethod */

};
extern Il2CppType UnicodeCategory_t1523_0_0_0;
static ParameterInfo ICollection_1_t8878_ICollection_1_Contains_m50123_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnicodeCategory_t1523_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50123_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Contains(T)
MethodInfo ICollection_1_Contains_m50123_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8878_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8878_ICollection_1_Contains_m50123_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50123_GenericMethod/* genericMethod */

};
extern Il2CppType UnicodeCategoryU5BU5D_t5292_0_0_0;
extern Il2CppType UnicodeCategoryU5BU5D_t5292_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8878_ICollection_1_CopyTo_m50124_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UnicodeCategoryU5BU5D_t5292_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50124_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50124_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8878_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8878_ICollection_1_CopyTo_m50124_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50124_GenericMethod/* genericMethod */

};
extern Il2CppType UnicodeCategory_t1523_0_0_0;
static ParameterInfo ICollection_1_t8878_ICollection_1_Remove_m50125_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnicodeCategory_t1523_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50125_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Globalization.UnicodeCategory>::Remove(T)
MethodInfo ICollection_1_Remove_m50125_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8878_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8878_ICollection_1_Remove_m50125_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50125_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8878_MethodInfos[] =
{
	&ICollection_1_get_Count_m50119_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50120_MethodInfo,
	&ICollection_1_Add_m50121_MethodInfo,
	&ICollection_1_Clear_m50122_MethodInfo,
	&ICollection_1_Contains_m50123_MethodInfo,
	&ICollection_1_CopyTo_m50124_MethodInfo,
	&ICollection_1_Remove_m50125_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8880_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8878_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8880_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8878_0_0_0;
extern Il2CppType ICollection_1_t8878_1_0_0;
struct ICollection_1_t8878;
extern Il2CppGenericClass ICollection_1_t8878_GenericClass;
TypeInfo ICollection_1_t8878_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8878_MethodInfos/* methods */
	, ICollection_1_t8878_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8878_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8878_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8878_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8878_0_0_0/* byval_arg */
	, &ICollection_1_t8878_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8878_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.UnicodeCategory>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Globalization.UnicodeCategory>
extern Il2CppType IEnumerator_1_t6953_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50126_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Globalization.UnicodeCategory>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50126_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8880_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6953_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50126_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8880_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50126_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8880_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8880_0_0_0;
extern Il2CppType IEnumerable_1_t8880_1_0_0;
struct IEnumerable_1_t8880;
extern Il2CppGenericClass IEnumerable_1_t8880_GenericClass;
TypeInfo IEnumerable_1_t8880_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8880_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8880_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8880_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8880_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8880_0_0_0/* byval_arg */
	, &IEnumerable_1_t8880_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8880_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8879_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>
extern MethodInfo IList_1_get_Item_m50127_MethodInfo;
extern MethodInfo IList_1_set_Item_m50128_MethodInfo;
static PropertyInfo IList_1_t8879____Item_PropertyInfo = 
{
	&IList_1_t8879_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50127_MethodInfo/* get */
	, &IList_1_set_Item_m50128_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8879_PropertyInfos[] =
{
	&IList_1_t8879____Item_PropertyInfo,
	NULL
};
extern Il2CppType UnicodeCategory_t1523_0_0_0;
static ParameterInfo IList_1_t8879_IList_1_IndexOf_m50129_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UnicodeCategory_t1523_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50129_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50129_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8879_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8879_IList_1_IndexOf_m50129_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50129_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType UnicodeCategory_t1523_0_0_0;
static ParameterInfo IList_1_t8879_IList_1_Insert_m50130_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UnicodeCategory_t1523_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50130_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50130_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8879_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8879_IList_1_Insert_m50130_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50130_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8879_IList_1_RemoveAt_m50131_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50131_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50131_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8879_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8879_IList_1_RemoveAt_m50131_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50131_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8879_IList_1_get_Item_m50127_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType UnicodeCategory_t1523_0_0_0;
extern void* RuntimeInvoker_UnicodeCategory_t1523_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50127_GenericMethod;
// T System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50127_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8879_il2cpp_TypeInfo/* declaring_type */
	, &UnicodeCategory_t1523_0_0_0/* return_type */
	, RuntimeInvoker_UnicodeCategory_t1523_Int32_t93/* invoker_method */
	, IList_1_t8879_IList_1_get_Item_m50127_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50127_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType UnicodeCategory_t1523_0_0_0;
static ParameterInfo IList_1_t8879_IList_1_set_Item_m50128_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UnicodeCategory_t1523_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50128_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Globalization.UnicodeCategory>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50128_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8879_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8879_IList_1_set_Item_m50128_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50128_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8879_MethodInfos[] =
{
	&IList_1_IndexOf_m50129_MethodInfo,
	&IList_1_Insert_m50130_MethodInfo,
	&IList_1_RemoveAt_m50131_MethodInfo,
	&IList_1_get_Item_m50127_MethodInfo,
	&IList_1_set_Item_m50128_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8879_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8878_il2cpp_TypeInfo,
	&IEnumerable_1_t8880_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8879_0_0_0;
extern Il2CppType IList_1_t8879_1_0_0;
struct IList_1_t8879;
extern Il2CppGenericClass IList_1_t8879_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8879_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8879_MethodInfos/* methods */
	, IList_1_t8879_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8879_il2cpp_TypeInfo/* element_class */
	, IList_1_t8879_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8879_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8879_0_0_0/* byval_arg */
	, &IList_1_t8879_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8879_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6955_il2cpp_TypeInfo;

// System.IO.FileAttributes
#include "mscorlib_System_IO_FileAttributes.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.FileAttributes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileAttributes>
extern MethodInfo IEnumerator_1_get_Current_m50132_MethodInfo;
static PropertyInfo IEnumerator_1_t6955____Current_PropertyInfo = 
{
	&IEnumerator_1_t6955_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50132_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6955_PropertyInfos[] =
{
	&IEnumerator_1_t6955____Current_PropertyInfo,
	NULL
};
extern Il2CppType FileAttributes_t1871_0_0_0;
extern void* RuntimeInvoker_FileAttributes_t1871 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50132_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.FileAttributes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50132_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6955_il2cpp_TypeInfo/* declaring_type */
	, &FileAttributes_t1871_0_0_0/* return_type */
	, RuntimeInvoker_FileAttributes_t1871/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50132_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6955_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50132_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6955_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6955_0_0_0;
extern Il2CppType IEnumerator_1_t6955_1_0_0;
struct IEnumerator_1_t6955;
extern Il2CppGenericClass IEnumerator_1_t6955_GenericClass;
TypeInfo IEnumerator_1_t6955_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6955_MethodInfos/* methods */
	, IEnumerator_1_t6955_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6955_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6955_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6955_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6955_0_0_0/* byval_arg */
	, &IEnumerator_1_t6955_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6955_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.FileAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_611.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4994_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.FileAttributes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_611MethodDeclarations.h"

extern TypeInfo FileAttributes_t1871_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30556_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFileAttributes_t1871_m39601_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.FileAttributes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.FileAttributes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisFileAttributes_t1871_m39601 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.FileAttributes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30552_MethodInfo;
 void InternalEnumerator_1__ctor_m30552 (InternalEnumerator_1_t4994 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.FileAttributes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30553_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30553 (InternalEnumerator_1_t4994 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30556(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30556_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&FileAttributes_t1871_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.FileAttributes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30554_MethodInfo;
 void InternalEnumerator_1_Dispose_m30554 (InternalEnumerator_1_t4994 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileAttributes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30555_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30555 (InternalEnumerator_1_t4994 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.FileAttributes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30556 (InternalEnumerator_1_t4994 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisFileAttributes_t1871_m39601(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisFileAttributes_t1871_m39601_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileAttributes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4994____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4994_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4994, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4994____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4994_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4994, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4994_FieldInfos[] =
{
	&InternalEnumerator_1_t4994____array_0_FieldInfo,
	&InternalEnumerator_1_t4994____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4994____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4994_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30553_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4994____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4994_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30556_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4994_PropertyInfos[] =
{
	&InternalEnumerator_1_t4994____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4994____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4994_InternalEnumerator_1__ctor_m30552_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30552_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileAttributes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30552_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30552/* method */
	, &InternalEnumerator_1_t4994_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4994_InternalEnumerator_1__ctor_m30552_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30552_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30553_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.FileAttributes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30553_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30553/* method */
	, &InternalEnumerator_1_t4994_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30553_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30554_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileAttributes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30554_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30554/* method */
	, &InternalEnumerator_1_t4994_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30554_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30555_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileAttributes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30555_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30555/* method */
	, &InternalEnumerator_1_t4994_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30555_GenericMethod/* genericMethod */

};
extern Il2CppType FileAttributes_t1871_0_0_0;
extern void* RuntimeInvoker_FileAttributes_t1871 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30556_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.FileAttributes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30556_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30556/* method */
	, &InternalEnumerator_1_t4994_il2cpp_TypeInfo/* declaring_type */
	, &FileAttributes_t1871_0_0_0/* return_type */
	, RuntimeInvoker_FileAttributes_t1871/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30556_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4994_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30552_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30553_MethodInfo,
	&InternalEnumerator_1_Dispose_m30554_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30555_MethodInfo,
	&InternalEnumerator_1_get_Current_m30556_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4994_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30553_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30555_MethodInfo,
	&InternalEnumerator_1_Dispose_m30554_MethodInfo,
	&InternalEnumerator_1_get_Current_m30556_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4994_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6955_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4994_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6955_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4994_0_0_0;
extern Il2CppType InternalEnumerator_1_t4994_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4994_GenericClass;
TypeInfo InternalEnumerator_1_t4994_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4994_MethodInfos/* methods */
	, InternalEnumerator_1_t4994_PropertyInfos/* properties */
	, InternalEnumerator_1_t4994_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4994_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4994_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4994_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4994_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4994_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4994_1_0_0/* this_arg */
	, InternalEnumerator_1_t4994_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4994_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4994)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8881_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.FileAttributes>
extern MethodInfo ICollection_1_get_Count_m50133_MethodInfo;
static PropertyInfo ICollection_1_t8881____Count_PropertyInfo = 
{
	&ICollection_1_t8881_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50133_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50134_MethodInfo;
static PropertyInfo ICollection_1_t8881____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8881_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50134_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8881_PropertyInfos[] =
{
	&ICollection_1_t8881____Count_PropertyInfo,
	&ICollection_1_t8881____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50133_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::get_Count()
MethodInfo ICollection_1_get_Count_m50133_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8881_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50133_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50134_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50134_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8881_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50134_GenericMethod/* genericMethod */

};
extern Il2CppType FileAttributes_t1871_0_0_0;
extern Il2CppType FileAttributes_t1871_0_0_0;
static ParameterInfo ICollection_1_t8881_ICollection_1_Add_m50135_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileAttributes_t1871_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50135_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Add(T)
MethodInfo ICollection_1_Add_m50135_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8881_ICollection_1_Add_m50135_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50135_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50136_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Clear()
MethodInfo ICollection_1_Clear_m50136_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50136_GenericMethod/* genericMethod */

};
extern Il2CppType FileAttributes_t1871_0_0_0;
static ParameterInfo ICollection_1_t8881_ICollection_1_Contains_m50137_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileAttributes_t1871_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50137_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Contains(T)
MethodInfo ICollection_1_Contains_m50137_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8881_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8881_ICollection_1_Contains_m50137_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50137_GenericMethod/* genericMethod */

};
extern Il2CppType FileAttributesU5BU5D_t5293_0_0_0;
extern Il2CppType FileAttributesU5BU5D_t5293_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8881_ICollection_1_CopyTo_m50138_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FileAttributesU5BU5D_t5293_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50138_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50138_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8881_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8881_ICollection_1_CopyTo_m50138_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50138_GenericMethod/* genericMethod */

};
extern Il2CppType FileAttributes_t1871_0_0_0;
static ParameterInfo ICollection_1_t8881_ICollection_1_Remove_m50139_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileAttributes_t1871_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50139_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileAttributes>::Remove(T)
MethodInfo ICollection_1_Remove_m50139_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8881_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8881_ICollection_1_Remove_m50139_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50139_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8881_MethodInfos[] =
{
	&ICollection_1_get_Count_m50133_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50134_MethodInfo,
	&ICollection_1_Add_m50135_MethodInfo,
	&ICollection_1_Clear_m50136_MethodInfo,
	&ICollection_1_Contains_m50137_MethodInfo,
	&ICollection_1_CopyTo_m50138_MethodInfo,
	&ICollection_1_Remove_m50139_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8883_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8881_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8883_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8881_0_0_0;
extern Il2CppType ICollection_1_t8881_1_0_0;
struct ICollection_1_t8881;
extern Il2CppGenericClass ICollection_1_t8881_GenericClass;
TypeInfo ICollection_1_t8881_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8881_MethodInfos/* methods */
	, ICollection_1_t8881_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8881_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8881_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8881_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8881_0_0_0/* byval_arg */
	, &ICollection_1_t8881_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8881_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileAttributes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.FileAttributes>
extern Il2CppType IEnumerator_1_t6955_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50140_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileAttributes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50140_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8883_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6955_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50140_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8883_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50140_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8883_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8883_0_0_0;
extern Il2CppType IEnumerable_1_t8883_1_0_0;
struct IEnumerable_1_t8883;
extern Il2CppGenericClass IEnumerable_1_t8883_GenericClass;
TypeInfo IEnumerable_1_t8883_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8883_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8883_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8883_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8883_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8883_0_0_0/* byval_arg */
	, &IEnumerable_1_t8883_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8883_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8882_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.FileAttributes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileAttributes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileAttributes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.FileAttributes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.FileAttributes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.FileAttributes>
extern MethodInfo IList_1_get_Item_m50141_MethodInfo;
extern MethodInfo IList_1_set_Item_m50142_MethodInfo;
static PropertyInfo IList_1_t8882____Item_PropertyInfo = 
{
	&IList_1_t8882_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50141_MethodInfo/* get */
	, &IList_1_set_Item_m50142_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8882_PropertyInfos[] =
{
	&IList_1_t8882____Item_PropertyInfo,
	NULL
};
extern Il2CppType FileAttributes_t1871_0_0_0;
static ParameterInfo IList_1_t8882_IList_1_IndexOf_m50143_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileAttributes_t1871_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50143_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.FileAttributes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50143_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8882_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8882_IList_1_IndexOf_m50143_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50143_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FileAttributes_t1871_0_0_0;
static ParameterInfo IList_1_t8882_IList_1_Insert_m50144_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FileAttributes_t1871_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50144_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileAttributes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50144_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8882_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8882_IList_1_Insert_m50144_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50144_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8882_IList_1_RemoveAt_m50145_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50145_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileAttributes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50145_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8882_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8882_IList_1_RemoveAt_m50145_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50145_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8882_IList_1_get_Item_m50141_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType FileAttributes_t1871_0_0_0;
extern void* RuntimeInvoker_FileAttributes_t1871_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50141_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.FileAttributes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50141_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8882_il2cpp_TypeInfo/* declaring_type */
	, &FileAttributes_t1871_0_0_0/* return_type */
	, RuntimeInvoker_FileAttributes_t1871_Int32_t93/* invoker_method */
	, IList_1_t8882_IList_1_get_Item_m50141_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50141_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FileAttributes_t1871_0_0_0;
static ParameterInfo IList_1_t8882_IList_1_set_Item_m50142_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FileAttributes_t1871_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50142_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileAttributes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50142_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8882_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8882_IList_1_set_Item_m50142_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50142_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8882_MethodInfos[] =
{
	&IList_1_IndexOf_m50143_MethodInfo,
	&IList_1_Insert_m50144_MethodInfo,
	&IList_1_RemoveAt_m50145_MethodInfo,
	&IList_1_get_Item_m50141_MethodInfo,
	&IList_1_set_Item_m50142_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8882_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8881_il2cpp_TypeInfo,
	&IEnumerable_1_t8883_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8882_0_0_0;
extern Il2CppType IList_1_t8882_1_0_0;
struct IList_1_t8882;
extern Il2CppGenericClass IList_1_t8882_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8882_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8882_MethodInfos/* methods */
	, IList_1_t8882_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8882_il2cpp_TypeInfo/* element_class */
	, IList_1_t8882_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8882_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8882_0_0_0/* byval_arg */
	, &IList_1_t8882_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8882_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6957_il2cpp_TypeInfo;

// System.IO.FileMode
#include "mscorlib_System_IO_FileMode.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.FileMode>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileMode>
extern MethodInfo IEnumerator_1_get_Current_m50146_MethodInfo;
static PropertyInfo IEnumerator_1_t6957____Current_PropertyInfo = 
{
	&IEnumerator_1_t6957_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50146_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6957_PropertyInfos[] =
{
	&IEnumerator_1_t6957____Current_PropertyInfo,
	NULL
};
extern Il2CppType FileMode_t1872_0_0_0;
extern void* RuntimeInvoker_FileMode_t1872 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50146_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.FileMode>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50146_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6957_il2cpp_TypeInfo/* declaring_type */
	, &FileMode_t1872_0_0_0/* return_type */
	, RuntimeInvoker_FileMode_t1872/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50146_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6957_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50146_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6957_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6957_0_0_0;
extern Il2CppType IEnumerator_1_t6957_1_0_0;
struct IEnumerator_1_t6957;
extern Il2CppGenericClass IEnumerator_1_t6957_GenericClass;
TypeInfo IEnumerator_1_t6957_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6957_MethodInfos/* methods */
	, IEnumerator_1_t6957_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6957_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6957_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6957_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6957_0_0_0/* byval_arg */
	, &IEnumerator_1_t6957_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6957_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.FileMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_612.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4995_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.FileMode>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_612MethodDeclarations.h"

extern TypeInfo FileMode_t1872_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30561_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFileMode_t1872_m39612_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.FileMode>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.FileMode>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisFileMode_t1872_m39612 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.FileMode>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30557_MethodInfo;
 void InternalEnumerator_1__ctor_m30557 (InternalEnumerator_1_t4995 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.FileMode>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558 (InternalEnumerator_1_t4995 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30561(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30561_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&FileMode_t1872_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.FileMode>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30559_MethodInfo;
 void InternalEnumerator_1_Dispose_m30559 (InternalEnumerator_1_t4995 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileMode>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30560_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30560 (InternalEnumerator_1_t4995 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.FileMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30561 (InternalEnumerator_1_t4995 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisFileMode_t1872_m39612(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisFileMode_t1872_m39612_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileMode>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4995____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4995_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4995, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4995____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4995_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4995, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4995_FieldInfos[] =
{
	&InternalEnumerator_1_t4995____array_0_FieldInfo,
	&InternalEnumerator_1_t4995____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4995____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4995_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4995____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4995_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30561_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4995_PropertyInfos[] =
{
	&InternalEnumerator_1_t4995____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4995____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4995_InternalEnumerator_1__ctor_m30557_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30557_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileMode>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30557_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30557/* method */
	, &InternalEnumerator_1_t4995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4995_InternalEnumerator_1__ctor_m30557_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30557_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.FileMode>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558/* method */
	, &InternalEnumerator_1_t4995_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30559_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileMode>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30559_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30559/* method */
	, &InternalEnumerator_1_t4995_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30559_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30560_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileMode>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30560_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30560/* method */
	, &InternalEnumerator_1_t4995_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30560_GenericMethod/* genericMethod */

};
extern Il2CppType FileMode_t1872_0_0_0;
extern void* RuntimeInvoker_FileMode_t1872 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30561_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.FileMode>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30561_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30561/* method */
	, &InternalEnumerator_1_t4995_il2cpp_TypeInfo/* declaring_type */
	, &FileMode_t1872_0_0_0/* return_type */
	, RuntimeInvoker_FileMode_t1872/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30561_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4995_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30557_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_MethodInfo,
	&InternalEnumerator_1_Dispose_m30559_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30560_MethodInfo,
	&InternalEnumerator_1_get_Current_m30561_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4995_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30558_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30560_MethodInfo,
	&InternalEnumerator_1_Dispose_m30559_MethodInfo,
	&InternalEnumerator_1_get_Current_m30561_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4995_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6957_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4995_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6957_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4995_0_0_0;
extern Il2CppType InternalEnumerator_1_t4995_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4995_GenericClass;
TypeInfo InternalEnumerator_1_t4995_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4995_MethodInfos/* methods */
	, InternalEnumerator_1_t4995_PropertyInfos/* properties */
	, InternalEnumerator_1_t4995_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4995_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4995_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4995_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4995_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4995_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4995_1_0_0/* this_arg */
	, InternalEnumerator_1_t4995_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4995_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4995)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8884_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileMode>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileMode>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileMode>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileMode>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileMode>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileMode>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileMode>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.FileMode>
extern MethodInfo ICollection_1_get_Count_m50147_MethodInfo;
static PropertyInfo ICollection_1_t8884____Count_PropertyInfo = 
{
	&ICollection_1_t8884_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50147_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50148_MethodInfo;
static PropertyInfo ICollection_1_t8884____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8884_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50148_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8884_PropertyInfos[] =
{
	&ICollection_1_t8884____Count_PropertyInfo,
	&ICollection_1_t8884____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50147_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileMode>::get_Count()
MethodInfo ICollection_1_get_Count_m50147_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8884_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50147_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50148_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileMode>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50148_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8884_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50148_GenericMethod/* genericMethod */

};
extern Il2CppType FileMode_t1872_0_0_0;
extern Il2CppType FileMode_t1872_0_0_0;
static ParameterInfo ICollection_1_t8884_ICollection_1_Add_m50149_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileMode_t1872_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50149_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileMode>::Add(T)
MethodInfo ICollection_1_Add_m50149_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8884_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8884_ICollection_1_Add_m50149_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50149_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50150_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileMode>::Clear()
MethodInfo ICollection_1_Clear_m50150_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8884_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50150_GenericMethod/* genericMethod */

};
extern Il2CppType FileMode_t1872_0_0_0;
static ParameterInfo ICollection_1_t8884_ICollection_1_Contains_m50151_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileMode_t1872_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50151_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileMode>::Contains(T)
MethodInfo ICollection_1_Contains_m50151_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8884_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8884_ICollection_1_Contains_m50151_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50151_GenericMethod/* genericMethod */

};
extern Il2CppType FileModeU5BU5D_t5294_0_0_0;
extern Il2CppType FileModeU5BU5D_t5294_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8884_ICollection_1_CopyTo_m50152_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FileModeU5BU5D_t5294_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50152_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileMode>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50152_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8884_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8884_ICollection_1_CopyTo_m50152_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50152_GenericMethod/* genericMethod */

};
extern Il2CppType FileMode_t1872_0_0_0;
static ParameterInfo ICollection_1_t8884_ICollection_1_Remove_m50153_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileMode_t1872_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50153_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileMode>::Remove(T)
MethodInfo ICollection_1_Remove_m50153_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8884_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8884_ICollection_1_Remove_m50153_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50153_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8884_MethodInfos[] =
{
	&ICollection_1_get_Count_m50147_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50148_MethodInfo,
	&ICollection_1_Add_m50149_MethodInfo,
	&ICollection_1_Clear_m50150_MethodInfo,
	&ICollection_1_Contains_m50151_MethodInfo,
	&ICollection_1_CopyTo_m50152_MethodInfo,
	&ICollection_1_Remove_m50153_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8886_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8884_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8886_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8884_0_0_0;
extern Il2CppType ICollection_1_t8884_1_0_0;
struct ICollection_1_t8884;
extern Il2CppGenericClass ICollection_1_t8884_GenericClass;
TypeInfo ICollection_1_t8884_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8884_MethodInfos/* methods */
	, ICollection_1_t8884_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8884_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8884_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8884_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8884_0_0_0/* byval_arg */
	, &ICollection_1_t8884_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8884_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileMode>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.FileMode>
extern Il2CppType IEnumerator_1_t6957_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50154_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileMode>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50154_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8886_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6957_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50154_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8886_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50154_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8886_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8886_0_0_0;
extern Il2CppType IEnumerable_1_t8886_1_0_0;
struct IEnumerable_1_t8886;
extern Il2CppGenericClass IEnumerable_1_t8886_GenericClass;
TypeInfo IEnumerable_1_t8886_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8886_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8886_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8886_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8886_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8886_0_0_0/* byval_arg */
	, &IEnumerable_1_t8886_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8886_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8885_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.FileMode>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileMode>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileMode>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.FileMode>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.FileMode>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.FileMode>
extern MethodInfo IList_1_get_Item_m50155_MethodInfo;
extern MethodInfo IList_1_set_Item_m50156_MethodInfo;
static PropertyInfo IList_1_t8885____Item_PropertyInfo = 
{
	&IList_1_t8885_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50155_MethodInfo/* get */
	, &IList_1_set_Item_m50156_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8885_PropertyInfos[] =
{
	&IList_1_t8885____Item_PropertyInfo,
	NULL
};
extern Il2CppType FileMode_t1872_0_0_0;
static ParameterInfo IList_1_t8885_IList_1_IndexOf_m50157_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileMode_t1872_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50157_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.FileMode>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50157_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8885_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8885_IList_1_IndexOf_m50157_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50157_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FileMode_t1872_0_0_0;
static ParameterInfo IList_1_t8885_IList_1_Insert_m50158_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FileMode_t1872_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50158_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileMode>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50158_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8885_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8885_IList_1_Insert_m50158_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50158_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8885_IList_1_RemoveAt_m50159_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50159_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileMode>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50159_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8885_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8885_IList_1_RemoveAt_m50159_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50159_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8885_IList_1_get_Item_m50155_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType FileMode_t1872_0_0_0;
extern void* RuntimeInvoker_FileMode_t1872_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50155_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.FileMode>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50155_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8885_il2cpp_TypeInfo/* declaring_type */
	, &FileMode_t1872_0_0_0/* return_type */
	, RuntimeInvoker_FileMode_t1872_Int32_t93/* invoker_method */
	, IList_1_t8885_IList_1_get_Item_m50155_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50155_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FileMode_t1872_0_0_0;
static ParameterInfo IList_1_t8885_IList_1_set_Item_m50156_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FileMode_t1872_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50156_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileMode>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50156_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8885_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8885_IList_1_set_Item_m50156_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50156_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8885_MethodInfos[] =
{
	&IList_1_IndexOf_m50157_MethodInfo,
	&IList_1_Insert_m50158_MethodInfo,
	&IList_1_RemoveAt_m50159_MethodInfo,
	&IList_1_get_Item_m50155_MethodInfo,
	&IList_1_set_Item_m50156_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8885_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8884_il2cpp_TypeInfo,
	&IEnumerable_1_t8886_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8885_0_0_0;
extern Il2CppType IList_1_t8885_1_0_0;
struct IList_1_t8885;
extern Il2CppGenericClass IList_1_t8885_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8885_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8885_MethodInfos/* methods */
	, IList_1_t8885_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8885_il2cpp_TypeInfo/* element_class */
	, IList_1_t8885_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8885_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8885_0_0_0/* byval_arg */
	, &IList_1_t8885_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8885_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6959_il2cpp_TypeInfo;

// System.IO.FileOptions
#include "mscorlib_System_IO_FileOptions.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.FileOptions>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileOptions>
extern MethodInfo IEnumerator_1_get_Current_m50160_MethodInfo;
static PropertyInfo IEnumerator_1_t6959____Current_PropertyInfo = 
{
	&IEnumerator_1_t6959_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50160_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6959_PropertyInfos[] =
{
	&IEnumerator_1_t6959____Current_PropertyInfo,
	NULL
};
extern Il2CppType FileOptions_t1874_0_0_0;
extern void* RuntimeInvoker_FileOptions_t1874 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50160_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.FileOptions>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50160_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6959_il2cpp_TypeInfo/* declaring_type */
	, &FileOptions_t1874_0_0_0/* return_type */
	, RuntimeInvoker_FileOptions_t1874/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50160_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6959_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50160_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6959_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6959_0_0_0;
extern Il2CppType IEnumerator_1_t6959_1_0_0;
struct IEnumerator_1_t6959;
extern Il2CppGenericClass IEnumerator_1_t6959_GenericClass;
TypeInfo IEnumerator_1_t6959_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6959_MethodInfos/* methods */
	, IEnumerator_1_t6959_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6959_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6959_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6959_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6959_0_0_0/* byval_arg */
	, &IEnumerator_1_t6959_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6959_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.FileOptions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_613.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4996_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.FileOptions>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_613MethodDeclarations.h"

extern TypeInfo FileOptions_t1874_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30566_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFileOptions_t1874_m39623_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.FileOptions>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.FileOptions>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisFileOptions_t1874_m39623 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.FileOptions>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30562_MethodInfo;
 void InternalEnumerator_1__ctor_m30562 (InternalEnumerator_1_t4996 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.FileOptions>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563 (InternalEnumerator_1_t4996 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30566(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30566_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&FileOptions_t1874_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.FileOptions>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30564_MethodInfo;
 void InternalEnumerator_1_Dispose_m30564 (InternalEnumerator_1_t4996 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileOptions>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30565_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30565 (InternalEnumerator_1_t4996 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.FileOptions>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30566 (InternalEnumerator_1_t4996 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisFileOptions_t1874_m39623(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisFileOptions_t1874_m39623_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileOptions>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4996____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4996_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4996, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4996____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4996_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4996, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4996_FieldInfos[] =
{
	&InternalEnumerator_1_t4996____array_0_FieldInfo,
	&InternalEnumerator_1_t4996____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4996____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4996_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4996____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4996_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30566_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4996_PropertyInfos[] =
{
	&InternalEnumerator_1_t4996____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4996____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4996_InternalEnumerator_1__ctor_m30562_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30562_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileOptions>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30562_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30562/* method */
	, &InternalEnumerator_1_t4996_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4996_InternalEnumerator_1__ctor_m30562_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30562_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.FileOptions>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563/* method */
	, &InternalEnumerator_1_t4996_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30564_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileOptions>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30564_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30564/* method */
	, &InternalEnumerator_1_t4996_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30564_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30565_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileOptions>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30565_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30565/* method */
	, &InternalEnumerator_1_t4996_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30565_GenericMethod/* genericMethod */

};
extern Il2CppType FileOptions_t1874_0_0_0;
extern void* RuntimeInvoker_FileOptions_t1874 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30566_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.FileOptions>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30566_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30566/* method */
	, &InternalEnumerator_1_t4996_il2cpp_TypeInfo/* declaring_type */
	, &FileOptions_t1874_0_0_0/* return_type */
	, RuntimeInvoker_FileOptions_t1874/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30566_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4996_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30562_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_MethodInfo,
	&InternalEnumerator_1_Dispose_m30564_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30565_MethodInfo,
	&InternalEnumerator_1_get_Current_m30566_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4996_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30563_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30565_MethodInfo,
	&InternalEnumerator_1_Dispose_m30564_MethodInfo,
	&InternalEnumerator_1_get_Current_m30566_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4996_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6959_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4996_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6959_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4996_0_0_0;
extern Il2CppType InternalEnumerator_1_t4996_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4996_GenericClass;
TypeInfo InternalEnumerator_1_t4996_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4996_MethodInfos/* methods */
	, InternalEnumerator_1_t4996_PropertyInfos/* properties */
	, InternalEnumerator_1_t4996_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4996_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4996_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4996_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4996_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4996_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4996_1_0_0/* this_arg */
	, InternalEnumerator_1_t4996_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4996_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4996)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8887_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileOptions>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileOptions>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileOptions>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.FileOptions>
extern MethodInfo ICollection_1_get_Count_m50161_MethodInfo;
static PropertyInfo ICollection_1_t8887____Count_PropertyInfo = 
{
	&ICollection_1_t8887_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50161_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50162_MethodInfo;
static PropertyInfo ICollection_1_t8887____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8887_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50162_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8887_PropertyInfos[] =
{
	&ICollection_1_t8887____Count_PropertyInfo,
	&ICollection_1_t8887____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50161_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileOptions>::get_Count()
MethodInfo ICollection_1_get_Count_m50161_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8887_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50161_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50162_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileOptions>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50162_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8887_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50162_GenericMethod/* genericMethod */

};
extern Il2CppType FileOptions_t1874_0_0_0;
extern Il2CppType FileOptions_t1874_0_0_0;
static ParameterInfo ICollection_1_t8887_ICollection_1_Add_m50163_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileOptions_t1874_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50163_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Add(T)
MethodInfo ICollection_1_Add_m50163_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8887_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8887_ICollection_1_Add_m50163_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50163_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50164_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Clear()
MethodInfo ICollection_1_Clear_m50164_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8887_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50164_GenericMethod/* genericMethod */

};
extern Il2CppType FileOptions_t1874_0_0_0;
static ParameterInfo ICollection_1_t8887_ICollection_1_Contains_m50165_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileOptions_t1874_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50165_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Contains(T)
MethodInfo ICollection_1_Contains_m50165_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8887_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8887_ICollection_1_Contains_m50165_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50165_GenericMethod/* genericMethod */

};
extern Il2CppType FileOptionsU5BU5D_t5295_0_0_0;
extern Il2CppType FileOptionsU5BU5D_t5295_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8887_ICollection_1_CopyTo_m50166_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FileOptionsU5BU5D_t5295_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50166_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileOptions>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50166_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8887_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8887_ICollection_1_CopyTo_m50166_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50166_GenericMethod/* genericMethod */

};
extern Il2CppType FileOptions_t1874_0_0_0;
static ParameterInfo ICollection_1_t8887_ICollection_1_Remove_m50167_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileOptions_t1874_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50167_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileOptions>::Remove(T)
MethodInfo ICollection_1_Remove_m50167_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8887_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8887_ICollection_1_Remove_m50167_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50167_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8887_MethodInfos[] =
{
	&ICollection_1_get_Count_m50161_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50162_MethodInfo,
	&ICollection_1_Add_m50163_MethodInfo,
	&ICollection_1_Clear_m50164_MethodInfo,
	&ICollection_1_Contains_m50165_MethodInfo,
	&ICollection_1_CopyTo_m50166_MethodInfo,
	&ICollection_1_Remove_m50167_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8889_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8887_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8889_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8887_0_0_0;
extern Il2CppType ICollection_1_t8887_1_0_0;
struct ICollection_1_t8887;
extern Il2CppGenericClass ICollection_1_t8887_GenericClass;
TypeInfo ICollection_1_t8887_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8887_MethodInfos/* methods */
	, ICollection_1_t8887_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8887_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8887_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8887_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8887_0_0_0/* byval_arg */
	, &ICollection_1_t8887_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8887_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileOptions>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.FileOptions>
extern Il2CppType IEnumerator_1_t6959_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50168_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileOptions>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50168_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8889_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6959_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50168_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8889_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50168_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8889_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8889_0_0_0;
extern Il2CppType IEnumerable_1_t8889_1_0_0;
struct IEnumerable_1_t8889;
extern Il2CppGenericClass IEnumerable_1_t8889_GenericClass;
TypeInfo IEnumerable_1_t8889_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8889_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8889_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8889_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8889_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8889_0_0_0/* byval_arg */
	, &IEnumerable_1_t8889_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8889_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8888_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.FileOptions>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileOptions>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileOptions>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.FileOptions>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.FileOptions>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.FileOptions>
extern MethodInfo IList_1_get_Item_m50169_MethodInfo;
extern MethodInfo IList_1_set_Item_m50170_MethodInfo;
static PropertyInfo IList_1_t8888____Item_PropertyInfo = 
{
	&IList_1_t8888_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50169_MethodInfo/* get */
	, &IList_1_set_Item_m50170_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8888_PropertyInfos[] =
{
	&IList_1_t8888____Item_PropertyInfo,
	NULL
};
extern Il2CppType FileOptions_t1874_0_0_0;
static ParameterInfo IList_1_t8888_IList_1_IndexOf_m50171_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileOptions_t1874_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50171_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.FileOptions>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50171_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8888_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8888_IList_1_IndexOf_m50171_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50171_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FileOptions_t1874_0_0_0;
static ParameterInfo IList_1_t8888_IList_1_Insert_m50172_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FileOptions_t1874_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50172_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileOptions>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50172_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8888_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8888_IList_1_Insert_m50172_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50172_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8888_IList_1_RemoveAt_m50173_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50173_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileOptions>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50173_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8888_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8888_IList_1_RemoveAt_m50173_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50173_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8888_IList_1_get_Item_m50169_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType FileOptions_t1874_0_0_0;
extern void* RuntimeInvoker_FileOptions_t1874_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50169_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.FileOptions>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50169_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8888_il2cpp_TypeInfo/* declaring_type */
	, &FileOptions_t1874_0_0_0/* return_type */
	, RuntimeInvoker_FileOptions_t1874_Int32_t93/* invoker_method */
	, IList_1_t8888_IList_1_get_Item_m50169_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50169_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FileOptions_t1874_0_0_0;
static ParameterInfo IList_1_t8888_IList_1_set_Item_m50170_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FileOptions_t1874_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50170_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileOptions>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50170_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8888_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8888_IList_1_set_Item_m50170_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50170_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8888_MethodInfos[] =
{
	&IList_1_IndexOf_m50171_MethodInfo,
	&IList_1_Insert_m50172_MethodInfo,
	&IList_1_RemoveAt_m50173_MethodInfo,
	&IList_1_get_Item_m50169_MethodInfo,
	&IList_1_set_Item_m50170_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8888_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8887_il2cpp_TypeInfo,
	&IEnumerable_1_t8889_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8888_0_0_0;
extern Il2CppType IList_1_t8888_1_0_0;
struct IList_1_t8888;
extern Il2CppGenericClass IList_1_t8888_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8888_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8888_MethodInfos/* methods */
	, IList_1_t8888_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8888_il2cpp_TypeInfo/* element_class */
	, IList_1_t8888_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8888_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8888_0_0_0/* byval_arg */
	, &IList_1_t8888_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8888_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6961_il2cpp_TypeInfo;

// System.IO.FileShare
#include "mscorlib_System_IO_FileShare.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.FileShare>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileShare>
extern MethodInfo IEnumerator_1_get_Current_m50174_MethodInfo;
static PropertyInfo IEnumerator_1_t6961____Current_PropertyInfo = 
{
	&IEnumerator_1_t6961_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50174_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6961_PropertyInfos[] =
{
	&IEnumerator_1_t6961____Current_PropertyInfo,
	NULL
};
extern Il2CppType FileShare_t1875_0_0_0;
extern void* RuntimeInvoker_FileShare_t1875 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50174_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.FileShare>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50174_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6961_il2cpp_TypeInfo/* declaring_type */
	, &FileShare_t1875_0_0_0/* return_type */
	, RuntimeInvoker_FileShare_t1875/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50174_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6961_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50174_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6961_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6961_0_0_0;
extern Il2CppType IEnumerator_1_t6961_1_0_0;
struct IEnumerator_1_t6961;
extern Il2CppGenericClass IEnumerator_1_t6961_GenericClass;
TypeInfo IEnumerator_1_t6961_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6961_MethodInfos/* methods */
	, IEnumerator_1_t6961_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6961_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6961_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6961_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6961_0_0_0/* byval_arg */
	, &IEnumerator_1_t6961_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6961_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.FileShare>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_614.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4997_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.FileShare>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_614MethodDeclarations.h"

extern TypeInfo FileShare_t1875_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30571_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFileShare_t1875_m39634_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.FileShare>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.FileShare>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisFileShare_t1875_m39634 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.FileShare>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30567_MethodInfo;
 void InternalEnumerator_1__ctor_m30567 (InternalEnumerator_1_t4997 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.FileShare>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568 (InternalEnumerator_1_t4997 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30571(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30571_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&FileShare_t1875_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.FileShare>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30569_MethodInfo;
 void InternalEnumerator_1_Dispose_m30569 (InternalEnumerator_1_t4997 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileShare>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30570_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30570 (InternalEnumerator_1_t4997 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.FileShare>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30571 (InternalEnumerator_1_t4997 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisFileShare_t1875_m39634(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisFileShare_t1875_m39634_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileShare>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4997____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4997_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4997, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4997____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4997_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4997, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4997_FieldInfos[] =
{
	&InternalEnumerator_1_t4997____array_0_FieldInfo,
	&InternalEnumerator_1_t4997____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4997____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4997_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4997____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4997_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30571_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4997_PropertyInfos[] =
{
	&InternalEnumerator_1_t4997____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4997____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4997_InternalEnumerator_1__ctor_m30567_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30567_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileShare>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30567_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30567/* method */
	, &InternalEnumerator_1_t4997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4997_InternalEnumerator_1__ctor_m30567_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30567_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.FileShare>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568/* method */
	, &InternalEnumerator_1_t4997_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30569_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileShare>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30569_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30569/* method */
	, &InternalEnumerator_1_t4997_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30569_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30570_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileShare>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30570_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30570/* method */
	, &InternalEnumerator_1_t4997_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30570_GenericMethod/* genericMethod */

};
extern Il2CppType FileShare_t1875_0_0_0;
extern void* RuntimeInvoker_FileShare_t1875 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30571_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.FileShare>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30571_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30571/* method */
	, &InternalEnumerator_1_t4997_il2cpp_TypeInfo/* declaring_type */
	, &FileShare_t1875_0_0_0/* return_type */
	, RuntimeInvoker_FileShare_t1875/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30571_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4997_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30567_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_MethodInfo,
	&InternalEnumerator_1_Dispose_m30569_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30570_MethodInfo,
	&InternalEnumerator_1_get_Current_m30571_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4997_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30568_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30570_MethodInfo,
	&InternalEnumerator_1_Dispose_m30569_MethodInfo,
	&InternalEnumerator_1_get_Current_m30571_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4997_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6961_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4997_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6961_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4997_0_0_0;
extern Il2CppType InternalEnumerator_1_t4997_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4997_GenericClass;
TypeInfo InternalEnumerator_1_t4997_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4997_MethodInfos/* methods */
	, InternalEnumerator_1_t4997_PropertyInfos/* properties */
	, InternalEnumerator_1_t4997_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4997_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4997_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4997_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4997_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4997_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4997_1_0_0/* this_arg */
	, InternalEnumerator_1_t4997_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4997_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4997)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8890_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileShare>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileShare>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileShare>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileShare>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileShare>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileShare>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileShare>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.FileShare>
extern MethodInfo ICollection_1_get_Count_m50175_MethodInfo;
static PropertyInfo ICollection_1_t8890____Count_PropertyInfo = 
{
	&ICollection_1_t8890_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50175_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50176_MethodInfo;
static PropertyInfo ICollection_1_t8890____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8890_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50176_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8890_PropertyInfos[] =
{
	&ICollection_1_t8890____Count_PropertyInfo,
	&ICollection_1_t8890____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50175_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.FileShare>::get_Count()
MethodInfo ICollection_1_get_Count_m50175_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8890_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50175_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50176_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileShare>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50176_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8890_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50176_GenericMethod/* genericMethod */

};
extern Il2CppType FileShare_t1875_0_0_0;
extern Il2CppType FileShare_t1875_0_0_0;
static ParameterInfo ICollection_1_t8890_ICollection_1_Add_m50177_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileShare_t1875_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50177_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileShare>::Add(T)
MethodInfo ICollection_1_Add_m50177_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8890_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8890_ICollection_1_Add_m50177_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50177_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50178_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileShare>::Clear()
MethodInfo ICollection_1_Clear_m50178_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8890_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50178_GenericMethod/* genericMethod */

};
extern Il2CppType FileShare_t1875_0_0_0;
static ParameterInfo ICollection_1_t8890_ICollection_1_Contains_m50179_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileShare_t1875_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50179_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileShare>::Contains(T)
MethodInfo ICollection_1_Contains_m50179_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8890_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8890_ICollection_1_Contains_m50179_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50179_GenericMethod/* genericMethod */

};
extern Il2CppType FileShareU5BU5D_t5296_0_0_0;
extern Il2CppType FileShareU5BU5D_t5296_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8890_ICollection_1_CopyTo_m50180_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FileShareU5BU5D_t5296_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50180_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.FileShare>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50180_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8890_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8890_ICollection_1_CopyTo_m50180_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50180_GenericMethod/* genericMethod */

};
extern Il2CppType FileShare_t1875_0_0_0;
static ParameterInfo ICollection_1_t8890_ICollection_1_Remove_m50181_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileShare_t1875_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50181_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.FileShare>::Remove(T)
MethodInfo ICollection_1_Remove_m50181_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8890_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8890_ICollection_1_Remove_m50181_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50181_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8890_MethodInfos[] =
{
	&ICollection_1_get_Count_m50175_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50176_MethodInfo,
	&ICollection_1_Add_m50177_MethodInfo,
	&ICollection_1_Clear_m50178_MethodInfo,
	&ICollection_1_Contains_m50179_MethodInfo,
	&ICollection_1_CopyTo_m50180_MethodInfo,
	&ICollection_1_Remove_m50181_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8892_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8890_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8892_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8890_0_0_0;
extern Il2CppType ICollection_1_t8890_1_0_0;
struct ICollection_1_t8890;
extern Il2CppGenericClass ICollection_1_t8890_GenericClass;
TypeInfo ICollection_1_t8890_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8890_MethodInfos/* methods */
	, ICollection_1_t8890_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8890_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8890_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8890_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8890_0_0_0/* byval_arg */
	, &ICollection_1_t8890_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8890_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileShare>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.FileShare>
extern Il2CppType IEnumerator_1_t6961_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50182_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.FileShare>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50182_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8892_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6961_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50182_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8892_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50182_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8892_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8892_0_0_0;
extern Il2CppType IEnumerable_1_t8892_1_0_0;
struct IEnumerable_1_t8892;
extern Il2CppGenericClass IEnumerable_1_t8892_GenericClass;
TypeInfo IEnumerable_1_t8892_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8892_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8892_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8892_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8892_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8892_0_0_0/* byval_arg */
	, &IEnumerable_1_t8892_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8892_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8891_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.FileShare>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileShare>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.FileShare>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.FileShare>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.FileShare>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.FileShare>
extern MethodInfo IList_1_get_Item_m50183_MethodInfo;
extern MethodInfo IList_1_set_Item_m50184_MethodInfo;
static PropertyInfo IList_1_t8891____Item_PropertyInfo = 
{
	&IList_1_t8891_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50183_MethodInfo/* get */
	, &IList_1_set_Item_m50184_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8891_PropertyInfos[] =
{
	&IList_1_t8891____Item_PropertyInfo,
	NULL
};
extern Il2CppType FileShare_t1875_0_0_0;
static ParameterInfo IList_1_t8891_IList_1_IndexOf_m50185_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FileShare_t1875_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50185_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.FileShare>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50185_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8891_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8891_IList_1_IndexOf_m50185_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50185_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FileShare_t1875_0_0_0;
static ParameterInfo IList_1_t8891_IList_1_Insert_m50186_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FileShare_t1875_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50186_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileShare>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50186_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8891_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8891_IList_1_Insert_m50186_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50186_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8891_IList_1_RemoveAt_m50187_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50187_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileShare>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50187_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8891_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8891_IList_1_RemoveAt_m50187_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50187_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8891_IList_1_get_Item_m50183_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType FileShare_t1875_0_0_0;
extern void* RuntimeInvoker_FileShare_t1875_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50183_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.FileShare>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50183_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8891_il2cpp_TypeInfo/* declaring_type */
	, &FileShare_t1875_0_0_0/* return_type */
	, RuntimeInvoker_FileShare_t1875_Int32_t93/* invoker_method */
	, IList_1_t8891_IList_1_get_Item_m50183_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50183_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FileShare_t1875_0_0_0;
static ParameterInfo IList_1_t8891_IList_1_set_Item_m50184_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FileShare_t1875_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50184_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.FileShare>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50184_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8891_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8891_IList_1_set_Item_m50184_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50184_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8891_MethodInfos[] =
{
	&IList_1_IndexOf_m50185_MethodInfo,
	&IList_1_Insert_m50186_MethodInfo,
	&IList_1_RemoveAt_m50187_MethodInfo,
	&IList_1_get_Item_m50183_MethodInfo,
	&IList_1_set_Item_m50184_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8891_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8890_il2cpp_TypeInfo,
	&IEnumerable_1_t8892_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8891_0_0_0;
extern Il2CppType IList_1_t8891_1_0_0;
struct IList_1_t8891;
extern Il2CppGenericClass IList_1_t8891_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8891_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8891_MethodInfos/* methods */
	, IList_1_t8891_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8891_il2cpp_TypeInfo/* element_class */
	, IList_1_t8891_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8891_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8891_0_0_0/* byval_arg */
	, &IList_1_t8891_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8891_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6963_il2cpp_TypeInfo;

// System.IO.MonoFileType
#include "mscorlib_System_IO_MonoFileType.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.MonoFileType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.MonoFileType>
extern MethodInfo IEnumerator_1_get_Current_m50188_MethodInfo;
static PropertyInfo IEnumerator_1_t6963____Current_PropertyInfo = 
{
	&IEnumerator_1_t6963_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50188_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6963_PropertyInfos[] =
{
	&IEnumerator_1_t6963____Current_PropertyInfo,
	NULL
};
extern Il2CppType MonoFileType_t1880_0_0_0;
extern void* RuntimeInvoker_MonoFileType_t1880 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50188_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.MonoFileType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50188_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6963_il2cpp_TypeInfo/* declaring_type */
	, &MonoFileType_t1880_0_0_0/* return_type */
	, RuntimeInvoker_MonoFileType_t1880/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50188_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6963_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50188_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6963_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6963_0_0_0;
extern Il2CppType IEnumerator_1_t6963_1_0_0;
struct IEnumerator_1_t6963;
extern Il2CppGenericClass IEnumerator_1_t6963_GenericClass;
TypeInfo IEnumerator_1_t6963_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6963_MethodInfos/* methods */
	, IEnumerator_1_t6963_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6963_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6963_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6963_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6963_0_0_0/* byval_arg */
	, &IEnumerator_1_t6963_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6963_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.MonoFileType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_615.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4998_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.MonoFileType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_615MethodDeclarations.h"

extern TypeInfo MonoFileType_t1880_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30576_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMonoFileType_t1880_m39645_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.MonoFileType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.MonoFileType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisMonoFileType_t1880_m39645 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.MonoFileType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30572_MethodInfo;
 void InternalEnumerator_1__ctor_m30572 (InternalEnumerator_1_t4998 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.MonoFileType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573 (InternalEnumerator_1_t4998 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30576(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30576_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&MonoFileType_t1880_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.MonoFileType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30574_MethodInfo;
 void InternalEnumerator_1_Dispose_m30574 (InternalEnumerator_1_t4998 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.MonoFileType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30575_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30575 (InternalEnumerator_1_t4998 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.MonoFileType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30576 (InternalEnumerator_1_t4998 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisMonoFileType_t1880_m39645(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisMonoFileType_t1880_m39645_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.MonoFileType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4998____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4998_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4998, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4998____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4998_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4998, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4998_FieldInfos[] =
{
	&InternalEnumerator_1_t4998____array_0_FieldInfo,
	&InternalEnumerator_1_t4998____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4998____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4998_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4998____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4998_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30576_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4998_PropertyInfos[] =
{
	&InternalEnumerator_1_t4998____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4998____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4998_InternalEnumerator_1__ctor_m30572_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30572_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.MonoFileType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30572_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30572/* method */
	, &InternalEnumerator_1_t4998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4998_InternalEnumerator_1__ctor_m30572_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30572_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.MonoFileType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573/* method */
	, &InternalEnumerator_1_t4998_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30574_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.MonoFileType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30574_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30574/* method */
	, &InternalEnumerator_1_t4998_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30574_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30575_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.MonoFileType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30575_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30575/* method */
	, &InternalEnumerator_1_t4998_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30575_GenericMethod/* genericMethod */

};
extern Il2CppType MonoFileType_t1880_0_0_0;
extern void* RuntimeInvoker_MonoFileType_t1880 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30576_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.MonoFileType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30576_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30576/* method */
	, &InternalEnumerator_1_t4998_il2cpp_TypeInfo/* declaring_type */
	, &MonoFileType_t1880_0_0_0/* return_type */
	, RuntimeInvoker_MonoFileType_t1880/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30576_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4998_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30572_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_MethodInfo,
	&InternalEnumerator_1_Dispose_m30574_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30575_MethodInfo,
	&InternalEnumerator_1_get_Current_m30576_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4998_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30573_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30575_MethodInfo,
	&InternalEnumerator_1_Dispose_m30574_MethodInfo,
	&InternalEnumerator_1_get_Current_m30576_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4998_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6963_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4998_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6963_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4998_0_0_0;
extern Il2CppType InternalEnumerator_1_t4998_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4998_GenericClass;
TypeInfo InternalEnumerator_1_t4998_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4998_MethodInfos/* methods */
	, InternalEnumerator_1_t4998_PropertyInfos/* properties */
	, InternalEnumerator_1_t4998_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4998_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4998_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4998_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4998_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4998_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4998_1_0_0/* this_arg */
	, InternalEnumerator_1_t4998_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4998_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4998)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8893_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.MonoFileType>
extern MethodInfo ICollection_1_get_Count_m50189_MethodInfo;
static PropertyInfo ICollection_1_t8893____Count_PropertyInfo = 
{
	&ICollection_1_t8893_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50189_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50190_MethodInfo;
static PropertyInfo ICollection_1_t8893____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8893_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50190_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8893_PropertyInfos[] =
{
	&ICollection_1_t8893____Count_PropertyInfo,
	&ICollection_1_t8893____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50189_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::get_Count()
MethodInfo ICollection_1_get_Count_m50189_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8893_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50189_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50190_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50190_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8893_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50190_GenericMethod/* genericMethod */

};
extern Il2CppType MonoFileType_t1880_0_0_0;
extern Il2CppType MonoFileType_t1880_0_0_0;
static ParameterInfo ICollection_1_t8893_ICollection_1_Add_m50191_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoFileType_t1880_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50191_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Add(T)
MethodInfo ICollection_1_Add_m50191_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8893_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8893_ICollection_1_Add_m50191_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50191_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50192_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Clear()
MethodInfo ICollection_1_Clear_m50192_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8893_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50192_GenericMethod/* genericMethod */

};
extern Il2CppType MonoFileType_t1880_0_0_0;
static ParameterInfo ICollection_1_t8893_ICollection_1_Contains_m50193_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoFileType_t1880_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50193_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Contains(T)
MethodInfo ICollection_1_Contains_m50193_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8893_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8893_ICollection_1_Contains_m50193_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50193_GenericMethod/* genericMethod */

};
extern Il2CppType MonoFileTypeU5BU5D_t5297_0_0_0;
extern Il2CppType MonoFileTypeU5BU5D_t5297_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8893_ICollection_1_CopyTo_m50194_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MonoFileTypeU5BU5D_t5297_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50194_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50194_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8893_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8893_ICollection_1_CopyTo_m50194_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50194_GenericMethod/* genericMethod */

};
extern Il2CppType MonoFileType_t1880_0_0_0;
static ParameterInfo ICollection_1_t8893_ICollection_1_Remove_m50195_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoFileType_t1880_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50195_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoFileType>::Remove(T)
MethodInfo ICollection_1_Remove_m50195_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8893_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8893_ICollection_1_Remove_m50195_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50195_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8893_MethodInfos[] =
{
	&ICollection_1_get_Count_m50189_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50190_MethodInfo,
	&ICollection_1_Add_m50191_MethodInfo,
	&ICollection_1_Clear_m50192_MethodInfo,
	&ICollection_1_Contains_m50193_MethodInfo,
	&ICollection_1_CopyTo_m50194_MethodInfo,
	&ICollection_1_Remove_m50195_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8895_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8893_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8895_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8893_0_0_0;
extern Il2CppType ICollection_1_t8893_1_0_0;
struct ICollection_1_t8893;
extern Il2CppGenericClass ICollection_1_t8893_GenericClass;
TypeInfo ICollection_1_t8893_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8893_MethodInfos/* methods */
	, ICollection_1_t8893_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8893_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8893_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8893_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8893_0_0_0/* byval_arg */
	, &ICollection_1_t8893_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8893_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.MonoFileType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.MonoFileType>
extern Il2CppType IEnumerator_1_t6963_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50196_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.MonoFileType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50196_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8895_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6963_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50196_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8895_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50196_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8895_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8895_0_0_0;
extern Il2CppType IEnumerable_1_t8895_1_0_0;
struct IEnumerable_1_t8895;
extern Il2CppGenericClass IEnumerable_1_t8895_GenericClass;
TypeInfo IEnumerable_1_t8895_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8895_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8895_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8895_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8895_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8895_0_0_0/* byval_arg */
	, &IEnumerable_1_t8895_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8895_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8894_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.MonoFileType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.MonoFileType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.MonoFileType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.MonoFileType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.MonoFileType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.MonoFileType>
extern MethodInfo IList_1_get_Item_m50197_MethodInfo;
extern MethodInfo IList_1_set_Item_m50198_MethodInfo;
static PropertyInfo IList_1_t8894____Item_PropertyInfo = 
{
	&IList_1_t8894_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50197_MethodInfo/* get */
	, &IList_1_set_Item_m50198_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8894_PropertyInfos[] =
{
	&IList_1_t8894____Item_PropertyInfo,
	NULL
};
extern Il2CppType MonoFileType_t1880_0_0_0;
static ParameterInfo IList_1_t8894_IList_1_IndexOf_m50199_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoFileType_t1880_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50199_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.MonoFileType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50199_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8894_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8894_IList_1_IndexOf_m50199_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50199_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MonoFileType_t1880_0_0_0;
static ParameterInfo IList_1_t8894_IList_1_Insert_m50200_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MonoFileType_t1880_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50200_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.MonoFileType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50200_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8894_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8894_IList_1_Insert_m50200_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50200_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8894_IList_1_RemoveAt_m50201_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50201_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.MonoFileType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50201_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8894_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8894_IList_1_RemoveAt_m50201_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50201_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8894_IList_1_get_Item_m50197_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType MonoFileType_t1880_0_0_0;
extern void* RuntimeInvoker_MonoFileType_t1880_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50197_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.MonoFileType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50197_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8894_il2cpp_TypeInfo/* declaring_type */
	, &MonoFileType_t1880_0_0_0/* return_type */
	, RuntimeInvoker_MonoFileType_t1880_Int32_t93/* invoker_method */
	, IList_1_t8894_IList_1_get_Item_m50197_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50197_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MonoFileType_t1880_0_0_0;
static ParameterInfo IList_1_t8894_IList_1_set_Item_m50198_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MonoFileType_t1880_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50198_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.MonoFileType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50198_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8894_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8894_IList_1_set_Item_m50198_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50198_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8894_MethodInfos[] =
{
	&IList_1_IndexOf_m50199_MethodInfo,
	&IList_1_Insert_m50200_MethodInfo,
	&IList_1_RemoveAt_m50201_MethodInfo,
	&IList_1_get_Item_m50197_MethodInfo,
	&IList_1_set_Item_m50198_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8894_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8893_il2cpp_TypeInfo,
	&IEnumerable_1_t8895_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8894_0_0_0;
extern Il2CppType IList_1_t8894_1_0_0;
struct IList_1_t8894;
extern Il2CppGenericClass IList_1_t8894_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8894_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8894_MethodInfos/* methods */
	, IList_1_t8894_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8894_il2cpp_TypeInfo/* element_class */
	, IList_1_t8894_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8894_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8894_0_0_0/* byval_arg */
	, &IList_1_t8894_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8894_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6964_il2cpp_TypeInfo;

// System.IO.MonoIOError
#include "mscorlib_System_IO_MonoIOError.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.MonoIOError>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.MonoIOError>
extern MethodInfo IEnumerator_1_get_Current_m50202_MethodInfo;
static PropertyInfo IEnumerator_1_t6964____Current_PropertyInfo = 
{
	&IEnumerator_1_t6964_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50202_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6964_PropertyInfos[] =
{
	&IEnumerator_1_t6964____Current_PropertyInfo,
	NULL
};
extern Il2CppType MonoIOError_t1882_0_0_0;
extern void* RuntimeInvoker_MonoIOError_t1882 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50202_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.MonoIOError>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50202_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6964_il2cpp_TypeInfo/* declaring_type */
	, &MonoIOError_t1882_0_0_0/* return_type */
	, RuntimeInvoker_MonoIOError_t1882/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50202_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6964_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50202_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6964_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6964_0_0_0;
extern Il2CppType IEnumerator_1_t6964_1_0_0;
struct IEnumerator_1_t6964;
extern Il2CppGenericClass IEnumerator_1_t6964_GenericClass;
TypeInfo IEnumerator_1_t6964_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6964_MethodInfos/* methods */
	, IEnumerator_1_t6964_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6964_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6964_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6964_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6964_0_0_0/* byval_arg */
	, &IEnumerator_1_t6964_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6964_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.MonoIOError>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_616.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4999_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.MonoIOError>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_616MethodDeclarations.h"

extern TypeInfo MonoIOError_t1882_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30581_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMonoIOError_t1882_m39656_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.MonoIOError>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.MonoIOError>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisMonoIOError_t1882_m39656 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.MonoIOError>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30577_MethodInfo;
 void InternalEnumerator_1__ctor_m30577 (InternalEnumerator_1_t4999 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.MonoIOError>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578 (InternalEnumerator_1_t4999 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30581(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30581_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&MonoIOError_t1882_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.MonoIOError>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30579_MethodInfo;
 void InternalEnumerator_1_Dispose_m30579 (InternalEnumerator_1_t4999 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.MonoIOError>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30580_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30580 (InternalEnumerator_1_t4999 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.MonoIOError>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30581 (InternalEnumerator_1_t4999 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisMonoIOError_t1882_m39656(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisMonoIOError_t1882_m39656_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.MonoIOError>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4999____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4999_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4999, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4999____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4999_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4999, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4999_FieldInfos[] =
{
	&InternalEnumerator_1_t4999____array_0_FieldInfo,
	&InternalEnumerator_1_t4999____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4999____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4999_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4999____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4999_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30581_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4999_PropertyInfos[] =
{
	&InternalEnumerator_1_t4999____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4999____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4999_InternalEnumerator_1__ctor_m30577_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30577_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.MonoIOError>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30577_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30577/* method */
	, &InternalEnumerator_1_t4999_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4999_InternalEnumerator_1__ctor_m30577_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30577_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.MonoIOError>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578/* method */
	, &InternalEnumerator_1_t4999_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30579_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.MonoIOError>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30579_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30579/* method */
	, &InternalEnumerator_1_t4999_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30579_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30580_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.MonoIOError>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30580_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30580/* method */
	, &InternalEnumerator_1_t4999_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30580_GenericMethod/* genericMethod */

};
extern Il2CppType MonoIOError_t1882_0_0_0;
extern void* RuntimeInvoker_MonoIOError_t1882 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30581_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.MonoIOError>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30581_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30581/* method */
	, &InternalEnumerator_1_t4999_il2cpp_TypeInfo/* declaring_type */
	, &MonoIOError_t1882_0_0_0/* return_type */
	, RuntimeInvoker_MonoIOError_t1882/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30581_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4999_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30577_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_MethodInfo,
	&InternalEnumerator_1_Dispose_m30579_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30580_MethodInfo,
	&InternalEnumerator_1_get_Current_m30581_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4999_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30578_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30580_MethodInfo,
	&InternalEnumerator_1_Dispose_m30579_MethodInfo,
	&InternalEnumerator_1_get_Current_m30581_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4999_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6964_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4999_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6964_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4999_0_0_0;
extern Il2CppType InternalEnumerator_1_t4999_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4999_GenericClass;
TypeInfo InternalEnumerator_1_t4999_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4999_MethodInfos/* methods */
	, InternalEnumerator_1_t4999_PropertyInfos/* properties */
	, InternalEnumerator_1_t4999_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4999_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4999_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4999_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4999_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4999_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4999_1_0_0/* this_arg */
	, InternalEnumerator_1_t4999_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4999_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4999)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8896_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.MonoIOError>
extern MethodInfo ICollection_1_get_Count_m50203_MethodInfo;
static PropertyInfo ICollection_1_t8896____Count_PropertyInfo = 
{
	&ICollection_1_t8896_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50203_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50204_MethodInfo;
static PropertyInfo ICollection_1_t8896____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8896_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50204_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8896_PropertyInfos[] =
{
	&ICollection_1_t8896____Count_PropertyInfo,
	&ICollection_1_t8896____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50203_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::get_Count()
MethodInfo ICollection_1_get_Count_m50203_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8896_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50203_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50204_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50204_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8896_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50204_GenericMethod/* genericMethod */

};
extern Il2CppType MonoIOError_t1882_0_0_0;
extern Il2CppType MonoIOError_t1882_0_0_0;
static ParameterInfo ICollection_1_t8896_ICollection_1_Add_m50205_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoIOError_t1882_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50205_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Add(T)
MethodInfo ICollection_1_Add_m50205_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8896_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8896_ICollection_1_Add_m50205_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50205_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50206_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Clear()
MethodInfo ICollection_1_Clear_m50206_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8896_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50206_GenericMethod/* genericMethod */

};
extern Il2CppType MonoIOError_t1882_0_0_0;
static ParameterInfo ICollection_1_t8896_ICollection_1_Contains_m50207_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoIOError_t1882_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50207_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Contains(T)
MethodInfo ICollection_1_Contains_m50207_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8896_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8896_ICollection_1_Contains_m50207_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50207_GenericMethod/* genericMethod */

};
extern Il2CppType MonoIOErrorU5BU5D_t5298_0_0_0;
extern Il2CppType MonoIOErrorU5BU5D_t5298_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8896_ICollection_1_CopyTo_m50208_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MonoIOErrorU5BU5D_t5298_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50208_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50208_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8896_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8896_ICollection_1_CopyTo_m50208_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50208_GenericMethod/* genericMethod */

};
extern Il2CppType MonoIOError_t1882_0_0_0;
static ParameterInfo ICollection_1_t8896_ICollection_1_Remove_m50209_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoIOError_t1882_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50209_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.MonoIOError>::Remove(T)
MethodInfo ICollection_1_Remove_m50209_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8896_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8896_ICollection_1_Remove_m50209_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50209_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8896_MethodInfos[] =
{
	&ICollection_1_get_Count_m50203_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50204_MethodInfo,
	&ICollection_1_Add_m50205_MethodInfo,
	&ICollection_1_Clear_m50206_MethodInfo,
	&ICollection_1_Contains_m50207_MethodInfo,
	&ICollection_1_CopyTo_m50208_MethodInfo,
	&ICollection_1_Remove_m50209_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8898_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8896_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8898_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8896_0_0_0;
extern Il2CppType ICollection_1_t8896_1_0_0;
struct ICollection_1_t8896;
extern Il2CppGenericClass ICollection_1_t8896_GenericClass;
TypeInfo ICollection_1_t8896_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8896_MethodInfos/* methods */
	, ICollection_1_t8896_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8896_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8896_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8896_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8896_0_0_0/* byval_arg */
	, &ICollection_1_t8896_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8896_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.MonoIOError>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.MonoIOError>
extern Il2CppType IEnumerator_1_t6964_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50210_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.MonoIOError>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50210_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8898_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6964_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50210_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8898_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50210_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8898_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8898_0_0_0;
extern Il2CppType IEnumerable_1_t8898_1_0_0;
struct IEnumerable_1_t8898;
extern Il2CppGenericClass IEnumerable_1_t8898_GenericClass;
TypeInfo IEnumerable_1_t8898_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8898_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8898_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8898_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8898_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8898_0_0_0/* byval_arg */
	, &IEnumerable_1_t8898_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8898_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8897_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.MonoIOError>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.MonoIOError>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.MonoIOError>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.MonoIOError>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.MonoIOError>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.MonoIOError>
extern MethodInfo IList_1_get_Item_m50211_MethodInfo;
extern MethodInfo IList_1_set_Item_m50212_MethodInfo;
static PropertyInfo IList_1_t8897____Item_PropertyInfo = 
{
	&IList_1_t8897_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50211_MethodInfo/* get */
	, &IList_1_set_Item_m50212_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8897_PropertyInfos[] =
{
	&IList_1_t8897____Item_PropertyInfo,
	NULL
};
extern Il2CppType MonoIOError_t1882_0_0_0;
static ParameterInfo IList_1_t8897_IList_1_IndexOf_m50213_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoIOError_t1882_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50213_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.MonoIOError>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50213_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8897_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8897_IList_1_IndexOf_m50213_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50213_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MonoIOError_t1882_0_0_0;
static ParameterInfo IList_1_t8897_IList_1_Insert_m50214_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MonoIOError_t1882_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50214_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.MonoIOError>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50214_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8897_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8897_IList_1_Insert_m50214_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50214_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8897_IList_1_RemoveAt_m50215_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50215_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.MonoIOError>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50215_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8897_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8897_IList_1_RemoveAt_m50215_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50215_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8897_IList_1_get_Item_m50211_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType MonoIOError_t1882_0_0_0;
extern void* RuntimeInvoker_MonoIOError_t1882_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50211_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.MonoIOError>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50211_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8897_il2cpp_TypeInfo/* declaring_type */
	, &MonoIOError_t1882_0_0_0/* return_type */
	, RuntimeInvoker_MonoIOError_t1882_Int32_t93/* invoker_method */
	, IList_1_t8897_IList_1_get_Item_m50211_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50211_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MonoIOError_t1882_0_0_0;
static ParameterInfo IList_1_t8897_IList_1_set_Item_m50212_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MonoIOError_t1882_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50212_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.MonoIOError>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50212_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8897_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8897_IList_1_set_Item_m50212_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50212_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8897_MethodInfos[] =
{
	&IList_1_IndexOf_m50213_MethodInfo,
	&IList_1_Insert_m50214_MethodInfo,
	&IList_1_RemoveAt_m50215_MethodInfo,
	&IList_1_get_Item_m50211_MethodInfo,
	&IList_1_set_Item_m50212_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8897_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8896_il2cpp_TypeInfo,
	&IEnumerable_1_t8898_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8897_0_0_0;
extern Il2CppType IList_1_t8897_1_0_0;
struct IList_1_t8897;
extern Il2CppGenericClass IList_1_t8897_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8897_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8897_MethodInfos/* methods */
	, IList_1_t8897_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8897_il2cpp_TypeInfo/* element_class */
	, IList_1_t8897_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8897_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8897_0_0_0/* byval_arg */
	, &IList_1_t8897_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8897_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6966_il2cpp_TypeInfo;

// System.IO.SeekOrigin
#include "mscorlib_System_IO_SeekOrigin.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.SeekOrigin>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.SeekOrigin>
extern MethodInfo IEnumerator_1_get_Current_m50216_MethodInfo;
static PropertyInfo IEnumerator_1_t6966____Current_PropertyInfo = 
{
	&IEnumerator_1_t6966_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50216_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6966_PropertyInfos[] =
{
	&IEnumerator_1_t6966____Current_PropertyInfo,
	NULL
};
extern Il2CppType SeekOrigin_t1683_0_0_0;
extern void* RuntimeInvoker_SeekOrigin_t1683 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50216_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.SeekOrigin>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50216_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6966_il2cpp_TypeInfo/* declaring_type */
	, &SeekOrigin_t1683_0_0_0/* return_type */
	, RuntimeInvoker_SeekOrigin_t1683/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50216_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6966_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50216_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6966_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6966_0_0_0;
extern Il2CppType IEnumerator_1_t6966_1_0_0;
struct IEnumerator_1_t6966;
extern Il2CppGenericClass IEnumerator_1_t6966_GenericClass;
TypeInfo IEnumerator_1_t6966_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6966_MethodInfos/* methods */
	, IEnumerator_1_t6966_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6966_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6966_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6966_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6966_0_0_0/* byval_arg */
	, &IEnumerator_1_t6966_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6966_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.SeekOrigin>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_617.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5000_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.SeekOrigin>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_617MethodDeclarations.h"

extern TypeInfo SeekOrigin_t1683_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30586_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSeekOrigin_t1683_m39667_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.SeekOrigin>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.SeekOrigin>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisSeekOrigin_t1683_m39667 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30582_MethodInfo;
 void InternalEnumerator_1__ctor_m30582 (InternalEnumerator_1_t5000 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30583_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30583 (InternalEnumerator_1_t5000 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30586(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30586_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&SeekOrigin_t1683_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30584_MethodInfo;
 void InternalEnumerator_1_Dispose_m30584 (InternalEnumerator_1_t5000 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30585_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30585 (InternalEnumerator_1_t5000 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30586 (InternalEnumerator_1_t5000 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisSeekOrigin_t1683_m39667(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisSeekOrigin_t1683_m39667_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.SeekOrigin>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5000____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5000_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5000, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5000____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5000_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5000, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5000_FieldInfos[] =
{
	&InternalEnumerator_1_t5000____array_0_FieldInfo,
	&InternalEnumerator_1_t5000____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5000____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5000_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30583_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5000____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5000_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30586_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5000_PropertyInfos[] =
{
	&InternalEnumerator_1_t5000____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5000____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5000_InternalEnumerator_1__ctor_m30582_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30582_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30582_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30582/* method */
	, &InternalEnumerator_1_t5000_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5000_InternalEnumerator_1__ctor_m30582_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30582_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30583_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30583_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30583/* method */
	, &InternalEnumerator_1_t5000_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30583_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30584_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30584_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30584/* method */
	, &InternalEnumerator_1_t5000_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30584_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30585_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30585_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30585/* method */
	, &InternalEnumerator_1_t5000_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30585_GenericMethod/* genericMethod */

};
extern Il2CppType SeekOrigin_t1683_0_0_0;
extern void* RuntimeInvoker_SeekOrigin_t1683 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30586_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.SeekOrigin>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30586_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30586/* method */
	, &InternalEnumerator_1_t5000_il2cpp_TypeInfo/* declaring_type */
	, &SeekOrigin_t1683_0_0_0/* return_type */
	, RuntimeInvoker_SeekOrigin_t1683/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30586_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5000_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30582_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30583_MethodInfo,
	&InternalEnumerator_1_Dispose_m30584_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30585_MethodInfo,
	&InternalEnumerator_1_get_Current_m30586_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5000_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30583_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30585_MethodInfo,
	&InternalEnumerator_1_Dispose_m30584_MethodInfo,
	&InternalEnumerator_1_get_Current_m30586_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5000_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6966_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5000_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6966_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5000_0_0_0;
extern Il2CppType InternalEnumerator_1_t5000_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5000_GenericClass;
TypeInfo InternalEnumerator_1_t5000_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5000_MethodInfos/* methods */
	, InternalEnumerator_1_t5000_PropertyInfos/* properties */
	, InternalEnumerator_1_t5000_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5000_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5000_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5000_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5000_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5000_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5000_1_0_0/* this_arg */
	, InternalEnumerator_1_t5000_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5000_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5000)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8899_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>
extern MethodInfo ICollection_1_get_Count_m50217_MethodInfo;
static PropertyInfo ICollection_1_t8899____Count_PropertyInfo = 
{
	&ICollection_1_t8899_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50217_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50218_MethodInfo;
static PropertyInfo ICollection_1_t8899____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8899_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50218_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8899_PropertyInfos[] =
{
	&ICollection_1_t8899____Count_PropertyInfo,
	&ICollection_1_t8899____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50217_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::get_Count()
MethodInfo ICollection_1_get_Count_m50217_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8899_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50217_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50218_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50218_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8899_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50218_GenericMethod/* genericMethod */

};
extern Il2CppType SeekOrigin_t1683_0_0_0;
extern Il2CppType SeekOrigin_t1683_0_0_0;
static ParameterInfo ICollection_1_t8899_ICollection_1_Add_m50219_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SeekOrigin_t1683_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50219_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Add(T)
MethodInfo ICollection_1_Add_m50219_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8899_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8899_ICollection_1_Add_m50219_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50219_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50220_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Clear()
MethodInfo ICollection_1_Clear_m50220_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8899_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50220_GenericMethod/* genericMethod */

};
extern Il2CppType SeekOrigin_t1683_0_0_0;
static ParameterInfo ICollection_1_t8899_ICollection_1_Contains_m50221_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SeekOrigin_t1683_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50221_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Contains(T)
MethodInfo ICollection_1_Contains_m50221_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8899_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8899_ICollection_1_Contains_m50221_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50221_GenericMethod/* genericMethod */

};
extern Il2CppType SeekOriginU5BU5D_t5299_0_0_0;
extern Il2CppType SeekOriginU5BU5D_t5299_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8899_ICollection_1_CopyTo_m50222_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SeekOriginU5BU5D_t5299_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50222_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50222_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8899_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8899_ICollection_1_CopyTo_m50222_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50222_GenericMethod/* genericMethod */

};
extern Il2CppType SeekOrigin_t1683_0_0_0;
static ParameterInfo ICollection_1_t8899_ICollection_1_Remove_m50223_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SeekOrigin_t1683_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50223_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IO.SeekOrigin>::Remove(T)
MethodInfo ICollection_1_Remove_m50223_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8899_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8899_ICollection_1_Remove_m50223_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50223_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8899_MethodInfos[] =
{
	&ICollection_1_get_Count_m50217_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50218_MethodInfo,
	&ICollection_1_Add_m50219_MethodInfo,
	&ICollection_1_Clear_m50220_MethodInfo,
	&ICollection_1_Contains_m50221_MethodInfo,
	&ICollection_1_CopyTo_m50222_MethodInfo,
	&ICollection_1_Remove_m50223_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8901_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8899_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8901_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8899_0_0_0;
extern Il2CppType ICollection_1_t8899_1_0_0;
struct ICollection_1_t8899;
extern Il2CppGenericClass ICollection_1_t8899_GenericClass;
TypeInfo ICollection_1_t8899_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8899_MethodInfos/* methods */
	, ICollection_1_t8899_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8899_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8899_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8899_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8899_0_0_0/* byval_arg */
	, &ICollection_1_t8899_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8899_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.SeekOrigin>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IO.SeekOrigin>
extern Il2CppType IEnumerator_1_t6966_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50224_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IO.SeekOrigin>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50224_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8901_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6966_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50224_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8901_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50224_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8901_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8901_0_0_0;
extern Il2CppType IEnumerable_1_t8901_1_0_0;
struct IEnumerable_1_t8901;
extern Il2CppGenericClass IEnumerable_1_t8901_GenericClass;
TypeInfo IEnumerable_1_t8901_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8901_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8901_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8901_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8901_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8901_0_0_0/* byval_arg */
	, &IEnumerable_1_t8901_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8901_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8900_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IO.SeekOrigin>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IO.SeekOrigin>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IO.SeekOrigin>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IO.SeekOrigin>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IO.SeekOrigin>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IO.SeekOrigin>
extern MethodInfo IList_1_get_Item_m50225_MethodInfo;
extern MethodInfo IList_1_set_Item_m50226_MethodInfo;
static PropertyInfo IList_1_t8900____Item_PropertyInfo = 
{
	&IList_1_t8900_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50225_MethodInfo/* get */
	, &IList_1_set_Item_m50226_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8900_PropertyInfos[] =
{
	&IList_1_t8900____Item_PropertyInfo,
	NULL
};
extern Il2CppType SeekOrigin_t1683_0_0_0;
static ParameterInfo IList_1_t8900_IList_1_IndexOf_m50227_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SeekOrigin_t1683_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50227_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IO.SeekOrigin>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50227_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8900_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8900_IList_1_IndexOf_m50227_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50227_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SeekOrigin_t1683_0_0_0;
static ParameterInfo IList_1_t8900_IList_1_Insert_m50228_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SeekOrigin_t1683_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50228_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.SeekOrigin>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50228_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8900_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8900_IList_1_Insert_m50228_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50228_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8900_IList_1_RemoveAt_m50229_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50229_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.SeekOrigin>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50229_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8900_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8900_IList_1_RemoveAt_m50229_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50229_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8900_IList_1_get_Item_m50225_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType SeekOrigin_t1683_0_0_0;
extern void* RuntimeInvoker_SeekOrigin_t1683_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50225_GenericMethod;
// T System.Collections.Generic.IList`1<System.IO.SeekOrigin>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50225_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8900_il2cpp_TypeInfo/* declaring_type */
	, &SeekOrigin_t1683_0_0_0/* return_type */
	, RuntimeInvoker_SeekOrigin_t1683_Int32_t93/* invoker_method */
	, IList_1_t8900_IList_1_get_Item_m50225_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50225_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SeekOrigin_t1683_0_0_0;
static ParameterInfo IList_1_t8900_IList_1_set_Item_m50226_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SeekOrigin_t1683_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50226_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IO.SeekOrigin>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50226_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8900_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8900_IList_1_set_Item_m50226_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50226_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8900_MethodInfos[] =
{
	&IList_1_IndexOf_m50227_MethodInfo,
	&IList_1_Insert_m50228_MethodInfo,
	&IList_1_RemoveAt_m50229_MethodInfo,
	&IList_1_get_Item_m50225_MethodInfo,
	&IList_1_set_Item_m50226_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8900_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8899_il2cpp_TypeInfo,
	&IEnumerable_1_t8901_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8900_0_0_0;
extern Il2CppType IList_1_t8900_1_0_0;
struct IList_1_t8900;
extern Il2CppGenericClass IList_1_t8900_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8900_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8900_MethodInfos/* methods */
	, IList_1_t8900_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8900_il2cpp_TypeInfo/* element_class */
	, IList_1_t8900_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8900_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8900_0_0_0/* byval_arg */
	, &IList_1_t8900_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8900_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6968_il2cpp_TypeInfo;

// System.Reflection.Emit.ModuleBuilder
#include "mscorlib_System_Reflection_Emit_ModuleBuilder.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ModuleBuilder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ModuleBuilder>
extern MethodInfo IEnumerator_1_get_Current_m50230_MethodInfo;
static PropertyInfo IEnumerator_1_t6968____Current_PropertyInfo = 
{
	&IEnumerator_1_t6968_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50230_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6968_PropertyInfos[] =
{
	&IEnumerator_1_t6968____Current_PropertyInfo,
	NULL
};
extern Il2CppType ModuleBuilder_t1910_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50230_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ModuleBuilder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50230_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6968_il2cpp_TypeInfo/* declaring_type */
	, &ModuleBuilder_t1910_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50230_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6968_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50230_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6968_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6968_0_0_0;
extern Il2CppType IEnumerator_1_t6968_1_0_0;
struct IEnumerator_1_t6968;
extern Il2CppGenericClass IEnumerator_1_t6968_GenericClass;
TypeInfo IEnumerator_1_t6968_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6968_MethodInfos/* methods */
	, IEnumerator_1_t6968_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6968_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6968_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6968_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6968_0_0_0/* byval_arg */
	, &IEnumerator_1_t6968_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6968_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_618.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5001_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_618MethodDeclarations.h"

extern TypeInfo ModuleBuilder_t1910_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30591_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisModuleBuilder_t1910_m39678_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ModuleBuilder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ModuleBuilder>(System.Int32)
#define Array_InternalArray__get_Item_TisModuleBuilder_t1910_m39678(__this, p0, method) (ModuleBuilder_t1910 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5001____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5001_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5001, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5001____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5001_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5001, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5001_FieldInfos[] =
{
	&InternalEnumerator_1_t5001____array_0_FieldInfo,
	&InternalEnumerator_1_t5001____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30588_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5001____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5001_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30588_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5001____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5001_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30591_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5001_PropertyInfos[] =
{
	&InternalEnumerator_1_t5001____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5001____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5001_InternalEnumerator_1__ctor_m30587_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30587_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30587_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5001_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5001_InternalEnumerator_1__ctor_m30587_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30587_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30588_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30588_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5001_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30588_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30589_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30589_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5001_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30589_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30590_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30590_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5001_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30590_GenericMethod/* genericMethod */

};
extern Il2CppType ModuleBuilder_t1910_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30591_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ModuleBuilder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30591_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5001_il2cpp_TypeInfo/* declaring_type */
	, &ModuleBuilder_t1910_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30591_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5001_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30587_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30588_MethodInfo,
	&InternalEnumerator_1_Dispose_m30589_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30590_MethodInfo,
	&InternalEnumerator_1_get_Current_m30591_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30590_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30589_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5001_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30588_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30590_MethodInfo,
	&InternalEnumerator_1_Dispose_m30589_MethodInfo,
	&InternalEnumerator_1_get_Current_m30591_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5001_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6968_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5001_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6968_il2cpp_TypeInfo, 7},
};
extern TypeInfo ModuleBuilder_t1910_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5001_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30591_MethodInfo/* Method Usage */,
	&ModuleBuilder_t1910_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisModuleBuilder_t1910_m39678_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5001_0_0_0;
extern Il2CppType InternalEnumerator_1_t5001_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5001_GenericClass;
TypeInfo InternalEnumerator_1_t5001_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5001_MethodInfos/* methods */
	, InternalEnumerator_1_t5001_PropertyInfos/* properties */
	, InternalEnumerator_1_t5001_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5001_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5001_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5001_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5001_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5001_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5001_1_0_0/* this_arg */
	, InternalEnumerator_1_t5001_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5001_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5001_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5001)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8902_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>
extern MethodInfo ICollection_1_get_Count_m50231_MethodInfo;
static PropertyInfo ICollection_1_t8902____Count_PropertyInfo = 
{
	&ICollection_1_t8902_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50231_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50232_MethodInfo;
static PropertyInfo ICollection_1_t8902____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8902_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50232_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8902_PropertyInfos[] =
{
	&ICollection_1_t8902____Count_PropertyInfo,
	&ICollection_1_t8902____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50231_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::get_Count()
MethodInfo ICollection_1_get_Count_m50231_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8902_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50231_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50232_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50232_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8902_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50232_GenericMethod/* genericMethod */

};
extern Il2CppType ModuleBuilder_t1910_0_0_0;
extern Il2CppType ModuleBuilder_t1910_0_0_0;
static ParameterInfo ICollection_1_t8902_ICollection_1_Add_m50233_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ModuleBuilder_t1910_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50233_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Add(T)
MethodInfo ICollection_1_Add_m50233_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8902_ICollection_1_Add_m50233_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50233_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50234_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Clear()
MethodInfo ICollection_1_Clear_m50234_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50234_GenericMethod/* genericMethod */

};
extern Il2CppType ModuleBuilder_t1910_0_0_0;
static ParameterInfo ICollection_1_t8902_ICollection_1_Contains_m50235_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ModuleBuilder_t1910_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50235_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Contains(T)
MethodInfo ICollection_1_Contains_m50235_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8902_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8902_ICollection_1_Contains_m50235_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50235_GenericMethod/* genericMethod */

};
extern Il2CppType ModuleBuilderU5BU5D_t1896_0_0_0;
extern Il2CppType ModuleBuilderU5BU5D_t1896_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8902_ICollection_1_CopyTo_m50236_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ModuleBuilderU5BU5D_t1896_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50236_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50236_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8902_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8902_ICollection_1_CopyTo_m50236_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50236_GenericMethod/* genericMethod */

};
extern Il2CppType ModuleBuilder_t1910_0_0_0;
static ParameterInfo ICollection_1_t8902_ICollection_1_Remove_m50237_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ModuleBuilder_t1910_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50237_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ModuleBuilder>::Remove(T)
MethodInfo ICollection_1_Remove_m50237_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8902_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8902_ICollection_1_Remove_m50237_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50237_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8902_MethodInfos[] =
{
	&ICollection_1_get_Count_m50231_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50232_MethodInfo,
	&ICollection_1_Add_m50233_MethodInfo,
	&ICollection_1_Clear_m50234_MethodInfo,
	&ICollection_1_Contains_m50235_MethodInfo,
	&ICollection_1_CopyTo_m50236_MethodInfo,
	&ICollection_1_Remove_m50237_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8904_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8902_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8904_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8902_0_0_0;
extern Il2CppType ICollection_1_t8902_1_0_0;
struct ICollection_1_t8902;
extern Il2CppGenericClass ICollection_1_t8902_GenericClass;
TypeInfo ICollection_1_t8902_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8902_MethodInfos/* methods */
	, ICollection_1_t8902_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8902_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8902_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8902_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8902_0_0_0/* byval_arg */
	, &ICollection_1_t8902_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8902_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ModuleBuilder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ModuleBuilder>
extern Il2CppType IEnumerator_1_t6968_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50238_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ModuleBuilder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50238_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8904_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6968_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50238_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8904_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50238_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8904_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8904_0_0_0;
extern Il2CppType IEnumerable_1_t8904_1_0_0;
struct IEnumerable_1_t8904;
extern Il2CppGenericClass IEnumerable_1_t8904_GenericClass;
TypeInfo IEnumerable_1_t8904_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8904_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8904_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8904_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8904_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8904_0_0_0/* byval_arg */
	, &IEnumerable_1_t8904_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8904_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8903_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>
extern MethodInfo IList_1_get_Item_m50239_MethodInfo;
extern MethodInfo IList_1_set_Item_m50240_MethodInfo;
static PropertyInfo IList_1_t8903____Item_PropertyInfo = 
{
	&IList_1_t8903_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50239_MethodInfo/* get */
	, &IList_1_set_Item_m50240_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8903_PropertyInfos[] =
{
	&IList_1_t8903____Item_PropertyInfo,
	NULL
};
extern Il2CppType ModuleBuilder_t1910_0_0_0;
static ParameterInfo IList_1_t8903_IList_1_IndexOf_m50241_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ModuleBuilder_t1910_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50241_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50241_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8903_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8903_IList_1_IndexOf_m50241_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50241_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ModuleBuilder_t1910_0_0_0;
static ParameterInfo IList_1_t8903_IList_1_Insert_m50242_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ModuleBuilder_t1910_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50242_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50242_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8903_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8903_IList_1_Insert_m50242_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50242_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8903_IList_1_RemoveAt_m50243_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50243_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50243_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8903_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8903_IList_1_RemoveAt_m50243_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50243_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8903_IList_1_get_Item_m50239_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ModuleBuilder_t1910_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50239_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50239_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8903_il2cpp_TypeInfo/* declaring_type */
	, &ModuleBuilder_t1910_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8903_IList_1_get_Item_m50239_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50239_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ModuleBuilder_t1910_0_0_0;
static ParameterInfo IList_1_t8903_IList_1_set_Item_m50240_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ModuleBuilder_t1910_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50240_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ModuleBuilder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50240_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8903_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8903_IList_1_set_Item_m50240_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50240_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8903_MethodInfos[] =
{
	&IList_1_IndexOf_m50241_MethodInfo,
	&IList_1_Insert_m50242_MethodInfo,
	&IList_1_RemoveAt_m50243_MethodInfo,
	&IList_1_get_Item_m50239_MethodInfo,
	&IList_1_set_Item_m50240_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8903_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8902_il2cpp_TypeInfo,
	&IEnumerable_1_t8904_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8903_0_0_0;
extern Il2CppType IList_1_t8903_1_0_0;
struct IList_1_t8903;
extern Il2CppGenericClass IList_1_t8903_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8903_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8903_MethodInfos/* methods */
	, IList_1_t8903_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8903_il2cpp_TypeInfo/* element_class */
	, IList_1_t8903_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8903_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8903_0_0_0/* byval_arg */
	, &IList_1_t8903_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8903_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8905_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>
extern MethodInfo ICollection_1_get_Count_m50244_MethodInfo;
static PropertyInfo ICollection_1_t8905____Count_PropertyInfo = 
{
	&ICollection_1_t8905_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50244_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50245_MethodInfo;
static PropertyInfo ICollection_1_t8905____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8905_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50245_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8905_PropertyInfos[] =
{
	&ICollection_1_t8905____Count_PropertyInfo,
	&ICollection_1_t8905____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50244_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::get_Count()
MethodInfo ICollection_1_get_Count_m50244_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8905_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50244_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50245_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50245_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8905_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50245_GenericMethod/* genericMethod */

};
extern Il2CppType _ModuleBuilder_t2603_0_0_0;
extern Il2CppType _ModuleBuilder_t2603_0_0_0;
static ParameterInfo ICollection_1_t8905_ICollection_1_Add_m50246_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilder_t2603_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50246_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Add(T)
MethodInfo ICollection_1_Add_m50246_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8905_ICollection_1_Add_m50246_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50246_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50247_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Clear()
MethodInfo ICollection_1_Clear_m50247_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50247_GenericMethod/* genericMethod */

};
extern Il2CppType _ModuleBuilder_t2603_0_0_0;
static ParameterInfo ICollection_1_t8905_ICollection_1_Contains_m50248_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilder_t2603_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50248_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Contains(T)
MethodInfo ICollection_1_Contains_m50248_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8905_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8905_ICollection_1_Contains_m50248_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50248_GenericMethod/* genericMethod */

};
extern Il2CppType _ModuleBuilderU5BU5D_t5300_0_0_0;
extern Il2CppType _ModuleBuilderU5BU5D_t5300_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8905_ICollection_1_CopyTo_m50249_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilderU5BU5D_t5300_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50249_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50249_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8905_ICollection_1_CopyTo_m50249_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50249_GenericMethod/* genericMethod */

};
extern Il2CppType _ModuleBuilder_t2603_0_0_0;
static ParameterInfo ICollection_1_t8905_ICollection_1_Remove_m50250_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilder_t2603_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50250_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ModuleBuilder>::Remove(T)
MethodInfo ICollection_1_Remove_m50250_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8905_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8905_ICollection_1_Remove_m50250_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50250_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8905_MethodInfos[] =
{
	&ICollection_1_get_Count_m50244_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50245_MethodInfo,
	&ICollection_1_Add_m50246_MethodInfo,
	&ICollection_1_Clear_m50247_MethodInfo,
	&ICollection_1_Contains_m50248_MethodInfo,
	&ICollection_1_CopyTo_m50249_MethodInfo,
	&ICollection_1_Remove_m50250_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8907_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8905_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8907_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8905_0_0_0;
extern Il2CppType ICollection_1_t8905_1_0_0;
struct ICollection_1_t8905;
extern Il2CppGenericClass ICollection_1_t8905_GenericClass;
TypeInfo ICollection_1_t8905_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8905_MethodInfos/* methods */
	, ICollection_1_t8905_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8905_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8905_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8905_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8905_0_0_0/* byval_arg */
	, &ICollection_1_t8905_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8905_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ModuleBuilder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ModuleBuilder>
extern Il2CppType IEnumerator_1_t6970_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50251_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ModuleBuilder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50251_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8907_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6970_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50251_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8907_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50251_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8907_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8907_0_0_0;
extern Il2CppType IEnumerable_1_t8907_1_0_0;
struct IEnumerable_1_t8907;
extern Il2CppGenericClass IEnumerable_1_t8907_GenericClass;
TypeInfo IEnumerable_1_t8907_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8907_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8907_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8907_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8907_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8907_0_0_0/* byval_arg */
	, &IEnumerable_1_t8907_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8907_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6970_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>
extern MethodInfo IEnumerator_1_get_Current_m50252_MethodInfo;
static PropertyInfo IEnumerator_1_t6970____Current_PropertyInfo = 
{
	&IEnumerator_1_t6970_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50252_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6970_PropertyInfos[] =
{
	&IEnumerator_1_t6970____Current_PropertyInfo,
	NULL
};
extern Il2CppType _ModuleBuilder_t2603_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50252_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50252_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6970_il2cpp_TypeInfo/* declaring_type */
	, &_ModuleBuilder_t2603_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50252_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6970_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50252_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6970_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6970_0_0_0;
extern Il2CppType IEnumerator_1_t6970_1_0_0;
struct IEnumerator_1_t6970;
extern Il2CppGenericClass IEnumerator_1_t6970_GenericClass;
TypeInfo IEnumerator_1_t6970_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6970_MethodInfos/* methods */
	, IEnumerator_1_t6970_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6970_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6970_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6970_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6970_0_0_0/* byval_arg */
	, &IEnumerator_1_t6970_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6970_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_619.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5002_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_619MethodDeclarations.h"

extern TypeInfo _ModuleBuilder_t2603_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30596_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_Tis_ModuleBuilder_t2603_m39689_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._ModuleBuilder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._ModuleBuilder>(System.Int32)
#define Array_InternalArray__get_Item_Tis_ModuleBuilder_t2603_m39689(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5002____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5002_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5002, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5002____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5002_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5002, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5002_FieldInfos[] =
{
	&InternalEnumerator_1_t5002____array_0_FieldInfo,
	&InternalEnumerator_1_t5002____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30593_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5002____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5002_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30593_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5002____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5002_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30596_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5002_PropertyInfos[] =
{
	&InternalEnumerator_1_t5002____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5002____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5002_InternalEnumerator_1__ctor_m30592_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30592_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30592_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5002_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5002_InternalEnumerator_1__ctor_m30592_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30592_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30593_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30593_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5002_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30593_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30594_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30594_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5002_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30594_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30595_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30595_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5002_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30595_GenericMethod/* genericMethod */

};
extern Il2CppType _ModuleBuilder_t2603_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30596_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ModuleBuilder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30596_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5002_il2cpp_TypeInfo/* declaring_type */
	, &_ModuleBuilder_t2603_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30596_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5002_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30592_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30593_MethodInfo,
	&InternalEnumerator_1_Dispose_m30594_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30595_MethodInfo,
	&InternalEnumerator_1_get_Current_m30596_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30595_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30594_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5002_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30593_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30595_MethodInfo,
	&InternalEnumerator_1_Dispose_m30594_MethodInfo,
	&InternalEnumerator_1_get_Current_m30596_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5002_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6970_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5002_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6970_il2cpp_TypeInfo, 7},
};
extern TypeInfo _ModuleBuilder_t2603_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5002_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30596_MethodInfo/* Method Usage */,
	&_ModuleBuilder_t2603_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_Tis_ModuleBuilder_t2603_m39689_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5002_0_0_0;
extern Il2CppType InternalEnumerator_1_t5002_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5002_GenericClass;
TypeInfo InternalEnumerator_1_t5002_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5002_MethodInfos/* methods */
	, InternalEnumerator_1_t5002_PropertyInfos/* properties */
	, InternalEnumerator_1_t5002_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5002_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5002_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5002_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5002_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5002_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5002_1_0_0/* this_arg */
	, InternalEnumerator_1_t5002_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5002_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5002_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5002)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8906_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>
extern MethodInfo IList_1_get_Item_m50253_MethodInfo;
extern MethodInfo IList_1_set_Item_m50254_MethodInfo;
static PropertyInfo IList_1_t8906____Item_PropertyInfo = 
{
	&IList_1_t8906_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50253_MethodInfo/* get */
	, &IList_1_set_Item_m50254_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8906_PropertyInfos[] =
{
	&IList_1_t8906____Item_PropertyInfo,
	NULL
};
extern Il2CppType _ModuleBuilder_t2603_0_0_0;
static ParameterInfo IList_1_t8906_IList_1_IndexOf_m50255_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilder_t2603_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50255_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50255_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8906_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8906_IList_1_IndexOf_m50255_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50255_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _ModuleBuilder_t2603_0_0_0;
static ParameterInfo IList_1_t8906_IList_1_Insert_m50256_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilder_t2603_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50256_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50256_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8906_IList_1_Insert_m50256_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50256_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8906_IList_1_RemoveAt_m50257_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50257_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50257_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8906_IList_1_RemoveAt_m50257_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50257_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8906_IList_1_get_Item_m50253_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType _ModuleBuilder_t2603_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50253_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50253_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8906_il2cpp_TypeInfo/* declaring_type */
	, &_ModuleBuilder_t2603_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8906_IList_1_get_Item_m50253_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50253_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _ModuleBuilder_t2603_0_0_0;
static ParameterInfo IList_1_t8906_IList_1_set_Item_m50254_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &_ModuleBuilder_t2603_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50254_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ModuleBuilder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50254_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8906_IList_1_set_Item_m50254_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50254_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8906_MethodInfos[] =
{
	&IList_1_IndexOf_m50255_MethodInfo,
	&IList_1_Insert_m50256_MethodInfo,
	&IList_1_RemoveAt_m50257_MethodInfo,
	&IList_1_get_Item_m50253_MethodInfo,
	&IList_1_set_Item_m50254_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8906_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8905_il2cpp_TypeInfo,
	&IEnumerable_1_t8907_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8906_0_0_0;
extern Il2CppType IList_1_t8906_1_0_0;
struct IList_1_t8906;
extern Il2CppGenericClass IList_1_t8906_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8906_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8906_MethodInfos/* methods */
	, IList_1_t8906_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8906_il2cpp_TypeInfo/* element_class */
	, IList_1_t8906_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8906_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8906_0_0_0/* byval_arg */
	, &IList_1_t8906_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8906_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8908_il2cpp_TypeInfo;

// System.Reflection.Module
#include "mscorlib_System_Reflection_Module.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Module>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Module>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Module>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Module>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Module>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Module>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Module>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Module>
extern MethodInfo ICollection_1_get_Count_m50258_MethodInfo;
static PropertyInfo ICollection_1_t8908____Count_PropertyInfo = 
{
	&ICollection_1_t8908_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50258_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50259_MethodInfo;
static PropertyInfo ICollection_1_t8908____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8908_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50259_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8908_PropertyInfos[] =
{
	&ICollection_1_t8908____Count_PropertyInfo,
	&ICollection_1_t8908____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50258_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Module>::get_Count()
MethodInfo ICollection_1_get_Count_m50258_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8908_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50258_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50259_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Module>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50259_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8908_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50259_GenericMethod/* genericMethod */

};
extern Il2CppType Module_t1702_0_0_0;
extern Il2CppType Module_t1702_0_0_0;
static ParameterInfo ICollection_1_t8908_ICollection_1_Add_m50260_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Module_t1702_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50260_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Module>::Add(T)
MethodInfo ICollection_1_Add_m50260_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8908_ICollection_1_Add_m50260_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50260_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50261_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Module>::Clear()
MethodInfo ICollection_1_Clear_m50261_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50261_GenericMethod/* genericMethod */

};
extern Il2CppType Module_t1702_0_0_0;
static ParameterInfo ICollection_1_t8908_ICollection_1_Contains_m50262_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Module_t1702_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50262_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Module>::Contains(T)
MethodInfo ICollection_1_Contains_m50262_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8908_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8908_ICollection_1_Contains_m50262_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50262_GenericMethod/* genericMethod */

};
extern Il2CppType ModuleU5BU5D_t1898_0_0_0;
extern Il2CppType ModuleU5BU5D_t1898_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8908_ICollection_1_CopyTo_m50263_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ModuleU5BU5D_t1898_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50263_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Module>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50263_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8908_ICollection_1_CopyTo_m50263_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50263_GenericMethod/* genericMethod */

};
extern Il2CppType Module_t1702_0_0_0;
static ParameterInfo ICollection_1_t8908_ICollection_1_Remove_m50264_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Module_t1702_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50264_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Module>::Remove(T)
MethodInfo ICollection_1_Remove_m50264_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8908_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8908_ICollection_1_Remove_m50264_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50264_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8908_MethodInfos[] =
{
	&ICollection_1_get_Count_m50258_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50259_MethodInfo,
	&ICollection_1_Add_m50260_MethodInfo,
	&ICollection_1_Clear_m50261_MethodInfo,
	&ICollection_1_Contains_m50262_MethodInfo,
	&ICollection_1_CopyTo_m50263_MethodInfo,
	&ICollection_1_Remove_m50264_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8910_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8908_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8910_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8908_0_0_0;
extern Il2CppType ICollection_1_t8908_1_0_0;
struct ICollection_1_t8908;
extern Il2CppGenericClass ICollection_1_t8908_GenericClass;
TypeInfo ICollection_1_t8908_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8908_MethodInfos/* methods */
	, ICollection_1_t8908_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8908_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8908_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8908_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8908_0_0_0/* byval_arg */
	, &ICollection_1_t8908_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8908_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Module>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Module>
extern Il2CppType IEnumerator_1_t6971_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50265_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Module>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50265_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8910_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6971_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50265_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8910_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50265_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8910_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8910_0_0_0;
extern Il2CppType IEnumerable_1_t8910_1_0_0;
struct IEnumerable_1_t8910;
extern Il2CppGenericClass IEnumerable_1_t8910_GenericClass;
TypeInfo IEnumerable_1_t8910_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8910_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8910_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8910_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8910_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8910_0_0_0/* byval_arg */
	, &IEnumerable_1_t8910_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8910_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6971_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Reflection.Module>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Module>
extern MethodInfo IEnumerator_1_get_Current_m50266_MethodInfo;
static PropertyInfo IEnumerator_1_t6971____Current_PropertyInfo = 
{
	&IEnumerator_1_t6971_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50266_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6971_PropertyInfos[] =
{
	&IEnumerator_1_t6971____Current_PropertyInfo,
	NULL
};
extern Il2CppType Module_t1702_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50266_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.Module>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50266_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6971_il2cpp_TypeInfo/* declaring_type */
	, &Module_t1702_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50266_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6971_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50266_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6971_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6971_0_0_0;
extern Il2CppType IEnumerator_1_t6971_1_0_0;
struct IEnumerator_1_t6971;
extern Il2CppGenericClass IEnumerator_1_t6971_GenericClass;
TypeInfo IEnumerator_1_t6971_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6971_MethodInfos/* methods */
	, IEnumerator_1_t6971_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6971_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6971_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6971_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6971_0_0_0/* byval_arg */
	, &IEnumerator_1_t6971_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6971_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.Module>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_620.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5003_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.Module>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_620MethodDeclarations.h"

extern TypeInfo Module_t1702_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30601_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisModule_t1702_m39700_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.Module>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Module>(System.Int32)
#define Array_InternalArray__get_Item_TisModule_t1702_m39700(__this, p0, method) (Module_t1702 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.Module>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Module>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Module>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Module>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.Module>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Module>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5003____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5003_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5003, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5003____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5003_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5003, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5003_FieldInfos[] =
{
	&InternalEnumerator_1_t5003____array_0_FieldInfo,
	&InternalEnumerator_1_t5003____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30598_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5003____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5003_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30598_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5003____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5003_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30601_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5003_PropertyInfos[] =
{
	&InternalEnumerator_1_t5003____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5003____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5003_InternalEnumerator_1__ctor_m30597_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30597_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Module>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30597_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5003_InternalEnumerator_1__ctor_m30597_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30597_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30598_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Module>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30598_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5003_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30598_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30599_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Module>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30599_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5003_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30599_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30600_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Module>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30600_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5003_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30600_GenericMethod/* genericMethod */

};
extern Il2CppType Module_t1702_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30601_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.Module>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30601_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5003_il2cpp_TypeInfo/* declaring_type */
	, &Module_t1702_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30601_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5003_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30597_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30598_MethodInfo,
	&InternalEnumerator_1_Dispose_m30599_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30600_MethodInfo,
	&InternalEnumerator_1_get_Current_m30601_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30600_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30599_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5003_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30598_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30600_MethodInfo,
	&InternalEnumerator_1_Dispose_m30599_MethodInfo,
	&InternalEnumerator_1_get_Current_m30601_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5003_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6971_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5003_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6971_il2cpp_TypeInfo, 7},
};
extern TypeInfo Module_t1702_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5003_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30601_MethodInfo/* Method Usage */,
	&Module_t1702_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisModule_t1702_m39700_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5003_0_0_0;
extern Il2CppType InternalEnumerator_1_t5003_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5003_GenericClass;
TypeInfo InternalEnumerator_1_t5003_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5003_MethodInfos/* methods */
	, InternalEnumerator_1_t5003_PropertyInfos/* properties */
	, InternalEnumerator_1_t5003_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5003_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5003_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5003_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5003_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5003_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5003_1_0_0/* this_arg */
	, InternalEnumerator_1_t5003_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5003_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5003_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5003)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8909_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Module>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Module>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Module>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.Module>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Module>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Module>
extern MethodInfo IList_1_get_Item_m50267_MethodInfo;
extern MethodInfo IList_1_set_Item_m50268_MethodInfo;
static PropertyInfo IList_1_t8909____Item_PropertyInfo = 
{
	&IList_1_t8909_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50267_MethodInfo/* get */
	, &IList_1_set_Item_m50268_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8909_PropertyInfos[] =
{
	&IList_1_t8909____Item_PropertyInfo,
	NULL
};
extern Il2CppType Module_t1702_0_0_0;
static ParameterInfo IList_1_t8909_IList_1_IndexOf_m50269_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Module_t1702_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50269_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Module>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50269_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8909_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8909_IList_1_IndexOf_m50269_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50269_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Module_t1702_0_0_0;
static ParameterInfo IList_1_t8909_IList_1_Insert_m50270_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Module_t1702_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50270_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Module>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50270_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8909_IList_1_Insert_m50270_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50270_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8909_IList_1_RemoveAt_m50271_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50271_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Module>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50271_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8909_IList_1_RemoveAt_m50271_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50271_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8909_IList_1_get_Item_m50267_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Module_t1702_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50267_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.Module>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50267_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8909_il2cpp_TypeInfo/* declaring_type */
	, &Module_t1702_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8909_IList_1_get_Item_m50267_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50267_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Module_t1702_0_0_0;
static ParameterInfo IList_1_t8909_IList_1_set_Item_m50268_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Module_t1702_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50268_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Module>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50268_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8909_IList_1_set_Item_m50268_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50268_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8909_MethodInfos[] =
{
	&IList_1_IndexOf_m50269_MethodInfo,
	&IList_1_Insert_m50270_MethodInfo,
	&IList_1_RemoveAt_m50271_MethodInfo,
	&IList_1_get_Item_m50267_MethodInfo,
	&IList_1_set_Item_m50268_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8909_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8908_il2cpp_TypeInfo,
	&IEnumerable_1_t8910_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8909_0_0_0;
extern Il2CppType IList_1_t8909_1_0_0;
struct IList_1_t8909;
extern Il2CppGenericClass IList_1_t8909_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8909_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8909_MethodInfos/* methods */
	, IList_1_t8909_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8909_il2cpp_TypeInfo/* element_class */
	, IList_1_t8909_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8909_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8909_0_0_0/* byval_arg */
	, &IList_1_t8909_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8909_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8911_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>
extern MethodInfo ICollection_1_get_Count_m50272_MethodInfo;
static PropertyInfo ICollection_1_t8911____Count_PropertyInfo = 
{
	&ICollection_1_t8911_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50272_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50273_MethodInfo;
static PropertyInfo ICollection_1_t8911____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8911_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50273_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8911_PropertyInfos[] =
{
	&ICollection_1_t8911____Count_PropertyInfo,
	&ICollection_1_t8911____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50272_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::get_Count()
MethodInfo ICollection_1_get_Count_m50272_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8911_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50272_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50273_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50273_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8911_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50273_GenericMethod/* genericMethod */

};
extern Il2CppType _Module_t2604_0_0_0;
extern Il2CppType _Module_t2604_0_0_0;
static ParameterInfo ICollection_1_t8911_ICollection_1_Add_m50274_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_Module_t2604_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50274_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Add(T)
MethodInfo ICollection_1_Add_m50274_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8911_ICollection_1_Add_m50274_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50274_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50275_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Clear()
MethodInfo ICollection_1_Clear_m50275_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50275_GenericMethod/* genericMethod */

};
extern Il2CppType _Module_t2604_0_0_0;
static ParameterInfo ICollection_1_t8911_ICollection_1_Contains_m50276_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_Module_t2604_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50276_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Contains(T)
MethodInfo ICollection_1_Contains_m50276_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8911_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8911_ICollection_1_Contains_m50276_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50276_GenericMethod/* genericMethod */

};
extern Il2CppType _ModuleU5BU5D_t5301_0_0_0;
extern Il2CppType _ModuleU5BU5D_t5301_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8911_ICollection_1_CopyTo_m50277_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &_ModuleU5BU5D_t5301_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50277_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50277_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8911_ICollection_1_CopyTo_m50277_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50277_GenericMethod/* genericMethod */

};
extern Il2CppType _Module_t2604_0_0_0;
static ParameterInfo ICollection_1_t8911_ICollection_1_Remove_m50278_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_Module_t2604_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50278_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._Module>::Remove(T)
MethodInfo ICollection_1_Remove_m50278_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8911_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8911_ICollection_1_Remove_m50278_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50278_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8911_MethodInfos[] =
{
	&ICollection_1_get_Count_m50272_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50273_MethodInfo,
	&ICollection_1_Add_m50274_MethodInfo,
	&ICollection_1_Clear_m50275_MethodInfo,
	&ICollection_1_Contains_m50276_MethodInfo,
	&ICollection_1_CopyTo_m50277_MethodInfo,
	&ICollection_1_Remove_m50278_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8913_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8911_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8913_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8911_0_0_0;
extern Il2CppType ICollection_1_t8911_1_0_0;
struct ICollection_1_t8911;
extern Il2CppGenericClass ICollection_1_t8911_GenericClass;
TypeInfo ICollection_1_t8911_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8911_MethodInfos/* methods */
	, ICollection_1_t8911_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8911_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8911_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8911_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8911_0_0_0/* byval_arg */
	, &ICollection_1_t8911_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8911_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._Module>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._Module>
extern Il2CppType IEnumerator_1_t6973_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50279_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._Module>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50279_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8913_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6973_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50279_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8913_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50279_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8913_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8913_0_0_0;
extern Il2CppType IEnumerable_1_t8913_1_0_0;
struct IEnumerable_1_t8913;
extern Il2CppGenericClass IEnumerable_1_t8913_GenericClass;
TypeInfo IEnumerable_1_t8913_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8913_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8913_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8913_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8913_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8913_0_0_0/* byval_arg */
	, &IEnumerable_1_t8913_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8913_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6973_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._Module>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._Module>
extern MethodInfo IEnumerator_1_get_Current_m50280_MethodInfo;
static PropertyInfo IEnumerator_1_t6973____Current_PropertyInfo = 
{
	&IEnumerator_1_t6973_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50280_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6973_PropertyInfos[] =
{
	&IEnumerator_1_t6973____Current_PropertyInfo,
	NULL
};
extern Il2CppType _Module_t2604_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50280_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._Module>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50280_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6973_il2cpp_TypeInfo/* declaring_type */
	, &_Module_t2604_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50280_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6973_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50280_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6973_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6973_0_0_0;
extern Il2CppType IEnumerator_1_t6973_1_0_0;
struct IEnumerator_1_t6973;
extern Il2CppGenericClass IEnumerator_1_t6973_GenericClass;
TypeInfo IEnumerator_1_t6973_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6973_MethodInfos/* methods */
	, IEnumerator_1_t6973_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6973_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6973_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6973_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6973_0_0_0/* byval_arg */
	, &IEnumerator_1_t6973_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6973_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_621.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5004_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_621MethodDeclarations.h"

extern TypeInfo _Module_t2604_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30606_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_Tis_Module_t2604_m39711_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._Module>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._Module>(System.Int32)
#define Array_InternalArray__get_Item_Tis_Module_t2604_m39711(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5004____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5004_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5004, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5004____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5004_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5004, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5004_FieldInfos[] =
{
	&InternalEnumerator_1_t5004____array_0_FieldInfo,
	&InternalEnumerator_1_t5004____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30603_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5004____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5004_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30603_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5004____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5004_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30606_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5004_PropertyInfos[] =
{
	&InternalEnumerator_1_t5004____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5004____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5004_InternalEnumerator_1__ctor_m30602_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30602_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30602_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5004_InternalEnumerator_1__ctor_m30602_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30602_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30603_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30603_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5004_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30603_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30604_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30604_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5004_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30604_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30605_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30605_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5004_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30605_GenericMethod/* genericMethod */

};
extern Il2CppType _Module_t2604_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30606_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._Module>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30606_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5004_il2cpp_TypeInfo/* declaring_type */
	, &_Module_t2604_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30606_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5004_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30602_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30603_MethodInfo,
	&InternalEnumerator_1_Dispose_m30604_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30605_MethodInfo,
	&InternalEnumerator_1_get_Current_m30606_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30605_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30604_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5004_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30603_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30605_MethodInfo,
	&InternalEnumerator_1_Dispose_m30604_MethodInfo,
	&InternalEnumerator_1_get_Current_m30606_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5004_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6973_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5004_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6973_il2cpp_TypeInfo, 7},
};
extern TypeInfo _Module_t2604_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5004_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30606_MethodInfo/* Method Usage */,
	&_Module_t2604_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_Tis_Module_t2604_m39711_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5004_0_0_0;
extern Il2CppType InternalEnumerator_1_t5004_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5004_GenericClass;
TypeInfo InternalEnumerator_1_t5004_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5004_MethodInfos/* methods */
	, InternalEnumerator_1_t5004_PropertyInfos/* properties */
	, InternalEnumerator_1_t5004_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5004_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5004_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5004_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5004_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5004_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5004_1_0_0/* this_arg */
	, InternalEnumerator_1_t5004_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5004_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5004_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5004)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8912_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>
extern MethodInfo IList_1_get_Item_m50281_MethodInfo;
extern MethodInfo IList_1_set_Item_m50282_MethodInfo;
static PropertyInfo IList_1_t8912____Item_PropertyInfo = 
{
	&IList_1_t8912_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50281_MethodInfo/* get */
	, &IList_1_set_Item_m50282_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8912_PropertyInfos[] =
{
	&IList_1_t8912____Item_PropertyInfo,
	NULL
};
extern Il2CppType _Module_t2604_0_0_0;
static ParameterInfo IList_1_t8912_IList_1_IndexOf_m50283_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_Module_t2604_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50283_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50283_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8912_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8912_IList_1_IndexOf_m50283_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50283_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _Module_t2604_0_0_0;
static ParameterInfo IList_1_t8912_IList_1_Insert_m50284_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &_Module_t2604_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50284_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50284_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8912_IList_1_Insert_m50284_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50284_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8912_IList_1_RemoveAt_m50285_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50285_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50285_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8912_IList_1_RemoveAt_m50285_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50285_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8912_IList_1_get_Item_m50281_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType _Module_t2604_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50281_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50281_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8912_il2cpp_TypeInfo/* declaring_type */
	, &_Module_t2604_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8912_IList_1_get_Item_m50281_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50281_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _Module_t2604_0_0_0;
static ParameterInfo IList_1_t8912_IList_1_set_Item_m50282_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &_Module_t2604_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50282_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._Module>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50282_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8912_IList_1_set_Item_m50282_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50282_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8912_MethodInfos[] =
{
	&IList_1_IndexOf_m50283_MethodInfo,
	&IList_1_Insert_m50284_MethodInfo,
	&IList_1_RemoveAt_m50285_MethodInfo,
	&IList_1_get_Item_m50281_MethodInfo,
	&IList_1_set_Item_m50282_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8912_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8911_il2cpp_TypeInfo,
	&IEnumerable_1_t8913_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8912_0_0_0;
extern Il2CppType IList_1_t8912_1_0_0;
struct IList_1_t8912;
extern Il2CppGenericClass IList_1_t8912_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8912_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8912_MethodInfos/* methods */
	, IList_1_t8912_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8912_il2cpp_TypeInfo/* element_class */
	, IList_1_t8912_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8912_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8912_0_0_0/* byval_arg */
	, &IList_1_t8912_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8912_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6975_il2cpp_TypeInfo;

// System.Reflection.Emit.ParameterBuilder
#include "mscorlib_System_Reflection_Emit_ParameterBuilder.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ParameterBuilder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ParameterBuilder>
extern MethodInfo IEnumerator_1_get_Current_m50286_MethodInfo;
static PropertyInfo IEnumerator_1_t6975____Current_PropertyInfo = 
{
	&IEnumerator_1_t6975_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50286_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6975_PropertyInfos[] =
{
	&IEnumerator_1_t6975____Current_PropertyInfo,
	NULL
};
extern Il2CppType ParameterBuilder_t1911_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50286_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ParameterBuilder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50286_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6975_il2cpp_TypeInfo/* declaring_type */
	, &ParameterBuilder_t1911_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50286_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6975_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50286_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6975_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6975_0_0_0;
extern Il2CppType IEnumerator_1_t6975_1_0_0;
struct IEnumerator_1_t6975;
extern Il2CppGenericClass IEnumerator_1_t6975_GenericClass;
TypeInfo IEnumerator_1_t6975_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6975_MethodInfos/* methods */
	, IEnumerator_1_t6975_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6975_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6975_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6975_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6975_0_0_0/* byval_arg */
	, &IEnumerator_1_t6975_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6975_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_622.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5005_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_622MethodDeclarations.h"

extern TypeInfo ParameterBuilder_t1911_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30611_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisParameterBuilder_t1911_m39722_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ParameterBuilder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ParameterBuilder>(System.Int32)
#define Array_InternalArray__get_Item_TisParameterBuilder_t1911_m39722(__this, p0, method) (ParameterBuilder_t1911 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5005____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5005_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5005, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5005____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5005_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5005, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5005_FieldInfos[] =
{
	&InternalEnumerator_1_t5005____array_0_FieldInfo,
	&InternalEnumerator_1_t5005____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30608_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5005____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5005_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30608_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5005____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5005_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30611_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5005_PropertyInfos[] =
{
	&InternalEnumerator_1_t5005____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5005____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5005_InternalEnumerator_1__ctor_m30607_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30607_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30607_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5005_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5005_InternalEnumerator_1__ctor_m30607_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30607_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30608_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30608_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5005_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30608_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30609_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30609_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5005_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30609_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30610_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30610_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5005_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30610_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterBuilder_t1911_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30611_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ParameterBuilder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30611_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5005_il2cpp_TypeInfo/* declaring_type */
	, &ParameterBuilder_t1911_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30611_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5005_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30607_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30608_MethodInfo,
	&InternalEnumerator_1_Dispose_m30609_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30610_MethodInfo,
	&InternalEnumerator_1_get_Current_m30611_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30610_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30609_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5005_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30608_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30610_MethodInfo,
	&InternalEnumerator_1_Dispose_m30609_MethodInfo,
	&InternalEnumerator_1_get_Current_m30611_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5005_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6975_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5005_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6975_il2cpp_TypeInfo, 7},
};
extern TypeInfo ParameterBuilder_t1911_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5005_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30611_MethodInfo/* Method Usage */,
	&ParameterBuilder_t1911_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisParameterBuilder_t1911_m39722_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5005_0_0_0;
extern Il2CppType InternalEnumerator_1_t5005_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5005_GenericClass;
TypeInfo InternalEnumerator_1_t5005_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5005_MethodInfos/* methods */
	, InternalEnumerator_1_t5005_PropertyInfos/* properties */
	, InternalEnumerator_1_t5005_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5005_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5005_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5005_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5005_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5005_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5005_1_0_0/* this_arg */
	, InternalEnumerator_1_t5005_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5005_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5005_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5005)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8914_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>
extern MethodInfo ICollection_1_get_Count_m50287_MethodInfo;
static PropertyInfo ICollection_1_t8914____Count_PropertyInfo = 
{
	&ICollection_1_t8914_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50287_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50288_MethodInfo;
static PropertyInfo ICollection_1_t8914____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8914_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50288_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8914_PropertyInfos[] =
{
	&ICollection_1_t8914____Count_PropertyInfo,
	&ICollection_1_t8914____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50287_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::get_Count()
MethodInfo ICollection_1_get_Count_m50287_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8914_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50287_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50288_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50288_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8914_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50288_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterBuilder_t1911_0_0_0;
extern Il2CppType ParameterBuilder_t1911_0_0_0;
static ParameterInfo ICollection_1_t8914_ICollection_1_Add_m50289_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ParameterBuilder_t1911_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50289_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::Add(T)
MethodInfo ICollection_1_Add_m50289_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8914_ICollection_1_Add_m50289_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50289_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50290_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::Clear()
MethodInfo ICollection_1_Clear_m50290_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50290_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterBuilder_t1911_0_0_0;
static ParameterInfo ICollection_1_t8914_ICollection_1_Contains_m50291_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ParameterBuilder_t1911_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50291_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::Contains(T)
MethodInfo ICollection_1_Contains_m50291_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8914_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8914_ICollection_1_Contains_m50291_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50291_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterBuilderU5BU5D_t1901_0_0_0;
extern Il2CppType ParameterBuilderU5BU5D_t1901_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8914_ICollection_1_CopyTo_m50292_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ParameterBuilderU5BU5D_t1901_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50292_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50292_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8914_ICollection_1_CopyTo_m50292_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50292_GenericMethod/* genericMethod */

};
extern Il2CppType ParameterBuilder_t1911_0_0_0;
static ParameterInfo ICollection_1_t8914_ICollection_1_Remove_m50293_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ParameterBuilder_t1911_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50293_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ParameterBuilder>::Remove(T)
MethodInfo ICollection_1_Remove_m50293_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8914_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8914_ICollection_1_Remove_m50293_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50293_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8914_MethodInfos[] =
{
	&ICollection_1_get_Count_m50287_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50288_MethodInfo,
	&ICollection_1_Add_m50289_MethodInfo,
	&ICollection_1_Clear_m50290_MethodInfo,
	&ICollection_1_Contains_m50291_MethodInfo,
	&ICollection_1_CopyTo_m50292_MethodInfo,
	&ICollection_1_Remove_m50293_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8916_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8914_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8916_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8914_0_0_0;
extern Il2CppType ICollection_1_t8914_1_0_0;
struct ICollection_1_t8914;
extern Il2CppGenericClass ICollection_1_t8914_GenericClass;
TypeInfo ICollection_1_t8914_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8914_MethodInfos/* methods */
	, ICollection_1_t8914_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8914_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8914_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8914_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8914_0_0_0/* byval_arg */
	, &ICollection_1_t8914_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8914_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ParameterBuilder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ParameterBuilder>
extern Il2CppType IEnumerator_1_t6975_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50294_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ParameterBuilder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50294_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8916_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6975_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50294_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8916_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50294_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8916_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8916_0_0_0;
extern Il2CppType IEnumerable_1_t8916_1_0_0;
struct IEnumerable_1_t8916;
extern Il2CppGenericClass IEnumerable_1_t8916_GenericClass;
TypeInfo IEnumerable_1_t8916_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8916_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8916_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8916_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8916_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8916_0_0_0/* byval_arg */
	, &IEnumerable_1_t8916_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8916_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8915_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.ParameterBuilder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ParameterBuilder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ParameterBuilder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.Emit.ParameterBuilder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ParameterBuilder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Emit.ParameterBuilder>
extern MethodInfo IList_1_get_Item_m50295_MethodInfo;
extern MethodInfo IList_1_set_Item_m50296_MethodInfo;
static PropertyInfo IList_1_t8915____Item_PropertyInfo = 
{
	&IList_1_t8915_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50295_MethodInfo/* get */
	, &IList_1_set_Item_m50296_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8915_PropertyInfos[] =
{
	&IList_1_t8915____Item_PropertyInfo,
	NULL
};
extern Il2CppType ParameterBuilder_t1911_0_0_0;
static ParameterInfo IList_1_t8915_IList_1_IndexOf_m50297_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ParameterBuilder_t1911_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50297_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.ParameterBuilder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50297_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8915_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8915_IList_1_IndexOf_m50297_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50297_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ParameterBuilder_t1911_0_0_0;
static ParameterInfo IList_1_t8915_IList_1_Insert_m50298_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ParameterBuilder_t1911_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50298_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ParameterBuilder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50298_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8915_IList_1_Insert_m50298_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50298_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8915_IList_1_RemoveAt_m50299_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50299_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ParameterBuilder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50299_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8915_IList_1_RemoveAt_m50299_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50299_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8915_IList_1_get_Item_m50295_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ParameterBuilder_t1911_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50295_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.Emit.ParameterBuilder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50295_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8915_il2cpp_TypeInfo/* declaring_type */
	, &ParameterBuilder_t1911_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8915_IList_1_get_Item_m50295_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50295_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ParameterBuilder_t1911_0_0_0;
static ParameterInfo IList_1_t8915_IList_1_set_Item_m50296_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ParameterBuilder_t1911_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50296_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ParameterBuilder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50296_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8915_IList_1_set_Item_m50296_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50296_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8915_MethodInfos[] =
{
	&IList_1_IndexOf_m50297_MethodInfo,
	&IList_1_Insert_m50298_MethodInfo,
	&IList_1_RemoveAt_m50299_MethodInfo,
	&IList_1_get_Item_m50295_MethodInfo,
	&IList_1_set_Item_m50296_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8915_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8914_il2cpp_TypeInfo,
	&IEnumerable_1_t8916_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8915_0_0_0;
extern Il2CppType IList_1_t8915_1_0_0;
struct IList_1_t8915;
extern Il2CppGenericClass IList_1_t8915_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8915_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8915_MethodInfos/* methods */
	, IList_1_t8915_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8915_il2cpp_TypeInfo/* element_class */
	, IList_1_t8915_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8915_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8915_0_0_0/* byval_arg */
	, &IList_1_t8915_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8915_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8917_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>
extern MethodInfo ICollection_1_get_Count_m50300_MethodInfo;
static PropertyInfo ICollection_1_t8917____Count_PropertyInfo = 
{
	&ICollection_1_t8917_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50300_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50301_MethodInfo;
static PropertyInfo ICollection_1_t8917____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8917_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50301_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8917_PropertyInfos[] =
{
	&ICollection_1_t8917____Count_PropertyInfo,
	&ICollection_1_t8917____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50300_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::get_Count()
MethodInfo ICollection_1_get_Count_m50300_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8917_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50300_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50301_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50301_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8917_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50301_GenericMethod/* genericMethod */

};
extern Il2CppType _ParameterBuilder_t2605_0_0_0;
extern Il2CppType _ParameterBuilder_t2605_0_0_0;
static ParameterInfo ICollection_1_t8917_ICollection_1_Add_m50302_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ParameterBuilder_t2605_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50302_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::Add(T)
MethodInfo ICollection_1_Add_m50302_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8917_ICollection_1_Add_m50302_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50302_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50303_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::Clear()
MethodInfo ICollection_1_Clear_m50303_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50303_GenericMethod/* genericMethod */

};
extern Il2CppType _ParameterBuilder_t2605_0_0_0;
static ParameterInfo ICollection_1_t8917_ICollection_1_Contains_m50304_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ParameterBuilder_t2605_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50304_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::Contains(T)
MethodInfo ICollection_1_Contains_m50304_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8917_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8917_ICollection_1_Contains_m50304_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50304_GenericMethod/* genericMethod */

};
extern Il2CppType _ParameterBuilderU5BU5D_t5302_0_0_0;
extern Il2CppType _ParameterBuilderU5BU5D_t5302_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8917_ICollection_1_CopyTo_m50305_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &_ParameterBuilderU5BU5D_t5302_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50305_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50305_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8917_ICollection_1_CopyTo_m50305_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50305_GenericMethod/* genericMethod */

};
extern Il2CppType _ParameterBuilder_t2605_0_0_0;
static ParameterInfo ICollection_1_t8917_ICollection_1_Remove_m50306_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ParameterBuilder_t2605_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50306_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ParameterBuilder>::Remove(T)
MethodInfo ICollection_1_Remove_m50306_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8917_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8917_ICollection_1_Remove_m50306_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50306_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8917_MethodInfos[] =
{
	&ICollection_1_get_Count_m50300_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50301_MethodInfo,
	&ICollection_1_Add_m50302_MethodInfo,
	&ICollection_1_Clear_m50303_MethodInfo,
	&ICollection_1_Contains_m50304_MethodInfo,
	&ICollection_1_CopyTo_m50305_MethodInfo,
	&ICollection_1_Remove_m50306_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8919_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8917_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8919_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8917_0_0_0;
extern Il2CppType ICollection_1_t8917_1_0_0;
struct ICollection_1_t8917;
extern Il2CppGenericClass ICollection_1_t8917_GenericClass;
TypeInfo ICollection_1_t8917_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8917_MethodInfos/* methods */
	, ICollection_1_t8917_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8917_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8917_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8917_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8917_0_0_0/* byval_arg */
	, &ICollection_1_t8917_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8917_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ParameterBuilder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ParameterBuilder>
extern Il2CppType IEnumerator_1_t6977_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50307_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ParameterBuilder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50307_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8919_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6977_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50307_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8919_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50307_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8919_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8919_0_0_0;
extern Il2CppType IEnumerable_1_t8919_1_0_0;
struct IEnumerable_1_t8919;
extern Il2CppGenericClass IEnumerable_1_t8919_GenericClass;
TypeInfo IEnumerable_1_t8919_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8919_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8919_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8919_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8919_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8919_0_0_0/* byval_arg */
	, &IEnumerable_1_t8919_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8919_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6977_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>
extern MethodInfo IEnumerator_1_get_Current_m50308_MethodInfo;
static PropertyInfo IEnumerator_1_t6977____Current_PropertyInfo = 
{
	&IEnumerator_1_t6977_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50308_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6977_PropertyInfos[] =
{
	&IEnumerator_1_t6977____Current_PropertyInfo,
	NULL
};
extern Il2CppType _ParameterBuilder_t2605_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50308_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50308_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6977_il2cpp_TypeInfo/* declaring_type */
	, &_ParameterBuilder_t2605_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50308_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6977_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50308_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6977_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6977_0_0_0;
extern Il2CppType IEnumerator_1_t6977_1_0_0;
struct IEnumerator_1_t6977;
extern Il2CppGenericClass IEnumerator_1_t6977_GenericClass;
TypeInfo IEnumerator_1_t6977_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6977_MethodInfos/* methods */
	, IEnumerator_1_t6977_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6977_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6977_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6977_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6977_0_0_0/* byval_arg */
	, &IEnumerator_1_t6977_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6977_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
