﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.String>
struct DefaultComparer_t3749;
// System.String
struct String_t;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.String>::.ctor()
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_0MethodDeclarations.h"
#define DefaultComparer__ctor_m20905(__this, method) (void)DefaultComparer__ctor_m14758_gshared((DefaultComparer_t2862 *)__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.String>::Compare(T,T)
#define DefaultComparer_Compare_m20906(__this, ___x, ___y, method) (int32_t)DefaultComparer_Compare_m14759_gshared((DefaultComparer_t2862 *)__this, (Object_t *)___x, (Object_t *)___y, method)
