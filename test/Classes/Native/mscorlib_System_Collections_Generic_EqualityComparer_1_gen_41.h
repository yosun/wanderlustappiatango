﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct EqualityComparer_1_t4030;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct EqualityComparer_1_t4030  : public Object_t
{
};
struct EqualityComparer_1_t4030_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::_default
	EqualityComparer_1_t4030 * ____default_0;
};
