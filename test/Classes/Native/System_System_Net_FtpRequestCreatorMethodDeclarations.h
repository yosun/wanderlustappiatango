﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.FtpRequestCreator
struct FtpRequestCreator_t1327;
// System.Net.WebRequest
struct WebRequest_t1321;
// System.Uri
struct Uri_t1322;

// System.Void System.Net.FtpRequestCreator::.ctor()
 void FtpRequestCreator__ctor_m6814 (FtpRequestCreator_t1327 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.FtpRequestCreator::Create(System.Uri)
 WebRequest_t1321 * FtpRequestCreator_Create_m6815 (FtpRequestCreator_t1327 * __this, Uri_t1322 * ___uri, MethodInfo* method) IL2CPP_METHOD_ATTR;
