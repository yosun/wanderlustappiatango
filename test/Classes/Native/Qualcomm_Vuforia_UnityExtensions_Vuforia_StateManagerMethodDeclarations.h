﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.StateManager
struct StateManager_t724;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>
struct IEnumerable_1_t725;
// Vuforia.Trackable
struct Trackable_t550;
// Vuforia.WordManager
struct WordManager_t688;

// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManager::GetActiveTrackableBehaviours()
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManager::GetTrackableBehaviours()
// System.Void Vuforia.StateManager::DestroyTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean)
// Vuforia.WordManager Vuforia.StateManager::GetWordManager()
// System.Void Vuforia.StateManager::.ctor()
 void StateManager__ctor_m3982 (StateManager_t724 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
