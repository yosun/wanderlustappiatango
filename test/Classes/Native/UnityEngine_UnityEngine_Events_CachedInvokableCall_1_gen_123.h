﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.ReflectionProbe>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_125.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.ReflectionProbe>
struct CachedInvokableCall_1_t4491  : public InvokableCall_1_t4492
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.ReflectionProbe>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
