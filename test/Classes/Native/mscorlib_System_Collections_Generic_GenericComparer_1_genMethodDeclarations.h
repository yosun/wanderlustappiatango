﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct GenericComparer_1_t2657;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTime>::.ctor()
 void GenericComparer_1__ctor_m13945 (GenericComparer_1_t2657 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::Compare(T,T)
 int32_t GenericComparer_1_Compare_m31303 (GenericComparer_1_t2657 * __this, DateTime_t110  ___x, DateTime_t110  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
