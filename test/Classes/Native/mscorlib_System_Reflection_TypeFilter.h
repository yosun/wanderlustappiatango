﻿#pragma once
#include <stdint.h>
// System.Type
struct Type_t;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Reflection.TypeFilter
struct TypeFilter_t1933  : public MulticastDelegate_t325
{
};
