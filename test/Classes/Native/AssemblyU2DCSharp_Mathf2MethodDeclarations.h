﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mathf2
struct Mathf2_t15;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t10;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"
// UnityEngine.LayerMask
#include "UnityEngine_UnityEngine_LayerMask.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Ray
#include "UnityEngine_UnityEngine_Ray.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void Mathf2::.ctor()
 void Mathf2__ctor_m26 (Mathf2_t15 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mathf2::.cctor()
 void Mathf2__cctor_m27 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreenIgnore0()
 RaycastHit_t16  Mathf2_WhatDidWeHitCenterScreenIgnore0_m28 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen(UnityEngine.LayerMask)
 RaycastHit_t16  Mathf2_WhatDidWeHitCenterScreen_m29 (Object_t * __this/* static, unused */, LayerMask_t17  ___lm, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHitCenterScreen()
 RaycastHit_t16  Mathf2_WhatDidWeHitCenterScreen_m30 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2)
 RaycastHit_t16  Mathf2_WhatDidWeHit_m31 (Object_t * __this/* static, unused */, Vector2_t9  ___v, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Vector2,UnityEngine.LayerMask)
 RaycastHit_t16  Mathf2_WhatDidWeHit_m32 (Object_t * __this/* static, unused */, Vector2_t9  ___v, LayerMask_t17  ___lm, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit Mathf2::WhatDidWeHit(UnityEngine.Ray,UnityEngine.LayerMask)
 RaycastHit_t16  Mathf2_WhatDidWeHit_m33 (Object_t * __this/* static, unused */, Ray_t18  ___ray, LayerMask_t17  ___lm, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::Pad0s(System.Int32)
 String_t* Mathf2_Pad0s_m34 (Object_t * __this/* static, unused */, int32_t ___n, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mathf2::Round10th(System.Single)
 float Mathf2_Round10th_m35 (Object_t * __this/* static, unused */, float ___n, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GenUUID()
 String_t* Mathf2_GenUUID_m36 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Mathf2::RoundVector3(UnityEngine.Vector3)
 Vector3_t13  Mathf2_RoundVector3_m37 (Object_t * __this/* static, unused */, Vector3_t13  ___v, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Mathf2::RandRot()
 Quaternion_t12  Mathf2_RandRot_m38 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mathf2::round(System.Single)
 float Mathf2_round_m39 (Object_t * __this/* static, unused */, float ___f, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mathf2::String2Float(System.String)
 float Mathf2_String2Float_m40 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mathf2::isNumeric(System.String)
 bool Mathf2_isNumeric_m41 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mathf2::String2Int(System.String)
 int32_t Mathf2_String2Int_m42 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Mathf2::String2Color(System.String)
 Color_t19  Mathf2_String2Color_m43 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Mathf2::String2Vector3(System.String)
 Vector3_t13  Mathf2_String2Vector3_m44 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Mathf2::String2Vector2(System.String)
 Vector2_t9  Mathf2_String2Vector2_m45 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mathf2::RoundFraction(System.Single,System.Single)
 float Mathf2_RoundFraction_m46 (Object_t * __this/* static, unused */, float ___val, float ___denominator, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion Mathf2::String2Quat(System.String)
 Quaternion_t12  Mathf2_String2Quat_m47 (Object_t * __this/* static, unused */, String_t* ___str, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Mathf2::UnsignedVector3(UnityEngine.Vector3)
 Vector3_t13  Mathf2_UnsignedVector3_m48 (Object_t * __this/* static, unused */, Vector3_t13  ___v, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GetUnixTime()
 String_t* Mathf2_GetUnixTime_m49 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GetPositionString(UnityEngine.Transform)
 String_t* Mathf2_GetPositionString_m50 (Object_t * __this/* static, unused */, Transform_t10 * ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GetRotationString(UnityEngine.Transform)
 String_t* Mathf2_GetRotationString_m51 (Object_t * __this/* static, unused */, Transform_t10 * ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mathf2::GetScaleString(UnityEngine.Transform)
 String_t* Mathf2_GetScaleString_m52 (Object_t * __this/* static, unused */, Transform_t10 * ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
