﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Collider>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_142.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Collider>
struct CachedInvokableCall_1_t4629  : public InvokableCall_1_t4630
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Collider>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
