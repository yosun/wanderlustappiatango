﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>
struct IndexedSet_1_t473;
// UnityEngine.UI.Graphic
struct Graphic_t293;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Graphic>
struct IEnumerator_1_t3369;
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t3367;
// System.Predicate`1<UnityEngine.UI.Graphic>
struct Predicate_1_t3371;
// System.Comparison`1<UnityEngine.UI.Graphic>
struct Comparison_1_t298;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"
#define IndexedSet_1__ctor_m2181(__this, method) (void)IndexedSet_1__ctor_m17125_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m18530(__this, method) (Object_t *)IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17127_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m2180(__this, ___item, method) (void)IndexedSet_1_Add_m17128_gshared((IndexedSet_1_t3223 *)__this, (Object_t *)___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m2183(__this, ___item, method) (bool)IndexedSet_1_Remove_m17129_gshared((IndexedSet_1_t3223 *)__this, (Object_t *)___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m18531(__this, method) (Object_t*)IndexedSet_1_GetEnumerator_m17131_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m18532(__this, method) (void)IndexedSet_1_Clear_m17132_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m18533(__this, ___item, method) (bool)IndexedSet_1_Contains_m17134_gshared((IndexedSet_1_t3223 *)__this, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m18534(__this, ___array, ___arrayIndex, method) (void)IndexedSet_1_CopyTo_m17136_gshared((IndexedSet_1_t3223 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m18535(__this, method) (int32_t)IndexedSet_1_get_Count_m17137_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m18536(__this, method) (bool)IndexedSet_1_get_IsReadOnly_m17139_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m18537(__this, ___item, method) (int32_t)IndexedSet_1_IndexOf_m17141_gshared((IndexedSet_1_t3223 *)__this, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m18538(__this, ___index, ___item, method) (void)IndexedSet_1_Insert_m17143_gshared((IndexedSet_1_t3223 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m18539(__this, ___index, method) (void)IndexedSet_1_RemoveAt_m17145_gshared((IndexedSet_1_t3223 *)__this, (int32_t)___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m18540(__this, ___index, method) (Graphic_t293 *)IndexedSet_1_get_Item_m17146_gshared((IndexedSet_1_t3223 *)__this, (int32_t)___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m18541(__this, ___index, ___value, method) (void)IndexedSet_1_set_Item_m17148_gshared((IndexedSet_1_t3223 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m18542(__this, ___match, method) (void)IndexedSet_1_RemoveAll_m17149_gshared((IndexedSet_1_t3223 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m18543(__this, ___sortLayoutFunction, method) (void)IndexedSet_1_Sort_m17150_gshared((IndexedSet_1_t3223 *)__this, (Comparison_1_t2846 *)___sortLayoutFunction, method)
