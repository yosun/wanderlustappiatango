﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>
struct Enumerator_t803;
// System.Object
struct Object_t;
// Vuforia.DataSetImpl
struct DataSetImpl_t566;
// System.Collections.Generic.List`1<Vuforia.DataSetImpl>
struct List_1_t618;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m21815(__this, ___l, method) (void)Enumerator__ctor_m14640_gshared((Enumerator_t2850 *)__this, (List_1_t150 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21816(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14641_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::Dispose()
#define Enumerator_Dispose_m21817(__this, method) (void)Enumerator_Dispose_m14642_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::VerifyState()
#define Enumerator_VerifyState_m21818(__this, method) (void)Enumerator_VerifyState_m14643_gshared((Enumerator_t2850 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::MoveNext()
#define Enumerator_MoveNext_m4776(__this, method) (bool)Enumerator_MoveNext_m14644_gshared((Enumerator_t2850 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.DataSetImpl>::get_Current()
#define Enumerator_get_Current_m4773(__this, method) (DataSetImpl_t566 *)Enumerator_get_Current_m14645_gshared((Enumerator_t2850 *)__this, method)
