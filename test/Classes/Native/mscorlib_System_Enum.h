﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t108;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Enum
struct Enum_t97  : public ValueType_t436
{
};
struct Enum_t97_StaticFields{
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t108* ___split_char_0;
};
