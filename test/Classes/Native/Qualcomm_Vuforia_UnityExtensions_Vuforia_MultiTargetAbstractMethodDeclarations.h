﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t59;
// Vuforia.MultiTarget
struct MultiTarget_t573;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t553;
// UnityEngine.Transform
struct Transform_t10;
// UnityEngine.GameObject
struct GameObject_t2;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// Vuforia.MultiTarget Vuforia.MultiTargetAbstractBehaviour::get_MultiTarget()
 Object_t * MultiTargetAbstractBehaviour_get_MultiTarget_m4087 (MultiTargetAbstractBehaviour_t59 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetAbstractBehaviour::InternalUnregisterTrackable()
 void MultiTargetAbstractBehaviour_InternalUnregisterTrackable_m495 (MultiTargetAbstractBehaviour_t59 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetAbstractBehaviour::CalculateDefaultOccluderBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
 void MultiTargetAbstractBehaviour_CalculateDefaultOccluderBounds_m497 (MultiTargetAbstractBehaviour_t59 * __this, Vector3_t13 * ___boundsMin, Vector3_t13 * ___boundsMax, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetAbstractBehaviour::ProtectedSetAsSmartTerrainInitializationTarget(Vuforia.ReconstructionFromTarget)
 void MultiTargetAbstractBehaviour_ProtectedSetAsSmartTerrainInitializationTarget_m498 (MultiTargetAbstractBehaviour_t59 * __this, Object_t * ___reconstructionFromTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetAbstractBehaviour::Vuforia.IEditorMultiTargetBehaviour.InitializeMultiTarget(Vuforia.MultiTarget)
 void MultiTargetAbstractBehaviour_Vuforia_IEditorMultiTargetBehaviour_InitializeMultiTarget_m499 (MultiTargetAbstractBehaviour_t59 * __this, Object_t * ___multiTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetAbstractBehaviour::.ctor()
 void MultiTargetAbstractBehaviour__ctor_m490 (MultiTargetAbstractBehaviour_t59 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MultiTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m491 (MultiTargetAbstractBehaviour_t59 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MultiTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m492 (MultiTargetAbstractBehaviour_t59 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.MultiTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t10 * MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m493 (MultiTargetAbstractBehaviour_t59 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.MultiTargetAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t2 * MultiTargetAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m494 (MultiTargetAbstractBehaviour_t59 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
