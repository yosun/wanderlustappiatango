﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.MonoEventInfo
struct MonoEventInfo_t1934;
// System.Reflection.MonoEvent
struct MonoEvent_t1935;
// System.Reflection.MonoEventInfo
#include "mscorlib_System_Reflection_MonoEventInfo.h"

// System.Void System.Reflection.MonoEventInfo::get_event_info(System.Reflection.MonoEvent,System.Reflection.MonoEventInfo&)
 void MonoEventInfo_get_event_info_m11249 (Object_t * __this/* static, unused */, MonoEvent_t1935 * ___ev, MonoEventInfo_t1934 * ___info, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MonoEventInfo System.Reflection.MonoEventInfo::GetEventInfo(System.Reflection.MonoEvent)
 MonoEventInfo_t1934  MonoEventInfo_GetEventInfo_m11250 (Object_t * __this/* static, unused */, MonoEvent_t1935 * ___ev, MethodInfo* method) IL2CPP_METHOD_ATTR;
