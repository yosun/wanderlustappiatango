﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Marker,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>>
struct Transform_1_t3883;
// System.Object
struct Object_t;
// Vuforia.Marker
struct Marker_t623;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_12.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Marker,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m22172 (Transform_1_t3883 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Marker,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>>::Invoke(TKey,TValue)
 KeyValuePair_2_t3874  Transform_1_Invoke_m22173 (Transform_1_t3883 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Marker,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m22174 (Transform_1_t3883 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Marker,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>>::EndInvoke(System.IAsyncResult)
 KeyValuePair_2_t3874  Transform_1_EndInvoke_m22175 (Transform_1_t3883 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
