﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct Enumerator_t4290;
// System.Object
struct Object_t;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t56;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct Dictionary_2_t750;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_23.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void Enumerator__ctor_m26157 (Enumerator_t4290 * __this, Dictionary_2_t750 * ___dictionary, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26158 (Enumerator_t4290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Entry()
 DictionaryEntry_t1302  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26159 (Enumerator_t4290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Key()
 Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26160 (Enumerator_t4290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Value()
 Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26161 (Enumerator_t4290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::MoveNext()
 bool Enumerator_MoveNext_m26162 (Enumerator_t4290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
 KeyValuePair_2_t4288  Enumerator_get_Current_m26163 (Enumerator_t4290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_CurrentKey()
 int32_t Enumerator_get_CurrentKey_m26164 (Enumerator_t4290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_CurrentValue()
 VirtualButtonAbstractBehaviour_t56 * Enumerator_get_CurrentValue_m26165 (Enumerator_t4290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::VerifyState()
 void Enumerator_VerifyState_m26166 (Enumerator_t4290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::VerifyCurrent()
 void Enumerator_VerifyCurrent_m26167 (Enumerator_t4290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::Dispose()
 void Enumerator_Dispose_m26168 (Enumerator_t4290 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
