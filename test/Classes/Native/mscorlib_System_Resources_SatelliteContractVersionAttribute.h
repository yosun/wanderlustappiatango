﻿#pragma once
#include <stdint.h>
// System.Version
struct Version_t1333;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Resources.SatelliteContractVersionAttribute
struct SatelliteContractVersionAttribute_t1281  : public Attribute_t146
{
	// System.Version System.Resources.SatelliteContractVersionAttribute::ver
	Version_t1333 * ___ver_0;
};
