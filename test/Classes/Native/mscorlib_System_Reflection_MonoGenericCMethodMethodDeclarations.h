﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.MonoGenericCMethod
struct MonoGenericCMethod_t1939;
// System.Type
struct Type_t;

// System.Void System.Reflection.MonoGenericCMethod::.ctor()
 void MonoGenericCMethod__ctor_m11283 (MonoGenericCMethod_t1939 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Reflection.MonoGenericCMethod::get_ReflectedType()
 Type_t * MonoGenericCMethod_get_ReflectedType_m11284 (MonoGenericCMethod_t1939 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
