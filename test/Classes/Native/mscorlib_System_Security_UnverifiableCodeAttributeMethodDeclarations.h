﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.UnverifiableCodeAttribute
struct UnverifiableCodeAttribute_t2139;

// System.Void System.Security.UnverifiableCodeAttribute::.ctor()
 void UnverifiableCodeAttribute__ctor_m12208 (UnverifiableCodeAttribute_t2139 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
