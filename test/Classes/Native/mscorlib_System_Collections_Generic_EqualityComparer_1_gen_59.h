﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.MeshRenderer>
struct EqualityComparer_1_t4396;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.MeshRenderer>
struct EqualityComparer_1_t4396  : public Object_t
{
};
struct EqualityComparer_1_t4396_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.MeshRenderer>::_default
	EqualityComparer_1_t4396 * ____default_0;
};
