﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.RectangleData>
struct InternalEnumerator_1_t3958;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.RectangleData>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m22943 (InternalEnumerator_1_t3958 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.RectangleData>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22944 (InternalEnumerator_1_t3958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.RectangleData>::Dispose()
 void InternalEnumerator_1_Dispose_m22945 (InternalEnumerator_1_t3958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.RectangleData>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m22946 (InternalEnumerator_1_t3958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.RectangleData>::get_Current()
 RectangleData_t588  InternalEnumerator_1_get_Current_m22947 (InternalEnumerator_1_t3958 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
