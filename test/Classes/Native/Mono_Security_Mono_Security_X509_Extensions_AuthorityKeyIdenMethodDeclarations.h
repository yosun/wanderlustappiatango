﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension
struct AuthorityKeyIdentifierExtension_t1518;
// System.Byte[]
struct ByteU5BU5D_t609;
// Mono.Security.X509.X509Extension
struct X509Extension_t1378;
// System.String
struct String_t;

// System.Void Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension::.ctor(Mono.Security.X509.X509Extension)
 void AuthorityKeyIdentifierExtension__ctor_m7845 (AuthorityKeyIdentifierExtension_t1518 * __this, X509Extension_t1378 * ___extension, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension::Decode()
 void AuthorityKeyIdentifierExtension_Decode_m8242 (AuthorityKeyIdentifierExtension_t1518 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension::get_Identifier()
 ByteU5BU5D_t609* AuthorityKeyIdentifierExtension_get_Identifier_m7846 (AuthorityKeyIdentifierExtension_t1518 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.Extensions.AuthorityKeyIdentifierExtension::ToString()
 String_t* AuthorityKeyIdentifierExtension_ToString_m8243 (AuthorityKeyIdentifierExtension_t1518 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
