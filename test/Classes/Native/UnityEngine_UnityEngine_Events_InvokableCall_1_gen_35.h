﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.TurnOffWordBehaviour>
struct UnityAction_1_t2954;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>
struct InvokableCall_1_t2953  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.TurnOffWordBehaviour>::Delegate
	UnityAction_1_t2954 * ___Delegate_0;
};
