﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.FixupRecord
struct FixupRecord_t2076;
// System.Runtime.Serialization.ObjectRecord
struct ObjectRecord_t2072;
// System.Reflection.MemberInfo
struct MemberInfo_t145;
// System.Runtime.Serialization.ObjectManager
struct ObjectManager_t2065;

// System.Void System.Runtime.Serialization.FixupRecord::.ctor(System.Runtime.Serialization.ObjectRecord,System.Reflection.MemberInfo,System.Runtime.Serialization.ObjectRecord)
 void FixupRecord__ctor_m11784 (FixupRecord_t2076 * __this, ObjectRecord_t2072 * ___objectToBeFixed, MemberInfo_t145 * ___member, ObjectRecord_t2072 * ___objectRequired, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.FixupRecord::FixupImpl(System.Runtime.Serialization.ObjectManager)
 void FixupRecord_FixupImpl_m11785 (FixupRecord_t2076 * __this, ObjectManager_t2065 * ___manager, MethodInfo* method) IL2CPP_METHOD_ATTR;
