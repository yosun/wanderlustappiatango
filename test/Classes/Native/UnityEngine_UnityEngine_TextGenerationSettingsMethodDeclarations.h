﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.TextGenerationSettings
struct TextGenerationSettings_t365;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.TextGenerationSettings
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"

// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
 bool TextGenerationSettings_CompareColors_m6344 (TextGenerationSettings_t365 * __this, Color_t19  ___left, Color_t19  ___right, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
 bool TextGenerationSettings_CompareVector2_m6345 (TextGenerationSettings_t365 * __this, Vector2_t9  ___left, Vector2_t9  ___right, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
 bool TextGenerationSettings_Equals_m6346 (TextGenerationSettings_t365 * __this, TextGenerationSettings_t365  ___other, MethodInfo* method) IL2CPP_METHOD_ATTR;
