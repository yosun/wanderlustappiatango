﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.Transform>
struct Comparer_1_t3135;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Transform>
struct Comparer_1_t3135  : public Object_t
{
};
struct Comparer_1_t3135_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Transform>::_default
	Comparer_1_t3135 * ____default_0;
};
