﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Globalization.CCMath
struct CCMath_t1849;

// System.Int32 System.Globalization.CCMath::div(System.Int32,System.Int32)
 int32_t CCMath_div_m10465 (Object_t * __this/* static, unused */, int32_t ___x, int32_t ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCMath::mod(System.Int32,System.Int32)
 int32_t CCMath_mod_m10466 (Object_t * __this/* static, unused */, int32_t ___x, int32_t ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Globalization.CCMath::div_mod(System.Int32&,System.Int32,System.Int32)
 int32_t CCMath_div_mod_m10467 (Object_t * __this/* static, unused */, int32_t* ___remainder, int32_t ___x, int32_t ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
