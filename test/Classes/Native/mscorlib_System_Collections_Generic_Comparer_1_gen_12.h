﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.Canvas>
struct Comparer_1_t3360;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.Canvas>
struct Comparer_1_t3360  : public Object_t
{
};
struct Comparer_1_t3360_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.Canvas>::_default
	Comparer_1_t3360 * ____default_0;
};
