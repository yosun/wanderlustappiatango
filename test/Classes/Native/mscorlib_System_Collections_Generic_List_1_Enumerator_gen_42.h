﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct List_1_t371;
// UnityEngine.UI.Toggle
struct Toggle_t370;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>
struct Enumerator_t3539 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::l
	List_1_t371 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>::current
	Toggle_t370 * ___current_3;
};
