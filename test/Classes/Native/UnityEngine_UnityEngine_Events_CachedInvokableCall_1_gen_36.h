﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_32.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>
struct CachedInvokableCall_1_t2935  : public InvokableCall_1_t2936
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.SurfaceBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
