﻿#pragma once
#include <stdint.h>
// Vuforia.Surface
struct Surface_t43;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Surface>
struct Comparison_1_t4148  : public MulticastDelegate_t325
{
};
