﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$48
struct $ArrayType$48_t2268;
struct $ArrayType$48_t2268_marshaled;

void $ArrayType$48_t2268_marshal(const $ArrayType$48_t2268& unmarshaled, $ArrayType$48_t2268_marshaled& marshaled);
void $ArrayType$48_t2268_marshal_back(const $ArrayType$48_t2268_marshaled& marshaled, $ArrayType$48_t2268& unmarshaled);
void $ArrayType$48_t2268_marshal_cleanup($ArrayType$48_t2268_marshaled& marshaled);
