﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.Word>
struct EqualityComparer_1_t3992;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.Word>
struct EqualityComparer_1_t3992  : public Object_t
{
};
struct EqualityComparer_1_t3992_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.Word>::_default
	EqualityComparer_1_t3992 * ____default_0;
};
