﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// System.Single[]
struct SingleU5BU5D_t578;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t1009  : public MulticastDelegate_t325
{
};
