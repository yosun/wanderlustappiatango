﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.TypeLibVersionAttribute
struct TypeLibVersionAttribute_t1978;

// System.Void System.Runtime.InteropServices.TypeLibVersionAttribute::.ctor(System.Int32,System.Int32)
 void TypeLibVersionAttribute__ctor_m11438 (TypeLibVersionAttribute_t1978 * __this, int32_t ___major, int32_t ___minor, MethodInfo* method) IL2CPP_METHOD_ATTR;
