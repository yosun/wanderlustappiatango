﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_11.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
struct CachedInvokableCall_1_t2777  : public InvokableCall_1_t2778
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
