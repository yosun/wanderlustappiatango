﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.IOSUnityPlayer
struct IOSUnityPlayer_t53;
// System.String
struct String_t;
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void Vuforia.IOSUnityPlayer::.ctor()
 void IOSUnityPlayer__ctor_m133 (IOSUnityPlayer_t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::LoadNativeLibraries()
 void IOSUnityPlayer_LoadNativeLibraries_m134 (IOSUnityPlayer_t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::InitializePlatform()
 void IOSUnityPlayer_InitializePlatform_m135 (IOSUnityPlayer_t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARUnity/InitError Vuforia.IOSUnityPlayer::Start(System.String)
 int32_t IOSUnityPlayer_Start_m136 (IOSUnityPlayer_t53 * __this, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::Update()
 void IOSUnityPlayer_Update_m137 (IOSUnityPlayer_t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::Dispose()
 void IOSUnityPlayer_Dispose_m138 (IOSUnityPlayer_t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::OnPause()
 void IOSUnityPlayer_OnPause_m139 (IOSUnityPlayer_t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::OnResume()
 void IOSUnityPlayer_OnResume_m140 (IOSUnityPlayer_t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::OnDestroy()
 void IOSUnityPlayer_OnDestroy_m141 (IOSUnityPlayer_t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::InitializeSurface()
 void IOSUnityPlayer_InitializeSurface_m142 (IOSUnityPlayer_t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::SetUnityScreenOrientation()
 void IOSUnityPlayer_SetUnityScreenOrientation_m143 (IOSUnityPlayer_t53 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::setPlatFormNative()
 void IOSUnityPlayer_setPlatFormNative_m144 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.IOSUnityPlayer::initQCARiOS(System.Int32,System.String)
 int32_t IOSUnityPlayer_initQCARiOS_m145 (Object_t * __this/* static, unused */, int32_t ___screenOrientation, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.IOSUnityPlayer::setSurfaceOrientationiOS(System.Int32)
 void IOSUnityPlayer_setSurfaceOrientationiOS_m146 (Object_t * __this/* static, unused */, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
