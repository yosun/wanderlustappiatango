﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.EventTrigger/Entry
struct Entry_t193;

// System.Void UnityEngine.EventSystems.EventTrigger/Entry::.ctor()
 void Entry__ctor_m707 (Entry_t193 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
