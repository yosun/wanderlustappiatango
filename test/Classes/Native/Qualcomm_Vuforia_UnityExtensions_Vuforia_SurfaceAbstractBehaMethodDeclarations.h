﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t77;
// Vuforia.Surface
struct Surface_t43;
// UnityEngine.MeshFilter
struct MeshFilter_t169;
// UnityEngine.MeshCollider
struct MeshCollider_t582;
// UnityEngine.Transform
struct Transform_t10;
// UnityEngine.GameObject
struct GameObject_t2;

// Vuforia.Surface Vuforia.SurfaceAbstractBehaviour::get_Surface()
 Object_t * SurfaceAbstractBehaviour_get_Surface_m2771 (SurfaceAbstractBehaviour_t77 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::InternalUnregisterTrackable()
 void SurfaceAbstractBehaviour_InternalUnregisterTrackable_m581 (SurfaceAbstractBehaviour_t77 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.InitializeSurface(Vuforia.Surface)
 void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_InitializeSurface_m584 (SurfaceAbstractBehaviour_t77 * __this, Object_t * ___surface, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.SetMeshFilterToUpdate(UnityEngine.MeshFilter)
 void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshFilterToUpdate_m585 (SurfaceAbstractBehaviour_t77 * __this, MeshFilter_t169 * ___meshFilterToUpdate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshFilter Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.get_MeshFilterToUpdate()
 MeshFilter_t169 * SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshFilterToUpdate_m586 (SurfaceAbstractBehaviour_t77 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.SetMeshColliderToUpdate(UnityEngine.MeshCollider)
 void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshColliderToUpdate_m587 (SurfaceAbstractBehaviour_t77 * __this, MeshCollider_t582 * ___meshColliderToUpdate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshCollider Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.get_MeshColliderToUpdate()
 MeshCollider_t582 * SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshColliderToUpdate_m588 (SurfaceAbstractBehaviour_t77 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::.ctor()
 void SurfaceAbstractBehaviour__ctor_m576 (SurfaceAbstractBehaviour_t77 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m577 (SurfaceAbstractBehaviour_t77 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m578 (SurfaceAbstractBehaviour_t77 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t10 * SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m579 (SurfaceAbstractBehaviour_t77 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t2 * SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m580 (SurfaceAbstractBehaviour_t77 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
