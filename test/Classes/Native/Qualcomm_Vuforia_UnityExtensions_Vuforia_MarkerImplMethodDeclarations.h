﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.MarkerImpl
struct MarkerImpl_t621;
// System.String
struct String_t;

// System.Int32 Vuforia.MarkerImpl::get_MarkerID()
 int32_t MarkerImpl_get_MarkerID_m2938 (MarkerImpl_t621 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerImpl::set_MarkerID(System.Int32)
 void MarkerImpl_set_MarkerID_m2939 (MarkerImpl_t621 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerImpl::.ctor(System.String,System.Int32,System.Single,System.Int32)
 void MarkerImpl__ctor_m2940 (MarkerImpl_t621 * __this, String_t* ___name, int32_t ___id, float ___size, int32_t ___markerID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.MarkerImpl::GetSize()
 float MarkerImpl_GetSize_m2941 (MarkerImpl_t621 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.MarkerImpl::SetSize(System.Single)
 void MarkerImpl_SetSize_m2942 (MarkerImpl_t621 * __this, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerImpl::StartExtendedTracking()
 bool MarkerImpl_StartExtendedTracking_m2943 (MarkerImpl_t621 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.MarkerImpl::StopExtendedTracking()
 bool MarkerImpl_StopExtendedTracking_m2944 (MarkerImpl_t621 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
