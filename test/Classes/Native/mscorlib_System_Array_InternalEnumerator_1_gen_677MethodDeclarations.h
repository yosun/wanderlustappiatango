﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>
struct InternalEnumerator_1_t5062;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Runtime.InteropServices.GCHandleType
#include "mscorlib_System_Runtime_InteropServices_GCHandleType.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30890 (InternalEnumerator_1_t5062 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30891 (InternalEnumerator_1_t5062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::Dispose()
 void InternalEnumerator_1_Dispose_m30892 (InternalEnumerator_1_t5062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30893 (InternalEnumerator_1_t5062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.GCHandleType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30894 (InternalEnumerator_1_t5062 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
