﻿#pragma once
#include <stdint.h>
// System.Text.Encoding
struct Encoding_t1513;
// System.Text.Decoder
#include "mscorlib_System_Text_Decoder.h"
// System.Text.Encoding/ForwardingDecoder
struct ForwardingDecoder_t2155  : public Decoder_t1862
{
	// System.Text.Encoding System.Text.Encoding/ForwardingDecoder::encoding
	Encoding_t1513 * ___encoding_2;
};
