﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$256
struct $ArrayType$256_t2276;
struct $ArrayType$256_t2276_marshaled;

void $ArrayType$256_t2276_marshal(const $ArrayType$256_t2276& unmarshaled, $ArrayType$256_t2276_marshaled& marshaled);
void $ArrayType$256_t2276_marshal_back(const $ArrayType$256_t2276_marshaled& marshaled, $ArrayType$256_t2276& unmarshaled);
void $ArrayType$256_t2276_marshal_cleanup($ArrayType$256_t2276_marshaled& marshaled);
