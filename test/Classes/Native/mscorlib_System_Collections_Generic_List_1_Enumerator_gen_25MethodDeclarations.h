﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_t2850;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t150;

// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
 void Enumerator__ctor_m14640_gshared (Enumerator_t2850 * __this, List_1_t150 * ___l, MethodInfo* method);
#define Enumerator__ctor_m14640(__this, ___l, method) (void)Enumerator__ctor_m14640_gshared((Enumerator_t2850 *)__this, (List_1_t150 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14641_gshared (Enumerator_t2850 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14641(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14641_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
 void Enumerator_Dispose_m14642_gshared (Enumerator_t2850 * __this, MethodInfo* method);
#define Enumerator_Dispose_m14642(__this, method) (void)Enumerator_Dispose_m14642_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
 void Enumerator_VerifyState_m14643_gshared (Enumerator_t2850 * __this, MethodInfo* method);
#define Enumerator_VerifyState_m14643(__this, method) (void)Enumerator_VerifyState_m14643_gshared((Enumerator_t2850 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
 bool Enumerator_MoveNext_m14644_gshared (Enumerator_t2850 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m14644(__this, method) (bool)Enumerator_MoveNext_m14644_gshared((Enumerator_t2850 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
 Object_t * Enumerator_get_Current_m14645_gshared (Enumerator_t2850 * __this, MethodInfo* method);
#define Enumerator_get_Current_m14645(__this, method) (Object_t *)Enumerator_get_Current_m14645_gshared((Enumerator_t2850 *)__this, method)
