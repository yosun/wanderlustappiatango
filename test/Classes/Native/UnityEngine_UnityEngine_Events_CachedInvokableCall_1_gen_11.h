﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<WorldLoop>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_7.h"
// UnityEngine.Events.CachedInvokableCall`1<WorldLoop>
struct CachedInvokableCall_1_t2756  : public InvokableCall_1_t2757
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<WorldLoop>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
