﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>
struct KeyValuePair_2_t3802;
// Vuforia.Trackable
struct Trackable_t550;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m21383 (KeyValuePair_2_t3802 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::get_Key()
 int32_t KeyValuePair_2_get_Key_m21384 (KeyValuePair_2_t3802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m21385 (KeyValuePair_2_t3802 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::get_Value()
 Object_t * KeyValuePair_2_get_Value_m21386 (KeyValuePair_2_t3802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m21387 (KeyValuePair_2_t3802 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>::ToString()
 String_t* KeyValuePair_2_ToString_m21388 (KeyValuePair_2_t3802 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
