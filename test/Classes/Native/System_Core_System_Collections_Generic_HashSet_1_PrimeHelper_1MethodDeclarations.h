﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/PrimeHelper<UnityEngine.MeshRenderer>
struct PrimeHelper_t4398;

// System.Void System.Collections.Generic.HashSet`1/PrimeHelper<UnityEngine.MeshRenderer>::.cctor()
// System.Collections.Generic.HashSet`1/PrimeHelper<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_PrimeHelper_0MethodDeclarations.h"
#define PrimeHelper__cctor_m26955(__this/* static, unused */, method) (void)PrimeHelper__cctor_m26934_gshared((Object_t *)__this/* static, unused */, method)
// System.Boolean System.Collections.Generic.HashSet`1/PrimeHelper<UnityEngine.MeshRenderer>::TestPrime(System.Int32)
#define PrimeHelper_TestPrime_m26956(__this/* static, unused */, ___x, method) (bool)PrimeHelper_TestPrime_m26935_gshared((Object_t *)__this/* static, unused */, (int32_t)___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<UnityEngine.MeshRenderer>::CalcPrime(System.Int32)
#define PrimeHelper_CalcPrime_m26957(__this/* static, unused */, ___x, method) (int32_t)PrimeHelper_CalcPrime_m26936_gshared((Object_t *)__this/* static, unused */, (int32_t)___x, method)
// System.Int32 System.Collections.Generic.HashSet`1/PrimeHelper<UnityEngine.MeshRenderer>::ToPrime(System.Int32)
#define PrimeHelper_ToPrime_m26958(__this/* static, unused */, ___x, method) (int32_t)PrimeHelper_ToPrime_m26937_gshared((Object_t *)__this/* static, unused */, (int32_t)___x, method)
