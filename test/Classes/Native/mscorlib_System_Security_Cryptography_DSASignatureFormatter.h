﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.DSA
struct DSA_t1355;
// System.Security.Cryptography.AsymmetricSignatureFormatter
#include "mscorlib_System_Security_Cryptography_AsymmetricSignatureFor.h"
// System.Security.Cryptography.DSASignatureFormatter
struct DSASignatureFormatter_t2095  : public AsymmetricSignatureFormatter_t1621
{
	// System.Security.Cryptography.DSA System.Security.Cryptography.DSASignatureFormatter::dsa
	DSA_t1355 * ___dsa_0;
};
