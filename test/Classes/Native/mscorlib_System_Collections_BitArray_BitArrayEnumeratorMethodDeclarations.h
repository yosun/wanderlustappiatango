﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.BitArray/BitArrayEnumerator
struct BitArrayEnumerator_t1826;
// System.Object
struct Object_t;
// System.Collections.BitArray
struct BitArray_t1426;

// System.Void System.Collections.BitArray/BitArrayEnumerator::.ctor(System.Collections.BitArray)
 void BitArrayEnumerator__ctor_m10329 (BitArrayEnumerator_t1826 * __this, BitArray_t1426 * ___ba, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.BitArray/BitArrayEnumerator::get_Current()
 Object_t * BitArrayEnumerator_get_Current_m10330 (BitArrayEnumerator_t1826 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray/BitArrayEnumerator::MoveNext()
 bool BitArrayEnumerator_MoveNext_m10331 (BitArrayEnumerator_t1826 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray/BitArrayEnumerator::checkVersion()
 void BitArrayEnumerator_checkVersion_m10332 (BitArrayEnumerator_t1826 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
