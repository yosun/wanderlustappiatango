﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>
struct InternalEnumerator_1_t5096;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Runtime.Serialization.ObjectRecordStatus
#include "mscorlib_System_Runtime_Serialization_ObjectRecordStatus.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31060 (InternalEnumerator_1_t5096 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31061 (InternalEnumerator_1_t5096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>::Dispose()
 void InternalEnumerator_1_Dispose_m31062 (InternalEnumerator_1_t5096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31063 (InternalEnumerator_1_t5096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.ObjectRecordStatus>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m31064 (InternalEnumerator_1_t5096 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
