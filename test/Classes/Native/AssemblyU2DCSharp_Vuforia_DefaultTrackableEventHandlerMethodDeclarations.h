﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DefaultTrackableEventHandler
struct DefaultTrackableEventHandler_t45;
// Vuforia.TrackableBehaviour/Status
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_.h"

// System.Void Vuforia.DefaultTrackableEventHandler::.ctor()
 void DefaultTrackableEventHandler__ctor_m103 (DefaultTrackableEventHandler_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultTrackableEventHandler::Start()
 void DefaultTrackableEventHandler_Start_m104 (DefaultTrackableEventHandler_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackableStateChanged(Vuforia.TrackableBehaviour/Status,Vuforia.TrackableBehaviour/Status)
 void DefaultTrackableEventHandler_OnTrackableStateChanged_m105 (DefaultTrackableEventHandler_t45 * __this, int32_t ___previousStatus, int32_t ___newStatus, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingFound()
 void DefaultTrackableEventHandler_OnTrackingFound_m106 (DefaultTrackableEventHandler_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DefaultTrackableEventHandler::OnTrackingLost()
 void DefaultTrackableEventHandler_OnTrackingLost_m107 (DefaultTrackableEventHandler_t45 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
