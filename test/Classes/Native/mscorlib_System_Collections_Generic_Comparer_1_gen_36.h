﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.TrackableBehaviour>
struct Comparer_1_t4059;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.TrackableBehaviour>
struct Comparer_1_t4059  : public Object_t
{
};
struct Comparer_1_t4059_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.TrackableBehaviour>::_default
	Comparer_1_t4059 * ____default_0;
};
