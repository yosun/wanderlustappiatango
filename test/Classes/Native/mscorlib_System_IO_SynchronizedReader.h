﻿#pragma once
#include <stdint.h>
// System.IO.TextReader
struct TextReader_t1795;
// System.IO.TextReader
#include "mscorlib_System_IO_TextReader.h"
// System.IO.SynchronizedReader
struct SynchronizedReader_t1891  : public TextReader_t1795
{
	// System.IO.TextReader System.IO.SynchronizedReader::reader
	TextReader_t1795 * ___reader_1;
};
