﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.RenderTexture>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_124.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RenderTexture>
struct CachedInvokableCall_1_t4487  : public InvokableCall_1_t4488
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RenderTexture>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
