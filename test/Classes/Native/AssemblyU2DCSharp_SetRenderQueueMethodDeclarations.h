﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SetRenderQueue
struct SetRenderQueue_t22;

// System.Void SetRenderQueue::.ctor()
 void SetRenderQueue__ctor_m55 (SetRenderQueue_t22 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetRenderQueue::Awake()
 void SetRenderQueue_Awake_m56 (SetRenderQueue_t22 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
