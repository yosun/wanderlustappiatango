﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t514;
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.RectTransform
struct RectTransform_t287  : public Transform_t10
{
};
struct RectTransform_t287_StaticFields{
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t514 * ___reapplyDrivenProperties_2;
};
