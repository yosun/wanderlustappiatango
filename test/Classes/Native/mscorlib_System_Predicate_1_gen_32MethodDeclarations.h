﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.SmartTerrainTrackable>
struct Predicate_1_t3917;
// System.Object
struct Object_t;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t581;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<Vuforia.SmartTerrainTrackable>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m22647(__this, ___object, ___method, method) (void)Predicate_1__ctor_m14750_gshared((Predicate_1_t2845 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// System.Boolean System.Predicate`1<Vuforia.SmartTerrainTrackable>::Invoke(T)
#define Predicate_1_Invoke_m22648(__this, ___obj, method) (bool)Predicate_1_Invoke_m14751_gshared((Predicate_1_t2845 *)__this, (Object_t *)___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.SmartTerrainTrackable>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m22649(__this, ___obj, ___callback, ___object, method) (Object_t *)Predicate_1_BeginInvoke_m14752_gshared((Predicate_1_t2845 *)__this, (Object_t *)___obj, (AsyncCallback_t200 *)___callback, (Object_t *)___object, method)
// System.Boolean System.Predicate`1<Vuforia.SmartTerrainTrackable>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m22650(__this, ___result, method) (bool)Predicate_1_EndInvoke_m14753_gshared((Predicate_1_t2845 *)__this, (Object_t *)___result, method)
