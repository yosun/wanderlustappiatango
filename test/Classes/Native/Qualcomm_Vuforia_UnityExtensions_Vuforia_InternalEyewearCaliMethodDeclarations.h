﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.InternalEyewearCalibrationProfileManager
struct InternalEyewearCalibrationProfileManager_t543;
// System.String
struct String_t;
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// Vuforia.InternalEyewear/EyeID
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_InternalEyewear_Eye.h"

// System.Int32 Vuforia.InternalEyewearCalibrationProfileManager::getMaxCount()
 int32_t InternalEyewearCalibrationProfileManager_getMaxCount_m2595 (InternalEyewearCalibrationProfileManager_t543 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.InternalEyewearCalibrationProfileManager::getUsedCount()
 int32_t InternalEyewearCalibrationProfileManager_getUsedCount_m2596 (InternalEyewearCalibrationProfileManager_t543 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::isProfileUsed(System.Int32)
 bool InternalEyewearCalibrationProfileManager_isProfileUsed_m2597 (InternalEyewearCalibrationProfileManager_t543 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.InternalEyewearCalibrationProfileManager::getActiveProfile()
 int32_t InternalEyewearCalibrationProfileManager_getActiveProfile_m2598 (InternalEyewearCalibrationProfileManager_t543 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::setActiveProfile(System.Int32)
 bool InternalEyewearCalibrationProfileManager_setActiveProfile_m2599 (InternalEyewearCalibrationProfileManager_t543 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 Vuforia.InternalEyewearCalibrationProfileManager::getProjectionMatrix(System.Int32,Vuforia.InternalEyewear/EyeID)
 Matrix4x4_t176  InternalEyewearCalibrationProfileManager_getProjectionMatrix_m2600 (InternalEyewearCalibrationProfileManager_t543 * __this, int32_t ___profileID, int32_t ___eyeID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::setProjectionMatrix(System.Int32,Vuforia.InternalEyewear/EyeID,UnityEngine.Matrix4x4)
 bool InternalEyewearCalibrationProfileManager_setProjectionMatrix_m2601 (InternalEyewearCalibrationProfileManager_t543 * __this, int32_t ___profileID, int32_t ___eyeID, Matrix4x4_t176  ___projectionMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Vuforia.InternalEyewearCalibrationProfileManager::getProfileName(System.Int32)
 String_t* InternalEyewearCalibrationProfileManager_getProfileName_m2602 (InternalEyewearCalibrationProfileManager_t543 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::setProfileName(System.Int32,System.String)
 bool InternalEyewearCalibrationProfileManager_setProfileName_m2603 (InternalEyewearCalibrationProfileManager_t543 * __this, int32_t ___profileID, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.InternalEyewearCalibrationProfileManager::clearProfile(System.Int32)
 bool InternalEyewearCalibrationProfileManager_clearProfile_m2604 (InternalEyewearCalibrationProfileManager_t543 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.InternalEyewearCalibrationProfileManager::.ctor()
 void InternalEyewearCalibrationProfileManager__ctor_m2605 (InternalEyewearCalibrationProfileManager_t543 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
