﻿#pragma once
#include <stdint.h>
// UnityEngine.Animator
struct Animator_t356;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.Animator>
struct CastHelper_1_t3479 
{
	// T UnityEngine.CastHelper`1<UnityEngine.Animator>::t
	Animator_t356 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.Animator>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
