﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>
struct InternalEnumerator_1_t3434;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m18852 (InternalEnumerator_1_t3434 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18853 (InternalEnumerator_1_t3434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
 void InternalEnumerator_1_Dispose_m18854 (InternalEnumerator_1_t3434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m18855 (InternalEnumerator_1_t3434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
 UICharInfo_t489  InternalEnumerator_1_get_Current_m18856 (InternalEnumerator_1_t3434 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
