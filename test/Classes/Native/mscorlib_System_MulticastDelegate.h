﻿#pragma once
#include <stdint.h>
// System.MulticastDelegate
struct MulticastDelegate_t325;
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.MulticastDelegate
struct MulticastDelegate_t325  : public Delegate_t153
{
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t325 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t325 * ___kpm_next_10;
};
