﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDeviceMode>
struct InternalEnumerator_1_t3680;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.CameraDevice/CameraDeviceMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDeviceMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m20386 (InternalEnumerator_1_t3680 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDeviceMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m20387 (InternalEnumerator_1_t3680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDeviceMode>::Dispose()
 void InternalEnumerator_1_Dispose_m20388 (InternalEnumerator_1_t3680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDeviceMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m20389 (InternalEnumerator_1_t3680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.CameraDevice/CameraDeviceMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m20390 (InternalEnumerator_1_t3680 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
