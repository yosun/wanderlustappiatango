﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.Impl.LocalUser
struct LocalUser_t926;
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t1052;

// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::.ctor()
 void LocalUser__ctor_m6249 (LocalUser_t926 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetFriends(UnityEngine.SocialPlatforms.IUserProfile[])
 void LocalUser_SetFriends_m6250 (LocalUser_t926 * __this, IUserProfileU5BU5D_t1052* ___friends, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetAuthenticated(System.Boolean)
 void LocalUser_SetAuthenticated_m6251 (LocalUser_t926 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SocialPlatforms.Impl.LocalUser::SetUnderage(System.Boolean)
 void LocalUser_SetUnderage_m6252 (LocalUser_t926 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SocialPlatforms.Impl.LocalUser::get_authenticated()
 bool LocalUser_get_authenticated_m6253 (LocalUser_t926 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
