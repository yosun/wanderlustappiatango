﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManagerImpl/TrackableResultData
struct TrackableResultData_t639;
struct TrackableResultData_t639_marshaled;

void TrackableResultData_t639_marshal(const TrackableResultData_t639& unmarshaled, TrackableResultData_t639_marshaled& marshaled);
void TrackableResultData_t639_marshal_back(const TrackableResultData_t639_marshaled& marshaled, TrackableResultData_t639& unmarshaled);
void TrackableResultData_t639_marshal_cleanup(TrackableResultData_t639_marshaled& marshaled);
