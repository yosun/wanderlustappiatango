﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// WorldNav
struct WorldNav_t25;

// System.Void WorldNav::.ctor()
 void WorldNav__ctor_m64 (WorldNav_t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::.cctor()
 void WorldNav__cctor_m65 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::Awake()
 void WorldNav_Awake_m66 (WorldNav_t25 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::Walk(System.Int32)
 void WorldNav_Walk_m67 (Object_t * __this/* static, unused */, int32_t ___dir, MethodInfo* method) IL2CPP_METHOD_ATTR;
