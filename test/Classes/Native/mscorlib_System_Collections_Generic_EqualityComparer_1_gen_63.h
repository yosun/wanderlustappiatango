﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.GUILayoutEntry>
struct EqualityComparer_1_t4532;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.GUILayoutEntry>
struct EqualityComparer_1_t4532  : public Object_t
{
};
struct EqualityComparer_1_t4532_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.GUILayoutEntry>::_default
	EqualityComparer_1_t4532 * ____default_0;
};
