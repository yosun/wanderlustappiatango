﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t3342;
// UnityEngine.UIVertex
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
 void DefaultComparer__ctor_m18142 (DefaultComparer_t3342 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::GetHashCode(T)
 int32_t DefaultComparer_GetHashCode_m18143 (DefaultComparer_t3342 * __this, UIVertex_t315  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UIVertex>::Equals(T,T)
 bool DefaultComparer_Equals_m18144 (DefaultComparer_t3342 * __this, UIVertex_t315  ___x, UIVertex_t315  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
