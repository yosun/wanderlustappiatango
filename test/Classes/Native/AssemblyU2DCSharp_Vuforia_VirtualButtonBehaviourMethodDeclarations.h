﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VirtualButtonBehaviour
struct VirtualButtonBehaviour_t87;

// System.Void Vuforia.VirtualButtonBehaviour::.ctor()
 void VirtualButtonBehaviour__ctor_m179 (VirtualButtonBehaviour_t87 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
