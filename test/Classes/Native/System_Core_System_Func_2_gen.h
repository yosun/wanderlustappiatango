﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Toggle
struct Toggle_t370;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<UnityEngine.UI.Toggle,System.Boolean>
struct Func_2_t373  : public MulticastDelegate_t325
{
};
