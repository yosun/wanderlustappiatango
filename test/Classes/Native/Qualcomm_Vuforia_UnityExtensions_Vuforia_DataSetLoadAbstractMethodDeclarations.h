﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.DataSetLoadAbstractBehaviour
struct DataSetLoadAbstractBehaviour_t36;
// System.String
struct String_t;

// System.Void Vuforia.DataSetLoadAbstractBehaviour::LoadDatasets()
 void DataSetLoadAbstractBehaviour_LoadDatasets_m2786 (DataSetLoadAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::AddOSSpecificExternalDatasetSearchDirs()
 void DataSetLoadAbstractBehaviour_AddOSSpecificExternalDatasetSearchDirs_m2787 (DataSetLoadAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::AddExternalDatasetSearchDir(System.String)
 void DataSetLoadAbstractBehaviour_AddExternalDatasetSearchDir_m2788 (DataSetLoadAbstractBehaviour_t36 * __this, String_t* ___searchDir, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::Start()
 void DataSetLoadAbstractBehaviour_Start_m2789 (DataSetLoadAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.DataSetLoadAbstractBehaviour::.ctor()
 void DataSetLoadAbstractBehaviour__ctor_m394 (DataSetLoadAbstractBehaviour_t36 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
