﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<ARVRModes>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen.h"
// UnityEngine.Events.CachedInvokableCall`1<ARVRModes>
struct CachedInvokableCall_1_t2703  : public InvokableCall_1_t2704
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<ARVRModes>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
