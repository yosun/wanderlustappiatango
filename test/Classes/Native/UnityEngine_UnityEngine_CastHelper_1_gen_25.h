﻿#pragma once
#include <stdint.h>
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t50;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Vuforia.ImageTargetAbstractBehaviour>
struct CastHelper_1_t4420 
{
	// T UnityEngine.CastHelper`1<Vuforia.ImageTargetAbstractBehaviour>::t
	ImageTargetAbstractBehaviour_t50 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Vuforia.ImageTargetAbstractBehaviour>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
