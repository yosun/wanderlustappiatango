﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IEnumerable_1_t8628_il2cpp_TypeInfo;


// System.Array
#include "mscorlib_System_Array.h"

// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Math.BigInteger/Sign>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Math.BigInteger/Sign>
extern Il2CppType IEnumerator_1_t6791_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48940_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Math.BigInteger/Sign>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48940_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8628_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6791_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48940_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8628_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48940_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
static TypeInfo* IEnumerable_1_t8628_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8628_0_0_0;
extern Il2CppType IEnumerable_1_t8628_1_0_0;
struct IEnumerable_1_t8628;
extern Il2CppGenericClass IEnumerable_1_t8628_GenericClass;
TypeInfo IEnumerable_1_t8628_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8628_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8628_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8628_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8628_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8628_0_0_0/* byval_arg */
	, &IEnumerable_1_t8628_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8628_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8627_il2cpp_TypeInfo;

// Mono.Math.BigInteger/Sign
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Void
#include "mscorlib_System_Void.h"


// System.Int32 System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>
extern MethodInfo IList_1_get_Item_m48941_MethodInfo;
extern MethodInfo IList_1_set_Item_m48942_MethodInfo;
static PropertyInfo IList_1_t8627____Item_PropertyInfo = 
{
	&IList_1_t8627_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48941_MethodInfo/* get */
	, &IList_1_set_Item_m48942_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8627_PropertyInfos[] =
{
	&IList_1_t8627____Item_PropertyInfo,
	NULL
};
extern Il2CppType Sign_t1536_0_0_0;
extern Il2CppType Sign_t1536_0_0_0;
static ParameterInfo IList_1_t8627_IList_1_IndexOf_m48943_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Sign_t1536_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48943_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48943_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8627_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8627_IList_1_IndexOf_m48943_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48943_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Sign_t1536_0_0_0;
static ParameterInfo IList_1_t8627_IList_1_Insert_m48944_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Sign_t1536_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48944_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48944_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8627_IList_1_Insert_m48944_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48944_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8627_IList_1_RemoveAt_m48945_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48945_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48945_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8627_IList_1_RemoveAt_m48945_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48945_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8627_IList_1_get_Item_m48941_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Sign_t1536_0_0_0;
extern void* RuntimeInvoker_Sign_t1536_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48941_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48941_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8627_il2cpp_TypeInfo/* declaring_type */
	, &Sign_t1536_0_0_0/* return_type */
	, RuntimeInvoker_Sign_t1536_Int32_t93/* invoker_method */
	, IList_1_t8627_IList_1_get_Item_m48941_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48941_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Sign_t1536_0_0_0;
static ParameterInfo IList_1_t8627_IList_1_set_Item_m48942_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Sign_t1536_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48942_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.BigInteger/Sign>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48942_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8627_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8627_IList_1_set_Item_m48942_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48942_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8627_MethodInfos[] =
{
	&IList_1_IndexOf_m48943_MethodInfo,
	&IList_1_Insert_m48944_MethodInfo,
	&IList_1_RemoveAt_m48945_MethodInfo,
	&IList_1_get_Item_m48941_MethodInfo,
	&IList_1_set_Item_m48942_MethodInfo,
	NULL
};
extern TypeInfo ICollection_1_t8626_il2cpp_TypeInfo;
static TypeInfo* IList_1_t8627_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8626_il2cpp_TypeInfo,
	&IEnumerable_1_t8628_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8627_0_0_0;
extern Il2CppType IList_1_t8627_1_0_0;
struct IList_1_t8627;
extern Il2CppGenericClass IList_1_t8627_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8627_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8627_MethodInfos/* methods */
	, IList_1_t8627_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8627_il2cpp_TypeInfo/* element_class */
	, IList_1_t8627_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8627_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8627_0_0_0/* byval_arg */
	, &IList_1_t8627_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8627_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6793_il2cpp_TypeInfo;

// Mono.Math.Prime.ConfidenceFactor
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Math.Prime.ConfidenceFactor>
extern MethodInfo IEnumerator_1_get_Current_m48946_MethodInfo;
static PropertyInfo IEnumerator_1_t6793____Current_PropertyInfo = 
{
	&IEnumerator_1_t6793_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48946_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6793_PropertyInfos[] =
{
	&IEnumerator_1_t6793____Current_PropertyInfo,
	NULL
};
extern Il2CppType ConfidenceFactor_t1543_0_0_0;
extern void* RuntimeInvoker_ConfidenceFactor_t1543 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48946_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48946_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6793_il2cpp_TypeInfo/* declaring_type */
	, &ConfidenceFactor_t1543_0_0_0/* return_type */
	, RuntimeInvoker_ConfidenceFactor_t1543/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48946_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6793_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48946_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6793_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6793_0_0_0;
extern Il2CppType IEnumerator_1_t6793_1_0_0;
struct IEnumerator_1_t6793;
extern Il2CppGenericClass IEnumerator_1_t6793_GenericClass;
TypeInfo IEnumerator_1_t6793_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6793_MethodInfos/* methods */
	, IEnumerator_1_t6793_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6793_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6793_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6793_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6793_0_0_0/* byval_arg */
	, &IEnumerator_1_t6793_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6793_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_527.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4905_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_527MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo ConfidenceFactor_t1543_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1493_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m30111_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7651_MethodInfo;
extern MethodInfo Array_get_Length_m7656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisConfidenceFactor_t1543_m38649_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Math.Prime.ConfidenceFactor>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Math.Prime.ConfidenceFactor>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisConfidenceFactor_t1543_m38649 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30107_MethodInfo;
 void InternalEnumerator_1__ctor_m30107 (InternalEnumerator_1_t4905 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30108_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30108 (InternalEnumerator_1_t4905 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30111(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30111_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ConfidenceFactor_t1543_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30109_MethodInfo;
 void InternalEnumerator_1_Dispose_m30109 (InternalEnumerator_1_t4905 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30110_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30110 (InternalEnumerator_1_t4905 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30111 (InternalEnumerator_1_t4905 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisConfidenceFactor_t1543_m38649(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisConfidenceFactor_t1543_m38649_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4905____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4905_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4905, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4905____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4905_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4905, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4905_FieldInfos[] =
{
	&InternalEnumerator_1_t4905____array_0_FieldInfo,
	&InternalEnumerator_1_t4905____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4905____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4905_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30108_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4905____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4905_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30111_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4905_PropertyInfos[] =
{
	&InternalEnumerator_1_t4905____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4905____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4905_InternalEnumerator_1__ctor_m30107_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30107_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30107_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30107/* method */
	, &InternalEnumerator_1_t4905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4905_InternalEnumerator_1__ctor_m30107_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30107_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30108_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30108_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30108/* method */
	, &InternalEnumerator_1_t4905_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30108_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30109_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30109_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30109/* method */
	, &InternalEnumerator_1_t4905_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30109_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30110_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30110_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30110/* method */
	, &InternalEnumerator_1_t4905_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30110_GenericMethod/* genericMethod */

};
extern Il2CppType ConfidenceFactor_t1543_0_0_0;
extern void* RuntimeInvoker_ConfidenceFactor_t1543 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30111_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Math.Prime.ConfidenceFactor>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30111_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30111/* method */
	, &InternalEnumerator_1_t4905_il2cpp_TypeInfo/* declaring_type */
	, &ConfidenceFactor_t1543_0_0_0/* return_type */
	, RuntimeInvoker_ConfidenceFactor_t1543/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30111_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4905_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30107_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30108_MethodInfo,
	&InternalEnumerator_1_Dispose_m30109_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30110_MethodInfo,
	&InternalEnumerator_1_get_Current_m30111_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4905_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30108_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30110_MethodInfo,
	&InternalEnumerator_1_Dispose_m30109_MethodInfo,
	&InternalEnumerator_1_get_Current_m30111_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4905_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6793_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4905_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6793_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4905_0_0_0;
extern Il2CppType InternalEnumerator_1_t4905_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t4905_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t4905_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4905_MethodInfos/* methods */
	, InternalEnumerator_1_t4905_PropertyInfos/* properties */
	, InternalEnumerator_1_t4905_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4905_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4905_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4905_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4905_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4905_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4905_1_0_0/* this_arg */
	, InternalEnumerator_1_t4905_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4905_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4905)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8629_il2cpp_TypeInfo;

#include "Mono.Security_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>
extern MethodInfo ICollection_1_get_Count_m48947_MethodInfo;
static PropertyInfo ICollection_1_t8629____Count_PropertyInfo = 
{
	&ICollection_1_t8629_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48947_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48948_MethodInfo;
static PropertyInfo ICollection_1_t8629____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8629_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48948_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8629_PropertyInfos[] =
{
	&ICollection_1_t8629____Count_PropertyInfo,
	&ICollection_1_t8629____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48947_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::get_Count()
MethodInfo ICollection_1_get_Count_m48947_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8629_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48947_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48948_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48948_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8629_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48948_GenericMethod/* genericMethod */

};
extern Il2CppType ConfidenceFactor_t1543_0_0_0;
extern Il2CppType ConfidenceFactor_t1543_0_0_0;
static ParameterInfo ICollection_1_t8629_ICollection_1_Add_m48949_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactor_t1543_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48949_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Add(T)
MethodInfo ICollection_1_Add_m48949_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8629_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8629_ICollection_1_Add_m48949_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48949_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48950_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Clear()
MethodInfo ICollection_1_Clear_m48950_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8629_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48950_GenericMethod/* genericMethod */

};
extern Il2CppType ConfidenceFactor_t1543_0_0_0;
static ParameterInfo ICollection_1_t8629_ICollection_1_Contains_m48951_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactor_t1543_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48951_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Contains(T)
MethodInfo ICollection_1_Contains_m48951_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8629_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8629_ICollection_1_Contains_m48951_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48951_GenericMethod/* genericMethod */

};
extern Il2CppType ConfidenceFactorU5BU5D_t5680_0_0_0;
extern Il2CppType ConfidenceFactorU5BU5D_t5680_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8629_ICollection_1_CopyTo_m48952_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactorU5BU5D_t5680_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48952_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48952_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8629_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8629_ICollection_1_CopyTo_m48952_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48952_GenericMethod/* genericMethod */

};
extern Il2CppType ConfidenceFactor_t1543_0_0_0;
static ParameterInfo ICollection_1_t8629_ICollection_1_Remove_m48953_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactor_t1543_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48953_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Math.Prime.ConfidenceFactor>::Remove(T)
MethodInfo ICollection_1_Remove_m48953_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8629_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8629_ICollection_1_Remove_m48953_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48953_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8629_MethodInfos[] =
{
	&ICollection_1_get_Count_m48947_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48948_MethodInfo,
	&ICollection_1_Add_m48949_MethodInfo,
	&ICollection_1_Clear_m48950_MethodInfo,
	&ICollection_1_Contains_m48951_MethodInfo,
	&ICollection_1_CopyTo_m48952_MethodInfo,
	&ICollection_1_Remove_m48953_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8631_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8629_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8631_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8629_0_0_0;
extern Il2CppType ICollection_1_t8629_1_0_0;
struct ICollection_1_t8629;
extern Il2CppGenericClass ICollection_1_t8629_GenericClass;
TypeInfo ICollection_1_t8629_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8629_MethodInfos/* methods */
	, ICollection_1_t8629_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8629_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8629_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8629_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8629_0_0_0/* byval_arg */
	, &ICollection_1_t8629_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8629_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Math.Prime.ConfidenceFactor>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Math.Prime.ConfidenceFactor>
extern Il2CppType IEnumerator_1_t6793_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48954_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Math.Prime.ConfidenceFactor>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48954_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8631_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6793_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48954_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8631_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48954_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8631_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8631_0_0_0;
extern Il2CppType IEnumerable_1_t8631_1_0_0;
struct IEnumerable_1_t8631;
extern Il2CppGenericClass IEnumerable_1_t8631_GenericClass;
TypeInfo IEnumerable_1_t8631_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8631_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8631_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8631_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8631_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8631_0_0_0/* byval_arg */
	, &IEnumerable_1_t8631_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8631_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8630_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>
extern MethodInfo IList_1_get_Item_m48955_MethodInfo;
extern MethodInfo IList_1_set_Item_m48956_MethodInfo;
static PropertyInfo IList_1_t8630____Item_PropertyInfo = 
{
	&IList_1_t8630_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48955_MethodInfo/* get */
	, &IList_1_set_Item_m48956_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8630_PropertyInfos[] =
{
	&IList_1_t8630____Item_PropertyInfo,
	NULL
};
extern Il2CppType ConfidenceFactor_t1543_0_0_0;
static ParameterInfo IList_1_t8630_IList_1_IndexOf_m48957_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactor_t1543_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48957_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48957_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8630_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8630_IList_1_IndexOf_m48957_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48957_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ConfidenceFactor_t1543_0_0_0;
static ParameterInfo IList_1_t8630_IList_1_Insert_m48958_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactor_t1543_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48958_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48958_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8630_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8630_IList_1_Insert_m48958_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48958_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8630_IList_1_RemoveAt_m48959_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48959_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48959_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8630_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8630_IList_1_RemoveAt_m48959_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48959_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8630_IList_1_get_Item_m48955_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ConfidenceFactor_t1543_0_0_0;
extern void* RuntimeInvoker_ConfidenceFactor_t1543_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48955_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48955_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8630_il2cpp_TypeInfo/* declaring_type */
	, &ConfidenceFactor_t1543_0_0_0/* return_type */
	, RuntimeInvoker_ConfidenceFactor_t1543_Int32_t93/* invoker_method */
	, IList_1_t8630_IList_1_get_Item_m48955_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48955_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ConfidenceFactor_t1543_0_0_0;
static ParameterInfo IList_1_t8630_IList_1_set_Item_m48956_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ConfidenceFactor_t1543_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48956_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Math.Prime.ConfidenceFactor>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48956_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8630_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8630_IList_1_set_Item_m48956_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48956_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8630_MethodInfos[] =
{
	&IList_1_IndexOf_m48957_MethodInfo,
	&IList_1_Insert_m48958_MethodInfo,
	&IList_1_RemoveAt_m48959_MethodInfo,
	&IList_1_get_Item_m48955_MethodInfo,
	&IList_1_set_Item_m48956_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8630_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8629_il2cpp_TypeInfo,
	&IEnumerable_1_t8631_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8630_0_0_0;
extern Il2CppType IList_1_t8630_1_0_0;
struct IList_1_t8630;
extern Il2CppGenericClass IList_1_t8630_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8630_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8630_MethodInfos/* methods */
	, IList_1_t8630_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8630_il2cpp_TypeInfo/* element_class */
	, IList_1_t8630_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8630_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8630_0_0_0/* byval_arg */
	, &IList_1_t8630_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8630_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6795_il2cpp_TypeInfo;

// System.Security.Cryptography.KeySizes
#include "mscorlib_System_Security_Cryptography_KeySizes.h"


// T System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.KeySizes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.KeySizes>
extern MethodInfo IEnumerator_1_get_Current_m48960_MethodInfo;
static PropertyInfo IEnumerator_1_t6795____Current_PropertyInfo = 
{
	&IEnumerator_1_t6795_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48960_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6795_PropertyInfos[] =
{
	&IEnumerator_1_t6795____Current_PropertyInfo,
	NULL
};
extern Il2CppType KeySizes_t1664_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48960_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Security.Cryptography.KeySizes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48960_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6795_il2cpp_TypeInfo/* declaring_type */
	, &KeySizes_t1664_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48960_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6795_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48960_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6795_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6795_0_0_0;
extern Il2CppType IEnumerator_1_t6795_1_0_0;
struct IEnumerator_1_t6795;
extern Il2CppGenericClass IEnumerator_1_t6795_GenericClass;
TypeInfo IEnumerator_1_t6795_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6795_MethodInfos/* methods */
	, IEnumerator_1_t6795_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6795_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6795_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6795_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6795_0_0_0/* byval_arg */
	, &IEnumerator_1_t6795_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6795_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_528.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4906_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_528MethodDeclarations.h"

extern TypeInfo KeySizes_t1664_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30116_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeySizes_t1664_m38660_MethodInfo;
struct Array_t;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m31436_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m31436(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<System.Security.Cryptography.KeySizes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Security.Cryptography.KeySizes>(System.Int32)
#define Array_InternalArray__get_Item_TisKeySizes_t1664_m38660(__this, p0, method) (KeySizes_t1664 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4906____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4906_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4906, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4906____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4906_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4906, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4906_FieldInfos[] =
{
	&InternalEnumerator_1_t4906____array_0_FieldInfo,
	&InternalEnumerator_1_t4906____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30113_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4906____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4906_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30113_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4906____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4906_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30116_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4906_PropertyInfos[] =
{
	&InternalEnumerator_1_t4906____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4906____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4906_InternalEnumerator_1__ctor_m30112_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30112_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30112_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4906_InternalEnumerator_1__ctor_m30112_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30112_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30113_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30113_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4906_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30113_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30114_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30114_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4906_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30114_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30115_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30115_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4906_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30115_GenericMethod/* genericMethod */

};
extern Il2CppType KeySizes_t1664_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30116_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.KeySizes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30116_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4906_il2cpp_TypeInfo/* declaring_type */
	, &KeySizes_t1664_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30116_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4906_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30112_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30113_MethodInfo,
	&InternalEnumerator_1_Dispose_m30114_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30115_MethodInfo,
	&InternalEnumerator_1_get_Current_m30116_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30115_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30114_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4906_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30113_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30115_MethodInfo,
	&InternalEnumerator_1_Dispose_m30114_MethodInfo,
	&InternalEnumerator_1_get_Current_m30116_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4906_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6795_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4906_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6795_il2cpp_TypeInfo, 7},
};
extern TypeInfo KeySizes_t1664_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4906_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30116_MethodInfo/* Method Usage */,
	&KeySizes_t1664_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisKeySizes_t1664_m38660_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4906_0_0_0;
extern Il2CppType InternalEnumerator_1_t4906_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4906_GenericClass;
TypeInfo InternalEnumerator_1_t4906_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4906_MethodInfos/* methods */
	, InternalEnumerator_1_t4906_PropertyInfos/* properties */
	, InternalEnumerator_1_t4906_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4906_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4906_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4906_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4906_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4906_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4906_1_0_0/* this_arg */
	, InternalEnumerator_1_t4906_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4906_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4906_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4906)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8632_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>
extern MethodInfo ICollection_1_get_Count_m48961_MethodInfo;
static PropertyInfo ICollection_1_t8632____Count_PropertyInfo = 
{
	&ICollection_1_t8632_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48961_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48962_MethodInfo;
static PropertyInfo ICollection_1_t8632____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8632_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48962_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8632_PropertyInfos[] =
{
	&ICollection_1_t8632____Count_PropertyInfo,
	&ICollection_1_t8632____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48961_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::get_Count()
MethodInfo ICollection_1_get_Count_m48961_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8632_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48961_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48962_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48962_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8632_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48962_GenericMethod/* genericMethod */

};
extern Il2CppType KeySizes_t1664_0_0_0;
extern Il2CppType KeySizes_t1664_0_0_0;
static ParameterInfo ICollection_1_t8632_ICollection_1_Add_m48963_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeySizes_t1664_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48963_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Add(T)
MethodInfo ICollection_1_Add_m48963_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8632_ICollection_1_Add_m48963_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48963_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48964_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Clear()
MethodInfo ICollection_1_Clear_m48964_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48964_GenericMethod/* genericMethod */

};
extern Il2CppType KeySizes_t1664_0_0_0;
static ParameterInfo ICollection_1_t8632_ICollection_1_Contains_m48965_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeySizes_t1664_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48965_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Contains(T)
MethodInfo ICollection_1_Contains_m48965_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8632_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8632_ICollection_1_Contains_m48965_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48965_GenericMethod/* genericMethod */

};
extern Il2CppType KeySizesU5BU5D_t1565_0_0_0;
extern Il2CppType KeySizesU5BU5D_t1565_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8632_ICollection_1_CopyTo_m48966_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &KeySizesU5BU5D_t1565_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48966_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48966_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8632_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8632_ICollection_1_CopyTo_m48966_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48966_GenericMethod/* genericMethod */

};
extern Il2CppType KeySizes_t1664_0_0_0;
static ParameterInfo ICollection_1_t8632_ICollection_1_Remove_m48967_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeySizes_t1664_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48967_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Security.Cryptography.KeySizes>::Remove(T)
MethodInfo ICollection_1_Remove_m48967_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8632_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8632_ICollection_1_Remove_m48967_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48967_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8632_MethodInfos[] =
{
	&ICollection_1_get_Count_m48961_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48962_MethodInfo,
	&ICollection_1_Add_m48963_MethodInfo,
	&ICollection_1_Clear_m48964_MethodInfo,
	&ICollection_1_Contains_m48965_MethodInfo,
	&ICollection_1_CopyTo_m48966_MethodInfo,
	&ICollection_1_Remove_m48967_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8634_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8632_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8634_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8632_0_0_0;
extern Il2CppType ICollection_1_t8632_1_0_0;
struct ICollection_1_t8632;
extern Il2CppGenericClass ICollection_1_t8632_GenericClass;
TypeInfo ICollection_1_t8632_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8632_MethodInfos/* methods */
	, ICollection_1_t8632_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8632_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8632_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8632_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8632_0_0_0/* byval_arg */
	, &ICollection_1_t8632_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8632_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.KeySizes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.KeySizes>
extern Il2CppType IEnumerator_1_t6795_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48968_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Security.Cryptography.KeySizes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48968_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8634_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6795_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48968_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8634_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48968_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8634_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8634_0_0_0;
extern Il2CppType IEnumerable_1_t8634_1_0_0;
struct IEnumerable_1_t8634;
extern Il2CppGenericClass IEnumerable_1_t8634_GenericClass;
TypeInfo IEnumerable_1_t8634_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8634_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8634_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8634_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8634_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8634_0_0_0/* byval_arg */
	, &IEnumerable_1_t8634_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8634_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8633_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>
extern MethodInfo IList_1_get_Item_m48969_MethodInfo;
extern MethodInfo IList_1_set_Item_m48970_MethodInfo;
static PropertyInfo IList_1_t8633____Item_PropertyInfo = 
{
	&IList_1_t8633_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48969_MethodInfo/* get */
	, &IList_1_set_Item_m48970_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8633_PropertyInfos[] =
{
	&IList_1_t8633____Item_PropertyInfo,
	NULL
};
extern Il2CppType KeySizes_t1664_0_0_0;
static ParameterInfo IList_1_t8633_IList_1_IndexOf_m48971_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeySizes_t1664_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48971_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48971_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8633_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8633_IList_1_IndexOf_m48971_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48971_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType KeySizes_t1664_0_0_0;
static ParameterInfo IList_1_t8633_IList_1_Insert_m48972_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &KeySizes_t1664_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48972_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48972_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8633_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8633_IList_1_Insert_m48972_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48972_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8633_IList_1_RemoveAt_m48973_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48973_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48973_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8633_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8633_IList_1_RemoveAt_m48973_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48973_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8633_IList_1_get_Item_m48969_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType KeySizes_t1664_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48969_GenericMethod;
// T System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48969_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8633_il2cpp_TypeInfo/* declaring_type */
	, &KeySizes_t1664_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8633_IList_1_get_Item_m48969_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48969_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType KeySizes_t1664_0_0_0;
static ParameterInfo IList_1_t8633_IList_1_set_Item_m48970_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &KeySizes_t1664_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48970_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Security.Cryptography.KeySizes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48970_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8633_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8633_IList_1_set_Item_m48970_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48970_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8633_MethodInfos[] =
{
	&IList_1_IndexOf_m48971_MethodInfo,
	&IList_1_Insert_m48972_MethodInfo,
	&IList_1_RemoveAt_m48973_MethodInfo,
	&IList_1_get_Item_m48969_MethodInfo,
	&IList_1_set_Item_m48970_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8633_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8632_il2cpp_TypeInfo,
	&IEnumerable_1_t8634_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8633_0_0_0;
extern Il2CppType IList_1_t8633_1_0_0;
struct IList_1_t8633;
extern Il2CppGenericClass IList_1_t8633_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8633_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8633_MethodInfos/* methods */
	, IList_1_t8633_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8633_il2cpp_TypeInfo/* element_class */
	, IList_1_t8633_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8633_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8633_0_0_0/* byval_arg */
	, &IList_1_t8633_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8633_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6797_il2cpp_TypeInfo;

// Mono.Security.X509.X509ChainStatusFlags
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>
extern MethodInfo IEnumerator_1_get_Current_m48974_MethodInfo;
static PropertyInfo IEnumerator_1_t6797____Current_PropertyInfo = 
{
	&IEnumerator_1_t6797_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48974_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6797_PropertyInfos[] =
{
	&IEnumerator_1_t6797____Current_PropertyInfo,
	NULL
};
extern Il2CppType X509ChainStatusFlags_t1572_0_0_0;
extern void* RuntimeInvoker_X509ChainStatusFlags_t1572 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48974_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48974_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6797_il2cpp_TypeInfo/* declaring_type */
	, &X509ChainStatusFlags_t1572_0_0_0/* return_type */
	, RuntimeInvoker_X509ChainStatusFlags_t1572/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48974_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6797_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48974_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6797_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6797_0_0_0;
extern Il2CppType IEnumerator_1_t6797_1_0_0;
struct IEnumerator_1_t6797;
extern Il2CppGenericClass IEnumerator_1_t6797_GenericClass;
TypeInfo IEnumerator_1_t6797_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6797_MethodInfos/* methods */
	, IEnumerator_1_t6797_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6797_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6797_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6797_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6797_0_0_0/* byval_arg */
	, &IEnumerator_1_t6797_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6797_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_529.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4907_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_529MethodDeclarations.h"

extern TypeInfo X509ChainStatusFlags_t1572_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30121_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisX509ChainStatusFlags_t1572_m38671_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.X509.X509ChainStatusFlags>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.X509.X509ChainStatusFlags>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisX509ChainStatusFlags_t1572_m38671 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30117_MethodInfo;
 void InternalEnumerator_1__ctor_m30117 (InternalEnumerator_1_t4907 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30118_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30118 (InternalEnumerator_1_t4907 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30121(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30121_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&X509ChainStatusFlags_t1572_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30119_MethodInfo;
 void InternalEnumerator_1_Dispose_m30119 (InternalEnumerator_1_t4907 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30120_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30120 (InternalEnumerator_1_t4907 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30121 (InternalEnumerator_1_t4907 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisX509ChainStatusFlags_t1572_m38671(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisX509ChainStatusFlags_t1572_m38671_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4907____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4907_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4907, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4907____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4907_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4907, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4907_FieldInfos[] =
{
	&InternalEnumerator_1_t4907____array_0_FieldInfo,
	&InternalEnumerator_1_t4907____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4907____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4907_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30118_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4907____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4907_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30121_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4907_PropertyInfos[] =
{
	&InternalEnumerator_1_t4907____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4907____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4907_InternalEnumerator_1__ctor_m30117_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30117_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30117_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30117/* method */
	, &InternalEnumerator_1_t4907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4907_InternalEnumerator_1__ctor_m30117_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30117_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30118_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30118_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30118/* method */
	, &InternalEnumerator_1_t4907_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30118_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30119_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30119_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30119/* method */
	, &InternalEnumerator_1_t4907_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30119_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30120_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30120_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30120/* method */
	, &InternalEnumerator_1_t4907_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30120_GenericMethod/* genericMethod */

};
extern Il2CppType X509ChainStatusFlags_t1572_0_0_0;
extern void* RuntimeInvoker_X509ChainStatusFlags_t1572 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30121_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.X509.X509ChainStatusFlags>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30121_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30121/* method */
	, &InternalEnumerator_1_t4907_il2cpp_TypeInfo/* declaring_type */
	, &X509ChainStatusFlags_t1572_0_0_0/* return_type */
	, RuntimeInvoker_X509ChainStatusFlags_t1572/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30121_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4907_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30117_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30118_MethodInfo,
	&InternalEnumerator_1_Dispose_m30119_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30120_MethodInfo,
	&InternalEnumerator_1_get_Current_m30121_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4907_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30118_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30120_MethodInfo,
	&InternalEnumerator_1_Dispose_m30119_MethodInfo,
	&InternalEnumerator_1_get_Current_m30121_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4907_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6797_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4907_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6797_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4907_0_0_0;
extern Il2CppType InternalEnumerator_1_t4907_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4907_GenericClass;
TypeInfo InternalEnumerator_1_t4907_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4907_MethodInfos/* methods */
	, InternalEnumerator_1_t4907_PropertyInfos/* properties */
	, InternalEnumerator_1_t4907_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4907_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4907_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4907_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4907_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4907_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4907_1_0_0/* this_arg */
	, InternalEnumerator_1_t4907_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4907_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4907)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8635_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>
extern MethodInfo ICollection_1_get_Count_m48975_MethodInfo;
static PropertyInfo ICollection_1_t8635____Count_PropertyInfo = 
{
	&ICollection_1_t8635_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48975_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48976_MethodInfo;
static PropertyInfo ICollection_1_t8635____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8635_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48976_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8635_PropertyInfos[] =
{
	&ICollection_1_t8635____Count_PropertyInfo,
	&ICollection_1_t8635____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48975_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::get_Count()
MethodInfo ICollection_1_get_Count_m48975_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8635_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48975_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48976_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48976_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8635_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48976_GenericMethod/* genericMethod */

};
extern Il2CppType X509ChainStatusFlags_t1572_0_0_0;
extern Il2CppType X509ChainStatusFlags_t1572_0_0_0;
static ParameterInfo ICollection_1_t8635_ICollection_1_Add_m48977_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlags_t1572_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48977_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Add(T)
MethodInfo ICollection_1_Add_m48977_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8635_ICollection_1_Add_m48977_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48977_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48978_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Clear()
MethodInfo ICollection_1_Clear_m48978_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48978_GenericMethod/* genericMethod */

};
extern Il2CppType X509ChainStatusFlags_t1572_0_0_0;
static ParameterInfo ICollection_1_t8635_ICollection_1_Contains_m48979_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlags_t1572_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48979_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Contains(T)
MethodInfo ICollection_1_Contains_m48979_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8635_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8635_ICollection_1_Contains_m48979_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48979_GenericMethod/* genericMethod */

};
extern Il2CppType X509ChainStatusFlagsU5BU5D_t5681_0_0_0;
extern Il2CppType X509ChainStatusFlagsU5BU5D_t5681_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8635_ICollection_1_CopyTo_m48980_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlagsU5BU5D_t5681_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48980_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48980_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8635_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8635_ICollection_1_CopyTo_m48980_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48980_GenericMethod/* genericMethod */

};
extern Il2CppType X509ChainStatusFlags_t1572_0_0_0;
static ParameterInfo ICollection_1_t8635_ICollection_1_Remove_m48981_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlags_t1572_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48981_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.X509ChainStatusFlags>::Remove(T)
MethodInfo ICollection_1_Remove_m48981_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8635_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8635_ICollection_1_Remove_m48981_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48981_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8635_MethodInfos[] =
{
	&ICollection_1_get_Count_m48975_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48976_MethodInfo,
	&ICollection_1_Add_m48977_MethodInfo,
	&ICollection_1_Clear_m48978_MethodInfo,
	&ICollection_1_Contains_m48979_MethodInfo,
	&ICollection_1_CopyTo_m48980_MethodInfo,
	&ICollection_1_Remove_m48981_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8637_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8635_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8637_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8635_0_0_0;
extern Il2CppType ICollection_1_t8635_1_0_0;
struct ICollection_1_t8635;
extern Il2CppGenericClass ICollection_1_t8635_GenericClass;
TypeInfo ICollection_1_t8635_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8635_MethodInfos/* methods */
	, ICollection_1_t8635_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8635_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8635_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8635_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8635_0_0_0/* byval_arg */
	, &ICollection_1_t8635_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8635_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.X509.X509ChainStatusFlags>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.X509.X509ChainStatusFlags>
extern Il2CppType IEnumerator_1_t6797_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48982_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.X509.X509ChainStatusFlags>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48982_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8637_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6797_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48982_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8637_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48982_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8637_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8637_0_0_0;
extern Il2CppType IEnumerable_1_t8637_1_0_0;
struct IEnumerable_1_t8637;
extern Il2CppGenericClass IEnumerable_1_t8637_GenericClass;
TypeInfo IEnumerable_1_t8637_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8637_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8637_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8637_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8637_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8637_0_0_0/* byval_arg */
	, &IEnumerable_1_t8637_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8637_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8636_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>
extern MethodInfo IList_1_get_Item_m48983_MethodInfo;
extern MethodInfo IList_1_set_Item_m48984_MethodInfo;
static PropertyInfo IList_1_t8636____Item_PropertyInfo = 
{
	&IList_1_t8636_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48983_MethodInfo/* get */
	, &IList_1_set_Item_m48984_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8636_PropertyInfos[] =
{
	&IList_1_t8636____Item_PropertyInfo,
	NULL
};
extern Il2CppType X509ChainStatusFlags_t1572_0_0_0;
static ParameterInfo IList_1_t8636_IList_1_IndexOf_m48985_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlags_t1572_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48985_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48985_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8636_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8636_IList_1_IndexOf_m48985_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48985_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType X509ChainStatusFlags_t1572_0_0_0;
static ParameterInfo IList_1_t8636_IList_1_Insert_m48986_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlags_t1572_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48986_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48986_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8636_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8636_IList_1_Insert_m48986_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48986_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8636_IList_1_RemoveAt_m48987_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48987_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48987_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8636_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8636_IList_1_RemoveAt_m48987_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48987_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8636_IList_1_get_Item_m48983_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType X509ChainStatusFlags_t1572_0_0_0;
extern void* RuntimeInvoker_X509ChainStatusFlags_t1572_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48983_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48983_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8636_il2cpp_TypeInfo/* declaring_type */
	, &X509ChainStatusFlags_t1572_0_0_0/* return_type */
	, RuntimeInvoker_X509ChainStatusFlags_t1572_Int32_t93/* invoker_method */
	, IList_1_t8636_IList_1_get_Item_m48983_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48983_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType X509ChainStatusFlags_t1572_0_0_0;
static ParameterInfo IList_1_t8636_IList_1_set_Item_m48984_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &X509ChainStatusFlags_t1572_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48984_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.X509ChainStatusFlags>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48984_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8636_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8636_IList_1_set_Item_m48984_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48984_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8636_MethodInfos[] =
{
	&IList_1_IndexOf_m48985_MethodInfo,
	&IList_1_Insert_m48986_MethodInfo,
	&IList_1_RemoveAt_m48987_MethodInfo,
	&IList_1_get_Item_m48983_MethodInfo,
	&IList_1_set_Item_m48984_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8636_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8635_il2cpp_TypeInfo,
	&IEnumerable_1_t8637_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8636_0_0_0;
extern Il2CppType IList_1_t8636_1_0_0;
struct IList_1_t8636;
extern Il2CppGenericClass IList_1_t8636_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8636_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8636_MethodInfos/* methods */
	, IList_1_t8636_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8636_il2cpp_TypeInfo/* element_class */
	, IList_1_t8636_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8636_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8636_0_0_0/* byval_arg */
	, &IList_1_t8636_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8636_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6799_il2cpp_TypeInfo;

// Mono.Security.X509.Extensions.KeyUsages
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsages.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>
extern MethodInfo IEnumerator_1_get_Current_m48988_MethodInfo;
static PropertyInfo IEnumerator_1_t6799____Current_PropertyInfo = 
{
	&IEnumerator_1_t6799_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48988_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6799_PropertyInfos[] =
{
	&IEnumerator_1_t6799____Current_PropertyInfo,
	NULL
};
extern Il2CppType KeyUsages_t1577_0_0_0;
extern void* RuntimeInvoker_KeyUsages_t1577 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48988_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48988_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6799_il2cpp_TypeInfo/* declaring_type */
	, &KeyUsages_t1577_0_0_0/* return_type */
	, RuntimeInvoker_KeyUsages_t1577/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48988_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6799_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48988_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6799_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6799_0_0_0;
extern Il2CppType IEnumerator_1_t6799_1_0_0;
struct IEnumerator_1_t6799;
extern Il2CppGenericClass IEnumerator_1_t6799_GenericClass;
TypeInfo IEnumerator_1_t6799_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6799_MethodInfos/* methods */
	, IEnumerator_1_t6799_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6799_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6799_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6799_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6799_0_0_0/* byval_arg */
	, &IEnumerator_1_t6799_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6799_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_530.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4908_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_530MethodDeclarations.h"

extern TypeInfo KeyUsages_t1577_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30126_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisKeyUsages_t1577_m38682_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.X509.Extensions.KeyUsages>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.X509.Extensions.KeyUsages>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisKeyUsages_t1577_m38682 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30122_MethodInfo;
 void InternalEnumerator_1__ctor_m30122 (InternalEnumerator_1_t4908 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30123_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30123 (InternalEnumerator_1_t4908 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30126(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30126_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&KeyUsages_t1577_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30124_MethodInfo;
 void InternalEnumerator_1_Dispose_m30124 (InternalEnumerator_1_t4908 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30125_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30125 (InternalEnumerator_1_t4908 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30126 (InternalEnumerator_1_t4908 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisKeyUsages_t1577_m38682(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisKeyUsages_t1577_m38682_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4908____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4908_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4908, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4908____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4908_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4908, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4908_FieldInfos[] =
{
	&InternalEnumerator_1_t4908____array_0_FieldInfo,
	&InternalEnumerator_1_t4908____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4908____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4908_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30123_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4908____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4908_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30126_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4908_PropertyInfos[] =
{
	&InternalEnumerator_1_t4908____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4908____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4908_InternalEnumerator_1__ctor_m30122_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30122_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30122_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30122/* method */
	, &InternalEnumerator_1_t4908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4908_InternalEnumerator_1__ctor_m30122_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30122_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30123_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30123_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30123/* method */
	, &InternalEnumerator_1_t4908_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30123_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30124_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30124_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30124/* method */
	, &InternalEnumerator_1_t4908_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30124_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30125_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30125_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30125/* method */
	, &InternalEnumerator_1_t4908_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30125_GenericMethod/* genericMethod */

};
extern Il2CppType KeyUsages_t1577_0_0_0;
extern void* RuntimeInvoker_KeyUsages_t1577 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30126_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.KeyUsages>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30126_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30126/* method */
	, &InternalEnumerator_1_t4908_il2cpp_TypeInfo/* declaring_type */
	, &KeyUsages_t1577_0_0_0/* return_type */
	, RuntimeInvoker_KeyUsages_t1577/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30126_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4908_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30122_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30123_MethodInfo,
	&InternalEnumerator_1_Dispose_m30124_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30125_MethodInfo,
	&InternalEnumerator_1_get_Current_m30126_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4908_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30123_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30125_MethodInfo,
	&InternalEnumerator_1_Dispose_m30124_MethodInfo,
	&InternalEnumerator_1_get_Current_m30126_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4908_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6799_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4908_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6799_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4908_0_0_0;
extern Il2CppType InternalEnumerator_1_t4908_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4908_GenericClass;
TypeInfo InternalEnumerator_1_t4908_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4908_MethodInfos/* methods */
	, InternalEnumerator_1_t4908_PropertyInfos/* properties */
	, InternalEnumerator_1_t4908_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4908_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4908_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4908_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4908_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4908_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4908_1_0_0/* this_arg */
	, InternalEnumerator_1_t4908_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4908_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4908)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8638_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>
extern MethodInfo ICollection_1_get_Count_m48989_MethodInfo;
static PropertyInfo ICollection_1_t8638____Count_PropertyInfo = 
{
	&ICollection_1_t8638_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48989_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48990_MethodInfo;
static PropertyInfo ICollection_1_t8638____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8638_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48990_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8638_PropertyInfos[] =
{
	&ICollection_1_t8638____Count_PropertyInfo,
	&ICollection_1_t8638____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48989_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::get_Count()
MethodInfo ICollection_1_get_Count_m48989_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8638_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48989_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48990_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48990_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8638_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48990_GenericMethod/* genericMethod */

};
extern Il2CppType KeyUsages_t1577_0_0_0;
extern Il2CppType KeyUsages_t1577_0_0_0;
static ParameterInfo ICollection_1_t8638_ICollection_1_Add_m48991_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeyUsages_t1577_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48991_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Add(T)
MethodInfo ICollection_1_Add_m48991_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8638_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8638_ICollection_1_Add_m48991_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48991_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48992_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Clear()
MethodInfo ICollection_1_Clear_m48992_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8638_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48992_GenericMethod/* genericMethod */

};
extern Il2CppType KeyUsages_t1577_0_0_0;
static ParameterInfo ICollection_1_t8638_ICollection_1_Contains_m48993_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeyUsages_t1577_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48993_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Contains(T)
MethodInfo ICollection_1_Contains_m48993_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8638_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8638_ICollection_1_Contains_m48993_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48993_GenericMethod/* genericMethod */

};
extern Il2CppType KeyUsagesU5BU5D_t5682_0_0_0;
extern Il2CppType KeyUsagesU5BU5D_t5682_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8638_ICollection_1_CopyTo_m48994_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &KeyUsagesU5BU5D_t5682_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48994_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48994_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8638_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8638_ICollection_1_CopyTo_m48994_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48994_GenericMethod/* genericMethod */

};
extern Il2CppType KeyUsages_t1577_0_0_0;
static ParameterInfo ICollection_1_t8638_ICollection_1_Remove_m48995_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeyUsages_t1577_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48995_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.KeyUsages>::Remove(T)
MethodInfo ICollection_1_Remove_m48995_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8638_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8638_ICollection_1_Remove_m48995_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48995_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8638_MethodInfos[] =
{
	&ICollection_1_get_Count_m48989_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48990_MethodInfo,
	&ICollection_1_Add_m48991_MethodInfo,
	&ICollection_1_Clear_m48992_MethodInfo,
	&ICollection_1_Contains_m48993_MethodInfo,
	&ICollection_1_CopyTo_m48994_MethodInfo,
	&ICollection_1_Remove_m48995_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8640_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8638_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8640_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8638_0_0_0;
extern Il2CppType ICollection_1_t8638_1_0_0;
struct ICollection_1_t8638;
extern Il2CppGenericClass ICollection_1_t8638_GenericClass;
TypeInfo ICollection_1_t8638_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8638_MethodInfos/* methods */
	, ICollection_1_t8638_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8638_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8638_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8638_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8638_0_0_0/* byval_arg */
	, &ICollection_1_t8638_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8638_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.KeyUsages>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.KeyUsages>
extern Il2CppType IEnumerator_1_t6799_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48996_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.KeyUsages>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48996_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8640_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6799_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48996_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8640_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48996_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8640_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8640_0_0_0;
extern Il2CppType IEnumerable_1_t8640_1_0_0;
struct IEnumerable_1_t8640;
extern Il2CppGenericClass IEnumerable_1_t8640_GenericClass;
TypeInfo IEnumerable_1_t8640_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8640_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8640_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8640_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8640_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8640_0_0_0/* byval_arg */
	, &IEnumerable_1_t8640_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8640_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8639_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>
extern MethodInfo IList_1_get_Item_m48997_MethodInfo;
extern MethodInfo IList_1_set_Item_m48998_MethodInfo;
static PropertyInfo IList_1_t8639____Item_PropertyInfo = 
{
	&IList_1_t8639_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48997_MethodInfo/* get */
	, &IList_1_set_Item_m48998_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8639_PropertyInfos[] =
{
	&IList_1_t8639____Item_PropertyInfo,
	NULL
};
extern Il2CppType KeyUsages_t1577_0_0_0;
static ParameterInfo IList_1_t8639_IList_1_IndexOf_m48999_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &KeyUsages_t1577_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48999_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48999_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8639_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8639_IList_1_IndexOf_m48999_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48999_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType KeyUsages_t1577_0_0_0;
static ParameterInfo IList_1_t8639_IList_1_Insert_m49000_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &KeyUsages_t1577_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49000_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49000_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8639_IList_1_Insert_m49000_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49000_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8639_IList_1_RemoveAt_m49001_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49001_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49001_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8639_IList_1_RemoveAt_m49001_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49001_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8639_IList_1_get_Item_m48997_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType KeyUsages_t1577_0_0_0;
extern void* RuntimeInvoker_KeyUsages_t1577_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48997_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48997_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8639_il2cpp_TypeInfo/* declaring_type */
	, &KeyUsages_t1577_0_0_0/* return_type */
	, RuntimeInvoker_KeyUsages_t1577_Int32_t93/* invoker_method */
	, IList_1_t8639_IList_1_get_Item_m48997_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48997_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType KeyUsages_t1577_0_0_0;
static ParameterInfo IList_1_t8639_IList_1_set_Item_m48998_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &KeyUsages_t1577_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48998_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.KeyUsages>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48998_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8639_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8639_IList_1_set_Item_m48998_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48998_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8639_MethodInfos[] =
{
	&IList_1_IndexOf_m48999_MethodInfo,
	&IList_1_Insert_m49000_MethodInfo,
	&IList_1_RemoveAt_m49001_MethodInfo,
	&IList_1_get_Item_m48997_MethodInfo,
	&IList_1_set_Item_m48998_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8639_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8638_il2cpp_TypeInfo,
	&IEnumerable_1_t8640_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8639_0_0_0;
extern Il2CppType IList_1_t8639_1_0_0;
struct IList_1_t8639;
extern Il2CppGenericClass IList_1_t8639_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8639_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8639_MethodInfos/* methods */
	, IList_1_t8639_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8639_il2cpp_TypeInfo/* element_class */
	, IList_1_t8639_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8639_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8639_0_0_0/* byval_arg */
	, &IList_1_t8639_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8639_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6801_il2cpp_TypeInfo;

// Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes
#include "Mono_Security_Mono_Security_X509_Extensions_NetscapeCertType.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern MethodInfo IEnumerator_1_get_Current_m49002_MethodInfo;
static PropertyInfo IEnumerator_1_t6801____Current_PropertyInfo = 
{
	&IEnumerator_1_t6801_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49002_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6801_PropertyInfos[] =
{
	&IEnumerator_1_t6801____Current_PropertyInfo,
	NULL
};
extern Il2CppType CertTypes_t1579_0_0_0;
extern void* RuntimeInvoker_CertTypes_t1579 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49002_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49002_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6801_il2cpp_TypeInfo/* declaring_type */
	, &CertTypes_t1579_0_0_0/* return_type */
	, RuntimeInvoker_CertTypes_t1579/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49002_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6801_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49002_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6801_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6801_0_0_0;
extern Il2CppType IEnumerator_1_t6801_1_0_0;
struct IEnumerator_1_t6801;
extern Il2CppGenericClass IEnumerator_1_t6801_GenericClass;
TypeInfo IEnumerator_1_t6801_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6801_MethodInfos/* methods */
	, IEnumerator_1_t6801_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6801_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6801_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6801_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6801_0_0_0/* byval_arg */
	, &IEnumerator_1_t6801_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6801_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_531.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4909_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_531MethodDeclarations.h"

extern TypeInfo CertTypes_t1579_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30131_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCertTypes_t1579_m38693_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisCertTypes_t1579_m38693 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30127_MethodInfo;
 void InternalEnumerator_1__ctor_m30127 (InternalEnumerator_1_t4909 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30128_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30128 (InternalEnumerator_1_t4909 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30131(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30131_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&CertTypes_t1579_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30129_MethodInfo;
 void InternalEnumerator_1_Dispose_m30129 (InternalEnumerator_1_t4909 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30130_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30130 (InternalEnumerator_1_t4909 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30131 (InternalEnumerator_1_t4909 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisCertTypes_t1579_m38693(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisCertTypes_t1579_m38693_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4909____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4909_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4909, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4909____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4909_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4909, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4909_FieldInfos[] =
{
	&InternalEnumerator_1_t4909____array_0_FieldInfo,
	&InternalEnumerator_1_t4909____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4909____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4909_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30128_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4909____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4909_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30131_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4909_PropertyInfos[] =
{
	&InternalEnumerator_1_t4909____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4909____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4909_InternalEnumerator_1__ctor_m30127_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30127_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30127_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30127/* method */
	, &InternalEnumerator_1_t4909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4909_InternalEnumerator_1__ctor_m30127_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30127_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30128_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30128_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30128/* method */
	, &InternalEnumerator_1_t4909_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30128_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30129_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30129_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30129/* method */
	, &InternalEnumerator_1_t4909_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30129_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30130_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30130_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30130/* method */
	, &InternalEnumerator_1_t4909_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30130_GenericMethod/* genericMethod */

};
extern Il2CppType CertTypes_t1579_0_0_0;
extern void* RuntimeInvoker_CertTypes_t1579 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30131_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30131_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30131/* method */
	, &InternalEnumerator_1_t4909_il2cpp_TypeInfo/* declaring_type */
	, &CertTypes_t1579_0_0_0/* return_type */
	, RuntimeInvoker_CertTypes_t1579/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30131_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4909_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30127_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30128_MethodInfo,
	&InternalEnumerator_1_Dispose_m30129_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30130_MethodInfo,
	&InternalEnumerator_1_get_Current_m30131_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4909_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30128_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30130_MethodInfo,
	&InternalEnumerator_1_Dispose_m30129_MethodInfo,
	&InternalEnumerator_1_get_Current_m30131_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4909_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6801_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4909_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6801_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4909_0_0_0;
extern Il2CppType InternalEnumerator_1_t4909_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4909_GenericClass;
TypeInfo InternalEnumerator_1_t4909_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4909_MethodInfos/* methods */
	, InternalEnumerator_1_t4909_PropertyInfos/* properties */
	, InternalEnumerator_1_t4909_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4909_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4909_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4909_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4909_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4909_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4909_1_0_0/* this_arg */
	, InternalEnumerator_1_t4909_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4909_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4909)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8641_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern MethodInfo ICollection_1_get_Count_m49003_MethodInfo;
static PropertyInfo ICollection_1_t8641____Count_PropertyInfo = 
{
	&ICollection_1_t8641_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49003_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49004_MethodInfo;
static PropertyInfo ICollection_1_t8641____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8641_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49004_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8641_PropertyInfos[] =
{
	&ICollection_1_t8641____Count_PropertyInfo,
	&ICollection_1_t8641____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49003_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Count()
MethodInfo ICollection_1_get_Count_m49003_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8641_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49003_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49004_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49004_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8641_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49004_GenericMethod/* genericMethod */

};
extern Il2CppType CertTypes_t1579_0_0_0;
extern Il2CppType CertTypes_t1579_0_0_0;
static ParameterInfo ICollection_1_t8641_ICollection_1_Add_m49005_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CertTypes_t1579_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49005_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Add(T)
MethodInfo ICollection_1_Add_m49005_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8641_ICollection_1_Add_m49005_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49005_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49006_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Clear()
MethodInfo ICollection_1_Clear_m49006_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49006_GenericMethod/* genericMethod */

};
extern Il2CppType CertTypes_t1579_0_0_0;
static ParameterInfo ICollection_1_t8641_ICollection_1_Contains_m49007_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CertTypes_t1579_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49007_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Contains(T)
MethodInfo ICollection_1_Contains_m49007_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8641_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8641_ICollection_1_Contains_m49007_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49007_GenericMethod/* genericMethod */

};
extern Il2CppType CertTypesU5BU5D_t5683_0_0_0;
extern Il2CppType CertTypesU5BU5D_t5683_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8641_ICollection_1_CopyTo_m49008_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CertTypesU5BU5D_t5683_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49008_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49008_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8641_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8641_ICollection_1_CopyTo_m49008_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49008_GenericMethod/* genericMethod */

};
extern Il2CppType CertTypes_t1579_0_0_0;
static ParameterInfo ICollection_1_t8641_ICollection_1_Remove_m49009_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CertTypes_t1579_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49009_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Remove(T)
MethodInfo ICollection_1_Remove_m49009_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8641_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8641_ICollection_1_Remove_m49009_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49009_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8641_MethodInfos[] =
{
	&ICollection_1_get_Count_m49003_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49004_MethodInfo,
	&ICollection_1_Add_m49005_MethodInfo,
	&ICollection_1_Clear_m49006_MethodInfo,
	&ICollection_1_Contains_m49007_MethodInfo,
	&ICollection_1_CopyTo_m49008_MethodInfo,
	&ICollection_1_Remove_m49009_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8643_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8641_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8643_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8641_0_0_0;
extern Il2CppType ICollection_1_t8641_1_0_0;
struct ICollection_1_t8641;
extern Il2CppGenericClass ICollection_1_t8641_GenericClass;
TypeInfo ICollection_1_t8641_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8641_MethodInfos/* methods */
	, ICollection_1_t8641_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8641_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8641_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8641_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8641_0_0_0/* byval_arg */
	, &ICollection_1_t8641_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8641_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern Il2CppType IEnumerator_1_t6801_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49010_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49010_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8643_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6801_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49010_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8643_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49010_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8643_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8643_0_0_0;
extern Il2CppType IEnumerable_1_t8643_1_0_0;
struct IEnumerable_1_t8643;
extern Il2CppGenericClass IEnumerable_1_t8643_GenericClass;
TypeInfo IEnumerable_1_t8643_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8643_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8643_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8643_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8643_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8643_0_0_0/* byval_arg */
	, &IEnumerable_1_t8643_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8643_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8642_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>
extern MethodInfo IList_1_get_Item_m49011_MethodInfo;
extern MethodInfo IList_1_set_Item_m49012_MethodInfo;
static PropertyInfo IList_1_t8642____Item_PropertyInfo = 
{
	&IList_1_t8642_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49011_MethodInfo/* get */
	, &IList_1_set_Item_m49012_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8642_PropertyInfos[] =
{
	&IList_1_t8642____Item_PropertyInfo,
	NULL
};
extern Il2CppType CertTypes_t1579_0_0_0;
static ParameterInfo IList_1_t8642_IList_1_IndexOf_m49013_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CertTypes_t1579_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49013_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49013_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8642_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8642_IList_1_IndexOf_m49013_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49013_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CertTypes_t1579_0_0_0;
static ParameterInfo IList_1_t8642_IList_1_Insert_m49014_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CertTypes_t1579_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49014_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49014_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8642_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8642_IList_1_Insert_m49014_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49014_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8642_IList_1_RemoveAt_m49015_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49015_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49015_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8642_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8642_IList_1_RemoveAt_m49015_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49015_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8642_IList_1_get_Item_m49011_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType CertTypes_t1579_0_0_0;
extern void* RuntimeInvoker_CertTypes_t1579_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49011_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49011_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8642_il2cpp_TypeInfo/* declaring_type */
	, &CertTypes_t1579_0_0_0/* return_type */
	, RuntimeInvoker_CertTypes_t1579_Int32_t93/* invoker_method */
	, IList_1_t8642_IList_1_get_Item_m49011_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49011_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CertTypes_t1579_0_0_0;
static ParameterInfo IList_1_t8642_IList_1_set_Item_m49012_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CertTypes_t1579_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49012_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49012_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8642_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8642_IList_1_set_Item_m49012_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49012_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8642_MethodInfos[] =
{
	&IList_1_IndexOf_m49013_MethodInfo,
	&IList_1_Insert_m49014_MethodInfo,
	&IList_1_RemoveAt_m49015_MethodInfo,
	&IList_1_get_Item_m49011_MethodInfo,
	&IList_1_set_Item_m49012_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8642_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8641_il2cpp_TypeInfo,
	&IEnumerable_1_t8643_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8642_0_0_0;
extern Il2CppType IList_1_t8642_1_0_0;
struct IList_1_t8642;
extern Il2CppGenericClass IList_1_t8642_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8642_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8642_MethodInfos/* methods */
	, IList_1_t8642_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8642_il2cpp_TypeInfo/* element_class */
	, IList_1_t8642_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8642_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8642_0_0_0/* byval_arg */
	, &IList_1_t8642_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8642_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6803_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.AlertLevel
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>
extern MethodInfo IEnumerator_1_get_Current_m49016_MethodInfo;
static PropertyInfo IEnumerator_1_t6803____Current_PropertyInfo = 
{
	&IEnumerator_1_t6803_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49016_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6803_PropertyInfos[] =
{
	&IEnumerator_1_t6803____Current_PropertyInfo,
	NULL
};
extern Il2CppType AlertLevel_t1585_0_0_0;
extern void* RuntimeInvoker_AlertLevel_t1585 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49016_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49016_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6803_il2cpp_TypeInfo/* declaring_type */
	, &AlertLevel_t1585_0_0_0/* return_type */
	, RuntimeInvoker_AlertLevel_t1585/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49016_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6803_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49016_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6803_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6803_0_0_0;
extern Il2CppType IEnumerator_1_t6803_1_0_0;
struct IEnumerator_1_t6803;
extern Il2CppGenericClass IEnumerator_1_t6803_GenericClass;
TypeInfo IEnumerator_1_t6803_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6803_MethodInfos/* methods */
	, IEnumerator_1_t6803_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6803_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6803_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6803_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6803_0_0_0/* byval_arg */
	, &IEnumerator_1_t6803_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6803_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_532.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4910_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_532MethodDeclarations.h"

extern TypeInfo AlertLevel_t1585_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30136_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAlertLevel_t1585_m38704_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.AlertLevel>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.AlertLevel>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisAlertLevel_t1585_m38704 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30132_MethodInfo;
 void InternalEnumerator_1__ctor_m30132 (InternalEnumerator_1_t4910 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30133_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30133 (InternalEnumerator_1_t4910 * __this, MethodInfo* method){
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m30136(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30136_MethodInfo);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&AlertLevel_t1585_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30134_MethodInfo;
 void InternalEnumerator_1_Dispose_m30134 (InternalEnumerator_1_t4910 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30135_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30135 (InternalEnumerator_1_t4910 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m30136 (InternalEnumerator_1_t4910 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint8_t L_8 = Array_InternalArray__get_Item_TisAlertLevel_t1585_m38704(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisAlertLevel_t1585_m38704_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4910____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4910_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4910, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4910____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4910_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4910, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4910_FieldInfos[] =
{
	&InternalEnumerator_1_t4910____array_0_FieldInfo,
	&InternalEnumerator_1_t4910____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4910____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4910_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30133_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4910____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4910_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30136_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4910_PropertyInfos[] =
{
	&InternalEnumerator_1_t4910____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4910____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4910_InternalEnumerator_1__ctor_m30132_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30132_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30132_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30132/* method */
	, &InternalEnumerator_1_t4910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4910_InternalEnumerator_1__ctor_m30132_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30132_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30133_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30133_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30133/* method */
	, &InternalEnumerator_1_t4910_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30133_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30134_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30134_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30134/* method */
	, &InternalEnumerator_1_t4910_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30134_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30135_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30135_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30135/* method */
	, &InternalEnumerator_1_t4910_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30135_GenericMethod/* genericMethod */

};
extern Il2CppType AlertLevel_t1585_0_0_0;
extern void* RuntimeInvoker_AlertLevel_t1585 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30136_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30136_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30136/* method */
	, &InternalEnumerator_1_t4910_il2cpp_TypeInfo/* declaring_type */
	, &AlertLevel_t1585_0_0_0/* return_type */
	, RuntimeInvoker_AlertLevel_t1585/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30136_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4910_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30132_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30133_MethodInfo,
	&InternalEnumerator_1_Dispose_m30134_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30135_MethodInfo,
	&InternalEnumerator_1_get_Current_m30136_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4910_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30133_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30135_MethodInfo,
	&InternalEnumerator_1_Dispose_m30134_MethodInfo,
	&InternalEnumerator_1_get_Current_m30136_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4910_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6803_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4910_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6803_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4910_0_0_0;
extern Il2CppType InternalEnumerator_1_t4910_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4910_GenericClass;
TypeInfo InternalEnumerator_1_t4910_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4910_MethodInfos/* methods */
	, InternalEnumerator_1_t4910_PropertyInfos/* properties */
	, InternalEnumerator_1_t4910_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4910_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4910_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4910_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4910_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4910_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4910_1_0_0/* this_arg */
	, InternalEnumerator_1_t4910_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4910_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4910)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8644_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>
extern MethodInfo ICollection_1_get_Count_m49017_MethodInfo;
static PropertyInfo ICollection_1_t8644____Count_PropertyInfo = 
{
	&ICollection_1_t8644_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49017_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49018_MethodInfo;
static PropertyInfo ICollection_1_t8644____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8644_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49018_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8644_PropertyInfos[] =
{
	&ICollection_1_t8644____Count_PropertyInfo,
	&ICollection_1_t8644____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49017_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Count()
MethodInfo ICollection_1_get_Count_m49017_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8644_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49017_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49018_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49018_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8644_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49018_GenericMethod/* genericMethod */

};
extern Il2CppType AlertLevel_t1585_0_0_0;
extern Il2CppType AlertLevel_t1585_0_0_0;
static ParameterInfo ICollection_1_t8644_ICollection_1_Add_m49019_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertLevel_t1585_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49019_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Add(T)
MethodInfo ICollection_1_Add_m49019_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Byte_t796/* invoker_method */
	, ICollection_1_t8644_ICollection_1_Add_m49019_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49019_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49020_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Clear()
MethodInfo ICollection_1_Clear_m49020_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49020_GenericMethod/* genericMethod */

};
extern Il2CppType AlertLevel_t1585_0_0_0;
static ParameterInfo ICollection_1_t8644_ICollection_1_Contains_m49021_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertLevel_t1585_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49021_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Contains(T)
MethodInfo ICollection_1_Contains_m49021_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8644_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Byte_t796/* invoker_method */
	, ICollection_1_t8644_ICollection_1_Contains_m49021_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49021_GenericMethod/* genericMethod */

};
extern Il2CppType AlertLevelU5BU5D_t5684_0_0_0;
extern Il2CppType AlertLevelU5BU5D_t5684_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8644_ICollection_1_CopyTo_m49022_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AlertLevelU5BU5D_t5684_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49022_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49022_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8644_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8644_ICollection_1_CopyTo_m49022_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49022_GenericMethod/* genericMethod */

};
extern Il2CppType AlertLevel_t1585_0_0_0;
static ParameterInfo ICollection_1_t8644_ICollection_1_Remove_m49023_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertLevel_t1585_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49023_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertLevel>::Remove(T)
MethodInfo ICollection_1_Remove_m49023_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8644_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Byte_t796/* invoker_method */
	, ICollection_1_t8644_ICollection_1_Remove_m49023_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49023_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8644_MethodInfos[] =
{
	&ICollection_1_get_Count_m49017_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49018_MethodInfo,
	&ICollection_1_Add_m49019_MethodInfo,
	&ICollection_1_Clear_m49020_MethodInfo,
	&ICollection_1_Contains_m49021_MethodInfo,
	&ICollection_1_CopyTo_m49022_MethodInfo,
	&ICollection_1_Remove_m49023_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8646_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8644_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8646_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8644_0_0_0;
extern Il2CppType ICollection_1_t8644_1_0_0;
struct ICollection_1_t8644;
extern Il2CppGenericClass ICollection_1_t8644_GenericClass;
TypeInfo ICollection_1_t8644_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8644_MethodInfos/* methods */
	, ICollection_1_t8644_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8644_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8644_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8644_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8644_0_0_0/* byval_arg */
	, &ICollection_1_t8644_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8644_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertLevel>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertLevel>
extern Il2CppType IEnumerator_1_t6803_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49024_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertLevel>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49024_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8646_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6803_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49024_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8646_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49024_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8646_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8646_0_0_0;
extern Il2CppType IEnumerable_1_t8646_1_0_0;
struct IEnumerable_1_t8646;
extern Il2CppGenericClass IEnumerable_1_t8646_GenericClass;
TypeInfo IEnumerable_1_t8646_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8646_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8646_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8646_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8646_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8646_0_0_0/* byval_arg */
	, &IEnumerable_1_t8646_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8646_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8645_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>
extern MethodInfo IList_1_get_Item_m49025_MethodInfo;
extern MethodInfo IList_1_set_Item_m49026_MethodInfo;
static PropertyInfo IList_1_t8645____Item_PropertyInfo = 
{
	&IList_1_t8645_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49025_MethodInfo/* get */
	, &IList_1_set_Item_m49026_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8645_PropertyInfos[] =
{
	&IList_1_t8645____Item_PropertyInfo,
	NULL
};
extern Il2CppType AlertLevel_t1585_0_0_0;
static ParameterInfo IList_1_t8645_IList_1_IndexOf_m49027_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertLevel_t1585_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49027_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49027_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8645_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Byte_t796/* invoker_method */
	, IList_1_t8645_IList_1_IndexOf_m49027_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49027_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AlertLevel_t1585_0_0_0;
static ParameterInfo IList_1_t8645_IList_1_Insert_m49028_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AlertLevel_t1585_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49028_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49028_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8645_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Byte_t796/* invoker_method */
	, IList_1_t8645_IList_1_Insert_m49028_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49028_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8645_IList_1_RemoveAt_m49029_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49029_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49029_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8645_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8645_IList_1_RemoveAt_m49029_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49029_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8645_IList_1_get_Item_m49025_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AlertLevel_t1585_0_0_0;
extern void* RuntimeInvoker_AlertLevel_t1585_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49025_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49025_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8645_il2cpp_TypeInfo/* declaring_type */
	, &AlertLevel_t1585_0_0_0/* return_type */
	, RuntimeInvoker_AlertLevel_t1585_Int32_t93/* invoker_method */
	, IList_1_t8645_IList_1_get_Item_m49025_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49025_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AlertLevel_t1585_0_0_0;
static ParameterInfo IList_1_t8645_IList_1_set_Item_m49026_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AlertLevel_t1585_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49026_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertLevel>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49026_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8645_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Byte_t796/* invoker_method */
	, IList_1_t8645_IList_1_set_Item_m49026_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49026_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8645_MethodInfos[] =
{
	&IList_1_IndexOf_m49027_MethodInfo,
	&IList_1_Insert_m49028_MethodInfo,
	&IList_1_RemoveAt_m49029_MethodInfo,
	&IList_1_get_Item_m49025_MethodInfo,
	&IList_1_set_Item_m49026_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8645_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8644_il2cpp_TypeInfo,
	&IEnumerable_1_t8646_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8645_0_0_0;
extern Il2CppType IList_1_t8645_1_0_0;
struct IList_1_t8645;
extern Il2CppGenericClass IList_1_t8645_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8645_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8645_MethodInfos/* methods */
	, IList_1_t8645_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8645_il2cpp_TypeInfo/* element_class */
	, IList_1_t8645_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8645_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8645_0_0_0/* byval_arg */
	, &IList_1_t8645_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8645_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6805_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.AlertDescription
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>
extern MethodInfo IEnumerator_1_get_Current_m49030_MethodInfo;
static PropertyInfo IEnumerator_1_t6805____Current_PropertyInfo = 
{
	&IEnumerator_1_t6805_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49030_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6805_PropertyInfos[] =
{
	&IEnumerator_1_t6805____Current_PropertyInfo,
	NULL
};
extern Il2CppType AlertDescription_t1586_0_0_0;
extern void* RuntimeInvoker_AlertDescription_t1586 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49030_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49030_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6805_il2cpp_TypeInfo/* declaring_type */
	, &AlertDescription_t1586_0_0_0/* return_type */
	, RuntimeInvoker_AlertDescription_t1586/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49030_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6805_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49030_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6805_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6805_0_0_0;
extern Il2CppType IEnumerator_1_t6805_1_0_0;
struct IEnumerator_1_t6805;
extern Il2CppGenericClass IEnumerator_1_t6805_GenericClass;
TypeInfo IEnumerator_1_t6805_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6805_MethodInfos/* methods */
	, IEnumerator_1_t6805_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6805_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6805_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6805_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6805_0_0_0/* byval_arg */
	, &IEnumerator_1_t6805_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6805_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_533.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4911_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_533MethodDeclarations.h"

extern TypeInfo AlertDescription_t1586_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30141_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAlertDescription_t1586_m38715_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.AlertDescription>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.AlertDescription>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisAlertDescription_t1586_m38715 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30137_MethodInfo;
 void InternalEnumerator_1__ctor_m30137 (InternalEnumerator_1_t4911 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30138_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30138 (InternalEnumerator_1_t4911 * __this, MethodInfo* method){
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m30141(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30141_MethodInfo);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&AlertDescription_t1586_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30139_MethodInfo;
 void InternalEnumerator_1_Dispose_m30139 (InternalEnumerator_1_t4911 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30140_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30140 (InternalEnumerator_1_t4911 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m30141 (InternalEnumerator_1_t4911 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint8_t L_8 = Array_InternalArray__get_Item_TisAlertDescription_t1586_m38715(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisAlertDescription_t1586_m38715_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4911____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4911_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4911, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4911____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4911_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4911, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4911_FieldInfos[] =
{
	&InternalEnumerator_1_t4911____array_0_FieldInfo,
	&InternalEnumerator_1_t4911____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4911____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4911_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30138_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4911____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4911_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30141_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4911_PropertyInfos[] =
{
	&InternalEnumerator_1_t4911____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4911____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4911_InternalEnumerator_1__ctor_m30137_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30137_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30137_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30137/* method */
	, &InternalEnumerator_1_t4911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4911_InternalEnumerator_1__ctor_m30137_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30137_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30138_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30138_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30138/* method */
	, &InternalEnumerator_1_t4911_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30138_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30139_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30139_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30139/* method */
	, &InternalEnumerator_1_t4911_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30139_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30140_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30140_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30140/* method */
	, &InternalEnumerator_1_t4911_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30140_GenericMethod/* genericMethod */

};
extern Il2CppType AlertDescription_t1586_0_0_0;
extern void* RuntimeInvoker_AlertDescription_t1586 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30141_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30141_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30141/* method */
	, &InternalEnumerator_1_t4911_il2cpp_TypeInfo/* declaring_type */
	, &AlertDescription_t1586_0_0_0/* return_type */
	, RuntimeInvoker_AlertDescription_t1586/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30141_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4911_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30137_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30138_MethodInfo,
	&InternalEnumerator_1_Dispose_m30139_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30140_MethodInfo,
	&InternalEnumerator_1_get_Current_m30141_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4911_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30138_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30140_MethodInfo,
	&InternalEnumerator_1_Dispose_m30139_MethodInfo,
	&InternalEnumerator_1_get_Current_m30141_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4911_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6805_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4911_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6805_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4911_0_0_0;
extern Il2CppType InternalEnumerator_1_t4911_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4911_GenericClass;
TypeInfo InternalEnumerator_1_t4911_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4911_MethodInfos/* methods */
	, InternalEnumerator_1_t4911_PropertyInfos/* properties */
	, InternalEnumerator_1_t4911_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4911_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4911_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4911_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4911_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4911_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4911_1_0_0/* this_arg */
	, InternalEnumerator_1_t4911_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4911_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4911)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8647_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>
extern MethodInfo ICollection_1_get_Count_m49031_MethodInfo;
static PropertyInfo ICollection_1_t8647____Count_PropertyInfo = 
{
	&ICollection_1_t8647_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49031_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49032_MethodInfo;
static PropertyInfo ICollection_1_t8647____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8647_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49032_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8647_PropertyInfos[] =
{
	&ICollection_1_t8647____Count_PropertyInfo,
	&ICollection_1_t8647____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49031_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Count()
MethodInfo ICollection_1_get_Count_m49031_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8647_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49031_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49032_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49032_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8647_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49032_GenericMethod/* genericMethod */

};
extern Il2CppType AlertDescription_t1586_0_0_0;
extern Il2CppType AlertDescription_t1586_0_0_0;
static ParameterInfo ICollection_1_t8647_ICollection_1_Add_m49033_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertDescription_t1586_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49033_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Add(T)
MethodInfo ICollection_1_Add_m49033_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8647_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Byte_t796/* invoker_method */
	, ICollection_1_t8647_ICollection_1_Add_m49033_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49033_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49034_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Clear()
MethodInfo ICollection_1_Clear_m49034_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8647_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49034_GenericMethod/* genericMethod */

};
extern Il2CppType AlertDescription_t1586_0_0_0;
static ParameterInfo ICollection_1_t8647_ICollection_1_Contains_m49035_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertDescription_t1586_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49035_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Contains(T)
MethodInfo ICollection_1_Contains_m49035_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8647_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Byte_t796/* invoker_method */
	, ICollection_1_t8647_ICollection_1_Contains_m49035_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49035_GenericMethod/* genericMethod */

};
extern Il2CppType AlertDescriptionU5BU5D_t5685_0_0_0;
extern Il2CppType AlertDescriptionU5BU5D_t5685_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8647_ICollection_1_CopyTo_m49036_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AlertDescriptionU5BU5D_t5685_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49036_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49036_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8647_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8647_ICollection_1_CopyTo_m49036_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49036_GenericMethod/* genericMethod */

};
extern Il2CppType AlertDescription_t1586_0_0_0;
static ParameterInfo ICollection_1_t8647_ICollection_1_Remove_m49037_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertDescription_t1586_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49037_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.AlertDescription>::Remove(T)
MethodInfo ICollection_1_Remove_m49037_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8647_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Byte_t796/* invoker_method */
	, ICollection_1_t8647_ICollection_1_Remove_m49037_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49037_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8647_MethodInfos[] =
{
	&ICollection_1_get_Count_m49031_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49032_MethodInfo,
	&ICollection_1_Add_m49033_MethodInfo,
	&ICollection_1_Clear_m49034_MethodInfo,
	&ICollection_1_Contains_m49035_MethodInfo,
	&ICollection_1_CopyTo_m49036_MethodInfo,
	&ICollection_1_Remove_m49037_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8649_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8647_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8649_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8647_0_0_0;
extern Il2CppType ICollection_1_t8647_1_0_0;
struct ICollection_1_t8647;
extern Il2CppGenericClass ICollection_1_t8647_GenericClass;
TypeInfo ICollection_1_t8647_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8647_MethodInfos/* methods */
	, ICollection_1_t8647_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8647_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8647_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8647_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8647_0_0_0/* byval_arg */
	, &ICollection_1_t8647_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8647_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertDescription>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertDescription>
extern Il2CppType IEnumerator_1_t6805_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49038_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.AlertDescription>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49038_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8649_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6805_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49038_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8649_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49038_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8649_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8649_0_0_0;
extern Il2CppType IEnumerable_1_t8649_1_0_0;
struct IEnumerable_1_t8649;
extern Il2CppGenericClass IEnumerable_1_t8649_GenericClass;
TypeInfo IEnumerable_1_t8649_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8649_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8649_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8649_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8649_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8649_0_0_0/* byval_arg */
	, &IEnumerable_1_t8649_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8649_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8648_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>
extern MethodInfo IList_1_get_Item_m49039_MethodInfo;
extern MethodInfo IList_1_set_Item_m49040_MethodInfo;
static PropertyInfo IList_1_t8648____Item_PropertyInfo = 
{
	&IList_1_t8648_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49039_MethodInfo/* get */
	, &IList_1_set_Item_m49040_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8648_PropertyInfos[] =
{
	&IList_1_t8648____Item_PropertyInfo,
	NULL
};
extern Il2CppType AlertDescription_t1586_0_0_0;
static ParameterInfo IList_1_t8648_IList_1_IndexOf_m49041_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AlertDescription_t1586_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49041_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49041_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8648_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Byte_t796/* invoker_method */
	, IList_1_t8648_IList_1_IndexOf_m49041_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49041_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AlertDescription_t1586_0_0_0;
static ParameterInfo IList_1_t8648_IList_1_Insert_m49042_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AlertDescription_t1586_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49042_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49042_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8648_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Byte_t796/* invoker_method */
	, IList_1_t8648_IList_1_Insert_m49042_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49042_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8648_IList_1_RemoveAt_m49043_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49043_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49043_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8648_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8648_IList_1_RemoveAt_m49043_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49043_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8648_IList_1_get_Item_m49039_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AlertDescription_t1586_0_0_0;
extern void* RuntimeInvoker_AlertDescription_t1586_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49039_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49039_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8648_il2cpp_TypeInfo/* declaring_type */
	, &AlertDescription_t1586_0_0_0/* return_type */
	, RuntimeInvoker_AlertDescription_t1586_Int32_t93/* invoker_method */
	, IList_1_t8648_IList_1_get_Item_m49039_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49039_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AlertDescription_t1586_0_0_0;
static ParameterInfo IList_1_t8648_IList_1_set_Item_m49040_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AlertDescription_t1586_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49040_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.AlertDescription>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49040_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8648_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Byte_t796/* invoker_method */
	, IList_1_t8648_IList_1_set_Item_m49040_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49040_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8648_MethodInfos[] =
{
	&IList_1_IndexOf_m49041_MethodInfo,
	&IList_1_Insert_m49042_MethodInfo,
	&IList_1_RemoveAt_m49043_MethodInfo,
	&IList_1_get_Item_m49039_MethodInfo,
	&IList_1_set_Item_m49040_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8648_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8647_il2cpp_TypeInfo,
	&IEnumerable_1_t8649_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8648_0_0_0;
extern Il2CppType IList_1_t8648_1_0_0;
struct IList_1_t8648;
extern Il2CppGenericClass IList_1_t8648_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8648_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8648_MethodInfos/* methods */
	, IList_1_t8648_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8648_il2cpp_TypeInfo/* element_class */
	, IList_1_t8648_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8648_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8648_0_0_0/* byval_arg */
	, &IList_1_t8648_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8648_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6807_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.CipherAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern MethodInfo IEnumerator_1_get_Current_m49044_MethodInfo;
static PropertyInfo IEnumerator_1_t6807____Current_PropertyInfo = 
{
	&IEnumerator_1_t6807_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49044_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6807_PropertyInfos[] =
{
	&IEnumerator_1_t6807____Current_PropertyInfo,
	NULL
};
extern Il2CppType CipherAlgorithmType_t1588_0_0_0;
extern void* RuntimeInvoker_CipherAlgorithmType_t1588 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49044_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49044_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6807_il2cpp_TypeInfo/* declaring_type */
	, &CipherAlgorithmType_t1588_0_0_0/* return_type */
	, RuntimeInvoker_CipherAlgorithmType_t1588/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49044_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6807_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49044_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6807_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6807_0_0_0;
extern Il2CppType IEnumerator_1_t6807_1_0_0;
struct IEnumerator_1_t6807;
extern Il2CppGenericClass IEnumerator_1_t6807_GenericClass;
TypeInfo IEnumerator_1_t6807_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6807_MethodInfos/* methods */
	, IEnumerator_1_t6807_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6807_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6807_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6807_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6807_0_0_0/* byval_arg */
	, &IEnumerator_1_t6807_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6807_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_534.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4912_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_534MethodDeclarations.h"

extern TypeInfo CipherAlgorithmType_t1588_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30146_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCipherAlgorithmType_t1588_m38726_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.CipherAlgorithmType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.CipherAlgorithmType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisCipherAlgorithmType_t1588_m38726 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30142_MethodInfo;
 void InternalEnumerator_1__ctor_m30142 (InternalEnumerator_1_t4912 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30143_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30143 (InternalEnumerator_1_t4912 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30146(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30146_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&CipherAlgorithmType_t1588_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30144_MethodInfo;
 void InternalEnumerator_1_Dispose_m30144 (InternalEnumerator_1_t4912 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30145_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30145 (InternalEnumerator_1_t4912 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30146 (InternalEnumerator_1_t4912 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisCipherAlgorithmType_t1588_m38726(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisCipherAlgorithmType_t1588_m38726_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4912____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4912_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4912, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4912____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4912_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4912, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4912_FieldInfos[] =
{
	&InternalEnumerator_1_t4912____array_0_FieldInfo,
	&InternalEnumerator_1_t4912____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4912____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4912_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30143_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4912____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4912_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30146_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4912_PropertyInfos[] =
{
	&InternalEnumerator_1_t4912____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4912____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4912_InternalEnumerator_1__ctor_m30142_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30142_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30142_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30142/* method */
	, &InternalEnumerator_1_t4912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4912_InternalEnumerator_1__ctor_m30142_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30142_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30143_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30143_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30143/* method */
	, &InternalEnumerator_1_t4912_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30143_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30144_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30144_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30144/* method */
	, &InternalEnumerator_1_t4912_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30144_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30145_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30145_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30145/* method */
	, &InternalEnumerator_1_t4912_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30145_GenericMethod/* genericMethod */

};
extern Il2CppType CipherAlgorithmType_t1588_0_0_0;
extern void* RuntimeInvoker_CipherAlgorithmType_t1588 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30146_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30146_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30146/* method */
	, &InternalEnumerator_1_t4912_il2cpp_TypeInfo/* declaring_type */
	, &CipherAlgorithmType_t1588_0_0_0/* return_type */
	, RuntimeInvoker_CipherAlgorithmType_t1588/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30146_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4912_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30142_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30143_MethodInfo,
	&InternalEnumerator_1_Dispose_m30144_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30145_MethodInfo,
	&InternalEnumerator_1_get_Current_m30146_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4912_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30143_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30145_MethodInfo,
	&InternalEnumerator_1_Dispose_m30144_MethodInfo,
	&InternalEnumerator_1_get_Current_m30146_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4912_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6807_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4912_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6807_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4912_0_0_0;
extern Il2CppType InternalEnumerator_1_t4912_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4912_GenericClass;
TypeInfo InternalEnumerator_1_t4912_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4912_MethodInfos/* methods */
	, InternalEnumerator_1_t4912_PropertyInfos/* properties */
	, InternalEnumerator_1_t4912_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4912_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4912_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4912_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4912_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4912_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4912_1_0_0/* this_arg */
	, InternalEnumerator_1_t4912_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4912_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4912)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8650_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern MethodInfo ICollection_1_get_Count_m49045_MethodInfo;
static PropertyInfo ICollection_1_t8650____Count_PropertyInfo = 
{
	&ICollection_1_t8650_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49045_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49046_MethodInfo;
static PropertyInfo ICollection_1_t8650____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8650_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49046_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8650_PropertyInfos[] =
{
	&ICollection_1_t8650____Count_PropertyInfo,
	&ICollection_1_t8650____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49045_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Count()
MethodInfo ICollection_1_get_Count_m49045_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8650_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49045_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49046_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49046_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8650_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49046_GenericMethod/* genericMethod */

};
extern Il2CppType CipherAlgorithmType_t1588_0_0_0;
extern Il2CppType CipherAlgorithmType_t1588_0_0_0;
static ParameterInfo ICollection_1_t8650_ICollection_1_Add_m49047_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmType_t1588_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49047_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Add(T)
MethodInfo ICollection_1_Add_m49047_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8650_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8650_ICollection_1_Add_m49047_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49047_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49048_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Clear()
MethodInfo ICollection_1_Clear_m49048_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8650_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49048_GenericMethod/* genericMethod */

};
extern Il2CppType CipherAlgorithmType_t1588_0_0_0;
static ParameterInfo ICollection_1_t8650_ICollection_1_Contains_m49049_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmType_t1588_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49049_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Contains(T)
MethodInfo ICollection_1_Contains_m49049_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8650_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8650_ICollection_1_Contains_m49049_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49049_GenericMethod/* genericMethod */

};
extern Il2CppType CipherAlgorithmTypeU5BU5D_t5686_0_0_0;
extern Il2CppType CipherAlgorithmTypeU5BU5D_t5686_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8650_ICollection_1_CopyTo_m49050_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmTypeU5BU5D_t5686_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49050_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49050_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8650_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8650_ICollection_1_CopyTo_m49050_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49050_GenericMethod/* genericMethod */

};
extern Il2CppType CipherAlgorithmType_t1588_0_0_0;
static ParameterInfo ICollection_1_t8650_ICollection_1_Remove_m49051_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmType_t1588_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49051_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Remove(T)
MethodInfo ICollection_1_Remove_m49051_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8650_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8650_ICollection_1_Remove_m49051_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49051_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8650_MethodInfos[] =
{
	&ICollection_1_get_Count_m49045_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49046_MethodInfo,
	&ICollection_1_Add_m49047_MethodInfo,
	&ICollection_1_Clear_m49048_MethodInfo,
	&ICollection_1_Contains_m49049_MethodInfo,
	&ICollection_1_CopyTo_m49050_MethodInfo,
	&ICollection_1_Remove_m49051_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8652_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8650_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8652_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8650_0_0_0;
extern Il2CppType ICollection_1_t8650_1_0_0;
struct ICollection_1_t8650;
extern Il2CppGenericClass ICollection_1_t8650_GenericClass;
TypeInfo ICollection_1_t8650_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8650_MethodInfos/* methods */
	, ICollection_1_t8650_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8650_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8650_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8650_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8650_0_0_0/* byval_arg */
	, &ICollection_1_t8650_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8650_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern Il2CppType IEnumerator_1_t6807_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49052_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49052_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8652_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6807_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49052_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8652_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49052_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8652_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8652_0_0_0;
extern Il2CppType IEnumerable_1_t8652_1_0_0;
struct IEnumerable_1_t8652;
extern Il2CppGenericClass IEnumerable_1_t8652_GenericClass;
TypeInfo IEnumerable_1_t8652_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8652_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8652_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8652_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8652_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8652_0_0_0/* byval_arg */
	, &IEnumerable_1_t8652_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8652_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8651_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>
extern MethodInfo IList_1_get_Item_m49053_MethodInfo;
extern MethodInfo IList_1_set_Item_m49054_MethodInfo;
static PropertyInfo IList_1_t8651____Item_PropertyInfo = 
{
	&IList_1_t8651_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49053_MethodInfo/* get */
	, &IList_1_set_Item_m49054_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8651_PropertyInfos[] =
{
	&IList_1_t8651____Item_PropertyInfo,
	NULL
};
extern Il2CppType CipherAlgorithmType_t1588_0_0_0;
static ParameterInfo IList_1_t8651_IList_1_IndexOf_m49055_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmType_t1588_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49055_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49055_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8651_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8651_IList_1_IndexOf_m49055_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49055_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CipherAlgorithmType_t1588_0_0_0;
static ParameterInfo IList_1_t8651_IList_1_Insert_m49056_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmType_t1588_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49056_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49056_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8651_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8651_IList_1_Insert_m49056_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49056_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8651_IList_1_RemoveAt_m49057_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49057_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49057_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8651_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8651_IList_1_RemoveAt_m49057_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49057_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8651_IList_1_get_Item_m49053_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType CipherAlgorithmType_t1588_0_0_0;
extern void* RuntimeInvoker_CipherAlgorithmType_t1588_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49053_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49053_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8651_il2cpp_TypeInfo/* declaring_type */
	, &CipherAlgorithmType_t1588_0_0_0/* return_type */
	, RuntimeInvoker_CipherAlgorithmType_t1588_Int32_t93/* invoker_method */
	, IList_1_t8651_IList_1_get_Item_m49053_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49053_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType CipherAlgorithmType_t1588_0_0_0;
static ParameterInfo IList_1_t8651_IList_1_set_Item_m49054_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &CipherAlgorithmType_t1588_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49054_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.CipherAlgorithmType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49054_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8651_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8651_IList_1_set_Item_m49054_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49054_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8651_MethodInfos[] =
{
	&IList_1_IndexOf_m49055_MethodInfo,
	&IList_1_Insert_m49056_MethodInfo,
	&IList_1_RemoveAt_m49057_MethodInfo,
	&IList_1_get_Item_m49053_MethodInfo,
	&IList_1_set_Item_m49054_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8651_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8650_il2cpp_TypeInfo,
	&IEnumerable_1_t8652_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8651_0_0_0;
extern Il2CppType IList_1_t8651_1_0_0;
struct IList_1_t8651;
extern Il2CppGenericClass IList_1_t8651_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8651_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8651_MethodInfos/* methods */
	, IList_1_t8651_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8651_il2cpp_TypeInfo/* element_class */
	, IList_1_t8651_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8651_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8651_0_0_0/* byval_arg */
	, &IList_1_t8651_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8651_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6808_il2cpp_TypeInfo;

// System.Byte
#include "mscorlib_System_Byte.h"


// T System.Collections.Generic.IEnumerator`1<System.Byte[]>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Byte[]>
extern MethodInfo IEnumerator_1_get_Current_m49058_MethodInfo;
static PropertyInfo IEnumerator_1_t6808____Current_PropertyInfo = 
{
	&IEnumerator_1_t6808_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49058_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6808_PropertyInfos[] =
{
	&IEnumerator_1_t6808____Current_PropertyInfo,
	NULL
};
extern Il2CppType ByteU5BU5D_t609_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49058_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Byte[]>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49058_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6808_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t609_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49058_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6808_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49058_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6808_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6808_0_0_0;
extern Il2CppType IEnumerator_1_t6808_1_0_0;
struct IEnumerator_1_t6808;
extern Il2CppGenericClass IEnumerator_1_t6808_GenericClass;
TypeInfo IEnumerator_1_t6808_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6808_MethodInfos/* methods */
	, IEnumerator_1_t6808_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6808_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6808_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6808_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6808_0_0_0/* byval_arg */
	, &IEnumerator_1_t6808_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6808_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Byte[]>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_535.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4913_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Byte[]>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_535MethodDeclarations.h"

extern TypeInfo ByteU5BU5D_t609_il2cpp_TypeInfo;
extern TypeInfo Byte_t796_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30151_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisByteU5BU5D_t609_m38737_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Byte[]>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Byte[]>(System.Int32)
#define Array_InternalArray__get_Item_TisByteU5BU5D_t609_m38737(__this, p0, method) (ByteU5BU5D_t609*)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Byte[]>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Byte[]>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Byte[]>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Byte[]>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Byte[]>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Byte[]>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4913____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4913_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4913, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4913____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4913_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4913, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4913_FieldInfos[] =
{
	&InternalEnumerator_1_t4913____array_0_FieldInfo,
	&InternalEnumerator_1_t4913____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30148_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4913____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4913_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30148_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4913____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4913_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30151_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4913_PropertyInfos[] =
{
	&InternalEnumerator_1_t4913____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4913____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4913_InternalEnumerator_1__ctor_m30147_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30147_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Byte[]>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30147_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4913_InternalEnumerator_1__ctor_m30147_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30147_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30148_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Byte[]>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30148_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4913_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30148_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30149_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Byte[]>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30149_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4913_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30149_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30150_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Byte[]>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30150_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4913_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30150_GenericMethod/* genericMethod */

};
extern Il2CppType ByteU5BU5D_t609_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30151_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Byte[]>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30151_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4913_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t609_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30151_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4913_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30147_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30148_MethodInfo,
	&InternalEnumerator_1_Dispose_m30149_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30150_MethodInfo,
	&InternalEnumerator_1_get_Current_m30151_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30150_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30149_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4913_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30148_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30150_MethodInfo,
	&InternalEnumerator_1_Dispose_m30149_MethodInfo,
	&InternalEnumerator_1_get_Current_m30151_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4913_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6808_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4913_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6808_il2cpp_TypeInfo, 7},
};
extern TypeInfo ByteU5BU5D_t609_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4913_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30151_MethodInfo/* Method Usage */,
	&ByteU5BU5D_t609_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisByteU5BU5D_t609_m38737_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4913_0_0_0;
extern Il2CppType InternalEnumerator_1_t4913_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4913_GenericClass;
TypeInfo InternalEnumerator_1_t4913_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4913_MethodInfos/* methods */
	, InternalEnumerator_1_t4913_PropertyInfos/* properties */
	, InternalEnumerator_1_t4913_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4913_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4913_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4913_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4913_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4913_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4913_1_0_0/* this_arg */
	, InternalEnumerator_1_t4913_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4913_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4913_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4913)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8653_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Byte[]>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Byte[]>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Byte[]>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Byte[]>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Byte[]>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Byte[]>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Byte[]>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Byte[]>
extern MethodInfo ICollection_1_get_Count_m49059_MethodInfo;
static PropertyInfo ICollection_1_t8653____Count_PropertyInfo = 
{
	&ICollection_1_t8653_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49059_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49060_MethodInfo;
static PropertyInfo ICollection_1_t8653____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8653_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49060_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8653_PropertyInfos[] =
{
	&ICollection_1_t8653____Count_PropertyInfo,
	&ICollection_1_t8653____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49059_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Byte[]>::get_Count()
MethodInfo ICollection_1_get_Count_m49059_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8653_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49059_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49060_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Byte[]>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49060_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8653_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49060_GenericMethod/* genericMethod */

};
extern Il2CppType ByteU5BU5D_t609_0_0_0;
extern Il2CppType ByteU5BU5D_t609_0_0_0;
static ParameterInfo ICollection_1_t8653_ICollection_1_Add_m49061_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5D_t609_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49061_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Byte[]>::Add(T)
MethodInfo ICollection_1_Add_m49061_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8653_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8653_ICollection_1_Add_m49061_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49061_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49062_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Byte[]>::Clear()
MethodInfo ICollection_1_Clear_m49062_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8653_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49062_GenericMethod/* genericMethod */

};
extern Il2CppType ByteU5BU5D_t609_0_0_0;
static ParameterInfo ICollection_1_t8653_ICollection_1_Contains_m49063_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5D_t609_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49063_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Byte[]>::Contains(T)
MethodInfo ICollection_1_Contains_m49063_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8653_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8653_ICollection_1_Contains_m49063_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49063_GenericMethod/* genericMethod */

};
extern Il2CppType ByteU5BU5DU5BU5D_t1674_0_0_0;
extern Il2CppType ByteU5BU5DU5BU5D_t1674_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8653_ICollection_1_CopyTo_m49064_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5DU5BU5D_t1674_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49064_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Byte[]>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49064_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8653_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8653_ICollection_1_CopyTo_m49064_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49064_GenericMethod/* genericMethod */

};
extern Il2CppType ByteU5BU5D_t609_0_0_0;
static ParameterInfo ICollection_1_t8653_ICollection_1_Remove_m49065_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5D_t609_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49065_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Byte[]>::Remove(T)
MethodInfo ICollection_1_Remove_m49065_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8653_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8653_ICollection_1_Remove_m49065_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49065_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8653_MethodInfos[] =
{
	&ICollection_1_get_Count_m49059_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49060_MethodInfo,
	&ICollection_1_Add_m49061_MethodInfo,
	&ICollection_1_Clear_m49062_MethodInfo,
	&ICollection_1_Contains_m49063_MethodInfo,
	&ICollection_1_CopyTo_m49064_MethodInfo,
	&ICollection_1_Remove_m49065_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8655_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8653_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8655_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8653_0_0_0;
extern Il2CppType ICollection_1_t8653_1_0_0;
struct ICollection_1_t8653;
extern Il2CppGenericClass ICollection_1_t8653_GenericClass;
TypeInfo ICollection_1_t8653_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8653_MethodInfos/* methods */
	, ICollection_1_t8653_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8653_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8653_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8653_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8653_0_0_0/* byval_arg */
	, &ICollection_1_t8653_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8653_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Byte[]>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Byte[]>
extern Il2CppType IEnumerator_1_t6808_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49066_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Byte[]>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49066_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8655_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6808_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49066_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8655_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49066_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8655_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8655_0_0_0;
extern Il2CppType IEnumerable_1_t8655_1_0_0;
struct IEnumerable_1_t8655;
extern Il2CppGenericClass IEnumerable_1_t8655_GenericClass;
TypeInfo IEnumerable_1_t8655_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8655_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8655_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8655_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8655_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8655_0_0_0/* byval_arg */
	, &IEnumerable_1_t8655_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8655_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8654_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Byte[]>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Byte[]>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Byte[]>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Byte[]>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Byte[]>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Byte[]>
extern MethodInfo IList_1_get_Item_m49067_MethodInfo;
extern MethodInfo IList_1_set_Item_m49068_MethodInfo;
static PropertyInfo IList_1_t8654____Item_PropertyInfo = 
{
	&IList_1_t8654_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49067_MethodInfo/* get */
	, &IList_1_set_Item_m49068_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8654_PropertyInfos[] =
{
	&IList_1_t8654____Item_PropertyInfo,
	NULL
};
extern Il2CppType ByteU5BU5D_t609_0_0_0;
static ParameterInfo IList_1_t8654_IList_1_IndexOf_m49069_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5D_t609_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49069_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Byte[]>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49069_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8654_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8654_IList_1_IndexOf_m49069_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49069_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ByteU5BU5D_t609_0_0_0;
static ParameterInfo IList_1_t8654_IList_1_Insert_m49070_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5D_t609_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49070_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Byte[]>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49070_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8654_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8654_IList_1_Insert_m49070_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49070_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8654_IList_1_RemoveAt_m49071_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49071_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Byte[]>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49071_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8654_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8654_IList_1_RemoveAt_m49071_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49071_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8654_IList_1_get_Item_m49067_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ByteU5BU5D_t609_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49067_GenericMethod;
// T System.Collections.Generic.IList`1<System.Byte[]>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49067_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8654_il2cpp_TypeInfo/* declaring_type */
	, &ByteU5BU5D_t609_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8654_IList_1_get_Item_m49067_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49067_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ByteU5BU5D_t609_0_0_0;
static ParameterInfo IList_1_t8654_IList_1_set_Item_m49068_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ByteU5BU5D_t609_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49068_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Byte[]>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49068_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8654_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8654_IList_1_set_Item_m49068_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49068_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8654_MethodInfos[] =
{
	&IList_1_IndexOf_m49069_MethodInfo,
	&IList_1_Insert_m49070_MethodInfo,
	&IList_1_RemoveAt_m49071_MethodInfo,
	&IList_1_get_Item_m49067_MethodInfo,
	&IList_1_set_Item_m49068_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8654_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8653_il2cpp_TypeInfo,
	&IEnumerable_1_t8655_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8654_0_0_0;
extern Il2CppType IList_1_t8654_1_0_0;
struct IList_1_t8654;
extern Il2CppGenericClass IList_1_t8654_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8654_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8654_MethodInfos/* methods */
	, IList_1_t8654_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8654_il2cpp_TypeInfo/* element_class */
	, IList_1_t8654_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8654_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8654_0_0_0/* byval_arg */
	, &IList_1_t8654_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8654_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6810_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.ContentType
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ContentType>
extern MethodInfo IEnumerator_1_get_Current_m49072_MethodInfo;
static PropertyInfo IEnumerator_1_t6810____Current_PropertyInfo = 
{
	&IEnumerator_1_t6810_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49072_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6810_PropertyInfos[] =
{
	&IEnumerator_1_t6810____Current_PropertyInfo,
	NULL
};
extern Il2CppType ContentType_t1604_0_0_0;
extern void* RuntimeInvoker_ContentType_t1604 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49072_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49072_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6810_il2cpp_TypeInfo/* declaring_type */
	, &ContentType_t1604_0_0_0/* return_type */
	, RuntimeInvoker_ContentType_t1604/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49072_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6810_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49072_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6810_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6810_0_0_0;
extern Il2CppType IEnumerator_1_t6810_1_0_0;
struct IEnumerator_1_t6810;
extern Il2CppGenericClass IEnumerator_1_t6810_GenericClass;
TypeInfo IEnumerator_1_t6810_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6810_MethodInfos/* methods */
	, IEnumerator_1_t6810_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6810_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6810_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6810_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6810_0_0_0/* byval_arg */
	, &IEnumerator_1_t6810_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6810_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_536.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4914_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_536MethodDeclarations.h"

extern TypeInfo ContentType_t1604_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30156_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisContentType_t1604_m38748_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.ContentType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.ContentType>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisContentType_t1604_m38748 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30152_MethodInfo;
 void InternalEnumerator_1__ctor_m30152 (InternalEnumerator_1_t4914 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30153_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30153 (InternalEnumerator_1_t4914 * __this, MethodInfo* method){
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m30156(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30156_MethodInfo);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ContentType_t1604_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30154_MethodInfo;
 void InternalEnumerator_1_Dispose_m30154 (InternalEnumerator_1_t4914 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30155_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30155 (InternalEnumerator_1_t4914 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m30156 (InternalEnumerator_1_t4914 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint8_t L_8 = Array_InternalArray__get_Item_TisContentType_t1604_m38748(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisContentType_t1604_m38748_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4914____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4914_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4914, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4914____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4914_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4914, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4914_FieldInfos[] =
{
	&InternalEnumerator_1_t4914____array_0_FieldInfo,
	&InternalEnumerator_1_t4914____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4914____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4914_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30153_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4914____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4914_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30156_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4914_PropertyInfos[] =
{
	&InternalEnumerator_1_t4914____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4914____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4914_InternalEnumerator_1__ctor_m30152_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30152_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30152_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30152/* method */
	, &InternalEnumerator_1_t4914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4914_InternalEnumerator_1__ctor_m30152_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30152_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30153_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30153_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30153/* method */
	, &InternalEnumerator_1_t4914_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30153_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30154_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30154_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30154/* method */
	, &InternalEnumerator_1_t4914_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30154_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30155_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30155_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30155/* method */
	, &InternalEnumerator_1_t4914_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30155_GenericMethod/* genericMethod */

};
extern Il2CppType ContentType_t1604_0_0_0;
extern void* RuntimeInvoker_ContentType_t1604 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30156_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ContentType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30156_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30156/* method */
	, &InternalEnumerator_1_t4914_il2cpp_TypeInfo/* declaring_type */
	, &ContentType_t1604_0_0_0/* return_type */
	, RuntimeInvoker_ContentType_t1604/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30156_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4914_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30152_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30153_MethodInfo,
	&InternalEnumerator_1_Dispose_m30154_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30155_MethodInfo,
	&InternalEnumerator_1_get_Current_m30156_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4914_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30153_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30155_MethodInfo,
	&InternalEnumerator_1_Dispose_m30154_MethodInfo,
	&InternalEnumerator_1_get_Current_m30156_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4914_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6810_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4914_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6810_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4914_0_0_0;
extern Il2CppType InternalEnumerator_1_t4914_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4914_GenericClass;
TypeInfo InternalEnumerator_1_t4914_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4914_MethodInfos/* methods */
	, InternalEnumerator_1_t4914_PropertyInfos/* properties */
	, InternalEnumerator_1_t4914_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4914_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4914_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4914_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4914_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4914_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4914_1_0_0/* this_arg */
	, InternalEnumerator_1_t4914_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4914_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4914)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8656_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>
extern MethodInfo ICollection_1_get_Count_m49073_MethodInfo;
static PropertyInfo ICollection_1_t8656____Count_PropertyInfo = 
{
	&ICollection_1_t8656_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49073_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49074_MethodInfo;
static PropertyInfo ICollection_1_t8656____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8656_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49074_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8656_PropertyInfos[] =
{
	&ICollection_1_t8656____Count_PropertyInfo,
	&ICollection_1_t8656____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49073_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::get_Count()
MethodInfo ICollection_1_get_Count_m49073_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8656_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49073_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49074_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49074_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8656_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49074_GenericMethod/* genericMethod */

};
extern Il2CppType ContentType_t1604_0_0_0;
extern Il2CppType ContentType_t1604_0_0_0;
static ParameterInfo ICollection_1_t8656_ICollection_1_Add_m49075_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContentType_t1604_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49075_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Add(T)
MethodInfo ICollection_1_Add_m49075_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8656_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Byte_t796/* invoker_method */
	, ICollection_1_t8656_ICollection_1_Add_m49075_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49075_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49076_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Clear()
MethodInfo ICollection_1_Clear_m49076_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8656_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49076_GenericMethod/* genericMethod */

};
extern Il2CppType ContentType_t1604_0_0_0;
static ParameterInfo ICollection_1_t8656_ICollection_1_Contains_m49077_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContentType_t1604_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49077_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Contains(T)
MethodInfo ICollection_1_Contains_m49077_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8656_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Byte_t796/* invoker_method */
	, ICollection_1_t8656_ICollection_1_Contains_m49077_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49077_GenericMethod/* genericMethod */

};
extern Il2CppType ContentTypeU5BU5D_t5687_0_0_0;
extern Il2CppType ContentTypeU5BU5D_t5687_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8656_ICollection_1_CopyTo_m49078_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ContentTypeU5BU5D_t5687_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49078_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49078_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8656_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8656_ICollection_1_CopyTo_m49078_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49078_GenericMethod/* genericMethod */

};
extern Il2CppType ContentType_t1604_0_0_0;
static ParameterInfo ICollection_1_t8656_ICollection_1_Remove_m49079_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContentType_t1604_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49079_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ContentType>::Remove(T)
MethodInfo ICollection_1_Remove_m49079_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8656_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Byte_t796/* invoker_method */
	, ICollection_1_t8656_ICollection_1_Remove_m49079_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49079_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8656_MethodInfos[] =
{
	&ICollection_1_get_Count_m49073_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49074_MethodInfo,
	&ICollection_1_Add_m49075_MethodInfo,
	&ICollection_1_Clear_m49076_MethodInfo,
	&ICollection_1_Contains_m49077_MethodInfo,
	&ICollection_1_CopyTo_m49078_MethodInfo,
	&ICollection_1_Remove_m49079_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8658_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8656_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8658_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8656_0_0_0;
extern Il2CppType ICollection_1_t8656_1_0_0;
struct ICollection_1_t8656;
extern Il2CppGenericClass ICollection_1_t8656_GenericClass;
TypeInfo ICollection_1_t8656_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8656_MethodInfos/* methods */
	, ICollection_1_t8656_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8656_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8656_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8656_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8656_0_0_0/* byval_arg */
	, &ICollection_1_t8656_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8656_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ContentType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ContentType>
extern Il2CppType IEnumerator_1_t6810_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49080_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ContentType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49080_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8658_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6810_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49080_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8658_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49080_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8658_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8658_0_0_0;
extern Il2CppType IEnumerable_1_t8658_1_0_0;
struct IEnumerable_1_t8658;
extern Il2CppGenericClass IEnumerable_1_t8658_GenericClass;
TypeInfo IEnumerable_1_t8658_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8658_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8658_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8658_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8658_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8658_0_0_0/* byval_arg */
	, &IEnumerable_1_t8658_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8658_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8657_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>
extern MethodInfo IList_1_get_Item_m49081_MethodInfo;
extern MethodInfo IList_1_set_Item_m49082_MethodInfo;
static PropertyInfo IList_1_t8657____Item_PropertyInfo = 
{
	&IList_1_t8657_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49081_MethodInfo/* get */
	, &IList_1_set_Item_m49082_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8657_PropertyInfos[] =
{
	&IList_1_t8657____Item_PropertyInfo,
	NULL
};
extern Il2CppType ContentType_t1604_0_0_0;
static ParameterInfo IList_1_t8657_IList_1_IndexOf_m49083_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ContentType_t1604_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49083_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49083_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8657_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Byte_t796/* invoker_method */
	, IList_1_t8657_IList_1_IndexOf_m49083_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49083_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ContentType_t1604_0_0_0;
static ParameterInfo IList_1_t8657_IList_1_Insert_m49084_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ContentType_t1604_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49084_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49084_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8657_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Byte_t796/* invoker_method */
	, IList_1_t8657_IList_1_Insert_m49084_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49084_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8657_IList_1_RemoveAt_m49085_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49085_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49085_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8657_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8657_IList_1_RemoveAt_m49085_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49085_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8657_IList_1_get_Item_m49081_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ContentType_t1604_0_0_0;
extern void* RuntimeInvoker_ContentType_t1604_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49081_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49081_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8657_il2cpp_TypeInfo/* declaring_type */
	, &ContentType_t1604_0_0_0/* return_type */
	, RuntimeInvoker_ContentType_t1604_Int32_t93/* invoker_method */
	, IList_1_t8657_IList_1_get_Item_m49081_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49081_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ContentType_t1604_0_0_0;
static ParameterInfo IList_1_t8657_IList_1_set_Item_m49082_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ContentType_t1604_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49082_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ContentType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49082_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8657_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Byte_t796/* invoker_method */
	, IList_1_t8657_IList_1_set_Item_m49082_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49082_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8657_MethodInfos[] =
{
	&IList_1_IndexOf_m49083_MethodInfo,
	&IList_1_Insert_m49084_MethodInfo,
	&IList_1_RemoveAt_m49085_MethodInfo,
	&IList_1_get_Item_m49081_MethodInfo,
	&IList_1_set_Item_m49082_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8657_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8656_il2cpp_TypeInfo,
	&IEnumerable_1_t8658_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8657_0_0_0;
extern Il2CppType IList_1_t8657_1_0_0;
struct IList_1_t8657;
extern Il2CppGenericClass IList_1_t8657_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8657_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8657_MethodInfos/* methods */
	, IList_1_t8657_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8657_il2cpp_TypeInfo/* element_class */
	, IList_1_t8657_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8657_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8657_0_0_0/* byval_arg */
	, &IList_1_t8657_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8657_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6812_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.ExchangeAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern MethodInfo IEnumerator_1_get_Current_m49086_MethodInfo;
static PropertyInfo IEnumerator_1_t6812____Current_PropertyInfo = 
{
	&IEnumerator_1_t6812_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49086_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6812_PropertyInfos[] =
{
	&IEnumerator_1_t6812____Current_PropertyInfo,
	NULL
};
extern Il2CppType ExchangeAlgorithmType_t1608_0_0_0;
extern void* RuntimeInvoker_ExchangeAlgorithmType_t1608 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49086_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49086_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6812_il2cpp_TypeInfo/* declaring_type */
	, &ExchangeAlgorithmType_t1608_0_0_0/* return_type */
	, RuntimeInvoker_ExchangeAlgorithmType_t1608/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49086_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6812_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49086_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6812_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6812_0_0_0;
extern Il2CppType IEnumerator_1_t6812_1_0_0;
struct IEnumerator_1_t6812;
extern Il2CppGenericClass IEnumerator_1_t6812_GenericClass;
TypeInfo IEnumerator_1_t6812_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6812_MethodInfos/* methods */
	, IEnumerator_1_t6812_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6812_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6812_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6812_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6812_0_0_0/* byval_arg */
	, &IEnumerator_1_t6812_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6812_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_537.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4915_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_537MethodDeclarations.h"

extern TypeInfo ExchangeAlgorithmType_t1608_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30161_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisExchangeAlgorithmType_t1608_m38759_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisExchangeAlgorithmType_t1608_m38759 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30157_MethodInfo;
 void InternalEnumerator_1__ctor_m30157 (InternalEnumerator_1_t4915 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30158_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30158 (InternalEnumerator_1_t4915 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30161(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30161_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ExchangeAlgorithmType_t1608_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30159_MethodInfo;
 void InternalEnumerator_1_Dispose_m30159 (InternalEnumerator_1_t4915 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30160_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30160 (InternalEnumerator_1_t4915 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30161 (InternalEnumerator_1_t4915 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisExchangeAlgorithmType_t1608_m38759(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisExchangeAlgorithmType_t1608_m38759_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4915____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4915_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4915, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4915____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4915_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4915, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4915_FieldInfos[] =
{
	&InternalEnumerator_1_t4915____array_0_FieldInfo,
	&InternalEnumerator_1_t4915____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4915____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4915_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30158_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4915____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4915_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30161_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4915_PropertyInfos[] =
{
	&InternalEnumerator_1_t4915____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4915____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4915_InternalEnumerator_1__ctor_m30157_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30157_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30157_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30157/* method */
	, &InternalEnumerator_1_t4915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4915_InternalEnumerator_1__ctor_m30157_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30157_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30158_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30158_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30158/* method */
	, &InternalEnumerator_1_t4915_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30158_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30159_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30159_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30159/* method */
	, &InternalEnumerator_1_t4915_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30159_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30160_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30160_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30160/* method */
	, &InternalEnumerator_1_t4915_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30160_GenericMethod/* genericMethod */

};
extern Il2CppType ExchangeAlgorithmType_t1608_0_0_0;
extern void* RuntimeInvoker_ExchangeAlgorithmType_t1608 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30161_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30161_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30161/* method */
	, &InternalEnumerator_1_t4915_il2cpp_TypeInfo/* declaring_type */
	, &ExchangeAlgorithmType_t1608_0_0_0/* return_type */
	, RuntimeInvoker_ExchangeAlgorithmType_t1608/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30161_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4915_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30157_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30158_MethodInfo,
	&InternalEnumerator_1_Dispose_m30159_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30160_MethodInfo,
	&InternalEnumerator_1_get_Current_m30161_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4915_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30158_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30160_MethodInfo,
	&InternalEnumerator_1_Dispose_m30159_MethodInfo,
	&InternalEnumerator_1_get_Current_m30161_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4915_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6812_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4915_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6812_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4915_0_0_0;
extern Il2CppType InternalEnumerator_1_t4915_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4915_GenericClass;
TypeInfo InternalEnumerator_1_t4915_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4915_MethodInfos/* methods */
	, InternalEnumerator_1_t4915_PropertyInfos/* properties */
	, InternalEnumerator_1_t4915_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4915_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4915_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4915_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4915_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4915_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4915_1_0_0/* this_arg */
	, InternalEnumerator_1_t4915_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4915_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4915)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8659_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern MethodInfo ICollection_1_get_Count_m49087_MethodInfo;
static PropertyInfo ICollection_1_t8659____Count_PropertyInfo = 
{
	&ICollection_1_t8659_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49087_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49088_MethodInfo;
static PropertyInfo ICollection_1_t8659____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8659_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49088_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8659_PropertyInfos[] =
{
	&ICollection_1_t8659____Count_PropertyInfo,
	&ICollection_1_t8659____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49087_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Count()
MethodInfo ICollection_1_get_Count_m49087_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8659_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49087_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49088_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49088_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8659_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49088_GenericMethod/* genericMethod */

};
extern Il2CppType ExchangeAlgorithmType_t1608_0_0_0;
extern Il2CppType ExchangeAlgorithmType_t1608_0_0_0;
static ParameterInfo ICollection_1_t8659_ICollection_1_Add_m49089_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmType_t1608_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49089_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Add(T)
MethodInfo ICollection_1_Add_m49089_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8659_ICollection_1_Add_m49089_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49089_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49090_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Clear()
MethodInfo ICollection_1_Clear_m49090_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49090_GenericMethod/* genericMethod */

};
extern Il2CppType ExchangeAlgorithmType_t1608_0_0_0;
static ParameterInfo ICollection_1_t8659_ICollection_1_Contains_m49091_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmType_t1608_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49091_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Contains(T)
MethodInfo ICollection_1_Contains_m49091_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8659_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8659_ICollection_1_Contains_m49091_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49091_GenericMethod/* genericMethod */

};
extern Il2CppType ExchangeAlgorithmTypeU5BU5D_t5688_0_0_0;
extern Il2CppType ExchangeAlgorithmTypeU5BU5D_t5688_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8659_ICollection_1_CopyTo_m49092_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmTypeU5BU5D_t5688_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49092_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49092_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8659_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8659_ICollection_1_CopyTo_m49092_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49092_GenericMethod/* genericMethod */

};
extern Il2CppType ExchangeAlgorithmType_t1608_0_0_0;
static ParameterInfo ICollection_1_t8659_ICollection_1_Remove_m49093_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmType_t1608_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49093_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Remove(T)
MethodInfo ICollection_1_Remove_m49093_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8659_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8659_ICollection_1_Remove_m49093_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49093_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8659_MethodInfos[] =
{
	&ICollection_1_get_Count_m49087_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49088_MethodInfo,
	&ICollection_1_Add_m49089_MethodInfo,
	&ICollection_1_Clear_m49090_MethodInfo,
	&ICollection_1_Contains_m49091_MethodInfo,
	&ICollection_1_CopyTo_m49092_MethodInfo,
	&ICollection_1_Remove_m49093_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8661_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8659_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8661_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8659_0_0_0;
extern Il2CppType ICollection_1_t8659_1_0_0;
struct ICollection_1_t8659;
extern Il2CppGenericClass ICollection_1_t8659_GenericClass;
TypeInfo ICollection_1_t8659_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8659_MethodInfos/* methods */
	, ICollection_1_t8659_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8659_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8659_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8659_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8659_0_0_0/* byval_arg */
	, &ICollection_1_t8659_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8659_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern Il2CppType IEnumerator_1_t6812_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49094_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49094_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8661_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6812_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49094_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8661_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49094_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8661_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8661_0_0_0;
extern Il2CppType IEnumerable_1_t8661_1_0_0;
struct IEnumerable_1_t8661;
extern Il2CppGenericClass IEnumerable_1_t8661_GenericClass;
TypeInfo IEnumerable_1_t8661_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8661_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8661_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8661_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8661_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8661_0_0_0/* byval_arg */
	, &IEnumerable_1_t8661_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8661_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8660_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>
extern MethodInfo IList_1_get_Item_m49095_MethodInfo;
extern MethodInfo IList_1_set_Item_m49096_MethodInfo;
static PropertyInfo IList_1_t8660____Item_PropertyInfo = 
{
	&IList_1_t8660_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49095_MethodInfo/* get */
	, &IList_1_set_Item_m49096_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8660_PropertyInfos[] =
{
	&IList_1_t8660____Item_PropertyInfo,
	NULL
};
extern Il2CppType ExchangeAlgorithmType_t1608_0_0_0;
static ParameterInfo IList_1_t8660_IList_1_IndexOf_m49097_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmType_t1608_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49097_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49097_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8660_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8660_IList_1_IndexOf_m49097_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49097_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ExchangeAlgorithmType_t1608_0_0_0;
static ParameterInfo IList_1_t8660_IList_1_Insert_m49098_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmType_t1608_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49098_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49098_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8660_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8660_IList_1_Insert_m49098_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49098_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8660_IList_1_RemoveAt_m49099_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49099_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49099_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8660_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8660_IList_1_RemoveAt_m49099_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49099_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8660_IList_1_get_Item_m49095_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ExchangeAlgorithmType_t1608_0_0_0;
extern void* RuntimeInvoker_ExchangeAlgorithmType_t1608_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49095_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49095_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8660_il2cpp_TypeInfo/* declaring_type */
	, &ExchangeAlgorithmType_t1608_0_0_0/* return_type */
	, RuntimeInvoker_ExchangeAlgorithmType_t1608_Int32_t93/* invoker_method */
	, IList_1_t8660_IList_1_get_Item_m49095_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49095_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ExchangeAlgorithmType_t1608_0_0_0;
static ParameterInfo IList_1_t8660_IList_1_set_Item_m49096_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ExchangeAlgorithmType_t1608_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49096_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.ExchangeAlgorithmType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49096_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8660_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8660_IList_1_set_Item_m49096_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49096_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8660_MethodInfos[] =
{
	&IList_1_IndexOf_m49097_MethodInfo,
	&IList_1_Insert_m49098_MethodInfo,
	&IList_1_RemoveAt_m49099_MethodInfo,
	&IList_1_get_Item_m49095_MethodInfo,
	&IList_1_set_Item_m49096_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8660_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8659_il2cpp_TypeInfo,
	&IEnumerable_1_t8661_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8660_0_0_0;
extern Il2CppType IList_1_t8660_1_0_0;
struct IList_1_t8660;
extern Il2CppGenericClass IList_1_t8660_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8660_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8660_MethodInfos/* methods */
	, IList_1_t8660_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8660_il2cpp_TypeInfo/* element_class */
	, IList_1_t8660_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8660_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8660_0_0_0/* byval_arg */
	, &IList_1_t8660_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8660_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6814_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.HandshakeState
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>
extern MethodInfo IEnumerator_1_get_Current_m49100_MethodInfo;
static PropertyInfo IEnumerator_1_t6814____Current_PropertyInfo = 
{
	&IEnumerator_1_t6814_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49100_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6814_PropertyInfos[] =
{
	&IEnumerator_1_t6814____Current_PropertyInfo,
	NULL
};
extern Il2CppType HandshakeState_t1609_0_0_0;
extern void* RuntimeInvoker_HandshakeState_t1609 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49100_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49100_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6814_il2cpp_TypeInfo/* declaring_type */
	, &HandshakeState_t1609_0_0_0/* return_type */
	, RuntimeInvoker_HandshakeState_t1609/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49100_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6814_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49100_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6814_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6814_0_0_0;
extern Il2CppType IEnumerator_1_t6814_1_0_0;
struct IEnumerator_1_t6814;
extern Il2CppGenericClass IEnumerator_1_t6814_GenericClass;
TypeInfo IEnumerator_1_t6814_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6814_MethodInfos/* methods */
	, IEnumerator_1_t6814_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6814_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6814_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6814_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6814_0_0_0/* byval_arg */
	, &IEnumerator_1_t6814_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6814_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_538.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4916_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_538MethodDeclarations.h"

extern TypeInfo HandshakeState_t1609_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30166_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHandshakeState_t1609_m38770_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.HandshakeState>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.HandshakeState>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisHandshakeState_t1609_m38770 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30162_MethodInfo;
 void InternalEnumerator_1__ctor_m30162 (InternalEnumerator_1_t4916 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30163_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30163 (InternalEnumerator_1_t4916 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30166(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30166_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&HandshakeState_t1609_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30164_MethodInfo;
 void InternalEnumerator_1_Dispose_m30164 (InternalEnumerator_1_t4916 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30165_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30165 (InternalEnumerator_1_t4916 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30166 (InternalEnumerator_1_t4916 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisHandshakeState_t1609_m38770(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisHandshakeState_t1609_m38770_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4916____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4916_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4916, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4916____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4916_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4916, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4916_FieldInfos[] =
{
	&InternalEnumerator_1_t4916____array_0_FieldInfo,
	&InternalEnumerator_1_t4916____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4916____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4916_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30163_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4916____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4916_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30166_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4916_PropertyInfos[] =
{
	&InternalEnumerator_1_t4916____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4916____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4916_InternalEnumerator_1__ctor_m30162_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30162_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30162_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30162/* method */
	, &InternalEnumerator_1_t4916_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4916_InternalEnumerator_1__ctor_m30162_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30162_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30163_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30163_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30163/* method */
	, &InternalEnumerator_1_t4916_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30163_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30164_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30164_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30164/* method */
	, &InternalEnumerator_1_t4916_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30164_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30165_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30165_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30165/* method */
	, &InternalEnumerator_1_t4916_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30165_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeState_t1609_0_0_0;
extern void* RuntimeInvoker_HandshakeState_t1609 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30166_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30166_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30166/* method */
	, &InternalEnumerator_1_t4916_il2cpp_TypeInfo/* declaring_type */
	, &HandshakeState_t1609_0_0_0/* return_type */
	, RuntimeInvoker_HandshakeState_t1609/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30166_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4916_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30162_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30163_MethodInfo,
	&InternalEnumerator_1_Dispose_m30164_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30165_MethodInfo,
	&InternalEnumerator_1_get_Current_m30166_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4916_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30163_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30165_MethodInfo,
	&InternalEnumerator_1_Dispose_m30164_MethodInfo,
	&InternalEnumerator_1_get_Current_m30166_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4916_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6814_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4916_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6814_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4916_0_0_0;
extern Il2CppType InternalEnumerator_1_t4916_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4916_GenericClass;
TypeInfo InternalEnumerator_1_t4916_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4916_MethodInfos/* methods */
	, InternalEnumerator_1_t4916_PropertyInfos/* properties */
	, InternalEnumerator_1_t4916_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4916_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4916_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4916_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4916_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4916_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4916_1_0_0/* this_arg */
	, InternalEnumerator_1_t4916_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4916_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4916)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8662_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>
extern MethodInfo ICollection_1_get_Count_m49101_MethodInfo;
static PropertyInfo ICollection_1_t8662____Count_PropertyInfo = 
{
	&ICollection_1_t8662_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49101_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49102_MethodInfo;
static PropertyInfo ICollection_1_t8662____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8662_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49102_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8662_PropertyInfos[] =
{
	&ICollection_1_t8662____Count_PropertyInfo,
	&ICollection_1_t8662____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49101_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Count()
MethodInfo ICollection_1_get_Count_m49101_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8662_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49101_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49102_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49102_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8662_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49102_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeState_t1609_0_0_0;
extern Il2CppType HandshakeState_t1609_0_0_0;
static ParameterInfo ICollection_1_t8662_ICollection_1_Add_m49103_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeState_t1609_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49103_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Add(T)
MethodInfo ICollection_1_Add_m49103_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8662_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8662_ICollection_1_Add_m49103_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49103_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49104_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Clear()
MethodInfo ICollection_1_Clear_m49104_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8662_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49104_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeState_t1609_0_0_0;
static ParameterInfo ICollection_1_t8662_ICollection_1_Contains_m49105_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeState_t1609_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49105_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Contains(T)
MethodInfo ICollection_1_Contains_m49105_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8662_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8662_ICollection_1_Contains_m49105_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49105_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeStateU5BU5D_t5689_0_0_0;
extern Il2CppType HandshakeStateU5BU5D_t5689_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8662_ICollection_1_CopyTo_m49106_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeStateU5BU5D_t5689_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49106_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49106_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8662_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8662_ICollection_1_CopyTo_m49106_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49106_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeState_t1609_0_0_0;
static ParameterInfo ICollection_1_t8662_ICollection_1_Remove_m49107_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeState_t1609_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49107_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HandshakeState>::Remove(T)
MethodInfo ICollection_1_Remove_m49107_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8662_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8662_ICollection_1_Remove_m49107_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49107_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8662_MethodInfos[] =
{
	&ICollection_1_get_Count_m49101_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49102_MethodInfo,
	&ICollection_1_Add_m49103_MethodInfo,
	&ICollection_1_Clear_m49104_MethodInfo,
	&ICollection_1_Contains_m49105_MethodInfo,
	&ICollection_1_CopyTo_m49106_MethodInfo,
	&ICollection_1_Remove_m49107_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8664_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8662_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8664_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8662_0_0_0;
extern Il2CppType ICollection_1_t8662_1_0_0;
struct ICollection_1_t8662;
extern Il2CppGenericClass ICollection_1_t8662_GenericClass;
TypeInfo ICollection_1_t8662_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8662_MethodInfos/* methods */
	, ICollection_1_t8662_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8662_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8662_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8662_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8662_0_0_0/* byval_arg */
	, &ICollection_1_t8662_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8662_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HandshakeState>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HandshakeState>
extern Il2CppType IEnumerator_1_t6814_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49108_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HandshakeState>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49108_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8664_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6814_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49108_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8664_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49108_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8664_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8664_0_0_0;
extern Il2CppType IEnumerable_1_t8664_1_0_0;
struct IEnumerable_1_t8664;
extern Il2CppGenericClass IEnumerable_1_t8664_GenericClass;
TypeInfo IEnumerable_1_t8664_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8664_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8664_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8664_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8664_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8664_0_0_0/* byval_arg */
	, &IEnumerable_1_t8664_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8664_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8663_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>
extern MethodInfo IList_1_get_Item_m49109_MethodInfo;
extern MethodInfo IList_1_set_Item_m49110_MethodInfo;
static PropertyInfo IList_1_t8663____Item_PropertyInfo = 
{
	&IList_1_t8663_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49109_MethodInfo/* get */
	, &IList_1_set_Item_m49110_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8663_PropertyInfos[] =
{
	&IList_1_t8663____Item_PropertyInfo,
	NULL
};
extern Il2CppType HandshakeState_t1609_0_0_0;
static ParameterInfo IList_1_t8663_IList_1_IndexOf_m49111_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeState_t1609_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49111_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49111_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8663_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8663_IList_1_IndexOf_m49111_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49111_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType HandshakeState_t1609_0_0_0;
static ParameterInfo IList_1_t8663_IList_1_Insert_m49112_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &HandshakeState_t1609_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49112_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49112_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8663_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8663_IList_1_Insert_m49112_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49112_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8663_IList_1_RemoveAt_m49113_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49113_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49113_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8663_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8663_IList_1_RemoveAt_m49113_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49113_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8663_IList_1_get_Item_m49109_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType HandshakeState_t1609_0_0_0;
extern void* RuntimeInvoker_HandshakeState_t1609_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49109_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49109_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8663_il2cpp_TypeInfo/* declaring_type */
	, &HandshakeState_t1609_0_0_0/* return_type */
	, RuntimeInvoker_HandshakeState_t1609_Int32_t93/* invoker_method */
	, IList_1_t8663_IList_1_get_Item_m49109_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49109_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType HandshakeState_t1609_0_0_0;
static ParameterInfo IList_1_t8663_IList_1_set_Item_m49110_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &HandshakeState_t1609_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49110_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HandshakeState>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49110_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8663_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8663_IList_1_set_Item_m49110_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49110_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8663_MethodInfos[] =
{
	&IList_1_IndexOf_m49111_MethodInfo,
	&IList_1_Insert_m49112_MethodInfo,
	&IList_1_RemoveAt_m49113_MethodInfo,
	&IList_1_get_Item_m49109_MethodInfo,
	&IList_1_set_Item_m49110_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8663_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8662_il2cpp_TypeInfo,
	&IEnumerable_1_t8664_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8663_0_0_0;
extern Il2CppType IList_1_t8663_1_0_0;
struct IList_1_t8663;
extern Il2CppGenericClass IList_1_t8663_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8663_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8663_MethodInfos/* methods */
	, IList_1_t8663_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8663_il2cpp_TypeInfo/* element_class */
	, IList_1_t8663_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8663_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8663_0_0_0/* byval_arg */
	, &IList_1_t8663_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8663_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6816_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.HashAlgorithmType
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern MethodInfo IEnumerator_1_get_Current_m49114_MethodInfo;
static PropertyInfo IEnumerator_1_t6816____Current_PropertyInfo = 
{
	&IEnumerator_1_t6816_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49114_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6816_PropertyInfos[] =
{
	&IEnumerator_1_t6816____Current_PropertyInfo,
	NULL
};
extern Il2CppType HashAlgorithmType_t1610_0_0_0;
extern void* RuntimeInvoker_HashAlgorithmType_t1610 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49114_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49114_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6816_il2cpp_TypeInfo/* declaring_type */
	, &HashAlgorithmType_t1610_0_0_0/* return_type */
	, RuntimeInvoker_HashAlgorithmType_t1610/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49114_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6816_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49114_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6816_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6816_0_0_0;
extern Il2CppType IEnumerator_1_t6816_1_0_0;
struct IEnumerator_1_t6816;
extern Il2CppGenericClass IEnumerator_1_t6816_GenericClass;
TypeInfo IEnumerator_1_t6816_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6816_MethodInfos/* methods */
	, IEnumerator_1_t6816_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6816_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6816_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6816_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6816_0_0_0/* byval_arg */
	, &IEnumerator_1_t6816_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6816_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_539.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4917_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_539MethodDeclarations.h"

extern TypeInfo HashAlgorithmType_t1610_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30171_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHashAlgorithmType_t1610_m38781_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.HashAlgorithmType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.HashAlgorithmType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisHashAlgorithmType_t1610_m38781 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30167_MethodInfo;
 void InternalEnumerator_1__ctor_m30167 (InternalEnumerator_1_t4917 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30168_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30168 (InternalEnumerator_1_t4917 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30171(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30171_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&HashAlgorithmType_t1610_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30169_MethodInfo;
 void InternalEnumerator_1_Dispose_m30169 (InternalEnumerator_1_t4917 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30170_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30170 (InternalEnumerator_1_t4917 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30171 (InternalEnumerator_1_t4917 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisHashAlgorithmType_t1610_m38781(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisHashAlgorithmType_t1610_m38781_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4917____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4917_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4917, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4917____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4917_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4917, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4917_FieldInfos[] =
{
	&InternalEnumerator_1_t4917____array_0_FieldInfo,
	&InternalEnumerator_1_t4917____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4917____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4917_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30168_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4917____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4917_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30171_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4917_PropertyInfos[] =
{
	&InternalEnumerator_1_t4917____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4917____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4917_InternalEnumerator_1__ctor_m30167_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30167_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30167_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30167/* method */
	, &InternalEnumerator_1_t4917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4917_InternalEnumerator_1__ctor_m30167_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30167_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30168_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30168_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30168/* method */
	, &InternalEnumerator_1_t4917_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30168_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30169_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30169_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30169/* method */
	, &InternalEnumerator_1_t4917_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30169_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30170_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30170_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30170/* method */
	, &InternalEnumerator_1_t4917_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30170_GenericMethod/* genericMethod */

};
extern Il2CppType HashAlgorithmType_t1610_0_0_0;
extern void* RuntimeInvoker_HashAlgorithmType_t1610 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30171_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30171_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30171/* method */
	, &InternalEnumerator_1_t4917_il2cpp_TypeInfo/* declaring_type */
	, &HashAlgorithmType_t1610_0_0_0/* return_type */
	, RuntimeInvoker_HashAlgorithmType_t1610/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30171_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4917_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30167_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30168_MethodInfo,
	&InternalEnumerator_1_Dispose_m30169_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30170_MethodInfo,
	&InternalEnumerator_1_get_Current_m30171_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4917_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30168_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30170_MethodInfo,
	&InternalEnumerator_1_Dispose_m30169_MethodInfo,
	&InternalEnumerator_1_get_Current_m30171_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4917_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6816_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4917_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6816_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4917_0_0_0;
extern Il2CppType InternalEnumerator_1_t4917_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4917_GenericClass;
TypeInfo InternalEnumerator_1_t4917_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4917_MethodInfos/* methods */
	, InternalEnumerator_1_t4917_PropertyInfos/* properties */
	, InternalEnumerator_1_t4917_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4917_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4917_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4917_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4917_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4917_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4917_1_0_0/* this_arg */
	, InternalEnumerator_1_t4917_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4917_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4917)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8665_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern MethodInfo ICollection_1_get_Count_m49115_MethodInfo;
static PropertyInfo ICollection_1_t8665____Count_PropertyInfo = 
{
	&ICollection_1_t8665_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49115_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49116_MethodInfo;
static PropertyInfo ICollection_1_t8665____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8665_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49116_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8665_PropertyInfos[] =
{
	&ICollection_1_t8665____Count_PropertyInfo,
	&ICollection_1_t8665____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49115_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Count()
MethodInfo ICollection_1_get_Count_m49115_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8665_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49115_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49116_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49116_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8665_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49116_GenericMethod/* genericMethod */

};
extern Il2CppType HashAlgorithmType_t1610_0_0_0;
extern Il2CppType HashAlgorithmType_t1610_0_0_0;
static ParameterInfo ICollection_1_t8665_ICollection_1_Add_m49117_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmType_t1610_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49117_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Add(T)
MethodInfo ICollection_1_Add_m49117_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8665_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8665_ICollection_1_Add_m49117_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49117_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49118_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Clear()
MethodInfo ICollection_1_Clear_m49118_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8665_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49118_GenericMethod/* genericMethod */

};
extern Il2CppType HashAlgorithmType_t1610_0_0_0;
static ParameterInfo ICollection_1_t8665_ICollection_1_Contains_m49119_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmType_t1610_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49119_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Contains(T)
MethodInfo ICollection_1_Contains_m49119_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8665_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8665_ICollection_1_Contains_m49119_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49119_GenericMethod/* genericMethod */

};
extern Il2CppType HashAlgorithmTypeU5BU5D_t5690_0_0_0;
extern Il2CppType HashAlgorithmTypeU5BU5D_t5690_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8665_ICollection_1_CopyTo_m49120_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmTypeU5BU5D_t5690_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49120_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49120_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8665_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8665_ICollection_1_CopyTo_m49120_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49120_GenericMethod/* genericMethod */

};
extern Il2CppType HashAlgorithmType_t1610_0_0_0;
static ParameterInfo ICollection_1_t8665_ICollection_1_Remove_m49121_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmType_t1610_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49121_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Remove(T)
MethodInfo ICollection_1_Remove_m49121_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8665_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8665_ICollection_1_Remove_m49121_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49121_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8665_MethodInfos[] =
{
	&ICollection_1_get_Count_m49115_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49116_MethodInfo,
	&ICollection_1_Add_m49117_MethodInfo,
	&ICollection_1_Clear_m49118_MethodInfo,
	&ICollection_1_Contains_m49119_MethodInfo,
	&ICollection_1_CopyTo_m49120_MethodInfo,
	&ICollection_1_Remove_m49121_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8667_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8665_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8667_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8665_0_0_0;
extern Il2CppType ICollection_1_t8665_1_0_0;
struct ICollection_1_t8665;
extern Il2CppGenericClass ICollection_1_t8665_GenericClass;
TypeInfo ICollection_1_t8665_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8665_MethodInfos/* methods */
	, ICollection_1_t8665_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8665_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8665_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8665_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8665_0_0_0/* byval_arg */
	, &ICollection_1_t8665_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8665_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern Il2CppType IEnumerator_1_t6816_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49122_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49122_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8667_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6816_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49122_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8667_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49122_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8667_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8667_0_0_0;
extern Il2CppType IEnumerable_1_t8667_1_0_0;
struct IEnumerable_1_t8667;
extern Il2CppGenericClass IEnumerable_1_t8667_GenericClass;
TypeInfo IEnumerable_1_t8667_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8667_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8667_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8667_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8667_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8667_0_0_0/* byval_arg */
	, &IEnumerable_1_t8667_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8667_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8666_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>
extern MethodInfo IList_1_get_Item_m49123_MethodInfo;
extern MethodInfo IList_1_set_Item_m49124_MethodInfo;
static PropertyInfo IList_1_t8666____Item_PropertyInfo = 
{
	&IList_1_t8666_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49123_MethodInfo/* get */
	, &IList_1_set_Item_m49124_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8666_PropertyInfos[] =
{
	&IList_1_t8666____Item_PropertyInfo,
	NULL
};
extern Il2CppType HashAlgorithmType_t1610_0_0_0;
static ParameterInfo IList_1_t8666_IList_1_IndexOf_m49125_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmType_t1610_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49125_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49125_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8666_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8666_IList_1_IndexOf_m49125_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49125_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType HashAlgorithmType_t1610_0_0_0;
static ParameterInfo IList_1_t8666_IList_1_Insert_m49126_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmType_t1610_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49126_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49126_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8666_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8666_IList_1_Insert_m49126_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49126_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8666_IList_1_RemoveAt_m49127_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49127_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49127_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8666_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8666_IList_1_RemoveAt_m49127_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49127_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8666_IList_1_get_Item_m49123_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType HashAlgorithmType_t1610_0_0_0;
extern void* RuntimeInvoker_HashAlgorithmType_t1610_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49123_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49123_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8666_il2cpp_TypeInfo/* declaring_type */
	, &HashAlgorithmType_t1610_0_0_0/* return_type */
	, RuntimeInvoker_HashAlgorithmType_t1610_Int32_t93/* invoker_method */
	, IList_1_t8666_IList_1_get_Item_m49123_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49123_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType HashAlgorithmType_t1610_0_0_0;
static ParameterInfo IList_1_t8666_IList_1_set_Item_m49124_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &HashAlgorithmType_t1610_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49124_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.HashAlgorithmType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49124_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8666_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8666_IList_1_set_Item_m49124_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49124_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8666_MethodInfos[] =
{
	&IList_1_IndexOf_m49125_MethodInfo,
	&IList_1_Insert_m49126_MethodInfo,
	&IList_1_RemoveAt_m49127_MethodInfo,
	&IList_1_get_Item_m49123_MethodInfo,
	&IList_1_set_Item_m49124_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8666_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8665_il2cpp_TypeInfo,
	&IEnumerable_1_t8667_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8666_0_0_0;
extern Il2CppType IList_1_t8666_1_0_0;
struct IList_1_t8666;
extern Il2CppGenericClass IList_1_t8666_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8666_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8666_MethodInfos/* methods */
	, IList_1_t8666_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8666_il2cpp_TypeInfo/* element_class */
	, IList_1_t8666_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8666_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8666_0_0_0/* byval_arg */
	, &IList_1_t8666_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8666_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6818_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.SecurityCompressionType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern MethodInfo IEnumerator_1_get_Current_m49128_MethodInfo;
static PropertyInfo IEnumerator_1_t6818____Current_PropertyInfo = 
{
	&IEnumerator_1_t6818_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49128_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6818_PropertyInfos[] =
{
	&IEnumerator_1_t6818____Current_PropertyInfo,
	NULL
};
extern Il2CppType SecurityCompressionType_t1622_0_0_0;
extern void* RuntimeInvoker_SecurityCompressionType_t1622 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49128_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49128_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6818_il2cpp_TypeInfo/* declaring_type */
	, &SecurityCompressionType_t1622_0_0_0/* return_type */
	, RuntimeInvoker_SecurityCompressionType_t1622/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49128_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6818_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49128_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6818_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6818_0_0_0;
extern Il2CppType IEnumerator_1_t6818_1_0_0;
struct IEnumerator_1_t6818;
extern Il2CppGenericClass IEnumerator_1_t6818_GenericClass;
TypeInfo IEnumerator_1_t6818_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6818_MethodInfos/* methods */
	, IEnumerator_1_t6818_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6818_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6818_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6818_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6818_0_0_0/* byval_arg */
	, &IEnumerator_1_t6818_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6818_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_540.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4918_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_540MethodDeclarations.h"

extern TypeInfo SecurityCompressionType_t1622_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30176_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSecurityCompressionType_t1622_m38792_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.SecurityCompressionType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.SecurityCompressionType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisSecurityCompressionType_t1622_m38792 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30172_MethodInfo;
 void InternalEnumerator_1__ctor_m30172 (InternalEnumerator_1_t4918 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30173_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30173 (InternalEnumerator_1_t4918 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30176(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30176_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&SecurityCompressionType_t1622_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30174_MethodInfo;
 void InternalEnumerator_1_Dispose_m30174 (InternalEnumerator_1_t4918 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30175_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30175 (InternalEnumerator_1_t4918 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30176 (InternalEnumerator_1_t4918 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisSecurityCompressionType_t1622_m38792(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisSecurityCompressionType_t1622_m38792_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4918____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4918_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4918, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4918____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4918_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4918, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4918_FieldInfos[] =
{
	&InternalEnumerator_1_t4918____array_0_FieldInfo,
	&InternalEnumerator_1_t4918____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4918____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4918_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30173_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4918____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4918_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30176_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4918_PropertyInfos[] =
{
	&InternalEnumerator_1_t4918____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4918____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4918_InternalEnumerator_1__ctor_m30172_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30172_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30172_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30172/* method */
	, &InternalEnumerator_1_t4918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4918_InternalEnumerator_1__ctor_m30172_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30172_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30173_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30173_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30173/* method */
	, &InternalEnumerator_1_t4918_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30173_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30174_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30174_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30174/* method */
	, &InternalEnumerator_1_t4918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30174_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30175_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30175_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30175/* method */
	, &InternalEnumerator_1_t4918_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30175_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityCompressionType_t1622_0_0_0;
extern void* RuntimeInvoker_SecurityCompressionType_t1622 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30176_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30176_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30176/* method */
	, &InternalEnumerator_1_t4918_il2cpp_TypeInfo/* declaring_type */
	, &SecurityCompressionType_t1622_0_0_0/* return_type */
	, RuntimeInvoker_SecurityCompressionType_t1622/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30176_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4918_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30172_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30173_MethodInfo,
	&InternalEnumerator_1_Dispose_m30174_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30175_MethodInfo,
	&InternalEnumerator_1_get_Current_m30176_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4918_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30173_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30175_MethodInfo,
	&InternalEnumerator_1_Dispose_m30174_MethodInfo,
	&InternalEnumerator_1_get_Current_m30176_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4918_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6818_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4918_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6818_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4918_0_0_0;
extern Il2CppType InternalEnumerator_1_t4918_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4918_GenericClass;
TypeInfo InternalEnumerator_1_t4918_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4918_MethodInfos/* methods */
	, InternalEnumerator_1_t4918_PropertyInfos/* properties */
	, InternalEnumerator_1_t4918_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4918_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4918_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4918_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4918_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4918_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4918_1_0_0/* this_arg */
	, InternalEnumerator_1_t4918_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4918_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4918)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8668_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern MethodInfo ICollection_1_get_Count_m49129_MethodInfo;
static PropertyInfo ICollection_1_t8668____Count_PropertyInfo = 
{
	&ICollection_1_t8668_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49129_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49130_MethodInfo;
static PropertyInfo ICollection_1_t8668____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8668_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49130_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8668_PropertyInfos[] =
{
	&ICollection_1_t8668____Count_PropertyInfo,
	&ICollection_1_t8668____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49129_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Count()
MethodInfo ICollection_1_get_Count_m49129_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8668_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49129_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49130_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49130_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8668_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49130_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityCompressionType_t1622_0_0_0;
extern Il2CppType SecurityCompressionType_t1622_0_0_0;
static ParameterInfo ICollection_1_t8668_ICollection_1_Add_m49131_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionType_t1622_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49131_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Add(T)
MethodInfo ICollection_1_Add_m49131_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8668_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8668_ICollection_1_Add_m49131_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49131_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49132_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Clear()
MethodInfo ICollection_1_Clear_m49132_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8668_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49132_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityCompressionType_t1622_0_0_0;
static ParameterInfo ICollection_1_t8668_ICollection_1_Contains_m49133_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionType_t1622_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49133_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Contains(T)
MethodInfo ICollection_1_Contains_m49133_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8668_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8668_ICollection_1_Contains_m49133_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49133_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityCompressionTypeU5BU5D_t5691_0_0_0;
extern Il2CppType SecurityCompressionTypeU5BU5D_t5691_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8668_ICollection_1_CopyTo_m49134_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionTypeU5BU5D_t5691_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49134_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49134_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8668_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8668_ICollection_1_CopyTo_m49134_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49134_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityCompressionType_t1622_0_0_0;
static ParameterInfo ICollection_1_t8668_ICollection_1_Remove_m49135_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionType_t1622_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49135_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Remove(T)
MethodInfo ICollection_1_Remove_m49135_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8668_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8668_ICollection_1_Remove_m49135_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49135_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8668_MethodInfos[] =
{
	&ICollection_1_get_Count_m49129_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49130_MethodInfo,
	&ICollection_1_Add_m49131_MethodInfo,
	&ICollection_1_Clear_m49132_MethodInfo,
	&ICollection_1_Contains_m49133_MethodInfo,
	&ICollection_1_CopyTo_m49134_MethodInfo,
	&ICollection_1_Remove_m49135_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8670_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8668_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8670_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8668_0_0_0;
extern Il2CppType ICollection_1_t8668_1_0_0;
struct ICollection_1_t8668;
extern Il2CppGenericClass ICollection_1_t8668_GenericClass;
TypeInfo ICollection_1_t8668_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8668_MethodInfos/* methods */
	, ICollection_1_t8668_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8668_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8668_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8668_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8668_0_0_0/* byval_arg */
	, &ICollection_1_t8668_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8668_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern Il2CppType IEnumerator_1_t6818_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49136_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49136_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8670_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6818_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49136_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8670_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49136_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8670_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8670_0_0_0;
extern Il2CppType IEnumerable_1_t8670_1_0_0;
struct IEnumerable_1_t8670;
extern Il2CppGenericClass IEnumerable_1_t8670_GenericClass;
TypeInfo IEnumerable_1_t8670_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8670_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8670_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8670_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8670_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8670_0_0_0/* byval_arg */
	, &IEnumerable_1_t8670_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8670_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8669_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>
extern MethodInfo IList_1_get_Item_m49137_MethodInfo;
extern MethodInfo IList_1_set_Item_m49138_MethodInfo;
static PropertyInfo IList_1_t8669____Item_PropertyInfo = 
{
	&IList_1_t8669_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49137_MethodInfo/* get */
	, &IList_1_set_Item_m49138_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8669_PropertyInfos[] =
{
	&IList_1_t8669____Item_PropertyInfo,
	NULL
};
extern Il2CppType SecurityCompressionType_t1622_0_0_0;
static ParameterInfo IList_1_t8669_IList_1_IndexOf_m49139_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionType_t1622_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49139_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49139_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8669_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8669_IList_1_IndexOf_m49139_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49139_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SecurityCompressionType_t1622_0_0_0;
static ParameterInfo IList_1_t8669_IList_1_Insert_m49140_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionType_t1622_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49140_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49140_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8669_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8669_IList_1_Insert_m49140_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49140_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8669_IList_1_RemoveAt_m49141_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49141_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49141_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8669_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8669_IList_1_RemoveAt_m49141_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49141_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8669_IList_1_get_Item_m49137_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType SecurityCompressionType_t1622_0_0_0;
extern void* RuntimeInvoker_SecurityCompressionType_t1622_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49137_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49137_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8669_il2cpp_TypeInfo/* declaring_type */
	, &SecurityCompressionType_t1622_0_0_0/* return_type */
	, RuntimeInvoker_SecurityCompressionType_t1622_Int32_t93/* invoker_method */
	, IList_1_t8669_IList_1_get_Item_m49137_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49137_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SecurityCompressionType_t1622_0_0_0;
static ParameterInfo IList_1_t8669_IList_1_set_Item_m49138_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SecurityCompressionType_t1622_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49138_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityCompressionType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49138_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8669_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8669_IList_1_set_Item_m49138_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49138_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8669_MethodInfos[] =
{
	&IList_1_IndexOf_m49139_MethodInfo,
	&IList_1_Insert_m49140_MethodInfo,
	&IList_1_RemoveAt_m49141_MethodInfo,
	&IList_1_get_Item_m49137_MethodInfo,
	&IList_1_set_Item_m49138_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8669_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8668_il2cpp_TypeInfo,
	&IEnumerable_1_t8670_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8669_0_0_0;
extern Il2CppType IList_1_t8669_1_0_0;
struct IList_1_t8669;
extern Il2CppGenericClass IList_1_t8669_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8669_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8669_MethodInfos/* methods */
	, IList_1_t8669_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8669_il2cpp_TypeInfo/* element_class */
	, IList_1_t8669_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8669_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8669_0_0_0/* byval_arg */
	, &IList_1_t8669_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8669_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6820_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.SecurityProtocolType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern MethodInfo IEnumerator_1_get_Current_m49142_MethodInfo;
static PropertyInfo IEnumerator_1_t6820____Current_PropertyInfo = 
{
	&IEnumerator_1_t6820_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49142_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6820_PropertyInfos[] =
{
	&IEnumerator_1_t6820____Current_PropertyInfo,
	NULL
};
extern Il2CppType SecurityProtocolType_t1623_0_0_0;
extern void* RuntimeInvoker_SecurityProtocolType_t1623 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49142_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49142_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6820_il2cpp_TypeInfo/* declaring_type */
	, &SecurityProtocolType_t1623_0_0_0/* return_type */
	, RuntimeInvoker_SecurityProtocolType_t1623/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49142_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6820_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49142_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6820_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6820_0_0_0;
extern Il2CppType IEnumerator_1_t6820_1_0_0;
struct IEnumerator_1_t6820;
extern Il2CppGenericClass IEnumerator_1_t6820_GenericClass;
TypeInfo IEnumerator_1_t6820_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6820_MethodInfos/* methods */
	, IEnumerator_1_t6820_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6820_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6820_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6820_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6820_0_0_0/* byval_arg */
	, &IEnumerator_1_t6820_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6820_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_541.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4919_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_541MethodDeclarations.h"

extern TypeInfo SecurityProtocolType_t1623_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30181_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSecurityProtocolType_t1623_m38803_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.SecurityProtocolType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.SecurityProtocolType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisSecurityProtocolType_t1623_m38803 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30177_MethodInfo;
 void InternalEnumerator_1__ctor_m30177 (InternalEnumerator_1_t4919 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30178_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30178 (InternalEnumerator_1_t4919 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30181(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30181_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&SecurityProtocolType_t1623_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30179_MethodInfo;
 void InternalEnumerator_1_Dispose_m30179 (InternalEnumerator_1_t4919 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30180_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30180 (InternalEnumerator_1_t4919 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30181 (InternalEnumerator_1_t4919 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisSecurityProtocolType_t1623_m38803(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisSecurityProtocolType_t1623_m38803_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4919____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4919_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4919, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4919____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4919_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4919, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4919_FieldInfos[] =
{
	&InternalEnumerator_1_t4919____array_0_FieldInfo,
	&InternalEnumerator_1_t4919____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4919____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4919_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30178_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4919____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4919_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30181_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4919_PropertyInfos[] =
{
	&InternalEnumerator_1_t4919____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4919____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4919_InternalEnumerator_1__ctor_m30177_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30177_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30177_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30177/* method */
	, &InternalEnumerator_1_t4919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4919_InternalEnumerator_1__ctor_m30177_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30177_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30178_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30178_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30178/* method */
	, &InternalEnumerator_1_t4919_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30178_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30179_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30179_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30179/* method */
	, &InternalEnumerator_1_t4919_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30179_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30180_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30180_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30180/* method */
	, &InternalEnumerator_1_t4919_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30180_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityProtocolType_t1623_0_0_0;
extern void* RuntimeInvoker_SecurityProtocolType_t1623 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30181_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30181_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30181/* method */
	, &InternalEnumerator_1_t4919_il2cpp_TypeInfo/* declaring_type */
	, &SecurityProtocolType_t1623_0_0_0/* return_type */
	, RuntimeInvoker_SecurityProtocolType_t1623/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30181_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4919_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30177_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30178_MethodInfo,
	&InternalEnumerator_1_Dispose_m30179_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30180_MethodInfo,
	&InternalEnumerator_1_get_Current_m30181_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4919_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30178_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30180_MethodInfo,
	&InternalEnumerator_1_Dispose_m30179_MethodInfo,
	&InternalEnumerator_1_get_Current_m30181_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4919_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6820_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4919_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6820_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4919_0_0_0;
extern Il2CppType InternalEnumerator_1_t4919_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4919_GenericClass;
TypeInfo InternalEnumerator_1_t4919_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4919_MethodInfos/* methods */
	, InternalEnumerator_1_t4919_PropertyInfos/* properties */
	, InternalEnumerator_1_t4919_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4919_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4919_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4919_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4919_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4919_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4919_1_0_0/* this_arg */
	, InternalEnumerator_1_t4919_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4919_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4919)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8671_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern MethodInfo ICollection_1_get_Count_m49143_MethodInfo;
static PropertyInfo ICollection_1_t8671____Count_PropertyInfo = 
{
	&ICollection_1_t8671_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49143_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49144_MethodInfo;
static PropertyInfo ICollection_1_t8671____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8671_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49144_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8671_PropertyInfos[] =
{
	&ICollection_1_t8671____Count_PropertyInfo,
	&ICollection_1_t8671____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49143_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Count()
MethodInfo ICollection_1_get_Count_m49143_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8671_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49143_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49144_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49144_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8671_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49144_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityProtocolType_t1623_0_0_0;
extern Il2CppType SecurityProtocolType_t1623_0_0_0;
static ParameterInfo ICollection_1_t8671_ICollection_1_Add_m49145_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolType_t1623_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49145_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Add(T)
MethodInfo ICollection_1_Add_m49145_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8671_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8671_ICollection_1_Add_m49145_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49145_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49146_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Clear()
MethodInfo ICollection_1_Clear_m49146_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8671_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49146_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityProtocolType_t1623_0_0_0;
static ParameterInfo ICollection_1_t8671_ICollection_1_Contains_m49147_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolType_t1623_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49147_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Contains(T)
MethodInfo ICollection_1_Contains_m49147_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8671_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8671_ICollection_1_Contains_m49147_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49147_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityProtocolTypeU5BU5D_t5692_0_0_0;
extern Il2CppType SecurityProtocolTypeU5BU5D_t5692_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8671_ICollection_1_CopyTo_m49148_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolTypeU5BU5D_t5692_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49148_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49148_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8671_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8671_ICollection_1_CopyTo_m49148_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49148_GenericMethod/* genericMethod */

};
extern Il2CppType SecurityProtocolType_t1623_0_0_0;
static ParameterInfo ICollection_1_t8671_ICollection_1_Remove_m49149_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolType_t1623_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49149_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Remove(T)
MethodInfo ICollection_1_Remove_m49149_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8671_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8671_ICollection_1_Remove_m49149_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49149_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8671_MethodInfos[] =
{
	&ICollection_1_get_Count_m49143_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49144_MethodInfo,
	&ICollection_1_Add_m49145_MethodInfo,
	&ICollection_1_Clear_m49146_MethodInfo,
	&ICollection_1_Contains_m49147_MethodInfo,
	&ICollection_1_CopyTo_m49148_MethodInfo,
	&ICollection_1_Remove_m49149_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8673_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8671_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8673_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8671_0_0_0;
extern Il2CppType ICollection_1_t8671_1_0_0;
struct ICollection_1_t8671;
extern Il2CppGenericClass ICollection_1_t8671_GenericClass;
TypeInfo ICollection_1_t8671_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8671_MethodInfos/* methods */
	, ICollection_1_t8671_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8671_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8671_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8671_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8671_0_0_0/* byval_arg */
	, &ICollection_1_t8671_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8671_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern Il2CppType IEnumerator_1_t6820_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49150_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49150_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8673_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6820_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49150_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8673_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49150_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8673_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8673_0_0_0;
extern Il2CppType IEnumerable_1_t8673_1_0_0;
struct IEnumerable_1_t8673;
extern Il2CppGenericClass IEnumerable_1_t8673_GenericClass;
TypeInfo IEnumerable_1_t8673_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8673_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8673_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8673_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8673_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8673_0_0_0/* byval_arg */
	, &IEnumerable_1_t8673_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8673_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8672_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
extern MethodInfo IList_1_get_Item_m49151_MethodInfo;
extern MethodInfo IList_1_set_Item_m49152_MethodInfo;
static PropertyInfo IList_1_t8672____Item_PropertyInfo = 
{
	&IList_1_t8672_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49151_MethodInfo/* get */
	, &IList_1_set_Item_m49152_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8672_PropertyInfos[] =
{
	&IList_1_t8672____Item_PropertyInfo,
	NULL
};
extern Il2CppType SecurityProtocolType_t1623_0_0_0;
static ParameterInfo IList_1_t8672_IList_1_IndexOf_m49153_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolType_t1623_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49153_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49153_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8672_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8672_IList_1_IndexOf_m49153_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49153_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SecurityProtocolType_t1623_0_0_0;
static ParameterInfo IList_1_t8672_IList_1_Insert_m49154_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolType_t1623_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49154_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49154_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8672_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8672_IList_1_Insert_m49154_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49154_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8672_IList_1_RemoveAt_m49155_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49155_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49155_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8672_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8672_IList_1_RemoveAt_m49155_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49155_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8672_IList_1_get_Item_m49151_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType SecurityProtocolType_t1623_0_0_0;
extern void* RuntimeInvoker_SecurityProtocolType_t1623_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49151_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49151_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8672_il2cpp_TypeInfo/* declaring_type */
	, &SecurityProtocolType_t1623_0_0_0/* return_type */
	, RuntimeInvoker_SecurityProtocolType_t1623_Int32_t93/* invoker_method */
	, IList_1_t8672_IList_1_get_Item_m49151_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49151_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SecurityProtocolType_t1623_0_0_0;
static ParameterInfo IList_1_t8672_IList_1_set_Item_m49152_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SecurityProtocolType_t1623_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49152_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49152_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8672_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8672_IList_1_set_Item_m49152_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49152_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8672_MethodInfos[] =
{
	&IList_1_IndexOf_m49153_MethodInfo,
	&IList_1_Insert_m49154_MethodInfo,
	&IList_1_RemoveAt_m49155_MethodInfo,
	&IList_1_get_Item_m49151_MethodInfo,
	&IList_1_set_Item_m49152_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8672_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8671_il2cpp_TypeInfo,
	&IEnumerable_1_t8673_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8672_0_0_0;
extern Il2CppType IList_1_t8672_1_0_0;
struct IList_1_t8672;
extern Il2CppGenericClass IList_1_t8672_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8672_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8672_MethodInfos/* methods */
	, IList_1_t8672_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8672_il2cpp_TypeInfo/* element_class */
	, IList_1_t8672_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8672_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8672_0_0_0/* byval_arg */
	, &IList_1_t8672_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8672_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6822_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern MethodInfo IEnumerator_1_get_Current_m49156_MethodInfo;
static PropertyInfo IEnumerator_1_t6822____Current_PropertyInfo = 
{
	&IEnumerator_1_t6822_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49156_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6822_PropertyInfos[] =
{
	&IEnumerator_1_t6822____Current_PropertyInfo,
	NULL
};
extern Il2CppType ClientCertificateType_t1634_0_0_0;
extern void* RuntimeInvoker_ClientCertificateType_t1634 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49156_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49156_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6822_il2cpp_TypeInfo/* declaring_type */
	, &ClientCertificateType_t1634_0_0_0/* return_type */
	, RuntimeInvoker_ClientCertificateType_t1634/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49156_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6822_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49156_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6822_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6822_0_0_0;
extern Il2CppType IEnumerator_1_t6822_1_0_0;
struct IEnumerator_1_t6822;
extern Il2CppGenericClass IEnumerator_1_t6822_GenericClass;
TypeInfo IEnumerator_1_t6822_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6822_MethodInfos/* methods */
	, IEnumerator_1_t6822_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6822_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6822_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6822_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6822_0_0_0/* byval_arg */
	, &IEnumerator_1_t6822_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6822_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_542.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4920_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_542MethodDeclarations.h"

extern TypeInfo ClientCertificateType_t1634_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30186_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisClientCertificateType_t1634_m38814_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisClientCertificateType_t1634_m38814 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30182_MethodInfo;
 void InternalEnumerator_1__ctor_m30182 (InternalEnumerator_1_t4920 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30183_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30183 (InternalEnumerator_1_t4920 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30186(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30186_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&ClientCertificateType_t1634_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30184_MethodInfo;
 void InternalEnumerator_1_Dispose_m30184 (InternalEnumerator_1_t4920 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30185_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30185 (InternalEnumerator_1_t4920 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30186 (InternalEnumerator_1_t4920 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisClientCertificateType_t1634_m38814(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisClientCertificateType_t1634_m38814_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4920____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4920_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4920, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4920____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4920_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4920, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4920_FieldInfos[] =
{
	&InternalEnumerator_1_t4920____array_0_FieldInfo,
	&InternalEnumerator_1_t4920____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4920____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4920_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30183_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4920____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4920_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30186_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4920_PropertyInfos[] =
{
	&InternalEnumerator_1_t4920____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4920____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4920_InternalEnumerator_1__ctor_m30182_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30182_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30182_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30182/* method */
	, &InternalEnumerator_1_t4920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4920_InternalEnumerator_1__ctor_m30182_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30182_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30183_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30183_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30183/* method */
	, &InternalEnumerator_1_t4920_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30183_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30184_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30184_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30184/* method */
	, &InternalEnumerator_1_t4920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30184_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30185_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30185_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30185/* method */
	, &InternalEnumerator_1_t4920_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30185_GenericMethod/* genericMethod */

};
extern Il2CppType ClientCertificateType_t1634_0_0_0;
extern void* RuntimeInvoker_ClientCertificateType_t1634 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30186_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30186_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30186/* method */
	, &InternalEnumerator_1_t4920_il2cpp_TypeInfo/* declaring_type */
	, &ClientCertificateType_t1634_0_0_0/* return_type */
	, RuntimeInvoker_ClientCertificateType_t1634/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30186_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4920_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30182_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30183_MethodInfo,
	&InternalEnumerator_1_Dispose_m30184_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30185_MethodInfo,
	&InternalEnumerator_1_get_Current_m30186_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4920_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30183_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30185_MethodInfo,
	&InternalEnumerator_1_Dispose_m30184_MethodInfo,
	&InternalEnumerator_1_get_Current_m30186_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4920_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6822_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4920_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6822_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4920_0_0_0;
extern Il2CppType InternalEnumerator_1_t4920_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4920_GenericClass;
TypeInfo InternalEnumerator_1_t4920_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4920_MethodInfos/* methods */
	, InternalEnumerator_1_t4920_PropertyInfos/* properties */
	, InternalEnumerator_1_t4920_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4920_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4920_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4920_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4920_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4920_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4920_1_0_0/* this_arg */
	, InternalEnumerator_1_t4920_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4920_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4920)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8674_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern MethodInfo ICollection_1_get_Count_m49157_MethodInfo;
static PropertyInfo ICollection_1_t8674____Count_PropertyInfo = 
{
	&ICollection_1_t8674_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49157_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49158_MethodInfo;
static PropertyInfo ICollection_1_t8674____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8674_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49158_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8674_PropertyInfos[] =
{
	&ICollection_1_t8674____Count_PropertyInfo,
	&ICollection_1_t8674____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49157_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Count()
MethodInfo ICollection_1_get_Count_m49157_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8674_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49157_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49158_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49158_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8674_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49158_GenericMethod/* genericMethod */

};
extern Il2CppType ClientCertificateType_t1634_0_0_0;
extern Il2CppType ClientCertificateType_t1634_0_0_0;
static ParameterInfo ICollection_1_t8674_ICollection_1_Add_m49159_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClientCertificateType_t1634_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49159_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Add(T)
MethodInfo ICollection_1_Add_m49159_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8674_ICollection_1_Add_m49159_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49159_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49160_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Clear()
MethodInfo ICollection_1_Clear_m49160_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49160_GenericMethod/* genericMethod */

};
extern Il2CppType ClientCertificateType_t1634_0_0_0;
static ParameterInfo ICollection_1_t8674_ICollection_1_Contains_m49161_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClientCertificateType_t1634_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49161_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Contains(T)
MethodInfo ICollection_1_Contains_m49161_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8674_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8674_ICollection_1_Contains_m49161_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49161_GenericMethod/* genericMethod */

};
extern Il2CppType ClientCertificateTypeU5BU5D_t1633_0_0_0;
extern Il2CppType ClientCertificateTypeU5BU5D_t1633_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8674_ICollection_1_CopyTo_m49162_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ClientCertificateTypeU5BU5D_t1633_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49162_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49162_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8674_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8674_ICollection_1_CopyTo_m49162_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49162_GenericMethod/* genericMethod */

};
extern Il2CppType ClientCertificateType_t1634_0_0_0;
static ParameterInfo ICollection_1_t8674_ICollection_1_Remove_m49163_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClientCertificateType_t1634_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49163_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Remove(T)
MethodInfo ICollection_1_Remove_m49163_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8674_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8674_ICollection_1_Remove_m49163_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49163_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8674_MethodInfos[] =
{
	&ICollection_1_get_Count_m49157_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49158_MethodInfo,
	&ICollection_1_Add_m49159_MethodInfo,
	&ICollection_1_Clear_m49160_MethodInfo,
	&ICollection_1_Contains_m49161_MethodInfo,
	&ICollection_1_CopyTo_m49162_MethodInfo,
	&ICollection_1_Remove_m49163_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8676_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8674_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8676_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8674_0_0_0;
extern Il2CppType ICollection_1_t8674_1_0_0;
struct ICollection_1_t8674;
extern Il2CppGenericClass ICollection_1_t8674_GenericClass;
TypeInfo ICollection_1_t8674_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8674_MethodInfos/* methods */
	, ICollection_1_t8674_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8674_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8674_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8674_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8674_0_0_0/* byval_arg */
	, &ICollection_1_t8674_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8674_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern Il2CppType IEnumerator_1_t6822_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49164_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49164_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8676_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6822_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49164_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8676_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49164_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8676_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8676_0_0_0;
extern Il2CppType IEnumerable_1_t8676_1_0_0;
struct IEnumerable_1_t8676;
extern Il2CppGenericClass IEnumerable_1_t8676_GenericClass;
TypeInfo IEnumerable_1_t8676_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8676_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8676_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8676_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8676_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8676_0_0_0/* byval_arg */
	, &IEnumerable_1_t8676_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8676_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8675_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>
extern MethodInfo IList_1_get_Item_m49165_MethodInfo;
extern MethodInfo IList_1_set_Item_m49166_MethodInfo;
static PropertyInfo IList_1_t8675____Item_PropertyInfo = 
{
	&IList_1_t8675_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49165_MethodInfo/* get */
	, &IList_1_set_Item_m49166_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8675_PropertyInfos[] =
{
	&IList_1_t8675____Item_PropertyInfo,
	NULL
};
extern Il2CppType ClientCertificateType_t1634_0_0_0;
static ParameterInfo IList_1_t8675_IList_1_IndexOf_m49167_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ClientCertificateType_t1634_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49167_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49167_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8675_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8675_IList_1_IndexOf_m49167_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49167_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ClientCertificateType_t1634_0_0_0;
static ParameterInfo IList_1_t8675_IList_1_Insert_m49168_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ClientCertificateType_t1634_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49168_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49168_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8675_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8675_IList_1_Insert_m49168_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49168_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8675_IList_1_RemoveAt_m49169_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49169_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49169_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8675_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8675_IList_1_RemoveAt_m49169_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49169_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8675_IList_1_get_Item_m49165_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ClientCertificateType_t1634_0_0_0;
extern void* RuntimeInvoker_ClientCertificateType_t1634_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49165_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49165_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8675_il2cpp_TypeInfo/* declaring_type */
	, &ClientCertificateType_t1634_0_0_0/* return_type */
	, RuntimeInvoker_ClientCertificateType_t1634_Int32_t93/* invoker_method */
	, IList_1_t8675_IList_1_get_Item_m49165_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49165_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ClientCertificateType_t1634_0_0_0;
static ParameterInfo IList_1_t8675_IList_1_set_Item_m49166_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ClientCertificateType_t1634_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49166_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49166_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8675_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8675_IList_1_set_Item_m49166_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49166_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8675_MethodInfos[] =
{
	&IList_1_IndexOf_m49167_MethodInfo,
	&IList_1_Insert_m49168_MethodInfo,
	&IList_1_RemoveAt_m49169_MethodInfo,
	&IList_1_get_Item_m49165_MethodInfo,
	&IList_1_set_Item_m49166_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8675_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8674_il2cpp_TypeInfo,
	&IEnumerable_1_t8676_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8675_0_0_0;
extern Il2CppType IList_1_t8675_1_0_0;
struct IList_1_t8675;
extern Il2CppGenericClass IList_1_t8675_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8675_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8675_MethodInfos/* methods */
	, IList_1_t8675_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8675_il2cpp_TypeInfo/* element_class */
	, IList_1_t8675_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8675_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8675_0_0_0/* byval_arg */
	, &IList_1_t8675_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8675_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6824_il2cpp_TypeInfo;

// Mono.Security.Protocol.Tls.Handshake.HandshakeType
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"


// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern MethodInfo IEnumerator_1_get_Current_m49170_MethodInfo;
static PropertyInfo IEnumerator_1_t6824____Current_PropertyInfo = 
{
	&IEnumerator_1_t6824_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49170_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6824_PropertyInfos[] =
{
	&IEnumerator_1_t6824____Current_PropertyInfo,
	NULL
};
extern Il2CppType HandshakeType_t1635_0_0_0;
extern void* RuntimeInvoker_HandshakeType_t1635 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49170_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49170_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6824_il2cpp_TypeInfo/* declaring_type */
	, &HandshakeType_t1635_0_0_0/* return_type */
	, RuntimeInvoker_HandshakeType_t1635/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49170_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6824_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49170_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6824_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6824_0_0_0;
extern Il2CppType IEnumerator_1_t6824_1_0_0;
struct IEnumerator_1_t6824;
extern Il2CppGenericClass IEnumerator_1_t6824_GenericClass;
TypeInfo IEnumerator_1_t6824_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6824_MethodInfos/* methods */
	, IEnumerator_1_t6824_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6824_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6824_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6824_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6824_0_0_0/* byval_arg */
	, &IEnumerator_1_t6824_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6824_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_543.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4921_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_543MethodDeclarations.h"

extern TypeInfo HandshakeType_t1635_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30191_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHandshakeType_t1635_m38825_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.HandshakeType>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.HandshakeType>(System.Int32)
 uint8_t Array_InternalArray__get_Item_TisHandshakeType_t1635_m38825 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30187_MethodInfo;
 void InternalEnumerator_1__ctor_m30187 (InternalEnumerator_1_t4921 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30188_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30188 (InternalEnumerator_1_t4921 * __this, MethodInfo* method){
	{
		uint8_t L_0 = InternalEnumerator_1_get_Current_m30191(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30191_MethodInfo);
		uint8_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&HandshakeType_t1635_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30189_MethodInfo;
 void InternalEnumerator_1_Dispose_m30189 (InternalEnumerator_1_t4921 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30190_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30190 (InternalEnumerator_1_t4921 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Current()
 uint8_t InternalEnumerator_1_get_Current_m30191 (InternalEnumerator_1_t4921 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		uint8_t L_8 = Array_InternalArray__get_Item_TisHandshakeType_t1635_m38825(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisHandshakeType_t1635_m38825_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4921____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4921_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4921, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4921____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4921_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4921, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4921_FieldInfos[] =
{
	&InternalEnumerator_1_t4921____array_0_FieldInfo,
	&InternalEnumerator_1_t4921____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4921____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4921_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30188_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4921____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4921_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30191_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4921_PropertyInfos[] =
{
	&InternalEnumerator_1_t4921____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4921____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4921_InternalEnumerator_1__ctor_m30187_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30187_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30187_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30187/* method */
	, &InternalEnumerator_1_t4921_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4921_InternalEnumerator_1__ctor_m30187_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30187_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30188_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30188_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30188/* method */
	, &InternalEnumerator_1_t4921_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30188_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30189_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30189_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30189/* method */
	, &InternalEnumerator_1_t4921_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30189_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30190_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30190_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30190/* method */
	, &InternalEnumerator_1_t4921_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30190_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeType_t1635_0_0_0;
extern void* RuntimeInvoker_HandshakeType_t1635 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30191_GenericMethod;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30191_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30191/* method */
	, &InternalEnumerator_1_t4921_il2cpp_TypeInfo/* declaring_type */
	, &HandshakeType_t1635_0_0_0/* return_type */
	, RuntimeInvoker_HandshakeType_t1635/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30191_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4921_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30187_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30188_MethodInfo,
	&InternalEnumerator_1_Dispose_m30189_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30190_MethodInfo,
	&InternalEnumerator_1_get_Current_m30191_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4921_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30188_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30190_MethodInfo,
	&InternalEnumerator_1_Dispose_m30189_MethodInfo,
	&InternalEnumerator_1_get_Current_m30191_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4921_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6824_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4921_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6824_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4921_0_0_0;
extern Il2CppType InternalEnumerator_1_t4921_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4921_GenericClass;
TypeInfo InternalEnumerator_1_t4921_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4921_MethodInfos/* methods */
	, InternalEnumerator_1_t4921_PropertyInfos/* properties */
	, InternalEnumerator_1_t4921_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4921_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4921_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4921_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4921_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4921_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4921_1_0_0/* this_arg */
	, InternalEnumerator_1_t4921_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4921_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4921)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8677_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern MethodInfo ICollection_1_get_Count_m49171_MethodInfo;
static PropertyInfo ICollection_1_t8677____Count_PropertyInfo = 
{
	&ICollection_1_t8677_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49171_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49172_MethodInfo;
static PropertyInfo ICollection_1_t8677____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8677_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49172_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8677_PropertyInfos[] =
{
	&ICollection_1_t8677____Count_PropertyInfo,
	&ICollection_1_t8677____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49171_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Count()
MethodInfo ICollection_1_get_Count_m49171_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8677_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49171_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49172_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49172_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8677_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49172_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeType_t1635_0_0_0;
extern Il2CppType HandshakeType_t1635_0_0_0;
static ParameterInfo ICollection_1_t8677_ICollection_1_Add_m49173_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeType_t1635_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49173_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Add(T)
MethodInfo ICollection_1_Add_m49173_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8677_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Byte_t796/* invoker_method */
	, ICollection_1_t8677_ICollection_1_Add_m49173_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49173_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49174_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Clear()
MethodInfo ICollection_1_Clear_m49174_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8677_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49174_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeType_t1635_0_0_0;
static ParameterInfo ICollection_1_t8677_ICollection_1_Contains_m49175_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeType_t1635_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49175_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Contains(T)
MethodInfo ICollection_1_Contains_m49175_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8677_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Byte_t796/* invoker_method */
	, ICollection_1_t8677_ICollection_1_Contains_m49175_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49175_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeTypeU5BU5D_t5693_0_0_0;
extern Il2CppType HandshakeTypeU5BU5D_t5693_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8677_ICollection_1_CopyTo_m49176_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeTypeU5BU5D_t5693_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49176_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49176_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8677_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8677_ICollection_1_CopyTo_m49176_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49176_GenericMethod/* genericMethod */

};
extern Il2CppType HandshakeType_t1635_0_0_0;
static ParameterInfo ICollection_1_t8677_ICollection_1_Remove_m49177_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeType_t1635_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49177_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Remove(T)
MethodInfo ICollection_1_Remove_m49177_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8677_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Byte_t796/* invoker_method */
	, ICollection_1_t8677_ICollection_1_Remove_m49177_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49177_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8677_MethodInfos[] =
{
	&ICollection_1_get_Count_m49171_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49172_MethodInfo,
	&ICollection_1_Add_m49173_MethodInfo,
	&ICollection_1_Clear_m49174_MethodInfo,
	&ICollection_1_Contains_m49175_MethodInfo,
	&ICollection_1_CopyTo_m49176_MethodInfo,
	&ICollection_1_Remove_m49177_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8679_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8677_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8679_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8677_0_0_0;
extern Il2CppType ICollection_1_t8677_1_0_0;
struct ICollection_1_t8677;
extern Il2CppGenericClass ICollection_1_t8677_GenericClass;
TypeInfo ICollection_1_t8677_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8677_MethodInfos/* methods */
	, ICollection_1_t8677_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8677_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8677_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8677_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8677_0_0_0/* byval_arg */
	, &ICollection_1_t8677_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8677_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern Il2CppType IEnumerator_1_t6824_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49178_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49178_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8679_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6824_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49178_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8679_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49178_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8679_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8679_0_0_0;
extern Il2CppType IEnumerable_1_t8679_1_0_0;
struct IEnumerable_1_t8679;
extern Il2CppGenericClass IEnumerable_1_t8679_GenericClass;
TypeInfo IEnumerable_1_t8679_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8679_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8679_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8679_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8679_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8679_0_0_0/* byval_arg */
	, &IEnumerable_1_t8679_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8679_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8678_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>
extern MethodInfo IList_1_get_Item_m49179_MethodInfo;
extern MethodInfo IList_1_set_Item_m49180_MethodInfo;
static PropertyInfo IList_1_t8678____Item_PropertyInfo = 
{
	&IList_1_t8678_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49179_MethodInfo/* get */
	, &IList_1_set_Item_m49180_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8678_PropertyInfos[] =
{
	&IList_1_t8678____Item_PropertyInfo,
	NULL
};
extern Il2CppType HandshakeType_t1635_0_0_0;
static ParameterInfo IList_1_t8678_IList_1_IndexOf_m49181_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HandshakeType_t1635_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49181_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49181_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8678_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Byte_t796/* invoker_method */
	, IList_1_t8678_IList_1_IndexOf_m49181_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49181_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType HandshakeType_t1635_0_0_0;
static ParameterInfo IList_1_t8678_IList_1_Insert_m49182_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &HandshakeType_t1635_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49182_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49182_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8678_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Byte_t796/* invoker_method */
	, IList_1_t8678_IList_1_Insert_m49182_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49182_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8678_IList_1_RemoveAt_m49183_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49183_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49183_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8678_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8678_IList_1_RemoveAt_m49183_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49183_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8678_IList_1_get_Item_m49179_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType HandshakeType_t1635_0_0_0;
extern void* RuntimeInvoker_HandshakeType_t1635_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49179_GenericMethod;
// T System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49179_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8678_il2cpp_TypeInfo/* declaring_type */
	, &HandshakeType_t1635_0_0_0/* return_type */
	, RuntimeInvoker_HandshakeType_t1635_Int32_t93/* invoker_method */
	, IList_1_t8678_IList_1_get_Item_m49179_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49179_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType HandshakeType_t1635_0_0_0;
static ParameterInfo IList_1_t8678_IList_1_set_Item_m49180_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &HandshakeType_t1635_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Byte_t796 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49180_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Mono.Security.Protocol.Tls.Handshake.HandshakeType>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49180_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8678_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Byte_t796/* invoker_method */
	, IList_1_t8678_IList_1_set_Item_m49180_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49180_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8678_MethodInfos[] =
{
	&IList_1_IndexOf_m49181_MethodInfo,
	&IList_1_Insert_m49182_MethodInfo,
	&IList_1_RemoveAt_m49183_MethodInfo,
	&IList_1_get_Item_m49179_MethodInfo,
	&IList_1_set_Item_m49180_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8678_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8677_il2cpp_TypeInfo,
	&IEnumerable_1_t8679_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8678_0_0_0;
extern Il2CppType IList_1_t8678_1_0_0;
struct IList_1_t8678;
extern Il2CppGenericClass IList_1_t8678_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8678_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8678_MethodInfos/* methods */
	, IList_1_t8678_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8678_il2cpp_TypeInfo/* element_class */
	, IList_1_t8678_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8678_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8678_0_0_0/* byval_arg */
	, &IList_1_t8678_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8678_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6826_il2cpp_TypeInfo;

// System.SerializableAttribute
#include "mscorlib_System_SerializableAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.SerializableAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.SerializableAttribute>
extern MethodInfo IEnumerator_1_get_Current_m49184_MethodInfo;
static PropertyInfo IEnumerator_1_t6826____Current_PropertyInfo = 
{
	&IEnumerator_1_t6826_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49184_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6826_PropertyInfos[] =
{
	&IEnumerator_1_t6826____Current_PropertyInfo,
	NULL
};
extern Il2CppType SerializableAttribute_t1688_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49184_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.SerializableAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49184_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6826_il2cpp_TypeInfo/* declaring_type */
	, &SerializableAttribute_t1688_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49184_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6826_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49184_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6826_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6826_0_0_0;
extern Il2CppType IEnumerator_1_t6826_1_0_0;
struct IEnumerator_1_t6826;
extern Il2CppGenericClass IEnumerator_1_t6826_GenericClass;
TypeInfo IEnumerator_1_t6826_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6826_MethodInfos/* methods */
	, IEnumerator_1_t6826_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6826_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6826_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6826_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6826_0_0_0/* byval_arg */
	, &IEnumerator_1_t6826_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6826_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.SerializableAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_544.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4922_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.SerializableAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_544MethodDeclarations.h"

extern TypeInfo SerializableAttribute_t1688_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30196_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSerializableAttribute_t1688_m38836_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.SerializableAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.SerializableAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisSerializableAttribute_t1688_m38836(__this, p0, method) (SerializableAttribute_t1688 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.SerializableAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.SerializableAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.SerializableAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.SerializableAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.SerializableAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.SerializableAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4922____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4922_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4922, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4922____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4922_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4922, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4922_FieldInfos[] =
{
	&InternalEnumerator_1_t4922____array_0_FieldInfo,
	&InternalEnumerator_1_t4922____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30193_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4922____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4922_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30193_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4922____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4922_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30196_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4922_PropertyInfos[] =
{
	&InternalEnumerator_1_t4922____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4922____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4922_InternalEnumerator_1__ctor_m30192_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30192_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.SerializableAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30192_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4922_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4922_InternalEnumerator_1__ctor_m30192_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30192_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30193_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.SerializableAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30193_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4922_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30193_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30194_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.SerializableAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30194_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4922_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30194_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30195_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.SerializableAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30195_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4922_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30195_GenericMethod/* genericMethod */

};
extern Il2CppType SerializableAttribute_t1688_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30196_GenericMethod;
// T System.Array/InternalEnumerator`1<System.SerializableAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30196_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4922_il2cpp_TypeInfo/* declaring_type */
	, &SerializableAttribute_t1688_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30196_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4922_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30192_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30193_MethodInfo,
	&InternalEnumerator_1_Dispose_m30194_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30195_MethodInfo,
	&InternalEnumerator_1_get_Current_m30196_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30195_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30194_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4922_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30193_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30195_MethodInfo,
	&InternalEnumerator_1_Dispose_m30194_MethodInfo,
	&InternalEnumerator_1_get_Current_m30196_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4922_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6826_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4922_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6826_il2cpp_TypeInfo, 7},
};
extern TypeInfo SerializableAttribute_t1688_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4922_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30196_MethodInfo/* Method Usage */,
	&SerializableAttribute_t1688_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisSerializableAttribute_t1688_m38836_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4922_0_0_0;
extern Il2CppType InternalEnumerator_1_t4922_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4922_GenericClass;
TypeInfo InternalEnumerator_1_t4922_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4922_MethodInfos/* methods */
	, InternalEnumerator_1_t4922_PropertyInfos/* properties */
	, InternalEnumerator_1_t4922_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4922_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4922_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4922_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4922_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4922_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4922_1_0_0/* this_arg */
	, InternalEnumerator_1_t4922_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4922_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4922_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4922)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8680_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.SerializableAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.SerializableAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.SerializableAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.SerializableAttribute>
extern MethodInfo ICollection_1_get_Count_m49185_MethodInfo;
static PropertyInfo ICollection_1_t8680____Count_PropertyInfo = 
{
	&ICollection_1_t8680_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49185_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49186_MethodInfo;
static PropertyInfo ICollection_1_t8680____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8680_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49186_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8680_PropertyInfos[] =
{
	&ICollection_1_t8680____Count_PropertyInfo,
	&ICollection_1_t8680____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49185_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.SerializableAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m49185_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8680_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49185_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49186_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.SerializableAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49186_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8680_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49186_GenericMethod/* genericMethod */

};
extern Il2CppType SerializableAttribute_t1688_0_0_0;
extern Il2CppType SerializableAttribute_t1688_0_0_0;
static ParameterInfo ICollection_1_t8680_ICollection_1_Add_m49187_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializableAttribute_t1688_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49187_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Add(T)
MethodInfo ICollection_1_Add_m49187_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8680_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8680_ICollection_1_Add_m49187_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49187_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49188_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Clear()
MethodInfo ICollection_1_Clear_m49188_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8680_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49188_GenericMethod/* genericMethod */

};
extern Il2CppType SerializableAttribute_t1688_0_0_0;
static ParameterInfo ICollection_1_t8680_ICollection_1_Contains_m49189_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializableAttribute_t1688_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49189_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m49189_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8680_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8680_ICollection_1_Contains_m49189_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49189_GenericMethod/* genericMethod */

};
extern Il2CppType SerializableAttributeU5BU5D_t5241_0_0_0;
extern Il2CppType SerializableAttributeU5BU5D_t5241_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8680_ICollection_1_CopyTo_m49190_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SerializableAttributeU5BU5D_t5241_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49190_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.SerializableAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49190_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8680_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8680_ICollection_1_CopyTo_m49190_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49190_GenericMethod/* genericMethod */

};
extern Il2CppType SerializableAttribute_t1688_0_0_0;
static ParameterInfo ICollection_1_t8680_ICollection_1_Remove_m49191_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializableAttribute_t1688_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49191_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.SerializableAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m49191_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8680_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8680_ICollection_1_Remove_m49191_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49191_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8680_MethodInfos[] =
{
	&ICollection_1_get_Count_m49185_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49186_MethodInfo,
	&ICollection_1_Add_m49187_MethodInfo,
	&ICollection_1_Clear_m49188_MethodInfo,
	&ICollection_1_Contains_m49189_MethodInfo,
	&ICollection_1_CopyTo_m49190_MethodInfo,
	&ICollection_1_Remove_m49191_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8682_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8680_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8682_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8680_0_0_0;
extern Il2CppType ICollection_1_t8680_1_0_0;
struct ICollection_1_t8680;
extern Il2CppGenericClass ICollection_1_t8680_GenericClass;
TypeInfo ICollection_1_t8680_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8680_MethodInfos/* methods */
	, ICollection_1_t8680_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8680_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8680_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8680_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8680_0_0_0/* byval_arg */
	, &ICollection_1_t8680_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8680_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.SerializableAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.SerializableAttribute>
extern Il2CppType IEnumerator_1_t6826_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49192_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.SerializableAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49192_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8682_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6826_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49192_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8682_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49192_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8682_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8682_0_0_0;
extern Il2CppType IEnumerable_1_t8682_1_0_0;
struct IEnumerable_1_t8682;
extern Il2CppGenericClass IEnumerable_1_t8682_GenericClass;
TypeInfo IEnumerable_1_t8682_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8682_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8682_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8682_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8682_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8682_0_0_0/* byval_arg */
	, &IEnumerable_1_t8682_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8682_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8681_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.SerializableAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.SerializableAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.SerializableAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.SerializableAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.SerializableAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.SerializableAttribute>
extern MethodInfo IList_1_get_Item_m49193_MethodInfo;
extern MethodInfo IList_1_set_Item_m49194_MethodInfo;
static PropertyInfo IList_1_t8681____Item_PropertyInfo = 
{
	&IList_1_t8681_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49193_MethodInfo/* get */
	, &IList_1_set_Item_m49194_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8681_PropertyInfos[] =
{
	&IList_1_t8681____Item_PropertyInfo,
	NULL
};
extern Il2CppType SerializableAttribute_t1688_0_0_0;
static ParameterInfo IList_1_t8681_IList_1_IndexOf_m49195_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SerializableAttribute_t1688_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49195_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.SerializableAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49195_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8681_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8681_IList_1_IndexOf_m49195_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49195_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SerializableAttribute_t1688_0_0_0;
static ParameterInfo IList_1_t8681_IList_1_Insert_m49196_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SerializableAttribute_t1688_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49196_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.SerializableAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49196_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8681_IList_1_Insert_m49196_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49196_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8681_IList_1_RemoveAt_m49197_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49197_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.SerializableAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49197_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8681_IList_1_RemoveAt_m49197_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49197_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8681_IList_1_get_Item_m49193_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType SerializableAttribute_t1688_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49193_GenericMethod;
// T System.Collections.Generic.IList`1<System.SerializableAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49193_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8681_il2cpp_TypeInfo/* declaring_type */
	, &SerializableAttribute_t1688_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8681_IList_1_get_Item_m49193_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49193_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SerializableAttribute_t1688_0_0_0;
static ParameterInfo IList_1_t8681_IList_1_set_Item_m49194_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SerializableAttribute_t1688_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49194_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.SerializableAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49194_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8681_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8681_IList_1_set_Item_m49194_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49194_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8681_MethodInfos[] =
{
	&IList_1_IndexOf_m49195_MethodInfo,
	&IList_1_Insert_m49196_MethodInfo,
	&IList_1_RemoveAt_m49197_MethodInfo,
	&IList_1_get_Item_m49193_MethodInfo,
	&IList_1_set_Item_m49194_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8681_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8680_il2cpp_TypeInfo,
	&IEnumerable_1_t8682_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8681_0_0_0;
extern Il2CppType IList_1_t8681_1_0_0;
struct IList_1_t8681;
extern Il2CppGenericClass IList_1_t8681_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8681_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8681_MethodInfos/* methods */
	, IList_1_t8681_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8681_il2cpp_TypeInfo/* element_class */
	, IList_1_t8681_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8681_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8681_0_0_0/* byval_arg */
	, &IList_1_t8681_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8681_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6828_il2cpp_TypeInfo;

// System.AttributeUsageAttribute
#include "mscorlib_System_AttributeUsageAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.AttributeUsageAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.AttributeUsageAttribute>
extern MethodInfo IEnumerator_1_get_Current_m49198_MethodInfo;
static PropertyInfo IEnumerator_1_t6828____Current_PropertyInfo = 
{
	&IEnumerator_1_t6828_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49198_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6828_PropertyInfos[] =
{
	&IEnumerator_1_t6828____Current_PropertyInfo,
	NULL
};
extern Il2CppType AttributeUsageAttribute_t1160_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49198_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.AttributeUsageAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49198_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6828_il2cpp_TypeInfo/* declaring_type */
	, &AttributeUsageAttribute_t1160_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49198_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6828_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49198_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6828_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6828_0_0_0;
extern Il2CppType IEnumerator_1_t6828_1_0_0;
struct IEnumerator_1_t6828;
extern Il2CppGenericClass IEnumerator_1_t6828_GenericClass;
TypeInfo IEnumerator_1_t6828_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6828_MethodInfos/* methods */
	, IEnumerator_1_t6828_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6828_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6828_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6828_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6828_0_0_0/* byval_arg */
	, &IEnumerator_1_t6828_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6828_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_545.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4923_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_545MethodDeclarations.h"

extern TypeInfo AttributeUsageAttribute_t1160_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30201_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAttributeUsageAttribute_t1160_m38847_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.AttributeUsageAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.AttributeUsageAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAttributeUsageAttribute_t1160_m38847(__this, p0, method) (AttributeUsageAttribute_t1160 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4923____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4923_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4923, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4923____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4923_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4923, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4923_FieldInfos[] =
{
	&InternalEnumerator_1_t4923____array_0_FieldInfo,
	&InternalEnumerator_1_t4923____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30198_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4923____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4923_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30198_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4923____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4923_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30201_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4923_PropertyInfos[] =
{
	&InternalEnumerator_1_t4923____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4923____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4923_InternalEnumerator_1__ctor_m30197_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30197_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30197_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4923_InternalEnumerator_1__ctor_m30197_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30197_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30198_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30198_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4923_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30198_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30199_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30199_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30199_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30200_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30200_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4923_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30200_GenericMethod/* genericMethod */

};
extern Il2CppType AttributeUsageAttribute_t1160_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30201_GenericMethod;
// T System.Array/InternalEnumerator`1<System.AttributeUsageAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30201_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4923_il2cpp_TypeInfo/* declaring_type */
	, &AttributeUsageAttribute_t1160_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30201_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4923_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30197_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30198_MethodInfo,
	&InternalEnumerator_1_Dispose_m30199_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30200_MethodInfo,
	&InternalEnumerator_1_get_Current_m30201_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30200_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30199_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4923_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30198_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30200_MethodInfo,
	&InternalEnumerator_1_Dispose_m30199_MethodInfo,
	&InternalEnumerator_1_get_Current_m30201_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4923_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6828_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4923_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6828_il2cpp_TypeInfo, 7},
};
extern TypeInfo AttributeUsageAttribute_t1160_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4923_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30201_MethodInfo/* Method Usage */,
	&AttributeUsageAttribute_t1160_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAttributeUsageAttribute_t1160_m38847_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4923_0_0_0;
extern Il2CppType InternalEnumerator_1_t4923_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4923_GenericClass;
TypeInfo InternalEnumerator_1_t4923_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4923_MethodInfos/* methods */
	, InternalEnumerator_1_t4923_PropertyInfos/* properties */
	, InternalEnumerator_1_t4923_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4923_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4923_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4923_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4923_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4923_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4923_1_0_0/* this_arg */
	, InternalEnumerator_1_t4923_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4923_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4923_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4923)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8683_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>
extern MethodInfo ICollection_1_get_Count_m49199_MethodInfo;
static PropertyInfo ICollection_1_t8683____Count_PropertyInfo = 
{
	&ICollection_1_t8683_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49199_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49200_MethodInfo;
static PropertyInfo ICollection_1_t8683____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8683_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49200_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8683_PropertyInfos[] =
{
	&ICollection_1_t8683____Count_PropertyInfo,
	&ICollection_1_t8683____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49199_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m49199_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8683_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49199_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49200_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49200_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8683_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49200_GenericMethod/* genericMethod */

};
extern Il2CppType AttributeUsageAttribute_t1160_0_0_0;
extern Il2CppType AttributeUsageAttribute_t1160_0_0_0;
static ParameterInfo ICollection_1_t8683_ICollection_1_Add_m49201_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AttributeUsageAttribute_t1160_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49201_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::Add(T)
MethodInfo ICollection_1_Add_m49201_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8683_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8683_ICollection_1_Add_m49201_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49201_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49202_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::Clear()
MethodInfo ICollection_1_Clear_m49202_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8683_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49202_GenericMethod/* genericMethod */

};
extern Il2CppType AttributeUsageAttribute_t1160_0_0_0;
static ParameterInfo ICollection_1_t8683_ICollection_1_Contains_m49203_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AttributeUsageAttribute_t1160_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49203_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m49203_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8683_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8683_ICollection_1_Contains_m49203_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49203_GenericMethod/* genericMethod */

};
extern Il2CppType AttributeUsageAttributeU5BU5D_t5242_0_0_0;
extern Il2CppType AttributeUsageAttributeU5BU5D_t5242_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8683_ICollection_1_CopyTo_m49204_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AttributeUsageAttributeU5BU5D_t5242_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49204_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49204_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8683_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8683_ICollection_1_CopyTo_m49204_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49204_GenericMethod/* genericMethod */

};
extern Il2CppType AttributeUsageAttribute_t1160_0_0_0;
static ParameterInfo ICollection_1_t8683_ICollection_1_Remove_m49205_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AttributeUsageAttribute_t1160_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49205_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.AttributeUsageAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m49205_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8683_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8683_ICollection_1_Remove_m49205_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49205_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8683_MethodInfos[] =
{
	&ICollection_1_get_Count_m49199_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49200_MethodInfo,
	&ICollection_1_Add_m49201_MethodInfo,
	&ICollection_1_Clear_m49202_MethodInfo,
	&ICollection_1_Contains_m49203_MethodInfo,
	&ICollection_1_CopyTo_m49204_MethodInfo,
	&ICollection_1_Remove_m49205_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8685_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8683_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8685_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8683_0_0_0;
extern Il2CppType ICollection_1_t8683_1_0_0;
struct ICollection_1_t8683;
extern Il2CppGenericClass ICollection_1_t8683_GenericClass;
TypeInfo ICollection_1_t8683_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8683_MethodInfos/* methods */
	, ICollection_1_t8683_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8683_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8683_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8683_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8683_0_0_0/* byval_arg */
	, &ICollection_1_t8683_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8683_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.AttributeUsageAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.AttributeUsageAttribute>
extern Il2CppType IEnumerator_1_t6828_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m49206_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.AttributeUsageAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m49206_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8685_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6828_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m49206_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8685_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m49206_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8685_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8685_0_0_0;
extern Il2CppType IEnumerable_1_t8685_1_0_0;
struct IEnumerable_1_t8685;
extern Il2CppGenericClass IEnumerable_1_t8685_GenericClass;
TypeInfo IEnumerable_1_t8685_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8685_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8685_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8685_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8685_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8685_0_0_0/* byval_arg */
	, &IEnumerable_1_t8685_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8685_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8684_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.AttributeUsageAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.AttributeUsageAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.AttributeUsageAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.AttributeUsageAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.AttributeUsageAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.AttributeUsageAttribute>
extern MethodInfo IList_1_get_Item_m49207_MethodInfo;
extern MethodInfo IList_1_set_Item_m49208_MethodInfo;
static PropertyInfo IList_1_t8684____Item_PropertyInfo = 
{
	&IList_1_t8684_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m49207_MethodInfo/* get */
	, &IList_1_set_Item_m49208_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8684_PropertyInfos[] =
{
	&IList_1_t8684____Item_PropertyInfo,
	NULL
};
extern Il2CppType AttributeUsageAttribute_t1160_0_0_0;
static ParameterInfo IList_1_t8684_IList_1_IndexOf_m49209_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AttributeUsageAttribute_t1160_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m49209_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.AttributeUsageAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m49209_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8684_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8684_IList_1_IndexOf_m49209_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m49209_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AttributeUsageAttribute_t1160_0_0_0;
static ParameterInfo IList_1_t8684_IList_1_Insert_m49210_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AttributeUsageAttribute_t1160_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m49210_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.AttributeUsageAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m49210_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8684_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8684_IList_1_Insert_m49210_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m49210_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8684_IList_1_RemoveAt_m49211_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m49211_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.AttributeUsageAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m49211_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8684_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8684_IList_1_RemoveAt_m49211_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m49211_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8684_IList_1_get_Item_m49207_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AttributeUsageAttribute_t1160_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m49207_GenericMethod;
// T System.Collections.Generic.IList`1<System.AttributeUsageAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m49207_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8684_il2cpp_TypeInfo/* declaring_type */
	, &AttributeUsageAttribute_t1160_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8684_IList_1_get_Item_m49207_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m49207_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AttributeUsageAttribute_t1160_0_0_0;
static ParameterInfo IList_1_t8684_IList_1_set_Item_m49208_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AttributeUsageAttribute_t1160_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m49208_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.AttributeUsageAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m49208_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8684_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8684_IList_1_set_Item_m49208_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m49208_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8684_MethodInfos[] =
{
	&IList_1_IndexOf_m49209_MethodInfo,
	&IList_1_Insert_m49210_MethodInfo,
	&IList_1_RemoveAt_m49211_MethodInfo,
	&IList_1_get_Item_m49207_MethodInfo,
	&IList_1_set_Item_m49208_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8684_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8683_il2cpp_TypeInfo,
	&IEnumerable_1_t8685_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8684_0_0_0;
extern Il2CppType IList_1_t8684_1_0_0;
struct IList_1_t8684;
extern Il2CppGenericClass IList_1_t8684_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8684_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8684_MethodInfos/* methods */
	, IList_1_t8684_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8684_il2cpp_TypeInfo/* element_class */
	, IList_1_t8684_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8684_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8684_0_0_0/* byval_arg */
	, &IList_1_t8684_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8684_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6830_il2cpp_TypeInfo;

// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>
extern MethodInfo IEnumerator_1_get_Current_m49212_MethodInfo;
static PropertyInfo IEnumerator_1_t6830____Current_PropertyInfo = 
{
	&IEnumerator_1_t6830_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m49212_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6830_PropertyInfos[] =
{
	&IEnumerator_1_t6830____Current_PropertyInfo,
	NULL
};
extern Il2CppType ComVisibleAttribute_t537_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m49212_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m49212_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6830_il2cpp_TypeInfo/* declaring_type */
	, &ComVisibleAttribute_t537_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m49212_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6830_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m49212_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6830_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6830_0_0_0;
extern Il2CppType IEnumerator_1_t6830_1_0_0;
struct IEnumerator_1_t6830;
extern Il2CppGenericClass IEnumerator_1_t6830_GenericClass;
TypeInfo IEnumerator_1_t6830_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6830_MethodInfos/* methods */
	, IEnumerator_1_t6830_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6830_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6830_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6830_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6830_0_0_0/* byval_arg */
	, &IEnumerator_1_t6830_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6830_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_546.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4924_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_546MethodDeclarations.h"

extern TypeInfo ComVisibleAttribute_t537_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30206_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisComVisibleAttribute_t537_m38858_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.ComVisibleAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices.ComVisibleAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisComVisibleAttribute_t537_m38858(__this, p0, method) (ComVisibleAttribute_t537 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4924____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4924_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4924, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4924____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4924_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4924, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4924_FieldInfos[] =
{
	&InternalEnumerator_1_t4924____array_0_FieldInfo,
	&InternalEnumerator_1_t4924____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30203_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4924____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4924_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30203_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4924____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4924_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30206_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4924_PropertyInfos[] =
{
	&InternalEnumerator_1_t4924____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4924____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4924_InternalEnumerator_1__ctor_m30202_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30202_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30202_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4924_InternalEnumerator_1__ctor_m30202_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30202_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30203_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30203_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4924_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30203_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30204_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30204_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30204_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30205_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30205_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4924_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30205_GenericMethod/* genericMethod */

};
extern Il2CppType ComVisibleAttribute_t537_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30206_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices.ComVisibleAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30206_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4924_il2cpp_TypeInfo/* declaring_type */
	, &ComVisibleAttribute_t537_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30206_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4924_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30202_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30203_MethodInfo,
	&InternalEnumerator_1_Dispose_m30204_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30205_MethodInfo,
	&InternalEnumerator_1_get_Current_m30206_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30205_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30204_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4924_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30203_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30205_MethodInfo,
	&InternalEnumerator_1_Dispose_m30204_MethodInfo,
	&InternalEnumerator_1_get_Current_m30206_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4924_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6830_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4924_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6830_il2cpp_TypeInfo, 7},
};
extern TypeInfo ComVisibleAttribute_t537_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4924_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30206_MethodInfo/* Method Usage */,
	&ComVisibleAttribute_t537_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisComVisibleAttribute_t537_m38858_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4924_0_0_0;
extern Il2CppType InternalEnumerator_1_t4924_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4924_GenericClass;
TypeInfo InternalEnumerator_1_t4924_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4924_MethodInfos/* methods */
	, InternalEnumerator_1_t4924_PropertyInfos/* properties */
	, InternalEnumerator_1_t4924_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4924_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4924_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4924_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4924_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4924_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4924_1_0_0/* this_arg */
	, InternalEnumerator_1_t4924_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4924_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4924_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4924)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8686_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>
extern MethodInfo ICollection_1_get_Count_m49213_MethodInfo;
static PropertyInfo ICollection_1_t8686____Count_PropertyInfo = 
{
	&ICollection_1_t8686_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m49213_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m49214_MethodInfo;
static PropertyInfo ICollection_1_t8686____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8686_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m49214_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8686_PropertyInfos[] =
{
	&ICollection_1_t8686____Count_PropertyInfo,
	&ICollection_1_t8686____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m49213_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m49213_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8686_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m49213_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m49214_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m49214_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8686_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m49214_GenericMethod/* genericMethod */

};
extern Il2CppType ComVisibleAttribute_t537_0_0_0;
extern Il2CppType ComVisibleAttribute_t537_0_0_0;
static ParameterInfo ICollection_1_t8686_ICollection_1_Add_m49215_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ComVisibleAttribute_t537_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m49215_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::Add(T)
MethodInfo ICollection_1_Add_m49215_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8686_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8686_ICollection_1_Add_m49215_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m49215_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m49216_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::Clear()
MethodInfo ICollection_1_Clear_m49216_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8686_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m49216_GenericMethod/* genericMethod */

};
extern Il2CppType ComVisibleAttribute_t537_0_0_0;
static ParameterInfo ICollection_1_t8686_ICollection_1_Contains_m49217_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ComVisibleAttribute_t537_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m49217_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m49217_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8686_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8686_ICollection_1_Contains_m49217_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m49217_GenericMethod/* genericMethod */

};
extern Il2CppType ComVisibleAttributeU5BU5D_t5243_0_0_0;
extern Il2CppType ComVisibleAttributeU5BU5D_t5243_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8686_ICollection_1_CopyTo_m49218_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ComVisibleAttributeU5BU5D_t5243_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m49218_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m49218_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8686_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8686_ICollection_1_CopyTo_m49218_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m49218_GenericMethod/* genericMethod */

};
extern Il2CppType ComVisibleAttribute_t537_0_0_0;
static ParameterInfo ICollection_1_t8686_ICollection_1_Remove_m49219_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ComVisibleAttribute_t537_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m49219_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices.ComVisibleAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m49219_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8686_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8686_ICollection_1_Remove_m49219_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m49219_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8686_MethodInfos[] =
{
	&ICollection_1_get_Count_m49213_MethodInfo,
	&ICollection_1_get_IsReadOnly_m49214_MethodInfo,
	&ICollection_1_Add_m49215_MethodInfo,
	&ICollection_1_Clear_m49216_MethodInfo,
	&ICollection_1_Contains_m49217_MethodInfo,
	&ICollection_1_CopyTo_m49218_MethodInfo,
	&ICollection_1_Remove_m49219_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8688_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8686_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8688_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8686_0_0_0;
extern Il2CppType ICollection_1_t8686_1_0_0;
struct ICollection_1_t8686;
extern Il2CppGenericClass ICollection_1_t8686_GenericClass;
TypeInfo ICollection_1_t8686_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8686_MethodInfos/* methods */
	, ICollection_1_t8686_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8686_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8686_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8686_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8686_0_0_0/* byval_arg */
	, &ICollection_1_t8686_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8686_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
