﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>
struct Enumerator_t3287;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3275;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void Enumerator__ctor_m17701_gshared (Enumerator_t3287 * __this, Dictionary_2_t3275 * ___host, MethodInfo* method);
#define Enumerator__ctor_m17701(__this, ___host, method) (void)Enumerator__ctor_m17701_gshared((Enumerator_t3287 *)__this, (Dictionary_2_t3275 *)___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17702_gshared (Enumerator_t3287 * __this, MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17702(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m17702_gshared((Enumerator_t3287 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::Dispose()
 void Enumerator_Dispose_m17703_gshared (Enumerator_t3287 * __this, MethodInfo* method);
#define Enumerator_Dispose_m17703(__this, method) (void)Enumerator_Dispose_m17703_gshared((Enumerator_t3287 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::MoveNext()
 bool Enumerator_MoveNext_m17704_gshared (Enumerator_t3287 * __this, MethodInfo* method);
#define Enumerator_MoveNext_m17704(__this, method) (bool)Enumerator_MoveNext_m17704_gshared((Enumerator_t3287 *)__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Object>::get_Current()
 Object_t * Enumerator_get_Current_m17705_gshared (Enumerator_t3287 * __this, MethodInfo* method);
#define Enumerator_get_Current_m17705(__this, method) (Object_t *)Enumerator_get_Current_m17705_gshared((Enumerator_t3287 *)__this, method)
