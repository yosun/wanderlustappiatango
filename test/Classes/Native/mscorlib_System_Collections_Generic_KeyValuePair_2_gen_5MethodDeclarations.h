﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct KeyValuePair_2_t3273;
// UnityEngine.Font
struct Font_t280;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t462;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"
#define KeyValuePair_2__ctor_m17720(__this, ___key, ___value, method) (void)KeyValuePair_2__ctor_m17641_gshared((KeyValuePair_2_t3281 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Key()
#define KeyValuePair_2_get_Key_m17721(__this, method) (Font_t280 *)KeyValuePair_2_get_Key_m17642_gshared((KeyValuePair_2_t3281 *)__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17722(__this, ___value, method) (void)KeyValuePair_2_set_Key_m17643_gshared((KeyValuePair_2_t3281 *)__this, (Object_t *)___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Value()
#define KeyValuePair_2_get_Value_m17723(__this, method) (List_1_t462 *)KeyValuePair_2_get_Value_m17644_gshared((KeyValuePair_2_t3281 *)__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17724(__this, ___value, method) (void)KeyValuePair_2_set_Value_m17645_gshared((KeyValuePair_2_t3281 *)__this, (Object_t *)___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::ToString()
#define KeyValuePair_2_ToString_m17725(__this, method) (String_t*)KeyValuePair_2_ToString_m17646_gshared((KeyValuePair_2_t3281 *)__this, method)
