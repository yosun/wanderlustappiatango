﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Activation.ContextLevelActivator
struct ContextLevelActivator_t1985;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t1980;

// System.Void System.Runtime.Remoting.Activation.ContextLevelActivator::.ctor(System.Runtime.Remoting.Activation.IActivator)
 void ContextLevelActivator__ctor_m11445 (ContextLevelActivator_t1985 * __this, Object_t * ___next, MethodInfo* method) IL2CPP_METHOD_ATTR;
