﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Pointer
struct Pointer_t1949;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1066;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Reflection.Pointer::.ctor()
 void Pointer__ctor_m11382 (Pointer_t1949 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Reflection.Pointer::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m11383 (Pointer_t1949 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
