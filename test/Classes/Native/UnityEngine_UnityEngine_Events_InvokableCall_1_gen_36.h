﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingBehaviour>
struct UnityAction_1_t2959;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>
struct InvokableCall_1_t2958  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingBehaviour>::Delegate
	UnityAction_1_t2959 * ___Delegate_0;
};
