﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t455;

// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::.ctor()
 void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m19652_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m19652(__this, method) (void)U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m19652_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 *)__this, method)
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
 Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m19653_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m19653(__this, method) (Object_t *)U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m19653_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 *)__this, method)
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
 Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m19654_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m19654(__this, method) (Object_t *)U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m19654_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 *)__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m19655_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m19655(__this, method) (Object_t *)U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m19655_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 *)__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
 Object_t* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m19656_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m19656(__this, method) (Object_t*)U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m19656_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 *)__this, method)
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::MoveNext()
 bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m19657_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m19657(__this, method) (bool)U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m19657_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 *)__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Dispose()
 void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m19658_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 * __this, MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m19658(__this, method) (void)U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m19658_gshared((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t3546 *)__this, method)
