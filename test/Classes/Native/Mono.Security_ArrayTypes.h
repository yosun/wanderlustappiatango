﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct BigIntegerU5BU5D_t1540  : public Array_t
{
};
struct BigIntegerU5BU5D_t1540_StaticFields{
};
// Mono.Math.BigInteger/Sign[]
// Mono.Math.BigInteger/Sign[]
struct SignU5BU5D_t5679  : public Array_t
{
};
// Mono.Math.Prime.ConfidenceFactor[]
// Mono.Math.Prime.ConfidenceFactor[]
struct ConfidenceFactorU5BU5D_t5680  : public Array_t
{
};
// Mono.Security.X509.X509ChainStatusFlags[]
// Mono.Security.X509.X509ChainStatusFlags[]
struct X509ChainStatusFlagsU5BU5D_t5681  : public Array_t
{
};
// Mono.Security.X509.Extensions.KeyUsages[]
// Mono.Security.X509.Extensions.KeyUsages[]
struct KeyUsagesU5BU5D_t5682  : public Array_t
{
};
// Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes[]
// Mono.Security.X509.Extensions.NetscapeCertTypeExtension/CertTypes[]
struct CertTypesU5BU5D_t5683  : public Array_t
{
};
// Mono.Security.Protocol.Tls.AlertLevel[]
// Mono.Security.Protocol.Tls.AlertLevel[]
struct AlertLevelU5BU5D_t5684  : public Array_t
{
};
// Mono.Security.Protocol.Tls.AlertDescription[]
// Mono.Security.Protocol.Tls.AlertDescription[]
struct AlertDescriptionU5BU5D_t5685  : public Array_t
{
};
// Mono.Security.Protocol.Tls.CipherAlgorithmType[]
// Mono.Security.Protocol.Tls.CipherAlgorithmType[]
struct CipherAlgorithmTypeU5BU5D_t5686  : public Array_t
{
};
// Mono.Security.Protocol.Tls.ContentType[]
// Mono.Security.Protocol.Tls.ContentType[]
struct ContentTypeU5BU5D_t5687  : public Array_t
{
};
// Mono.Security.Protocol.Tls.ExchangeAlgorithmType[]
// Mono.Security.Protocol.Tls.ExchangeAlgorithmType[]
struct ExchangeAlgorithmTypeU5BU5D_t5688  : public Array_t
{
};
// Mono.Security.Protocol.Tls.HandshakeState[]
// Mono.Security.Protocol.Tls.HandshakeState[]
struct HandshakeStateU5BU5D_t5689  : public Array_t
{
};
// Mono.Security.Protocol.Tls.HashAlgorithmType[]
// Mono.Security.Protocol.Tls.HashAlgorithmType[]
struct HashAlgorithmTypeU5BU5D_t5690  : public Array_t
{
};
// Mono.Security.Protocol.Tls.SecurityCompressionType[]
// Mono.Security.Protocol.Tls.SecurityCompressionType[]
struct SecurityCompressionTypeU5BU5D_t5691  : public Array_t
{
};
// Mono.Security.Protocol.Tls.SecurityProtocolType[]
// Mono.Security.Protocol.Tls.SecurityProtocolType[]
struct SecurityProtocolTypeU5BU5D_t5692  : public Array_t
{
};
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[]
// Mono.Security.Protocol.Tls.Handshake.ClientCertificateType[]
struct ClientCertificateTypeU5BU5D_t1633  : public Array_t
{
};
// Mono.Security.Protocol.Tls.Handshake.HandshakeType[]
// Mono.Security.Protocol.Tls.Handshake.HandshakeType[]
struct HandshakeTypeU5BU5D_t5693  : public Array_t
{
};
