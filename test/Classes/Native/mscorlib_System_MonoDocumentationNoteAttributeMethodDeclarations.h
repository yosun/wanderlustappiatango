﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoDocumentationNoteAttribute
struct MonoDocumentationNoteAttribute_t1724;
// System.String
struct String_t;

// System.Void System.MonoDocumentationNoteAttribute::.ctor(System.String)
 void MonoDocumentationNoteAttribute__ctor_m9755 (MonoDocumentationNoteAttribute_t1724 * __this, String_t* ___comment, MethodInfo* method) IL2CPP_METHOD_ATTR;
