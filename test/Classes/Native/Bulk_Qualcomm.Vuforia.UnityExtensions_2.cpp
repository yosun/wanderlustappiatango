﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// Vuforia.QCARRuntimeUtilities/WebCamUsed
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitie_0.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo WebCamUsed_t763_il2cpp_TypeInfo;
// Vuforia.QCARRuntimeUtilities/WebCamUsed
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitie_0MethodDeclarations.h"


// System.Array
#include "mscorlib_System_Array.h"

// Metadata Definition Vuforia.QCARRuntimeUtilities/WebCamUsed
extern Il2CppType Int32_t93_0_0_1542;
FieldInfo WebCamUsed_t763____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t93_0_0_1542/* type */
	, &WebCamUsed_t763_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamUsed_t763, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType WebCamUsed_t763_0_0_32854;
FieldInfo WebCamUsed_t763____UNKNOWN_2_FieldInfo = 
{
	"UNKNOWN"/* name */
	, &WebCamUsed_t763_0_0_32854/* type */
	, &WebCamUsed_t763_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType WebCamUsed_t763_0_0_32854;
FieldInfo WebCamUsed_t763____TRUE_3_FieldInfo = 
{
	"TRUE"/* name */
	, &WebCamUsed_t763_0_0_32854/* type */
	, &WebCamUsed_t763_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType WebCamUsed_t763_0_0_32854;
FieldInfo WebCamUsed_t763____FALSE_4_FieldInfo = 
{
	"FALSE"/* name */
	, &WebCamUsed_t763_0_0_32854/* type */
	, &WebCamUsed_t763_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* WebCamUsed_t763_FieldInfos[] =
{
	&WebCamUsed_t763____value___1_FieldInfo,
	&WebCamUsed_t763____UNKNOWN_2_FieldInfo,
	&WebCamUsed_t763____TRUE_3_FieldInfo,
	&WebCamUsed_t763____FALSE_4_FieldInfo,
	NULL
};
static const int32_t WebCamUsed_t763____UNKNOWN_2_DefaultValueData = 0;
extern Il2CppType Int32_t93_0_0_0;
static Il2CppFieldDefaultValueEntry WebCamUsed_t763____UNKNOWN_2_DefaultValue = 
{
	&WebCamUsed_t763____UNKNOWN_2_FieldInfo/* field */
	, { (char*)&WebCamUsed_t763____UNKNOWN_2_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t WebCamUsed_t763____TRUE_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry WebCamUsed_t763____TRUE_3_DefaultValue = 
{
	&WebCamUsed_t763____TRUE_3_FieldInfo/* field */
	, { (char*)&WebCamUsed_t763____TRUE_3_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t WebCamUsed_t763____FALSE_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry WebCamUsed_t763____FALSE_4_DefaultValue = 
{
	&WebCamUsed_t763____FALSE_4_FieldInfo/* field */
	, { (char*)&WebCamUsed_t763____FALSE_4_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* WebCamUsed_t763_FieldDefaultValues[] = 
{
	&WebCamUsed_t763____UNKNOWN_2_DefaultValue,
	&WebCamUsed_t763____TRUE_3_DefaultValue,
	&WebCamUsed_t763____FALSE_4_DefaultValue,
	NULL
};
static MethodInfo* WebCamUsed_t763_MethodInfos[] =
{
	NULL
};
extern MethodInfo Enum_Equals_m191_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo Enum_GetHashCode_m193_MethodInfo;
extern MethodInfo Enum_ToString_m194_MethodInfo;
extern MethodInfo Enum_ToString_m195_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToBoolean_m196_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToByte_m197_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToChar_m198_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDateTime_m199_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDecimal_m200_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToDouble_m201_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt16_m202_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt32_m203_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToInt64_m204_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSByte_m205_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToSingle_m206_MethodInfo;
extern MethodInfo Enum_ToString_m207_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToType_m208_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt16_m209_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt32_m210_MethodInfo;
extern MethodInfo Enum_System_IConvertible_ToUInt64_m211_MethodInfo;
extern MethodInfo Enum_CompareTo_m212_MethodInfo;
extern MethodInfo Enum_GetTypeCode_m213_MethodInfo;
static MethodInfo* WebCamUsed_t763_VTable[] =
{
	&Enum_Equals_m191_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Enum_GetHashCode_m193_MethodInfo,
	&Enum_ToString_m194_MethodInfo,
	&Enum_ToString_m195_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m196_MethodInfo,
	&Enum_System_IConvertible_ToByte_m197_MethodInfo,
	&Enum_System_IConvertible_ToChar_m198_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m199_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m200_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m201_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m202_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m203_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m204_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m205_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m206_MethodInfo,
	&Enum_ToString_m207_MethodInfo,
	&Enum_System_IConvertible_ToType_m208_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m209_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m210_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m211_MethodInfo,
	&Enum_CompareTo_m212_MethodInfo,
	&Enum_GetTypeCode_m213_MethodInfo,
};
extern TypeInfo IFormattable_t94_il2cpp_TypeInfo;
extern TypeInfo IConvertible_t95_il2cpp_TypeInfo;
extern TypeInfo IComparable_t96_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair WebCamUsed_t763_InterfacesOffsets[] = 
{
	{ &IFormattable_t94_il2cpp_TypeInfo, 4},
	{ &IConvertible_t95_il2cpp_TypeInfo, 5},
	{ &IComparable_t96_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WebCamUsed_t763_0_0_0;
extern Il2CppType WebCamUsed_t763_1_0_0;
extern TypeInfo Enum_t97_il2cpp_TypeInfo;
// System.Int32
#include "mscorlib_System_Int32.h"
extern TypeInfo Int32_t93_il2cpp_TypeInfo;
extern TypeInfo QCARRuntimeUtilities_t157_il2cpp_TypeInfo;
TypeInfo WebCamUsed_t763_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebCamUsed"/* name */
	, ""/* namespaze */
	, WebCamUsed_t763_MethodInfos/* methods */
	, NULL/* properties */
	, WebCamUsed_t763_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t97_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* nested_in */
	, &Int32_t93_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, WebCamUsed_t763_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &WebCamUsed_t763_0_0_0/* byval_arg */
	, &WebCamUsed_t763_1_0_0/* this_arg */
	, WebCamUsed_t763_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, WebCamUsed_t763_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebCamUsed_t763)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 259/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// Vuforia.QCARRuntimeUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitie.h"
#ifndef _MSC_VER
#else
#endif
// Vuforia.QCARRuntimeUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitieMethodDeclarations.h"

// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.String
#include "mscorlib_System_String.h"
#include "mscorlib_ArrayTypes.h"
// System.Char
#include "mscorlib_System_Char.h"
// System.Void
#include "mscorlib_System_Void.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviour.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "UnityEngine_ArrayTypes.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehav.h"
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"
// System.Single
#include "mscorlib_System_Single.h"
// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBox.h"
extern TypeInfo SurfaceUtilities_t137_il2cpp_TypeInfo;
extern TypeInfo CharU5BU5D_t108_il2cpp_TypeInfo;
extern TypeInfo Char_t109_il2cpp_TypeInfo;
extern TypeInfo TrackableBehaviour_t44_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo TrackableBehaviourU5BU5D_t813_il2cpp_TypeInfo;
extern TypeInfo WebCamAbstractBehaviour_t89_il2cpp_TypeInfo;
extern TypeInfo VideoModeData_t558_il2cpp_TypeInfo;
extern TypeInfo Vector2_t9_il2cpp_TypeInfo;
extern TypeInfo Mathf_t101_il2cpp_TypeInfo;
extern TypeInfo OrientedBoundingBox_t590_il2cpp_TypeInfo;
// Vuforia.SurfaceUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilitiesMethodDeclarations.h"
// System.String
#include "mscorlib_System_StringMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
// UnityEngine.Behaviour
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
// UnityEngine.Application
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
// Vuforia.WebCamAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamAbstractBehavMethodDeclarations.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
// UnityEngine.Mathf
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2IMethodDeclarations.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
// Vuforia.OrientedBoundingBox
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_OrientedBoundingBoxMethodDeclarations.h"
// UnityEngine.Screen
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
extern Il2CppType TrackableBehaviour_t44_0_0_0;
extern Il2CppType WebCamAbstractBehaviour_t89_0_0_0;
extern MethodInfo SurfaceUtilities_GetSurfaceOrientation_m4170_MethodInfo;
extern MethodInfo QCARRuntimeUtilities_get_ScreenOrientation_m4154_MethodInfo;
extern MethodInfo QCARRuntimeUtilities_get_IsLandscapeOrientation_m4155_MethodInfo;
extern MethodInfo String_Split_m293_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m395_MethodInfo;
extern MethodInfo Object_FindObjectsOfType_m4868_MethodInfo;
extern MethodInfo Behaviour_set_enabled_m216_MethodInfo;
extern MethodInfo Application_get_isEditor_m2356_MethodInfo;
extern MethodInfo QCARRuntimeUtilities_IsPlayMode_m540_MethodInfo;
extern MethodInfo Object_FindObjectOfType_m396_MethodInfo;
extern MethodInfo WebCamAbstractBehaviour_IsWebCamUsed_m4238_MethodInfo;
extern MethodInfo Rect_get_xMin_m2249_MethodInfo;
extern MethodInfo Rect_get_yMin_m2248_MethodInfo;
extern MethodInfo Rect_get_width_m2125_MethodInfo;
extern MethodInfo Rect_get_height_m2126_MethodInfo;
extern MethodInfo QCARRuntimeUtilities_PrepareCoordinateConversion_m4165_MethodInfo;
extern MethodInfo Mathf_RoundToInt_m2200_MethodInfo;
extern MethodInfo Vec2I__ctor_m3007_MethodInfo;
extern MethodInfo Vector2__ctor_m233_MethodInfo;
extern MethodInfo OrientedBoundingBox_get_Center_m2791_MethodInfo;
extern MethodInfo QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4159_MethodInfo;
extern MethodInfo OrientedBoundingBox_get_HalfExtents_m2793_MethodInfo;
extern MethodInfo OrientedBoundingBox_get_Rotation_m2795_MethodInfo;
extern MethodInfo OrientedBoundingBox__ctor_m2790_MethodInfo;
extern MethodInfo Rect_get_xMax_m2222_MethodInfo;
extern MethodInfo Rect_get_yMax_m2224_MethodInfo;
extern MethodInfo Rect__ctor_m332_MethodInfo;
extern MethodInfo Screen_set_sleepTimeout_m244_MethodInfo;
extern MethodInfo Object__ctor_m271_MethodInfo;


// System.String Vuforia.QCARRuntimeUtilities::StripFileNameFromPath(System.String)
extern MethodInfo QCARRuntimeUtilities_StripFileNameFromPath_m4152_MethodInfo;
 String_t* QCARRuntimeUtilities_StripFileNameFromPath_m4152 (Object_t * __this/* static, unused */, String_t* ___fullPath, MethodInfo* method){
	StringU5BU5D_t112* V_0 = {0};
	String_t* V_1 = {0};
	CharU5BU5D_t108* V_2 = {0};
	{
		V_2 = ((CharU5BU5D_t108*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t108_il2cpp_TypeInfo), 1));
		NullCheck(V_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_2, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(V_2, 0)) = (uint16_t)((int32_t)47);
		NullCheck(___fullPath);
		StringU5BU5D_t112* L_0 = String_Split_m293(___fullPath, V_2, /*hidden argument*/&String_Split_m293_MethodInfo);
		V_0 = L_0;
		NullCheck(V_0);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ((int32_t)((((int32_t)(((Array_t *)V_0)->max_length)))-1)));
		int32_t L_1 = ((int32_t)((((int32_t)(((Array_t *)V_0)->max_length)))-1));
		V_1 = (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_1));
		return V_1;
	}
}
// System.String Vuforia.QCARRuntimeUtilities::StripExtensionFromPath(System.String)
extern MethodInfo QCARRuntimeUtilities_StripExtensionFromPath_m4153_MethodInfo;
 String_t* QCARRuntimeUtilities_StripExtensionFromPath_m4153 (Object_t * __this/* static, unused */, String_t* ___fullPath, MethodInfo* method){
	StringU5BU5D_t112* V_0 = {0};
	String_t* V_1 = {0};
	CharU5BU5D_t108* V_2 = {0};
	{
		V_2 = ((CharU5BU5D_t108*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t108_il2cpp_TypeInfo), 1));
		NullCheck(V_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_2, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(V_2, 0)) = (uint16_t)((int32_t)46);
		NullCheck(___fullPath);
		StringU5BU5D_t112* L_0 = String_Split_m293(___fullPath, V_2, /*hidden argument*/&String_Split_m293_MethodInfo);
		V_0 = L_0;
		NullCheck(V_0);
		if ((((int32_t)(((int32_t)(((Array_t *)V_0)->max_length)))) > ((int32_t)1)))
		{
			goto IL_0020;
		}
	}
	{
		return (String_t*) &_stringLiteral113;
	}

IL_0020:
	{
		NullCheck(V_0);
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, ((int32_t)((((int32_t)(((Array_t *)V_0)->max_length)))-1)));
		int32_t L_1 = ((int32_t)((((int32_t)(((Array_t *)V_0)->max_length)))-1));
		V_1 = (*(String_t**)(String_t**)SZArrayLdElema(V_0, L_1));
		return V_1;
	}
}
// UnityEngine.ScreenOrientation Vuforia.QCARRuntimeUtilities::get_ScreenOrientation()
 int32_t QCARRuntimeUtilities_get_ScreenOrientation_m4154 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SurfaceUtilities_t137_il2cpp_TypeInfo));
		int32_t L_0 = SurfaceUtilities_GetSurfaceOrientation_m4170(NULL /*static, unused*/, /*hidden argument*/&SurfaceUtilities_GetSurfaceOrientation_m4170_MethodInfo);
		return L_0;
	}
}
// System.Boolean Vuforia.QCARRuntimeUtilities::get_IsLandscapeOrientation()
 bool QCARRuntimeUtilities_get_IsLandscapeOrientation_m4155 (Object_t * __this/* static, unused */, MethodInfo* method){
	int32_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		int32_t L_0 = QCARRuntimeUtilities_get_ScreenOrientation_m4154(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4154_MethodInfo);
		V_0 = L_0;
		if ((((int32_t)V_0) == ((int32_t)3)))
		{
			goto IL_0013;
		}
	}
	{
		if ((((int32_t)V_0) == ((int32_t)3)))
		{
			goto IL_0013;
		}
	}
	{
		return ((((int32_t)V_0) == ((int32_t)4))? 1 : 0);
	}

IL_0013:
	{
		return 1;
	}
}
// System.Boolean Vuforia.QCARRuntimeUtilities::get_IsPortraitOrientation()
extern MethodInfo QCARRuntimeUtilities_get_IsPortraitOrientation_m4156_MethodInfo;
 bool QCARRuntimeUtilities_get_IsPortraitOrientation_m4156 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_get_IsLandscapeOrientation_m4155(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_IsLandscapeOrientation_m4155_MethodInfo);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::ForceDisableTrackables()
extern MethodInfo QCARRuntimeUtilities_ForceDisableTrackables_m4157_MethodInfo;
 void QCARRuntimeUtilities_ForceDisableTrackables_m4157 (Object_t * __this/* static, unused */, MethodInfo* method){
	TrackableBehaviourU5BU5D_t813* V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&TrackableBehaviour_t44_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		ObjectU5BU5D_t816* L_1 = Object_FindObjectsOfType_m4868(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_FindObjectsOfType_m4868_MethodInfo);
		V_0 = ((TrackableBehaviourU5BU5D_t813*)Castclass(L_1, InitializedTypeInfo(&TrackableBehaviourU5BU5D_t813_il2cpp_TypeInfo)));
		if (!V_0)
		{
			goto IL_002f;
		}
	}
	{
		V_1 = 0;
		goto IL_0029;
	}

IL_001c:
	{
		NullCheck(V_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_0, V_1);
		int32_t L_2 = V_1;
		NullCheck((*(TrackableBehaviour_t44 **)(TrackableBehaviour_t44 **)SZArrayLdElema(V_0, L_2)));
		Behaviour_set_enabled_m216((*(TrackableBehaviour_t44 **)(TrackableBehaviour_t44 **)SZArrayLdElema(V_0, L_2)), 0, /*hidden argument*/&Behaviour_set_enabled_m216_MethodInfo);
		V_1 = ((int32_t)(V_1+1));
	}

IL_0029:
	{
		NullCheck(V_0);
		if ((((int32_t)V_1) < ((int32_t)(((int32_t)(((Array_t *)V_0)->max_length))))))
		{
			goto IL_001c;
		}
	}

IL_002f:
	{
		return;
	}
}
// System.Boolean Vuforia.QCARRuntimeUtilities::IsPlayMode()
 bool QCARRuntimeUtilities_IsPlayMode_m540 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		bool L_0 = Application_get_isEditor_m2356(NULL /*static, unused*/, /*hidden argument*/&Application_get_isEditor_m2356_MethodInfo);
		return L_0;
	}
}
// System.Boolean Vuforia.QCARRuntimeUtilities::IsQCAREnabled()
extern MethodInfo QCARRuntimeUtilities_IsQCAREnabled_m487_MethodInfo;
 bool QCARRuntimeUtilities_IsQCAREnabled_m487 (Object_t * __this/* static, unused */, MethodInfo* method){
	WebCamAbstractBehaviour_t89 * V_0 = {0};
	int32_t G_B5_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m540(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsPlayMode_m540_MethodInfo);
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		if ((((QCARRuntimeUtilities_t157_StaticFields*)InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo)->static_fields)->___sWebCamUsed_0))
		{
			goto IL_0034;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&WebCamAbstractBehaviour_t89_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		Object_t117 * L_2 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_1, /*hidden argument*/&Object_FindObjectOfType_m396_MethodInfo);
		V_0 = ((WebCamAbstractBehaviour_t89 *)Castclass(L_2, InitializedTypeInfo(&WebCamAbstractBehaviour_t89_il2cpp_TypeInfo)));
		NullCheck(V_0);
		bool L_3 = WebCamAbstractBehaviour_IsWebCamUsed_m4238(V_0, /*hidden argument*/&WebCamAbstractBehaviour_IsWebCamUsed_m4238_MethodInfo);
		if (L_3)
		{
			goto IL_002e;
		}
	}
	{
		G_B5_0 = 2;
		goto IL_002f;
	}

IL_002e:
	{
		G_B5_0 = 1;
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		((QCARRuntimeUtilities_t157_StaticFields*)InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo)->static_fields)->___sWebCamUsed_0 = G_B5_0;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		return ((((int32_t)(((QCARRuntimeUtilities_t157_StaticFields*)InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo)->static_fields)->___sWebCamUsed_0)) == ((int32_t)1))? 1 : 0);
	}

IL_003d:
	{
		return 1;
	}
}
// Vuforia.QCARRenderer/Vec2I Vuforia.QCARRuntimeUtilities::ScreenSpaceToCameraFrameCoordinates(UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
extern MethodInfo QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4158_MethodInfo;
 Vec2I_t630  QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4158 (Object_t * __this/* static, unused */, Vector2_t9  ___screenSpaceCoordinate, Rect_t118  ___bgTextureViewPortRect, bool ___isTextureMirrored, VideoModeData_t558  ___videoModeData, MethodInfo* method){
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	Vec2I_t630  V_13 = {0};
	{
		float L_0 = Rect_get_xMin_m2249((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_xMin_m2249_MethodInfo);
		V_0 = L_0;
		float L_1 = Rect_get_yMin_m2248((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_yMin_m2248_MethodInfo);
		V_1 = L_1;
		float L_2 = Rect_get_width_m2125((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_width_m2125_MethodInfo);
		V_2 = L_2;
		float L_3 = Rect_get_height_m2126((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_height_m2126_MethodInfo);
		V_3 = L_3;
		V_4 = 0;
		NullCheck((&___videoModeData));
		int32_t L_4 = ((&___videoModeData)->___width_0);
		V_5 = (((float)L_4));
		NullCheck((&___videoModeData));
		int32_t L_5 = ((&___videoModeData)->___height_1);
		V_6 = (((float)L_5));
		V_7 = (0.0f);
		V_8 = (0.0f);
		V_9 = (0.0f);
		V_10 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		QCARRuntimeUtilities_PrepareCoordinateConversion_m4165(NULL /*static, unused*/, ___isTextureMirrored, (&V_7), (&V_8), (&V_9), (&V_10), (&V_4), /*hidden argument*/&QCARRuntimeUtilities_PrepareCoordinateConversion_m4165_MethodInfo);
		NullCheck((&___screenSpaceCoordinate));
		float L_6 = ((&___screenSpaceCoordinate)->___x_1);
		V_11 = ((float)((float)((float)(L_6-V_0))/(float)V_2));
		NullCheck((&___screenSpaceCoordinate));
		float L_7 = ((&___screenSpaceCoordinate)->___y_2);
		V_12 = ((float)((float)((float)(L_7-V_1))/(float)V_3));
		if (!V_4)
		{
			goto IL_00aa;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		int32_t L_8 = Mathf_RoundToInt_m2200(NULL /*static, unused*/, ((float)((float)((float)(V_7+((float)((float)V_9*(float)V_12))))*(float)V_5)), /*hidden argument*/&Mathf_RoundToInt_m2200_MethodInfo);
		int32_t L_9 = Mathf_RoundToInt_m2200(NULL /*static, unused*/, ((float)((float)((float)(V_8+((float)((float)V_10*(float)V_11))))*(float)V_6)), /*hidden argument*/&Mathf_RoundToInt_m2200_MethodInfo);
		Vec2I__ctor_m3007((&V_13), L_8, L_9, /*hidden argument*/&Vec2I__ctor_m3007_MethodInfo);
		goto IL_00d1;
	}

IL_00aa:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		int32_t L_10 = Mathf_RoundToInt_m2200(NULL /*static, unused*/, ((float)((float)((float)(V_7+((float)((float)V_9*(float)V_11))))*(float)V_5)), /*hidden argument*/&Mathf_RoundToInt_m2200_MethodInfo);
		int32_t L_11 = Mathf_RoundToInt_m2200(NULL /*static, unused*/, ((float)((float)((float)(V_8+((float)((float)V_10*(float)V_12))))*(float)V_6)), /*hidden argument*/&Mathf_RoundToInt_m2200_MethodInfo);
		Vec2I__ctor_m3007((&V_13), L_10, L_11, /*hidden argument*/&Vec2I__ctor_m3007_MethodInfo);
	}

IL_00d1:
	{
		return V_13;
	}
}
// UnityEngine.Vector2 Vuforia.QCARRuntimeUtilities::CameraFrameToScreenSpaceCoordinates(UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
 Vector2_t9  QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4159 (Object_t * __this/* static, unused */, Vector2_t9  ___cameraFrameCoordinate, Rect_t118  ___bgTextureViewPortRect, bool ___isTextureMirrored, VideoModeData_t558  ___videoModeData, MethodInfo* method){
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	bool V_4 = false;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	Vector2_t9  V_13 = {0};
	{
		float L_0 = Rect_get_xMin_m2249((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_xMin_m2249_MethodInfo);
		V_0 = L_0;
		float L_1 = Rect_get_yMin_m2248((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_yMin_m2248_MethodInfo);
		V_1 = L_1;
		float L_2 = Rect_get_width_m2125((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_width_m2125_MethodInfo);
		V_2 = L_2;
		float L_3 = Rect_get_height_m2126((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_height_m2126_MethodInfo);
		V_3 = L_3;
		V_4 = 0;
		NullCheck((&___videoModeData));
		int32_t L_4 = ((&___videoModeData)->___width_0);
		V_5 = (((float)L_4));
		NullCheck((&___videoModeData));
		int32_t L_5 = ((&___videoModeData)->___height_1);
		V_6 = (((float)L_5));
		V_7 = (0.0f);
		V_8 = (0.0f);
		V_9 = (0.0f);
		V_10 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		QCARRuntimeUtilities_PrepareCoordinateConversion_m4165(NULL /*static, unused*/, ___isTextureMirrored, (&V_7), (&V_8), (&V_9), (&V_10), (&V_4), /*hidden argument*/&QCARRuntimeUtilities_PrepareCoordinateConversion_m4165_MethodInfo);
		NullCheck((&___cameraFrameCoordinate));
		float L_6 = ((&___cameraFrameCoordinate)->___x_1);
		V_11 = ((float)((float)((float)(((float)((float)L_6/(float)V_5))-V_7))/(float)V_9));
		NullCheck((&___cameraFrameCoordinate));
		float L_7 = ((&___cameraFrameCoordinate)->___y_2);
		V_12 = ((float)((float)((float)(((float)((float)L_7/(float)V_6))-V_8))/(float)V_10));
		if (!V_4)
		{
			goto IL_00a0;
		}
	}
	{
		Vector2__ctor_m233((&V_13), ((float)(((float)((float)V_2*(float)V_12))+V_0)), ((float)(((float)((float)V_3*(float)V_11))+V_1)), /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		goto IL_00b3;
	}

IL_00a0:
	{
		Vector2__ctor_m233((&V_13), ((float)(((float)((float)V_2*(float)V_11))+V_0)), ((float)(((float)((float)V_3*(float)V_12))+V_1)), /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
	}

IL_00b3:
	{
		return V_13;
	}
}
// Vuforia.OrientedBoundingBox Vuforia.QCARRuntimeUtilities::CameraFrameToScreenSpaceCoordinates(Vuforia.OrientedBoundingBox,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
extern MethodInfo QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4160_MethodInfo;
 OrientedBoundingBox_t590  QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4160 (Object_t * __this/* static, unused */, OrientedBoundingBox_t590  ___cameraFrameObb, Rect_t118  ___bgTextureViewPortRect, bool ___isTextureMirrored, VideoModeData_t558  ___videoModeData, MethodInfo* method){
	bool V_0 = false;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Vector2_t9  V_4 = {0};
	Vector2_t9  V_5 = {0};
	float V_6 = 0.0f;
	int32_t V_7 = {0};
	float G_B7_0 = 0.0f;
	float G_B6_0 = 0.0f;
	int32_t G_B8_0 = 0;
	float G_B8_1 = 0.0f;
	float G_B10_0 = 0.0f;
	float G_B9_0 = 0.0f;
	int32_t G_B11_0 = 0;
	float G_B11_1 = 0.0f;
	{
		V_0 = 0;
		V_1 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		int32_t L_0 = QCARRuntimeUtilities_get_ScreenOrientation_m4154(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4154_MethodInfo);
		V_7 = L_0;
		if (((int32_t)(V_7-1)) == 0)
		{
			goto IL_002a;
		}
		if (((int32_t)(V_7-1)) == 1)
		{
			goto IL_0040;
		}
		if (((int32_t)(V_7-1)) == 2)
		{
			goto IL_004a;
		}
		if (((int32_t)(V_7-1)) == 3)
		{
			goto IL_0036;
		}
	}
	{
		goto IL_004a;
	}

IL_002a:
	{
		V_1 = ((float)(V_1+(90.0f)));
		V_0 = 1;
		goto IL_004a;
	}

IL_0036:
	{
		V_1 = ((float)(V_1+(180.0f)));
		goto IL_004a;
	}

IL_0040:
	{
		V_1 = ((float)(V_1+(270.0f)));
		V_0 = 1;
	}

IL_004a:
	{
		float L_1 = Rect_get_width_m2125((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_width_m2125_MethodInfo);
		G_B6_0 = L_1;
		if (V_0)
		{
			G_B7_0 = L_1;
			goto IL_005d;
		}
	}
	{
		NullCheck((&___videoModeData));
		int32_t L_2 = ((&___videoModeData)->___width_0);
		G_B8_0 = L_2;
		G_B8_1 = G_B6_0;
		goto IL_0064;
	}

IL_005d:
	{
		NullCheck((&___videoModeData));
		int32_t L_3 = ((&___videoModeData)->___height_1);
		G_B8_0 = L_3;
		G_B8_1 = G_B7_0;
	}

IL_0064:
	{
		V_2 = ((float)((float)G_B8_1/(float)(((float)G_B8_0))));
		float L_4 = Rect_get_height_m2126((&___bgTextureViewPortRect), /*hidden argument*/&Rect_get_height_m2126_MethodInfo);
		G_B9_0 = L_4;
		if (V_0)
		{
			G_B10_0 = L_4;
			goto IL_007a;
		}
	}
	{
		NullCheck((&___videoModeData));
		int32_t L_5 = ((&___videoModeData)->___height_1);
		G_B11_0 = L_5;
		G_B11_1 = G_B9_0;
		goto IL_0081;
	}

IL_007a:
	{
		NullCheck((&___videoModeData));
		int32_t L_6 = ((&___videoModeData)->___width_0);
		G_B11_0 = L_6;
		G_B11_1 = G_B10_0;
	}

IL_0081:
	{
		V_3 = ((float)((float)G_B11_1/(float)(((float)G_B11_0))));
		Vector2_t9  L_7 = OrientedBoundingBox_get_Center_m2791((&___cameraFrameObb), /*hidden argument*/&OrientedBoundingBox_get_Center_m2791_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		Vector2_t9  L_8 = QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4159(NULL /*static, unused*/, L_7, ___bgTextureViewPortRect, ___isTextureMirrored, ___videoModeData, /*hidden argument*/&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4159_MethodInfo);
		V_4 = L_8;
		Vector2_t9  L_9 = OrientedBoundingBox_get_HalfExtents_m2793((&___cameraFrameObb), /*hidden argument*/&OrientedBoundingBox_get_HalfExtents_m2793_MethodInfo);
		;
		float L_10 = (L_9.___x_1);
		Vector2_t9  L_11 = OrientedBoundingBox_get_HalfExtents_m2793((&___cameraFrameObb), /*hidden argument*/&OrientedBoundingBox_get_HalfExtents_m2793_MethodInfo);
		;
		float L_12 = (L_11.___y_2);
		Vector2__ctor_m233((&V_5), ((float)((float)L_10*(float)V_2)), ((float)((float)L_12*(float)V_3)), /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		float L_13 = OrientedBoundingBox_get_Rotation_m2795((&___cameraFrameObb), /*hidden argument*/&OrientedBoundingBox_get_Rotation_m2795_MethodInfo);
		V_6 = L_13;
		if (!___isTextureMirrored)
		{
			goto IL_00c9;
		}
	}
	{
		V_6 = ((-V_6));
	}

IL_00c9:
	{
		V_6 = ((float)(((float)((float)((float)((float)V_6*(float)(180.0f)))/(float)(3.14159274f)))+V_1));
		OrientedBoundingBox_t590  L_14 = {0};
		OrientedBoundingBox__ctor_m2790(&L_14, V_4, V_5, V_6, /*hidden argument*/&OrientedBoundingBox__ctor_m2790_MethodInfo);
		return L_14;
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::SelectRectTopLeftAndBottomRightForLandscapeLeft(UnityEngine.Rect,System.Boolean,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern MethodInfo QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4161_MethodInfo;
 void QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4161 (Object_t * __this/* static, unused */, Rect_t118  ___screenSpaceRect, bool ___isMirrored, Vector2_t9 * ___topLeft, Vector2_t9 * ___bottomRight, MethodInfo* method){
	int32_t V_0 = {0};
	int32_t V_1 = {0};
	{
		if (___isMirrored)
		{
			goto IL_00f5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		int32_t L_0 = QCARRuntimeUtilities_get_ScreenOrientation_m4154(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4154_MethodInfo);
		V_0 = L_0;
		if (((int32_t)(V_0-1)) == 0)
		{
			goto IL_005c;
		}
		if (((int32_t)(V_0-1)) == 1)
		{
			goto IL_008f;
		}
		if (((int32_t)(V_0-1)) == 2)
		{
			goto IL_00c2;
		}
		if (((int32_t)(V_0-1)) == 3)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_00c2;
	}

IL_0029:
	{
		float L_1 = Rect_get_xMax_m2222((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2222_MethodInfo);
		float L_2 = Rect_get_yMax_m2224((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2224_MethodInfo);
		Vector2_t9  L_3 = {0};
		Vector2__ctor_m233(&L_3, L_1, L_2, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___topLeft = L_3;
		float L_4 = Rect_get_xMin_m2249((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2249_MethodInfo);
		float L_5 = Rect_get_yMin_m2248((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2248_MethodInfo);
		Vector2_t9  L_6 = {0};
		Vector2__ctor_m233(&L_6, L_4, L_5, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___bottomRight = L_6;
		return;
	}

IL_005c:
	{
		float L_7 = Rect_get_xMax_m2222((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2222_MethodInfo);
		float L_8 = Rect_get_yMin_m2248((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2248_MethodInfo);
		Vector2_t9  L_9 = {0};
		Vector2__ctor_m233(&L_9, L_7, L_8, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___topLeft = L_9;
		float L_10 = Rect_get_xMin_m2249((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2249_MethodInfo);
		float L_11 = Rect_get_yMax_m2224((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2224_MethodInfo);
		Vector2_t9  L_12 = {0};
		Vector2__ctor_m233(&L_12, L_10, L_11, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___bottomRight = L_12;
		return;
	}

IL_008f:
	{
		float L_13 = Rect_get_xMin_m2249((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2249_MethodInfo);
		float L_14 = Rect_get_yMax_m2224((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2224_MethodInfo);
		Vector2_t9  L_15 = {0};
		Vector2__ctor_m233(&L_15, L_13, L_14, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___topLeft = L_15;
		float L_16 = Rect_get_xMax_m2222((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2222_MethodInfo);
		float L_17 = Rect_get_yMin_m2248((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2248_MethodInfo);
		Vector2_t9  L_18 = {0};
		Vector2__ctor_m233(&L_18, L_16, L_17, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___bottomRight = L_18;
		return;
	}

IL_00c2:
	{
		float L_19 = Rect_get_xMin_m2249((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2249_MethodInfo);
		float L_20 = Rect_get_yMin_m2248((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2248_MethodInfo);
		Vector2_t9  L_21 = {0};
		Vector2__ctor_m233(&L_21, L_19, L_20, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___topLeft = L_21;
		float L_22 = Rect_get_xMax_m2222((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2222_MethodInfo);
		float L_23 = Rect_get_yMax_m2224((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2224_MethodInfo);
		Vector2_t9  L_24 = {0};
		Vector2__ctor_m233(&L_24, L_22, L_23, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___bottomRight = L_24;
		return;
	}

IL_00f5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		int32_t L_25 = QCARRuntimeUtilities_get_ScreenOrientation_m4154(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4154_MethodInfo);
		V_1 = L_25;
		if (((int32_t)(V_1-1)) == 0)
		{
			goto IL_014b;
		}
		if (((int32_t)(V_1-1)) == 1)
		{
			goto IL_017e;
		}
		if (((int32_t)(V_1-1)) == 2)
		{
			goto IL_01b1;
		}
		if (((int32_t)(V_1-1)) == 3)
		{
			goto IL_0118;
		}
	}
	{
		goto IL_01b1;
	}

IL_0118:
	{
		float L_26 = Rect_get_xMin_m2249((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2249_MethodInfo);
		float L_27 = Rect_get_yMax_m2224((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2224_MethodInfo);
		Vector2_t9  L_28 = {0};
		Vector2__ctor_m233(&L_28, L_26, L_27, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___topLeft = L_28;
		float L_29 = Rect_get_xMax_m2222((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2222_MethodInfo);
		float L_30 = Rect_get_yMin_m2248((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2248_MethodInfo);
		Vector2_t9  L_31 = {0};
		Vector2__ctor_m233(&L_31, L_29, L_30, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___bottomRight = L_31;
		return;
	}

IL_014b:
	{
		float L_32 = Rect_get_xMax_m2222((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2222_MethodInfo);
		float L_33 = Rect_get_yMax_m2224((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2224_MethodInfo);
		Vector2_t9  L_34 = {0};
		Vector2__ctor_m233(&L_34, L_32, L_33, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___topLeft = L_34;
		float L_35 = Rect_get_xMin_m2249((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2249_MethodInfo);
		float L_36 = Rect_get_yMin_m2248((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2248_MethodInfo);
		Vector2_t9  L_37 = {0};
		Vector2__ctor_m233(&L_37, L_35, L_36, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___bottomRight = L_37;
		return;
	}

IL_017e:
	{
		float L_38 = Rect_get_xMin_m2249((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2249_MethodInfo);
		float L_39 = Rect_get_yMin_m2248((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2248_MethodInfo);
		Vector2_t9  L_40 = {0};
		Vector2__ctor_m233(&L_40, L_38, L_39, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___topLeft = L_40;
		float L_41 = Rect_get_xMax_m2222((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2222_MethodInfo);
		float L_42 = Rect_get_yMax_m2224((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2224_MethodInfo);
		Vector2_t9  L_43 = {0};
		Vector2__ctor_m233(&L_43, L_41, L_42, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___bottomRight = L_43;
		return;
	}

IL_01b1:
	{
		float L_44 = Rect_get_xMax_m2222((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMax_m2222_MethodInfo);
		float L_45 = Rect_get_yMin_m2248((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMin_m2248_MethodInfo);
		Vector2_t9  L_46 = {0};
		Vector2__ctor_m233(&L_46, L_44, L_45, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___topLeft = L_46;
		float L_47 = Rect_get_xMin_m2249((&___screenSpaceRect), /*hidden argument*/&Rect_get_xMin_m2249_MethodInfo);
		float L_48 = Rect_get_yMax_m2224((&___screenSpaceRect), /*hidden argument*/&Rect_get_yMax_m2224_MethodInfo);
		Vector2_t9  L_49 = {0};
		Vector2__ctor_m233(&L_49, L_47, L_48, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		*___bottomRight = L_49;
		return;
	}
}
// UnityEngine.Rect Vuforia.QCARRuntimeUtilities::CalculateRectFromLandscapeLeftCorners(UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern MethodInfo QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4162_MethodInfo;
 Rect_t118  QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4162 (Object_t * __this/* static, unused */, Vector2_t9  ___topLeft, Vector2_t9  ___bottomRight, bool ___isMirrored, MethodInfo* method){
	Rect_t118  V_0 = {0};
	int32_t V_1 = {0};
	int32_t V_2 = {0};
	{
		if (___isMirrored)
		{
			goto IL_0109;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		int32_t L_0 = QCARRuntimeUtilities_get_ScreenOrientation_m4154(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4154_MethodInfo);
		V_1 = L_0;
		if (((int32_t)(V_1-1)) == 0)
		{
			goto IL_0061;
		}
		if (((int32_t)(V_1-1)) == 1)
		{
			goto IL_0099;
		}
		if (((int32_t)(V_1-1)) == 2)
		{
			goto IL_00d1;
		}
		if (((int32_t)(V_1-1)) == 3)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_00d1;
	}

IL_0029:
	{
		NullCheck((&___bottomRight));
		float L_1 = ((&___bottomRight)->___x_1);
		NullCheck((&___bottomRight));
		float L_2 = ((&___bottomRight)->___y_2);
		NullCheck((&___topLeft));
		float L_3 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_4 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_5 = ((&___topLeft)->___y_2);
		NullCheck((&___bottomRight));
		float L_6 = ((&___bottomRight)->___y_2);
		Rect__ctor_m332((&V_0), L_1, L_2, ((float)(L_3-L_4)), ((float)(L_5-L_6)), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		goto IL_0201;
	}

IL_0061:
	{
		NullCheck((&___bottomRight));
		float L_7 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_8 = ((&___topLeft)->___y_2);
		NullCheck((&___topLeft));
		float L_9 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_10 = ((&___bottomRight)->___x_1);
		NullCheck((&___bottomRight));
		float L_11 = ((&___bottomRight)->___y_2);
		NullCheck((&___topLeft));
		float L_12 = ((&___topLeft)->___y_2);
		Rect__ctor_m332((&V_0), L_7, L_8, ((float)(L_9-L_10)), ((float)(L_11-L_12)), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		goto IL_0201;
	}

IL_0099:
	{
		NullCheck((&___topLeft));
		float L_13 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_14 = ((&___bottomRight)->___y_2);
		NullCheck((&___bottomRight));
		float L_15 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_16 = ((&___topLeft)->___x_1);
		NullCheck((&___topLeft));
		float L_17 = ((&___topLeft)->___y_2);
		NullCheck((&___bottomRight));
		float L_18 = ((&___bottomRight)->___y_2);
		Rect__ctor_m332((&V_0), L_13, L_14, ((float)(L_15-L_16)), ((float)(L_17-L_18)), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		goto IL_0201;
	}

IL_00d1:
	{
		NullCheck((&___topLeft));
		float L_19 = ((&___topLeft)->___x_1);
		NullCheck((&___topLeft));
		float L_20 = ((&___topLeft)->___y_2);
		NullCheck((&___bottomRight));
		float L_21 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_22 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_23 = ((&___bottomRight)->___y_2);
		NullCheck((&___topLeft));
		float L_24 = ((&___topLeft)->___y_2);
		Rect__ctor_m332((&V_0), L_19, L_20, ((float)(L_21-L_22)), ((float)(L_23-L_24)), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		goto IL_0201;
	}

IL_0109:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		int32_t L_25 = QCARRuntimeUtilities_get_ScreenOrientation_m4154(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4154_MethodInfo);
		V_2 = L_25;
		if (((int32_t)(V_2-1)) == 0)
		{
			goto IL_0164;
		}
		if (((int32_t)(V_2-1)) == 1)
		{
			goto IL_0199;
		}
		if (((int32_t)(V_2-1)) == 2)
		{
			goto IL_01ce;
		}
		if (((int32_t)(V_2-1)) == 3)
		{
			goto IL_012c;
		}
	}
	{
		goto IL_01ce;
	}

IL_012c:
	{
		NullCheck((&___topLeft));
		float L_26 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_27 = ((&___bottomRight)->___y_2);
		NullCheck((&___bottomRight));
		float L_28 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_29 = ((&___topLeft)->___x_1);
		NullCheck((&___topLeft));
		float L_30 = ((&___topLeft)->___y_2);
		NullCheck((&___bottomRight));
		float L_31 = ((&___bottomRight)->___y_2);
		Rect__ctor_m332((&V_0), L_26, L_27, ((float)(L_28-L_29)), ((float)(L_30-L_31)), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		goto IL_0201;
	}

IL_0164:
	{
		NullCheck((&___bottomRight));
		float L_32 = ((&___bottomRight)->___x_1);
		NullCheck((&___bottomRight));
		float L_33 = ((&___bottomRight)->___y_2);
		NullCheck((&___topLeft));
		float L_34 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_35 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_36 = ((&___topLeft)->___y_2);
		NullCheck((&___bottomRight));
		float L_37 = ((&___bottomRight)->___y_2);
		Rect__ctor_m332((&V_0), L_32, L_33, ((float)(L_34-L_35)), ((float)(L_36-L_37)), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		goto IL_0201;
	}

IL_0199:
	{
		NullCheck((&___topLeft));
		float L_38 = ((&___topLeft)->___x_1);
		NullCheck((&___topLeft));
		float L_39 = ((&___topLeft)->___y_2);
		NullCheck((&___bottomRight));
		float L_40 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_41 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_42 = ((&___bottomRight)->___y_2);
		NullCheck((&___topLeft));
		float L_43 = ((&___topLeft)->___y_2);
		Rect__ctor_m332((&V_0), L_38, L_39, ((float)(L_40-L_41)), ((float)(L_42-L_43)), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
		goto IL_0201;
	}

IL_01ce:
	{
		NullCheck((&___bottomRight));
		float L_44 = ((&___bottomRight)->___x_1);
		NullCheck((&___topLeft));
		float L_45 = ((&___topLeft)->___y_2);
		NullCheck((&___topLeft));
		float L_46 = ((&___topLeft)->___x_1);
		NullCheck((&___bottomRight));
		float L_47 = ((&___bottomRight)->___x_1);
		NullCheck((&___bottomRight));
		float L_48 = ((&___bottomRight)->___y_2);
		NullCheck((&___topLeft));
		float L_49 = ((&___topLeft)->___y_2);
		Rect__ctor_m332((&V_0), L_44, L_45, ((float)(L_46-L_47)), ((float)(L_48-L_49)), /*hidden argument*/&Rect__ctor_m332_MethodInfo);
	}

IL_0201:
	{
		return V_0;
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::DisableSleepMode()
extern MethodInfo QCARRuntimeUtilities_DisableSleepMode_m4163_MethodInfo;
 void QCARRuntimeUtilities_DisableSleepMode_m4163 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		Screen_set_sleepTimeout_m244(NULL /*static, unused*/, (-1), /*hidden argument*/&Screen_set_sleepTimeout_m244_MethodInfo);
		return;
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::ResetSleepMode()
extern MethodInfo QCARRuntimeUtilities_ResetSleepMode_m4164_MethodInfo;
 void QCARRuntimeUtilities_ResetSleepMode_m4164 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		Screen_set_sleepTimeout_m244(NULL /*static, unused*/, ((int32_t)-2), /*hidden argument*/&Screen_set_sleepTimeout_m244_MethodInfo);
		return;
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::PrepareCoordinateConversion(System.Boolean,System.Single&,System.Single&,System.Single&,System.Single&,System.Boolean&)
 void QCARRuntimeUtilities_PrepareCoordinateConversion_m4165 (Object_t * __this/* static, unused */, bool ___isTextureMirrored, float* ___prefixX, float* ___prefixY, float* ___inversionMultiplierX, float* ___inversionMultiplierY, bool* ___isPortrait, MethodInfo* method){
	int32_t V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		int32_t L_0 = QCARRuntimeUtilities_get_ScreenOrientation_m4154(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_get_ScreenOrientation_m4154_MethodInfo);
		V_0 = L_0;
		if (((int32_t)(V_0-1)) == 0)
		{
			goto IL_0023;
		}
		if (((int32_t)(V_0-1)) == 1)
		{
			goto IL_0066;
		}
		if (((int32_t)(V_0-1)) == 2)
		{
			goto IL_00ec;
		}
		if (((int32_t)(V_0-1)) == 3)
		{
			goto IL_00a9;
		}
	}
	{
		goto IL_00ec;
	}

IL_0023:
	{
		*((int8_t*)(___isPortrait)) = (int8_t)1;
		if (___isTextureMirrored)
		{
			goto IL_0048;
		}
	}
	{
		*((float*)(___prefixX)) = (float)(0.0f);
		*((float*)(___prefixY)) = (float)(1.0f);
		*((float*)(___inversionMultiplierX)) = (float)(1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(-1.0f);
		return;
	}

IL_0048:
	{
		*((float*)(___prefixX)) = (float)(1.0f);
		*((float*)(___prefixY)) = (float)(1.0f);
		*((float*)(___inversionMultiplierX)) = (float)(-1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(-1.0f);
		return;
	}

IL_0066:
	{
		*((int8_t*)(___isPortrait)) = (int8_t)1;
		if (___isTextureMirrored)
		{
			goto IL_008b;
		}
	}
	{
		*((float*)(___prefixX)) = (float)(1.0f);
		*((float*)(___prefixY)) = (float)(0.0f);
		*((float*)(___inversionMultiplierX)) = (float)(-1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(1.0f);
		return;
	}

IL_008b:
	{
		*((float*)(___prefixX)) = (float)(0.0f);
		*((float*)(___prefixY)) = (float)(0.0f);
		*((float*)(___inversionMultiplierX)) = (float)(1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(1.0f);
		return;
	}

IL_00a9:
	{
		*((int8_t*)(___isPortrait)) = (int8_t)0;
		if (___isTextureMirrored)
		{
			goto IL_00ce;
		}
	}
	{
		*((float*)(___prefixX)) = (float)(1.0f);
		*((float*)(___prefixY)) = (float)(1.0f);
		*((float*)(___inversionMultiplierX)) = (float)(-1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(-1.0f);
		return;
	}

IL_00ce:
	{
		*((float*)(___prefixX)) = (float)(0.0f);
		*((float*)(___prefixY)) = (float)(1.0f);
		*((float*)(___inversionMultiplierX)) = (float)(1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(-1.0f);
		return;
	}

IL_00ec:
	{
		*((int8_t*)(___isPortrait)) = (int8_t)0;
		if (___isTextureMirrored)
		{
			goto IL_0111;
		}
	}
	{
		*((float*)(___prefixX)) = (float)(0.0f);
		*((float*)(___prefixY)) = (float)(0.0f);
		*((float*)(___inversionMultiplierX)) = (float)(1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(1.0f);
		return;
	}

IL_0111:
	{
		*((float*)(___prefixX)) = (float)(1.0f);
		*((float*)(___prefixY)) = (float)(0.0f);
		*((float*)(___inversionMultiplierX)) = (float)(-1.0f);
		*((float*)(___inversionMultiplierY)) = (float)(1.0f);
		return;
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::.ctor()
extern MethodInfo QCARRuntimeUtilities__ctor_m4166_MethodInfo;
 void QCARRuntimeUtilities__ctor_m4166 (QCARRuntimeUtilities_t157 * __this, MethodInfo* method){
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		return;
	}
}
// System.Void Vuforia.QCARRuntimeUtilities::.cctor()
extern MethodInfo QCARRuntimeUtilities__cctor_m4167_MethodInfo;
 void QCARRuntimeUtilities__cctor_m4167 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition Vuforia.QCARRuntimeUtilities
extern Il2CppType WebCamUsed_t763_0_0_17;
FieldInfo QCARRuntimeUtilities_t157____sWebCamUsed_0_FieldInfo = 
{
	"sWebCamUsed"/* name */
	, &WebCamUsed_t763_0_0_17/* type */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* parent */
	, offsetof(QCARRuntimeUtilities_t157_StaticFields, ___sWebCamUsed_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* QCARRuntimeUtilities_t157_FieldInfos[] =
{
	&QCARRuntimeUtilities_t157____sWebCamUsed_0_FieldInfo,
	NULL
};
static PropertyInfo QCARRuntimeUtilities_t157____ScreenOrientation_PropertyInfo = 
{
	&QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* parent */
	, "ScreenOrientation"/* name */
	, &QCARRuntimeUtilities_get_ScreenOrientation_m4154_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo QCARRuntimeUtilities_t157____IsLandscapeOrientation_PropertyInfo = 
{
	&QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* parent */
	, "IsLandscapeOrientation"/* name */
	, &QCARRuntimeUtilities_get_IsLandscapeOrientation_m4155_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo QCARRuntimeUtilities_t157____IsPortraitOrientation_PropertyInfo = 
{
	&QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* parent */
	, "IsPortraitOrientation"/* name */
	, &QCARRuntimeUtilities_get_IsPortraitOrientation_m4156_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* QCARRuntimeUtilities_t157_PropertyInfos[] =
{
	&QCARRuntimeUtilities_t157____ScreenOrientation_PropertyInfo,
	&QCARRuntimeUtilities_t157____IsLandscapeOrientation_PropertyInfo,
	&QCARRuntimeUtilities_t157____IsPortraitOrientation_PropertyInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
static ParameterInfo QCARRuntimeUtilities_t157_QCARRuntimeUtilities_StripFileNameFromPath_m4152_ParameterInfos[] = 
{
	{"fullPath", 0, 134219982, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.QCARRuntimeUtilities::StripFileNameFromPath(System.String)
MethodInfo QCARRuntimeUtilities_StripFileNameFromPath_m4152_MethodInfo = 
{
	"StripFileNameFromPath"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_StripFileNameFromPath_m4152/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, QCARRuntimeUtilities_t157_QCARRuntimeUtilities_StripFileNameFromPath_m4152_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2340/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo QCARRuntimeUtilities_t157_QCARRuntimeUtilities_StripExtensionFromPath_m4153_ParameterInfos[] = 
{
	{"fullPath", 0, 134219983, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.QCARRuntimeUtilities::StripExtensionFromPath(System.String)
MethodInfo QCARRuntimeUtilities_StripExtensionFromPath_m4153_MethodInfo = 
{
	"StripExtensionFromPath"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_StripExtensionFromPath_m4153/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, QCARRuntimeUtilities_t157_QCARRuntimeUtilities_StripExtensionFromPath_m4153_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2341/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ScreenOrientation_t138_0_0_0;
extern void* RuntimeInvoker_ScreenOrientation_t138 (MethodInfo* method, void* obj, void** args);
// UnityEngine.ScreenOrientation Vuforia.QCARRuntimeUtilities::get_ScreenOrientation()
MethodInfo QCARRuntimeUtilities_get_ScreenOrientation_m4154_MethodInfo = 
{
	"get_ScreenOrientation"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_get_ScreenOrientation_m4154/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &ScreenOrientation_t138_0_0_0/* return_type */
	, RuntimeInvoker_ScreenOrientation_t138/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2342/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.QCARRuntimeUtilities::get_IsLandscapeOrientation()
MethodInfo QCARRuntimeUtilities_get_IsLandscapeOrientation_m4155_MethodInfo = 
{
	"get_IsLandscapeOrientation"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_get_IsLandscapeOrientation_m4155/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2343/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.QCARRuntimeUtilities::get_IsPortraitOrientation()
MethodInfo QCARRuntimeUtilities_get_IsPortraitOrientation_m4156_MethodInfo = 
{
	"get_IsPortraitOrientation"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_get_IsPortraitOrientation_m4156/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2198/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2344/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::ForceDisableTrackables()
MethodInfo QCARRuntimeUtilities_ForceDisableTrackables_m4157_MethodInfo = 
{
	"ForceDisableTrackables"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_ForceDisableTrackables_m4157/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2345/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.QCARRuntimeUtilities::IsPlayMode()
MethodInfo QCARRuntimeUtilities_IsPlayMode_m540_MethodInfo = 
{
	"IsPlayMode"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_IsPlayMode_m540/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2346/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.QCARRuntimeUtilities::IsQCAREnabled()
MethodInfo QCARRuntimeUtilities_IsQCAREnabled_m487_MethodInfo = 
{
	"IsQCAREnabled"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_IsQCAREnabled_m487/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2347/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType Rect_t118_0_0_0;
extern Il2CppType Rect_t118_0_0_0;
extern Il2CppType Boolean_t106_0_0_0;
extern Il2CppType Boolean_t106_0_0_0;
extern Il2CppType VideoModeData_t558_0_0_0;
extern Il2CppType VideoModeData_t558_0_0_0;
static ParameterInfo QCARRuntimeUtilities_t157_QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4158_ParameterInfos[] = 
{
	{"screenSpaceCoordinate", 0, 134219984, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
	{"bgTextureViewPortRect", 1, 134219985, &EmptyCustomAttributesCache, &Rect_t118_0_0_0},
	{"isTextureMirrored", 2, 134219986, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
	{"videoModeData", 3, 134219987, &EmptyCustomAttributesCache, &VideoModeData_t558_0_0_0},
};
extern Il2CppType Vec2I_t630_0_0_0;
extern void* RuntimeInvoker_Vec2I_t630_Vector2_t9_Rect_t118_SByte_t129_VideoModeData_t558 (MethodInfo* method, void* obj, void** args);
// Vuforia.QCARRenderer/Vec2I Vuforia.QCARRuntimeUtilities::ScreenSpaceToCameraFrameCoordinates(UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
MethodInfo QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4158_MethodInfo = 
{
	"ScreenSpaceToCameraFrameCoordinates"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4158/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Vec2I_t630_0_0_0/* return_type */
	, RuntimeInvoker_Vec2I_t630_Vector2_t9_Rect_t118_SByte_t129_VideoModeData_t558/* invoker_method */
	, QCARRuntimeUtilities_t157_QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4158_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2348/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType Rect_t118_0_0_0;
extern Il2CppType Boolean_t106_0_0_0;
extern Il2CppType VideoModeData_t558_0_0_0;
static ParameterInfo QCARRuntimeUtilities_t157_QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4159_ParameterInfos[] = 
{
	{"cameraFrameCoordinate", 0, 134219988, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
	{"bgTextureViewPortRect", 1, 134219989, &EmptyCustomAttributesCache, &Rect_t118_0_0_0},
	{"isTextureMirrored", 2, 134219990, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
	{"videoModeData", 3, 134219991, &EmptyCustomAttributesCache, &VideoModeData_t558_0_0_0},
};
extern Il2CppType Vector2_t9_0_0_0;
extern void* RuntimeInvoker_Vector2_t9_Vector2_t9_Rect_t118_SByte_t129_VideoModeData_t558 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Vector2 Vuforia.QCARRuntimeUtilities::CameraFrameToScreenSpaceCoordinates(UnityEngine.Vector2,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
MethodInfo QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4159_MethodInfo = 
{
	"CameraFrameToScreenSpaceCoordinates"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4159/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Vector2_t9_0_0_0/* return_type */
	, RuntimeInvoker_Vector2_t9_Vector2_t9_Rect_t118_SByte_t129_VideoModeData_t558/* invoker_method */
	, QCARRuntimeUtilities_t157_QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4159_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2349/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType OrientedBoundingBox_t590_0_0_0;
extern Il2CppType OrientedBoundingBox_t590_0_0_0;
extern Il2CppType Rect_t118_0_0_0;
extern Il2CppType Boolean_t106_0_0_0;
extern Il2CppType VideoModeData_t558_0_0_0;
static ParameterInfo QCARRuntimeUtilities_t157_QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4160_ParameterInfos[] = 
{
	{"cameraFrameObb", 0, 134219992, &EmptyCustomAttributesCache, &OrientedBoundingBox_t590_0_0_0},
	{"bgTextureViewPortRect", 1, 134219993, &EmptyCustomAttributesCache, &Rect_t118_0_0_0},
	{"isTextureMirrored", 2, 134219994, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
	{"videoModeData", 3, 134219995, &EmptyCustomAttributesCache, &VideoModeData_t558_0_0_0},
};
extern Il2CppType OrientedBoundingBox_t590_0_0_0;
extern void* RuntimeInvoker_OrientedBoundingBox_t590_OrientedBoundingBox_t590_Rect_t118_SByte_t129_VideoModeData_t558 (MethodInfo* method, void* obj, void** args);
// Vuforia.OrientedBoundingBox Vuforia.QCARRuntimeUtilities::CameraFrameToScreenSpaceCoordinates(Vuforia.OrientedBoundingBox,UnityEngine.Rect,System.Boolean,Vuforia.CameraDevice/VideoModeData)
MethodInfo QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4160_MethodInfo = 
{
	"CameraFrameToScreenSpaceCoordinates"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4160/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &OrientedBoundingBox_t590_0_0_0/* return_type */
	, RuntimeInvoker_OrientedBoundingBox_t590_OrientedBoundingBox_t590_Rect_t118_SByte_t129_VideoModeData_t558/* invoker_method */
	, QCARRuntimeUtilities_t157_QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4160_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2350/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Rect_t118_0_0_0;
extern Il2CppType Boolean_t106_0_0_0;
extern Il2CppType Vector2_t9_1_0_2;
extern Il2CppType Vector2_t9_1_0_0;
extern Il2CppType Vector2_t9_1_0_2;
static ParameterInfo QCARRuntimeUtilities_t157_QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4161_ParameterInfos[] = 
{
	{"screenSpaceRect", 0, 134219996, &EmptyCustomAttributesCache, &Rect_t118_0_0_0},
	{"isMirrored", 1, 134219997, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
	{"topLeft", 2, 134219998, &EmptyCustomAttributesCache, &Vector2_t9_1_0_2},
	{"bottomRight", 3, 134219999, &EmptyCustomAttributesCache, &Vector2_t9_1_0_2},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Rect_t118_SByte_t129_Vector2U26_t896_Vector2U26_t896 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::SelectRectTopLeftAndBottomRightForLandscapeLeft(UnityEngine.Rect,System.Boolean,UnityEngine.Vector2&,UnityEngine.Vector2&)
MethodInfo QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4161_MethodInfo = 
{
	"SelectRectTopLeftAndBottomRightForLandscapeLeft"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4161/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Rect_t118_SByte_t129_Vector2U26_t896_Vector2U26_t896/* invoker_method */
	, QCARRuntimeUtilities_t157_QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4161_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2351/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo QCARRuntimeUtilities_t157_QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4162_ParameterInfos[] = 
{
	{"topLeft", 0, 134220000, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
	{"bottomRight", 1, 134220001, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
	{"isMirrored", 2, 134220002, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Rect_t118_0_0_0;
extern void* RuntimeInvoker_Rect_t118_Vector2_t9_Vector2_t9_SByte_t129 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Rect Vuforia.QCARRuntimeUtilities::CalculateRectFromLandscapeLeftCorners(UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
MethodInfo QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4162_MethodInfo = 
{
	"CalculateRectFromLandscapeLeftCorners"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4162/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Rect_t118_0_0_0/* return_type */
	, RuntimeInvoker_Rect_t118_Vector2_t9_Vector2_t9_SByte_t129/* invoker_method */
	, QCARRuntimeUtilities_t157_QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4162_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2352/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::DisableSleepMode()
MethodInfo QCARRuntimeUtilities_DisableSleepMode_m4163_MethodInfo = 
{
	"DisableSleepMode"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_DisableSleepMode_m4163/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2353/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::ResetSleepMode()
MethodInfo QCARRuntimeUtilities_ResetSleepMode_m4164_MethodInfo = 
{
	"ResetSleepMode"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_ResetSleepMode_m4164/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2354/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern Il2CppType Single_t105_1_0_0;
extern Il2CppType Single_t105_1_0_0;
extern Il2CppType Single_t105_1_0_0;
extern Il2CppType Single_t105_1_0_0;
extern Il2CppType Single_t105_1_0_0;
extern Il2CppType Boolean_t106_1_0_0;
extern Il2CppType Boolean_t106_1_0_0;
static ParameterInfo QCARRuntimeUtilities_t157_QCARRuntimeUtilities_PrepareCoordinateConversion_m4165_ParameterInfos[] = 
{
	{"isTextureMirrored", 0, 134220003, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
	{"prefixX", 1, 134220004, &EmptyCustomAttributesCache, &Single_t105_1_0_0},
	{"prefixY", 2, 134220005, &EmptyCustomAttributesCache, &Single_t105_1_0_0},
	{"inversionMultiplierX", 3, 134220006, &EmptyCustomAttributesCache, &Single_t105_1_0_0},
	{"inversionMultiplierY", 4, 134220007, &EmptyCustomAttributesCache, &Single_t105_1_0_0},
	{"isPortrait", 5, 134220008, &EmptyCustomAttributesCache, &Boolean_t106_1_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_SByte_t129_SingleU26_t897_SingleU26_t897_SingleU26_t897_SingleU26_t897_BooleanU26_t442 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::PrepareCoordinateConversion(System.Boolean,System.Single&,System.Single&,System.Single&,System.Single&,System.Boolean&)
MethodInfo QCARRuntimeUtilities_PrepareCoordinateConversion_m4165_MethodInfo = 
{
	"PrepareCoordinateConversion"/* name */
	, (methodPointerType)&QCARRuntimeUtilities_PrepareCoordinateConversion_m4165/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_SByte_t129_SingleU26_t897_SingleU26_t897_SingleU26_t897_SingleU26_t897_BooleanU26_t442/* invoker_method */
	, QCARRuntimeUtilities_t157_QCARRuntimeUtilities_PrepareCoordinateConversion_m4165_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 6/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2355/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::.ctor()
MethodInfo QCARRuntimeUtilities__ctor_m4166_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&QCARRuntimeUtilities__ctor_m4166/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2356/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.QCARRuntimeUtilities::.cctor()
MethodInfo QCARRuntimeUtilities__cctor_m4167_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&QCARRuntimeUtilities__cctor_m4167/* method */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2357/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* QCARRuntimeUtilities_t157_MethodInfos[] =
{
	&QCARRuntimeUtilities_StripFileNameFromPath_m4152_MethodInfo,
	&QCARRuntimeUtilities_StripExtensionFromPath_m4153_MethodInfo,
	&QCARRuntimeUtilities_get_ScreenOrientation_m4154_MethodInfo,
	&QCARRuntimeUtilities_get_IsLandscapeOrientation_m4155_MethodInfo,
	&QCARRuntimeUtilities_get_IsPortraitOrientation_m4156_MethodInfo,
	&QCARRuntimeUtilities_ForceDisableTrackables_m4157_MethodInfo,
	&QCARRuntimeUtilities_IsPlayMode_m540_MethodInfo,
	&QCARRuntimeUtilities_IsQCAREnabled_m487_MethodInfo,
	&QCARRuntimeUtilities_ScreenSpaceToCameraFrameCoordinates_m4158_MethodInfo,
	&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4159_MethodInfo,
	&QCARRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m4160_MethodInfo,
	&QCARRuntimeUtilities_SelectRectTopLeftAndBottomRightForLandscapeLeft_m4161_MethodInfo,
	&QCARRuntimeUtilities_CalculateRectFromLandscapeLeftCorners_m4162_MethodInfo,
	&QCARRuntimeUtilities_DisableSleepMode_m4163_MethodInfo,
	&QCARRuntimeUtilities_ResetSleepMode_m4164_MethodInfo,
	&QCARRuntimeUtilities_PrepareCoordinateConversion_m4165_MethodInfo,
	&QCARRuntimeUtilities__ctor_m4166_MethodInfo,
	&QCARRuntimeUtilities__cctor_m4167_MethodInfo,
	NULL
};
extern TypeInfo WebCamUsed_t763_il2cpp_TypeInfo;
static TypeInfo* QCARRuntimeUtilities_t157_il2cpp_TypeInfo__nestedTypes[2] =
{
	&WebCamUsed_t763_il2cpp_TypeInfo,
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
extern MethodInfo Object_ToString_m306_MethodInfo;
static MethodInfo* QCARRuntimeUtilities_t157_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType QCARRuntimeUtilities_t157_0_0_0;
extern Il2CppType QCARRuntimeUtilities_t157_1_0_0;
extern TypeInfo Object_t_il2cpp_TypeInfo;
struct QCARRuntimeUtilities_t157;
TypeInfo QCARRuntimeUtilities_t157_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "QCARRuntimeUtilities"/* name */
	, "Vuforia"/* namespaze */
	, QCARRuntimeUtilities_t157_MethodInfos/* methods */
	, QCARRuntimeUtilities_t157_PropertyInfos/* properties */
	, QCARRuntimeUtilities_t157_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, QCARRuntimeUtilities_t157_il2cpp_TypeInfo__nestedTypes/* nested_types */
	, NULL/* nested_in */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, QCARRuntimeUtilities_t157_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &QCARRuntimeUtilities_t157_il2cpp_TypeInfo/* cast_class */
	, &QCARRuntimeUtilities_t157_0_0_0/* byval_arg */
	, &QCARRuntimeUtilities_t157_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (QCARRuntimeUtilities_t157)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(QCARRuntimeUtilities_t157_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048577/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 18/* method_count */
	, 3/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.SurfaceUtilities
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SurfaceUtilities.h"
#ifndef _MSC_VER
#else
#endif

// Vuforia.QCARRendererImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl.h"
// Vuforia.QCARRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl_Re.h"
extern TypeInfo QCARRenderer_t661_il2cpp_TypeInfo;
extern TypeInfo QCARWrapper_t709_il2cpp_TypeInfo;
extern TypeInfo IQCARWrapper_t708_il2cpp_TypeInfo;
extern TypeInfo Boolean_t106_il2cpp_TypeInfo;
extern TypeInfo QCARUnityImpl_t664_il2cpp_TypeInfo;
extern TypeInfo Void_t99_il2cpp_TypeInfo;
// Vuforia.QCARRenderer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererMethodDeclarations.h"
// Vuforia.QCARRendererImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImplMethodDeclarations.h"
// Vuforia.QCARWrapper
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARWrapperMethodDeclarations.h"
// Vuforia.QCARUnityImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnityImplMethodDeclarations.h"
extern MethodInfo QCARRenderer_get_InternalInstance_m3009_MethodInfo;
extern MethodInfo QCARRendererImpl_UnityRenderEvent_m3020_MethodInfo;
extern MethodInfo QCARWrapper_get_Instance_m3923_MethodInfo;
extern MethodInfo IQCARWrapper_HasSurfaceBeenRecreated_m5086_MethodInfo;
extern MethodInfo QCARUnityImpl_SetRendererDirty_m3030_MethodInfo;
extern MethodInfo IQCARWrapper_OnSurfaceChanged_m5085_MethodInfo;


// System.Void Vuforia.SurfaceUtilities::OnSurfaceCreated()
extern MethodInfo SurfaceUtilities_OnSurfaceCreated_m447_MethodInfo;
 void SurfaceUtilities_OnSurfaceCreated_m447 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRenderer_t661_il2cpp_TypeInfo));
		QCARRendererImpl_t662 * L_0 = QCARRenderer_get_InternalInstance_m3009(NULL /*static, unused*/, /*hidden argument*/&QCARRenderer_get_InternalInstance_m3009_MethodInfo);
		NullCheck(L_0);
		QCARRendererImpl_UnityRenderEvent_m3020(L_0, ((int32_t)101), /*hidden argument*/&QCARRendererImpl_UnityRenderEvent_m3020_MethodInfo);
		return;
	}
}
// System.Void Vuforia.SurfaceUtilities::OnSurfaceDeinit()
extern MethodInfo SurfaceUtilities_OnSurfaceDeinit_m4168_MethodInfo;
 void SurfaceUtilities_OnSurfaceDeinit_m4168 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRenderer_t661_il2cpp_TypeInfo));
		QCARRendererImpl_t662 * L_0 = QCARRenderer_get_InternalInstance_m3009(NULL /*static, unused*/, /*hidden argument*/&QCARRenderer_get_InternalInstance_m3009_MethodInfo);
		NullCheck(L_0);
		QCARRendererImpl_UnityRenderEvent_m3020(L_0, ((int32_t)105), /*hidden argument*/&QCARRendererImpl_UnityRenderEvent_m3020_MethodInfo);
		return;
	}
}
// System.Boolean Vuforia.SurfaceUtilities::HasSurfaceBeenRecreated()
extern MethodInfo SurfaceUtilities_HasSurfaceBeenRecreated_m442_MethodInfo;
 bool SurfaceUtilities_HasSurfaceBeenRecreated_m442 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARWrapper_t709_il2cpp_TypeInfo));
		Object_t * L_0 = QCARWrapper_get_Instance_m3923(NULL /*static, unused*/, /*hidden argument*/&QCARWrapper_get_Instance_m3923_MethodInfo);
		NullCheck(L_0);
		bool L_1 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&IQCARWrapper_HasSurfaceBeenRecreated_m5086_MethodInfo, L_0);
		return L_1;
	}
}
// System.Void Vuforia.SurfaceUtilities::OnSurfaceChanged(System.Int32,System.Int32)
extern MethodInfo SurfaceUtilities_OnSurfaceChanged_m4169_MethodInfo;
 void SurfaceUtilities_OnSurfaceChanged_m4169 (Object_t * __this/* static, unused */, int32_t ___screenWidth, int32_t ___screenHeight, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARUnityImpl_t664_il2cpp_TypeInfo));
		QCARUnityImpl_SetRendererDirty_m3030(NULL /*static, unused*/, /*hidden argument*/&QCARUnityImpl_SetRendererDirty_m3030_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARWrapper_t709_il2cpp_TypeInfo));
		Object_t * L_0 = QCARWrapper_get_Instance_m3923(NULL /*static, unused*/, /*hidden argument*/&QCARWrapper_get_Instance_m3923_MethodInfo);
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, int32_t >::Invoke(&IQCARWrapper_OnSurfaceChanged_m5085_MethodInfo, L_0, ___screenWidth, ___screenHeight);
		return;
	}
}
// System.Void Vuforia.SurfaceUtilities::SetSurfaceOrientation(UnityEngine.ScreenOrientation)
extern MethodInfo SurfaceUtilities_SetSurfaceOrientation_m448_MethodInfo;
 void SurfaceUtilities_SetSurfaceOrientation_m448 (Object_t * __this/* static, unused */, int32_t ___screenOrientation, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SurfaceUtilities_t137_il2cpp_TypeInfo));
		((SurfaceUtilities_t137_StaticFields*)InitializedTypeInfo(&SurfaceUtilities_t137_il2cpp_TypeInfo)->static_fields)->___mScreenOrientation_0 = ___screenOrientation;
		return;
	}
}
// UnityEngine.ScreenOrientation Vuforia.SurfaceUtilities::GetSurfaceOrientation()
 int32_t SurfaceUtilities_GetSurfaceOrientation_m4170 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&SurfaceUtilities_t137_il2cpp_TypeInfo));
		return (((SurfaceUtilities_t137_StaticFields*)InitializedTypeInfo(&SurfaceUtilities_t137_il2cpp_TypeInfo)->static_fields)->___mScreenOrientation_0);
	}
}
// System.Void Vuforia.SurfaceUtilities::.cctor()
extern MethodInfo SurfaceUtilities__cctor_m4171_MethodInfo;
 void SurfaceUtilities__cctor_m4171 (Object_t * __this/* static, unused */, MethodInfo* method){
	{
		return;
	}
}
// Metadata Definition Vuforia.SurfaceUtilities
extern Il2CppType ScreenOrientation_t138_0_0_17;
FieldInfo SurfaceUtilities_t137____mScreenOrientation_0_FieldInfo = 
{
	"mScreenOrientation"/* name */
	, &ScreenOrientation_t138_0_0_17/* type */
	, &SurfaceUtilities_t137_il2cpp_TypeInfo/* parent */
	, offsetof(SurfaceUtilities_t137_StaticFields, ___mScreenOrientation_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* SurfaceUtilities_t137_FieldInfos[] =
{
	&SurfaceUtilities_t137____mScreenOrientation_0_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::OnSurfaceCreated()
MethodInfo SurfaceUtilities_OnSurfaceCreated_m447_MethodInfo = 
{
	"OnSurfaceCreated"/* name */
	, (methodPointerType)&SurfaceUtilities_OnSurfaceCreated_m447/* method */
	, &SurfaceUtilities_t137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2358/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::OnSurfaceDeinit()
MethodInfo SurfaceUtilities_OnSurfaceDeinit_m4168_MethodInfo = 
{
	"OnSurfaceDeinit"/* name */
	, (methodPointerType)&SurfaceUtilities_OnSurfaceDeinit_m4168/* method */
	, &SurfaceUtilities_t137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2359/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.SurfaceUtilities::HasSurfaceBeenRecreated()
MethodInfo SurfaceUtilities_HasSurfaceBeenRecreated_m442_MethodInfo = 
{
	"HasSurfaceBeenRecreated"/* name */
	, (methodPointerType)&SurfaceUtilities_HasSurfaceBeenRecreated_m442/* method */
	, &SurfaceUtilities_t137_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2360/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo SurfaceUtilities_t137_SurfaceUtilities_OnSurfaceChanged_m4169_ParameterInfos[] = 
{
	{"screenWidth", 0, 134220009, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"screenHeight", 1, 134220010, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::OnSurfaceChanged(System.Int32,System.Int32)
MethodInfo SurfaceUtilities_OnSurfaceChanged_m4169_MethodInfo = 
{
	"OnSurfaceChanged"/* name */
	, (methodPointerType)&SurfaceUtilities_OnSurfaceChanged_m4169/* method */
	, &SurfaceUtilities_t137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, SurfaceUtilities_t137_SurfaceUtilities_OnSurfaceChanged_m4169_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2361/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ScreenOrientation_t138_0_0_0;
extern Il2CppType ScreenOrientation_t138_0_0_0;
static ParameterInfo SurfaceUtilities_t137_SurfaceUtilities_SetSurfaceOrientation_m448_ParameterInfos[] = 
{
	{"screenOrientation", 0, 134220011, &EmptyCustomAttributesCache, &ScreenOrientation_t138_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::SetSurfaceOrientation(UnityEngine.ScreenOrientation)
MethodInfo SurfaceUtilities_SetSurfaceOrientation_m448_MethodInfo = 
{
	"SetSurfaceOrientation"/* name */
	, (methodPointerType)&SurfaceUtilities_SetSurfaceOrientation_m448/* method */
	, &SurfaceUtilities_t137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, SurfaceUtilities_t137_SurfaceUtilities_SetSurfaceOrientation_m448_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2362/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ScreenOrientation_t138_0_0_0;
extern void* RuntimeInvoker_ScreenOrientation_t138 (MethodInfo* method, void* obj, void** args);
// UnityEngine.ScreenOrientation Vuforia.SurfaceUtilities::GetSurfaceOrientation()
MethodInfo SurfaceUtilities_GetSurfaceOrientation_m4170_MethodInfo = 
{
	"GetSurfaceOrientation"/* name */
	, (methodPointerType)&SurfaceUtilities_GetSurfaceOrientation_m4170/* method */
	, &SurfaceUtilities_t137_il2cpp_TypeInfo/* declaring_type */
	, &ScreenOrientation_t138_0_0_0/* return_type */
	, RuntimeInvoker_ScreenOrientation_t138/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 150/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2363/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.SurfaceUtilities::.cctor()
MethodInfo SurfaceUtilities__cctor_m4171_MethodInfo = 
{
	".cctor"/* name */
	, (methodPointerType)&SurfaceUtilities__cctor_m4171/* method */
	, &SurfaceUtilities_t137_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6289/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2364/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* SurfaceUtilities_t137_MethodInfos[] =
{
	&SurfaceUtilities_OnSurfaceCreated_m447_MethodInfo,
	&SurfaceUtilities_OnSurfaceDeinit_m4168_MethodInfo,
	&SurfaceUtilities_HasSurfaceBeenRecreated_m442_MethodInfo,
	&SurfaceUtilities_OnSurfaceChanged_m4169_MethodInfo,
	&SurfaceUtilities_SetSurfaceOrientation_m448_MethodInfo,
	&SurfaceUtilities_GetSurfaceOrientation_m4170_MethodInfo,
	&SurfaceUtilities__cctor_m4171_MethodInfo,
	NULL
};
static MethodInfo* SurfaceUtilities_t137_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType SurfaceUtilities_t137_0_0_0;
extern Il2CppType SurfaceUtilities_t137_1_0_0;
struct SurfaceUtilities_t137;
TypeInfo SurfaceUtilities_t137_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "SurfaceUtilities"/* name */
	, "Vuforia"/* namespaze */
	, SurfaceUtilities_t137_MethodInfos/* methods */
	, NULL/* properties */
	, SurfaceUtilities_t137_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SurfaceUtilities_t137_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SurfaceUtilities_t137_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SurfaceUtilities_t137_il2cpp_TypeInfo/* cast_class */
	, &SurfaceUtilities_t137_0_0_0/* byval_arg */
	, &SurfaceUtilities_t137_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SurfaceUtilities_t137)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(SurfaceUtilities_t137_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048961/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, true/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBeh.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo;
// Vuforia.TextRecoAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBehMethodDeclarations.h"

// Vuforia.WordFilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterMode.h"
// Vuforia.WordPrefabCreationMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordPrefabCreationM.h"
// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio.h"
// System.Action
#include "System_Core_System_Action.h"
// System.Object
#include "mscorlib_System_Object.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3.h"
// Vuforia.KeepAliveAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBe.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObject.h"
// Vuforia.TextTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTracker.h"
// Vuforia.TrackerManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManager.h"
// Vuforia.Tracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Tracker.h"
// Vuforia.WordList
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordList.h"
// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_44.h"
// Vuforia.WordResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordResult.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_19.h"
// Vuforia.WordManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordManager.h"
// Vuforia.StateManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManager.h"
// Vuforia.WordManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordManagerImpl.h"
extern TypeInfo QCARAbstractBehaviour_t71_il2cpp_TypeInfo;
extern TypeInfo Action_t148_il2cpp_TypeInfo;
extern TypeInfo Action_1_t758_il2cpp_TypeInfo;
extern TypeInfo TrackerManager_t743_il2cpp_TypeInfo;
extern TypeInfo TextTracker_t677_il2cpp_TypeInfo;
extern TypeInfo Tracker_t615_il2cpp_TypeInfo;
extern TypeInfo WordList_t678_il2cpp_TypeInfo;
extern TypeInfo List_1_t764_il2cpp_TypeInfo;
extern TypeInfo ITextRecoEventHandler_t765_il2cpp_TypeInfo;
extern TypeInfo String_t_il2cpp_TypeInfo;
extern TypeInfo WordFilterMode_t772_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t690_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t898_il2cpp_TypeInfo;
extern TypeInfo Word_t691_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t899_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t689_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t900_il2cpp_TypeInfo;
extern TypeInfo WordResult_t702_il2cpp_TypeInfo;
extern TypeInfo StateManager_t724_il2cpp_TypeInfo;
extern TypeInfo WordManager_t688_il2cpp_TypeInfo;
extern TypeInfo WordManagerImpl_t699_il2cpp_TypeInfo;
// System.Action
#include "System_Core_System_ActionMethodDeclarations.h"
// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavioMethodDeclarations.h"
// System.Action`1<System.Boolean>
#include "mscorlib_System_Action_1_gen_3MethodDeclarations.h"
// Vuforia.KeepAliveAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_KeepAliveAbstractBeMethodDeclarations.h"
// UnityEngine.Component
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
// Vuforia.TrackerManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerManagerMethodDeclarations.h"
// Vuforia.Tracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackerMethodDeclarations.h"
// Vuforia.TextTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TextTrackerMethodDeclarations.h"
// Vuforia.WordList
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordListMethodDeclarations.h"
// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_44MethodDeclarations.h"
// UnityEngine.Debug
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_19MethodDeclarations.h"
// Vuforia.StateManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_StateManagerMethodDeclarations.h"
// Vuforia.WordManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordManagerImplMethodDeclarations.h"
// Vuforia.WordManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordManagerMethodDeclarations.h"
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
extern Il2CppType QCARAbstractBehaviour_t71_0_0_0;
extern MethodInfo Object_op_Implicit_m397_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_OnQCARInitialized_m4184_MethodInfo;
extern MethodInfo Action__ctor_m4427_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_RegisterQCARInitializedCallback_m4112_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_OnQCARStarted_m4185_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_RegisterQCARStartedCallback_m4114_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_OnTrackablesUpdated_m4186_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_RegisterTrackablesUpdatedCallback_m4116_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_OnPause_m4187_MethodInfo;
extern MethodInfo Action_1__ctor_m4519_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_RegisterOnPauseCallback_m4118_MethodInfo;
extern MethodInfo KeepAliveAbstractBehaviour_get_Instance_m3939_MethodInfo;
extern MethodInfo Object_op_Inequality_m335_MethodInfo;
extern MethodInfo KeepAliveAbstractBehaviour_get_KeepTextRecoBehaviourAlive_m3931_MethodInfo;
extern MethodInfo Component_get_gameObject_m322_MethodInfo;
extern MethodInfo Object_DontDestroyOnLoad_m4377_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_StartTextTracker_m4180_MethodInfo;
extern MethodInfo TrackerManager_get_Instance_m4040_MethodInfo;
extern MethodInfo TrackerManager_GetTracker_TisTextTracker_t677_m5358_MethodInfo;
extern MethodInfo Tracker_get_IsActive_m2920_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_StopTextTracker_m4181_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_UnregisterQCARInitializedCallback_m4113_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4115_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_UnregisterTrackablesUpdatedCallback_m4117_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_UnregisterOnPauseCallback_m4119_MethodInfo;
extern MethodInfo TextTracker_get_WordList_m4934_MethodInfo;
extern MethodInfo WordList_UnloadAllLists_m5057_MethodInfo;
extern MethodInfo List_1_Add_m5375_MethodInfo;
extern MethodInfo ITextRecoEventHandler_OnInitialized_m5117_MethodInfo;
extern MethodInfo List_1_Remove_m5376_MethodInfo;
extern MethodInfo Debug_Log_m419_MethodInfo;
extern MethodInfo Tracker_Start_m4521_MethodInfo;
extern MethodInfo Tracker_Stop_m4522_MethodInfo;
extern MethodInfo WordList_LoadWordListFile_m5048_MethodInfo;
extern MethodInfo String_op_Inequality_m2280_MethodInfo;
extern MethodInfo WordList_AddWordsFromFile_m5051_MethodInfo;
extern MethodInfo String_get_Length_m2255_MethodInfo;
extern MethodInfo WordList_AddWord_m5054_MethodInfo;
extern MethodInfo WordList_SetFilterMode_m5059_MethodInfo;
extern MethodInfo WordList_LoadFilterListFile_m5060_MethodInfo;
extern MethodInfo WordList_AddWordToFilterList_m5063_MethodInfo;
extern MethodInfo IEnumerable_1_GetEnumerator_m5377_MethodInfo;
extern MethodInfo IEnumerator_1_get_Current_m5378_MethodInfo;
extern MethodInfo List_1_GetEnumerator_m5379_MethodInfo;
extern MethodInfo Enumerator_get_Current_m5380_MethodInfo;
extern MethodInfo ITextRecoEventHandler_OnWordLost_m5119_MethodInfo;
extern MethodInfo Enumerator_MoveNext_m5381_MethodInfo;
extern MethodInfo IDisposable_Dispose_m459_MethodInfo;
extern MethodInfo IEnumerator_MoveNext_m4433_MethodInfo;
extern MethodInfo IEnumerable_1_GetEnumerator_m5382_MethodInfo;
extern MethodInfo IEnumerator_1_get_Current_m5383_MethodInfo;
extern MethodInfo ITextRecoEventHandler_OnWordDetected_m5118_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_get_HasStarted_m4103_MethodInfo;
extern MethodInfo TrackerManager_InitTracker_TisTextTracker_t677_m5384_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_SetupWordList_m4182_MethodInfo;
extern MethodInfo TrackerManager_GetStateManager_m4614_MethodInfo;
extern MethodInfo StateManager_GetWordManager_m4939_MethodInfo;
extern MethodInfo WordManagerImpl_InitializeWordBehaviourTemplates_m3103_MethodInfo;
extern MethodInfo WordManager_GetNewWords_m4961_MethodInfo;
extern MethodInfo WordManager_GetLostWords_m4962_MethodInfo;
extern MethodInfo TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4183_MethodInfo;
extern MethodInfo List_1__ctor_m5385_MethodInfo;
extern MethodInfo MonoBehaviour__ctor_m214_MethodInfo;
struct TrackerManager_t743;
// Declaration T Vuforia.TrackerManager::GetTracker<Vuforia.TextTracker>()
// T Vuforia.TrackerManager::GetTracker<Vuforia.TextTracker>()
struct TrackerManager_t743;
// Declaration T Vuforia.TrackerManager::InitTracker<Vuforia.TextTracker>()
// T Vuforia.TrackerManager::InitTracker<Vuforia.TextTracker>()


// System.Boolean Vuforia.TextRecoAbstractBehaviour::get_IsInitialized()
extern MethodInfo TextRecoAbstractBehaviour_get_IsInitialized_m4172_MethodInfo;
 bool TextRecoAbstractBehaviour_get_IsInitialized_m4172 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mHasInitialized_2);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Awake()
extern MethodInfo TextRecoAbstractBehaviour_Awake_m4173_MethodInfo;
 void TextRecoAbstractBehaviour_Awake_m4173 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	QCARAbstractBehaviour_t71 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m487(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsQCAREnabled_m487_MethodInfo);
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return;
	}

IL_0008:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t71_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		Object_t117 * L_2 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_1, /*hidden argument*/&Object_FindObjectOfType_m396_MethodInfo);
		V_0 = ((QCARAbstractBehaviour_t71 *)Castclass(L_2, InitializedTypeInfo(&QCARAbstractBehaviour_t71_il2cpp_TypeInfo)));
		bool L_3 = Object_op_Implicit_m397(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_3)
		{
			goto IL_006d;
		}
	}
	{
		IntPtr_t121 L_4 = { &TextRecoAbstractBehaviour_OnQCARInitialized_m4184_MethodInfo };
		Action_t148 * L_5 = (Action_t148 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t148_il2cpp_TypeInfo));
		Action__ctor_m4427(L_5, __this, L_4, /*hidden argument*/&Action__ctor_m4427_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterQCARInitializedCallback_m4112(V_0, L_5, /*hidden argument*/&QCARAbstractBehaviour_RegisterQCARInitializedCallback_m4112_MethodInfo);
		IntPtr_t121 L_6 = { &TextRecoAbstractBehaviour_OnQCARStarted_m4185_MethodInfo };
		Action_t148 * L_7 = (Action_t148 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t148_il2cpp_TypeInfo));
		Action__ctor_m4427(L_7, __this, L_6, /*hidden argument*/&Action__ctor_m4427_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterQCARStartedCallback_m4114(V_0, L_7, /*hidden argument*/&QCARAbstractBehaviour_RegisterQCARStartedCallback_m4114_MethodInfo);
		IntPtr_t121 L_8 = { &TextRecoAbstractBehaviour_OnTrackablesUpdated_m4186_MethodInfo };
		Action_t148 * L_9 = (Action_t148 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t148_il2cpp_TypeInfo));
		Action__ctor_m4427(L_9, __this, L_8, /*hidden argument*/&Action__ctor_m4427_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterTrackablesUpdatedCallback_m4116(V_0, L_9, /*hidden argument*/&QCARAbstractBehaviour_RegisterTrackablesUpdatedCallback_m4116_MethodInfo);
		IntPtr_t121 L_10 = { &TextRecoAbstractBehaviour_OnPause_m4187_MethodInfo };
		Action_1_t758 * L_11 = (Action_1_t758 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t758_il2cpp_TypeInfo));
		Action_1__ctor_m4519(L_11, __this, L_10, /*hidden argument*/&Action_1__ctor_m4519_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterOnPauseCallback_m4118(V_0, L_11, /*hidden argument*/&QCARAbstractBehaviour_RegisterOnPauseCallback_m4118_MethodInfo);
	}

IL_006d:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Start()
extern MethodInfo TextRecoAbstractBehaviour_Start_m4174_MethodInfo;
 void TextRecoAbstractBehaviour_Start_m4174 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	{
		KeepAliveAbstractBehaviour_t64 * L_0 = KeepAliveAbstractBehaviour_get_Instance_m3939(NULL /*static, unused*/, /*hidden argument*/&KeepAliveAbstractBehaviour_get_Instance_m3939_MethodInfo);
		bool L_1 = Object_op_Inequality_m335(NULL /*static, unused*/, L_0, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		KeepAliveAbstractBehaviour_t64 * L_2 = KeepAliveAbstractBehaviour_get_Instance_m3939(NULL /*static, unused*/, /*hidden argument*/&KeepAliveAbstractBehaviour_get_Instance_m3939_MethodInfo);
		NullCheck(L_2);
		bool L_3 = KeepAliveAbstractBehaviour_get_KeepTextRecoBehaviourAlive_m3931(L_2, /*hidden argument*/&KeepAliveAbstractBehaviour_get_KeepTextRecoBehaviourAlive_m3931_MethodInfo);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		GameObject_t2 * L_4 = Component_get_gameObject_m322(__this, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		Object_DontDestroyOnLoad_m4377(NULL /*static, unused*/, L_4, /*hidden argument*/&Object_DontDestroyOnLoad_m4377_MethodInfo);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnEnable()
extern MethodInfo TextRecoAbstractBehaviour_OnEnable_m4175_MethodInfo;
 void TextRecoAbstractBehaviour_OnEnable_m4175 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mTrackerWasActiveBeforeDisabling_4);
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		TextRecoAbstractBehaviour_StartTextTracker_m4180(__this, /*hidden argument*/&TextRecoAbstractBehaviour_StartTextTracker_m4180_MethodInfo);
	}

IL_000e:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnDisable()
extern MethodInfo TextRecoAbstractBehaviour_OnDisable_m4176_MethodInfo;
 void TextRecoAbstractBehaviour_OnDisable_m4176 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	TextTracker_t677 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t743_il2cpp_TypeInfo));
		TrackerManager_t743 * L_0 = TrackerManager_get_Instance_m4040(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4040_MethodInfo);
		NullCheck(L_0);
		TextTracker_t677 * L_1 = (TextTracker_t677 *)GenericVirtFuncInvoker0< TextTracker_t677 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t677_m5358_MethodInfo, L_0);
		V_0 = L_1;
		if (!V_0)
		{
			goto IL_0028;
		}
	}
	{
		NullCheck(V_0);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&Tracker_get_IsActive_m2920_MethodInfo, V_0);
		__this->___mTrackerWasActiveBeforeDisabling_4 = L_2;
		NullCheck(V_0);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&Tracker_get_IsActive_m2920_MethodInfo, V_0);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		TextRecoAbstractBehaviour_StopTextTracker_m4181(__this, /*hidden argument*/&TextRecoAbstractBehaviour_StopTextTracker_m4181_MethodInfo);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnDestroy()
extern MethodInfo TextRecoAbstractBehaviour_OnDestroy_m4177_MethodInfo;
 void TextRecoAbstractBehaviour_OnDestroy_m4177 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	QCARAbstractBehaviour_t71 * V_0 = {0};
	TextTracker_t677 * V_1 = {0};
	WordList_t678 * V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t71_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		Object_t117 * L_1 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_FindObjectOfType_m396_MethodInfo);
		V_0 = ((QCARAbstractBehaviour_t71 *)Castclass(L_1, InitializedTypeInfo(&QCARAbstractBehaviour_t71_il2cpp_TypeInfo)));
		bool L_2 = Object_op_Implicit_m397(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_2)
		{
			goto IL_0065;
		}
	}
	{
		IntPtr_t121 L_3 = { &TextRecoAbstractBehaviour_OnQCARInitialized_m4184_MethodInfo };
		Action_t148 * L_4 = (Action_t148 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t148_il2cpp_TypeInfo));
		Action__ctor_m4427(L_4, __this, L_3, /*hidden argument*/&Action__ctor_m4427_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_UnregisterQCARInitializedCallback_m4113(V_0, L_4, /*hidden argument*/&QCARAbstractBehaviour_UnregisterQCARInitializedCallback_m4113_MethodInfo);
		IntPtr_t121 L_5 = { &TextRecoAbstractBehaviour_OnQCARStarted_m4185_MethodInfo };
		Action_t148 * L_6 = (Action_t148 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t148_il2cpp_TypeInfo));
		Action__ctor_m4427(L_6, __this, L_5, /*hidden argument*/&Action__ctor_m4427_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4115(V_0, L_6, /*hidden argument*/&QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4115_MethodInfo);
		IntPtr_t121 L_7 = { &TextRecoAbstractBehaviour_OnTrackablesUpdated_m4186_MethodInfo };
		Action_t148 * L_8 = (Action_t148 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t148_il2cpp_TypeInfo));
		Action__ctor_m4427(L_8, __this, L_7, /*hidden argument*/&Action__ctor_m4427_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_UnregisterTrackablesUpdatedCallback_m4117(V_0, L_8, /*hidden argument*/&QCARAbstractBehaviour_UnregisterTrackablesUpdatedCallback_m4117_MethodInfo);
		IntPtr_t121 L_9 = { &TextRecoAbstractBehaviour_OnPause_m4187_MethodInfo };
		Action_1_t758 * L_10 = (Action_1_t758 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t758_il2cpp_TypeInfo));
		Action_1__ctor_m4519(L_10, __this, L_9, /*hidden argument*/&Action_1__ctor_m4519_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_UnregisterOnPauseCallback_m4119(V_0, L_10, /*hidden argument*/&QCARAbstractBehaviour_UnregisterOnPauseCallback_m4119_MethodInfo);
	}

IL_0065:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t743_il2cpp_TypeInfo));
		TrackerManager_t743 * L_11 = TrackerManager_get_Instance_m4040(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4040_MethodInfo);
		NullCheck(L_11);
		TextTracker_t677 * L_12 = (TextTracker_t677 *)GenericVirtFuncInvoker0< TextTracker_t677 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t677_m5358_MethodInfo, L_11);
		V_1 = L_12;
		if (!V_1)
		{
			goto IL_0081;
		}
	}
	{
		NullCheck(V_1);
		WordList_t678 * L_13 = (WordList_t678 *)VirtFuncInvoker0< WordList_t678 * >::Invoke(&TextTracker_get_WordList_m4934_MethodInfo, V_1);
		V_2 = L_13;
		NullCheck(V_2);
		VirtFuncInvoker0< bool >::Invoke(&WordList_UnloadAllLists_m5057_MethodInfo, V_2);
	}

IL_0081:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::RegisterTextRecoEventHandler(Vuforia.ITextRecoEventHandler)
extern MethodInfo TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4178_MethodInfo;
 void TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4178 (TextRecoAbstractBehaviour_t61 * __this, Object_t * ___trackableEventHandler, MethodInfo* method){
	{
		List_1_t764 * L_0 = (__this->___mTextRecoEventHandlers_13);
		NullCheck(L_0);
		VirtActionInvoker1< Object_t * >::Invoke(&List_1_Add_m5375_MethodInfo, L_0, ___trackableEventHandler);
		bool L_1 = (__this->___mHasInitialized_2);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		NullCheck(___trackableEventHandler);
		InterfaceActionInvoker0::Invoke(&ITextRecoEventHandler_OnInitialized_m5117_MethodInfo, ___trackableEventHandler);
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean Vuforia.TextRecoAbstractBehaviour::UnregisterTextRecoEventHandler(Vuforia.ITextRecoEventHandler)
extern MethodInfo TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4179_MethodInfo;
 bool TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4179 (TextRecoAbstractBehaviour_t61 * __this, Object_t * ___trackableEventHandler, MethodInfo* method){
	{
		List_1_t764 * L_0 = (__this->___mTextRecoEventHandlers_13);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&List_1_Remove_m5376_MethodInfo, L_0, ___trackableEventHandler);
		return L_1;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::StartTextTracker()
 void TextRecoAbstractBehaviour_StartTextTracker_m4180 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	TextTracker_t677 * V_0 = {0};
	{
		Debug_Log_m419(NULL /*static, unused*/, (String_t*) &_stringLiteral286, /*hidden argument*/&Debug_Log_m419_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t743_il2cpp_TypeInfo));
		TrackerManager_t743 * L_0 = TrackerManager_get_Instance_m4040(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4040_MethodInfo);
		NullCheck(L_0);
		TextTracker_t677 * L_1 = (TextTracker_t677 *)GenericVirtFuncInvoker0< TextTracker_t677 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t677_m5358_MethodInfo, L_0);
		V_0 = L_1;
		if (!V_0)
		{
			goto IL_001f;
		}
	}
	{
		NullCheck(V_0);
		VirtFuncInvoker0< bool >::Invoke(&Tracker_Start_m4521_MethodInfo, V_0);
	}

IL_001f:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::StopTextTracker()
 void TextRecoAbstractBehaviour_StopTextTracker_m4181 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	TextTracker_t677 * V_0 = {0};
	{
		Debug_Log_m419(NULL /*static, unused*/, (String_t*) &_stringLiteral287, /*hidden argument*/&Debug_Log_m419_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t743_il2cpp_TypeInfo));
		TrackerManager_t743 * L_0 = TrackerManager_get_Instance_m4040(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4040_MethodInfo);
		NullCheck(L_0);
		TextTracker_t677 * L_1 = (TextTracker_t677 *)GenericVirtFuncInvoker0< TextTracker_t677 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t677_m5358_MethodInfo, L_0);
		V_0 = L_1;
		if (!V_0)
		{
			goto IL_001e;
		}
	}
	{
		NullCheck(V_0);
		VirtActionInvoker0::Invoke(&Tracker_Stop_m4522_MethodInfo, V_0);
	}

IL_001e:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::SetupWordList()
 void TextRecoAbstractBehaviour_SetupWordList_m4182 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	TextTracker_t677 * V_0 = {0};
	WordList_t678 * V_1 = {0};
	StringU5BU5D_t112* V_2 = {0};
	String_t* V_3 = {0};
	StringU5BU5D_t112* V_4 = {0};
	String_t* V_5 = {0};
	CharU5BU5D_t108* V_6 = {0};
	StringU5BU5D_t112* V_7 = {0};
	int32_t V_8 = 0;
	CharU5BU5D_t108* V_9 = {0};
	StringU5BU5D_t112* V_10 = {0};
	int32_t V_11 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t743_il2cpp_TypeInfo));
		TrackerManager_t743 * L_0 = TrackerManager_get_Instance_m4040(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4040_MethodInfo);
		NullCheck(L_0);
		TextTracker_t677 * L_1 = (TextTracker_t677 *)GenericVirtFuncInvoker0< TextTracker_t677 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t677_m5358_MethodInfo, L_0);
		V_0 = L_1;
		if (!V_0)
		{
			goto IL_0125;
		}
	}
	{
		NullCheck(V_0);
		WordList_t678 * L_2 = (WordList_t678 *)VirtFuncInvoker0< WordList_t678 * >::Invoke(&TextTracker_get_WordList_m4934_MethodInfo, V_0);
		V_1 = L_2;
		String_t* L_3 = (__this->___mWordListFile_5);
		NullCheck(V_1);
		VirtFuncInvoker1< bool, String_t* >::Invoke(&WordList_LoadWordListFile_m5048_MethodInfo, V_1, L_3);
		String_t* L_4 = (__this->___mCustomWordListFile_6);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_5 = String_op_Inequality_m2280(NULL /*static, unused*/, L_4, (String_t*) &_stringLiteral113, /*hidden argument*/&String_op_Inequality_m2280_MethodInfo);
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		String_t* L_6 = (__this->___mCustomWordListFile_6);
		NullCheck(V_1);
		VirtFuncInvoker1< int32_t, String_t* >::Invoke(&WordList_AddWordsFromFile_m5051_MethodInfo, V_1, L_6);
	}

IL_0044:
	{
		String_t* L_7 = (__this->___mAdditionalCustomWords_7);
		if (!L_7)
		{
			goto IL_009b;
		}
	}
	{
		String_t* L_8 = (__this->___mAdditionalCustomWords_7);
		V_6 = ((CharU5BU5D_t108*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t108_il2cpp_TypeInfo), 2));
		NullCheck(V_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_6, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(V_6, 0)) = (uint16_t)((int32_t)13);
		NullCheck(V_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_6, 1);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(V_6, 1)) = (uint16_t)((int32_t)10);
		NullCheck(L_8);
		StringU5BU5D_t112* L_9 = String_Split_m293(L_8, V_6, /*hidden argument*/&String_Split_m293_MethodInfo);
		V_2 = L_9;
		V_7 = V_2;
		V_8 = 0;
		goto IL_0093;
	}

IL_0076:
	{
		NullCheck(V_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_7, V_8);
		int32_t L_10 = V_8;
		V_3 = (*(String_t**)(String_t**)SZArrayLdElema(V_7, L_10));
		NullCheck(V_3);
		int32_t L_11 = String_get_Length_m2255(V_3, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		if ((((int32_t)L_11) <= ((int32_t)0)))
		{
			goto IL_008d;
		}
	}
	{
		NullCheck(V_1);
		VirtFuncInvoker1< bool, String_t* >::Invoke(&WordList_AddWord_m5054_MethodInfo, V_1, V_3);
	}

IL_008d:
	{
		V_8 = ((int32_t)(V_8+1));
	}

IL_0093:
	{
		NullCheck(V_7);
		if ((((int32_t)V_8) < ((int32_t)(((int32_t)(((Array_t *)V_7)->max_length))))))
		{
			goto IL_0076;
		}
	}

IL_009b:
	{
		int32_t L_12 = (__this->___mFilterMode_8);
		NullCheck(V_1);
		VirtFuncInvoker1< bool, int32_t >::Invoke(&WordList_SetFilterMode_m5059_MethodInfo, V_1, L_12);
		int32_t L_13 = (__this->___mFilterMode_8);
		if (!L_13)
		{
			goto IL_0125;
		}
	}
	{
		String_t* L_14 = (__this->___mFilterListFile_9);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		bool L_15 = String_op_Inequality_m2280(NULL /*static, unused*/, L_14, (String_t*) &_stringLiteral113, /*hidden argument*/&String_op_Inequality_m2280_MethodInfo);
		if (!L_15)
		{
			goto IL_00cf;
		}
	}
	{
		String_t* L_16 = (__this->___mFilterListFile_9);
		NullCheck(V_1);
		VirtFuncInvoker1< bool, String_t* >::Invoke(&WordList_LoadFilterListFile_m5060_MethodInfo, V_1, L_16);
	}

IL_00cf:
	{
		String_t* L_17 = (__this->___mAdditionalFilterWords_10);
		if (!L_17)
		{
			goto IL_0125;
		}
	}
	{
		String_t* L_18 = (__this->___mAdditionalFilterWords_10);
		V_9 = ((CharU5BU5D_t108*)SZArrayNew(InitializedTypeInfo(&CharU5BU5D_t108_il2cpp_TypeInfo), 1));
		NullCheck(V_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_9, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(V_9, 0)) = (uint16_t)((int32_t)10);
		NullCheck(L_18);
		StringU5BU5D_t112* L_19 = String_Split_m293(L_18, V_9, /*hidden argument*/&String_Split_m293_MethodInfo);
		V_4 = L_19;
		V_10 = V_4;
		V_11 = 0;
		goto IL_011d;
	}

IL_00fd:
	{
		NullCheck(V_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_10, V_11);
		int32_t L_20 = V_11;
		V_5 = (*(String_t**)(String_t**)SZArrayLdElema(V_10, L_20));
		NullCheck(V_5);
		int32_t L_21 = String_get_Length_m2255(V_5, /*hidden argument*/&String_get_Length_m2255_MethodInfo);
		if ((((int32_t)L_21) <= ((int32_t)0)))
		{
			goto IL_0117;
		}
	}
	{
		NullCheck(V_1);
		VirtFuncInvoker1< bool, String_t* >::Invoke(&WordList_AddWordToFilterList_m5063_MethodInfo, V_1, V_5);
	}

IL_0117:
	{
		V_11 = ((int32_t)(V_11+1));
	}

IL_011d:
	{
		NullCheck(V_10);
		if ((((int32_t)V_11) < ((int32_t)(((int32_t)(((Array_t *)V_10)->max_length))))))
		{
			goto IL_00fd;
		}
	}

IL_0125:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::NotifyEventHandlersOfChanges(System.Collections.Generic.IEnumerable`1<Vuforia.Word>,System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>)
 void TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4183 (TextRecoAbstractBehaviour_t61 * __this, Object_t* ___lostWords, Object_t* ___newWords, MethodInfo* method){
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	WordResult_t702 * V_2 = {0};
	Object_t * V_3 = {0};
	Object_t* V_4 = {0};
	Enumerator_t899  V_5 = {0};
	Object_t* V_6 = {0};
	Enumerator_t899  V_7 = {0};
	int32_t leaveInstructions[2] = {0};
	Exception_t152 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t152 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		NullCheck(___lostWords);
		Object_t* L_0 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(&IEnumerable_1_GetEnumerator_m5377_MethodInfo, ___lostWords);
		V_4 = L_0;
	}

IL_0008:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0049;
		}

IL_000a:
		{
			NullCheck(V_4);
			Object_t * L_1 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(&IEnumerator_1_get_Current_m5378_MethodInfo, V_4);
			V_0 = L_1;
			List_1_t764 * L_2 = (__this->___mTextRecoEventHandlers_13);
			NullCheck(L_2);
			Enumerator_t899  L_3 = List_1_GetEnumerator_m5379(L_2, /*hidden argument*/&List_1_GetEnumerator_m5379_MethodInfo);
			V_5 = L_3;
		}

IL_001f:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0030;
			}

IL_0021:
			{
				Object_t * L_4 = Enumerator_get_Current_m5380((&V_5), /*hidden argument*/&Enumerator_get_Current_m5380_MethodInfo);
				V_1 = L_4;
				NullCheck(V_1);
				InterfaceActionInvoker1< Object_t * >::Invoke(&ITextRecoEventHandler_OnWordLost_m5119_MethodInfo, V_1, V_0);
			}

IL_0030:
			{
				bool L_5 = Enumerator_MoveNext_m5381((&V_5), /*hidden argument*/&Enumerator_MoveNext_m5381_MethodInfo);
				if (L_5)
				{
					goto IL_0021;
				}
			}

IL_0039:
			{
				// IL_0039: leave.s IL_0049
				leaveInstructions[1] = 0x49; // 2
				THROW_SENTINEL(IL_0049);
				// finally target depth: 2
			}
		} // end try (depth: 2)
		catch(Il2CppFinallySentinel& e)
		{
			goto IL_003b;
		}
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t152 *)e.ex;
			goto IL_003b;
		}

IL_003b:
		{ // begin finally (depth: 2)
			NullCheck(Box(InitializedTypeInfo(&Enumerator_t899_il2cpp_TypeInfo), &(*(&V_5))));
			InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t899_il2cpp_TypeInfo), &(*(&V_5))));
			// finally node depth: 2
			switch (leaveInstructions[1])
			{
				case 0x49:
					goto IL_0049;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 2, __last_unhandled_exception has not been set");
					#endif
					Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		} // end finally (depth: 2)

IL_0049:
		{
			NullCheck(V_4);
			bool L_6 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&IEnumerator_MoveNext_m4433_MethodInfo, V_4);
			if (L_6)
			{
				goto IL_000a;
			}
		}

IL_0052:
		{
			// IL_0052: leave.s IL_0060
			leaveInstructions[0] = 0x60; // 1
			THROW_SENTINEL(IL_0060);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0054;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t152 *)e.ex;
		goto IL_0054;
	}

IL_0054:
	{ // begin finally (depth: 1)
		{
			if (!V_4)
			{
				goto IL_005f;
			}
		}

IL_0058:
		{
			NullCheck(V_4);
			InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, V_4);
		}

IL_005f:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0x60:
					goto IL_0060;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_0060:
	{
		NullCheck(___newWords);
		Object_t* L_7 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(&IEnumerable_1_GetEnumerator_m5382_MethodInfo, ___newWords);
		V_6 = L_7;
	}

IL_0068:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00a9;
		}

IL_006a:
		{
			NullCheck(V_6);
			WordResult_t702 * L_8 = (WordResult_t702 *)InterfaceFuncInvoker0< WordResult_t702 * >::Invoke(&IEnumerator_1_get_Current_m5383_MethodInfo, V_6);
			V_2 = L_8;
			List_1_t764 * L_9 = (__this->___mTextRecoEventHandlers_13);
			NullCheck(L_9);
			Enumerator_t899  L_10 = List_1_GetEnumerator_m5379(L_9, /*hidden argument*/&List_1_GetEnumerator_m5379_MethodInfo);
			V_7 = L_10;
		}

IL_007f:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0090;
			}

IL_0081:
			{
				Object_t * L_11 = Enumerator_get_Current_m5380((&V_7), /*hidden argument*/&Enumerator_get_Current_m5380_MethodInfo);
				V_3 = L_11;
				NullCheck(V_3);
				InterfaceActionInvoker1< WordResult_t702 * >::Invoke(&ITextRecoEventHandler_OnWordDetected_m5118_MethodInfo, V_3, V_2);
			}

IL_0090:
			{
				bool L_12 = Enumerator_MoveNext_m5381((&V_7), /*hidden argument*/&Enumerator_MoveNext_m5381_MethodInfo);
				if (L_12)
				{
					goto IL_0081;
				}
			}

IL_0099:
			{
				// IL_0099: leave.s IL_00a9
				leaveInstructions[1] = 0xA9; // 2
				THROW_SENTINEL(IL_00a9);
				// finally target depth: 2
			}
		} // end try (depth: 2)
		catch(Il2CppFinallySentinel& e)
		{
			goto IL_009b;
		}
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t152 *)e.ex;
			goto IL_009b;
		}

IL_009b:
		{ // begin finally (depth: 2)
			NullCheck(Box(InitializedTypeInfo(&Enumerator_t899_il2cpp_TypeInfo), &(*(&V_7))));
			InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t899_il2cpp_TypeInfo), &(*(&V_7))));
			// finally node depth: 2
			switch (leaveInstructions[1])
			{
				case 0xA9:
					goto IL_00a9;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 2, __last_unhandled_exception has not been set");
					#endif
					Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		} // end finally (depth: 2)

IL_00a9:
		{
			NullCheck(V_6);
			bool L_13 = (bool)InterfaceFuncInvoker0< bool >::Invoke(&IEnumerator_MoveNext_m4433_MethodInfo, V_6);
			if (L_13)
			{
				goto IL_006a;
			}
		}

IL_00b2:
		{
			// IL_00b2: leave.s IL_00c0
			leaveInstructions[0] = 0xC0; // 1
			THROW_SENTINEL(IL_00c0);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_00b4;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t152 *)e.ex;
		goto IL_00b4;
	}

IL_00b4:
	{ // begin finally (depth: 1)
		{
			if (!V_6)
			{
				goto IL_00bf;
			}
		}

IL_00b8:
		{
			NullCheck(V_6);
			InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, V_6);
		}

IL_00bf:
		{
			// finally node depth: 1
			switch (leaveInstructions[0])
			{
				case 0xC0:
					goto IL_00c0;
				default:
				{
					#if IL2CPP_DEBUG
					assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
					#endif
					Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
					__last_unhandled_exception = 0;
					il2cpp_codegen_raise_exception(_tmp_exception_local);
				}
			}
		}
	} // end finally (depth: 1)

IL_00c0:
	{
		return;
	}
}
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_WordListFile()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m590_MethodInfo;
 String_t* TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m590 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mWordListFile_5);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_WordListFile(System.String)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m591_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m591 (TextRecoAbstractBehaviour_t61 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___mWordListFile_5 = ___value;
		return;
	}
}
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_CustomWordListFile()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m592_MethodInfo;
 String_t* TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m592 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mCustomWordListFile_6);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_CustomWordListFile(System.String)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m593_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m593 (TextRecoAbstractBehaviour_t61 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___mCustomWordListFile_6 = ___value;
		return;
	}
}
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_AdditionalCustomWords()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m594_MethodInfo;
 String_t* TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m594 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mAdditionalCustomWords_7);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_AdditionalCustomWords(System.String)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m595_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m595 (TextRecoAbstractBehaviour_t61 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___mAdditionalCustomWords_7 = ___value;
		return;
	}
}
// Vuforia.WordFilterMode Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_FilterMode()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m596_MethodInfo;
 int32_t TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m596 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mFilterMode_8);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_FilterMode(Vuforia.WordFilterMode)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m597_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m597 (TextRecoAbstractBehaviour_t61 * __this, int32_t ___value, MethodInfo* method){
	{
		__this->___mFilterMode_8 = ___value;
		return;
	}
}
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_FilterListFile()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m598_MethodInfo;
 String_t* TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m598 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mFilterListFile_9);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_FilterListFile(System.String)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m599_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m599 (TextRecoAbstractBehaviour_t61 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___mFilterListFile_9 = ___value;
		return;
	}
}
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_AdditionalFilterWords()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m600_MethodInfo;
 String_t* TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m600 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mAdditionalFilterWords_10);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_AdditionalFilterWords(System.String)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m601_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m601 (TextRecoAbstractBehaviour_t61 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___mAdditionalFilterWords_10 = ___value;
		return;
	}
}
// Vuforia.WordPrefabCreationMode Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_WordPrefabCreationMode()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m602_MethodInfo;
 int32_t TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m602 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mWordPrefabCreationMode_11);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_WordPrefabCreationMode(Vuforia.WordPrefabCreationMode)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m603_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m603 (TextRecoAbstractBehaviour_t61 * __this, int32_t ___value, MethodInfo* method){
	{
		__this->___mWordPrefabCreationMode_11 = ___value;
		return;
	}
}
// System.Int32 Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_MaximumWordInstances()
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m604_MethodInfo;
 int32_t TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m604 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mMaximumWordInstances_12);
		return L_0;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_MaximumWordInstances(System.Int32)
extern MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m605_MethodInfo;
 void TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m605 (TextRecoAbstractBehaviour_t61 * __this, int32_t ___value, MethodInfo* method){
	{
		__this->___mMaximumWordInstances_12 = ___value;
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnQCARInitialized()
 void TextRecoAbstractBehaviour_OnQCARInitialized_m4184 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	bool V_0 = false;
	QCARAbstractBehaviour_t71 * V_1 = {0};
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t71_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		Object_t117 * L_1 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_FindObjectOfType_m396_MethodInfo);
		V_1 = ((QCARAbstractBehaviour_t71 *)Castclass(L_1, InitializedTypeInfo(&QCARAbstractBehaviour_t71_il2cpp_TypeInfo)));
		bool L_2 = Object_op_Implicit_m397(NULL /*static, unused*/, V_1, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		NullCheck(V_1);
		bool L_3 = QCARAbstractBehaviour_get_HasStarted_m4103(V_1, /*hidden argument*/&QCARAbstractBehaviour_get_HasStarted_m4103_MethodInfo);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		NullCheck(V_1);
		Behaviour_set_enabled_m216(V_1, 0, /*hidden argument*/&Behaviour_set_enabled_m216_MethodInfo);
		V_0 = 1;
	}

IL_0030:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t743_il2cpp_TypeInfo));
		TrackerManager_t743 * L_4 = TrackerManager_get_Instance_m4040(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4040_MethodInfo);
		NullCheck(L_4);
		TextTracker_t677 * L_5 = (TextTracker_t677 *)GenericVirtFuncInvoker0< TextTracker_t677 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t677_m5358_MethodInfo, L_4);
		if (L_5)
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t743_il2cpp_TypeInfo));
		TrackerManager_t743 * L_6 = TrackerManager_get_Instance_m4040(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4040_MethodInfo);
		NullCheck(L_6);
		GenericVirtFuncInvoker0< TextTracker_t677 * >::Invoke(&TrackerManager_InitTracker_TisTextTracker_t677_m5384_MethodInfo, L_6);
	}

IL_0047:
	{
		if (!V_0)
		{
			goto IL_0051;
		}
	}
	{
		NullCheck(V_1);
		Behaviour_set_enabled_m216(V_1, 1, /*hidden argument*/&Behaviour_set_enabled_m216_MethodInfo);
	}

IL_0051:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnQCARStarted()
 void TextRecoAbstractBehaviour_OnQCARStarted_m4185 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	WordManager_t688 * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t899  V_2 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t152 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t152 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		TextRecoAbstractBehaviour_SetupWordList_m4182(__this, /*hidden argument*/&TextRecoAbstractBehaviour_SetupWordList_m4182_MethodInfo);
		TextRecoAbstractBehaviour_StartTextTracker_m4180(__this, /*hidden argument*/&TextRecoAbstractBehaviour_StartTextTracker_m4180_MethodInfo);
		__this->___mHasInitialized_2 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t743_il2cpp_TypeInfo));
		TrackerManager_t743 * L_0 = TrackerManager_get_Instance_m4040(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4040_MethodInfo);
		NullCheck(L_0);
		StateManager_t724 * L_1 = (StateManager_t724 *)VirtFuncInvoker0< StateManager_t724 * >::Invoke(&TrackerManager_GetStateManager_m4614_MethodInfo, L_0);
		NullCheck(L_1);
		WordManager_t688 * L_2 = (WordManager_t688 *)VirtFuncInvoker0< WordManager_t688 * >::Invoke(&StateManager_GetWordManager_m4939_MethodInfo, L_1);
		V_0 = L_2;
		int32_t L_3 = (__this->___mWordPrefabCreationMode_11);
		int32_t L_4 = (__this->___mMaximumWordInstances_12);
		NullCheck(((WordManagerImpl_t699 *)Castclass(V_0, InitializedTypeInfo(&WordManagerImpl_t699_il2cpp_TypeInfo))));
		WordManagerImpl_InitializeWordBehaviourTemplates_m3103(((WordManagerImpl_t699 *)Castclass(V_0, InitializedTypeInfo(&WordManagerImpl_t699_il2cpp_TypeInfo))), L_3, L_4, /*hidden argument*/&WordManagerImpl_InitializeWordBehaviourTemplates_m3103_MethodInfo);
		List_1_t764 * L_5 = (__this->___mTextRecoEventHandlers_13);
		NullCheck(L_5);
		Enumerator_t899  L_6 = List_1_GetEnumerator_m5379(L_5, /*hidden argument*/&List_1_GetEnumerator_m5379_MethodInfo);
		V_2 = L_6;
	}

IL_0046:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0056;
		}

IL_0048:
		{
			Object_t * L_7 = Enumerator_get_Current_m5380((&V_2), /*hidden argument*/&Enumerator_get_Current_m5380_MethodInfo);
			V_1 = L_7;
			NullCheck(V_1);
			InterfaceActionInvoker0::Invoke(&ITextRecoEventHandler_OnInitialized_m5117_MethodInfo, V_1);
		}

IL_0056:
		{
			bool L_8 = Enumerator_MoveNext_m5381((&V_2), /*hidden argument*/&Enumerator_MoveNext_m5381_MethodInfo);
			if (L_8)
			{
				goto IL_0048;
			}
		}

IL_005f:
		{
			// IL_005f: leave.s IL_006f
			leaveInstructions[0] = 0x6F; // 1
			THROW_SENTINEL(IL_006f);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0061;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t152 *)e.ex;
		goto IL_0061;
	}

IL_0061:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t899_il2cpp_TypeInfo), &(*(&V_2))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t899_il2cpp_TypeInfo), &(*(&V_2))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x6F:
				goto IL_006f;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_006f:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnTrackablesUpdated()
 void TextRecoAbstractBehaviour_OnTrackablesUpdated_m4186 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	WordManagerImpl_t699 * V_0 = {0};
	Object_t* V_1 = {0};
	Object_t* V_2 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t743_il2cpp_TypeInfo));
		TrackerManager_t743 * L_0 = TrackerManager_get_Instance_m4040(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4040_MethodInfo);
		NullCheck(L_0);
		StateManager_t724 * L_1 = (StateManager_t724 *)VirtFuncInvoker0< StateManager_t724 * >::Invoke(&TrackerManager_GetStateManager_m4614_MethodInfo, L_0);
		NullCheck(L_1);
		WordManager_t688 * L_2 = (WordManager_t688 *)VirtFuncInvoker0< WordManager_t688 * >::Invoke(&StateManager_GetWordManager_m4939_MethodInfo, L_1);
		V_0 = ((WordManagerImpl_t699 *)Castclass(L_2, InitializedTypeInfo(&WordManagerImpl_t699_il2cpp_TypeInfo)));
		NullCheck(V_0);
		Object_t* L_3 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(&WordManager_GetNewWords_m4961_MethodInfo, V_0);
		V_1 = L_3;
		NullCheck(V_0);
		Object_t* L_4 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(&WordManager_GetLostWords_m4962_MethodInfo, V_0);
		V_2 = L_4;
		TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4183(__this, V_2, V_1, /*hidden argument*/&TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4183_MethodInfo);
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::OnPause(System.Boolean)
 void TextRecoAbstractBehaviour_OnPause_m4187 (TextRecoAbstractBehaviour_t61 * __this, bool ___pause, MethodInfo* method){
	TextTracker_t677 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t743_il2cpp_TypeInfo));
		TrackerManager_t743 * L_0 = TrackerManager_get_Instance_m4040(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4040_MethodInfo);
		NullCheck(L_0);
		TextTracker_t677 * L_1 = (TextTracker_t677 *)GenericVirtFuncInvoker0< TextTracker_t677 * >::Invoke(&TrackerManager_GetTracker_TisTextTracker_t677_m5358_MethodInfo, L_0);
		V_0 = L_1;
		if (!V_0)
		{
			goto IL_003a;
		}
	}
	{
		if (!___pause)
		{
			goto IL_002c;
		}
	}
	{
		NullCheck(V_0);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(&Tracker_get_IsActive_m2920_MethodInfo, V_0);
		__this->___mTrackerWasActiveBeforePause_3 = L_2;
		NullCheck(V_0);
		bool L_3 = (bool)VirtFuncInvoker0< bool >::Invoke(&Tracker_get_IsActive_m2920_MethodInfo, V_0);
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		TextRecoAbstractBehaviour_StopTextTracker_m4181(__this, /*hidden argument*/&TextRecoAbstractBehaviour_StopTextTracker_m4181_MethodInfo);
		return;
	}

IL_002c:
	{
		bool L_4 = (__this->___mTrackerWasActiveBeforePause_3);
		if (!L_4)
		{
			goto IL_003a;
		}
	}
	{
		TextRecoAbstractBehaviour_StartTextTracker_m4180(__this, /*hidden argument*/&TextRecoAbstractBehaviour_StartTextTracker_m4180_MethodInfo);
	}

IL_003a:
	{
		return;
	}
}
// System.Void Vuforia.TextRecoAbstractBehaviour::.ctor()
extern MethodInfo TextRecoAbstractBehaviour__ctor_m589_MethodInfo;
 void TextRecoAbstractBehaviour__ctor_m589 (TextRecoAbstractBehaviour_t61 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&List_1_t764_il2cpp_TypeInfo));
		List_1_t764 * L_0 = (List_1_t764 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t764_il2cpp_TypeInfo));
		List_1__ctor_m5385(L_0, /*hidden argument*/&List_1__ctor_m5385_MethodInfo);
		__this->___mTextRecoEventHandlers_13 = L_0;
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.TextRecoAbstractBehaviour
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo TextRecoAbstractBehaviour_t61____mHasInitialized_2_FieldInfo = 
{
	"mHasInitialized"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t61, ___mHasInitialized_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo TextRecoAbstractBehaviour_t61____mTrackerWasActiveBeforePause_3_FieldInfo = 
{
	"mTrackerWasActiveBeforePause"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t61, ___mTrackerWasActiveBeforePause_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo TextRecoAbstractBehaviour_t61____mTrackerWasActiveBeforeDisabling_4_FieldInfo = 
{
	"mTrackerWasActiveBeforeDisabling"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t61, ___mTrackerWasActiveBeforeDisabling_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mWordListFile;
FieldInfo TextRecoAbstractBehaviour_t61____mWordListFile_5_FieldInfo = 
{
	"mWordListFile"/* name */
	, &String_t_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t61, ___mWordListFile_5)/* data */
	, &TextRecoAbstractBehaviour_t61__CustomAttributeCache_mWordListFile/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mCustomWordListFile;
FieldInfo TextRecoAbstractBehaviour_t61____mCustomWordListFile_6_FieldInfo = 
{
	"mCustomWordListFile"/* name */
	, &String_t_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t61, ___mCustomWordListFile_6)/* data */
	, &TextRecoAbstractBehaviour_t61__CustomAttributeCache_mCustomWordListFile/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mAdditionalCustomWords;
FieldInfo TextRecoAbstractBehaviour_t61____mAdditionalCustomWords_7_FieldInfo = 
{
	"mAdditionalCustomWords"/* name */
	, &String_t_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t61, ___mAdditionalCustomWords_7)/* data */
	, &TextRecoAbstractBehaviour_t61__CustomAttributeCache_mAdditionalCustomWords/* custom_attributes_cache */

};
extern Il2CppType WordFilterMode_t772_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mFilterMode;
FieldInfo TextRecoAbstractBehaviour_t61____mFilterMode_8_FieldInfo = 
{
	"mFilterMode"/* name */
	, &WordFilterMode_t772_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t61, ___mFilterMode_8)/* data */
	, &TextRecoAbstractBehaviour_t61__CustomAttributeCache_mFilterMode/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mFilterListFile;
FieldInfo TextRecoAbstractBehaviour_t61____mFilterListFile_9_FieldInfo = 
{
	"mFilterListFile"/* name */
	, &String_t_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t61, ___mFilterListFile_9)/* data */
	, &TextRecoAbstractBehaviour_t61__CustomAttributeCache_mFilterListFile/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mAdditionalFilterWords;
FieldInfo TextRecoAbstractBehaviour_t61____mAdditionalFilterWords_10_FieldInfo = 
{
	"mAdditionalFilterWords"/* name */
	, &String_t_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t61, ___mAdditionalFilterWords_10)/* data */
	, &TextRecoAbstractBehaviour_t61__CustomAttributeCache_mAdditionalFilterWords/* custom_attributes_cache */

};
extern Il2CppType WordPrefabCreationMode_t687_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mWordPrefabCreationMode;
FieldInfo TextRecoAbstractBehaviour_t61____mWordPrefabCreationMode_11_FieldInfo = 
{
	"mWordPrefabCreationMode"/* name */
	, &WordPrefabCreationMode_t687_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t61, ___mWordPrefabCreationMode_11)/* data */
	, &TextRecoAbstractBehaviour_t61__CustomAttributeCache_mWordPrefabCreationMode/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mMaximumWordInstances;
FieldInfo TextRecoAbstractBehaviour_t61____mMaximumWordInstances_12_FieldInfo = 
{
	"mMaximumWordInstances"/* name */
	, &Int32_t93_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t61, ___mMaximumWordInstances_12)/* data */
	, &TextRecoAbstractBehaviour_t61__CustomAttributeCache_mMaximumWordInstances/* custom_attributes_cache */

};
extern Il2CppType List_1_t764_0_0_1;
FieldInfo TextRecoAbstractBehaviour_t61____mTextRecoEventHandlers_13_FieldInfo = 
{
	"mTextRecoEventHandlers"/* name */
	, &List_1_t764_0_0_1/* type */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, offsetof(TextRecoAbstractBehaviour_t61, ___mTextRecoEventHandlers_13)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* TextRecoAbstractBehaviour_t61_FieldInfos[] =
{
	&TextRecoAbstractBehaviour_t61____mHasInitialized_2_FieldInfo,
	&TextRecoAbstractBehaviour_t61____mTrackerWasActiveBeforePause_3_FieldInfo,
	&TextRecoAbstractBehaviour_t61____mTrackerWasActiveBeforeDisabling_4_FieldInfo,
	&TextRecoAbstractBehaviour_t61____mWordListFile_5_FieldInfo,
	&TextRecoAbstractBehaviour_t61____mCustomWordListFile_6_FieldInfo,
	&TextRecoAbstractBehaviour_t61____mAdditionalCustomWords_7_FieldInfo,
	&TextRecoAbstractBehaviour_t61____mFilterMode_8_FieldInfo,
	&TextRecoAbstractBehaviour_t61____mFilterListFile_9_FieldInfo,
	&TextRecoAbstractBehaviour_t61____mAdditionalFilterWords_10_FieldInfo,
	&TextRecoAbstractBehaviour_t61____mWordPrefabCreationMode_11_FieldInfo,
	&TextRecoAbstractBehaviour_t61____mMaximumWordInstances_12_FieldInfo,
	&TextRecoAbstractBehaviour_t61____mTextRecoEventHandlers_13_FieldInfo,
	NULL
};
static PropertyInfo TextRecoAbstractBehaviour_t61____IsInitialized_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, "IsInitialized"/* name */
	, &TextRecoAbstractBehaviour_get_IsInitialized_m4172_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_WordListFile_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.WordListFile"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m590_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m591_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_CustomWordListFile_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.CustomWordListFile"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m592_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m593_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_AdditionalCustomWords_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.AdditionalCustomWords"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m594_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m595_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_FilterMode_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.FilterMode"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m596_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m597_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_FilterListFile_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.FilterListFile"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m598_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m599_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_AdditionalFilterWords_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.AdditionalFilterWords"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m600_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m601_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_WordPrefabCreationMode_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.WordPrefabCreationMode"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m602_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m603_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_MaximumWordInstances_PropertyInfo = 
{
	&TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorTextRecoBehaviour.MaximumWordInstances"/* name */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m604_MethodInfo/* get */
	, &TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m605_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* TextRecoAbstractBehaviour_t61_PropertyInfos[] =
{
	&TextRecoAbstractBehaviour_t61____IsInitialized_PropertyInfo,
	&TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_WordListFile_PropertyInfo,
	&TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_CustomWordListFile_PropertyInfo,
	&TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_AdditionalCustomWords_PropertyInfo,
	&TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_FilterMode_PropertyInfo,
	&TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_FilterListFile_PropertyInfo,
	&TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_AdditionalFilterWords_PropertyInfo,
	&TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_WordPrefabCreationMode_PropertyInfo,
	&TextRecoAbstractBehaviour_t61____Vuforia_IEditorTextRecoBehaviour_MaximumWordInstances_PropertyInfo,
	NULL
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.TextRecoAbstractBehaviour::get_IsInitialized()
MethodInfo TextRecoAbstractBehaviour_get_IsInitialized_m4172_MethodInfo = 
{
	"get_IsInitialized"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_get_IsInitialized_m4172/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2365/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Awake()
MethodInfo TextRecoAbstractBehaviour_Awake_m4173_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Awake_m4173/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2366/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Start()
MethodInfo TextRecoAbstractBehaviour_Start_m4174_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Start_m4174/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2367/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnEnable()
MethodInfo TextRecoAbstractBehaviour_OnEnable_m4175_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnEnable_m4175/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2368/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnDisable()
MethodInfo TextRecoAbstractBehaviour_OnDisable_m4176_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnDisable_m4176/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2369/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnDestroy()
MethodInfo TextRecoAbstractBehaviour_OnDestroy_m4177_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnDestroy_m4177/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2370/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ITextRecoEventHandler_t765_0_0_0;
extern Il2CppType ITextRecoEventHandler_t765_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4178_ParameterInfos[] = 
{
	{"trackableEventHandler", 0, 134220012, &EmptyCustomAttributesCache, &ITextRecoEventHandler_t765_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::RegisterTextRecoEventHandler(Vuforia.ITextRecoEventHandler)
MethodInfo TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4178_MethodInfo = 
{
	"RegisterTextRecoEventHandler"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4178/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4178_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2371/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ITextRecoEventHandler_t765_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4179_ParameterInfos[] = 
{
	{"trackableEventHandler", 0, 134220013, &EmptyCustomAttributesCache, &ITextRecoEventHandler_t765_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.TextRecoAbstractBehaviour::UnregisterTextRecoEventHandler(Vuforia.ITextRecoEventHandler)
MethodInfo TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4179_MethodInfo = 
{
	"UnregisterTextRecoEventHandler"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4179/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4179_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2372/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::StartTextTracker()
MethodInfo TextRecoAbstractBehaviour_StartTextTracker_m4180_MethodInfo = 
{
	"StartTextTracker"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_StartTextTracker_m4180/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2373/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::StopTextTracker()
MethodInfo TextRecoAbstractBehaviour_StopTextTracker_m4181_MethodInfo = 
{
	"StopTextTracker"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_StopTextTracker_m4181/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2374/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::SetupWordList()
MethodInfo TextRecoAbstractBehaviour_SetupWordList_m4182_MethodInfo = 
{
	"SetupWordList"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_SetupWordList_m4182/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2375/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IEnumerable_1_t690_0_0_0;
extern Il2CppType IEnumerable_1_t690_0_0_0;
extern Il2CppType IEnumerable_1_t689_0_0_0;
extern Il2CppType IEnumerable_1_t689_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4183_ParameterInfos[] = 
{
	{"lostWords", 0, 134220014, &EmptyCustomAttributesCache, &IEnumerable_1_t690_0_0_0},
	{"newWords", 1, 134220015, &EmptyCustomAttributesCache, &IEnumerable_1_t689_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::NotifyEventHandlersOfChanges(System.Collections.Generic.IEnumerable`1<Vuforia.Word>,System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>)
MethodInfo TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4183_MethodInfo = 
{
	"NotifyEventHandlersOfChanges"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4183/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4183_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2376/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_WordListFile()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m590_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_WordListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m590/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2377/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m591_ParameterInfos[] = 
{
	{"value", 0, 134220016, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_WordListFile(System.String)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m591_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_WordListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m591/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m591_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2378/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_CustomWordListFile()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m592_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_CustomWordListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m592/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2379/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m593_ParameterInfos[] = 
{
	{"value", 0, 134220017, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_CustomWordListFile(System.String)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m593_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_CustomWordListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m593/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m593_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2380/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_AdditionalCustomWords()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m594_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_AdditionalCustomWords"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m594/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2381/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m595_ParameterInfos[] = 
{
	{"value", 0, 134220018, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_AdditionalCustomWords(System.String)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m595_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_AdditionalCustomWords"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m595/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m595_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2382/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WordFilterMode_t772_0_0_0;
extern void* RuntimeInvoker_WordFilterMode_t772 (MethodInfo* method, void* obj, void** args);
// Vuforia.WordFilterMode Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_FilterMode()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m596_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_FilterMode"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m596/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &WordFilterMode_t772_0_0_0/* return_type */
	, RuntimeInvoker_WordFilterMode_t772/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2383/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WordFilterMode_t772_0_0_0;
extern Il2CppType WordFilterMode_t772_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m597_ParameterInfos[] = 
{
	{"value", 0, 134220019, &EmptyCustomAttributesCache, &WordFilterMode_t772_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_FilterMode(Vuforia.WordFilterMode)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m597_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_FilterMode"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m597/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m597_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2384/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_FilterListFile()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m598_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_FilterListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m598/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2385/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m599_ParameterInfos[] = 
{
	{"value", 0, 134220020, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_FilterListFile(System.String)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m599_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_FilterListFile"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m599/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m599_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2386/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_AdditionalFilterWords()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m600_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_AdditionalFilterWords"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m600/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2387/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m601_ParameterInfos[] = 
{
	{"value", 0, 134220021, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_AdditionalFilterWords(System.String)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m601_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_AdditionalFilterWords"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m601/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m601_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2388/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WordPrefabCreationMode_t687_0_0_0;
extern void* RuntimeInvoker_WordPrefabCreationMode_t687 (MethodInfo* method, void* obj, void** args);
// Vuforia.WordPrefabCreationMode Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_WordPrefabCreationMode()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m602_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_WordPrefabCreationMode"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m602/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &WordPrefabCreationMode_t687_0_0_0/* return_type */
	, RuntimeInvoker_WordPrefabCreationMode_t687/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2389/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WordPrefabCreationMode_t687_0_0_0;
extern Il2CppType WordPrefabCreationMode_t687_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m603_ParameterInfos[] = 
{
	{"value", 0, 134220022, &EmptyCustomAttributesCache, &WordPrefabCreationMode_t687_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_WordPrefabCreationMode(Vuforia.WordPrefabCreationMode)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m603_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_WordPrefabCreationMode"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m603/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m603_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2390/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Int32 Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.get_MaximumWordInstances()
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m604_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.get_MaximumWordInstances"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m604/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2391/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m605_ParameterInfos[] = 
{
	{"value", 0, 134220023, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::Vuforia.IEditorTextRecoBehaviour.set_MaximumWordInstances(System.Int32)
MethodInfo TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m605_MethodInfo = 
{
	"Vuforia.IEditorTextRecoBehaviour.set_MaximumWordInstances"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m605/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m605_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2392/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnQCARInitialized()
MethodInfo TextRecoAbstractBehaviour_OnQCARInitialized_m4184_MethodInfo = 
{
	"OnQCARInitialized"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnQCARInitialized_m4184/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2393/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnQCARStarted()
MethodInfo TextRecoAbstractBehaviour_OnQCARStarted_m4185_MethodInfo = 
{
	"OnQCARStarted"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnQCARStarted_m4185/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2394/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnTrackablesUpdated()
MethodInfo TextRecoAbstractBehaviour_OnTrackablesUpdated_m4186_MethodInfo = 
{
	"OnTrackablesUpdated"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnTrackablesUpdated_m4186/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2395/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_OnPause_m4187_ParameterInfos[] = 
{
	{"pause", 0, 134220024, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_SByte_t129 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::OnPause(System.Boolean)
MethodInfo TextRecoAbstractBehaviour_OnPause_m4187_MethodInfo = 
{
	"OnPause"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour_OnPause_m4187/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_SByte_t129/* invoker_method */
	, TextRecoAbstractBehaviour_t61_TextRecoAbstractBehaviour_OnPause_m4187_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2396/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TextRecoAbstractBehaviour::.ctor()
MethodInfo TextRecoAbstractBehaviour__ctor_m589_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TextRecoAbstractBehaviour__ctor_m589/* method */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2397/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TextRecoAbstractBehaviour_t61_MethodInfos[] =
{
	&TextRecoAbstractBehaviour_get_IsInitialized_m4172_MethodInfo,
	&TextRecoAbstractBehaviour_Awake_m4173_MethodInfo,
	&TextRecoAbstractBehaviour_Start_m4174_MethodInfo,
	&TextRecoAbstractBehaviour_OnEnable_m4175_MethodInfo,
	&TextRecoAbstractBehaviour_OnDisable_m4176_MethodInfo,
	&TextRecoAbstractBehaviour_OnDestroy_m4177_MethodInfo,
	&TextRecoAbstractBehaviour_RegisterTextRecoEventHandler_m4178_MethodInfo,
	&TextRecoAbstractBehaviour_UnregisterTextRecoEventHandler_m4179_MethodInfo,
	&TextRecoAbstractBehaviour_StartTextTracker_m4180_MethodInfo,
	&TextRecoAbstractBehaviour_StopTextTracker_m4181_MethodInfo,
	&TextRecoAbstractBehaviour_SetupWordList_m4182_MethodInfo,
	&TextRecoAbstractBehaviour_NotifyEventHandlersOfChanges_m4183_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m590_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m591_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m592_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m593_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m594_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m595_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m596_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m597_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m598_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m599_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m600_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m601_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m602_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m603_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m604_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m605_MethodInfo,
	&TextRecoAbstractBehaviour_OnQCARInitialized_m4184_MethodInfo,
	&TextRecoAbstractBehaviour_OnQCARStarted_m4185_MethodInfo,
	&TextRecoAbstractBehaviour_OnTrackablesUpdated_m4186_MethodInfo,
	&TextRecoAbstractBehaviour_OnPause_m4187_MethodInfo,
	&TextRecoAbstractBehaviour__ctor_m589_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m229_MethodInfo;
extern MethodInfo Object_GetHashCode_m230_MethodInfo;
extern MethodInfo Object_ToString_m231_MethodInfo;
static MethodInfo* TextRecoAbstractBehaviour_t61_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordListFile_m590_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordListFile_m591_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_CustomWordListFile_m592_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_CustomWordListFile_m593_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalCustomWords_m594_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalCustomWords_m595_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterMode_m596_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterMode_m597_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_FilterListFile_m598_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_FilterListFile_m599_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_AdditionalFilterWords_m600_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_AdditionalFilterWords_m601_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_WordPrefabCreationMode_m602_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_WordPrefabCreationMode_m603_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_get_MaximumWordInstances_m604_MethodInfo,
	&TextRecoAbstractBehaviour_Vuforia_IEditorTextRecoBehaviour_set_MaximumWordInstances_m605_MethodInfo,
};
extern TypeInfo IEditorTextRecoBehaviour_t167_il2cpp_TypeInfo;
static TypeInfo* TextRecoAbstractBehaviour_t61_InterfacesTypeInfos[] = 
{
	&IEditorTextRecoBehaviour_t167_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair TextRecoAbstractBehaviour_t61_InterfacesOffsets[] = 
{
	{ &IEditorTextRecoBehaviour_t167_il2cpp_TypeInfo, 4},
};
extern TypeInfo SerializeField_t103_il2cpp_TypeInfo;
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern MethodInfo SerializeField__ctor_m259_MethodInfo;
extern TypeInfo HideInInspector_t777_il2cpp_TypeInfo;
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspector.h"
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspectorMethodDeclarations.h"
extern MethodInfo HideInInspector__ctor_m4300_MethodInfo;
void TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mWordListFile(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mCustomWordListFile(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mAdditionalCustomWords(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mFilterMode(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mFilterListFile(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mAdditionalFilterWords(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mWordPrefabCreationMode(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mMaximumWordInstances(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mWordListFile = {
2,
NULL,
&TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mWordListFile
};
CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mCustomWordListFile = {
2,
NULL,
&TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mCustomWordListFile
};
CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mAdditionalCustomWords = {
2,
NULL,
&TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mAdditionalCustomWords
};
CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mFilterMode = {
2,
NULL,
&TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mFilterMode
};
CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mFilterListFile = {
2,
NULL,
&TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mFilterListFile
};
CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mAdditionalFilterWords = {
2,
NULL,
&TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mAdditionalFilterWords
};
CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mWordPrefabCreationMode = {
2,
NULL,
&TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mWordPrefabCreationMode
};
CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mMaximumWordInstances = {
2,
NULL,
&TextRecoAbstractBehaviour_t61_CustomAttributesCacheGenerator_mMaximumWordInstances
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType TextRecoAbstractBehaviour_t61_0_0_0;
extern Il2CppType TextRecoAbstractBehaviour_t61_1_0_0;
extern TypeInfo MonoBehaviour_t6_il2cpp_TypeInfo;
struct TextRecoAbstractBehaviour_t61;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mWordListFile;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mCustomWordListFile;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mAdditionalCustomWords;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mFilterMode;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mFilterListFile;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mAdditionalFilterWords;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mWordPrefabCreationMode;
extern CustomAttributesCache TextRecoAbstractBehaviour_t61__CustomAttributeCache_mMaximumWordInstances;
TypeInfo TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "TextRecoAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TextRecoAbstractBehaviour_t61_MethodInfos/* methods */
	, TextRecoAbstractBehaviour_t61_PropertyInfos/* properties */
	, TextRecoAbstractBehaviour_t61_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* element_class */
	, TextRecoAbstractBehaviour_t61_InterfacesTypeInfos/* implemented_interfaces */
	, TextRecoAbstractBehaviour_t61_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TextRecoAbstractBehaviour_t61_il2cpp_TypeInfo/* cast_class */
	, &TextRecoAbstractBehaviour_t61_0_0_0/* byval_arg */
	, &TextRecoAbstractBehaviour_t61_1_0_0/* this_arg */
	, TextRecoAbstractBehaviour_t61_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TextRecoAbstractBehaviour_t61)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 33/* method_count */
	, 9/* property_count */
	, 12/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 20/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.SimpleTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SimpleTargetData.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo SimpleTargetData_t766_il2cpp_TypeInfo;
// Vuforia.SimpleTargetData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SimpleTargetDataMethodDeclarations.h"



// Metadata Definition Vuforia.SimpleTargetData
extern Il2CppType Int32_t93_0_0_6;
FieldInfo SimpleTargetData_t766____id_0_FieldInfo = 
{
	"id"/* name */
	, &Int32_t93_0_0_6/* type */
	, &SimpleTargetData_t766_il2cpp_TypeInfo/* parent */
	, offsetof(SimpleTargetData_t766, ___id_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_3;
FieldInfo SimpleTargetData_t766____unused_1_FieldInfo = 
{
	"unused"/* name */
	, &Int32_t93_0_0_3/* type */
	, &SimpleTargetData_t766_il2cpp_TypeInfo/* parent */
	, offsetof(SimpleTargetData_t766, ___unused_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* SimpleTargetData_t766_FieldInfos[] =
{
	&SimpleTargetData_t766____id_0_FieldInfo,
	&SimpleTargetData_t766____unused_1_FieldInfo,
	NULL
};
static MethodInfo* SimpleTargetData_t766_MethodInfos[] =
{
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
static MethodInfo* SimpleTargetData_t766_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType SimpleTargetData_t766_0_0_0;
extern Il2CppType SimpleTargetData_t766_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
TypeInfo SimpleTargetData_t766_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "SimpleTargetData"/* name */
	, "Vuforia"/* namespaze */
	, SimpleTargetData_t766_MethodInfos/* methods */
	, NULL/* properties */
	, SimpleTargetData_t766_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &SimpleTargetData_t766_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, SimpleTargetData_t766_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &SimpleTargetData_t766_il2cpp_TypeInfo/* cast_class */
	, &SimpleTargetData_t766_0_0_0/* byval_arg */
	, &SimpleTargetData_t766_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (SimpleTargetData_t766)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, sizeof(SimpleTargetData_t766 )/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, true/* is_blittable */
	, true/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeha.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo TurnOffAbstractBehaviour_t57_il2cpp_TypeInfo;
// Vuforia.TurnOffAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBehaMethodDeclarations.h"



// System.Void Vuforia.TurnOffAbstractBehaviour::.ctor()
extern MethodInfo TurnOffAbstractBehaviour__ctor_m606_MethodInfo;
 void TurnOffAbstractBehaviour__ctor_m606 (TurnOffAbstractBehaviour_t57 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.TurnOffAbstractBehaviour
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.TurnOffAbstractBehaviour::.ctor()
MethodInfo TurnOffAbstractBehaviour__ctor_m606_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&TurnOffAbstractBehaviour__ctor_m606/* method */
	, &TurnOffAbstractBehaviour_t57_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2398/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* TurnOffAbstractBehaviour_t57_MethodInfos[] =
{
	&TurnOffAbstractBehaviour__ctor_m606_MethodInfo,
	NULL
};
static MethodInfo* TurnOffAbstractBehaviour_t57_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType TurnOffAbstractBehaviour_t57_0_0_0;
extern Il2CppType TurnOffAbstractBehaviour_t57_1_0_0;
struct TurnOffAbstractBehaviour_t57;
TypeInfo TurnOffAbstractBehaviour_t57_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "TurnOffAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, TurnOffAbstractBehaviour_t57_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &TurnOffAbstractBehaviour_t57_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, TurnOffAbstractBehaviour_t57_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &TurnOffAbstractBehaviour_t57_il2cpp_TypeInfo/* cast_class */
	, &TurnOffAbstractBehaviour_t57_0_0_0/* byval_arg */
	, &TurnOffAbstractBehaviour_t57_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (TurnOffAbstractBehaviour_t57)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBu.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo;
// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UserDefinedTargetBuMethodDeclarations.h"

// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_45.h"
// Vuforia.ObjectTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTracker.h"
// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder.h"
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_20.h"
// Vuforia.TrackableSource
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableSource.h"
extern TypeInfo List_1_t767_il2cpp_TypeInfo;
extern TypeInfo IUserDefinedTargetEventHandler_t768_il2cpp_TypeInfo;
extern TypeInfo ObjectTracker_t561_il2cpp_TypeInfo;
extern TypeInfo ImageTargetBuilder_t601_il2cpp_TypeInfo;
extern TypeInfo Single_t105_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t901_il2cpp_TypeInfo;
extern TypeInfo FrameQuality_t600_il2cpp_TypeInfo;
extern TypeInfo TrackableSource_t586_il2cpp_TypeInfo;
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_45MethodDeclarations.h"
// Vuforia.ObjectTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTrackerMethodDeclarations.h"
// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilderMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.IUserDefinedTargetEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_20MethodDeclarations.h"
extern MethodInfo List_1_Add_m5386_MethodInfo;
extern MethodInfo IUserDefinedTargetEventHandler_OnInitialized_m5120_MethodInfo;
extern MethodInfo List_1_Remove_m5387_MethodInfo;
extern MethodInfo ObjectTracker_get_ImageTargetBuilder_m4762_MethodInfo;
extern MethodInfo ImageTargetBuilder_StartScan_m4583_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193_MethodInfo;
extern MethodInfo ImageTargetBuilder_Build_m4582_MethodInfo;
extern MethodInfo ImageTargetBuilder_StopScan_m4584_MethodInfo;
extern MethodInfo List_1_GetEnumerator_m5388_MethodInfo;
extern MethodInfo Enumerator_get_Current_m5389_MethodInfo;
extern MethodInfo IUserDefinedTargetEventHandler_OnFrameQualityChanged_m5121_MethodInfo;
extern MethodInfo Enumerator_MoveNext_m5390_MethodInfo;
extern MethodInfo KeepAliveAbstractBehaviour_get_KeepUDTBuildingBehaviourAlive_m3933_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4199_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4200_MethodInfo;
extern MethodInfo ImageTargetBuilder_GetFrameQuality_m4585_MethodInfo;
extern MethodInfo ImageTargetBuilder_GetTrackableSource_m4586_MethodInfo;
extern MethodInfo IUserDefinedTargetEventHandler_OnNewTrackableSource_m5122_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4192_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4190_MethodInfo;
extern MethodInfo TrackerManager_GetTracker_TisObjectTracker_t561_m4435_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4197_MethodInfo;
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4196_MethodInfo;
extern MethodInfo List_1__ctor_m5391_MethodInfo;
struct TrackerManager_t743;
// Declaration T Vuforia.TrackerManager::GetTracker<Vuforia.ObjectTracker>()
// T Vuforia.TrackerManager::GetTracker<Vuforia.ObjectTracker>()


// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::RegisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4188_MethodInfo;
 void UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4188 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, Object_t * ___eventHandler, MethodInfo* method){
	{
		List_1_t767 * L_0 = (__this->___mHandlers_9);
		NullCheck(L_0);
		VirtActionInvoker1< Object_t * >::Invoke(&List_1_Add_m5386_MethodInfo, L_0, ___eventHandler);
		bool L_1 = (__this->___mOnInitializedCalled_8);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		NullCheck(___eventHandler);
		InterfaceActionInvoker0::Invoke(&IUserDefinedTargetEventHandler_OnInitialized_m5120_MethodInfo, ___eventHandler);
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::UnregisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4189_MethodInfo;
 bool UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4189 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, Object_t * ___eventHandler, MethodInfo* method){
	{
		List_1_t767 * L_0 = (__this->___mHandlers_9);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&List_1_Remove_m5387_MethodInfo, L_0, ___eventHandler);
		return L_1;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StartScanning()
 void UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4190 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method){
	{
		ObjectTracker_t561 * L_0 = (__this->___mObjectTracker_2);
		if (!L_0)
		{
			goto IL_0032;
		}
	}
	{
		bool L_1 = (__this->___StopTrackerWhileScanning_10);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		ObjectTracker_t561 * L_2 = (__this->___mObjectTracker_2);
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(&Tracker_Stop_m4522_MethodInfo, L_2);
	}

IL_001b:
	{
		ObjectTracker_t561 * L_3 = (__this->___mObjectTracker_2);
		NullCheck(L_3);
		ImageTargetBuilder_t601 * L_4 = (ImageTargetBuilder_t601 *)VirtFuncInvoker0< ImageTargetBuilder_t601 * >::Invoke(&ObjectTracker_get_ImageTargetBuilder_m4762_MethodInfo, L_3);
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(&ImageTargetBuilder_StartScan_m4583_MethodInfo, L_4);
		__this->___mCurrentlyScanning_4 = 1;
	}

IL_0032:
	{
		UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193(__this, 0, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193_MethodInfo);
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::BuildNewTarget(System.String,System.Single)
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4191_MethodInfo;
 void UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4191 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method){
	{
		__this->___mCurrentlyBuilding_6 = 1;
		ObjectTracker_t561 * L_0 = (__this->___mObjectTracker_2);
		NullCheck(L_0);
		ImageTargetBuilder_t601 * L_1 = (ImageTargetBuilder_t601 *)VirtFuncInvoker0< ImageTargetBuilder_t601 * >::Invoke(&ObjectTracker_get_ImageTargetBuilder_m4762_MethodInfo, L_0);
		NullCheck(L_1);
		VirtFuncInvoker2< bool, String_t*, float >::Invoke(&ImageTargetBuilder_Build_m4582_MethodInfo, L_1, ___targetName, ___sceenSizeWidth);
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopScanning()
 void UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4192 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method){
	{
		__this->___mCurrentlyScanning_4 = 0;
		ObjectTracker_t561 * L_0 = (__this->___mObjectTracker_2);
		NullCheck(L_0);
		ImageTargetBuilder_t601 * L_1 = (ImageTargetBuilder_t601 *)VirtFuncInvoker0< ImageTargetBuilder_t601 * >::Invoke(&ObjectTracker_get_ImageTargetBuilder_m4762_MethodInfo, L_0);
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(&ImageTargetBuilder_StopScan_m4584_MethodInfo, L_1);
		bool L_2 = (__this->___StopTrackerWhileScanning_10);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		ObjectTracker_t561 * L_3 = (__this->___mObjectTracker_2);
		NullCheck(L_3);
		VirtFuncInvoker0< bool >::Invoke(&Tracker_Start_m4521_MethodInfo, L_3);
	}

IL_002b:
	{
		UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193(__this, (-1), /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193_MethodInfo);
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::SetFrameQuality(Vuforia.ImageTargetBuilder/FrameQuality)
 void UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, int32_t ___frameQuality, MethodInfo* method){
	Object_t * V_0 = {0};
	Enumerator_t901  V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t152 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t152 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		int32_t L_0 = (__this->___mLastFrameQuality_3);
		if ((((int32_t)___frameQuality) == ((int32_t)L_0)))
		{
			goto IL_0046;
		}
	}
	{
		List_1_t767 * L_1 = (__this->___mHandlers_9);
		NullCheck(L_1);
		Enumerator_t901  L_2 = List_1_GetEnumerator_m5388(L_1, /*hidden argument*/&List_1_GetEnumerator_m5388_MethodInfo);
		V_1 = L_2;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0026;
		}

IL_0017:
		{
			Object_t * L_3 = Enumerator_get_Current_m5389((&V_1), /*hidden argument*/&Enumerator_get_Current_m5389_MethodInfo);
			V_0 = L_3;
			NullCheck(V_0);
			InterfaceActionInvoker1< int32_t >::Invoke(&IUserDefinedTargetEventHandler_OnFrameQualityChanged_m5121_MethodInfo, V_0, ___frameQuality);
		}

IL_0026:
		{
			bool L_4 = Enumerator_MoveNext_m5390((&V_1), /*hidden argument*/&Enumerator_MoveNext_m5390_MethodInfo);
			if (L_4)
			{
				goto IL_0017;
			}
		}

IL_002f:
		{
			// IL_002f: leave.s IL_003f
			leaveInstructions[0] = 0x3F; // 1
			THROW_SENTINEL(IL_003f);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0031;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t152 *)e.ex;
		goto IL_0031;
	}

IL_0031:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t901_il2cpp_TypeInfo), &(*(&V_1))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t901_il2cpp_TypeInfo), &(*(&V_1))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x3F:
				goto IL_003f;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_003f:
	{
		__this->___mLastFrameQuality_3 = ___frameQuality;
	}

IL_0046:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Start()
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_Start_m4194_MethodInfo;
 void UserDefinedTargetBuildingAbstractBehaviour_Start_m4194 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method){
	QCARAbstractBehaviour_t71 * V_0 = {0};
	{
		KeepAliveAbstractBehaviour_t64 * L_0 = KeepAliveAbstractBehaviour_get_Instance_m3939(NULL /*static, unused*/, /*hidden argument*/&KeepAliveAbstractBehaviour_get_Instance_m3939_MethodInfo);
		bool L_1 = Object_op_Inequality_m335(NULL /*static, unused*/, L_0, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		KeepAliveAbstractBehaviour_t64 * L_2 = KeepAliveAbstractBehaviour_get_Instance_m3939(NULL /*static, unused*/, /*hidden argument*/&KeepAliveAbstractBehaviour_get_Instance_m3939_MethodInfo);
		NullCheck(L_2);
		bool L_3 = KeepAliveAbstractBehaviour_get_KeepUDTBuildingBehaviourAlive_m3933(L_2, /*hidden argument*/&KeepAliveAbstractBehaviour_get_KeepUDTBuildingBehaviourAlive_m3933_MethodInfo);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		GameObject_t2 * L_4 = Component_get_gameObject_m322(__this, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		Object_DontDestroyOnLoad_m4377(NULL /*static, unused*/, L_4, /*hidden argument*/&Object_DontDestroyOnLoad_m4377_MethodInfo);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_5 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t71_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		Object_t117 * L_6 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_5, /*hidden argument*/&Object_FindObjectOfType_m396_MethodInfo);
		V_0 = ((QCARAbstractBehaviour_t71 *)Castclass(L_6, InitializedTypeInfo(&QCARAbstractBehaviour_t71_il2cpp_TypeInfo)));
		bool L_7 = Object_op_Implicit_m397(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_7)
		{
			goto IL_0065;
		}
	}
	{
		IntPtr_t121 L_8 = { &UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4199_MethodInfo };
		Action_t148 * L_9 = (Action_t148 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t148_il2cpp_TypeInfo));
		Action__ctor_m4427(L_9, __this, L_8, /*hidden argument*/&Action__ctor_m4427_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterQCARStartedCallback_m4114(V_0, L_9, /*hidden argument*/&QCARAbstractBehaviour_RegisterQCARStartedCallback_m4114_MethodInfo);
		IntPtr_t121 L_10 = { &UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4200_MethodInfo };
		Action_1_t758 * L_11 = (Action_1_t758 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t758_il2cpp_TypeInfo));
		Action_1__ctor_m4519(L_11, __this, L_10, /*hidden argument*/&Action_1__ctor_m4519_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterOnPauseCallback_m4118(V_0, L_11, /*hidden argument*/&QCARAbstractBehaviour_RegisterOnPauseCallback_m4118_MethodInfo);
	}

IL_0065:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Update()
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_Update_m4195_MethodInfo;
 void UserDefinedTargetBuildingAbstractBehaviour_Update_m4195 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method){
	TrackableSource_t586 * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t901  V_2 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t152 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t152 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		bool L_0 = (__this->___mOnInitializedCalled_8);
		if (!L_0)
		{
			goto IL_0090;
		}
	}
	{
		bool L_1 = (__this->___mCurrentlyScanning_4);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		ObjectTracker_t561 * L_2 = (__this->___mObjectTracker_2);
		NullCheck(L_2);
		ImageTargetBuilder_t601 * L_3 = (ImageTargetBuilder_t601 *)VirtFuncInvoker0< ImageTargetBuilder_t601 * >::Invoke(&ObjectTracker_get_ImageTargetBuilder_m4762_MethodInfo, L_2);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(&ImageTargetBuilder_GetFrameQuality_m4585_MethodInfo, L_3);
		UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193(__this, L_4, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193_MethodInfo);
	}

IL_0029:
	{
		bool L_5 = (__this->___mCurrentlyBuilding_6);
		if (!L_5)
		{
			goto IL_0090;
		}
	}
	{
		ObjectTracker_t561 * L_6 = (__this->___mObjectTracker_2);
		NullCheck(L_6);
		ImageTargetBuilder_t601 * L_7 = (ImageTargetBuilder_t601 *)VirtFuncInvoker0< ImageTargetBuilder_t601 * >::Invoke(&ObjectTracker_get_ImageTargetBuilder_m4762_MethodInfo, L_6);
		NullCheck(L_7);
		TrackableSource_t586 * L_8 = (TrackableSource_t586 *)VirtFuncInvoker0< TrackableSource_t586 * >::Invoke(&ImageTargetBuilder_GetTrackableSource_m4586_MethodInfo, L_7);
		V_0 = L_8;
		if (!V_0)
		{
			goto IL_0090;
		}
	}
	{
		__this->___mCurrentlyBuilding_6 = 0;
		List_1_t767 * L_9 = (__this->___mHandlers_9);
		NullCheck(L_9);
		Enumerator_t901  L_10 = List_1_GetEnumerator_m5388(L_9, /*hidden argument*/&List_1_GetEnumerator_m5388_MethodInfo);
		V_2 = L_10;
	}

IL_0058:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0069;
		}

IL_005a:
		{
			Object_t * L_11 = Enumerator_get_Current_m5389((&V_2), /*hidden argument*/&Enumerator_get_Current_m5389_MethodInfo);
			V_1 = L_11;
			NullCheck(V_1);
			InterfaceActionInvoker1< TrackableSource_t586 * >::Invoke(&IUserDefinedTargetEventHandler_OnNewTrackableSource_m5122_MethodInfo, V_1, V_0);
		}

IL_0069:
		{
			bool L_12 = Enumerator_MoveNext_m5390((&V_2), /*hidden argument*/&Enumerator_MoveNext_m5390_MethodInfo);
			if (L_12)
			{
				goto IL_005a;
			}
		}

IL_0072:
		{
			// IL_0072: leave.s IL_0082
			leaveInstructions[0] = 0x82; // 1
			THROW_SENTINEL(IL_0082);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0074;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t152 *)e.ex;
		goto IL_0074;
	}

IL_0074:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t901_il2cpp_TypeInfo), &(*(&V_2))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t901_il2cpp_TypeInfo), &(*(&V_2))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x82:
				goto IL_0082;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0082:
	{
		bool L_13 = (__this->___StopScanningWhenFinshedBuilding_12);
		if (!L_13)
		{
			goto IL_0090;
		}
	}
	{
		UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4192(__this, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4192_MethodInfo);
	}

IL_0090:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnEnable()
 void UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4196 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mOnInitializedCalled_8);
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		bool L_1 = (__this->___mWasScanningBeforeDisable_5);
		__this->___mCurrentlyScanning_4 = L_1;
		bool L_2 = (__this->___mWasBuildingBeforeDisable_7);
		__this->___mCurrentlyBuilding_6 = L_2;
		bool L_3 = (__this->___mWasScanningBeforeDisable_5);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4190(__this, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4190_MethodInfo);
	}

IL_002e:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDisable()
 void UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4197 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mOnInitializedCalled_8);
		if (!L_0)
		{
			goto IL_002e;
		}
	}
	{
		bool L_1 = (__this->___mCurrentlyScanning_4);
		__this->___mWasScanningBeforeDisable_5 = L_1;
		bool L_2 = (__this->___mCurrentlyBuilding_6);
		__this->___mWasBuildingBeforeDisable_7 = L_2;
		bool L_3 = (__this->___mCurrentlyScanning_4);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4192(__this, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4192_MethodInfo);
	}

IL_002e:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDestroy()
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4198_MethodInfo;
 void UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4198 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method){
	QCARAbstractBehaviour_t71 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t71_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		Object_t117 * L_1 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_FindObjectOfType_m396_MethodInfo);
		V_0 = ((QCARAbstractBehaviour_t71 *)Castclass(L_1, InitializedTypeInfo(&QCARAbstractBehaviour_t71_il2cpp_TypeInfo)));
		bool L_2 = Object_op_Implicit_m397(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		IntPtr_t121 L_3 = { &UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4199_MethodInfo };
		Action_t148 * L_4 = (Action_t148 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t148_il2cpp_TypeInfo));
		Action__ctor_m4427(L_4, __this, L_3, /*hidden argument*/&Action__ctor_m4427_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4115(V_0, L_4, /*hidden argument*/&QCARAbstractBehaviour_UnregisterQCARStartedCallback_m4115_MethodInfo);
		IntPtr_t121 L_5 = { &UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4200_MethodInfo };
		Action_1_t758 * L_6 = (Action_1_t758 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_1_t758_il2cpp_TypeInfo));
		Action_1__ctor_m4519(L_6, __this, L_5, /*hidden argument*/&Action_1__ctor_m4519_MethodInfo);
		NullCheck(V_0);
		QCARAbstractBehaviour_UnregisterOnPauseCallback_m4119(V_0, L_6, /*hidden argument*/&QCARAbstractBehaviour_UnregisterOnPauseCallback_m4119_MethodInfo);
	}

IL_0041:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnQCARStarted()
 void UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4199 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method){
	Object_t * V_0 = {0};
	Enumerator_t901  V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t152 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t152 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		__this->___mOnInitializedCalled_8 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&TrackerManager_t743_il2cpp_TypeInfo));
		TrackerManager_t743 * L_0 = TrackerManager_get_Instance_m4040(NULL /*static, unused*/, /*hidden argument*/&TrackerManager_get_Instance_m4040_MethodInfo);
		NullCheck(L_0);
		ObjectTracker_t561 * L_1 = (ObjectTracker_t561 *)GenericVirtFuncInvoker0< ObjectTracker_t561 * >::Invoke(&TrackerManager_GetTracker_TisObjectTracker_t561_m4435_MethodInfo, L_0);
		__this->___mObjectTracker_2 = L_1;
		List_1_t767 * L_2 = (__this->___mHandlers_9);
		NullCheck(L_2);
		Enumerator_t901  L_3 = List_1_GetEnumerator_m5388(L_2, /*hidden argument*/&List_1_GetEnumerator_m5388_MethodInfo);
		V_1 = L_3;
	}

IL_0023:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0033;
		}

IL_0025:
		{
			Object_t * L_4 = Enumerator_get_Current_m5389((&V_1), /*hidden argument*/&Enumerator_get_Current_m5389_MethodInfo);
			V_0 = L_4;
			NullCheck(V_0);
			InterfaceActionInvoker0::Invoke(&IUserDefinedTargetEventHandler_OnInitialized_m5120_MethodInfo, V_0);
		}

IL_0033:
		{
			bool L_5 = Enumerator_MoveNext_m5390((&V_1), /*hidden argument*/&Enumerator_MoveNext_m5390_MethodInfo);
			if (L_5)
			{
				goto IL_0025;
			}
		}

IL_003c:
		{
			// IL_003c: leave.s IL_004c
			leaveInstructions[0] = 0x4C; // 1
			THROW_SENTINEL(IL_004c);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_003e;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t152 *)e.ex;
		goto IL_003e;
	}

IL_003e:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t901_il2cpp_TypeInfo), &(*(&V_1))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t901_il2cpp_TypeInfo), &(*(&V_1))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x4C:
				goto IL_004c;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_004c:
	{
		bool L_6 = (__this->___StartScanningAutomatically_11);
		if (!L_6)
		{
			goto IL_005a;
		}
	}
	{
		UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4190(__this, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4190_MethodInfo);
	}

IL_005a:
	{
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnPause(System.Boolean)
 void UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4200 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, bool ___pause, MethodInfo* method){
	{
		if (!___pause)
		{
			goto IL_000a;
		}
	}
	{
		UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4197(__this, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4197_MethodInfo);
		return;
	}

IL_000a:
	{
		UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4196(__this, /*hidden argument*/&UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4196_MethodInfo);
		return;
	}
}
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::.ctor()
extern MethodInfo UserDefinedTargetBuildingAbstractBehaviour__ctor_m611_MethodInfo;
 void UserDefinedTargetBuildingAbstractBehaviour__ctor_m611 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method){
	{
		__this->___mLastFrameQuality_3 = (-1);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&List_1_t767_il2cpp_TypeInfo));
		List_1_t767 * L_0 = (List_1_t767 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t767_il2cpp_TypeInfo));
		List_1__ctor_m5391(L_0, /*hidden argument*/&List_1__ctor_m5391_MethodInfo);
		__this->___mHandlers_9 = L_0;
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.UserDefinedTargetBuildingAbstractBehaviour
extern Il2CppType ObjectTracker_t561_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t82____mObjectTracker_2_FieldInfo = 
{
	"mObjectTracker"/* name */
	, &ObjectTracker_t561_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t82, ___mObjectTracker_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType FrameQuality_t600_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t82____mLastFrameQuality_3_FieldInfo = 
{
	"mLastFrameQuality"/* name */
	, &FrameQuality_t600_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t82, ___mLastFrameQuality_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t82____mCurrentlyScanning_4_FieldInfo = 
{
	"mCurrentlyScanning"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t82, ___mCurrentlyScanning_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t82____mWasScanningBeforeDisable_5_FieldInfo = 
{
	"mWasScanningBeforeDisable"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t82, ___mWasScanningBeforeDisable_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t82____mCurrentlyBuilding_6_FieldInfo = 
{
	"mCurrentlyBuilding"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t82, ___mCurrentlyBuilding_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t82____mWasBuildingBeforeDisable_7_FieldInfo = 
{
	"mWasBuildingBeforeDisable"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t82, ___mWasBuildingBeforeDisable_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t82____mOnInitializedCalled_8_FieldInfo = 
{
	"mOnInitializedCalled"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t82, ___mOnInitializedCalled_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType List_1_t767_0_0_33;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t82____mHandlers_9_FieldInfo = 
{
	"mHandlers"/* name */
	, &List_1_t767_0_0_33/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t82, ___mHandlers_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_6;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t82____StopTrackerWhileScanning_10_FieldInfo = 
{
	"StopTrackerWhileScanning"/* name */
	, &Boolean_t106_0_0_6/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t82, ___StopTrackerWhileScanning_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_6;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t82____StartScanningAutomatically_11_FieldInfo = 
{
	"StartScanningAutomatically"/* name */
	, &Boolean_t106_0_0_6/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t82, ___StartScanningAutomatically_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_6;
FieldInfo UserDefinedTargetBuildingAbstractBehaviour_t82____StopScanningWhenFinshedBuilding_12_FieldInfo = 
{
	"StopScanningWhenFinshedBuilding"/* name */
	, &Boolean_t106_0_0_6/* type */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* parent */
	, offsetof(UserDefinedTargetBuildingAbstractBehaviour_t82, ___StopScanningWhenFinshedBuilding_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UserDefinedTargetBuildingAbstractBehaviour_t82_FieldInfos[] =
{
	&UserDefinedTargetBuildingAbstractBehaviour_t82____mObjectTracker_2_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t82____mLastFrameQuality_3_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t82____mCurrentlyScanning_4_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t82____mWasScanningBeforeDisable_5_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t82____mCurrentlyBuilding_6_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t82____mWasBuildingBeforeDisable_7_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t82____mOnInitializedCalled_8_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t82____mHandlers_9_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t82____StopTrackerWhileScanning_10_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t82____StartScanningAutomatically_11_FieldInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_t82____StopScanningWhenFinshedBuilding_12_FieldInfo,
	NULL
};
extern Il2CppType IUserDefinedTargetEventHandler_t768_0_0_0;
extern Il2CppType IUserDefinedTargetEventHandler_t768_0_0_0;
static ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t82_UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4188_ParameterInfos[] = 
{
	{"eventHandler", 0, 134220025, &EmptyCustomAttributesCache, &IUserDefinedTargetEventHandler_t768_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::RegisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4188_MethodInfo = 
{
	"RegisterEventHandler"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4188/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t82_UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4188_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2399/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IUserDefinedTargetEventHandler_t768_0_0_0;
static ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t82_UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4189_ParameterInfos[] = 
{
	{"eventHandler", 0, 134220026, &EmptyCustomAttributesCache, &IUserDefinedTargetEventHandler_t768_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::UnregisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4189_MethodInfo = 
{
	"UnregisterEventHandler"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4189/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t82_UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4189_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2400/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StartScanning()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4190_MethodInfo = 
{
	"StartScanning"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4190/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2401/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Single_t105_0_0_0;
extern Il2CppType Single_t105_0_0_0;
static ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t82_UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4191_ParameterInfos[] = 
{
	{"targetName", 0, 134220027, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"sceenSizeWidth", 1, 134220028, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Single_t105 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::BuildNewTarget(System.String,System.Single)
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4191_MethodInfo = 
{
	"BuildNewTarget"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4191/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Single_t105/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t82_UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4191_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2402/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopScanning()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4192_MethodInfo = 
{
	"StopScanning"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4192/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2403/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType FrameQuality_t600_0_0_0;
extern Il2CppType FrameQuality_t600_0_0_0;
static ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t82_UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193_ParameterInfos[] = 
{
	{"frameQuality", 0, 134220029, &EmptyCustomAttributesCache, &FrameQuality_t600_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::SetFrameQuality(Vuforia.ImageTargetBuilder/FrameQuality)
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193_MethodInfo = 
{
	"SetFrameQuality"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t82_UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2404/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Start()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_Start_m4194_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_Start_m4194/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2405/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Update()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_Update_m4195_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_Update_m4195/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2406/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnEnable()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4196_MethodInfo = 
{
	"OnEnable"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4196/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2407/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDisable()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4197_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4197/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2408/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDestroy()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4198_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4198/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2409/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnQCARStarted()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4199_MethodInfo = 
{
	"OnQCARStarted"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4199/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2410/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo UserDefinedTargetBuildingAbstractBehaviour_t82_UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4200_ParameterInfos[] = 
{
	{"pause", 0, 134220030, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_SByte_t129 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnPause(System.Boolean)
MethodInfo UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4200_MethodInfo = 
{
	"OnPause"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4200/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_SByte_t129/* invoker_method */
	, UserDefinedTargetBuildingAbstractBehaviour_t82_UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4200_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2411/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::.ctor()
MethodInfo UserDefinedTargetBuildingAbstractBehaviour__ctor_m611_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UserDefinedTargetBuildingAbstractBehaviour__ctor_m611/* method */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2412/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* UserDefinedTargetBuildingAbstractBehaviour_t82_MethodInfos[] =
{
	&UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4188_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4189_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4190_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4191_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4192_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_Start_m4194_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_Update_m4195_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4196_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4197_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4198_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4199_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4200_MethodInfo,
	&UserDefinedTargetBuildingAbstractBehaviour__ctor_m611_MethodInfo,
	NULL
};
static MethodInfo* UserDefinedTargetBuildingAbstractBehaviour_t82_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0;
extern Il2CppType UserDefinedTargetBuildingAbstractBehaviour_t82_1_0_0;
struct UserDefinedTargetBuildingAbstractBehaviour_t82;
TypeInfo UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "UserDefinedTargetBuildingAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, UserDefinedTargetBuildingAbstractBehaviour_t82_MethodInfos/* methods */
	, NULL/* properties */
	, UserDefinedTargetBuildingAbstractBehaviour_t82_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UserDefinedTargetBuildingAbstractBehaviour_t82_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_il2cpp_TypeInfo/* cast_class */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_0_0_0/* byval_arg */
	, &UserDefinedTargetBuildingAbstractBehaviour_t82_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UserDefinedTargetBuildingAbstractBehaviour_t82)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 14/* method_count */
	, 0/* property_count */
	, 11/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.VideoBackgroundAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo;
// Vuforia.VideoBackgroundAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbstMethodDeclarations.h"

// UnityEngine.MeshRenderer
#include "UnityEngine_UnityEngine_MeshRenderer.h"
// System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator_.h"
// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbst.h"
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
#include "System_Core_System_Collections_Generic_HashSet_1_gen.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_Transform.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManager.h"
// Vuforia.QCARManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
extern TypeInfo HashSet_1_t769_il2cpp_TypeInfo;
extern TypeInfo MeshRenderer_t168_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t902_il2cpp_TypeInfo;
extern TypeInfo Vector3_t13_il2cpp_TypeInfo;
extern TypeInfo QCARManager_t171_il2cpp_TypeInfo;
extern TypeInfo QCARManagerImpl_t657_il2cpp_TypeInfo;
extern TypeInfo Color_t19_il2cpp_TypeInfo;
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
#include "System_Core_System_Collections_Generic_HashSet_1_genMethodDeclarations.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
// System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator_MethodDeclarations.h"
// Vuforia.UnityCameraExtensions
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_UnityCameraExtensioMethodDeclarations.h"
// Vuforia.BackgroundPlaneAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_BackgroundPlaneAbstMethodDeclarations.h"
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
// UnityEngine.Transform
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4MethodDeclarations.h"
// Vuforia.QCARManager
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerMethodDeclarations.h"
// Vuforia.QCARManagerImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImplMethodDeclarations.h"
// UnityEngine.GL
#include "UnityEngine_UnityEngine_GLMethodDeclarations.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
extern MethodInfo Component_GetComponent_TisMeshRenderer_t168_m607_MethodInfo;
extern MethodInfo HashSet_1_Contains_m5392_MethodInfo;
extern MethodInfo Renderer_set_enabled_m416_MethodInfo;
extern MethodInfo HashSet_1_Add_m5393_MethodInfo;
extern MethodInfo HashSet_1_GetEnumerator_m5394_MethodInfo;
extern MethodInfo Enumerator_get_Current_m5395_MethodInfo;
extern MethodInfo Enumerator_MoveNext_m5396_MethodInfo;
extern MethodInfo HashSet_1_Clear_m5397_MethodInfo;
extern MethodInfo UnityCameraExtensions_GetMaxDepthForVideoBackground_m2630_MethodInfo;
extern MethodInfo Mathf_Min_m2482_MethodInfo;
extern MethodInfo UnityCameraExtensions_GetMinDepthForVideoBackground_m2631_MethodInfo;
extern MethodInfo Mathf_Max_m2422_MethodInfo;
extern MethodInfo VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4205_MethodInfo;
extern MethodInfo BackgroundPlaneAbstractBehaviour_SetStereoDepth_m2610_MethodInfo;
extern MethodInfo Camera_get_projectionMatrix_m4298_MethodInfo;
extern MethodInfo Component_get_transform_m219_MethodInfo;
extern MethodInfo Transform_get_localPosition_m2325_MethodInfo;
extern MethodInfo Matrix4x4_get_Item_m5398_MethodInfo;
extern MethodInfo Matrix4x4_set_Item_m5399_MethodInfo;
extern MethodInfo Camera_set_projectionMatrix_m5365_MethodInfo;
extern MethodInfo QCARManager_get_Instance_m640_MethodInfo;
extern MethodInfo QCARManagerImpl_StartRendering_m2994_MethodInfo;
extern MethodInfo Transform_get_parent_m220_MethodInfo;
extern MethodInfo Component_GetComponent_TisQCARAbstractBehaviour_t71_m5400_MethodInfo;
extern MethodInfo VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4206_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_RegisterRenderOnUpdateCallback_m4132_MethodInfo;
extern MethodInfo Component_GetComponent_TisCamera_t3_m315_MethodInfo;
extern MethodInfo Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t30_m4456_MethodInfo;
extern MethodInfo VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201_MethodInfo;
extern MethodInfo GL_SetRevertBackfacing_m5401_MethodInfo;
extern MethodInfo QCARManagerImpl_FinishRendering_m2995_MethodInfo;
extern MethodInfo Color__ctor_m217_MethodInfo;
extern MethodInfo GL_Clear_m4587_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_UnregisterRenderOnUpdateCallback_m4133_MethodInfo;
extern MethodInfo HashSet_1__ctor_m5402_MethodInfo;
struct Component_t100;
// UnityEngine.Component
#include "UnityEngine_UnityEngine_Component.h"
// UnityEngine.CastHelper`1<UnityEngine.MeshRenderer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_6.h"
struct Component_t100;
// UnityEngine.CastHelper`1<System.Object>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_0.h"
// Declaration !!0 UnityEngine.Component::GetComponent<System.Object>()
// !!0 UnityEngine.Component::GetComponent<System.Object>()
 Object_t * Component_GetComponent_TisObject_t_m241_gshared (Component_t100 * __this, MethodInfo* method);
#define Component_GetComponent_TisObject_t_m241(__this, method) (Object_t *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t168_m607(__this, method) (MeshRenderer_t168 *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)
struct Component_t100;
// UnityEngine.CastHelper`1<Vuforia.QCARAbstractBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_24.h"
// Declaration !!0 UnityEngine.Component::GetComponent<Vuforia.QCARAbstractBehaviour>()
// !!0 UnityEngine.Component::GetComponent<Vuforia.QCARAbstractBehaviour>()
#define Component_GetComponent_TisQCARAbstractBehaviour_t71_m5400(__this, method) (QCARAbstractBehaviour_t71 *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)
struct Component_t100;
// UnityEngine.CastHelper`1<UnityEngine.Camera>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_3.h"
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t3_m315(__this, method) (Camera_t3 *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)
struct Component_t100;
struct Component_t100;
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
 Object_t * Component_GetComponentInChildren_TisObject_t_m4459_gshared (Component_t100 * __this, MethodInfo* method);
#define Component_GetComponentInChildren_TisObject_t_m4459(__this, method) (Object_t *)Component_GetComponentInChildren_TisObject_t_m4459_gshared((Component_t100 *)__this, method)
// Declaration !!0 UnityEngine.Component::GetComponentInChildren<Vuforia.BackgroundPlaneAbstractBehaviour>()
// !!0 UnityEngine.Component::GetComponentInChildren<Vuforia.BackgroundPlaneAbstractBehaviour>()
#define Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t30_m4456(__this, method) (BackgroundPlaneAbstractBehaviour_t30 *)Component_GetComponentInChildren_TisObject_t_m4459_gshared((Component_t100 *)__this, method)


// System.Boolean Vuforia.VideoBackgroundAbstractBehaviour::get_VideoBackGroundMirrored()
 bool VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___U3CVideoBackGroundMirroredU3Ek__BackingField_9);
		return L_0;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::set_VideoBackGroundMirrored(System.Boolean)
extern MethodInfo VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202_MethodInfo;
 void VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202 (VideoBackgroundAbstractBehaviour_t84 * __this, bool ___value, MethodInfo* method){
	{
		__this->___U3CVideoBackGroundMirroredU3Ek__BackingField_9 = ___value;
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
extern MethodInfo VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4203_MethodInfo;
 void VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4203 (VideoBackgroundAbstractBehaviour_t84 * __this, bool ___disable, MethodInfo* method){
	MeshRenderer_t168 * V_0 = {0};
	MeshRenderer_t168 * V_1 = {0};
	Enumerator_t902  V_2 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t152 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t152 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		if (!___disable)
		{
			goto IL_0058;
		}
	}
	{
		BackgroundPlaneAbstractBehaviour_t30 * L_0 = (__this->___mBackgroundBehaviour_6);
		bool L_1 = Object_op_Inequality_m335(NULL /*static, unused*/, L_0, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_1)
		{
			goto IL_0048;
		}
	}
	{
		BackgroundPlaneAbstractBehaviour_t30 * L_2 = (__this->___mBackgroundBehaviour_6);
		NullCheck(L_2);
		MeshRenderer_t168 * L_3 = Component_GetComponent_TisMeshRenderer_t168_m607(L_2, /*hidden argument*/&Component_GetComponent_TisMeshRenderer_t168_m607_MethodInfo);
		V_0 = L_3;
		bool L_4 = Object_op_Inequality_m335(NULL /*static, unused*/, V_0, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_4)
		{
			goto IL_0048;
		}
	}
	{
		HashSet_1_t769 * L_5 = (__this->___mDisabledMeshRenderers_8);
		NullCheck(L_5);
		bool L_6 = (bool)VirtFuncInvoker1< bool, MeshRenderer_t168 * >::Invoke(&HashSet_1_Contains_m5392_MethodInfo, L_5, V_0);
		if (L_6)
		{
			goto IL_0048;
		}
	}
	{
		NullCheck(V_0);
		Renderer_set_enabled_m416(V_0, 0, /*hidden argument*/&Renderer_set_enabled_m416_MethodInfo);
		HashSet_1_t769 * L_7 = (__this->___mDisabledMeshRenderers_8);
		NullCheck(L_7);
		HashSet_1_Add_m5393(L_7, V_0, /*hidden argument*/&HashSet_1_Add_m5393_MethodInfo);
	}

IL_0048:
	{
		__this->___mClearBuffers_2 = ((int32_t)16);
		__this->___mSkipStateUpdates_3 = 5;
		return;
	}

IL_0058:
	{
		int32_t L_8 = (__this->___mSkipStateUpdates_3);
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_9 = (__this->___mSkipStateUpdates_3);
		__this->___mSkipStateUpdates_3 = ((int32_t)(L_9-1));
	}

IL_006f:
	{
		int32_t L_10 = (__this->___mSkipStateUpdates_3);
		if (L_10)
		{
			goto IL_00b8;
		}
	}
	{
		HashSet_1_t769 * L_11 = (__this->___mDisabledMeshRenderers_8);
		NullCheck(L_11);
		Enumerator_t902  L_12 = HashSet_1_GetEnumerator_m5394(L_11, /*hidden argument*/&HashSet_1_GetEnumerator_m5394_MethodInfo);
		V_2 = L_12;
	}

IL_0083:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0094;
		}

IL_0085:
		{
			MeshRenderer_t168 * L_13 = Enumerator_get_Current_m5395((&V_2), /*hidden argument*/&Enumerator_get_Current_m5395_MethodInfo);
			V_1 = L_13;
			NullCheck(V_1);
			Renderer_set_enabled_m416(V_1, 1, /*hidden argument*/&Renderer_set_enabled_m416_MethodInfo);
		}

IL_0094:
		{
			bool L_14 = Enumerator_MoveNext_m5396((&V_2), /*hidden argument*/&Enumerator_MoveNext_m5396_MethodInfo);
			if (L_14)
			{
				goto IL_0085;
			}
		}

IL_009d:
		{
			// IL_009d: leave.s IL_00ad
			leaveInstructions[0] = 0xAD; // 1
			THROW_SENTINEL(IL_00ad);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_009f;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t152 *)e.ex;
		goto IL_009f;
	}

IL_009f:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t902_il2cpp_TypeInfo), &(*(&V_2))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t902_il2cpp_TypeInfo), &(*(&V_2))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0xAD:
				goto IL_00ad;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_00ad:
	{
		HashSet_1_t769 * L_15 = (__this->___mDisabledMeshRenderers_8);
		NullCheck(L_15);
		VirtActionInvoker0::Invoke(&HashSet_1_Clear_m5397_MethodInfo, L_15);
	}

IL_00b8:
	{
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::SetStereoDepth(System.Single)
extern MethodInfo VideoBackgroundAbstractBehaviour_SetStereoDepth_m4204_MethodInfo;
 void VideoBackgroundAbstractBehaviour_SetStereoDepth_m4204 (VideoBackgroundAbstractBehaviour_t84 * __this, float ___depth, MethodInfo* method){
	{
		Camera_t3 * L_0 = (__this->___mCamera_5);
		float L_1 = UnityCameraExtensions_GetMaxDepthForVideoBackground_m2630(NULL /*static, unused*/, L_0, /*hidden argument*/&UnityCameraExtensions_GetMaxDepthForVideoBackground_m2630_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Mathf_t101_il2cpp_TypeInfo));
		float L_2 = Mathf_Min_m2482(NULL /*static, unused*/, ___depth, L_1, /*hidden argument*/&Mathf_Min_m2482_MethodInfo);
		Camera_t3 * L_3 = (__this->___mCamera_5);
		float L_4 = UnityCameraExtensions_GetMinDepthForVideoBackground_m2631(NULL /*static, unused*/, L_3, /*hidden argument*/&UnityCameraExtensions_GetMinDepthForVideoBackground_m2631_MethodInfo);
		float L_5 = Mathf_Max_m2422(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/&Mathf_Max_m2422_MethodInfo);
		___depth = L_5;
		float L_6 = (__this->___mStereoDepth_7);
		if ((((float)___depth) == ((float)L_6)))
		{
			goto IL_0039;
		}
	}
	{
		__this->___mStereoDepth_7 = ___depth;
		VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4205(__this, /*hidden argument*/&VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4205_MethodInfo);
	}

IL_0039:
	{
		BackgroundPlaneAbstractBehaviour_t30 * L_7 = (__this->___mBackgroundBehaviour_6);
		bool L_8 = Object_op_Inequality_m335(NULL /*static, unused*/, L_7, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_8)
		{
			goto IL_0053;
		}
	}
	{
		BackgroundPlaneAbstractBehaviour_t30 * L_9 = (__this->___mBackgroundBehaviour_6);
		NullCheck(L_9);
		BackgroundPlaneAbstractBehaviour_SetStereoDepth_m2610(L_9, ___depth, /*hidden argument*/&BackgroundPlaneAbstractBehaviour_SetStereoDepth_m2610_MethodInfo);
	}

IL_0053:
	{
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ApplyStereoDepthToMatrices()
 void VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4205 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method){
	Matrix4x4_t176  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Camera_t3 * L_0 = (__this->___mCamera_5);
		NullCheck(L_0);
		Matrix4x4_t176  L_1 = Camera_get_projectionMatrix_m4298(L_0, /*hidden argument*/&Camera_get_projectionMatrix_m4298_MethodInfo);
		V_0 = L_1;
		Transform_t10 * L_2 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_2);
		Vector3_t13  L_3 = Transform_get_localPosition_m2325(L_2, /*hidden argument*/&Transform_get_localPosition_m2325_MethodInfo);
		;
		float L_4 = (L_3.___x_1);
		V_1 = ((-L_4));
		float L_5 = Matrix4x4_get_Item_m5398((&V_0), 0, 0, /*hidden argument*/&Matrix4x4_get_Item_m5398_MethodInfo);
		V_2 = ((float)((float)(1.0f)/(float)L_5));
		float L_6 = (__this->___mStereoDepth_7);
		V_3 = ((float)((float)L_6*(float)V_2));
		V_4 = ((float)((float)V_1/(float)V_3));
		Matrix4x4_set_Item_m5399((&V_0), 0, 2, V_4, /*hidden argument*/&Matrix4x4_set_Item_m5399_MethodInfo);
		Camera_t3 * L_7 = (__this->___mCamera_5);
		NullCheck(L_7);
		Camera_set_projectionMatrix_m5365(L_7, V_0, /*hidden argument*/&Camera_set_projectionMatrix_m5365_MethodInfo);
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::RenderOnUpdate()
 void VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4206 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method){
	{
		QCARAbstractBehaviour_t71 * L_0 = (__this->___mQCARAbstractBehaviour_4);
		NullCheck(L_0);
		bool L_1 = QCARAbstractBehaviour_get_HasStarted_m4103(L_0, /*hidden argument*/&QCARAbstractBehaviour_get_HasStarted_m4103_MethodInfo);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARManager_t171_il2cpp_TypeInfo));
		QCARManager_t171 * L_2 = QCARManager_get_Instance_m640(NULL /*static, unused*/, /*hidden argument*/&QCARManager_get_Instance_m640_MethodInfo);
		NullCheck(((QCARManagerImpl_t657 *)Castclass(L_2, InitializedTypeInfo(&QCARManagerImpl_t657_il2cpp_TypeInfo))));
		QCARManagerImpl_StartRendering_m2994(((QCARManagerImpl_t657 *)Castclass(L_2, InitializedTypeInfo(&QCARManagerImpl_t657_il2cpp_TypeInfo))), /*hidden argument*/&QCARManagerImpl_StartRendering_m2994_MethodInfo);
	}

IL_001c:
	{
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Awake()
extern MethodInfo VideoBackgroundAbstractBehaviour_Awake_m4207_MethodInfo;
 void VideoBackgroundAbstractBehaviour_Awake_m4207 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method){
	{
		Transform_t10 * L_0 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_0);
		Transform_t10 * L_1 = Transform_get_parent_m220(L_0, /*hidden argument*/&Transform_get_parent_m220_MethodInfo);
		NullCheck(L_1);
		QCARAbstractBehaviour_t71 * L_2 = Component_GetComponent_TisQCARAbstractBehaviour_t71_m5400(L_1, /*hidden argument*/&Component_GetComponent_TisQCARAbstractBehaviour_t71_m5400_MethodInfo);
		__this->___mQCARAbstractBehaviour_4 = L_2;
		QCARAbstractBehaviour_t71 * L_3 = (__this->___mQCARAbstractBehaviour_4);
		bool L_4 = Object_op_Inequality_m335(NULL /*static, unused*/, L_3, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_4)
		{
			goto IL_003b;
		}
	}
	{
		QCARAbstractBehaviour_t71 * L_5 = (__this->___mQCARAbstractBehaviour_4);
		IntPtr_t121 L_6 = { &VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4206_MethodInfo };
		Action_t148 * L_7 = (Action_t148 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t148_il2cpp_TypeInfo));
		Action__ctor_m4427(L_7, __this, L_6, /*hidden argument*/&Action__ctor_m4427_MethodInfo);
		NullCheck(L_5);
		QCARAbstractBehaviour_RegisterRenderOnUpdateCallback_m4132(L_5, L_7, /*hidden argument*/&QCARAbstractBehaviour_RegisterRenderOnUpdateCallback_m4132_MethodInfo);
	}

IL_003b:
	{
		Camera_t3 * L_8 = Component_GetComponent_TisCamera_t3_m315(__this, /*hidden argument*/&Component_GetComponent_TisCamera_t3_m315_MethodInfo);
		__this->___mCamera_5 = L_8;
		Camera_t3 * L_9 = (__this->___mCamera_5);
		float L_10 = UnityCameraExtensions_GetMaxDepthForVideoBackground_m2630(NULL /*static, unused*/, L_9, /*hidden argument*/&UnityCameraExtensions_GetMaxDepthForVideoBackground_m2630_MethodInfo);
		__this->___mStereoDepth_7 = L_10;
		BackgroundPlaneAbstractBehaviour_t30 * L_11 = Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t30_m4456(__this, /*hidden argument*/&Component_GetComponentInChildren_TisBackgroundPlaneAbstractBehaviour_t30_m4456_MethodInfo);
		__this->___mBackgroundBehaviour_6 = L_11;
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPreRender()
extern MethodInfo VideoBackgroundAbstractBehaviour_OnPreRender_m4208_MethodInfo;
 void VideoBackgroundAbstractBehaviour_OnPreRender_m4208 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method){
	{
		bool L_0 = VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201(__this, /*hidden argument*/&VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201_MethodInfo);
		GL_SetRevertBackfacing_m5401(NULL /*static, unused*/, L_0, /*hidden argument*/&GL_SetRevertBackfacing_m5401_MethodInfo);
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPostRender()
extern MethodInfo VideoBackgroundAbstractBehaviour_OnPostRender_m4209_MethodInfo;
 void VideoBackgroundAbstractBehaviour_OnPostRender_m4209 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method){
	{
		QCARAbstractBehaviour_t71 * L_0 = (__this->___mQCARAbstractBehaviour_4);
		NullCheck(L_0);
		bool L_1 = QCARAbstractBehaviour_get_HasStarted_m4103(L_0, /*hidden argument*/&QCARAbstractBehaviour_get_HasStarted_m4103_MethodInfo);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARManager_t171_il2cpp_TypeInfo));
		QCARManager_t171 * L_2 = QCARManager_get_Instance_m640(NULL /*static, unused*/, /*hidden argument*/&QCARManager_get_Instance_m640_MethodInfo);
		NullCheck(((QCARManagerImpl_t657 *)Castclass(L_2, InitializedTypeInfo(&QCARManagerImpl_t657_il2cpp_TypeInfo))));
		QCARManagerImpl_FinishRendering_m2995(((QCARManagerImpl_t657 *)Castclass(L_2, InitializedTypeInfo(&QCARManagerImpl_t657_il2cpp_TypeInfo))), /*hidden argument*/&QCARManagerImpl_FinishRendering_m2995_MethodInfo);
	}

IL_001c:
	{
		int32_t L_3 = (__this->___mClearBuffers_2);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0053;
		}
	}
	{
		Color_t19  L_4 = {0};
		Color__ctor_m217(&L_4, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/&Color__ctor_m217_MethodInfo);
		GL_Clear_m4587(NULL /*static, unused*/, 0, 1, L_4, /*hidden argument*/&GL_Clear_m4587_MethodInfo);
		int32_t L_5 = (__this->___mClearBuffers_2);
		__this->___mClearBuffers_2 = ((int32_t)(L_5-1));
	}

IL_0053:
	{
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnDestroy()
extern MethodInfo VideoBackgroundAbstractBehaviour_OnDestroy_m4210_MethodInfo;
 void VideoBackgroundAbstractBehaviour_OnDestroy_m4210 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method){
	{
		QCARAbstractBehaviour_t71 * L_0 = (__this->___mQCARAbstractBehaviour_4);
		bool L_1 = Object_op_Inequality_m335(NULL /*static, unused*/, L_0, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		QCARAbstractBehaviour_t71 * L_2 = (__this->___mQCARAbstractBehaviour_4);
		IntPtr_t121 L_3 = { &VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4206_MethodInfo };
		Action_t148 * L_4 = (Action_t148 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Action_t148_il2cpp_TypeInfo));
		Action__ctor_m4427(L_4, __this, L_3, /*hidden argument*/&Action__ctor_m4427_MethodInfo);
		NullCheck(L_2);
		QCARAbstractBehaviour_UnregisterRenderOnUpdateCallback_m4133(L_2, L_4, /*hidden argument*/&QCARAbstractBehaviour_UnregisterRenderOnUpdateCallback_m4133_MethodInfo);
	}

IL_0025:
	{
		return;
	}
}
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::.ctor()
extern MethodInfo VideoBackgroundAbstractBehaviour__ctor_m612_MethodInfo;
 void VideoBackgroundAbstractBehaviour__ctor_m612 (VideoBackgroundAbstractBehaviour_t84 * __this, MethodInfo* method){
	{
		HashSet_1_t769 * L_0 = (HashSet_1_t769 *)il2cpp_codegen_object_new (InitializedTypeInfo(&HashSet_1_t769_il2cpp_TypeInfo));
		HashSet_1__ctor_m5402(L_0, /*hidden argument*/&HashSet_1__ctor_m5402_MethodInfo);
		__this->___mDisabledMeshRenderers_8 = L_0;
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.VideoBackgroundAbstractBehaviour
extern Il2CppType Int32_t93_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t84____mClearBuffers_2_FieldInfo = 
{
	"mClearBuffers"/* name */
	, &Int32_t93_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t84, ___mClearBuffers_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t84____mSkipStateUpdates_3_FieldInfo = 
{
	"mSkipStateUpdates"/* name */
	, &Int32_t93_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t84, ___mSkipStateUpdates_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType QCARAbstractBehaviour_t71_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t84____mQCARAbstractBehaviour_4_FieldInfo = 
{
	"mQCARAbstractBehaviour"/* name */
	, &QCARAbstractBehaviour_t71_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t84, ___mQCARAbstractBehaviour_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Camera_t3_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t84____mCamera_5_FieldInfo = 
{
	"mCamera"/* name */
	, &Camera_t3_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t84, ___mCamera_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType BackgroundPlaneAbstractBehaviour_t30_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t84____mBackgroundBehaviour_6_FieldInfo = 
{
	"mBackgroundBehaviour"/* name */
	, &BackgroundPlaneAbstractBehaviour_t30_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t84, ___mBackgroundBehaviour_6)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Single_t105_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t84____mStereoDepth_7_FieldInfo = 
{
	"mStereoDepth"/* name */
	, &Single_t105_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t84, ___mStereoDepth_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType HashSet_1_t769_0_0_1;
FieldInfo VideoBackgroundAbstractBehaviour_t84____mDisabledMeshRenderers_8_FieldInfo = 
{
	"mDisabledMeshRenderers"/* name */
	, &HashSet_1_t769_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t84, ___mDisabledMeshRenderers_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache_U3CVideoBackGroundMirroredU3Ek__BackingField;
FieldInfo VideoBackgroundAbstractBehaviour_t84____U3CVideoBackGroundMirroredU3Ek__BackingField_9_FieldInfo = 
{
	"<VideoBackGroundMirrored>k__BackingField"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* parent */
	, offsetof(VideoBackgroundAbstractBehaviour_t84, ___U3CVideoBackGroundMirroredU3Ek__BackingField_9)/* data */
	, &VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache_U3CVideoBackGroundMirroredU3Ek__BackingField/* custom_attributes_cache */

};
static FieldInfo* VideoBackgroundAbstractBehaviour_t84_FieldInfos[] =
{
	&VideoBackgroundAbstractBehaviour_t84____mClearBuffers_2_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t84____mSkipStateUpdates_3_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t84____mQCARAbstractBehaviour_4_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t84____mCamera_5_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t84____mBackgroundBehaviour_6_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t84____mStereoDepth_7_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t84____mDisabledMeshRenderers_8_FieldInfo,
	&VideoBackgroundAbstractBehaviour_t84____U3CVideoBackGroundMirroredU3Ek__BackingField_9_FieldInfo,
	NULL
};
static PropertyInfo VideoBackgroundAbstractBehaviour_t84____VideoBackGroundMirrored_PropertyInfo = 
{
	&VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* parent */
	, "VideoBackGroundMirrored"/* name */
	, &VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201_MethodInfo/* get */
	, &VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* VideoBackgroundAbstractBehaviour_t84_PropertyInfos[] =
{
	&VideoBackgroundAbstractBehaviour_t84____VideoBackGroundMirrored_PropertyInfo,
	NULL
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201;
// System.Boolean Vuforia.VideoBackgroundAbstractBehaviour::get_VideoBackGroundMirrored()
MethodInfo VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201_MethodInfo = 
{
	"get_VideoBackGroundMirrored"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201/* method */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2413/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo VideoBackgroundAbstractBehaviour_t84_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202_ParameterInfos[] = 
{
	{"value", 0, 134220031, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_SByte_t129 (MethodInfo* method, void* obj, void** args);
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202;
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::set_VideoBackGroundMirrored(System.Boolean)
MethodInfo VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202_MethodInfo = 
{
	"set_VideoBackGroundMirrored"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202/* method */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_SByte_t129/* invoker_method */
	, VideoBackgroundAbstractBehaviour_t84_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202_ParameterInfos/* parameters */
	, &VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2414/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo VideoBackgroundAbstractBehaviour_t84_VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4203_ParameterInfos[] = 
{
	{"disable", 0, 134220032, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_SByte_t129 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ResetBackgroundPlane(System.Boolean)
MethodInfo VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4203_MethodInfo = 
{
	"ResetBackgroundPlane"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4203/* method */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_SByte_t129/* invoker_method */
	, VideoBackgroundAbstractBehaviour_t84_VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4203_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2415/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Single_t105_0_0_0;
static ParameterInfo VideoBackgroundAbstractBehaviour_t84_VideoBackgroundAbstractBehaviour_SetStereoDepth_m4204_ParameterInfos[] = 
{
	{"depth", 0, 134220033, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Single_t105 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::SetStereoDepth(System.Single)
MethodInfo VideoBackgroundAbstractBehaviour_SetStereoDepth_m4204_MethodInfo = 
{
	"SetStereoDepth"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_SetStereoDepth_m4204/* method */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Single_t105/* invoker_method */
	, VideoBackgroundAbstractBehaviour_t84_VideoBackgroundAbstractBehaviour_SetStereoDepth_m4204_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2416/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::ApplyStereoDepthToMatrices()
MethodInfo VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4205_MethodInfo = 
{
	"ApplyStereoDepthToMatrices"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4205/* method */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2417/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::RenderOnUpdate()
MethodInfo VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4206_MethodInfo = 
{
	"RenderOnUpdate"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4206/* method */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2418/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::Awake()
MethodInfo VideoBackgroundAbstractBehaviour_Awake_m4207_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_Awake_m4207/* method */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2419/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPreRender()
MethodInfo VideoBackgroundAbstractBehaviour_OnPreRender_m4208_MethodInfo = 
{
	"OnPreRender"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_OnPreRender_m4208/* method */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2420/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnPostRender()
MethodInfo VideoBackgroundAbstractBehaviour_OnPostRender_m4209_MethodInfo = 
{
	"OnPostRender"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_OnPostRender_m4209/* method */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2421/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::OnDestroy()
MethodInfo VideoBackgroundAbstractBehaviour_OnDestroy_m4210_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour_OnDestroy_m4210/* method */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2422/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoBackgroundAbstractBehaviour::.ctor()
MethodInfo VideoBackgroundAbstractBehaviour__ctor_m612_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VideoBackgroundAbstractBehaviour__ctor_m612/* method */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2423/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* VideoBackgroundAbstractBehaviour_t84_MethodInfos[] =
{
	&VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201_MethodInfo,
	&VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202_MethodInfo,
	&VideoBackgroundAbstractBehaviour_ResetBackgroundPlane_m4203_MethodInfo,
	&VideoBackgroundAbstractBehaviour_SetStereoDepth_m4204_MethodInfo,
	&VideoBackgroundAbstractBehaviour_ApplyStereoDepthToMatrices_m4205_MethodInfo,
	&VideoBackgroundAbstractBehaviour_RenderOnUpdate_m4206_MethodInfo,
	&VideoBackgroundAbstractBehaviour_Awake_m4207_MethodInfo,
	&VideoBackgroundAbstractBehaviour_OnPreRender_m4208_MethodInfo,
	&VideoBackgroundAbstractBehaviour_OnPostRender_m4209_MethodInfo,
	&VideoBackgroundAbstractBehaviour_OnDestroy_m4210_MethodInfo,
	&VideoBackgroundAbstractBehaviour__ctor_m612_MethodInfo,
	NULL
};
static MethodInfo* VideoBackgroundAbstractBehaviour_t84_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
extern TypeInfo RequireComponent_t155_il2cpp_TypeInfo;
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
extern MethodInfo RequireComponent__ctor_m474_MethodInfo;
extern TypeInfo Camera_t3_il2cpp_TypeInfo;
void VideoBackgroundAbstractBehaviour_t84_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t155 * tmp;
		tmp = (RequireComponent_t155 *)il2cpp_codegen_object_new (&RequireComponent_t155_il2cpp_TypeInfo);
		RequireComponent__ctor_m474(tmp, il2cpp_codegen_type_get_object(InitializedTypeInfo(&Camera_t3_il2cpp_TypeInfo)), &RequireComponent__ctor_m474_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo CompilerGeneratedAttribute_t421_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern MethodInfo CompilerGeneratedAttribute__ctor_m1901_MethodInfo;
void VideoBackgroundAbstractBehaviour_t84_CustomAttributesCacheGenerator_U3CVideoBackGroundMirroredU3Ek__BackingField(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t421 * tmp;
		tmp = (CompilerGeneratedAttribute_t421 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t421_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m1901(tmp, &CompilerGeneratedAttribute__ctor_m1901_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void VideoBackgroundAbstractBehaviour_t84_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t421 * tmp;
		tmp = (CompilerGeneratedAttribute_t421 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t421_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m1901(tmp, &CompilerGeneratedAttribute__ctor_m1901_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
void VideoBackgroundAbstractBehaviour_t84_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t421 * tmp;
		tmp = (CompilerGeneratedAttribute_t421 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t421_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m1901(tmp, &CompilerGeneratedAttribute__ctor_m1901_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache = {
1,
NULL,
&VideoBackgroundAbstractBehaviour_t84_CustomAttributesCacheGenerator
};
CustomAttributesCache VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache_U3CVideoBackGroundMirroredU3Ek__BackingField = {
1,
NULL,
&VideoBackgroundAbstractBehaviour_t84_CustomAttributesCacheGenerator_U3CVideoBackGroundMirroredU3Ek__BackingField
};
CustomAttributesCache VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201 = {
1,
NULL,
&VideoBackgroundAbstractBehaviour_t84_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201
};
CustomAttributesCache VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202 = {
1,
NULL,
&VideoBackgroundAbstractBehaviour_t84_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType VideoBackgroundAbstractBehaviour_t84_0_0_0;
extern Il2CppType VideoBackgroundAbstractBehaviour_t84_1_0_0;
struct VideoBackgroundAbstractBehaviour_t84;
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache;
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache_U3CVideoBackGroundMirroredU3Ek__BackingField;
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4201;
extern CustomAttributesCache VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4202;
TypeInfo VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "VideoBackgroundAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VideoBackgroundAbstractBehaviour_t84_MethodInfos/* methods */
	, VideoBackgroundAbstractBehaviour_t84_PropertyInfos/* properties */
	, VideoBackgroundAbstractBehaviour_t84_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, VideoBackgroundAbstractBehaviour_t84_VTable/* vtable */
	, &VideoBackgroundAbstractBehaviour_t84__CustomAttributeCache/* custom_attributes_cache */
	, &VideoBackgroundAbstractBehaviour_t84_il2cpp_TypeInfo/* cast_class */
	, &VideoBackgroundAbstractBehaviour_t84_0_0_0/* byval_arg */
	, &VideoBackgroundAbstractBehaviour_t84_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VideoBackgroundAbstractBehaviour_t84)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 11/* method_count */
	, 1/* property_count */
	, 8/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.VideoTextureRendererAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendere.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo;
// Vuforia.VideoTextureRendererAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VideoTextureRendereMethodDeclarations.h"

// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2D.h"
// UnityEngine.TextureFormat
#include "UnityEngine_UnityEngine_TextureFormat.h"
// UnityEngine.FilterMode
#include "UnityEngine_UnityEngine_FilterMode.h"
// UnityEngine.TextureWrapMode
#include "UnityEngine_UnityEngine_TextureWrapMode.h"
// UnityEngine.Renderer
#include "UnityEngine_UnityEngine_Renderer.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_Material.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_Texture.h"
// Vuforia.QCARRenderer
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer.h"
extern TypeInfo Texture2D_t285_il2cpp_TypeInfo;
extern TypeInfo BackgroundPlaneAbstractBehaviour_t30_il2cpp_TypeInfo;
extern TypeInfo BackgroundPlaneAbstractBehaviourU5BU5D_t894_il2cpp_TypeInfo;
// UnityEngine.Texture2D
#include "UnityEngine_UnityEngine_Texture2DMethodDeclarations.h"
// UnityEngine.Texture
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
// UnityEngine.GameObject
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
// UnityEngine.Material
#include "UnityEngine_UnityEngine_MaterialMethodDeclarations.h"
extern Il2CppType BackgroundPlaneAbstractBehaviour_t30_0_0_0;
extern MethodInfo Texture2D__ctor_m5403_MethodInfo;
extern MethodInfo Texture_set_filterMode_m5404_MethodInfo;
extern MethodInfo Texture_set_wrapMode_m5405_MethodInfo;
extern MethodInfo Texture_GetNativeTextureID_m5406_MethodInfo;
extern MethodInfo QCARAbstractBehaviour_RegisterVideoBgEventHandler_m4123_MethodInfo;
extern MethodInfo GameObject_GetComponent_TisRenderer_t7_m4452_MethodInfo;
extern MethodInfo Renderer_get_material_m239_MethodInfo;
extern MethodInfo Material_set_mainTexture_m5289_MethodInfo;
extern MethodInfo QCARRenderer_get_Instance_m3008_MethodInfo;
extern MethodInfo QCARRenderer_get_VideoBackgroundTexture_m4877_MethodInfo;
extern MethodInfo QCARRenderer_SetVideoBackgroundTexture_m4880_MethodInfo;
extern MethodInfo String_Concat_m1894_MethodInfo;
extern MethodInfo Debug_LogWarning_m4812_MethodInfo;
extern MethodInfo Object_op_Equality_m324_MethodInfo;
struct GameObject_t2;
// UnityEngine.CastHelper`1<UnityEngine.Renderer>
#include "UnityEngine_UnityEngine_CastHelper_1_gen.h"
struct GameObject_t2;
// Declaration !!0 UnityEngine.GameObject::GetComponent<System.Object>()
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
 Object_t * GameObject_GetComponent_TisObject_t_m2034_gshared (GameObject_t2 * __this, MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m2034(__this, method) (Object_t *)GameObject_GetComponent_TisObject_t_m2034_gshared((GameObject_t2 *)__this, method)
// Declaration !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t7_m4452(__this, method) (Renderer_t7 *)GameObject_GetComponent_TisObject_t_m2034_gshared((GameObject_t2 *)__this, method)


// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Awake()
extern MethodInfo VideoTextureRendererAbstractBehaviour_Awake_m4211_MethodInfo;
 void VideoTextureRendererAbstractBehaviour_Awake_m4211 (VideoTextureRendererAbstractBehaviour_t86 * __this, MethodInfo* method){
	{
		Texture2D_t285 * L_0 = (Texture2D_t285 *)il2cpp_codegen_object_new (InitializedTypeInfo(&Texture2D_t285_il2cpp_TypeInfo));
		Texture2D__ctor_m5403(L_0, 0, 0, 7, 0, /*hidden argument*/&Texture2D__ctor_m5403_MethodInfo);
		__this->___mTexture_2 = L_0;
		Texture2D_t285 * L_1 = (__this->___mTexture_2);
		NullCheck(L_1);
		Texture_set_filterMode_m5404(L_1, 1, /*hidden argument*/&Texture_set_filterMode_m5404_MethodInfo);
		Texture2D_t285 * L_2 = (__this->___mTexture_2);
		NullCheck(L_2);
		Texture_set_wrapMode_m5405(L_2, 1, /*hidden argument*/&Texture_set_wrapMode_m5405_MethodInfo);
		Texture2D_t285 * L_3 = (__this->___mTexture_2);
		NullCheck(L_3);
		int32_t L_4 = Texture_GetNativeTextureID_m5406(L_3, /*hidden argument*/&Texture_GetNativeTextureID_m5406_MethodInfo);
		__this->___mNativeTextureID_5 = L_4;
		return;
	}
}
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Start()
extern MethodInfo VideoTextureRendererAbstractBehaviour_Start_m4212_MethodInfo;
 void VideoTextureRendererAbstractBehaviour_Start_m4212 (VideoTextureRendererAbstractBehaviour_t86 * __this, MethodInfo* method){
	QCARAbstractBehaviour_t71 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_0 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&QCARAbstractBehaviour_t71_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		Object_t117 * L_1 = Object_FindObjectOfType_m396(NULL /*static, unused*/, L_0, /*hidden argument*/&Object_FindObjectOfType_m396_MethodInfo);
		V_0 = ((QCARAbstractBehaviour_t71 *)Castclass(L_1, InitializedTypeInfo(&QCARAbstractBehaviour_t71_il2cpp_TypeInfo)));
		bool L_2 = Object_op_Implicit_m397(NULL /*static, unused*/, V_0, /*hidden argument*/&Object_op_Implicit_m397_MethodInfo);
		if (!L_2)
		{
			goto IL_0024;
		}
	}
	{
		NullCheck(V_0);
		QCARAbstractBehaviour_RegisterVideoBgEventHandler_m4123(V_0, __this, /*hidden argument*/&QCARAbstractBehaviour_RegisterVideoBgEventHandler_m4123_MethodInfo);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Update()
extern MethodInfo VideoTextureRendererAbstractBehaviour_Update_m4213_MethodInfo;
 void VideoTextureRendererAbstractBehaviour_Update_m4213 (VideoTextureRendererAbstractBehaviour_t86 * __this, MethodInfo* method){
	BackgroundPlaneAbstractBehaviourU5BU5D_t894* V_0 = {0};
	BackgroundPlaneAbstractBehaviour_t30 * V_1 = {0};
	bool V_2 = false;
	Texture2D_t285 * V_3 = {0};
	BackgroundPlaneAbstractBehaviourU5BU5D_t894* V_4 = {0};
	int32_t V_5 = 0;
	{
		bool L_0 = (__this->___mVideoBgConfigChanged_3);
		if (!L_0)
		{
			goto IL_00d4;
		}
	}
	{
		bool L_1 = (__this->___mTextureAppliedToMaterial_4);
		if (L_1)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_2 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(&BackgroundPlaneAbstractBehaviour_t30_0_0_0), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		ObjectU5BU5D_t816* L_3 = Object_FindObjectsOfType_m4868(NULL /*static, unused*/, L_2, /*hidden argument*/&Object_FindObjectsOfType_m4868_MethodInfo);
		V_0 = ((BackgroundPlaneAbstractBehaviourU5BU5D_t894*)Castclass(L_3, InitializedTypeInfo(&BackgroundPlaneAbstractBehaviourU5BU5D_t894_il2cpp_TypeInfo)));
		V_4 = V_0;
		V_5 = 0;
		goto IL_0057;
	}

IL_0030:
	{
		NullCheck(V_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_4, V_5);
		int32_t L_4 = V_5;
		V_1 = (*(BackgroundPlaneAbstractBehaviour_t30 **)(BackgroundPlaneAbstractBehaviour_t30 **)SZArrayLdElema(V_4, L_4));
		NullCheck(V_1);
		GameObject_t2 * L_5 = Component_get_gameObject_m322(V_1, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		NullCheck(L_5);
		Renderer_t7 * L_6 = GameObject_GetComponent_TisRenderer_t7_m4452(L_5, /*hidden argument*/&GameObject_GetComponent_TisRenderer_t7_m4452_MethodInfo);
		NullCheck(L_6);
		Material_t4 * L_7 = Renderer_get_material_m239(L_6, /*hidden argument*/&Renderer_get_material_m239_MethodInfo);
		Texture2D_t285 * L_8 = (__this->___mTexture_2);
		NullCheck(L_7);
		Material_set_mainTexture_m5289(L_7, L_8, /*hidden argument*/&Material_set_mainTexture_m5289_MethodInfo);
		V_5 = ((int32_t)(V_5+1));
	}

IL_0057:
	{
		NullCheck(V_4);
		if ((((int32_t)V_5) < ((int32_t)(((int32_t)(((Array_t *)V_4)->max_length))))))
		{
			goto IL_0030;
		}
	}
	{
		__this->___mTextureAppliedToMaterial_4 = 1;
	}

IL_0066:
	{
		V_2 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRenderer_t661_il2cpp_TypeInfo));
		QCARRenderer_t661 * L_9 = QCARRenderer_get_Instance_m3008(NULL /*static, unused*/, /*hidden argument*/&QCARRenderer_get_Instance_m3008_MethodInfo);
		NullCheck(L_9);
		Texture2D_t285 * L_10 = (Texture2D_t285 *)VirtFuncInvoker0< Texture2D_t285 * >::Invoke(&QCARRenderer_get_VideoBackgroundTexture_m4877_MethodInfo, L_9);
		V_3 = L_10;
		bool L_11 = Object_op_Inequality_m335(NULL /*static, unused*/, V_3, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_11)
		{
			goto IL_008c;
		}
	}
	{
		Texture2D_t285 * L_12 = (__this->___mTexture_2);
		bool L_13 = Object_op_Inequality_m335(NULL /*static, unused*/, V_3, L_12, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_13)
		{
			goto IL_008c;
		}
	}
	{
		V_2 = 1;
	}

IL_008c:
	{
		if (V_2)
		{
			goto IL_00c3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRenderer_t661_il2cpp_TypeInfo));
		QCARRenderer_t661 * L_14 = QCARRenderer_get_Instance_m3008(NULL /*static, unused*/, /*hidden argument*/&QCARRenderer_get_Instance_m3008_MethodInfo);
		Texture2D_t285 * L_15 = (__this->___mTexture_2);
		int32_t L_16 = (__this->___mNativeTextureID_5);
		NullCheck(L_14);
		bool L_17 = (bool)VirtFuncInvoker2< bool, Texture2D_t285 *, int32_t >::Invoke(&QCARRenderer_SetVideoBackgroundTexture_m4880_MethodInfo, L_14, L_15, L_16);
		if (L_17)
		{
			goto IL_00cd;
		}
	}
	{
		int32_t L_18 = (__this->___mNativeTextureID_5);
		int32_t L_19 = L_18;
		Object_t * L_20 = Box(InitializedTypeInfo(&Int32_t93_il2cpp_TypeInfo), &L_19);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_21 = String_Concat_m1894(NULL /*static, unused*/, (String_t*) &_stringLiteral288, L_20, /*hidden argument*/&String_Concat_m1894_MethodInfo);
		Debug_Log_m419(NULL /*static, unused*/, L_21, /*hidden argument*/&Debug_Log_m419_MethodInfo);
		goto IL_00cd;
	}

IL_00c3:
	{
		Debug_LogWarning_m4812(NULL /*static, unused*/, (String_t*) &_stringLiteral289, /*hidden argument*/&Debug_LogWarning_m4812_MethodInfo);
	}

IL_00cd:
	{
		__this->___mVideoBgConfigChanged_3 = 0;
	}

IL_00d4:
	{
		return;
	}
}
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnDestroy()
extern MethodInfo VideoTextureRendererAbstractBehaviour_OnDestroy_m4214_MethodInfo;
 void VideoTextureRendererAbstractBehaviour_OnDestroy_m4214 (VideoTextureRendererAbstractBehaviour_t86 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRenderer_t661_il2cpp_TypeInfo));
		QCARRenderer_t661 * L_0 = QCARRenderer_get_Instance_m3008(NULL /*static, unused*/, /*hidden argument*/&QCARRenderer_get_Instance_m3008_MethodInfo);
		NullCheck(L_0);
		Texture2D_t285 * L_1 = (Texture2D_t285 *)VirtFuncInvoker0< Texture2D_t285 * >::Invoke(&QCARRenderer_get_VideoBackgroundTexture_m4877_MethodInfo, L_0);
		Texture2D_t285 * L_2 = (__this->___mTexture_2);
		bool L_3 = Object_op_Equality_m324(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/&Object_op_Equality_m324_MethodInfo);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRenderer_t661_il2cpp_TypeInfo));
		QCARRenderer_t661 * L_4 = QCARRenderer_get_Instance_m3008(NULL /*static, unused*/, /*hidden argument*/&QCARRenderer_get_Instance_m3008_MethodInfo);
		NullCheck(L_4);
		VirtFuncInvoker2< bool, Texture2D_t285 *, int32_t >::Invoke(&QCARRenderer_SetVideoBackgroundTexture_m4880_MethodInfo, L_4, (Texture2D_t285 *)NULL, 0);
	}

IL_0024:
	{
		return;
	}
}
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern MethodInfo VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m614_MethodInfo;
 void VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m614 (VideoTextureRendererAbstractBehaviour_t86 * __this, MethodInfo* method){
	{
		__this->___mVideoBgConfigChanged_3 = 1;
		return;
	}
}
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::.ctor()
extern MethodInfo VideoTextureRendererAbstractBehaviour__ctor_m613_MethodInfo;
 void VideoTextureRendererAbstractBehaviour__ctor_m613 (VideoTextureRendererAbstractBehaviour_t86 * __this, MethodInfo* method){
	{
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.VideoTextureRendererAbstractBehaviour
extern Il2CppType Texture2D_t285_0_0_1;
FieldInfo VideoTextureRendererAbstractBehaviour_t86____mTexture_2_FieldInfo = 
{
	"mTexture"/* name */
	, &Texture2D_t285_0_0_1/* type */
	, &VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* parent */
	, offsetof(VideoTextureRendererAbstractBehaviour_t86, ___mTexture_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo VideoTextureRendererAbstractBehaviour_t86____mVideoBgConfigChanged_3_FieldInfo = 
{
	"mVideoBgConfigChanged"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* parent */
	, offsetof(VideoTextureRendererAbstractBehaviour_t86, ___mVideoBgConfigChanged_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo VideoTextureRendererAbstractBehaviour_t86____mTextureAppliedToMaterial_4_FieldInfo = 
{
	"mTextureAppliedToMaterial"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* parent */
	, offsetof(VideoTextureRendererAbstractBehaviour_t86, ___mTextureAppliedToMaterial_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo VideoTextureRendererAbstractBehaviour_t86____mNativeTextureID_5_FieldInfo = 
{
	"mNativeTextureID"/* name */
	, &Int32_t93_0_0_1/* type */
	, &VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* parent */
	, offsetof(VideoTextureRendererAbstractBehaviour_t86, ___mNativeTextureID_5)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* VideoTextureRendererAbstractBehaviour_t86_FieldInfos[] =
{
	&VideoTextureRendererAbstractBehaviour_t86____mTexture_2_FieldInfo,
	&VideoTextureRendererAbstractBehaviour_t86____mVideoBgConfigChanged_3_FieldInfo,
	&VideoTextureRendererAbstractBehaviour_t86____mTextureAppliedToMaterial_4_FieldInfo,
	&VideoTextureRendererAbstractBehaviour_t86____mNativeTextureID_5_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Awake()
MethodInfo VideoTextureRendererAbstractBehaviour_Awake_m4211_MethodInfo = 
{
	"Awake"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_Awake_m4211/* method */
	, &VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2424/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Start()
MethodInfo VideoTextureRendererAbstractBehaviour_Start_m4212_MethodInfo = 
{
	"Start"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_Start_m4212/* method */
	, &VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2425/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::Update()
MethodInfo VideoTextureRendererAbstractBehaviour_Update_m4213_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_Update_m4213/* method */
	, &VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2426/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnDestroy()
MethodInfo VideoTextureRendererAbstractBehaviour_OnDestroy_m4214_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_OnDestroy_m4214/* method */
	, &VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2427/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::OnVideoBackgroundConfigChanged()
MethodInfo VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m614_MethodInfo = 
{
	"OnVideoBackgroundConfigChanged"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m614/* method */
	, &VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2428/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VideoTextureRendererAbstractBehaviour::.ctor()
MethodInfo VideoTextureRendererAbstractBehaviour__ctor_m613_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VideoTextureRendererAbstractBehaviour__ctor_m613/* method */
	, &VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2429/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* VideoTextureRendererAbstractBehaviour_t86_MethodInfos[] =
{
	&VideoTextureRendererAbstractBehaviour_Awake_m4211_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_Start_m4212_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_Update_m4213_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_OnDestroy_m4214_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m614_MethodInfo,
	&VideoTextureRendererAbstractBehaviour__ctor_m613_MethodInfo,
	NULL
};
static MethodInfo* VideoTextureRendererAbstractBehaviour_t86_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&VideoTextureRendererAbstractBehaviour_OnVideoBackgroundConfigChanged_m614_MethodInfo,
};
extern TypeInfo IVideoBackgroundEventHandler_t122_il2cpp_TypeInfo;
static TypeInfo* VideoTextureRendererAbstractBehaviour_t86_InterfacesTypeInfos[] = 
{
	&IVideoBackgroundEventHandler_t122_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair VideoTextureRendererAbstractBehaviour_t86_InterfacesOffsets[] = 
{
	{ &IVideoBackgroundEventHandler_t122_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType VideoTextureRendererAbstractBehaviour_t86_0_0_0;
extern Il2CppType VideoTextureRendererAbstractBehaviour_t86_1_0_0;
struct VideoTextureRendererAbstractBehaviour_t86;
TypeInfo VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "VideoTextureRendererAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VideoTextureRendererAbstractBehaviour_t86_MethodInfos/* methods */
	, NULL/* properties */
	, VideoTextureRendererAbstractBehaviour_t86_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* element_class */
	, VideoTextureRendererAbstractBehaviour_t86_InterfacesTypeInfos/* implemented_interfaces */
	, VideoTextureRendererAbstractBehaviour_t86_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &VideoTextureRendererAbstractBehaviour_t86_il2cpp_TypeInfo/* cast_class */
	, &VideoTextureRendererAbstractBehaviour_t86_0_0_0/* byval_arg */
	, &VideoTextureRendererAbstractBehaviour_t86_1_0_0/* this_arg */
	, VideoTextureRendererAbstractBehaviour_t86_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VideoTextureRendererAbstractBehaviour_t86)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 6/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 5/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstra.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo;
// Vuforia.VirtualButtonAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstraMethodDeclarations.h"

// Vuforia.VirtualButton
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton.h"
// Vuforia.VirtualButton/Sensitivity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton_Sensi.h"
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_46.h"
// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstract.h"
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21.h"
extern TypeInfo List_1_t770_il2cpp_TypeInfo;
extern TypeInfo IVirtualButtonEventHandler_t771_il2cpp_TypeInfo;
extern TypeInfo RectangleData_t588_il2cpp_TypeInfo;
extern TypeInfo VirtualButton_t595_il2cpp_TypeInfo;
extern TypeInfo Sensitivity_t745_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t903_il2cpp_TypeInfo;
extern TypeInfo ImageTarget_t572_il2cpp_TypeInfo;
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_gen_46MethodDeclarations.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
// Vuforia.VirtualButton
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButtonMethodDeclarations.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_21MethodDeclarations.h"
// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstractMethodDeclarations.h"
// System.Math
#include "mscorlib_System_MathMethodDeclarations.h"
extern MethodInfo Matrix4x4_get_zero_m5407_MethodInfo;
extern MethodInfo List_1__ctor_m5408_MethodInfo;
extern MethodInfo List_1_Add_m5409_MethodInfo;
extern MethodInfo List_1_Remove_m5410_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4224_MethodInfo;
extern MethodInfo Vector2_get_zero_m232_MethodInfo;
extern MethodInfo Transform_get_position_m299_MethodInfo;
extern MethodInfo Transform_InverseTransformPoint_m2283_MethodInfo;
extern MethodInfo Transform_get_lossyScale_m656_MethodInfo;
extern MethodInfo Vector3_get_Item_m2386_MethodInfo;
extern MethodInfo Vector2_op_Multiply_m236_MethodInfo;
extern MethodInfo Vector2_Scale_m2336_MethodInfo;
extern MethodInfo Vector2_op_Subtraction_m265_MethodInfo;
extern MethodInfo Vector2_op_Addition_m237_MethodInfo;
extern MethodInfo VirtualButton_SetArea_m5305_MethodInfo;
extern MethodInfo VirtualButton_SetSensitivity_m5306_MethodInfo;
extern MethodInfo Behaviour_get_enabled_m655_MethodInfo;
extern MethodInfo VirtualButton_SetEnabled_m5307_MethodInfo;
extern MethodInfo Transform_get_localScale_m303_MethodInfo;
extern MethodInfo Object_get_name_m226_MethodInfo;
extern MethodInfo String_Concat_m418_MethodInfo;
extern MethodInfo Vector3__ctor_m248_MethodInfo;
extern MethodInfo Transform_set_localScale_m2332_MethodInfo;
extern MethodInfo Vector3_get_zero_m321_MethodInfo;
extern MethodInfo Transform_set_localPosition_m2331_MethodInfo;
extern MethodInfo Transform_TransformPoint_m2418_MethodInfo;
extern MethodInfo Transform_set_position_m319_MethodInfo;
extern MethodInfo Transform_get_rotation_m301_MethodInfo;
extern MethodInfo Transform_set_rotation_m5032_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_CalculateButtonArea_m4219_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_Equals_m4228_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_UpdateEnabled_m4222_MethodInfo;
extern MethodInfo List_1_GetEnumerator_m5411_MethodInfo;
extern MethodInfo Enumerator_get_Current_m5412_MethodInfo;
extern MethodInfo IVirtualButtonEventHandler_OnButtonPressed_m5323_MethodInfo;
extern MethodInfo Enumerator_MoveNext_m5413_MethodInfo;
extern MethodInfo IVirtualButtonEventHandler_OnButtonReleased_m5324_MethodInfo;
extern MethodInfo GameObject_GetComponent_TisImageTargetAbstractBehaviour_t50_m5414_MethodInfo;
extern MethodInfo GameObject_get_transform_m313_MethodInfo;
extern MethodInfo Vector2_get_Item_m2215_MethodInfo;
extern MethodInfo Vector3_op_Division_m4383_MethodInfo;
extern MethodInfo Component_GetComponent_TisRenderer_t7_m234_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_UpdatePose_m629_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4220_MethodInfo;
extern MethodInfo VirtualButtonAbstractBehaviour_UpdateSensitivity_m4221_MethodInfo;
extern MethodInfo Application_get_isPlaying_m2353_MethodInfo;
extern MethodInfo ImageTargetAbstractBehaviour_get_ImageTarget_m4079_MethodInfo;
extern MethodInfo ImageTarget_DestroyVirtualButton_m4575_MethodInfo;
extern MethodInfo Math_Abs_m4533_MethodInfo;
struct GameObject_t2;
// UnityEngine.CastHelper`1<Vuforia.ImageTargetAbstractBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_25.h"
// Declaration !!0 UnityEngine.GameObject::GetComponent<Vuforia.ImageTargetAbstractBehaviour>()
// !!0 UnityEngine.GameObject::GetComponent<Vuforia.ImageTargetAbstractBehaviour>()
#define GameObject_GetComponent_TisImageTargetAbstractBehaviour_t50_m5414(__this, method) (ImageTargetAbstractBehaviour_t50 *)GameObject_GetComponent_TisObject_t_m2034_gshared((GameObject_t2 *)__this, method)
struct Component_t100;
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t7_m234(__this, method) (Renderer_t7 *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)


// System.String Vuforia.VirtualButtonAbstractBehaviour::get_VirtualButtonName()
extern MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButtonName_m616_MethodInfo;
 String_t* VirtualButtonAbstractBehaviour_get_VirtualButtonName_m616 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mName_3);
		return L_0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_Pressed()
extern MethodInfo VirtualButtonAbstractBehaviour_get_Pressed_m4215_MethodInfo;
 bool VirtualButtonAbstractBehaviour_get_Pressed_m4215 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mPressed_10);
		return L_0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_HasUpdatedPose()
extern MethodInfo VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m628_MethodInfo;
 bool VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m628 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mHasUpdatedPose_5);
		return L_0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_UnregisterOnDestroy()
extern MethodInfo VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m626_MethodInfo;
 bool VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m626 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mUnregisterOnDestroy_14);
		return L_0;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::set_UnregisterOnDestroy(System.Boolean)
extern MethodInfo VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m627_MethodInfo;
 void VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m627 (VirtualButtonAbstractBehaviour_t56 * __this, bool ___value, MethodInfo* method){
	{
		__this->___mUnregisterOnDestroy_14 = ___value;
		return;
	}
}
// Vuforia.VirtualButton Vuforia.VirtualButtonAbstractBehaviour::get_VirtualButton()
extern MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButton_m4216_MethodInfo;
 VirtualButton_t595 * VirtualButtonAbstractBehaviour_get_VirtualButton_m4216 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		VirtualButton_t595 * L_0 = (__this->___mVirtualButton_15);
		return L_0;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::.ctor()
extern MethodInfo VirtualButtonAbstractBehaviour__ctor_m615_MethodInfo;
 void VirtualButtonAbstractBehaviour__ctor_m615 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		Matrix4x4_t176  L_0 = Matrix4x4_get_zero_m5407(NULL /*static, unused*/, /*hidden argument*/&Matrix4x4_get_zero_m5407_MethodInfo);
		__this->___mPrevTransform_6 = L_0;
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		__this->___mName_3 = (String_t*) &_stringLiteral113;
		__this->___mPressed_10 = 0;
		__this->___mSensitivity_4 = 2;
		__this->___mSensitivityDirty_8 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&List_1_t770_il2cpp_TypeInfo));
		List_1_t770 * L_1 = (List_1_t770 *)il2cpp_codegen_object_new (InitializedTypeInfo(&List_1_t770_il2cpp_TypeInfo));
		List_1__ctor_m5408(L_1, /*hidden argument*/&List_1__ctor_m5408_MethodInfo);
		__this->___mHandlers_11 = L_1;
		__this->___mHasUpdatedPose_5 = 0;
		return;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::RegisterEventHandler(Vuforia.IVirtualButtonEventHandler)
extern MethodInfo VirtualButtonAbstractBehaviour_RegisterEventHandler_m4217_MethodInfo;
 void VirtualButtonAbstractBehaviour_RegisterEventHandler_m4217 (VirtualButtonAbstractBehaviour_t56 * __this, Object_t * ___eventHandler, MethodInfo* method){
	{
		List_1_t770 * L_0 = (__this->___mHandlers_11);
		NullCheck(L_0);
		VirtActionInvoker1< Object_t * >::Invoke(&List_1_Add_m5409_MethodInfo, L_0, ___eventHandler);
		return;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UnregisterEventHandler(Vuforia.IVirtualButtonEventHandler)
extern MethodInfo VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4218_MethodInfo;
 bool VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4218 (VirtualButtonAbstractBehaviour_t56 * __this, Object_t * ___eventHandler, MethodInfo* method){
	{
		List_1_t770 * L_0 = (__this->___mHandlers_11);
		NullCheck(L_0);
		bool L_1 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&List_1_Remove_m5410_MethodInfo, L_0, ___eventHandler);
		return L_1;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::CalculateButtonArea(UnityEngine.Vector2&,UnityEngine.Vector2&)
 bool VirtualButtonAbstractBehaviour_CalculateButtonArea_m4219 (VirtualButtonAbstractBehaviour_t56 * __this, Vector2_t9 * ___topLeft, Vector2_t9 * ___bottomRight, MethodInfo* method){
	ImageTargetAbstractBehaviour_t50 * V_0 = {0};
	Vector3_t13  V_1 = {0};
	float V_2 = 0.0f;
	Vector2_t9  V_3 = {0};
	Vector2_t9  V_4 = {0};
	Vector2_t9  V_5 = {0};
	Vector2_t9  V_6 = {0};
	Vector3_t13  V_7 = {0};
	Vector3_t13  V_8 = {0};
	Vector3_t13  V_9 = {0};
	{
		ImageTargetAbstractBehaviour_t50 * L_0 = VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4224(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4224_MethodInfo);
		V_0 = L_0;
		bool L_1 = Object_op_Equality_m324(NULL /*static, unused*/, V_0, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Equality_m324_MethodInfo);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		Vector2_t9  L_2 = Vector2_get_zero_m232(NULL /*static, unused*/, /*hidden argument*/&Vector2_get_zero_m232_MethodInfo);
		Vector2_t9  L_3 = L_2;
		V_6 = L_3;
		*___bottomRight = L_3;
		*___topLeft = V_6;
		return 0;
	}

IL_0028:
	{
		NullCheck(V_0);
		Transform_t10 * L_4 = Component_get_transform_m219(V_0, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		Transform_t10 * L_5 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_5);
		Vector3_t13  L_6 = Transform_get_position_m299(L_5, /*hidden argument*/&Transform_get_position_m299_MethodInfo);
		NullCheck(L_4);
		Vector3_t13  L_7 = Transform_InverseTransformPoint_m2283(L_4, L_6, /*hidden argument*/&Transform_InverseTransformPoint_m2283_MethodInfo);
		V_1 = L_7;
		NullCheck(V_0);
		Transform_t10 * L_8 = Component_get_transform_m219(V_0, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_8);
		Vector3_t13  L_9 = Transform_get_lossyScale_m656(L_8, /*hidden argument*/&Transform_get_lossyScale_m656_MethodInfo);
		V_7 = L_9;
		float L_10 = Vector3_get_Item_m2386((&V_7), 0, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		V_2 = L_10;
		float L_11 = Vector3_get_Item_m2386((&V_1), 0, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		float L_12 = Vector3_get_Item_m2386((&V_1), 2, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		Vector2__ctor_m233((&V_3), ((float)((float)L_11*(float)V_2)), ((float)((float)L_12*(float)V_2)), /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		Transform_t10 * L_13 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_13);
		Vector3_t13  L_14 = Transform_get_lossyScale_m656(L_13, /*hidden argument*/&Transform_get_lossyScale_m656_MethodInfo);
		V_8 = L_14;
		float L_15 = Vector3_get_Item_m2386((&V_8), 0, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		Transform_t10 * L_16 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_16);
		Vector3_t13  L_17 = Transform_get_lossyScale_m656(L_16, /*hidden argument*/&Transform_get_lossyScale_m656_MethodInfo);
		V_9 = L_17;
		float L_18 = Vector3_get_Item_m2386((&V_9), 2, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		Vector2__ctor_m233((&V_4), L_15, L_18, /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		Vector2_t9  L_19 = Vector2_op_Multiply_m236(NULL /*static, unused*/, V_4, (0.5f), /*hidden argument*/&Vector2_op_Multiply_m236_MethodInfo);
		Vector2_t9  L_20 = {0};
		Vector2__ctor_m233(&L_20, (1.0f), (-1.0f), /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		Vector2_t9  L_21 = Vector2_Scale_m2336(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/&Vector2_Scale_m2336_MethodInfo);
		V_5 = L_21;
		Vector2_t9  L_22 = Vector2_op_Subtraction_m265(NULL /*static, unused*/, V_3, V_5, /*hidden argument*/&Vector2_op_Subtraction_m265_MethodInfo);
		*___topLeft = L_22;
		Vector2_t9  L_23 = Vector2_op_Addition_m237(NULL /*static, unused*/, V_3, V_5, /*hidden argument*/&Vector2_op_Addition_m237_MethodInfo);
		*___bottomRight = L_23;
		return 1;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateAreaRectangle()
 bool VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4220 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	RectangleData_t588  V_0 = {0};
	{
		Initobj (&RectangleData_t588_il2cpp_TypeInfo, (&V_0));
		Vector2_t9 * L_0 = &(__this->___mLeftTop_12);
		NullCheck(L_0);
		float L_1 = (L_0->___x_1);
		NullCheck((&V_0));
		(&V_0)->___leftTopX_0 = L_1;
		Vector2_t9 * L_2 = &(__this->___mLeftTop_12);
		NullCheck(L_2);
		float L_3 = (L_2->___y_2);
		NullCheck((&V_0));
		(&V_0)->___leftTopY_1 = L_3;
		Vector2_t9 * L_4 = &(__this->___mRightBottom_13);
		NullCheck(L_4);
		float L_5 = (L_4->___x_1);
		NullCheck((&V_0));
		(&V_0)->___rightBottomX_2 = L_5;
		Vector2_t9 * L_6 = &(__this->___mRightBottom_13);
		NullCheck(L_6);
		float L_7 = (L_6->___y_2);
		NullCheck((&V_0));
		(&V_0)->___rightBottomY_3 = L_7;
		VirtualButton_t595 * L_8 = (__this->___mVirtualButton_15);
		if (L_8)
		{
			goto IL_005a;
		}
	}
	{
		return 0;
	}

IL_005a:
	{
		VirtualButton_t595 * L_9 = (__this->___mVirtualButton_15);
		NullCheck(L_9);
		bool L_10 = (bool)VirtFuncInvoker1< bool, RectangleData_t588  >::Invoke(&VirtualButton_SetArea_m5305_MethodInfo, L_9, V_0);
		return L_10;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateSensitivity()
 bool VirtualButtonAbstractBehaviour_UpdateSensitivity_m4221 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		VirtualButton_t595 * L_0 = (__this->___mVirtualButton_15);
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return 0;
	}

IL_000a:
	{
		VirtualButton_t595 * L_1 = (__this->___mVirtualButton_15);
		int32_t L_2 = (__this->___mSensitivity_4);
		NullCheck(L_1);
		bool L_3 = (bool)VirtFuncInvoker1< bool, int32_t >::Invoke(&VirtualButton_SetSensitivity_m5306_MethodInfo, L_1, L_2);
		return L_3;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateEnabled()
 bool VirtualButtonAbstractBehaviour_UpdateEnabled_m4222 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		VirtualButton_t595 * L_0 = (__this->___mVirtualButton_15);
		bool L_1 = Behaviour_get_enabled_m655(__this, /*hidden argument*/&Behaviour_get_enabled_m655_MethodInfo);
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, bool >::Invoke(&VirtualButton_SetEnabled_m5307_MethodInfo, L_0, L_1);
		return L_2;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdatePose()
 bool VirtualButtonAbstractBehaviour_UpdatePose_m629 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	ImageTargetAbstractBehaviour_t50 * V_0 = {0};
	Transform_t10 * V_1 = {0};
	Vector3_t13  V_2 = {0};
	Vector3_t13  V_3 = {0};
	Vector2_t9  V_4 = {0};
	Vector2_t9  V_5 = {0};
	float V_6 = 0.0f;
	Vector3_t13  V_7 = {0};
	Vector3_t13  V_8 = {0};
	Vector3_t13  V_9 = {0};
	Vector3_t13  V_10 = {0};
	Vector3_t13  V_11 = {0};
	Vector3_t13  V_12 = {0};
	Vector3_t13  V_13 = {0};
	Vector3_t13  V_14 = {0};
	{
		ImageTargetAbstractBehaviour_t50 * L_0 = VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4224(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4224_MethodInfo);
		V_0 = L_0;
		bool L_1 = Object_op_Equality_m324(NULL /*static, unused*/, V_0, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Equality_m324_MethodInfo);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return 0;
	}

IL_0012:
	{
		Transform_t10 * L_2 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_2);
		Transform_t10 * L_3 = Transform_get_parent_m220(L_2, /*hidden argument*/&Transform_get_parent_m220_MethodInfo);
		V_1 = L_3;
		goto IL_00c3;
	}

IL_0023:
	{
		NullCheck(V_1);
		Vector3_t13  L_4 = Transform_get_localScale_m303(V_1, /*hidden argument*/&Transform_get_localScale_m303_MethodInfo);
		V_7 = L_4;
		float L_5 = Vector3_get_Item_m2386((&V_7), 0, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		NullCheck(V_1);
		Vector3_t13  L_6 = Transform_get_localScale_m303(V_1, /*hidden argument*/&Transform_get_localScale_m303_MethodInfo);
		V_8 = L_6;
		float L_7 = Vector3_get_Item_m2386((&V_8), 1, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		if ((((float)L_5) != ((float)L_7)))
		{
			goto IL_0067;
		}
	}
	{
		NullCheck(V_1);
		Vector3_t13  L_8 = Transform_get_localScale_m303(V_1, /*hidden argument*/&Transform_get_localScale_m303_MethodInfo);
		V_9 = L_8;
		float L_9 = Vector3_get_Item_m2386((&V_9), 0, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		NullCheck(V_1);
		Vector3_t13  L_10 = Transform_get_localScale_m303(V_1, /*hidden argument*/&Transform_get_localScale_m303_MethodInfo);
		V_10 = L_10;
		float L_11 = Vector3_get_Item_m2386((&V_10), 2, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		if ((((float)L_9) == ((float)L_11)))
		{
			goto IL_00bc;
		}
	}

IL_0067:
	{
		NullCheck(V_1);
		String_t* L_12 = Object_get_name_m226(V_1, /*hidden argument*/&Object_get_name_m226_MethodInfo);
		IL2CPP_RUNTIME_CLASS_INIT((&String_t_il2cpp_TypeInfo));
		String_t* L_13 = String_Concat_m418(NULL /*static, unused*/, (String_t*) &_stringLiteral290, L_12, (String_t*) &_stringLiteral291, /*hidden argument*/&String_Concat_m418_MethodInfo);
		Debug_LogWarning_m4812(NULL /*static, unused*/, L_13, /*hidden argument*/&Debug_LogWarning_m4812_MethodInfo);
		NullCheck(V_1);
		Vector3_t13  L_14 = Transform_get_localScale_m303(V_1, /*hidden argument*/&Transform_get_localScale_m303_MethodInfo);
		V_11 = L_14;
		float L_15 = Vector3_get_Item_m2386((&V_11), 0, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		NullCheck(V_1);
		Vector3_t13  L_16 = Transform_get_localScale_m303(V_1, /*hidden argument*/&Transform_get_localScale_m303_MethodInfo);
		V_12 = L_16;
		float L_17 = Vector3_get_Item_m2386((&V_12), 0, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		NullCheck(V_1);
		Vector3_t13  L_18 = Transform_get_localScale_m303(V_1, /*hidden argument*/&Transform_get_localScale_m303_MethodInfo);
		V_13 = L_18;
		float L_19 = Vector3_get_Item_m2386((&V_13), 0, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		Vector3_t13  L_20 = {0};
		Vector3__ctor_m248(&L_20, L_15, L_17, L_19, /*hidden argument*/&Vector3__ctor_m248_MethodInfo);
		NullCheck(V_1);
		Transform_set_localScale_m2332(V_1, L_20, /*hidden argument*/&Transform_set_localScale_m2332_MethodInfo);
	}

IL_00bc:
	{
		NullCheck(V_1);
		Transform_t10 * L_21 = Transform_get_parent_m220(V_1, /*hidden argument*/&Transform_get_parent_m220_MethodInfo);
		V_1 = L_21;
	}

IL_00c3:
	{
		bool L_22 = Object_op_Inequality_m335(NULL /*static, unused*/, V_1, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (L_22)
		{
			goto IL_0023;
		}
	}
	{
		__this->___mHasUpdatedPose_5 = 1;
		Transform_t10 * L_23 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_23);
		Transform_t10 * L_24 = Transform_get_parent_m220(L_23, /*hidden argument*/&Transform_get_parent_m220_MethodInfo);
		bool L_25 = Object_op_Inequality_m335(NULL /*static, unused*/, L_24, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_25)
		{
			goto IL_0116;
		}
	}
	{
		Transform_t10 * L_26 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_26);
		Transform_t10 * L_27 = Transform_get_parent_m220(L_26, /*hidden argument*/&Transform_get_parent_m220_MethodInfo);
		NullCheck(L_27);
		GameObject_t2 * L_28 = Component_get_gameObject_m322(L_27, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		NullCheck(V_0);
		GameObject_t2 * L_29 = Component_get_gameObject_m322(V_0, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		bool L_30 = Object_op_Inequality_m335(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_30)
		{
			goto IL_0116;
		}
	}
	{
		Transform_t10 * L_31 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		Vector3_t13  L_32 = Vector3_get_zero_m321(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_zero_m321_MethodInfo);
		NullCheck(L_31);
		Transform_set_localPosition_m2331(L_31, L_32, /*hidden argument*/&Transform_set_localPosition_m2331_MethodInfo);
	}

IL_0116:
	{
		NullCheck(V_0);
		Transform_t10 * L_33 = Component_get_transform_m219(V_0, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		Transform_t10 * L_34 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_34);
		Vector3_t13  L_35 = Transform_get_position_m299(L_34, /*hidden argument*/&Transform_get_position_m299_MethodInfo);
		NullCheck(L_33);
		Vector3_t13  L_36 = Transform_InverseTransformPoint_m2283(L_33, L_35, /*hidden argument*/&Transform_InverseTransformPoint_m2283_MethodInfo);
		V_2 = L_36;
		NullCheck((&V_2));
		(&V_2)->___y_2 = (0.001f);
		NullCheck(V_0);
		Transform_t10 * L_37 = Component_get_transform_m219(V_0, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_37);
		Vector3_t13  L_38 = Transform_TransformPoint_m2418(L_37, V_2, /*hidden argument*/&Transform_TransformPoint_m2418_MethodInfo);
		V_3 = L_38;
		Transform_t10 * L_39 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_39);
		Transform_set_position_m319(L_39, V_3, /*hidden argument*/&Transform_set_position_m319_MethodInfo);
		Transform_t10 * L_40 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(V_0);
		Transform_t10 * L_41 = Component_get_transform_m219(V_0, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_41);
		Quaternion_t12  L_42 = Transform_get_rotation_m301(L_41, /*hidden argument*/&Transform_get_rotation_m301_MethodInfo);
		NullCheck(L_40);
		Transform_set_rotation_m5032(L_40, L_42, /*hidden argument*/&Transform_set_rotation_m5032_MethodInfo);
		VirtualButtonAbstractBehaviour_CalculateButtonArea_m4219(__this, (&V_4), (&V_5), /*hidden argument*/&VirtualButtonAbstractBehaviour_CalculateButtonArea_m4219_MethodInfo);
		NullCheck(V_0);
		Transform_t10 * L_43 = Component_get_transform_m219(V_0, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_43);
		Vector3_t13  L_44 = Transform_get_localScale_m303(L_43, /*hidden argument*/&Transform_get_localScale_m303_MethodInfo);
		V_14 = L_44;
		float L_45 = Vector3_get_Item_m2386((&V_14), 0, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		V_6 = ((float)((float)L_45*(float)(0.001f)));
		Vector2_t9  L_46 = (__this->___mLeftTop_12);
		bool L_47 = VirtualButtonAbstractBehaviour_Equals_m4228(NULL /*static, unused*/, V_4, L_46, V_6, /*hidden argument*/&VirtualButtonAbstractBehaviour_Equals_m4228_MethodInfo);
		if (!L_47)
		{
			goto IL_01b2;
		}
	}
	{
		Vector2_t9  L_48 = (__this->___mRightBottom_13);
		bool L_49 = VirtualButtonAbstractBehaviour_Equals_m4228(NULL /*static, unused*/, V_5, L_48, V_6, /*hidden argument*/&VirtualButtonAbstractBehaviour_Equals_m4228_MethodInfo);
		if (L_49)
		{
			goto IL_01c4;
		}
	}

IL_01b2:
	{
		__this->___mLeftTop_12 = V_4;
		__this->___mRightBottom_13 = V_5;
		return 1;
	}

IL_01c4:
	{
		return 0;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnTrackerUpdated(System.Boolean)
extern MethodInfo VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4223_MethodInfo;
 void VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4223 (VirtualButtonAbstractBehaviour_t56 * __this, bool ___pressed, MethodInfo* method){
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	Enumerator_t903  V_2 = {0};
	Enumerator_t903  V_3 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t152 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t152 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		bool L_0 = (__this->___mPreviouslyEnabled_9);
		bool L_1 = Behaviour_get_enabled_m655(__this, /*hidden argument*/&Behaviour_get_enabled_m655_MethodInfo);
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0021;
		}
	}
	{
		bool L_2 = Behaviour_get_enabled_m655(__this, /*hidden argument*/&Behaviour_get_enabled_m655_MethodInfo);
		__this->___mPreviouslyEnabled_9 = L_2;
		VirtualButtonAbstractBehaviour_UpdateEnabled_m4222(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_UpdateEnabled_m4222_MethodInfo);
	}

IL_0021:
	{
		bool L_3 = Behaviour_get_enabled_m655(__this, /*hidden argument*/&Behaviour_get_enabled_m655_MethodInfo);
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		return;
	}

IL_002a:
	{
		bool L_4 = (__this->___mPressed_10);
		if ((((int32_t)L_4) == ((int32_t)___pressed)))
		{
			goto IL_00aa;
		}
	}
	{
		List_1_t770 * L_5 = (__this->___mHandlers_11);
		if (!L_5)
		{
			goto IL_00aa;
		}
	}
	{
		if (!___pressed)
		{
			goto IL_0074;
		}
	}
	{
		List_1_t770 * L_6 = (__this->___mHandlers_11);
		NullCheck(L_6);
		Enumerator_t903  L_7 = List_1_GetEnumerator_m5411(L_6, /*hidden argument*/&List_1_GetEnumerator_m5411_MethodInfo);
		V_2 = L_7;
	}

IL_004a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005b;
		}

IL_004c:
		{
			Object_t * L_8 = Enumerator_get_Current_m5412((&V_2), /*hidden argument*/&Enumerator_get_Current_m5412_MethodInfo);
			V_0 = L_8;
			NullCheck(V_0);
			InterfaceActionInvoker1< VirtualButtonAbstractBehaviour_t56 * >::Invoke(&IVirtualButtonEventHandler_OnButtonPressed_m5323_MethodInfo, V_0, __this);
		}

IL_005b:
		{
			bool L_9 = Enumerator_MoveNext_m5413((&V_2), /*hidden argument*/&Enumerator_MoveNext_m5413_MethodInfo);
			if (L_9)
			{
				goto IL_004c;
			}
		}

IL_0064:
		{
			// IL_0064: leave.s IL_00aa
			leaveInstructions[0] = 0xAA; // 1
			THROW_SENTINEL(IL_00aa);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0066;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t152 *)e.ex;
		goto IL_0066;
	}

IL_0066:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t903_il2cpp_TypeInfo), &(*(&V_2))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t903_il2cpp_TypeInfo), &(*(&V_2))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0xAA:
				goto IL_00aa;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_0074:
	{
		List_1_t770 * L_10 = (__this->___mHandlers_11);
		NullCheck(L_10);
		Enumerator_t903  L_11 = List_1_GetEnumerator_m5411(L_10, /*hidden argument*/&List_1_GetEnumerator_m5411_MethodInfo);
		V_3 = L_11;
	}

IL_0080:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0091;
		}

IL_0082:
		{
			Object_t * L_12 = Enumerator_get_Current_m5412((&V_3), /*hidden argument*/&Enumerator_get_Current_m5412_MethodInfo);
			V_1 = L_12;
			NullCheck(V_1);
			InterfaceActionInvoker1< VirtualButtonAbstractBehaviour_t56 * >::Invoke(&IVirtualButtonEventHandler_OnButtonReleased_m5324_MethodInfo, V_1, __this);
		}

IL_0091:
		{
			bool L_13 = Enumerator_MoveNext_m5413((&V_3), /*hidden argument*/&Enumerator_MoveNext_m5413_MethodInfo);
			if (L_13)
			{
				goto IL_0082;
			}
		}

IL_009a:
		{
			// IL_009a: leave.s IL_00aa
			leaveInstructions[0] = 0xAA; // 1
			THROW_SENTINEL(IL_00aa);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_009c;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t152 *)e.ex;
		goto IL_009c;
	}

IL_009c:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t903_il2cpp_TypeInfo), &(*(&V_3))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t903_il2cpp_TypeInfo), &(*(&V_3))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0xAA:
				goto IL_00aa;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_00aa:
	{
		__this->___mPressed_10 = ___pressed;
		return;
	}
}
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VirtualButtonAbstractBehaviour::GetImageTargetBehaviour()
 ImageTargetAbstractBehaviour_t50 * VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4224 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	GameObject_t2 * V_0 = {0};
	ImageTargetAbstractBehaviour_t50 * V_1 = {0};
	{
		Transform_t10 * L_0 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_0);
		Transform_t10 * L_1 = Transform_get_parent_m220(L_0, /*hidden argument*/&Transform_get_parent_m220_MethodInfo);
		bool L_2 = Object_op_Equality_m324(NULL /*static, unused*/, L_1, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Equality_m324_MethodInfo);
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		return (ImageTargetAbstractBehaviour_t50 *)NULL;
	}

IL_0015:
	{
		Transform_t10 * L_3 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_3);
		Transform_t10 * L_4 = Transform_get_parent_m220(L_3, /*hidden argument*/&Transform_get_parent_m220_MethodInfo);
		NullCheck(L_4);
		GameObject_t2 * L_5 = Component_get_gameObject_m322(L_4, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		V_0 = L_5;
		goto IL_0060;
	}

IL_0028:
	{
		NullCheck(V_0);
		ImageTargetAbstractBehaviour_t50 * L_6 = GameObject_GetComponent_TisImageTargetAbstractBehaviour_t50_m5414(V_0, /*hidden argument*/&GameObject_GetComponent_TisImageTargetAbstractBehaviour_t50_m5414_MethodInfo);
		V_1 = L_6;
		bool L_7 = Object_op_Inequality_m335(NULL /*static, unused*/, V_1, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_7)
		{
			goto IL_003a;
		}
	}
	{
		return V_1;
	}

IL_003a:
	{
		NullCheck(V_0);
		Transform_t10 * L_8 = GameObject_get_transform_m313(V_0, /*hidden argument*/&GameObject_get_transform_m313_MethodInfo);
		NullCheck(L_8);
		Transform_t10 * L_9 = Transform_get_parent_m220(L_8, /*hidden argument*/&Transform_get_parent_m220_MethodInfo);
		bool L_10 = Object_op_Equality_m324(NULL /*static, unused*/, L_9, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Equality_m324_MethodInfo);
		if (!L_10)
		{
			goto IL_004f;
		}
	}
	{
		return (ImageTargetAbstractBehaviour_t50 *)NULL;
	}

IL_004f:
	{
		NullCheck(V_0);
		Transform_t10 * L_11 = GameObject_get_transform_m313(V_0, /*hidden argument*/&GameObject_get_transform_m313_MethodInfo);
		NullCheck(L_11);
		Transform_t10 * L_12 = Transform_get_parent_m220(L_11, /*hidden argument*/&Transform_get_parent_m220_MethodInfo);
		NullCheck(L_12);
		GameObject_t2 * L_13 = Component_get_gameObject_m322(L_12, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		V_0 = L_13;
	}

IL_0060:
	{
		bool L_14 = Object_op_Inequality_m335(NULL /*static, unused*/, V_0, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (L_14)
		{
			goto IL_0028;
		}
	}
	{
		return (ImageTargetAbstractBehaviour_t50 *)NULL;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetVirtualButtonName(System.String)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m617_MethodInfo;
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m617 (VirtualButtonAbstractBehaviour_t56 * __this, String_t* ___virtualButtonName, MethodInfo* method){
	{
		VirtualButton_t595 * L_0 = (__this->___mVirtualButton_15);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		__this->___mName_3 = ___virtualButtonName;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_SensitivitySetting()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m618_MethodInfo;
 int32_t VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m618 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mSensitivity_4);
		return L_0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetSensitivitySetting(Vuforia.VirtualButton/Sensitivity)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m619_MethodInfo;
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m619 (VirtualButtonAbstractBehaviour_t56 * __this, int32_t ___sensibility, MethodInfo* method){
	{
		VirtualButton_t595 * L_0 = (__this->___mVirtualButton_15);
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		__this->___mSensitivity_4 = ___sensibility;
		__this->___mSensitivityDirty_8 = 1;
		return 1;
	}

IL_0018:
	{
		return 0;
	}
}
// UnityEngine.Matrix4x4 Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_PreviousTransform()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m620_MethodInfo;
 Matrix4x4_t176  VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m620 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		Matrix4x4_t176  L_0 = (__this->___mPrevTransform_6);
		return L_0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPreviousTransform(UnityEngine.Matrix4x4)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m621_MethodInfo;
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m621 (VirtualButtonAbstractBehaviour_t56 * __this, Matrix4x4_t176  ___transformMatrix, MethodInfo* method){
	{
		VirtualButton_t595 * L_0 = (__this->___mVirtualButton_15);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		__this->___mPrevTransform_6 = ___transformMatrix;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_PreviousParent()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m622_MethodInfo;
 GameObject_t2 * VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m622 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		GameObject_t2 * L_0 = (__this->___mPrevParent_7);
		return L_0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPreviousParent(UnityEngine.GameObject)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m623_MethodInfo;
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m623 (VirtualButtonAbstractBehaviour_t56 * __this, GameObject_t2 * ___parent, MethodInfo* method){
	{
		VirtualButton_t595 * L_0 = (__this->___mVirtualButton_15);
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		__this->___mPrevParent_7 = ___parent;
		return 1;
	}

IL_0011:
	{
		return 0;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.InitializeVirtualButton(Vuforia.VirtualButton)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m624_MethodInfo;
 void VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m624 (VirtualButtonAbstractBehaviour_t56 * __this, VirtualButton_t595 * ___virtualButton, MethodInfo* method){
	{
		__this->___mVirtualButton_15 = ___virtualButton;
		return;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPosAndScaleFromButtonArea(UnityEngine.Vector2,UnityEngine.Vector2)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m625_MethodInfo;
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m625 (VirtualButtonAbstractBehaviour_t56 * __this, Vector2_t9  ___topLeft, Vector2_t9  ___bottomRight, MethodInfo* method){
	ImageTargetAbstractBehaviour_t50 * V_0 = {0};
	float V_1 = 0.0f;
	Vector2_t9  V_2 = {0};
	Vector2_t9  V_3 = {0};
	Vector3_t13  V_4 = {0};
	Vector3_t13  V_5 = {0};
	Vector3_t13  V_6 = {0};
	Vector3_t13  V_7 = {0};
	{
		ImageTargetAbstractBehaviour_t50 * L_0 = VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4224(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4224_MethodInfo);
		V_0 = L_0;
		bool L_1 = Object_op_Equality_m324(NULL /*static, unused*/, V_0, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Equality_m324_MethodInfo);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return 0;
	}

IL_0012:
	{
		NullCheck(V_0);
		Transform_t10 * L_2 = Component_get_transform_m219(V_0, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_2);
		Vector3_t13  L_3 = Transform_get_lossyScale_m656(L_2, /*hidden argument*/&Transform_get_lossyScale_m656_MethodInfo);
		V_6 = L_3;
		float L_4 = Vector3_get_Item_m2386((&V_6), 0, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		V_1 = L_4;
		Vector2_t9  L_5 = Vector2_op_Addition_m237(NULL /*static, unused*/, ___topLeft, ___bottomRight, /*hidden argument*/&Vector2_op_Addition_m237_MethodInfo);
		Vector2_t9  L_6 = Vector2_op_Multiply_m236(NULL /*static, unused*/, L_5, (0.5f), /*hidden argument*/&Vector2_op_Multiply_m236_MethodInfo);
		V_2 = L_6;
		float L_7 = Vector2_get_Item_m2215((&___bottomRight), 0, /*hidden argument*/&Vector2_get_Item_m2215_MethodInfo);
		float L_8 = Vector2_get_Item_m2215((&___topLeft), 0, /*hidden argument*/&Vector2_get_Item_m2215_MethodInfo);
		float L_9 = Vector2_get_Item_m2215((&___topLeft), 1, /*hidden argument*/&Vector2_get_Item_m2215_MethodInfo);
		float L_10 = Vector2_get_Item_m2215((&___bottomRight), 1, /*hidden argument*/&Vector2_get_Item_m2215_MethodInfo);
		Vector2__ctor_m233((&V_3), ((float)(L_7-L_8)), ((float)(L_9-L_10)), /*hidden argument*/&Vector2__ctor_m233_MethodInfo);
		float L_11 = Vector2_get_Item_m2215((&V_2), 0, /*hidden argument*/&Vector2_get_Item_m2215_MethodInfo);
		float L_12 = Vector2_get_Item_m2215((&V_2), 1, /*hidden argument*/&Vector2_get_Item_m2215_MethodInfo);
		Vector3__ctor_m248((&V_4), ((float)((float)L_11/(float)V_1)), (0.001f), ((float)((float)L_12/(float)V_1)), /*hidden argument*/&Vector3__ctor_m248_MethodInfo);
		float L_13 = Vector2_get_Item_m2215((&V_3), 0, /*hidden argument*/&Vector2_get_Item_m2215_MethodInfo);
		float L_14 = Vector2_get_Item_m2215((&V_3), 0, /*hidden argument*/&Vector2_get_Item_m2215_MethodInfo);
		float L_15 = Vector2_get_Item_m2215((&V_3), 1, /*hidden argument*/&Vector2_get_Item_m2215_MethodInfo);
		float L_16 = Vector2_get_Item_m2215((&V_3), 1, /*hidden argument*/&Vector2_get_Item_m2215_MethodInfo);
		Vector3__ctor_m248((&V_5), L_13, ((float)((float)((float)(L_14+L_15))*(float)(0.5f))), L_16, /*hidden argument*/&Vector3__ctor_m248_MethodInfo);
		Transform_t10 * L_17 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(V_0);
		Transform_t10 * L_18 = Component_get_transform_m219(V_0, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_18);
		Vector3_t13  L_19 = Transform_TransformPoint_m2418(L_18, V_4, /*hidden argument*/&Transform_TransformPoint_m2418_MethodInfo);
		NullCheck(L_17);
		Transform_set_position_m319(L_17, L_19, /*hidden argument*/&Transform_set_position_m319_MethodInfo);
		Transform_t10 * L_20 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		Transform_t10 * L_21 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		NullCheck(L_21);
		Transform_t10 * L_22 = Transform_get_parent_m220(L_21, /*hidden argument*/&Transform_get_parent_m220_MethodInfo);
		NullCheck(L_22);
		Vector3_t13  L_23 = Transform_get_lossyScale_m656(L_22, /*hidden argument*/&Transform_get_lossyScale_m656_MethodInfo);
		V_7 = L_23;
		float L_24 = Vector3_get_Item_m2386((&V_7), 0, /*hidden argument*/&Vector3_get_Item_m2386_MethodInfo);
		Vector3_t13  L_25 = Vector3_op_Division_m4383(NULL /*static, unused*/, V_5, L_24, /*hidden argument*/&Vector3_op_Division_m4383_MethodInfo);
		NullCheck(L_20);
		Transform_set_localScale_m2332(L_20, L_25, /*hidden argument*/&Transform_set_localScale_m2332_MethodInfo);
		return 1;
	}
}
// UnityEngine.Renderer Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.GetRenderer()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m634_MethodInfo;
 Renderer_t7 * VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m634 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		Renderer_t7 * L_0 = Component_GetComponent_TisRenderer_t7_m234(__this, /*hidden argument*/&Component_GetComponent_TisRenderer_t7_m234_MethodInfo);
		return L_0;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::LateUpdate()
extern MethodInfo VirtualButtonAbstractBehaviour_LateUpdate_m4225_MethodInfo;
 void VirtualButtonAbstractBehaviour_LateUpdate_m4225 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		bool L_0 = VirtualButtonAbstractBehaviour_UpdatePose_m629(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_UpdatePose_m629_MethodInfo);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
		VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4220(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4220_MethodInfo);
	}

IL_000f:
	{
		bool L_1 = (__this->___mSensitivityDirty_8);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		bool L_2 = VirtualButtonAbstractBehaviour_UpdateSensitivity_m4221(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_UpdateSensitivity_m4221_MethodInfo);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		__this->___mSensitivityDirty_8 = 0;
	}

IL_0026:
	{
		return;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnDisable()
extern MethodInfo VirtualButtonAbstractBehaviour_OnDisable_m4226_MethodInfo;
 void VirtualButtonAbstractBehaviour_OnDisable_m4226 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	Object_t * V_0 = {0};
	Enumerator_t903  V_1 = {0};
	int32_t leaveInstructions[1] = {0};
	Exception_t152 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t152 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsQCAREnabled_m487(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsQCAREnabled_m487_MethodInfo);
		if (!L_0)
		{
			goto IL_0075;
		}
	}
	{
		bool L_1 = (__this->___mPreviouslyEnabled_9);
		bool L_2 = Behaviour_get_enabled_m655(__this, /*hidden argument*/&Behaviour_get_enabled_m655_MethodInfo);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0028;
		}
	}
	{
		bool L_3 = Behaviour_get_enabled_m655(__this, /*hidden argument*/&Behaviour_get_enabled_m655_MethodInfo);
		__this->___mPreviouslyEnabled_9 = L_3;
		VirtualButtonAbstractBehaviour_UpdateEnabled_m4222(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_UpdateEnabled_m4222_MethodInfo);
	}

IL_0028:
	{
		bool L_4 = (__this->___mPressed_10);
		if (!L_4)
		{
			goto IL_006e;
		}
	}
	{
		List_1_t770 * L_5 = (__this->___mHandlers_11);
		if (!L_5)
		{
			goto IL_006e;
		}
	}
	{
		List_1_t770 * L_6 = (__this->___mHandlers_11);
		NullCheck(L_6);
		Enumerator_t903  L_7 = List_1_GetEnumerator_m5411(L_6, /*hidden argument*/&List_1_GetEnumerator_m5411_MethodInfo);
		V_1 = L_7;
	}

IL_0044:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0055;
		}

IL_0046:
		{
			Object_t * L_8 = Enumerator_get_Current_m5412((&V_1), /*hidden argument*/&Enumerator_get_Current_m5412_MethodInfo);
			V_0 = L_8;
			NullCheck(V_0);
			InterfaceActionInvoker1< VirtualButtonAbstractBehaviour_t56 * >::Invoke(&IVirtualButtonEventHandler_OnButtonReleased_m5324_MethodInfo, V_0, __this);
		}

IL_0055:
		{
			bool L_9 = Enumerator_MoveNext_m5413((&V_1), /*hidden argument*/&Enumerator_MoveNext_m5413_MethodInfo);
			if (L_9)
			{
				goto IL_0046;
			}
		}

IL_005e:
		{
			// IL_005e: leave.s IL_006e
			leaveInstructions[0] = 0x6E; // 1
			THROW_SENTINEL(IL_006e);
			// finally target depth: 1
		}
	} // end try (depth: 1)
	catch(Il2CppFinallySentinel& e)
	{
		goto IL_0060;
	}
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t152 *)e.ex;
		goto IL_0060;
	}

IL_0060:
	{ // begin finally (depth: 1)
		NullCheck(Box(InitializedTypeInfo(&Enumerator_t903_il2cpp_TypeInfo), &(*(&V_1))));
		InterfaceActionInvoker0::Invoke(&IDisposable_Dispose_m459_MethodInfo, Box(InitializedTypeInfo(&Enumerator_t903_il2cpp_TypeInfo), &(*(&V_1))));
		// finally node depth: 1
		switch (leaveInstructions[0])
		{
			case 0x6E:
				goto IL_006e;
			default:
			{
				#if IL2CPP_DEBUG
				assert( __last_unhandled_exception != 0 && "invalid leaveInstruction at depth 1, __last_unhandled_exception has not been set");
				#endif
				Exception_t152 * _tmp_exception_local = __last_unhandled_exception;
				__last_unhandled_exception = 0;
				il2cpp_codegen_raise_exception(_tmp_exception_local);
			}
		}
	} // end finally (depth: 1)

IL_006e:
	{
		__this->___mPressed_10 = 0;
	}

IL_0075:
	{
		return;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnDestroy()
extern MethodInfo VirtualButtonAbstractBehaviour_OnDestroy_m4227_MethodInfo;
 void VirtualButtonAbstractBehaviour_OnDestroy_m4227 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	ImageTargetAbstractBehaviour_t50 * V_0 = {0};
	{
		bool L_0 = Application_get_isPlaying_m2353(NULL /*static, unused*/, /*hidden argument*/&Application_get_isPlaying_m2353_MethodInfo);
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		bool L_1 = (__this->___mUnregisterOnDestroy_14);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		ImageTargetAbstractBehaviour_t50 * L_2 = VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4224(__this, /*hidden argument*/&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4224_MethodInfo);
		V_0 = L_2;
		bool L_3 = Object_op_Inequality_m335(NULL /*static, unused*/, V_0, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_3)
		{
			goto IL_0031;
		}
	}
	{
		NullCheck(V_0);
		Object_t * L_4 = ImageTargetAbstractBehaviour_get_ImageTarget_m4079(V_0, /*hidden argument*/&ImageTargetAbstractBehaviour_get_ImageTarget_m4079_MethodInfo);
		VirtualButton_t595 * L_5 = (__this->___mVirtualButton_15);
		NullCheck(L_4);
		InterfaceFuncInvoker1< bool, VirtualButton_t595 * >::Invoke(&ImageTarget_DestroyVirtualButton_m4575_MethodInfo, L_4, L_5);
	}

IL_0031:
	{
		return;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Equals(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
 bool VirtualButtonAbstractBehaviour_Equals_m4228 (Object_t * __this/* static, unused */, Vector2_t9  ___vec1, Vector2_t9  ___vec2, float ___threshold, MethodInfo* method){
	Vector2_t9  V_0 = {0};
	{
		Vector2_t9  L_0 = Vector2_op_Subtraction_m265(NULL /*static, unused*/, ___vec1, ___vec2, /*hidden argument*/&Vector2_op_Subtraction_m265_MethodInfo);
		V_0 = L_0;
		NullCheck((&V_0));
		float L_1 = ((&V_0)->___x_1);
		float L_2 = fabsf(L_1);
		if ((((float)L_2) >= ((float)___threshold)))
		{
			goto IL_0027;
		}
	}
	{
		NullCheck((&V_0));
		float L_3 = ((&V_0)->___y_2);
		float L_4 = fabsf(L_3);
		return ((((float)L_4) < ((float)___threshold))? 1 : 0);
	}

IL_0027:
	{
		return 0;
	}
}
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_enabled()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m630_MethodInfo;
 bool VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m630 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		bool L_0 = Behaviour_get_enabled_m655(__this, /*hidden argument*/&Behaviour_get_enabled_m655_MethodInfo);
		return L_0;
	}
}
// System.Void Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.set_enabled(System.Boolean)
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m631_MethodInfo;
 void VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m631 (VirtualButtonAbstractBehaviour_t56 * __this, bool p0, MethodInfo* method){
	{
		Behaviour_set_enabled_m216(__this, p0, /*hidden argument*/&Behaviour_set_enabled_m216_MethodInfo);
		return;
	}
}
// UnityEngine.Transform Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_transform()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m632_MethodInfo;
 Transform_t10 * VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m632 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		Transform_t10 * L_0 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_gameObject()
extern MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m633_MethodInfo;
 GameObject_t2 * VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m633 (VirtualButtonAbstractBehaviour_t56 * __this, MethodInfo* method){
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m322(__this, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		return L_0;
	}
}
// Metadata Definition Vuforia.VirtualButtonAbstractBehaviour
extern Il2CppType Single_t105_0_0_32854;
FieldInfo VirtualButtonAbstractBehaviour_t56____TARGET_OFFSET_2_FieldInfo = 
{
	"TARGET_OFFSET"/* name */
	, &Single_t105_0_0_32854/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mName;
FieldInfo VirtualButtonAbstractBehaviour_t56____mName_3_FieldInfo = 
{
	"mName"/* name */
	, &String_t_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t56, ___mName_3)/* data */
	, &VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mName/* custom_attributes_cache */

};
extern Il2CppType Sensitivity_t745_0_0_1;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mSensitivity;
FieldInfo VirtualButtonAbstractBehaviour_t56____mSensitivity_4_FieldInfo = 
{
	"mSensitivity"/* name */
	, &Sensitivity_t745_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t56, ___mSensitivity_4)/* data */
	, &VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mSensitivity/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mHasUpdatedPose;
FieldInfo VirtualButtonAbstractBehaviour_t56____mHasUpdatedPose_5_FieldInfo = 
{
	"mHasUpdatedPose"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t56, ___mHasUpdatedPose_5)/* data */
	, &VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mHasUpdatedPose/* custom_attributes_cache */

};
extern Il2CppType Matrix4x4_t176_0_0_1;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mPrevTransform;
FieldInfo VirtualButtonAbstractBehaviour_t56____mPrevTransform_6_FieldInfo = 
{
	"mPrevTransform"/* name */
	, &Matrix4x4_t176_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t56, ___mPrevTransform_6)/* data */
	, &VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mPrevTransform/* custom_attributes_cache */

};
extern Il2CppType GameObject_t2_0_0_1;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mPrevParent;
FieldInfo VirtualButtonAbstractBehaviour_t56____mPrevParent_7_FieldInfo = 
{
	"mPrevParent"/* name */
	, &GameObject_t2_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t56, ___mPrevParent_7)/* data */
	, &VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mPrevParent/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t56____mSensitivityDirty_8_FieldInfo = 
{
	"mSensitivityDirty"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t56, ___mSensitivityDirty_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t56____mPreviouslyEnabled_9_FieldInfo = 
{
	"mPreviouslyEnabled"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t56, ___mPreviouslyEnabled_9)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t56____mPressed_10_FieldInfo = 
{
	"mPressed"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t56, ___mPressed_10)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType List_1_t770_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t56____mHandlers_11_FieldInfo = 
{
	"mHandlers"/* name */
	, &List_1_t770_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t56, ___mHandlers_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t9_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t56____mLeftTop_12_FieldInfo = 
{
	"mLeftTop"/* name */
	, &Vector2_t9_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t56, ___mLeftTop_12)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Vector2_t9_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t56____mRightBottom_13_FieldInfo = 
{
	"mRightBottom"/* name */
	, &Vector2_t9_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t56, ___mRightBottom_13)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t56____mUnregisterOnDestroy_14_FieldInfo = 
{
	"mUnregisterOnDestroy"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t56, ___mUnregisterOnDestroy_14)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType VirtualButton_t595_0_0_1;
FieldInfo VirtualButtonAbstractBehaviour_t56____mVirtualButton_15_FieldInfo = 
{
	"mVirtualButton"/* name */
	, &VirtualButton_t595_0_0_1/* type */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, offsetof(VirtualButtonAbstractBehaviour_t56, ___mVirtualButton_15)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* VirtualButtonAbstractBehaviour_t56_FieldInfos[] =
{
	&VirtualButtonAbstractBehaviour_t56____TARGET_OFFSET_2_FieldInfo,
	&VirtualButtonAbstractBehaviour_t56____mName_3_FieldInfo,
	&VirtualButtonAbstractBehaviour_t56____mSensitivity_4_FieldInfo,
	&VirtualButtonAbstractBehaviour_t56____mHasUpdatedPose_5_FieldInfo,
	&VirtualButtonAbstractBehaviour_t56____mPrevTransform_6_FieldInfo,
	&VirtualButtonAbstractBehaviour_t56____mPrevParent_7_FieldInfo,
	&VirtualButtonAbstractBehaviour_t56____mSensitivityDirty_8_FieldInfo,
	&VirtualButtonAbstractBehaviour_t56____mPreviouslyEnabled_9_FieldInfo,
	&VirtualButtonAbstractBehaviour_t56____mPressed_10_FieldInfo,
	&VirtualButtonAbstractBehaviour_t56____mHandlers_11_FieldInfo,
	&VirtualButtonAbstractBehaviour_t56____mLeftTop_12_FieldInfo,
	&VirtualButtonAbstractBehaviour_t56____mRightBottom_13_FieldInfo,
	&VirtualButtonAbstractBehaviour_t56____mUnregisterOnDestroy_14_FieldInfo,
	&VirtualButtonAbstractBehaviour_t56____mVirtualButton_15_FieldInfo,
	NULL
};
static const float VirtualButtonAbstractBehaviour_t56____TARGET_OFFSET_2_DefaultValueData = 0.001f;
static Il2CppFieldDefaultValueEntry VirtualButtonAbstractBehaviour_t56____TARGET_OFFSET_2_DefaultValue = 
{
	&VirtualButtonAbstractBehaviour_t56____TARGET_OFFSET_2_FieldInfo/* field */
	, { (char*)&VirtualButtonAbstractBehaviour_t56____TARGET_OFFSET_2_DefaultValueData, &Single_t105_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* VirtualButtonAbstractBehaviour_t56_FieldDefaultValues[] = 
{
	&VirtualButtonAbstractBehaviour_t56____TARGET_OFFSET_2_DefaultValue,
	NULL
};
static PropertyInfo VirtualButtonAbstractBehaviour_t56____VirtualButtonName_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, "VirtualButtonName"/* name */
	, &VirtualButtonAbstractBehaviour_get_VirtualButtonName_m616_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t56____Pressed_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, "Pressed"/* name */
	, &VirtualButtonAbstractBehaviour_get_Pressed_m4215_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t56____HasUpdatedPose_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, "HasUpdatedPose"/* name */
	, &VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m628_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t56____UnregisterOnDestroy_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, "UnregisterOnDestroy"/* name */
	, &VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m626_MethodInfo/* get */
	, &VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m627_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t56____VirtualButton_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, "VirtualButton"/* name */
	, &VirtualButtonAbstractBehaviour_get_VirtualButton_m4216_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t56____Vuforia_IEditorVirtualButtonBehaviour_SensitivitySetting_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorVirtualButtonBehaviour.SensitivitySetting"/* name */
	, &VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m618_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t56____Vuforia_IEditorVirtualButtonBehaviour_PreviousTransform_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorVirtualButtonBehaviour.PreviousTransform"/* name */
	, &VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m620_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo VirtualButtonAbstractBehaviour_t56____Vuforia_IEditorVirtualButtonBehaviour_PreviousParent_PropertyInfo = 
{
	&VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorVirtualButtonBehaviour.PreviousParent"/* name */
	, &VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m622_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* VirtualButtonAbstractBehaviour_t56_PropertyInfos[] =
{
	&VirtualButtonAbstractBehaviour_t56____VirtualButtonName_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t56____Pressed_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t56____HasUpdatedPose_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t56____UnregisterOnDestroy_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t56____VirtualButton_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t56____Vuforia_IEditorVirtualButtonBehaviour_SensitivitySetting_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t56____Vuforia_IEditorVirtualButtonBehaviour_PreviousTransform_PropertyInfo,
	&VirtualButtonAbstractBehaviour_t56____Vuforia_IEditorVirtualButtonBehaviour_PreviousParent_PropertyInfo,
	NULL
};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.VirtualButtonAbstractBehaviour::get_VirtualButtonName()
MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButtonName_m616_MethodInfo = 
{
	"get_VirtualButtonName"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_VirtualButtonName_m616/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2430/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_Pressed()
MethodInfo VirtualButtonAbstractBehaviour_get_Pressed_m4215_MethodInfo = 
{
	"get_Pressed"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_Pressed_m4215/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2431/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_HasUpdatedPose()
MethodInfo VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m628_MethodInfo = 
{
	"get_HasUpdatedPose"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m628/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2432/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::get_UnregisterOnDestroy()
MethodInfo VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m626_MethodInfo = 
{
	"get_UnregisterOnDestroy"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m626/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2433/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m627_ParameterInfos[] = 
{
	{"value", 0, 134220034, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_SByte_t129 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::set_UnregisterOnDestroy(System.Boolean)
MethodInfo VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m627_MethodInfo = 
{
	"set_UnregisterOnDestroy"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m627/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_SByte_t129/* invoker_method */
	, VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m627_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2434/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType VirtualButton_t595_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.VirtualButton Vuforia.VirtualButtonAbstractBehaviour::get_VirtualButton()
MethodInfo VirtualButtonAbstractBehaviour_get_VirtualButton_m4216_MethodInfo = 
{
	"get_VirtualButton"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_get_VirtualButton_m4216/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &VirtualButton_t595_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2435/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::.ctor()
MethodInfo VirtualButtonAbstractBehaviour__ctor_m615_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour__ctor_m615/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2436/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IVirtualButtonEventHandler_t771_0_0_0;
extern Il2CppType IVirtualButtonEventHandler_t771_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_RegisterEventHandler_m4217_ParameterInfos[] = 
{
	{"eventHandler", 0, 134220035, &EmptyCustomAttributesCache, &IVirtualButtonEventHandler_t771_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::RegisterEventHandler(Vuforia.IVirtualButtonEventHandler)
MethodInfo VirtualButtonAbstractBehaviour_RegisterEventHandler_m4217_MethodInfo = 
{
	"RegisterEventHandler"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_RegisterEventHandler_m4217/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_RegisterEventHandler_m4217_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2437/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType IVirtualButtonEventHandler_t771_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4218_ParameterInfos[] = 
{
	{"eventHandler", 0, 134220036, &EmptyCustomAttributesCache, &IVirtualButtonEventHandler_t771_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UnregisterEventHandler(Vuforia.IVirtualButtonEventHandler)
MethodInfo VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4218_MethodInfo = 
{
	"UnregisterEventHandler"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4218/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4218_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2438/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t9_1_0_2;
extern Il2CppType Vector2_t9_1_0_2;
static ParameterInfo VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_CalculateButtonArea_m4219_ParameterInfos[] = 
{
	{"topLeft", 0, 134220037, &EmptyCustomAttributesCache, &Vector2_t9_1_0_2},
	{"bottomRight", 1, 134220038, &EmptyCustomAttributesCache, &Vector2_t9_1_0_2},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Vector2U26_t896_Vector2U26_t896 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::CalculateButtonArea(UnityEngine.Vector2&,UnityEngine.Vector2&)
MethodInfo VirtualButtonAbstractBehaviour_CalculateButtonArea_m4219_MethodInfo = 
{
	"CalculateButtonArea"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_CalculateButtonArea_m4219/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Vector2U26_t896_Vector2U26_t896/* invoker_method */
	, VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_CalculateButtonArea_m4219_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2439/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateAreaRectangle()
MethodInfo VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4220_MethodInfo = 
{
	"UpdateAreaRectangle"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4220/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2440/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateSensitivity()
MethodInfo VirtualButtonAbstractBehaviour_UpdateSensitivity_m4221_MethodInfo = 
{
	"UpdateSensitivity"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UpdateSensitivity_m4221/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2441/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdateEnabled()
MethodInfo VirtualButtonAbstractBehaviour_UpdateEnabled_m4222_MethodInfo = 
{
	"UpdateEnabled"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UpdateEnabled_m4222/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2442/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::UpdatePose()
MethodInfo VirtualButtonAbstractBehaviour_UpdatePose_m629_MethodInfo = 
{
	"UpdatePose"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_UpdatePose_m629/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2443/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4223_ParameterInfos[] = 
{
	{"pressed", 0, 134220039, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_SByte_t129 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnTrackerUpdated(System.Boolean)
MethodInfo VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4223_MethodInfo = 
{
	"OnTrackerUpdated"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4223/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_SByte_t129/* invoker_method */
	, VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4223_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2444/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType ImageTargetAbstractBehaviour_t50_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.ImageTargetAbstractBehaviour Vuforia.VirtualButtonAbstractBehaviour::GetImageTargetBehaviour()
MethodInfo VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4224_MethodInfo = 
{
	"GetImageTargetBehaviour"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4224/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetAbstractBehaviour_t50_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2445/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m617_ParameterInfos[] = 
{
	{"virtualButtonName", 0, 134220040, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetVirtualButtonName(System.String)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m617_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetVirtualButtonName"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m617/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m617_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2446/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Sensitivity_t745_0_0_0;
extern void* RuntimeInvoker_Sensitivity_t745 (MethodInfo* method, void* obj, void** args);
// Vuforia.VirtualButton/Sensitivity Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_SensitivitySetting()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m618_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_SensitivitySetting"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m618/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Sensitivity_t745_0_0_0/* return_type */
	, RuntimeInvoker_Sensitivity_t745/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2447/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Sensitivity_t745_0_0_0;
extern Il2CppType Sensitivity_t745_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m619_ParameterInfos[] = 
{
	{"sensibility", 0, 134220041, &EmptyCustomAttributesCache, &Sensitivity_t745_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetSensitivitySetting(Vuforia.VirtualButton/Sensitivity)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m619_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetSensitivitySetting"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m619/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m619_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2448/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Matrix4x4_t176_0_0_0;
extern void* RuntimeInvoker_Matrix4x4_t176 (MethodInfo* method, void* obj, void** args);
// UnityEngine.Matrix4x4 Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_PreviousTransform()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m620_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_PreviousTransform"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m620/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Matrix4x4_t176_0_0_0/* return_type */
	, RuntimeInvoker_Matrix4x4_t176/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2449/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Matrix4x4_t176_0_0_0;
extern Il2CppType Matrix4x4_t176_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m621_ParameterInfos[] = 
{
	{"transformMatrix", 0, 134220042, &EmptyCustomAttributesCache, &Matrix4x4_t176_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Matrix4x4_t176 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPreviousTransform(UnityEngine.Matrix4x4)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m621_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetPreviousTransform"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m621/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Matrix4x4_t176/* invoker_method */
	, VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m621_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 9/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2450/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_PreviousParent()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m622_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_PreviousParent"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m622/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t2_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2451/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
extern Il2CppType GameObject_t2_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m623_ParameterInfos[] = 
{
	{"parent", 0, 134220043, &EmptyCustomAttributesCache, &GameObject_t2_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPreviousParent(UnityEngine.GameObject)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m623_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetPreviousParent"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m623/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m623_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2452/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType VirtualButton_t595_0_0_0;
extern Il2CppType VirtualButton_t595_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m624_ParameterInfos[] = 
{
	{"virtualButton", 0, 134220044, &EmptyCustomAttributesCache, &VirtualButton_t595_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.InitializeVirtualButton(Vuforia.VirtualButton)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m624_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.InitializeVirtualButton"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m624/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m624_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2453/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType Vector2_t9_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m625_ParameterInfos[] = 
{
	{"topLeft", 0, 134220045, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
	{"bottomRight", 1, 134220046, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Vector2_t9_Vector2_t9 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.SetPosAndScaleFromButtonArea(UnityEngine.Vector2,UnityEngine.Vector2)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m625_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.SetPosAndScaleFromButtonArea"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m625/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Vector2_t9_Vector2_t9/* invoker_method */
	, VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m625_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2454/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Renderer_t7_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Renderer Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.GetRenderer()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m634_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.GetRenderer"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m634/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Renderer_t7_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 22/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2455/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::LateUpdate()
MethodInfo VirtualButtonAbstractBehaviour_LateUpdate_m4225_MethodInfo = 
{
	"LateUpdate"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_LateUpdate_m4225/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2456/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnDisable()
MethodInfo VirtualButtonAbstractBehaviour_OnDisable_m4226_MethodInfo = 
{
	"OnDisable"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_OnDisable_m4226/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2457/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::OnDestroy()
MethodInfo VirtualButtonAbstractBehaviour_OnDestroy_m4227_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_OnDestroy_m4227/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2458/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType Vector2_t9_0_0_0;
extern Il2CppType Single_t105_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Equals_m4228_ParameterInfos[] = 
{
	{"vec1", 0, 134220047, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
	{"vec2", 1, 134220048, &EmptyCustomAttributesCache, &Vector2_t9_0_0_0},
	{"threshold", 2, 134220049, &EmptyCustomAttributesCache, &Single_t105_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Vector2_t9_Vector2_t9_Single_t105 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Equals(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
MethodInfo VirtualButtonAbstractBehaviour_Equals_m4228_MethodInfo = 
{
	"Equals"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Equals_m4228/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Vector2_t9_Vector2_t9_Single_t105/* invoker_method */
	, VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Equals_m4228_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 145/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2459/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_enabled()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m630_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_enabled"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m630/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2460/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m631_ParameterInfos[] = 
{
	{"", 0, 134217728, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_SByte_t129 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.set_enabled(System.Boolean)
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m631_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.set_enabled"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m631/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_SByte_t129/* invoker_method */
	, VirtualButtonAbstractBehaviour_t56_VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m631_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2461/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Transform_t10_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_transform()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m632_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_transform"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m632/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t10_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2462/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject Vuforia.VirtualButtonAbstractBehaviour::Vuforia.IEditorVirtualButtonBehaviour.get_gameObject()
MethodInfo VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m633_MethodInfo = 
{
	"Vuforia.IEditorVirtualButtonBehaviour.get_gameObject"/* name */
	, (methodPointerType)&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m633/* method */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t2_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 21/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2463/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* VirtualButtonAbstractBehaviour_t56_MethodInfos[] =
{
	&VirtualButtonAbstractBehaviour_get_VirtualButtonName_m616_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_Pressed_m4215_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m628_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m626_MethodInfo,
	&VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m627_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_VirtualButton_m4216_MethodInfo,
	&VirtualButtonAbstractBehaviour__ctor_m615_MethodInfo,
	&VirtualButtonAbstractBehaviour_RegisterEventHandler_m4217_MethodInfo,
	&VirtualButtonAbstractBehaviour_UnregisterEventHandler_m4218_MethodInfo,
	&VirtualButtonAbstractBehaviour_CalculateButtonArea_m4219_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdateAreaRectangle_m4220_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdateSensitivity_m4221_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdateEnabled_m4222_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdatePose_m629_MethodInfo,
	&VirtualButtonAbstractBehaviour_OnTrackerUpdated_m4223_MethodInfo,
	&VirtualButtonAbstractBehaviour_GetImageTargetBehaviour_m4224_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m617_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m618_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m619_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m620_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m621_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m622_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m623_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m624_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m625_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m634_MethodInfo,
	&VirtualButtonAbstractBehaviour_LateUpdate_m4225_MethodInfo,
	&VirtualButtonAbstractBehaviour_OnDisable_m4226_MethodInfo,
	&VirtualButtonAbstractBehaviour_OnDestroy_m4227_MethodInfo,
	&VirtualButtonAbstractBehaviour_Equals_m4228_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m630_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m631_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m632_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m633_MethodInfo,
	NULL
};
static MethodInfo* VirtualButtonAbstractBehaviour_t56_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_VirtualButtonName_m616_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetVirtualButtonName_m617_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_SensitivitySetting_m618_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetSensitivitySetting_m619_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousTransform_m620_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousTransform_m621_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_PreviousParent_m622_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPreviousParent_m623_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_InitializeVirtualButton_m624_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_SetPosAndScaleFromButtonArea_m625_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_UnregisterOnDestroy_m626_MethodInfo,
	&VirtualButtonAbstractBehaviour_set_UnregisterOnDestroy_m627_MethodInfo,
	&VirtualButtonAbstractBehaviour_get_HasUpdatedPose_m628_MethodInfo,
	&VirtualButtonAbstractBehaviour_UpdatePose_m629_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_enabled_m630_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_set_enabled_m631_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_transform_m632_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_get_gameObject_m633_MethodInfo,
	&VirtualButtonAbstractBehaviour_Vuforia_IEditorVirtualButtonBehaviour_GetRenderer_m634_MethodInfo,
};
extern TypeInfo IEditorVirtualButtonBehaviour_t170_il2cpp_TypeInfo;
static TypeInfo* VirtualButtonAbstractBehaviour_t56_InterfacesTypeInfos[] = 
{
	&IEditorVirtualButtonBehaviour_t170_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair VirtualButtonAbstractBehaviour_t56_InterfacesOffsets[] = 
{
	{ &IEditorVirtualButtonBehaviour_t170_il2cpp_TypeInfo, 4},
};
void VirtualButtonAbstractBehaviour_t56_CustomAttributesCacheGenerator_mName(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void VirtualButtonAbstractBehaviour_t56_CustomAttributesCacheGenerator_mSensitivity(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void VirtualButtonAbstractBehaviour_t56_CustomAttributesCacheGenerator_mHasUpdatedPose(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void VirtualButtonAbstractBehaviour_t56_CustomAttributesCacheGenerator_mPrevTransform(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void VirtualButtonAbstractBehaviour_t56_CustomAttributesCacheGenerator_mPrevParent(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mName = {
2,
NULL,
&VirtualButtonAbstractBehaviour_t56_CustomAttributesCacheGenerator_mName
};
CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mSensitivity = {
2,
NULL,
&VirtualButtonAbstractBehaviour_t56_CustomAttributesCacheGenerator_mSensitivity
};
CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mHasUpdatedPose = {
2,
NULL,
&VirtualButtonAbstractBehaviour_t56_CustomAttributesCacheGenerator_mHasUpdatedPose
};
CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mPrevTransform = {
2,
NULL,
&VirtualButtonAbstractBehaviour_t56_CustomAttributesCacheGenerator_mPrevTransform
};
CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mPrevParent = {
2,
NULL,
&VirtualButtonAbstractBehaviour_t56_CustomAttributesCacheGenerator_mPrevParent
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType VirtualButtonAbstractBehaviour_t56_0_0_0;
extern Il2CppType VirtualButtonAbstractBehaviour_t56_1_0_0;
struct VirtualButtonAbstractBehaviour_t56;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mName;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mSensitivity;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mHasUpdatedPose;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mPrevTransform;
extern CustomAttributesCache VirtualButtonAbstractBehaviour_t56__CustomAttributeCache_mPrevParent;
TypeInfo VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "VirtualButtonAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, VirtualButtonAbstractBehaviour_t56_MethodInfos/* methods */
	, VirtualButtonAbstractBehaviour_t56_PropertyInfos/* properties */
	, VirtualButtonAbstractBehaviour_t56_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* element_class */
	, VirtualButtonAbstractBehaviour_t56_InterfacesTypeInfos/* implemented_interfaces */
	, VirtualButtonAbstractBehaviour_t56_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &VirtualButtonAbstractBehaviour_t56_il2cpp_TypeInfo/* cast_class */
	, &VirtualButtonAbstractBehaviour_t56_0_0_0/* byval_arg */
	, &VirtualButtonAbstractBehaviour_t56_1_0_0/* this_arg */
	, VirtualButtonAbstractBehaviour_t56_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, VirtualButtonAbstractBehaviour_t56_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (VirtualButtonAbstractBehaviour_t56)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 34/* method_count */
	, 8/* property_count */
	, 14/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 1/* interfaces_count */
	, 1/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// Vuforia.WebCamImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamImpl.h"
// UnityEngine.WebCamDevice
#include "UnityEngine_UnityEngine_WebCamDevice.h"
extern TypeInfo WebCamImpl_t604_il2cpp_TypeInfo;
extern TypeInfo Exception_t152_il2cpp_TypeInfo;
// Vuforia.WebCamImpl
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamImplMethodDeclarations.h"
// UnityEngine.WebCamTexture
#include "UnityEngine_UnityEngine_WebCamTextureMethodDeclarations.h"
extern MethodInfo WebCamImpl_get_IsPlaying_m4055_MethodInfo;
extern MethodInfo WebCamAbstractBehaviour_CheckNativePluginSupport_m4241_MethodInfo;
extern MethodInfo WebCamTexture_get_devices_m5308_MethodInfo;
extern MethodInfo Application_set_runInBackground_m5415_MethodInfo;
extern MethodInfo Component_GetComponentsInChildren_TisCamera_t3_m5416_MethodInfo;
extern MethodInfo WebCamImpl__ctor_m4063_MethodInfo;
extern MethodInfo WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4245_MethodInfo;
extern MethodInfo WebCamImpl_ResetPlaying_m4066_MethodInfo;
extern MethodInfo WebCamImpl_OnDestroy_m4072_MethodInfo;
extern MethodInfo Object_Destroy_m608_MethodInfo;
extern MethodInfo WebCamImpl_Update_m4073_MethodInfo;
struct Component_t100;
struct Component_t100;
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
 ObjectU5BU5D_t115* Component_GetComponentsInChildren_TisObject_t_m4926_gshared (Component_t100 * __this, MethodInfo* method);
#define Component_GetComponentsInChildren_TisObject_t_m4926(__this, method) (ObjectU5BU5D_t115*)Component_GetComponentsInChildren_TisObject_t_m4926_gshared((Component_t100 *)__this, method)
// Declaration !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Camera>()
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Camera>()
#define Component_GetComponentsInChildren_TisCamera_t3_m5416(__this, method) (CameraU5BU5D_t172*)Component_GetComponentsInChildren_TisObject_t_m4926_gshared((Component_t100 *)__this, method)


// System.Boolean Vuforia.WebCamAbstractBehaviour::get_PlayModeRenderVideo()
extern MethodInfo WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4229_MethodInfo;
 bool WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4229 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mPlayModeRenderVideo_3);
		return L_0;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::set_PlayModeRenderVideo(System.Boolean)
extern MethodInfo WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4230_MethodInfo;
 void WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4230 (WebCamAbstractBehaviour_t89 * __this, bool ___value, MethodInfo* method){
	{
		__this->___mPlayModeRenderVideo_3 = ___value;
		return;
	}
}
// System.String Vuforia.WebCamAbstractBehaviour::get_DeviceName()
extern MethodInfo WebCamAbstractBehaviour_get_DeviceName_m4231_MethodInfo;
 String_t* WebCamAbstractBehaviour_get_DeviceName_m4231 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mDeviceNameSetInEditor_4);
		return L_0;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::set_DeviceName(System.String)
extern MethodInfo WebCamAbstractBehaviour_set_DeviceName_m4232_MethodInfo;
 void WebCamAbstractBehaviour_set_DeviceName_m4232 (WebCamAbstractBehaviour_t89 * __this, String_t* ___value, MethodInfo* method){
	{
		__this->___mDeviceNameSetInEditor_4 = ___value;
		return;
	}
}
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_FlipHorizontally()
extern MethodInfo WebCamAbstractBehaviour_get_FlipHorizontally_m4233_MethodInfo;
 bool WebCamAbstractBehaviour_get_FlipHorizontally_m4233 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mFlipHorizontally_5);
		return L_0;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::set_FlipHorizontally(System.Boolean)
extern MethodInfo WebCamAbstractBehaviour_set_FlipHorizontally_m4234_MethodInfo;
 void WebCamAbstractBehaviour_set_FlipHorizontally_m4234 (WebCamAbstractBehaviour_t89 * __this, bool ___value, MethodInfo* method){
	{
		__this->___mFlipHorizontally_5 = ___value;
		return;
	}
}
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_TurnOffWebCam()
extern MethodInfo WebCamAbstractBehaviour_get_TurnOffWebCam_m4235_MethodInfo;
 bool WebCamAbstractBehaviour_get_TurnOffWebCam_m4235 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mTurnOffWebCam_6);
		return L_0;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::set_TurnOffWebCam(System.Boolean)
extern MethodInfo WebCamAbstractBehaviour_set_TurnOffWebCam_m4236_MethodInfo;
 void WebCamAbstractBehaviour_set_TurnOffWebCam_m4236 (WebCamAbstractBehaviour_t89 * __this, bool ___value, MethodInfo* method){
	{
		__this->___mTurnOffWebCam_6 = ___value;
		return;
	}
}
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_IsPlaying()
extern MethodInfo WebCamAbstractBehaviour_get_IsPlaying_m4237_MethodInfo;
 bool WebCamAbstractBehaviour_get_IsPlaying_m4237 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method){
	{
		WebCamImpl_t604 * L_0 = (__this->___mWebCamImpl_7);
		NullCheck(L_0);
		bool L_1 = WebCamImpl_get_IsPlaying_m4055(L_0, /*hidden argument*/&WebCamImpl_get_IsPlaying_m4055_MethodInfo);
		return L_1;
	}
}
// System.Boolean Vuforia.WebCamAbstractBehaviour::IsWebCamUsed()
 bool WebCamAbstractBehaviour_IsWebCamUsed_m4238 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method){
	{
		bool L_0 = (__this->___mTurnOffWebCam_6);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		bool L_1 = WebCamAbstractBehaviour_CheckNativePluginSupport_m4241(__this, /*hidden argument*/&WebCamAbstractBehaviour_CheckNativePluginSupport_m4241_MethodInfo);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		WebCamDeviceU5BU5D_t886* L_2 = WebCamTexture_get_devices_m5308(NULL /*static, unused*/, /*hidden argument*/&WebCamTexture_get_devices_m5308_MethodInfo);
		NullCheck(L_2);
		return ((((int32_t)(((int32_t)(((Array_t *)L_2)->max_length)))) > ((int32_t)0))? 1 : 0);
	}

IL_001b:
	{
		return 0;
	}
}
// Vuforia.WebCamImpl Vuforia.WebCamAbstractBehaviour::get_ImplementationClass()
extern MethodInfo WebCamAbstractBehaviour_get_ImplementationClass_m4239_MethodInfo;
 WebCamImpl_t604 * WebCamAbstractBehaviour_get_ImplementationClass_m4239 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method){
	{
		WebCamImpl_t604 * L_0 = (__this->___mWebCamImpl_7);
		return L_0;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::InitCamera()
extern MethodInfo WebCamAbstractBehaviour_InitCamera_m4240_MethodInfo;
 void WebCamAbstractBehaviour_InitCamera_m4240 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method){
	CameraU5BU5D_t172* V_0 = {0};
	{
		WebCamImpl_t604 * L_0 = (__this->___mWebCamImpl_7);
		if (L_0)
		{
			goto IL_0033;
		}
	}
	{
		Application_set_runInBackground_m5415(NULL /*static, unused*/, 1, /*hidden argument*/&Application_set_runInBackground_m5415_MethodInfo);
		CameraU5BU5D_t172* L_1 = Component_GetComponentsInChildren_TisCamera_t3_m5416(__this, /*hidden argument*/&Component_GetComponentsInChildren_TisCamera_t3_m5416_MethodInfo);
		V_0 = L_1;
		int32_t L_2 = (__this->___RenderTextureLayer_2);
		String_t* L_3 = (__this->___mDeviceNameSetInEditor_4);
		bool L_4 = (__this->___mFlipHorizontally_5);
		WebCamImpl_t604 * L_5 = (WebCamImpl_t604 *)il2cpp_codegen_object_new (InitializedTypeInfo(&WebCamImpl_t604_il2cpp_TypeInfo));
		WebCamImpl__ctor_m4063(L_5, V_0, L_2, L_3, L_4, /*hidden argument*/&WebCamImpl__ctor_m4063_MethodInfo);
		__this->___mWebCamImpl_7 = L_5;
	}

IL_0033:
	{
		return;
	}
}
// System.Boolean Vuforia.WebCamAbstractBehaviour::CheckNativePluginSupport()
 bool WebCamAbstractBehaviour_CheckNativePluginSupport_m4241 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	Exception_t152 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t152 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m540(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsPlayMode_m540_MethodInfo);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		V_0 = 0;
	}

IL_0009:
	try
	{ // begin try (depth: 1)
		int32_t L_1 = WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4245(NULL /*static, unused*/, /*hidden argument*/&WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4245_MethodInfo);
		V_0 = L_1;
		// IL_000f: leave.s IL_0016
		goto IL_0016;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t152 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (&Exception_t152_il2cpp_TypeInfo, e.ex->object.klass))
			goto IL_0011;
		throw e;
	}

IL_0011:
	{ // begin catch(System.Exception)
		V_0 = 0;
		// IL_0014: leave.s IL_0016
		goto IL_0016;
	} // end catch (depth: 1)

IL_0016:
	{
		return ((((int32_t)V_0) == ((int32_t)1))? 1 : 0);
	}

IL_001b:
	{
		return 1;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::OnLevelWasLoaded()
extern MethodInfo WebCamAbstractBehaviour_OnLevelWasLoaded_m4242_MethodInfo;
 void WebCamAbstractBehaviour_OnLevelWasLoaded_m4242 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m540(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsPlayMode_m540_MethodInfo);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		WebCamImpl_t604 * L_1 = (__this->___mWebCamImpl_7);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		WebCamImpl_t604 * L_2 = (__this->___mWebCamImpl_7);
		NullCheck(L_2);
		WebCamImpl_ResetPlaying_m4066(L_2, /*hidden argument*/&WebCamImpl_ResetPlaying_m4066_MethodInfo);
	}

IL_001a:
	{
		return;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::OnDestroy()
extern MethodInfo WebCamAbstractBehaviour_OnDestroy_m4243_MethodInfo;
 void WebCamAbstractBehaviour_OnDestroy_m4243 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m540(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsPlayMode_m540_MethodInfo);
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		WebCamImpl_t604 * L_1 = (__this->___mWebCamImpl_7);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		WebCamImpl_t604 * L_2 = (__this->___mWebCamImpl_7);
		NullCheck(L_2);
		WebCamImpl_OnDestroy_m4072(L_2, /*hidden argument*/&WebCamImpl_OnDestroy_m4072_MethodInfo);
		Camera_t3 * L_3 = (__this->___mBackgroundCameraInstance_8);
		Object_Destroy_m608(NULL /*static, unused*/, L_3, /*hidden argument*/&Object_Destroy_m608_MethodInfo);
	}

IL_0025:
	{
		return;
	}
}
// System.Void Vuforia.WebCamAbstractBehaviour::Update()
extern MethodInfo WebCamAbstractBehaviour_Update_m4244_MethodInfo;
 void WebCamAbstractBehaviour_Update_m4244 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method){
	{
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&QCARRuntimeUtilities_t157_il2cpp_TypeInfo));
		bool L_0 = QCARRuntimeUtilities_IsPlayMode_m540(NULL /*static, unused*/, /*hidden argument*/&QCARRuntimeUtilities_IsPlayMode_m540_MethodInfo);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		WebCamImpl_t604 * L_1 = (__this->___mWebCamImpl_7);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		WebCamImpl_t604 * L_2 = (__this->___mWebCamImpl_7);
		NullCheck(L_2);
		WebCamImpl_Update_m4073(L_2, /*hidden argument*/&WebCamImpl_Update_m4073_MethodInfo);
	}

IL_001a:
	{
		return;
	}
}
// System.Int32 Vuforia.WebCamAbstractBehaviour::qcarCheckNativePluginSupport()
 int32_t WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4245 (Object_t * __this/* static, unused */, MethodInfo* method){
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();
	static PInvokeFunc _il2cpp_pinvoke_func;
	if (!_il2cpp_pinvoke_func)
	{
		int parameterSize = 0;
		_il2cpp_pinvoke_func = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>("QCARWrapper", "qcarCheckNativePluginSupport", IL2CPP_CALL_DEFAULT, CHARSET_UNICODE, parameterSize, false);

		if (_il2cpp_pinvoke_func == NULL)
		{
			il2cpp_codegen_raise_exception(il2cpp_codegen_get_not_supported_exception("Unable to find method for p/invoke: 'qcarCheckNativePluginSupport'"));
		}
	}

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func();

	return _return_value;
}
// System.Void Vuforia.WebCamAbstractBehaviour::.ctor()
extern MethodInfo WebCamAbstractBehaviour__ctor_m635_MethodInfo;
 void WebCamAbstractBehaviour__ctor_m635 (WebCamAbstractBehaviour_t89 * __this, MethodInfo* method){
	{
		__this->___mPlayModeRenderVideo_3 = 1;
		MonoBehaviour__ctor_m214(__this, /*hidden argument*/&MonoBehaviour__ctor_m214_MethodInfo);
		return;
	}
}
// Metadata Definition Vuforia.WebCamAbstractBehaviour
extern Il2CppType Int32_t93_0_0_6;
FieldInfo WebCamAbstractBehaviour_t89____RenderTextureLayer_2_FieldInfo = 
{
	"RenderTextureLayer"/* name */
	, &Int32_t93_0_0_6/* type */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t89, ___RenderTextureLayer_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
extern CustomAttributesCache WebCamAbstractBehaviour_t89__CustomAttributeCache_mPlayModeRenderVideo;
FieldInfo WebCamAbstractBehaviour_t89____mPlayModeRenderVideo_3_FieldInfo = 
{
	"mPlayModeRenderVideo"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t89, ___mPlayModeRenderVideo_3)/* data */
	, &WebCamAbstractBehaviour_t89__CustomAttributeCache_mPlayModeRenderVideo/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache WebCamAbstractBehaviour_t89__CustomAttributeCache_mDeviceNameSetInEditor;
FieldInfo WebCamAbstractBehaviour_t89____mDeviceNameSetInEditor_4_FieldInfo = 
{
	"mDeviceNameSetInEditor"/* name */
	, &String_t_0_0_1/* type */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t89, ___mDeviceNameSetInEditor_4)/* data */
	, &WebCamAbstractBehaviour_t89__CustomAttributeCache_mDeviceNameSetInEditor/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
extern CustomAttributesCache WebCamAbstractBehaviour_t89__CustomAttributeCache_mFlipHorizontally;
FieldInfo WebCamAbstractBehaviour_t89____mFlipHorizontally_5_FieldInfo = 
{
	"mFlipHorizontally"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t89, ___mFlipHorizontally_5)/* data */
	, &WebCamAbstractBehaviour_t89__CustomAttributeCache_mFlipHorizontally/* custom_attributes_cache */

};
extern Il2CppType Boolean_t106_0_0_1;
extern CustomAttributesCache WebCamAbstractBehaviour_t89__CustomAttributeCache_mTurnOffWebCam;
FieldInfo WebCamAbstractBehaviour_t89____mTurnOffWebCam_6_FieldInfo = 
{
	"mTurnOffWebCam"/* name */
	, &Boolean_t106_0_0_1/* type */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t89, ___mTurnOffWebCam_6)/* data */
	, &WebCamAbstractBehaviour_t89__CustomAttributeCache_mTurnOffWebCam/* custom_attributes_cache */

};
extern Il2CppType WebCamImpl_t604_0_0_1;
FieldInfo WebCamAbstractBehaviour_t89____mWebCamImpl_7_FieldInfo = 
{
	"mWebCamImpl"/* name */
	, &WebCamImpl_t604_0_0_1/* type */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t89, ___mWebCamImpl_7)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Camera_t3_0_0_1;
FieldInfo WebCamAbstractBehaviour_t89____mBackgroundCameraInstance_8_FieldInfo = 
{
	"mBackgroundCameraInstance"/* name */
	, &Camera_t3_0_0_1/* type */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, offsetof(WebCamAbstractBehaviour_t89, ___mBackgroundCameraInstance_8)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* WebCamAbstractBehaviour_t89_FieldInfos[] =
{
	&WebCamAbstractBehaviour_t89____RenderTextureLayer_2_FieldInfo,
	&WebCamAbstractBehaviour_t89____mPlayModeRenderVideo_3_FieldInfo,
	&WebCamAbstractBehaviour_t89____mDeviceNameSetInEditor_4_FieldInfo,
	&WebCamAbstractBehaviour_t89____mFlipHorizontally_5_FieldInfo,
	&WebCamAbstractBehaviour_t89____mTurnOffWebCam_6_FieldInfo,
	&WebCamAbstractBehaviour_t89____mWebCamImpl_7_FieldInfo,
	&WebCamAbstractBehaviour_t89____mBackgroundCameraInstance_8_FieldInfo,
	NULL
};
static PropertyInfo WebCamAbstractBehaviour_t89____PlayModeRenderVideo_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, "PlayModeRenderVideo"/* name */
	, &WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4229_MethodInfo/* get */
	, &WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4230_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WebCamAbstractBehaviour_t89____DeviceName_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, "DeviceName"/* name */
	, &WebCamAbstractBehaviour_get_DeviceName_m4231_MethodInfo/* get */
	, &WebCamAbstractBehaviour_set_DeviceName_m4232_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WebCamAbstractBehaviour_t89____FlipHorizontally_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, "FlipHorizontally"/* name */
	, &WebCamAbstractBehaviour_get_FlipHorizontally_m4233_MethodInfo/* get */
	, &WebCamAbstractBehaviour_set_FlipHorizontally_m4234_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WebCamAbstractBehaviour_t89____TurnOffWebCam_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, "TurnOffWebCam"/* name */
	, &WebCamAbstractBehaviour_get_TurnOffWebCam_m4235_MethodInfo/* get */
	, &WebCamAbstractBehaviour_set_TurnOffWebCam_m4236_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WebCamAbstractBehaviour_t89____IsPlaying_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, "IsPlaying"/* name */
	, &WebCamAbstractBehaviour_get_IsPlaying_m4237_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WebCamAbstractBehaviour_t89____ImplementationClass_PropertyInfo = 
{
	&WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* parent */
	, "ImplementationClass"/* name */
	, &WebCamAbstractBehaviour_get_ImplementationClass_m4239_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* WebCamAbstractBehaviour_t89_PropertyInfos[] =
{
	&WebCamAbstractBehaviour_t89____PlayModeRenderVideo_PropertyInfo,
	&WebCamAbstractBehaviour_t89____DeviceName_PropertyInfo,
	&WebCamAbstractBehaviour_t89____FlipHorizontally_PropertyInfo,
	&WebCamAbstractBehaviour_t89____TurnOffWebCam_PropertyInfo,
	&WebCamAbstractBehaviour_t89____IsPlaying_PropertyInfo,
	&WebCamAbstractBehaviour_t89____ImplementationClass_PropertyInfo,
	NULL
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_PlayModeRenderVideo()
MethodInfo WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4229_MethodInfo = 
{
	"get_PlayModeRenderVideo"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4229/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2464/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo WebCamAbstractBehaviour_t89_WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4230_ParameterInfos[] = 
{
	{"value", 0, 134220050, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_SByte_t129 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::set_PlayModeRenderVideo(System.Boolean)
MethodInfo WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4230_MethodInfo = 
{
	"set_PlayModeRenderVideo"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4230/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_SByte_t129/* invoker_method */
	, WebCamAbstractBehaviour_t89_WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4230_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2465/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.WebCamAbstractBehaviour::get_DeviceName()
MethodInfo WebCamAbstractBehaviour_get_DeviceName_m4231_MethodInfo = 
{
	"get_DeviceName"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_DeviceName_m4231/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2466/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo WebCamAbstractBehaviour_t89_WebCamAbstractBehaviour_set_DeviceName_m4232_ParameterInfos[] = 
{
	{"value", 0, 134220051, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::set_DeviceName(System.String)
MethodInfo WebCamAbstractBehaviour_set_DeviceName_m4232_MethodInfo = 
{
	"set_DeviceName"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_set_DeviceName_m4232/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, WebCamAbstractBehaviour_t89_WebCamAbstractBehaviour_set_DeviceName_m4232_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2467/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_FlipHorizontally()
MethodInfo WebCamAbstractBehaviour_get_FlipHorizontally_m4233_MethodInfo = 
{
	"get_FlipHorizontally"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_FlipHorizontally_m4233/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2468/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo WebCamAbstractBehaviour_t89_WebCamAbstractBehaviour_set_FlipHorizontally_m4234_ParameterInfos[] = 
{
	{"value", 0, 134220052, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_SByte_t129 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::set_FlipHorizontally(System.Boolean)
MethodInfo WebCamAbstractBehaviour_set_FlipHorizontally_m4234_MethodInfo = 
{
	"set_FlipHorizontally"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_set_FlipHorizontally_m4234/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_SByte_t129/* invoker_method */
	, WebCamAbstractBehaviour_t89_WebCamAbstractBehaviour_set_FlipHorizontally_m4234_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2469/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_TurnOffWebCam()
MethodInfo WebCamAbstractBehaviour_get_TurnOffWebCam_m4235_MethodInfo = 
{
	"get_TurnOffWebCam"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_TurnOffWebCam_m4235/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2470/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo WebCamAbstractBehaviour_t89_WebCamAbstractBehaviour_set_TurnOffWebCam_m4236_ParameterInfos[] = 
{
	{"value", 0, 134220053, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_SByte_t129 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::set_TurnOffWebCam(System.Boolean)
MethodInfo WebCamAbstractBehaviour_set_TurnOffWebCam_m4236_MethodInfo = 
{
	"set_TurnOffWebCam"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_set_TurnOffWebCam_m4236/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_SByte_t129/* invoker_method */
	, WebCamAbstractBehaviour_t89_WebCamAbstractBehaviour_set_TurnOffWebCam_m4236_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2471/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::get_IsPlaying()
MethodInfo WebCamAbstractBehaviour_get_IsPlaying_m4237_MethodInfo = 
{
	"get_IsPlaying"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_IsPlaying_m4237/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2472/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::IsWebCamUsed()
MethodInfo WebCamAbstractBehaviour_IsWebCamUsed_m4238_MethodInfo = 
{
	"IsWebCamUsed"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_IsWebCamUsed_m4238/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2473/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WebCamImpl_t604_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.WebCamImpl Vuforia.WebCamAbstractBehaviour::get_ImplementationClass()
MethodInfo WebCamAbstractBehaviour_get_ImplementationClass_m4239_MethodInfo = 
{
	"get_ImplementationClass"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_get_ImplementationClass_m4239/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &WebCamImpl_t604_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2179/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2474/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::InitCamera()
MethodInfo WebCamAbstractBehaviour_InitCamera_m4240_MethodInfo = 
{
	"InitCamera"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_InitCamera_m4240/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2475/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WebCamAbstractBehaviour::CheckNativePluginSupport()
MethodInfo WebCamAbstractBehaviour_CheckNativePluginSupport_m4241_MethodInfo = 
{
	"CheckNativePluginSupport"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_CheckNativePluginSupport_m4241/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2476/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::OnLevelWasLoaded()
MethodInfo WebCamAbstractBehaviour_OnLevelWasLoaded_m4242_MethodInfo = 
{
	"OnLevelWasLoaded"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_OnLevelWasLoaded_m4242/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2477/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::OnDestroy()
MethodInfo WebCamAbstractBehaviour_OnDestroy_m4243_MethodInfo = 
{
	"OnDestroy"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_OnDestroy_m4243/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2478/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::Update()
MethodInfo WebCamAbstractBehaviour_Update_m4244_MethodInfo = 
{
	"Update"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_Update_m4244/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2479/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Int32 Vuforia.WebCamAbstractBehaviour::qcarCheckNativePluginSupport()
MethodInfo WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4245_MethodInfo = 
{
	"qcarCheckNativePluginSupport"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4245/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 8337/* flags */
	, 128/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2480/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WebCamAbstractBehaviour::.ctor()
MethodInfo WebCamAbstractBehaviour__ctor_m635_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WebCamAbstractBehaviour__ctor_m635/* method */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2481/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* WebCamAbstractBehaviour_t89_MethodInfos[] =
{
	&WebCamAbstractBehaviour_get_PlayModeRenderVideo_m4229_MethodInfo,
	&WebCamAbstractBehaviour_set_PlayModeRenderVideo_m4230_MethodInfo,
	&WebCamAbstractBehaviour_get_DeviceName_m4231_MethodInfo,
	&WebCamAbstractBehaviour_set_DeviceName_m4232_MethodInfo,
	&WebCamAbstractBehaviour_get_FlipHorizontally_m4233_MethodInfo,
	&WebCamAbstractBehaviour_set_FlipHorizontally_m4234_MethodInfo,
	&WebCamAbstractBehaviour_get_TurnOffWebCam_m4235_MethodInfo,
	&WebCamAbstractBehaviour_set_TurnOffWebCam_m4236_MethodInfo,
	&WebCamAbstractBehaviour_get_IsPlaying_m4237_MethodInfo,
	&WebCamAbstractBehaviour_IsWebCamUsed_m4238_MethodInfo,
	&WebCamAbstractBehaviour_get_ImplementationClass_m4239_MethodInfo,
	&WebCamAbstractBehaviour_InitCamera_m4240_MethodInfo,
	&WebCamAbstractBehaviour_CheckNativePluginSupport_m4241_MethodInfo,
	&WebCamAbstractBehaviour_OnLevelWasLoaded_m4242_MethodInfo,
	&WebCamAbstractBehaviour_OnDestroy_m4243_MethodInfo,
	&WebCamAbstractBehaviour_Update_m4244_MethodInfo,
	&WebCamAbstractBehaviour_qcarCheckNativePluginSupport_m4245_MethodInfo,
	&WebCamAbstractBehaviour__ctor_m635_MethodInfo,
	NULL
};
static MethodInfo* WebCamAbstractBehaviour_t89_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
};
void WebCamAbstractBehaviour_t89_CustomAttributesCacheGenerator_mPlayModeRenderVideo(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void WebCamAbstractBehaviour_t89_CustomAttributesCacheGenerator_mDeviceNameSetInEditor(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void WebCamAbstractBehaviour_t89_CustomAttributesCacheGenerator_mFlipHorizontally(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void WebCamAbstractBehaviour_t89_CustomAttributesCacheGenerator_mTurnOffWebCam(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache WebCamAbstractBehaviour_t89__CustomAttributeCache_mPlayModeRenderVideo = {
2,
NULL,
&WebCamAbstractBehaviour_t89_CustomAttributesCacheGenerator_mPlayModeRenderVideo
};
CustomAttributesCache WebCamAbstractBehaviour_t89__CustomAttributeCache_mDeviceNameSetInEditor = {
2,
NULL,
&WebCamAbstractBehaviour_t89_CustomAttributesCacheGenerator_mDeviceNameSetInEditor
};
CustomAttributesCache WebCamAbstractBehaviour_t89__CustomAttributeCache_mFlipHorizontally = {
2,
NULL,
&WebCamAbstractBehaviour_t89_CustomAttributesCacheGenerator_mFlipHorizontally
};
CustomAttributesCache WebCamAbstractBehaviour_t89__CustomAttributeCache_mTurnOffWebCam = {
2,
NULL,
&WebCamAbstractBehaviour_t89_CustomAttributesCacheGenerator_mTurnOffWebCam
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WebCamAbstractBehaviour_t89_1_0_0;
struct WebCamAbstractBehaviour_t89;
extern CustomAttributesCache WebCamAbstractBehaviour_t89__CustomAttributeCache_mPlayModeRenderVideo;
extern CustomAttributesCache WebCamAbstractBehaviour_t89__CustomAttributeCache_mDeviceNameSetInEditor;
extern CustomAttributesCache WebCamAbstractBehaviour_t89__CustomAttributeCache_mFlipHorizontally;
extern CustomAttributesCache WebCamAbstractBehaviour_t89__CustomAttributeCache_mTurnOffWebCam;
TypeInfo WebCamAbstractBehaviour_t89_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WebCamAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WebCamAbstractBehaviour_t89_MethodInfos/* methods */
	, WebCamAbstractBehaviour_t89_PropertyInfos/* properties */
	, WebCamAbstractBehaviour_t89_FieldInfos/* fields */
	, NULL/* events */
	, &MonoBehaviour_t6_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, WebCamAbstractBehaviour_t89_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WebCamAbstractBehaviour_t89_il2cpp_TypeInfo/* cast_class */
	, &WebCamAbstractBehaviour_t89_0_0_0/* byval_arg */
	, &WebCamAbstractBehaviour_t89_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WebCamAbstractBehaviour_t89)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 18/* method_count */
	, 6/* property_count */
	, 7/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavio.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo WordAbstractBehaviour_t60_il2cpp_TypeInfo;
// Vuforia.WordAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordAbstractBehavioMethodDeclarations.h"

// Vuforia.WordTemplateMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordTemplateMode.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilter.h"
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_Bounds.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_Mesh.h"
// UnityEngine.MeshFilter
#include "UnityEngine_UnityEngine_MeshFilterMethodDeclarations.h"
// UnityEngine.Mesh
#include "UnityEngine_UnityEngine_MeshMethodDeclarations.h"
// UnityEngine.Bounds
#include "UnityEngine_UnityEngine_BoundsMethodDeclarations.h"
// Vuforia.TrackableBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TrackableBehaviourMethodDeclarations.h"
extern MethodInfo Word_get_Size_m4955_MethodInfo;
extern MethodInfo Vector3_get_one_m4382_MethodInfo;
extern MethodInfo Component_GetComponent_TisMeshFilter_t169_m609_MethodInfo;
extern MethodInfo MeshFilter_get_sharedMesh_m644_MethodInfo;
extern MethodInfo Mesh_get_bounds_m5417_MethodInfo;
extern MethodInfo Bounds_get_size_m2378_MethodInfo;
extern MethodInfo TrackableBehaviour__ctor_m2636_MethodInfo;
struct Component_t100;
// UnityEngine.CastHelper`1<UnityEngine.MeshFilter>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_7.h"
// Declaration !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t169_m609(__this, method) (MeshFilter_t169 *)Component_GetComponent_TisObject_t_m241_gshared((Component_t100 *)__this, method)


// System.Void Vuforia.WordAbstractBehaviour::InternalUnregisterTrackable()
extern MethodInfo WordAbstractBehaviour_InternalUnregisterTrackable_m668_MethodInfo;
 void WordAbstractBehaviour_InternalUnregisterTrackable_m668 (WordAbstractBehaviour_t60 * __this, MethodInfo* method){
	Object_t * V_0 = {0};
	{
		V_0 = (Object_t *)NULL;
		__this->___mWord_11 = (Object_t *)NULL;
		__this->___mTrackable_7 = V_0;
		return;
	}
}
// Vuforia.Word Vuforia.WordAbstractBehaviour::get_Word()
extern MethodInfo WordAbstractBehaviour_get_Word_m4246_MethodInfo;
 Object_t * WordAbstractBehaviour_get_Word_m4246 (WordAbstractBehaviour_t60 * __this, MethodInfo* method){
	{
		Object_t * L_0 = (__this->___mWord_11);
		return L_0;
	}
}
// System.String Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_SpecificWord()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m669_MethodInfo;
 String_t* WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m669 (WordAbstractBehaviour_t60 * __this, MethodInfo* method){
	{
		String_t* L_0 = (__this->___mSpecificWord_10);
		return L_0;
	}
}
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetSpecificWord(System.String)
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m670_MethodInfo;
 void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m670 (WordAbstractBehaviour_t60 * __this, String_t* ___word, MethodInfo* method){
	{
		__this->___mSpecificWord_10 = ___word;
		return;
	}
}
// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_Mode()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m671_MethodInfo;
 int32_t WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m671 (WordAbstractBehaviour_t60 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mMode_9);
		return L_0;
	}
}
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsTemplateMode()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m672_MethodInfo;
 bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m672 (WordAbstractBehaviour_t60 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mMode_9);
		return ((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m673_MethodInfo;
 bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m673 (WordAbstractBehaviour_t60 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___mMode_9);
		return ((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetMode(Vuforia.WordTemplateMode)
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m674_MethodInfo;
 void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m674 (WordAbstractBehaviour_t60 * __this, int32_t ___mode, MethodInfo* method){
	{
		__this->___mMode_9 = ___mode;
		return;
	}
}
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.InitializeWord(Vuforia.Word)
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m675_MethodInfo;
 void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m675 (WordAbstractBehaviour_t60 * __this, Object_t * ___word, MethodInfo* method){
	Vector2_t9  V_0 = {0};
	Vector3_t13  V_1 = {0};
	MeshFilter_t169 * V_2 = {0};
	float V_3 = 0.0f;
	Object_t * V_4 = {0};
	Bounds_t350  V_5 = {0};
	{
		Object_t * L_0 = ___word;
		V_4 = L_0;
		__this->___mWord_11 = L_0;
		__this->___mTrackable_7 = V_4;
		NullCheck(___word);
		Vector2_t9  L_1 = (Vector2_t9 )InterfaceFuncInvoker0< Vector2_t9  >::Invoke(&Word_get_Size_m4955_MethodInfo, ___word);
		V_0 = L_1;
		Vector3_t13  L_2 = Vector3_get_one_m4382(NULL /*static, unused*/, /*hidden argument*/&Vector3_get_one_m4382_MethodInfo);
		V_1 = L_2;
		MeshFilter_t169 * L_3 = Component_GetComponent_TisMeshFilter_t169_m609(__this, /*hidden argument*/&Component_GetComponent_TisMeshFilter_t169_m609_MethodInfo);
		V_2 = L_3;
		bool L_4 = Object_op_Inequality_m335(NULL /*static, unused*/, V_2, (Object_t117 *)NULL, /*hidden argument*/&Object_op_Inequality_m335_MethodInfo);
		if (!L_4)
		{
			goto IL_0044;
		}
	}
	{
		NullCheck(V_2);
		Mesh_t174 * L_5 = MeshFilter_get_sharedMesh_m644(V_2, /*hidden argument*/&MeshFilter_get_sharedMesh_m644_MethodInfo);
		NullCheck(L_5);
		Bounds_t350  L_6 = Mesh_get_bounds_m5417(L_5, /*hidden argument*/&Mesh_get_bounds_m5417_MethodInfo);
		V_5 = L_6;
		Vector3_t13  L_7 = Bounds_get_size_m2378((&V_5), /*hidden argument*/&Bounds_get_size_m2378_MethodInfo);
		V_1 = L_7;
	}

IL_0044:
	{
		NullCheck((&V_0));
		float L_8 = ((&V_0)->___y_2);
		NullCheck((&V_1));
		float L_9 = ((&V_1)->___z_3);
		V_3 = ((float)((float)L_8/(float)L_9));
		Transform_t10 * L_10 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		Vector3_t13  L_11 = {0};
		Vector3__ctor_m248(&L_11, V_3, V_3, V_3, /*hidden argument*/&Vector3__ctor_m248_MethodInfo);
		NullCheck(L_10);
		Transform_set_localScale_m2332(L_10, L_11, /*hidden argument*/&Transform_set_localScale_m2332_MethodInfo);
		return;
	}
}
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
extern MethodInfo WordAbstractBehaviour__ctor_m663_MethodInfo;
 void WordAbstractBehaviour__ctor_m663 (WordAbstractBehaviour_t60 * __this, MethodInfo* method){
	{
		TrackableBehaviour__ctor_m2636(__this, /*hidden argument*/&TrackableBehaviour__ctor_m2636_MethodInfo);
		return;
	}
}
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m664_MethodInfo;
 bool WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m664 (WordAbstractBehaviour_t60 * __this, MethodInfo* method){
	{
		bool L_0 = Behaviour_get_enabled_m655(__this, /*hidden argument*/&Behaviour_get_enabled_m655_MethodInfo);
		return L_0;
	}
}
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m665_MethodInfo;
 void WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m665 (WordAbstractBehaviour_t60 * __this, bool p0, MethodInfo* method){
	{
		Behaviour_set_enabled_m216(__this, p0, /*hidden argument*/&Behaviour_set_enabled_m216_MethodInfo);
		return;
	}
}
// UnityEngine.Transform Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m666_MethodInfo;
 Transform_t10 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m666 (WordAbstractBehaviour_t60 * __this, MethodInfo* method){
	{
		Transform_t10 * L_0 = Component_get_transform_m219(__this, /*hidden argument*/&Component_get_transform_m219_MethodInfo);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m667_MethodInfo;
 GameObject_t2 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m667 (WordAbstractBehaviour_t60 * __this, MethodInfo* method){
	{
		GameObject_t2 * L_0 = Component_get_gameObject_m322(__this, /*hidden argument*/&Component_get_gameObject_m322_MethodInfo);
		return L_0;
	}
}
// Metadata Definition Vuforia.WordAbstractBehaviour
extern Il2CppType WordTemplateMode_t607_0_0_1;
extern CustomAttributesCache WordAbstractBehaviour_t60__CustomAttributeCache_mMode;
FieldInfo WordAbstractBehaviour_t60____mMode_9_FieldInfo = 
{
	"mMode"/* name */
	, &WordTemplateMode_t607_0_0_1/* type */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* parent */
	, offsetof(WordAbstractBehaviour_t60, ___mMode_9)/* data */
	, &WordAbstractBehaviour_t60__CustomAttributeCache_mMode/* custom_attributes_cache */

};
extern Il2CppType String_t_0_0_1;
extern CustomAttributesCache WordAbstractBehaviour_t60__CustomAttributeCache_mSpecificWord;
FieldInfo WordAbstractBehaviour_t60____mSpecificWord_10_FieldInfo = 
{
	"mSpecificWord"/* name */
	, &String_t_0_0_1/* type */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* parent */
	, offsetof(WordAbstractBehaviour_t60, ___mSpecificWord_10)/* data */
	, &WordAbstractBehaviour_t60__CustomAttributeCache_mSpecificWord/* custom_attributes_cache */

};
extern Il2CppType Word_t691_0_0_1;
FieldInfo WordAbstractBehaviour_t60____mWord_11_FieldInfo = 
{
	"mWord"/* name */
	, &Word_t691_0_0_1/* type */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* parent */
	, offsetof(WordAbstractBehaviour_t60, ___mWord_11)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* WordAbstractBehaviour_t60_FieldInfos[] =
{
	&WordAbstractBehaviour_t60____mMode_9_FieldInfo,
	&WordAbstractBehaviour_t60____mSpecificWord_10_FieldInfo,
	&WordAbstractBehaviour_t60____mWord_11_FieldInfo,
	NULL
};
static PropertyInfo WordAbstractBehaviour_t60____Word_PropertyInfo = 
{
	&WordAbstractBehaviour_t60_il2cpp_TypeInfo/* parent */
	, "Word"/* name */
	, &WordAbstractBehaviour_get_Word_m4246_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WordAbstractBehaviour_t60____Vuforia_IEditorWordBehaviour_SpecificWord_PropertyInfo = 
{
	&WordAbstractBehaviour_t60_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorWordBehaviour.SpecificWord"/* name */
	, &WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m669_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WordAbstractBehaviour_t60____Vuforia_IEditorWordBehaviour_Mode_PropertyInfo = 
{
	&WordAbstractBehaviour_t60_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorWordBehaviour.Mode"/* name */
	, &WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m671_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WordAbstractBehaviour_t60____Vuforia_IEditorWordBehaviour_IsTemplateMode_PropertyInfo = 
{
	&WordAbstractBehaviour_t60_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorWordBehaviour.IsTemplateMode"/* name */
	, &WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m672_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo WordAbstractBehaviour_t60____Vuforia_IEditorWordBehaviour_IsSpecificWordMode_PropertyInfo = 
{
	&WordAbstractBehaviour_t60_il2cpp_TypeInfo/* parent */
	, "Vuforia.IEditorWordBehaviour.IsSpecificWordMode"/* name */
	, &WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m673_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* WordAbstractBehaviour_t60_PropertyInfos[] =
{
	&WordAbstractBehaviour_t60____Word_PropertyInfo,
	&WordAbstractBehaviour_t60____Vuforia_IEditorWordBehaviour_SpecificWord_PropertyInfo,
	&WordAbstractBehaviour_t60____Vuforia_IEditorWordBehaviour_Mode_PropertyInfo,
	&WordAbstractBehaviour_t60____Vuforia_IEditorWordBehaviour_IsTemplateMode_PropertyInfo,
	&WordAbstractBehaviour_t60____Vuforia_IEditorWordBehaviour_IsSpecificWordMode_PropertyInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::InternalUnregisterTrackable()
MethodInfo WordAbstractBehaviour_InternalUnregisterTrackable_m668_MethodInfo = 
{
	"InternalUnregisterTrackable"/* name */
	, (methodPointerType)&WordAbstractBehaviour_InternalUnregisterTrackable_m668/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 23/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2482/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Word_t691_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// Vuforia.Word Vuforia.WordAbstractBehaviour::get_Word()
MethodInfo WordAbstractBehaviour_get_Word_m4246_MethodInfo = 
{
	"get_Word"/* name */
	, (methodPointerType)&WordAbstractBehaviour_get_Word_m4246/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Word_t691_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2483/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// System.String Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_SpecificWord()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m669_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.get_SpecificWord"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m669/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &String_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 25/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2484/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType String_t_0_0_0;
static ParameterInfo WordAbstractBehaviour_t60_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m670_ParameterInfos[] = 
{
	{"word", 0, 134220054, &EmptyCustomAttributesCache, &String_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetSpecificWord(System.String)
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m670_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.SetSpecificWord"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m670/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, WordAbstractBehaviour_t60_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m670_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 26/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2485/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WordTemplateMode_t607_0_0_0;
extern void* RuntimeInvoker_WordTemplateMode_t607 (MethodInfo* method, void* obj, void** args);
// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_Mode()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m671_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.get_Mode"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m671/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &WordTemplateMode_t607_0_0_0/* return_type */
	, RuntimeInvoker_WordTemplateMode_t607/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 27/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2486/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsTemplateMode()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m672_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.get_IsTemplateMode"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m672/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 28/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2487/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m673_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m673/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 29/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2488/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType WordTemplateMode_t607_0_0_0;
extern Il2CppType WordTemplateMode_t607_0_0_0;
static ParameterInfo WordAbstractBehaviour_t60_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m674_ParameterInfos[] = 
{
	{"mode", 0, 134220055, &EmptyCustomAttributesCache, &WordTemplateMode_t607_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetMode(Vuforia.WordTemplateMode)
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m674_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.SetMode"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m674/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, WordAbstractBehaviour_t60_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m674_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 30/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2489/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Word_t691_0_0_0;
extern Il2CppType Word_t691_0_0_0;
static ParameterInfo WordAbstractBehaviour_t60_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m675_ParameterInfos[] = 
{
	{"word", 0, 134220056, &EmptyCustomAttributesCache, &Word_t691_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.InitializeWord(Vuforia.Word)
MethodInfo WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m675_MethodInfo = 
{
	"Vuforia.IEditorWordBehaviour.InitializeWord"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m675/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, WordAbstractBehaviour_t60_WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m675_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 31/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2490/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
MethodInfo WordAbstractBehaviour__ctor_m663_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&WordAbstractBehaviour__ctor_m663/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2491/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m664_MethodInfo = 
{
	"Vuforia.IEditorTrackableBehaviour.get_enabled"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m664/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2492/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo WordAbstractBehaviour_t60_WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m665_ParameterInfos[] = 
{
	{"", 0, 134217728, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_SByte_t129 (MethodInfo* method, void* obj, void** args);
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m665_MethodInfo = 
{
	"Vuforia.IEditorTrackableBehaviour.set_enabled"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m665/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_SByte_t129/* invoker_method */
	, WordAbstractBehaviour_t60_WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m665_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2493/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType Transform_t10_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.Transform Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m666_MethodInfo = 
{
	"Vuforia.IEditorTrackableBehaviour.get_transform"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m666/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &Transform_t10_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2494/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
extern Il2CppType GameObject_t2_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
// UnityEngine.GameObject Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
MethodInfo WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m667_MethodInfo = 
{
	"Vuforia.IEditorTrackableBehaviour.get_gameObject"/* name */
	, (methodPointerType)&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m667/* method */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* declaring_type */
	, &GameObject_t2_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 18/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, false/* is_inflated */
	, 2495/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, NULL/* dummy */

};
static MethodInfo* WordAbstractBehaviour_t60_MethodInfos[] =
{
	&WordAbstractBehaviour_InternalUnregisterTrackable_m668_MethodInfo,
	&WordAbstractBehaviour_get_Word_m4246_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m669_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m670_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m671_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m672_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m673_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m674_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m675_MethodInfo,
	&WordAbstractBehaviour__ctor_m663_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m664_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m665_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m666_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m667_MethodInfo,
	NULL
};
extern MethodInfo TrackableBehaviour_get_TrackableName_m345_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m346_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m347_MethodInfo;
extern MethodInfo TrackableBehaviour_get_Trackable_m348_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m349_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m350_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m351_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m352_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m353_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m354_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m355_MethodInfo;
extern MethodInfo TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m360_MethodInfo;
extern MethodInfo TrackableBehaviour_OnTrackerUpdate_m480_MethodInfo;
extern MethodInfo TrackableBehaviour_OnFrameIndexUpdate_m426_MethodInfo;
extern MethodInfo TrackableBehaviour_CorrectScaleImpl_m496_MethodInfo;
static MethodInfo* WordAbstractBehaviour_t60_VTable[] =
{
	&Object_Equals_m229_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m230_MethodInfo,
	&Object_ToString_m231_MethodInfo,
	&TrackableBehaviour_get_TrackableName_m345_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_CorrectScale_m346_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetNameForTrackable_m347_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreviousScale_m349_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreviousScale_m350_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_PreserveChildSize_m351_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetPreserveChildSize_m352_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_get_InitializedInEditor_m353_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_SetInitializedInEditor_m354_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_UnregisterTrackable_m355_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m664_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m665_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m666_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m667_MethodInfo,
	&TrackableBehaviour_Vuforia_IEditorTrackableBehaviour_GetRenderer_m360_MethodInfo,
	&TrackableBehaviour_get_Trackable_m348_MethodInfo,
	&TrackableBehaviour_OnTrackerUpdate_m480_MethodInfo,
	&TrackableBehaviour_OnFrameIndexUpdate_m426_MethodInfo,
	&WordAbstractBehaviour_InternalUnregisterTrackable_m668_MethodInfo,
	&TrackableBehaviour_CorrectScaleImpl_m496_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m669_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m670_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m671_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m672_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m673_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m674_MethodInfo,
	&WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m675_MethodInfo,
};
extern TypeInfo IEditorWordBehaviour_t178_il2cpp_TypeInfo;
extern TypeInfo IEditorTrackableBehaviour_t125_il2cpp_TypeInfo;
static TypeInfo* WordAbstractBehaviour_t60_InterfacesTypeInfos[] = 
{
	&IEditorWordBehaviour_t178_il2cpp_TypeInfo,
	&IEditorTrackableBehaviour_t125_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair WordAbstractBehaviour_t60_InterfacesOffsets[] = 
{
	{ &IEditorTrackableBehaviour_t125_il2cpp_TypeInfo, 4},
	{ &IEditorWordBehaviour_t178_il2cpp_TypeInfo, 25},
};
void WordAbstractBehaviour_t60_CustomAttributesCacheGenerator_mMode(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
void WordAbstractBehaviour_t60_CustomAttributesCacheGenerator_mSpecificWord(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t103 * tmp;
		tmp = (SerializeField_t103 *)il2cpp_codegen_object_new (&SerializeField_t103_il2cpp_TypeInfo);
		SerializeField__ctor_m259(tmp, &SerializeField__ctor_m259_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t777 * tmp;
		tmp = (HideInInspector_t777 *)il2cpp_codegen_object_new (&HideInInspector_t777_il2cpp_TypeInfo);
		HideInInspector__ctor_m4300(tmp, &HideInInspector__ctor_m4300_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache WordAbstractBehaviour_t60__CustomAttributeCache_mMode = {
2,
NULL,
&WordAbstractBehaviour_t60_CustomAttributesCacheGenerator_mMode
};
CustomAttributesCache WordAbstractBehaviour_t60__CustomAttributeCache_mSpecificWord = {
2,
NULL,
&WordAbstractBehaviour_t60_CustomAttributesCacheGenerator_mSpecificWord
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WordAbstractBehaviour_t60_0_0_0;
extern Il2CppType WordAbstractBehaviour_t60_1_0_0;
struct WordAbstractBehaviour_t60;
extern CustomAttributesCache WordAbstractBehaviour_t60__CustomAttributeCache_mMode;
extern CustomAttributesCache WordAbstractBehaviour_t60__CustomAttributeCache_mSpecificWord;
TypeInfo WordAbstractBehaviour_t60_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordAbstractBehaviour"/* name */
	, "Vuforia"/* namespaze */
	, WordAbstractBehaviour_t60_MethodInfos/* methods */
	, WordAbstractBehaviour_t60_PropertyInfos/* properties */
	, WordAbstractBehaviour_t60_FieldInfos/* fields */
	, NULL/* events */
	, &TrackableBehaviour_t44_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* element_class */
	, WordAbstractBehaviour_t60_InterfacesTypeInfos/* implemented_interfaces */
	, WordAbstractBehaviour_t60_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &WordAbstractBehaviour_t60_il2cpp_TypeInfo/* cast_class */
	, &WordAbstractBehaviour_t60_0_0_0/* byval_arg */
	, &WordAbstractBehaviour_t60_1_0_0/* this_arg */
	, WordAbstractBehaviour_t60_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WordAbstractBehaviour_t60)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048705/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 14/* method_count */
	, 5/* property_count */
	, 3/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 32/* vtable_count */
	, 2/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
// Vuforia.WordFilterMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordFilterModeMethodDeclarations.h"



// Metadata Definition Vuforia.WordFilterMode
extern Il2CppType Int32_t93_0_0_1542;
FieldInfo WordFilterMode_t772____value___1_FieldInfo = 
{
	"value__"/* name */
	, &Int32_t93_0_0_1542/* type */
	, &WordFilterMode_t772_il2cpp_TypeInfo/* parent */
	, offsetof(WordFilterMode_t772, ___value___1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType WordFilterMode_t772_0_0_32854;
FieldInfo WordFilterMode_t772____NONE_2_FieldInfo = 
{
	"NONE"/* name */
	, &WordFilterMode_t772_0_0_32854/* type */
	, &WordFilterMode_t772_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType WordFilterMode_t772_0_0_32854;
FieldInfo WordFilterMode_t772____BLACK_LIST_3_FieldInfo = 
{
	"BLACK_LIST"/* name */
	, &WordFilterMode_t772_0_0_32854/* type */
	, &WordFilterMode_t772_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType WordFilterMode_t772_0_0_32854;
FieldInfo WordFilterMode_t772____WHITE_LIST_4_FieldInfo = 
{
	"WHITE_LIST"/* name */
	, &WordFilterMode_t772_0_0_32854/* type */
	, &WordFilterMode_t772_il2cpp_TypeInfo/* parent */
	, 0 /*field is const -> no data*//* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* WordFilterMode_t772_FieldInfos[] =
{
	&WordFilterMode_t772____value___1_FieldInfo,
	&WordFilterMode_t772____NONE_2_FieldInfo,
	&WordFilterMode_t772____BLACK_LIST_3_FieldInfo,
	&WordFilterMode_t772____WHITE_LIST_4_FieldInfo,
	NULL
};
static const int32_t WordFilterMode_t772____NONE_2_DefaultValueData = 0;
static Il2CppFieldDefaultValueEntry WordFilterMode_t772____NONE_2_DefaultValue = 
{
	&WordFilterMode_t772____NONE_2_FieldInfo/* field */
	, { (char*)&WordFilterMode_t772____NONE_2_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t WordFilterMode_t772____BLACK_LIST_3_DefaultValueData = 1;
static Il2CppFieldDefaultValueEntry WordFilterMode_t772____BLACK_LIST_3_DefaultValue = 
{
	&WordFilterMode_t772____BLACK_LIST_3_FieldInfo/* field */
	, { (char*)&WordFilterMode_t772____BLACK_LIST_3_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static const int32_t WordFilterMode_t772____WHITE_LIST_4_DefaultValueData = 2;
static Il2CppFieldDefaultValueEntry WordFilterMode_t772____WHITE_LIST_4_DefaultValue = 
{
	&WordFilterMode_t772____WHITE_LIST_4_FieldInfo/* field */
	, { (char*)&WordFilterMode_t772____WHITE_LIST_4_DefaultValueData, &Int32_t93_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* WordFilterMode_t772_FieldDefaultValues[] = 
{
	&WordFilterMode_t772____NONE_2_DefaultValue,
	&WordFilterMode_t772____BLACK_LIST_3_DefaultValue,
	&WordFilterMode_t772____WHITE_LIST_4_DefaultValue,
	NULL
};
static MethodInfo* WordFilterMode_t772_MethodInfos[] =
{
	NULL
};
static MethodInfo* WordFilterMode_t772_VTable[] =
{
	&Enum_Equals_m191_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Enum_GetHashCode_m193_MethodInfo,
	&Enum_ToString_m194_MethodInfo,
	&Enum_ToString_m195_MethodInfo,
	&Enum_System_IConvertible_ToBoolean_m196_MethodInfo,
	&Enum_System_IConvertible_ToByte_m197_MethodInfo,
	&Enum_System_IConvertible_ToChar_m198_MethodInfo,
	&Enum_System_IConvertible_ToDateTime_m199_MethodInfo,
	&Enum_System_IConvertible_ToDecimal_m200_MethodInfo,
	&Enum_System_IConvertible_ToDouble_m201_MethodInfo,
	&Enum_System_IConvertible_ToInt16_m202_MethodInfo,
	&Enum_System_IConvertible_ToInt32_m203_MethodInfo,
	&Enum_System_IConvertible_ToInt64_m204_MethodInfo,
	&Enum_System_IConvertible_ToSByte_m205_MethodInfo,
	&Enum_System_IConvertible_ToSingle_m206_MethodInfo,
	&Enum_ToString_m207_MethodInfo,
	&Enum_System_IConvertible_ToType_m208_MethodInfo,
	&Enum_System_IConvertible_ToUInt16_m209_MethodInfo,
	&Enum_System_IConvertible_ToUInt32_m210_MethodInfo,
	&Enum_System_IConvertible_ToUInt64_m211_MethodInfo,
	&Enum_CompareTo_m212_MethodInfo,
	&Enum_GetTypeCode_m213_MethodInfo,
};
static Il2CppInterfaceOffsetPair WordFilterMode_t772_InterfacesOffsets[] = 
{
	{ &IFormattable_t94_il2cpp_TypeInfo, 4},
	{ &IConvertible_t95_il2cpp_TypeInfo, 5},
	{ &IComparable_t96_il2cpp_TypeInfo, 21},
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType WordFilterMode_t772_1_0_0;
TypeInfo WordFilterMode_t772_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "WordFilterMode"/* name */
	, "Vuforia"/* namespaze */
	, WordFilterMode_t772_MethodInfos/* methods */
	, NULL/* properties */
	, WordFilterMode_t772_FieldInfos/* fields */
	, NULL/* events */
	, &Enum_t97_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Int32_t93_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, WordFilterMode_t772_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Int32_t93_il2cpp_TypeInfo/* cast_class */
	, &WordFilterMode_t772_0_0_0/* byval_arg */
	, &WordFilterMode_t772_1_0_0/* this_arg */
	, WordFilterMode_t772_InterfacesOffsets/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, WordFilterMode_t772_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (WordFilterMode_t772)+ sizeof (Il2CppObject)/* instance_size */
	, sizeof (int32_t)/* element_size */
	, sizeof(int32_t)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, true/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 23/* vtable_count */
	, 0/* interfaces_count */
	, 3/* interface_offsets_count */

};
// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDet.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo __StaticArrayInitTypeSizeU3D24_t773_il2cpp_TypeInfo;
// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDetMethodDeclarations.h"



// Conversion methods for marshalling of: <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
void __StaticArrayInitTypeSizeU3D24_t773_marshal(const __StaticArrayInitTypeSizeU3D24_t773& unmarshaled, __StaticArrayInitTypeSizeU3D24_t773_marshaled& marshaled)
{
}
void __StaticArrayInitTypeSizeU3D24_t773_marshal_back(const __StaticArrayInitTypeSizeU3D24_t773_marshaled& marshaled, __StaticArrayInitTypeSizeU3D24_t773& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
void __StaticArrayInitTypeSizeU3D24_t773_marshal_cleanup(__StaticArrayInitTypeSizeU3D24_t773_marshaled& marshaled)
{
}
// Metadata Definition <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}/__StaticArrayInitTypeSize=24
static MethodInfo* __StaticArrayInitTypeSizeU3D24_t773_MethodInfos[] =
{
	NULL
};
static MethodInfo* __StaticArrayInitTypeSizeU3D24_t773_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType __StaticArrayInitTypeSizeU3D24_t773_0_0_0;
extern Il2CppType __StaticArrayInitTypeSizeU3D24_t773_1_0_0;
extern TypeInfo U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_il2cpp_TypeInfo;
TypeInfo __StaticArrayInitTypeSizeU3D24_t773_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "__StaticArrayInitTypeSize=24"/* name */
	, ""/* namespaze */
	, __StaticArrayInitTypeSizeU3D24_t773_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_il2cpp_TypeInfo/* nested_in */
	, &__StaticArrayInitTypeSizeU3D24_t773_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, __StaticArrayInitTypeSizeU3D24_t773_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &__StaticArrayInitTypeSizeU3D24_t773_il2cpp_TypeInfo/* cast_class */
	, &__StaticArrayInitTypeSizeU3D24_t773_0_0_0/* byval_arg */
	, &__StaticArrayInitTypeSizeU3D24_t773_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D24_t773_marshal/* marshal_to_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D24_t773_marshal_back/* marshal_from_native_func */
	, (methodPointerType)__StaticArrayInitTypeSizeU3D24_t773_marshal_cleanup/* marshal_cleanup_func */
	, sizeof (__StaticArrayInitTypeSizeU3D24_t773)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, sizeof(__StaticArrayInitTypeSizeU3D24_t773_marshaled)/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 275/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDet_0.h"
#ifndef _MSC_VER
#else
#endif
// <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}
#include "Qualcomm_Vuforia_UnityExtensions_U3CPrivateImplementationDet_0MethodDeclarations.h"



// Metadata Definition <PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}
extern Il2CppType __StaticArrayInitTypeSizeU3D24_t773_0_0_275;
FieldInfo U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774____$$method0x6000856U2D1_0_FieldInfo = 
{
	"$$method0x6000856-1"/* name */
	, &__StaticArrayInitTypeSizeU3D24_t773_0_0_275/* type */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_il2cpp_TypeInfo/* parent */
	, offsetof(U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_StaticFields, ___$$method0x6000856U2D1_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_FieldInfos[] =
{
	&U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774____$$method0x6000856U2D1_0_FieldInfo,
	NULL
};
static const uint8_t U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774____$$method0x6000856U2D1_0_DefaultValueData[] = { 0x0, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x2, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x3, 0x0, 0x0, 0x0 };
static Il2CppFieldDefaultValueEntry U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774____$$method0x6000856U2D1_0_DefaultValue = 
{
	&U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774____$$method0x6000856U2D1_0_FieldInfo/* field */
	, { (char*)U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774____$$method0x6000856U2D1_0_DefaultValueData, &__StaticArrayInitTypeSizeU3D24_t773_0_0_0 }/* value */

};
static Il2CppFieldDefaultValueEntry* U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_FieldDefaultValues[] = 
{
	&U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774____$$method0x6000856U2D1_0_DefaultValue,
	NULL
};
static MethodInfo* U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_MethodInfos[] =
{
	NULL
};
extern TypeInfo __StaticArrayInitTypeSizeU3D24_t773_il2cpp_TypeInfo;
static TypeInfo* U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_il2cpp_TypeInfo__nestedTypes[2] =
{
	&__StaticArrayInitTypeSizeU3D24_t773_il2cpp_TypeInfo,
	NULL
};
static MethodInfo* U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
void U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t421 * tmp;
		tmp = (CompilerGeneratedAttribute_t421 *)il2cpp_codegen_object_new (&CompilerGeneratedAttribute_t421_il2cpp_TypeInfo);
		CompilerGeneratedAttribute__ctor_m1901(tmp, &CompilerGeneratedAttribute__ctor_m1901_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774__CustomAttributeCache = {
1,
NULL,
&U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_CustomAttributesCacheGenerator
};
extern Il2CppImage g_Qualcomm_Vuforia_UnityExtensions_dll_Image;
extern Il2CppType U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_0_0_0;
extern Il2CppType U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_1_0_0;
struct U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774;
extern CustomAttributesCache U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774__CustomAttributeCache;
TypeInfo U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_il2cpp_TypeInfo = 
{
	&g_Qualcomm_Vuforia_UnityExtensions_dll_Image/* image */
	, NULL/* gc_desc */
	, "<PrivateImplementationDetails>{66DCC020-EBD6-4DBA-A757-272BEBA33044}"/* name */
	, ""/* namespaze */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_MethodInfos/* methods */
	, NULL/* properties */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_il2cpp_TypeInfo__nestedTypes/* nested_types */
	, NULL/* nested_in */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_VTable/* vtable */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774__CustomAttributeCache/* custom_attributes_cache */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_il2cpp_TypeInfo/* cast_class */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_0_0_0/* byval_arg */
	, &U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, NULL/* generic_class */
	, NULL/* generic_container */
	, U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_FieldDefaultValues/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, sizeof(U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t774_StaticFields)/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 0/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, false/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 1/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
