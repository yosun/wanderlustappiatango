﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GL
struct GL_t943;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Matrix4x4
#include "UnityEngine_UnityEngine_Matrix4x4.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void UnityEngine.GL::Vertex(UnityEngine.Vector3)
 void GL_Vertex_m652 (Object_t * __this/* static, unused */, Vector3_t13  ___v, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::INTERNAL_CALL_Vertex(UnityEngine.Vector3&)
 void GL_INTERNAL_CALL_Vertex_m5545 (Object_t * __this/* static, unused */, Vector3_t13 * ___v, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Begin(System.Int32)
 void GL_Begin_m651 (Object_t * __this/* static, unused */, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::End()
 void GL_End_m653 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::MultMatrix(UnityEngine.Matrix4x4)
 void GL_MultMatrix_m649 (Object_t * __this/* static, unused */, Matrix4x4_t176  ___mat, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::INTERNAL_CALL_MultMatrix(UnityEngine.Matrix4x4&)
 void GL_INTERNAL_CALL_MultMatrix_m5546 (Object_t * __this/* static, unused */, Matrix4x4_t176 * ___mat, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::PushMatrix()
 void GL_PushMatrix_m647 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::PopMatrix()
 void GL_PopMatrix_m654 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::SetRevertBackfacing(System.Boolean)
 void GL_SetRevertBackfacing_m5401 (Object_t * __this/* static, unused */, bool ___revertBackFaces, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Clear(System.Boolean,System.Boolean,UnityEngine.Color)
 void GL_Clear_m4587 (Object_t * __this/* static, unused */, bool ___clearDepth, bool ___clearColor, Color_t19  ___backgroundColor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Clear(System.Boolean,System.Boolean,UnityEngine.Color,System.Single)
 void GL_Clear_m5547 (Object_t * __this/* static, unused */, bool ___clearDepth, bool ___clearColor, Color_t19  ___backgroundColor, float ___depth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color,System.Single)
 void GL_Internal_Clear_m5548 (Object_t * __this/* static, unused */, bool ___clearDepth, bool ___clearColor, Color_t19  ___backgroundColor, float ___depth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::INTERNAL_CALL_Internal_Clear(System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)
 void GL_INTERNAL_CALL_Internal_Clear_m5549 (Object_t * __this/* static, unused */, bool ___clearDepth, bool ___clearColor, Color_t19 * ___backgroundColor, float ___depth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GL::IssuePluginEvent(System.Int32)
 void GL_IssuePluginEvent_m4887 (Object_t * __this/* static, unused */, int32_t ___eventID, MethodInfo* method) IL2CPP_METHOD_ATTR;
