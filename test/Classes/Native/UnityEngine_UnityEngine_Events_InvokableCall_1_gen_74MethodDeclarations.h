﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.AspectRatioFitter>
struct InvokableCall_1_t3552;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t142;
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.AspectRatioFitter>
struct UnityAction_1_t3553;
// System.Object[]
struct ObjectU5BU5D_t115;

// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.AspectRatioFitter>::.ctor(System.Object,System.Reflection.MethodInfo)
// UnityEngine.Events.InvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_0MethodDeclarations.h"
#define InvokableCall_1__ctor_m19683(__this, ___target, ___theFunction, method) (void)InvokableCall_1__ctor_m14011_gshared((InvokableCall_1_t2706 *)__this, (Object_t *)___target, (MethodInfo_t142 *)___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.AspectRatioFitter>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
#define InvokableCall_1__ctor_m19684(__this, ___callback, method) (void)InvokableCall_1__ctor_m14012_gshared((InvokableCall_1_t2706 *)__this, (UnityAction_1_t2707 *)___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UI.AspectRatioFitter>::Invoke(System.Object[])
#define InvokableCall_1_Invoke_m19685(__this, ___args, method) (void)InvokableCall_1_Invoke_m14013_gshared((InvokableCall_1_t2706 *)__this, (ObjectU5BU5D_t115*)___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UI.AspectRatioFitter>::Find(System.Object,System.Reflection.MethodInfo)
#define InvokableCall_1_Find_m19686(__this, ___targetObj, ___method, method) (bool)InvokableCall_1_Find_m14014_gshared((InvokableCall_1_t2706 *)__this, (Object_t *)___targetObj, (MethodInfo_t142 *)___method, method)
