﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Security.Policy.IBuiltInEvidence>
struct InternalEnumerator_1_t5107;
// System.Object
struct Object_t;
// System.Security.Policy.IBuiltInEvidence
struct IBuiltInEvidence_t2650;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Security.Policy.IBuiltInEvidence>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m31115(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Security.Policy.IBuiltInEvidence>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31116(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Policy.IBuiltInEvidence>::Dispose()
#define InternalEnumerator_1_Dispose_m31117(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Policy.IBuiltInEvidence>::MoveNext()
#define InternalEnumerator_1_MoveNext_m31118(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<System.Security.Policy.IBuiltInEvidence>::get_Current()
#define InternalEnumerator_1_get_Current_m31119(__this, method) (Object_t *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
