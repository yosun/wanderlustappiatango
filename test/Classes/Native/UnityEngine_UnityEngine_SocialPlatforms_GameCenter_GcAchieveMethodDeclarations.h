﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData
struct GcAchievementDescriptionData_t936;
// UnityEngine.SocialPlatforms.Impl.AchievementDescription
struct AchievementDescription_t1043;

// UnityEngine.SocialPlatforms.Impl.AchievementDescription UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData::ToAchievementDescription()
 AchievementDescription_t1043 * GcAchievementDescriptionData_ToAchievementDescription_m6246 (GcAchievementDescriptionData_t936 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
