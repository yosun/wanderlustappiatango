﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<OrbitCamera>
struct UnityAction_1_t2768;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<OrbitCamera>
struct InvokableCall_1_t2767  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<OrbitCamera>::Delegate
	UnityAction_1_t2768 * ___Delegate_0;
};
