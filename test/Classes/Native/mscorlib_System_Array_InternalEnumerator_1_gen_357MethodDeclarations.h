﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARRuntimeUtilities/WebCamUsed>
struct InternalEnumerator_1_t4343;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARRuntimeUtilities/WebCamUsed
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRuntimeUtilitie_0.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARRuntimeUtilities/WebCamUsed>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m26551 (InternalEnumerator_1_t4343 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARRuntimeUtilities/WebCamUsed>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26552 (InternalEnumerator_1_t4343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARRuntimeUtilities/WebCamUsed>::Dispose()
 void InternalEnumerator_1_Dispose_m26553 (InternalEnumerator_1_t4343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARRuntimeUtilities/WebCamUsed>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m26554 (InternalEnumerator_1_t4343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.QCARRuntimeUtilities/WebCamUsed>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m26555 (InternalEnumerator_1_t4343 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
