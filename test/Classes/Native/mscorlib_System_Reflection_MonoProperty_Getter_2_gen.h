﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t5039  : public MulticastDelegate_t325
{
};
