﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MarshalByRefObject
struct MarshalByRefObject_t1348;

// System.Void System.MarshalByRefObject::.ctor()
 void MarshalByRefObject__ctor_m7714 (MarshalByRefObject_t1348 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
