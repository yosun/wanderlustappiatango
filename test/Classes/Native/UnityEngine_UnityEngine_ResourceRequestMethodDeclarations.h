﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.ResourceRequest
struct ResourceRequest_t982;
// UnityEngine.Object
struct Object_t117;
struct Object_t117_marshaled;

// System.Void UnityEngine.ResourceRequest::.ctor()
 void ResourceRequest__ctor_m5907 (ResourceRequest_t982 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
 Object_t117 * ResourceRequest_get_asset_m5908 (ResourceRequest_t982 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
