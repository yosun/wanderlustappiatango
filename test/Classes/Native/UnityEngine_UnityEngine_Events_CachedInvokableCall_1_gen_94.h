﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_96.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>
struct CachedInvokableCall_1_t3712  : public InvokableCall_1_t3713
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.SmartTerrainTrackerAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
