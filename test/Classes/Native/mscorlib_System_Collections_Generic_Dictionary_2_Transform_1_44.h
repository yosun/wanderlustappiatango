﻿#pragma once
#include <stdint.h>
// System.Type
struct Type_t;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.UInt16
#include "mscorlib_System_UInt16.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo UInt16_t823_il2cpp_TypeInfo;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Type,System.UInt16,System.UInt16>
struct Transform_1_t3952  : public MulticastDelegate_t325
{
};
