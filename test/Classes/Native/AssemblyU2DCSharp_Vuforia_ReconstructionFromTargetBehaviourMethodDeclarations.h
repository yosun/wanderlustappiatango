﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionFromTargetBehaviour
struct ReconstructionFromTargetBehaviour_t73;

// System.Void Vuforia.ReconstructionFromTargetBehaviour::.ctor()
 void ReconstructionFromTargetBehaviour__ctor_m168 (ReconstructionFromTargetBehaviour_t73 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
