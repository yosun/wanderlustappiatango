﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.Emit.UnmanagedMarshal
struct UnmanagedMarshal_t1905;
// System.Runtime.InteropServices.MarshalAsAttribute
struct MarshalAsAttribute_t1710;

// System.Runtime.InteropServices.MarshalAsAttribute System.Reflection.Emit.UnmanagedMarshal::ToMarshalAsAttribute()
 MarshalAsAttribute_t1710 * UnmanagedMarshal_ToMarshalAsAttribute_m11127 (UnmanagedMarshal_t1905 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
