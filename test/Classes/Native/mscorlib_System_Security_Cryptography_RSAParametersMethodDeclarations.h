﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAParameters
struct RSAParameters_t1507;
struct RSAParameters_t1507_marshaled;

void RSAParameters_t1507_marshal(const RSAParameters_t1507& unmarshaled, RSAParameters_t1507_marshaled& marshaled);
void RSAParameters_t1507_marshal_back(const RSAParameters_t1507_marshaled& marshaled, RSAParameters_t1507& unmarshaled);
void RSAParameters_t1507_marshal_cleanup(RSAParameters_t1507_marshaled& marshaled);
