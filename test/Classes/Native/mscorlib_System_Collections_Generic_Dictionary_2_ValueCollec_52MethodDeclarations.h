﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct Enumerator_t4294;
// System.Object
struct Object_t;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t56;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct Dictionary_2_t750;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void Enumerator__ctor_m26187 (Enumerator_t4294 * __this, Dictionary_2_t750 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26188 (Enumerator_t4294 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::Dispose()
 void Enumerator_Dispose_m26189 (Enumerator_t4294 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::MoveNext()
 bool Enumerator_MoveNext_m26190 (Enumerator_t4294 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
 VirtualButtonAbstractBehaviour_t56 * Enumerator_get_Current_m26191 (Enumerator_t4294 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
