﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<SetRenderQueueChildren>
struct UnityAction_1_t2753;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>
struct InvokableCall_1_t2752  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>::Delegate
	UnityAction_1_t2753 * ___Delegate_0;
};
