﻿#pragma once
#include <stdint.h>
// System.Reflection.Assembly
struct Assembly_t1503;
// System.Type
struct Type_t;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_t1160;
// System.Object
#include "mscorlib_System_Object.h"
// System.MonoCustomAttrs
struct MonoCustomAttrs_t2230  : public Object_t
{
};
struct MonoCustomAttrs_t2230_StaticFields{
	// System.Reflection.Assembly System.MonoCustomAttrs::corlib
	Assembly_t1503 * ___corlib_0;
	// System.Type System.MonoCustomAttrs::AttributeUsageType
	Type_t * ___AttributeUsageType_1;
	// System.AttributeUsageAttribute System.MonoCustomAttrs::DefaultAttributeUsage
	AttributeUsageAttribute_t1160 * ___DefaultAttributeUsage_2;
};
