﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.InputField>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_60.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.InputField>
struct CachedInvokableCall_1_t3429  : public InvokableCall_1_t3430
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.InputField>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
