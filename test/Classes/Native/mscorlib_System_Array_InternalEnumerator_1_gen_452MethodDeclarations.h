﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>
struct InternalEnumerator_1_t4744;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SocialPlatforms.UserScope
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29087 (InternalEnumerator_1_t4744 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29088 (InternalEnumerator_1_t4744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::Dispose()
 void InternalEnumerator_1_Dispose_m29089 (InternalEnumerator_1_t4744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29090 (InternalEnumerator_1_t4744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.UserScope>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29091 (InternalEnumerator_1_t4744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
