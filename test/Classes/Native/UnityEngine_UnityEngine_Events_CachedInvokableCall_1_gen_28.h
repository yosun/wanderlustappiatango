﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.MaskOutBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_24.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.MaskOutBehaviour>
struct CachedInvokableCall_1_t2889  : public InvokableCall_1_t2890
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.MaskOutBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
