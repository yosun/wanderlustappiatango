﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.IVideoBackgroundEventHandler>
struct Comparer_1_t4338;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.IVideoBackgroundEventHandler>
struct Comparer_1_t4338  : public Object_t
{
};
struct Comparer_1_t4338_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.IVideoBackgroundEventHandler>::_default
	Comparer_1_t4338 * ____default_0;
};
