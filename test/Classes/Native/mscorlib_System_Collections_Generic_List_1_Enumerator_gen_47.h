﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
struct List_1_t665;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t581;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>
struct Enumerator_t3919 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::l
	List_1_t665 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>::current
	Object_t * ___current_3;
};
