﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t1538;
// Mono.Math.BigInteger
struct BigInteger_t1537;

// System.Void Mono.Math.BigInteger/ModulusRing::.ctor(Mono.Math.BigInteger)
 void ModulusRing__ctor_m7973 (ModulusRing_t1538 * __this, BigInteger_t1537 * ___modulus, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/ModulusRing::BarrettReduction(Mono.Math.BigInteger)
 void ModulusRing_BarrettReduction_m7974 (ModulusRing_t1538 * __this, BigInteger_t1537 * ___x, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
 BigInteger_t1537 * ModulusRing_Multiply_m7975 (ModulusRing_t1538 * __this, BigInteger_t1537 * ___a, BigInteger_t1537 * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Difference(Mono.Math.BigInteger,Mono.Math.BigInteger)
 BigInteger_t1537 * ModulusRing_Difference_m7976 (ModulusRing_t1538 * __this, BigInteger_t1537 * ___a, BigInteger_t1537 * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(Mono.Math.BigInteger,Mono.Math.BigInteger)
 BigInteger_t1537 * ModulusRing_Pow_m7977 (ModulusRing_t1538 * __this, BigInteger_t1537 * ___a, BigInteger_t1537 * ___k, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(System.UInt32,Mono.Math.BigInteger)
 BigInteger_t1537 * ModulusRing_Pow_m7978 (ModulusRing_t1538 * __this, uint32_t ___b, BigInteger_t1537 * ___exp, MethodInfo* method) IL2CPP_METHOD_ATTR;
