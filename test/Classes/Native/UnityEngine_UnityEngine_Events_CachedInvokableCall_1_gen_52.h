﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.EventSystems.PointerInputModule>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_49.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.PointerInputModule>
struct CachedInvokableCall_1_t3148  : public InvokableCall_1_t3149
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.EventSystems.PointerInputModule>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
