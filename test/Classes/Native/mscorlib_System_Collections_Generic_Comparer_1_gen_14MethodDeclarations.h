﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.UI.Selectable>
struct Comparer_1_t3477;
// System.Object
struct Object_t;
// UnityEngine.UI.Selectable
struct Selectable_t272;

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UI.Selectable>::.ctor()
// System.Collections.Generic.Comparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_genMethodDeclarations.h"
#define Comparer_1__ctor_m19118(__this, method) (void)Comparer_1__ctor_m14754_gshared((Comparer_1_t2861 *)__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UI.Selectable>::.cctor()
#define Comparer_1__cctor_m19119(__this/* static, unused */, method) (void)Comparer_1__cctor_m14755_gshared((Object_t *)__this/* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UI.Selectable>::System.Collections.IComparer.Compare(System.Object,System.Object)
#define Comparer_1_System_Collections_IComparer_Compare_m19120(__this, ___x, ___y, method) (int32_t)Comparer_1_System_Collections_IComparer_Compare_m14756_gshared((Comparer_1_t2861 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UI.Selectable>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UI.Selectable>::get_Default()
#define Comparer_1_get_Default_m19121(__this/* static, unused */, method) (Comparer_1_t3477 *)Comparer_1_get_Default_m14757_gshared((Object_t *)__this/* static, unused */, method)
