﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.InvokableCall`1<System.Object>
struct InvokableCall_1_t2706;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t142;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t2707;
// System.Object[]
struct ObjectU5BU5D_t115;

// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(System.Object,System.Reflection.MethodInfo)
 void InvokableCall_1__ctor_m14011_gshared (InvokableCall_1_t2706 * __this, Object_t * ___target, MethodInfo_t142 * ___theFunction, MethodInfo* method);
#define InvokableCall_1__ctor_m14011(__this, ___target, ___theFunction, method) (void)InvokableCall_1__ctor_m14011_gshared((InvokableCall_1_t2706 *)__this, (Object_t *)___target, (MethodInfo_t142 *)___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
 void InvokableCall_1__ctor_m14012_gshared (InvokableCall_1_t2706 * __this, UnityAction_1_t2707 * ___callback, MethodInfo* method);
#define InvokableCall_1__ctor_m14012(__this, ___callback, method) (void)InvokableCall_1__ctor_m14012_gshared((InvokableCall_1_t2706 *)__this, (UnityAction_1_t2707 *)___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.Object>::Invoke(System.Object[])
 void InvokableCall_1_Invoke_m14013_gshared (InvokableCall_1_t2706 * __this, ObjectU5BU5D_t115* ___args, MethodInfo* method);
#define InvokableCall_1_Invoke_m14013(__this, ___args, method) (void)InvokableCall_1_Invoke_m14013_gshared((InvokableCall_1_t2706 *)__this, (ObjectU5BU5D_t115*)___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.Object>::Find(System.Object,System.Reflection.MethodInfo)
 bool InvokableCall_1_Find_m14014_gshared (InvokableCall_1_t2706 * __this, Object_t * ___targetObj, MethodInfo_t142 * ___method, MethodInfo* method);
#define InvokableCall_1_Find_m14014(__this, ___targetObj, ___method, method) (bool)InvokableCall_1_Find_m14014_gshared((InvokableCall_1_t2706 *)__this, (Object_t *)___targetObj, (MethodInfo_t142 *)___method, method)
