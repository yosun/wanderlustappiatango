﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SerializeField
struct SerializeField_t103;

// System.Void UnityEngine.SerializeField::.ctor()
 void SerializeField__ctor_m259 (SerializeField_t103 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
