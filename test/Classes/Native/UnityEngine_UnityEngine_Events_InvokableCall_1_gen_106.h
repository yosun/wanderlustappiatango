﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.MultiTargetAbstractBehaviour>
struct UnityAction_1_t4306;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetAbstractBehaviour>
struct InvokableCall_1_t4305  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.MultiTargetAbstractBehaviour>::Delegate
	UnityAction_1_t4306 * ___Delegate_0;
};
