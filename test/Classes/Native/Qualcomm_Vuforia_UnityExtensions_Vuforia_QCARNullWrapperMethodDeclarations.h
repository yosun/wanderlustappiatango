﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARNullWrapper
struct QCARNullWrapper_t706;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t418;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceInitCamera(System.Int32)
 int32_t QCARNullWrapper_CameraDeviceInitCamera_m3459 (QCARNullWrapper_t706 * __this, int32_t ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceDeinitCamera()
 int32_t QCARNullWrapper_CameraDeviceDeinitCamera_m3460 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceStartCamera()
 int32_t QCARNullWrapper_CameraDeviceStartCamera_m3461 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceStopCamera()
 int32_t QCARNullWrapper_CameraDeviceStopCamera_m3462 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceGetNumVideoModes()
 int32_t QCARNullWrapper_CameraDeviceGetNumVideoModes_m3463 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::CameraDeviceGetVideoMode(System.Int32,System.IntPtr)
 void QCARNullWrapper_CameraDeviceGetVideoMode_m3464 (QCARNullWrapper_t706 * __this, int32_t ___idx, IntPtr_t121 ___videoMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceSelectVideoMode(System.Int32)
 int32_t QCARNullWrapper_CameraDeviceSelectVideoMode_m3465 (QCARNullWrapper_t706 * __this, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceSetFlashTorchMode(System.Int32)
 int32_t QCARNullWrapper_CameraDeviceSetFlashTorchMode_m3466 (QCARNullWrapper_t706 * __this, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceSetFocusMode(System.Int32)
 int32_t QCARNullWrapper_CameraDeviceSetFocusMode_m3467 (QCARNullWrapper_t706 * __this, int32_t ___focusMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
 int32_t QCARNullWrapper_CameraDeviceSetCameraConfiguration_m3468 (QCARNullWrapper_t706 * __this, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarSetFrameFormat(System.Int32,System.Int32)
 int32_t QCARNullWrapper_QcarSetFrameFormat_m3469 (QCARNullWrapper_t706 * __this, int32_t ___format, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetExists(System.String,System.Int32)
 int32_t QCARNullWrapper_DataSetExists_m3470 (QCARNullWrapper_t706 * __this, String_t* ___relativePath, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetLoad(System.String,System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_DataSetLoad_m3471 (QCARNullWrapper_t706 * __this, String_t* ___relativePath, int32_t ___storageType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_DataSetGetNumTrackableType_m3472 (QCARNullWrapper_t706 * __this, int32_t ___trackableType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_DataSetGetTrackablesOfType_m3473 (QCARNullWrapper_t706 * __this, int32_t ___trackableType, IntPtr_t121 ___trackableDataArray, int32_t ___trackableDataArrayLength, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNullWrapper_DataSetGetTrackableName_m3474 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, StringBuilder_t418 * ___trackableName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetCreateTrackable(System.IntPtr,System.IntPtr,System.Text.StringBuilder,System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_DataSetCreateTrackable_m3475 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, IntPtr_t121 ___trackableSourcePtr, StringBuilder_t418 * ___trackableName, int32_t ___nameMaxLength, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetDestroyTrackable(System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_DataSetDestroyTrackable_m3476 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::DataSetHasReachedTrackableLimit(System.IntPtr)
 int32_t QCARNullWrapper_DataSetHasReachedTrackableLimit_m3477 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::GetCameraThreadID()
 int32_t QCARNullWrapper_GetCameraThreadID_m3478 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetBuilderBuild(System.String,System.Single)
 int32_t QCARNullWrapper_ImageTargetBuilderBuild_m3479 (QCARNullWrapper_t706 * __this, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::FrameCounterGetBenchmarkingData(System.IntPtr,System.Boolean)
 void QCARNullWrapper_FrameCounterGetBenchmarkingData_m3480 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___benchmarkingData, bool ___isStereoRendering, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::ImageTargetBuilderStartScan()
 void QCARNullWrapper_ImageTargetBuilderStartScan_m3481 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::ImageTargetBuilderStopScan()
 void QCARNullWrapper_ImageTargetBuilderStopScan_m3482 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetBuilderGetFrameQuality()
 int32_t QCARNullWrapper_ImageTargetBuilderGetFrameQuality_m3483 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::ImageTargetBuilderGetTrackableSource()
 IntPtr_t121 QCARNullWrapper_ImageTargetBuilderGetTrackableSource_m3484 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNullWrapper_ImageTargetCreateVirtualButton_m3485 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
 int32_t QCARNullWrapper_ImageTargetDestroyVirtualButton_m3486 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::VirtualButtonGetId(System.IntPtr,System.String,System.String)
 int32_t QCARNullWrapper_VirtualButtonGetId_m3487 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetGetNumVirtualButtons(System.IntPtr,System.String)
 int32_t QCARNullWrapper_ImageTargetGetNumVirtualButtons_m3488 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
 int32_t QCARNullWrapper_ImageTargetGetVirtualButtons_m3489 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___virtualButtonDataArray, IntPtr_t121 ___rectangleDataArray, int32_t ___virtualButtonDataArrayLength, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ImageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNullWrapper_ImageTargetGetVirtualButtonName_m3490 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, int32_t ___idx, StringBuilder_t418 * ___vbName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNullWrapper_CylinderTargetGetDimensions_m3491 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___dimensions, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
 int32_t QCARNullWrapper_CylinderTargetSetSideLength_m3492 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___sideLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CylinderTargetSetTopDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNullWrapper_CylinderTargetSetTopDiameter_m3493 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___topDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::CylinderTargetSetBottomDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNullWrapper_CylinderTargetSetBottomDiameter_m3494 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___bottomDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNullWrapper_ObjectTargetSetSize_m3495 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNullWrapper_ObjectTargetGetSize_m3496 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerStart()
 int32_t QCARNullWrapper_ObjectTrackerStart_m3497 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::ObjectTrackerStop()
 void QCARNullWrapper_ObjectTrackerStop_m3498 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::ObjectTrackerCreateDataSet()
 IntPtr_t121 QCARNullWrapper_ObjectTrackerCreateDataSet_m3499 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerDestroyDataSet(System.IntPtr)
 int32_t QCARNullWrapper_ObjectTrackerDestroyDataSet_m3500 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerActivateDataSet(System.IntPtr)
 int32_t QCARNullWrapper_ObjectTrackerActivateDataSet_m3501 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerDeactivateDataSet(System.IntPtr)
 int32_t QCARNullWrapper_ObjectTrackerDeactivateDataSet_m3502 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerPersistExtendedTracking(System.Int32)
 int32_t QCARNullWrapper_ObjectTrackerPersistExtendedTracking_m3503 (QCARNullWrapper_t706 * __this, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ObjectTrackerResetExtendedTracking()
 int32_t QCARNullWrapper_ObjectTrackerResetExtendedTracking_m3504 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::MarkerTrackerStart()
 int32_t QCARNullWrapper_MarkerTrackerStart_m3505 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::MarkerTrackerStop()
 void QCARNullWrapper_MarkerTrackerStop_m3506 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::OnSurfaceChanged(System.Int32,System.Int32)
 void QCARNullWrapper_OnSurfaceChanged_m3507 (QCARNullWrapper_t706 * __this, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::OnPause()
 void QCARNullWrapper_OnPause_m3508 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::OnResume()
 void QCARNullWrapper_OnResume_m3509 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::HasSurfaceBeenRecreated()
 bool QCARNullWrapper_HasSurfaceBeenRecreated_m3510 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::MarkerSetSize(System.Int32,System.Single)
 int32_t QCARNullWrapper_MarkerSetSize_m3511 (QCARNullWrapper_t706 * __this, int32_t ___trackableIndex, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::MarkerTrackerCreateMarker(System.Int32,System.String,System.Single)
 int32_t QCARNullWrapper_MarkerTrackerCreateMarker_m3512 (QCARNullWrapper_t706 * __this, int32_t ___id, String_t* ___trackableName, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::MarkerTrackerDestroyMarker(System.Int32)
 int32_t QCARNullWrapper_MarkerTrackerDestroyMarker_m3513 (QCARNullWrapper_t706 * __this, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::InitPlatformNative()
 void QCARNullWrapper_InitPlatformNative_m3514 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::InitFrameState(System.IntPtr)
 void QCARNullWrapper_InitFrameState_m3515 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::DeinitFrameState(System.IntPtr)
 void QCARNullWrapper_DeinitFrameState_m3516 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::UpdateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_UpdateQCAR_m3517 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___imageHeaderDataArray, int32_t ___imageHeaderArrayLength, IntPtr_t121 ___frameIndex, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::RendererEnd()
 void QCARNullWrapper_RendererEnd_m3518 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32)
 int32_t QCARNullWrapper_QcarGetBufferSize_m3519 (QCARNullWrapper_t706 * __this, int32_t ___width, int32_t ___height, int32_t ___format, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::QcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 void QCARNullWrapper_QcarAddCameraFrame_m3520 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___pixels, int32_t ___width, int32_t ___height, int32_t ___format, int32_t ___stride, int32_t ___frameIdx, int32_t ___flipHorizontally, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::RendererSetVideoBackgroundCfg(System.IntPtr)
 void QCARNullWrapper_RendererSetVideoBackgroundCfg_m3521 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::RendererGetVideoBackgroundCfg(System.IntPtr)
 void QCARNullWrapper_RendererGetVideoBackgroundCfg_m3522 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::RendererGetVideoBackgroundTextureInfo(System.IntPtr)
 void QCARNullWrapper_RendererGetVideoBackgroundTextureInfo_m3523 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___texInfo, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::RendererSetVideoBackgroundTextureID(System.Int32)
 int32_t QCARNullWrapper_RendererSetVideoBackgroundTextureID_m3524 (QCARNullWrapper_t706 * __this, int32_t ___textureID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::RendererIsVideoBackgroundTextureInfoAvailable()
 int32_t QCARNullWrapper_RendererIsVideoBackgroundTextureInfoAvailable_m3525 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarSetHint(System.Int32,System.Int32)
 int32_t QCARNullWrapper_QcarSetHint_m3526 (QCARNullWrapper_t706 * __this, int32_t ___hint, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::GetProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_GetProjectionGL_m3527 (QCARNullWrapper_t706 * __this, float ___nearClip, float ___farClip, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::SetApplicationEnvironment(System.Int32,System.Int32,System.Int32)
 void QCARNullWrapper_SetApplicationEnvironment_m3528 (QCARNullWrapper_t706 * __this, int32_t ___major, int32_t ___minor, int32_t ___change, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::SetStateBufferSize(System.Int32)
 void QCARNullWrapper_SetStateBufferSize_m3529 (QCARNullWrapper_t706 * __this, int32_t ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::SmartTerrainTrackerStart()
 int32_t QCARNullWrapper_SmartTerrainTrackerStart_m3530 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::SmartTerrainTrackerStop()
 void QCARNullWrapper_SmartTerrainTrackerStop_m3531 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainTrackerSetScaleToMillimeter(System.Single)
 bool QCARNullWrapper_SmartTerrainTrackerSetScaleToMillimeter_m3532 (QCARNullWrapper_t706 * __this, float ___scaleFactor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainTrackerInitBuilder()
 bool QCARNullWrapper_SmartTerrainTrackerInitBuilder_m3533 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainTrackerDeinitBuilder()
 bool QCARNullWrapper_SmartTerrainTrackerDeinitBuilder_m3534 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::SmartTerrainBuilderCreateReconstructionFromTarget()
 IntPtr_t121 QCARNullWrapper_SmartTerrainBuilderCreateReconstructionFromTarget_m3535 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::SmartTerrainBuilderCreateReconstructionFromEnvironment()
 IntPtr_t121 QCARNullWrapper_SmartTerrainBuilderCreateReconstructionFromEnvironment_m3536 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainBuilderAddReconstruction(System.IntPtr)
 bool QCARNullWrapper_SmartTerrainBuilderAddReconstruction_m3537 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainBuilderRemoveReconstruction(System.IntPtr)
 bool QCARNullWrapper_SmartTerrainBuilderRemoveReconstruction_m3538 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::SmartTerrainBuilderDestroyReconstruction(System.IntPtr)
 bool QCARNullWrapper_SmartTerrainBuilderDestroyReconstruction_m3539 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionStart(System.IntPtr)
 bool QCARNullWrapper_ReconstructionStart_m3540 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionStop(System.IntPtr)
 bool QCARNullWrapper_ReconstructionStop_m3541 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionIsReconstructing(System.IntPtr)
 bool QCARNullWrapper_ReconstructionIsReconstructing_m3542 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionReset(System.IntPtr)
 bool QCARNullWrapper_ReconstructionReset_m3543 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::ReconstructionSetNavMeshPadding(System.IntPtr,System.Single)
 void QCARNullWrapper_ReconstructionSetNavMeshPadding_m3544 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___reconstruction, float ___padding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
 bool QCARNullWrapper_ReconstructionFromTargetSetInitializationTarget_m3545 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___reconstruction, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, IntPtr_t121 ___occluderMin, IntPtr_t121 ___occluderMax, IntPtr_t121 ___offsetToOccluder, IntPtr_t121 ___rotationAxisToOccluder, float ___rotationAngleToOccluder, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::ReconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
 bool QCARNullWrapper_ReconstructionSetMaximumArea_m3546 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___reconstruction, IntPtr_t121 ___maximumArea, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::ReconstructioFromEnvironmentGetReconstructionState(System.IntPtr)
 int32_t QCARNullWrapper_ReconstructioFromEnvironmentGetReconstructionState_m3547 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderStartInit(System.String,System.String)
 int32_t QCARNullWrapper_TargetFinderStartInit_m3548 (QCARNullWrapper_t706 * __this, String_t* ___userKey, String_t* ___secretKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderGetInitState()
 int32_t QCARNullWrapper_TargetFinderGetInitState_m3549 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderDeinit()
 int32_t QCARNullWrapper_TargetFinderDeinit_m3550 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderStartRecognition()
 int32_t QCARNullWrapper_TargetFinderStartRecognition_m3551 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderStop()
 int32_t QCARNullWrapper_TargetFinderStop_m3552 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderSetUIScanlineColor(System.Single,System.Single,System.Single)
 void QCARNullWrapper_TargetFinderSetUIScanlineColor_m3553 (QCARNullWrapper_t706 * __this, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderSetUIPointColor(System.Single,System.Single,System.Single)
 void QCARNullWrapper_TargetFinderSetUIPointColor_m3554 (QCARNullWrapper_t706 * __this, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderUpdate(System.IntPtr)
 void QCARNullWrapper_TargetFinderUpdate_m3555 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___targetFinderState, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderGetResults(System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_TargetFinderGetResults_m3556 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___searchResultArray, int32_t ___searchResultArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TargetFinderEnableTracking(System.IntPtr,System.IntPtr)
 int32_t QCARNullWrapper_TargetFinderEnableTracking_m3557 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___searchResult, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderGetImageTargets(System.IntPtr,System.Int32)
 void QCARNullWrapper_TargetFinderGetImageTargets_m3558 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___trackableIdArray, int32_t ___trackableIdArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TargetFinderClearTrackables()
 void QCARNullWrapper_TargetFinderClearTrackables_m3559 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TextTrackerStart()
 int32_t QCARNullWrapper_TextTrackerStart_m3560 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TextTrackerStop()
 void QCARNullWrapper_TextTrackerStop_m3561 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TextTrackerSetRegionOfInterest(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 int32_t QCARNullWrapper_TextTrackerSetRegionOfInterest_m3562 (QCARNullWrapper_t706 * __this, int32_t ___detectionLeftTopX, int32_t ___detectionLeftTopY, int32_t ___detectionRightBottomX, int32_t ___detectionRightBottomY, int32_t ___trackingLeftTopX, int32_t ___trackingLeftTopY, int32_t ___trackingRightBottomX, int32_t ___trackingRightBottomY, int32_t ___upDirection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::TextTrackerGetRegionOfInterest(System.IntPtr,System.IntPtr)
 void QCARNullWrapper_TextTrackerGetRegionOfInterest_m3563 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___detectionROI, IntPtr_t121 ___trackingROI, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListLoadWordList(System.String,System.Int32)
 int32_t QCARNullWrapper_WordListLoadWordList_m3564 (QCARNullWrapper_t706 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListAddWordsFromFile(System.String,System.Int32)
 int32_t QCARNullWrapper_WordListAddWordsFromFile_m3565 (QCARNullWrapper_t706 * __this, String_t* ___path, int32_t ___storagetType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListAddWordU(System.IntPtr)
 int32_t QCARNullWrapper_WordListAddWordU_m3566 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListRemoveWordU(System.IntPtr)
 int32_t QCARNullWrapper_WordListRemoveWordU_m3567 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListContainsWordU(System.IntPtr)
 int32_t QCARNullWrapper_WordListContainsWordU_m3568 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListUnloadAllLists()
 int32_t QCARNullWrapper_WordListUnloadAllLists_m3569 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListSetFilterMode(System.Int32)
 int32_t QCARNullWrapper_WordListSetFilterMode_m3570 (QCARNullWrapper_t706 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListGetFilterMode()
 int32_t QCARNullWrapper_WordListGetFilterMode_m3571 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListLoadFilterList(System.String,System.Int32)
 int32_t QCARNullWrapper_WordListLoadFilterList_m3572 (QCARNullWrapper_t706 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListAddWordToFilterListU(System.IntPtr)
 int32_t QCARNullWrapper_WordListAddWordToFilterListU_m3573 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListRemoveWordFromFilterListU(System.IntPtr)
 int32_t QCARNullWrapper_WordListRemoveWordFromFilterListU_m3574 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListClearFilterList()
 int32_t QCARNullWrapper_WordListClearFilterList_m3575 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordListGetFilterListWordCount()
 int32_t QCARNullWrapper_WordListGetFilterListWordCount_m3576 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::WordListGetFilterListWordU(System.Int32)
 IntPtr_t121 QCARNullWrapper_WordListGetFilterListWordU_m3577 (QCARNullWrapper_t706 * __this, int32_t ___i, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordGetLetterMask(System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_WordGetLetterMask_m3578 (QCARNullWrapper_t706 * __this, int32_t ___wordID, IntPtr_t121 ___letterMaskImage, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::WordGetLetterBoundingBoxes(System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_WordGetLetterBoundingBoxes_m3579 (QCARNullWrapper_t706 * __this, int32_t ___wordID, IntPtr_t121 ___letterBoundingBoxes, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TrackerManagerInitTracker(System.Int32)
 int32_t QCARNullWrapper_TrackerManagerInitTracker_m3580 (QCARNullWrapper_t706 * __this, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::TrackerManagerDeinitTracker(System.Int32)
 int32_t QCARNullWrapper_TrackerManagerDeinitTracker_m3581 (QCARNullWrapper_t706 * __this, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::VirtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNullWrapper_VirtualButtonSetEnabled_m3582 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::VirtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNullWrapper_VirtualButtonSetSensitivity_m3583 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___sensitivity, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::VirtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNullWrapper_VirtualButtonSetAreaRectangle_m3584 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarInit(System.String)
 int32_t QCARNullWrapper_QcarInit_m3585 (QCARNullWrapper_t706 * __this, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::QcarDeinit()
 int32_t QCARNullWrapper_QcarDeinit_m3586 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::StartExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_StartExtendedTracking_m3587 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::StopExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_StopExtendedTracking_m3588 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsSupportedDeviceDetected()
 bool QCARNullWrapper_EyewearIsSupportedDeviceDetected_m3589 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsSeeThru()
 bool QCARNullWrapper_EyewearIsSeeThru_m3590 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearGetScreenOrientation()
 int32_t QCARNullWrapper_EyewearGetScreenOrientation_m3591 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsStereoCapable()
 bool QCARNullWrapper_EyewearIsStereoCapable_m3592 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsStereoEnabled()
 bool QCARNullWrapper_EyewearIsStereoEnabled_m3593 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearIsStereoGLOnly()
 bool QCARNullWrapper_EyewearIsStereoGLOnly_m3594 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearSetStereo(System.Boolean)
 bool QCARNullWrapper_EyewearSetStereo_m3595 (QCARNullWrapper_t706 * __this, bool ___enable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearGetDefaultSceneScale(System.IntPtr)
 int32_t QCARNullWrapper_EyewearGetDefaultSceneScale_m3596 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNullWrapper_EyewearGetProjectionMatrix_m3597 (QCARNullWrapper_t706 * __this, int32_t ___eyeID, int32_t ___profileID, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearCPMGetMaxCount()
 int32_t QCARNullWrapper_EyewearCPMGetMaxCount_m3598 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearCPMGetUsedCount()
 int32_t QCARNullWrapper_EyewearCPMGetUsedCount_m3599 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMIsProfileUsed(System.Int32)
 bool QCARNullWrapper_EyewearCPMIsProfileUsed_m3600 (QCARNullWrapper_t706 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearCPMGetActiveProfile()
 int32_t QCARNullWrapper_EyewearCPMGetActiveProfile_m3601 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMSetActiveProfile(System.Int32)
 bool QCARNullWrapper_EyewearCPMSetActiveProfile_m3602 (QCARNullWrapper_t706 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNullWrapper::EyewearCPMGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 int32_t QCARNullWrapper_EyewearCPMGetProjectionMatrix_m3603 (QCARNullWrapper_t706 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMSetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 bool QCARNullWrapper_EyewearCPMSetProjectionMatrix_m3604 (QCARNullWrapper_t706 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNullWrapper::EyewearCPMGetProfileName(System.Int32)
 IntPtr_t121 QCARNullWrapper_EyewearCPMGetProfileName_m3605 (QCARNullWrapper_t706 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMSetProfileName(System.Int32,System.IntPtr)
 bool QCARNullWrapper_EyewearCPMSetProfileName_m3606 (QCARNullWrapper_t706 * __this, int32_t ___profileID, IntPtr_t121 ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearCPMClearProfile(System.Int32)
 bool QCARNullWrapper_EyewearCPMClearProfile_m3607 (QCARNullWrapper_t706 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearUserCalibratorInit(System.Int32,System.Int32,System.Int32,System.Int32)
 bool QCARNullWrapper_EyewearUserCalibratorInit_m3608 (QCARNullWrapper_t706 * __this, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNullWrapper::EyewearUserCalibratorGetMinScaleHint()
 float QCARNullWrapper_EyewearUserCalibratorGetMinScaleHint_m3609 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNullWrapper::EyewearUserCalibratorGetMaxScaleHint()
 float QCARNullWrapper_EyewearUserCalibratorGetMaxScaleHint_m3610 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearUserCalibratorIsStereoStretched()
 bool QCARNullWrapper_EyewearUserCalibratorIsStereoStretched_m3611 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNullWrapper::EyewearUserCalibratorGetProjectionMatrix(System.IntPtr,System.Int32,System.IntPtr)
 bool QCARNullWrapper_EyewearUserCalibratorGetProjectionMatrix_m3612 (QCARNullWrapper_t706 * __this, IntPtr_t121 ___readingsArray, int32_t ___numReadings, IntPtr_t121 ___calibrationResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNullWrapper::.ctor()
 void QCARNullWrapper__ctor_m3613 (QCARNullWrapper_t706 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
