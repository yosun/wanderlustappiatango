﻿#pragma once
#include <stdint.h>
// Vuforia.IVirtualButtonEventHandler
struct IVirtualButtonEventHandler_t771;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.IVirtualButtonEventHandler>
struct Predicate_1_t4411  : public MulticastDelegate_t325
{
};
