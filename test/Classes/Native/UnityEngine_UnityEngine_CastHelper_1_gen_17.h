﻿#pragma once
#include <stdint.h>
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t72;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<Vuforia.ReconstructionAbstractBehaviour>
struct CastHelper_1_t3707 
{
	// T UnityEngine.CastHelper`1<Vuforia.ReconstructionAbstractBehaviour>::t
	ReconstructionAbstractBehaviour_t72 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<Vuforia.ReconstructionAbstractBehaviour>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
