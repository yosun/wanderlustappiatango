﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.MonoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_136.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>
struct CachedInvokableCall_1_t4608  : public InvokableCall_1_t4609
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.MonoBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
