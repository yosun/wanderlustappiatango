﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACSHA384
struct HMACSHA384_t2099;
// System.Byte[]
struct ByteU5BU5D_t609;

// System.Void System.Security.Cryptography.HMACSHA384::.ctor()
 void HMACSHA384__ctor_m11923 (HMACSHA384_t2099 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::.ctor(System.Byte[])
 void HMACSHA384__ctor_m11924 (HMACSHA384_t2099 * __this, ByteU5BU5D_t609* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::.cctor()
 void HMACSHA384__cctor_m11925 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA384::set_ProduceLegacyHmacValues(System.Boolean)
 void HMACSHA384_set_ProduceLegacyHmacValues_m11926 (HMACSHA384_t2099 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
