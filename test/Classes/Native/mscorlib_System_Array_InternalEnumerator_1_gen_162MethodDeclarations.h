﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.PointerEventData/InputButton>
struct InternalEnumerator_1_t3141;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.EventSystems.PointerEventData/InputButton
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.PointerEventData/InputButton>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m16666 (InternalEnumerator_1_t3141 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.PointerEventData/InputButton>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16667 (InternalEnumerator_1_t3141 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.PointerEventData/InputButton>::Dispose()
 void InternalEnumerator_1_Dispose_m16668 (InternalEnumerator_1_t3141 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.PointerEventData/InputButton>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m16669 (InternalEnumerator_1_t3141 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.PointerEventData/InputButton>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m16670 (InternalEnumerator_1_t3141 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
