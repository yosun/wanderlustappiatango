﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>
struct List_1_t665;
// System.Object
struct Object_t;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t581;
// System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackable>
struct IEnumerable_1_t667;
// System.Collections.Generic.IEnumerator`1<Vuforia.SmartTerrainTrackable>
struct IEnumerator_1_t3914;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.ICollection`1<Vuforia.SmartTerrainTrackable>
struct ICollection_1_t3915;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.SmartTerrainTrackable>
struct ReadOnlyCollection_1_t3916;
// Vuforia.SmartTerrainTrackable[]
struct SmartTerrainTrackableU5BU5D_t3913;
// System.Predicate`1<Vuforia.SmartTerrainTrackable>
struct Predicate_1_t3917;
// System.Comparison`1<Vuforia.SmartTerrainTrackable>
struct Comparison_1_t3918;
// System.Collections.Generic.List`1/Enumerator<Vuforia.SmartTerrainTrackable>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_47.h"

// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
#define List_1__ctor_m4901(__this, method) (void)List_1__ctor_m14543_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m22516(__this, ___collection, method) (void)List_1__ctor_m14545_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::.ctor(System.Int32)
#define List_1__ctor_m22517(__this, ___capacity, method) (void)List_1__ctor_m14547_gshared((List_1_t150 *)__this, (int32_t)___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::.cctor()
#define List_1__cctor_m22518(__this/* static, unused */, method) (void)List_1__cctor_m14549_gshared((Object_t *)__this/* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m22519(__this, method) (Object_t*)List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14551_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m22520(__this, ___array, ___arrayIndex, method) (void)List_1_System_Collections_ICollection_CopyTo_m14553_gshared((List_1_t150 *)__this, (Array_t *)___array, (int32_t)___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m22521(__this, method) (Object_t *)List_1_System_Collections_IEnumerable_GetEnumerator_m14555_gshared((List_1_t150 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m22522(__this, ___item, method) (int32_t)List_1_System_Collections_IList_Add_m14557_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m22523(__this, ___item, method) (bool)List_1_System_Collections_IList_Contains_m14559_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m22524(__this, ___item, method) (int32_t)List_1_System_Collections_IList_IndexOf_m14561_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m22525(__this, ___index, ___item, method) (void)List_1_System_Collections_IList_Insert_m14563_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m22526(__this, ___item, method) (void)List_1_System_Collections_IList_Remove_m14565_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22527(__this, method) (bool)List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14567_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m22528(__this, method) (bool)List_1_System_Collections_ICollection_get_IsSynchronized_m14569_gshared((List_1_t150 *)__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m22529(__this, method) (Object_t *)List_1_System_Collections_ICollection_get_SyncRoot_m14571_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m22530(__this, method) (bool)List_1_System_Collections_IList_get_IsFixedSize_m14573_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m22531(__this, method) (bool)List_1_System_Collections_IList_get_IsReadOnly_m14575_gshared((List_1_t150 *)__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m22532(__this, ___index, method) (Object_t *)List_1_System_Collections_IList_get_Item_m14577_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m22533(__this, ___index, ___value, method) (void)List_1_System_Collections_IList_set_Item_m14579_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Add(T)
#define List_1_Add_m4903(__this, ___item, method) (void)List_1_Add_m14581_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m22534(__this, ___newCount, method) (void)List_1_GrowIfNeeded_m14583_gshared((List_1_t150 *)__this, (int32_t)___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m22535(__this, ___collection, method) (void)List_1_AddCollection_m14585_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m22536(__this, ___enumerable, method) (void)List_1_AddEnumerable_m14587_gshared((List_1_t150 *)__this, (Object_t*)___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m22537(__this, ___collection, method) (void)List_1_AddRange_m14588_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::AsReadOnly()
#define List_1_AsReadOnly_m22538(__this, method) (ReadOnlyCollection_1_t3916 *)List_1_AsReadOnly_m14590_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Clear()
#define List_1_Clear_m22539(__this, method) (void)List_1_Clear_m14592_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Contains(T)
#define List_1_Contains_m4902(__this, ___item, method) (bool)List_1_Contains_m14594_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m22540(__this, ___array, ___arrayIndex, method) (void)List_1_CopyTo_m14596_gshared((List_1_t150 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Find(System.Predicate`1<T>)
#define List_1_Find_m22541(__this, ___match, method) (Object_t *)List_1_Find_m14598_gshared((List_1_t150 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m22542(__this/* static, unused */, ___match, method) (void)List_1_CheckMatch_m14600_gshared((Object_t *)__this/* static, unused */, (Predicate_1_t2845 *)___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m22543(__this, ___startIndex, ___count, ___match, method) (int32_t)List_1_GetIndex_m14602_gshared((List_1_t150 *)__this, (int32_t)___startIndex, (int32_t)___count, (Predicate_1_t2845 *)___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::GetEnumerator()
 Enumerator_t3919  List_1_GetEnumerator_m22544 (List_1_t665 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::IndexOf(T)
#define List_1_IndexOf_m22545(__this, ___item, method) (int32_t)List_1_IndexOf_m14604_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m22546(__this, ___start, ___delta, method) (void)List_1_Shift_m14606_gshared((List_1_t150 *)__this, (int32_t)___start, (int32_t)___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m22547(__this, ___index, method) (void)List_1_CheckIndex_m14608_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Insert(System.Int32,T)
#define List_1_Insert_m22548(__this, ___index, ___item, method) (void)List_1_Insert_m14610_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m22549(__this, ___collection, method) (void)List_1_CheckCollection_m14612_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Remove(T)
#define List_1_Remove_m4904(__this, ___item, method) (bool)List_1_Remove_m14614_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m22550(__this, ___match, method) (int32_t)List_1_RemoveAll_m14616_gshared((List_1_t150 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m22551(__this, ___index, method) (void)List_1_RemoveAt_m14618_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Reverse()
#define List_1_Reverse_m22552(__this, method) (void)List_1_Reverse_m14620_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Sort()
#define List_1_Sort_m22553(__this, method) (void)List_1_Sort_m14622_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m22554(__this, ___comparison, method) (void)List_1_Sort_m14624_gshared((List_1_t150 *)__this, (Comparison_1_t2846 *)___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::ToArray()
#define List_1_ToArray_m22555(__this, method) (SmartTerrainTrackableU5BU5D_t3913*)List_1_ToArray_m14626_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::TrimExcess()
#define List_1_TrimExcess_m22556(__this, method) (void)List_1_TrimExcess_m14628_gshared((List_1_t150 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::get_Capacity()
#define List_1_get_Capacity_m22557(__this, method) (int32_t)List_1_get_Capacity_m14630_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m22558(__this, ___value, method) (void)List_1_set_Capacity_m14632_gshared((List_1_t150 *)__this, (int32_t)___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::get_Count()
#define List_1_get_Count_m22559(__this, method) (int32_t)List_1_get_Count_m14634_gshared((List_1_t150 *)__this, method)
// T System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::get_Item(System.Int32)
#define List_1_get_Item_m22560(__this, ___index, method) (Object_t *)List_1_get_Item_m14636_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.SmartTerrainTrackable>::set_Item(System.Int32,T)
#define List_1_set_Item_m22561(__this, ___index, ___value, method) (void)List_1_set_Item_m14638_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___value, method)
