﻿#pragma once
#include <stdint.h>
// Vuforia.ITextRecoEventHandler
struct ITextRecoEventHandler_t765;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.ITextRecoEventHandler>
struct Comparison_1_t4353  : public MulticastDelegate_t325
{
};
