﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ComponentFactoryStarterBehaviour
struct ComponentFactoryStarterBehaviour_t52;

// System.Void Vuforia.ComponentFactoryStarterBehaviour::.ctor()
 void ComponentFactoryStarterBehaviour__ctor_m130 (ComponentFactoryStarterBehaviour_t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ComponentFactoryStarterBehaviour::Awake()
 void ComponentFactoryStarterBehaviour_Awake_m131 (ComponentFactoryStarterBehaviour_t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ComponentFactoryStarterBehaviour::SetBehaviourComponentFactory()
 void ComponentFactoryStarterBehaviour_SetBehaviourComponentFactory_m132 (ComponentFactoryStarterBehaviour_t52 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
