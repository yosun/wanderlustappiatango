﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t1086;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t1087;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t1084;

// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
 void PersistentCallGroup__ctor_m6371 (PersistentCallGroup_t1086 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
 void PersistentCallGroup_Initialize_m6372 (PersistentCallGroup_t1086 * __this, InvokableCallList_t1087 * ___invokableList, UnityEventBase_t1084 * ___unityEventBase, MethodInfo* method) IL2CPP_METHOD_ATTR;
