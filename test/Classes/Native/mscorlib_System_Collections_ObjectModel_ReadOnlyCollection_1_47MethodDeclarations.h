﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>
struct ReadOnlyCollection_1_t4371;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t768;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<Vuforia.IUserDefinedTargetEventHandler>
struct IList_1_t4375;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// Vuforia.IUserDefinedTargetEventHandler[]
struct IUserDefinedTargetEventHandlerU5BU5D_t4367;
// System.Collections.Generic.IEnumerator`1<Vuforia.IUserDefinedTargetEventHandler>
struct IEnumerator_1_t4369;

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::.ctor(System.Collections.Generic.IList`1<T>)
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_0MethodDeclarations.h"
#define ReadOnlyCollection_1__ctor_m26784(__this, ___list, method) (void)ReadOnlyCollection_1__ctor_m14646_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t*)___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26785(__this, ___item, method) (void)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14647_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m26786(__this, method) (void)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14648_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m26787(__this, ___index, ___item, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14649_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m26788(__this, ___item, method) (bool)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14650_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m26789(__this, ___index, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14651_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m26790(__this, ___index, method) (Object_t *)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14652_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m26791(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14653_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26792(__this, method) (bool)ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14654_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m26793(__this, ___array, ___index, method) (void)ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14655_gshared((ReadOnlyCollection_1_t2849 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m26794(__this, method) (Object_t *)ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14656_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m26795(__this, ___value, method) (int32_t)ReadOnlyCollection_1_System_Collections_IList_Add_m14657_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m26796(__this, method) (void)ReadOnlyCollection_1_System_Collections_IList_Clear_m14658_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m26797(__this, ___value, method) (bool)ReadOnlyCollection_1_System_Collections_IList_Contains_m14659_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m26798(__this, ___value, method) (int32_t)ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14660_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m26799(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_Insert_m14661_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m26800(__this, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_Remove_m14662_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m26801(__this, ___index, method) (void)ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14663_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m26802(__this, method) (bool)ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14664_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m26803(__this, method) (Object_t *)ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14665_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m26804(__this, method) (bool)ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14666_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m26805(__this, method) (bool)ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14667_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m26806(__this, ___index, method) (Object_t *)ReadOnlyCollection_1_System_Collections_IList_get_Item_m14668_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m26807(__this, ___index, ___value, method) (void)ReadOnlyCollection_1_System_Collections_IList_set_Item_m14669_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::Contains(T)
#define ReadOnlyCollection_1_Contains_m26808(__this, ___value, method) (bool)ReadOnlyCollection_1_Contains_m14670_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m26809(__this, ___array, ___index, method) (void)ReadOnlyCollection_1_CopyTo_m14671_gshared((ReadOnlyCollection_1_t2849 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m26810(__this, method) (Object_t*)ReadOnlyCollection_1_GetEnumerator_m14672_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m26811(__this, ___value, method) (int32_t)ReadOnlyCollection_1_IndexOf_m14673_gshared((ReadOnlyCollection_1_t2849 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::get_Count()
#define ReadOnlyCollection_1_get_Count_m26812(__this, method) (int32_t)ReadOnlyCollection_1_get_Count_m14674_gshared((ReadOnlyCollection_1_t2849 *)__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.IUserDefinedTargetEventHandler>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m26813(__this, ___index, method) (Object_t *)ReadOnlyCollection_1_get_Item_m14675_gshared((ReadOnlyCollection_1_t2849 *)__this, (int32_t)___index, method)
