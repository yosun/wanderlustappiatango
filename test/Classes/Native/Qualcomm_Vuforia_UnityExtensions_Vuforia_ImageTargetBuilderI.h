﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableSource
struct TrackableSource_t586;
// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder.h"
// Vuforia.ImageTargetBuilderImpl
struct ImageTargetBuilderImpl_t612  : public ImageTargetBuilder_t601
{
	// Vuforia.TrackableSource Vuforia.ImageTargetBuilderImpl::mTrackableSource
	TrackableSource_t586 * ___mTrackableSource_0;
};
