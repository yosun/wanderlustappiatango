﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>
struct KeyValuePair_2_t3393;
// UnityEngine.UI.Graphic
struct Graphic_t293;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m18589 (KeyValuePair_2_t3393 * __this, Graphic_t293 * ___key, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Key()
 Graphic_t293 * KeyValuePair_2_get_Key_m18590 (KeyValuePair_2_t3393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m18591 (KeyValuePair_2_t3393 * __this, Graphic_t293 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Value()
 int32_t KeyValuePair_2_get_Value_m18592 (KeyValuePair_2_t3393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m18593 (KeyValuePair_2_t3393 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::ToString()
 String_t* KeyValuePair_2_ToString_m18594 (KeyValuePair_2_t3393 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
