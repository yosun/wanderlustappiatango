﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Raycaster>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_4.h"
// UnityEngine.Events.CachedInvokableCall`1<Raycaster>
struct CachedInvokableCall_1_t2739  : public InvokableCall_1_t2740
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Raycaster>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
