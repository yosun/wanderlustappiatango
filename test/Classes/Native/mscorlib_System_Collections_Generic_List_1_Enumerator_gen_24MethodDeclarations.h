﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>
struct Enumerator_t1193;
// System.Object
struct Object_t;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t1083;
// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
struct List_1_t1085;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m29412(__this, ___l, method) (void)Enumerator__ctor_m14640_gshared((Enumerator_t2850 *)__this, (List_1_t150 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m29413(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14641_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::Dispose()
#define Enumerator_Dispose_m29414(__this, method) (void)Enumerator_Dispose_m14642_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::VerifyState()
#define Enumerator_VerifyState_m29415(__this, method) (void)Enumerator_VerifyState_m14643_gshared((Enumerator_t2850 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::MoveNext()
#define Enumerator_MoveNext_m6568(__this, method) (bool)Enumerator_MoveNext_m14644_gshared((Enumerator_t2850 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::get_Current()
#define Enumerator_get_Current_m6567(__this, method) (PersistentCall_t1083 *)Enumerator_get_Current_m14645_gshared((Enumerator_t2850 *)__this, method)
