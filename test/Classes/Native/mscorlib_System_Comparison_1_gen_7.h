﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t100;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.Component>
struct Comparison_1_t3048  : public MulticastDelegate_t325
{
};
