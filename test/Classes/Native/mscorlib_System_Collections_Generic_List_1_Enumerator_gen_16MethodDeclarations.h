﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>
struct Enumerator_t890;
// System.Object
struct Object_t;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t122;
// System.Collections.Generic.List`1<Vuforia.IVideoBackgroundEventHandler>
struct List_1_t757;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m26454(__this, ___l, method) (void)Enumerator__ctor_m14640_gshared((Enumerator_t2850 *)__this, (List_1_t150 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m26455(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14641_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::Dispose()
#define Enumerator_Dispose_m26456(__this, method) (void)Enumerator_Dispose_m14642_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::VerifyState()
#define Enumerator_VerifyState_m26457(__this, method) (void)Enumerator_VerifyState_m14643_gshared((Enumerator_t2850 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::MoveNext()
#define Enumerator_MoveNext_m5344(__this, method) (bool)Enumerator_MoveNext_m14644_gshared((Enumerator_t2850 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.IVideoBackgroundEventHandler>::get_Current()
#define Enumerator_get_Current_m5343(__this, method) (Object_t *)Enumerator_get_Current_m14645_gshared((Enumerator_t2850 *)__this, method)
