﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData
struct GcUserProfileData_t937;
// UnityEngine.SocialPlatforms.Impl.UserProfile
struct UserProfile_t1042;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t925;

// UnityEngine.SocialPlatforms.Impl.UserProfile UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::ToUserProfile()
 UserProfile_t1042 * GcUserProfileData_ToUserProfile_m6244 (GcUserProfileData_t937 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData::AddToArray(UnityEngine.SocialPlatforms.Impl.UserProfile[]&,System.Int32)
 void GcUserProfileData_AddToArray_m6245 (GcUserProfileData_t937 * __this, UserProfileU5BU5D_t925** ___array, int32_t ___number, MethodInfo* method) IL2CPP_METHOD_ATTR;
