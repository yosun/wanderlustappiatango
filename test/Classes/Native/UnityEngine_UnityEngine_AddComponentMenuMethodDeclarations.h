﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t419;
// System.String
struct String_t;

// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
 void AddComponentMenu__ctor_m1899 (AddComponentMenu_t419 * __this, String_t* ___menuName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
 void AddComponentMenu__ctor_m2071 (AddComponentMenu_t419 * __this, String_t* ___menuName, int32_t ___order, MethodInfo* method) IL2CPP_METHOD_ATTR;
