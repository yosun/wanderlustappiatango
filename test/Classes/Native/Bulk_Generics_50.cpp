﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_623.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo InternalEnumerator_1_t5006_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_623MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
// System.Void
#include "mscorlib_System_Void.h"
// System.Array
#include "mscorlib_System_Array.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo _ParameterBuilder_t2605_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1493_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m30616_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7651_MethodInfo;
extern MethodInfo Array_get_Length_m7656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_Tis_ParameterBuilder_t2605_m39733_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m31436_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m31436(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._ParameterBuilder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._ParameterBuilder>(System.Int32)
#define Array_InternalArray__get_Item_Tis_ParameterBuilder_t2605_m39733(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5006____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5006_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5006, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5006____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5006_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5006, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5006_FieldInfos[] =
{
	&InternalEnumerator_1_t5006____array_0_FieldInfo,
	&InternalEnumerator_1_t5006____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30613_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5006____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5006_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30613_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5006____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5006_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30616_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5006_PropertyInfos[] =
{
	&InternalEnumerator_1_t5006____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5006____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5006_InternalEnumerator_1__ctor_m30612_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30612_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30612_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5006_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5006_InternalEnumerator_1__ctor_m30612_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30612_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30613_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30613_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5006_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30613_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30614_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30614_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5006_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30614_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30615_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30615_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5006_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30615_GenericMethod/* genericMethod */

};
extern Il2CppType _ParameterBuilder_t2605_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30616_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ParameterBuilder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30616_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5006_il2cpp_TypeInfo/* declaring_type */
	, &_ParameterBuilder_t2605_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30616_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5006_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30612_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30613_MethodInfo,
	&InternalEnumerator_1_Dispose_m30614_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30615_MethodInfo,
	&InternalEnumerator_1_get_Current_m30616_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m30615_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30614_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5006_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30613_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30615_MethodInfo,
	&InternalEnumerator_1_Dispose_m30614_MethodInfo,
	&InternalEnumerator_1_get_Current_m30616_MethodInfo,
};
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
extern TypeInfo IEnumerator_1_t6977_il2cpp_TypeInfo;
static TypeInfo* InternalEnumerator_1_t5006_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6977_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5006_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6977_il2cpp_TypeInfo, 7},
};
extern TypeInfo _ParameterBuilder_t2605_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5006_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30616_MethodInfo/* Method Usage */,
	&_ParameterBuilder_t2605_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_Tis_ParameterBuilder_t2605_m39733_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5006_0_0_0;
extern Il2CppType InternalEnumerator_1_t5006_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t5006_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t5006_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5006_MethodInfos/* methods */
	, InternalEnumerator_1_t5006_PropertyInfos/* properties */
	, InternalEnumerator_1_t5006_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5006_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5006_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5006_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5006_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5006_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5006_1_0_0/* this_arg */
	, InternalEnumerator_1_t5006_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5006_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5006_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5006)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8918_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterBuilder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterBuilder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterBuilder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterBuilder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterBuilder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterBuilder>
extern MethodInfo IList_1_get_Item_m50309_MethodInfo;
extern MethodInfo IList_1_set_Item_m50310_MethodInfo;
static PropertyInfo IList_1_t8918____Item_PropertyInfo = 
{
	&IList_1_t8918_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50309_MethodInfo/* get */
	, &IList_1_set_Item_m50310_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8918_PropertyInfos[] =
{
	&IList_1_t8918____Item_PropertyInfo,
	NULL
};
extern Il2CppType _ParameterBuilder_t2605_0_0_0;
extern Il2CppType _ParameterBuilder_t2605_0_0_0;
static ParameterInfo IList_1_t8918_IList_1_IndexOf_m50311_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ParameterBuilder_t2605_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50311_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterBuilder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50311_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8918_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8918_IList_1_IndexOf_m50311_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50311_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _ParameterBuilder_t2605_0_0_0;
static ParameterInfo IList_1_t8918_IList_1_Insert_m50312_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &_ParameterBuilder_t2605_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50312_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterBuilder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50312_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8918_IList_1_Insert_m50312_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50312_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8918_IList_1_RemoveAt_m50313_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50313_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterBuilder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50313_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8918_IList_1_RemoveAt_m50313_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50313_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8918_IList_1_get_Item_m50309_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType _ParameterBuilder_t2605_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50309_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterBuilder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50309_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8918_il2cpp_TypeInfo/* declaring_type */
	, &_ParameterBuilder_t2605_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8918_IList_1_get_Item_m50309_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50309_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _ParameterBuilder_t2605_0_0_0;
static ParameterInfo IList_1_t8918_IList_1_set_Item_m50310_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &_ParameterBuilder_t2605_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50310_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ParameterBuilder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50310_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8918_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8918_IList_1_set_Item_m50310_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50310_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8918_MethodInfos[] =
{
	&IList_1_IndexOf_m50311_MethodInfo,
	&IList_1_Insert_m50312_MethodInfo,
	&IList_1_RemoveAt_m50313_MethodInfo,
	&IList_1_get_Item_m50309_MethodInfo,
	&IList_1_set_Item_m50310_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t8917_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8919_il2cpp_TypeInfo;
static TypeInfo* IList_1_t8918_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8917_il2cpp_TypeInfo,
	&IEnumerable_1_t8919_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8918_0_0_0;
extern Il2CppType IList_1_t8918_1_0_0;
struct IList_1_t8918;
extern Il2CppGenericClass IList_1_t8918_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8918_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8918_MethodInfos/* methods */
	, IList_1_t8918_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8918_il2cpp_TypeInfo/* element_class */
	, IList_1_t8918_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8918_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8918_0_0_0/* byval_arg */
	, &IList_1_t8918_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8918_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6979_il2cpp_TypeInfo;

// System.Reflection.Emit.GenericTypeParameterBuilder
#include "mscorlib_System_Reflection_Emit_GenericTypeParameterBuilder.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>
extern MethodInfo IEnumerator_1_get_Current_m50314_MethodInfo;
static PropertyInfo IEnumerator_1_t6979____Current_PropertyInfo = 
{
	&IEnumerator_1_t6979_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50314_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6979_PropertyInfos[] =
{
	&IEnumerator_1_t6979____Current_PropertyInfo,
	NULL
};
extern Il2CppType GenericTypeParameterBuilder_t1908_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50314_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50314_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6979_il2cpp_TypeInfo/* declaring_type */
	, &GenericTypeParameterBuilder_t1908_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50314_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6979_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50314_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6979_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6979_0_0_0;
extern Il2CppType IEnumerator_1_t6979_1_0_0;
struct IEnumerator_1_t6979;
extern Il2CppGenericClass IEnumerator_1_t6979_GenericClass;
TypeInfo IEnumerator_1_t6979_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6979_MethodInfos/* methods */
	, IEnumerator_1_t6979_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6979_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6979_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6979_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6979_0_0_0/* byval_arg */
	, &IEnumerator_1_t6979_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6979_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_624.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5007_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_624MethodDeclarations.h"

extern TypeInfo GenericTypeParameterBuilder_t1908_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30621_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGenericTypeParameterBuilder_t1908_m39744_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.GenericTypeParameterBuilder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.GenericTypeParameterBuilder>(System.Int32)
#define Array_InternalArray__get_Item_TisGenericTypeParameterBuilder_t1908_m39744(__this, p0, method) (GenericTypeParameterBuilder_t1908 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5007____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5007_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5007, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5007____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5007_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5007, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5007_FieldInfos[] =
{
	&InternalEnumerator_1_t5007____array_0_FieldInfo,
	&InternalEnumerator_1_t5007____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30618_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5007____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5007_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30618_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5007____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5007_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30621_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5007_PropertyInfos[] =
{
	&InternalEnumerator_1_t5007____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5007____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5007_InternalEnumerator_1__ctor_m30617_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30617_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30617_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5007_InternalEnumerator_1__ctor_m30617_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30617_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30618_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30618_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5007_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30618_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30619_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30619_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5007_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30619_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30620_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30620_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5007_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30620_GenericMethod/* genericMethod */

};
extern Il2CppType GenericTypeParameterBuilder_t1908_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30621_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.GenericTypeParameterBuilder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30621_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5007_il2cpp_TypeInfo/* declaring_type */
	, &GenericTypeParameterBuilder_t1908_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30621_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5007_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30617_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30618_MethodInfo,
	&InternalEnumerator_1_Dispose_m30619_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30620_MethodInfo,
	&InternalEnumerator_1_get_Current_m30621_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30620_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30619_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5007_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30618_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30620_MethodInfo,
	&InternalEnumerator_1_Dispose_m30619_MethodInfo,
	&InternalEnumerator_1_get_Current_m30621_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5007_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6979_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5007_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6979_il2cpp_TypeInfo, 7},
};
extern TypeInfo GenericTypeParameterBuilder_t1908_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5007_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30621_MethodInfo/* Method Usage */,
	&GenericTypeParameterBuilder_t1908_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisGenericTypeParameterBuilder_t1908_m39744_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5007_0_0_0;
extern Il2CppType InternalEnumerator_1_t5007_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5007_GenericClass;
TypeInfo InternalEnumerator_1_t5007_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5007_MethodInfos/* methods */
	, InternalEnumerator_1_t5007_PropertyInfos/* properties */
	, InternalEnumerator_1_t5007_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5007_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5007_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5007_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5007_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5007_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5007_1_0_0/* this_arg */
	, InternalEnumerator_1_t5007_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5007_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5007_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5007)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8920_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>
extern MethodInfo ICollection_1_get_Count_m50315_MethodInfo;
static PropertyInfo ICollection_1_t8920____Count_PropertyInfo = 
{
	&ICollection_1_t8920_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50315_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50316_MethodInfo;
static PropertyInfo ICollection_1_t8920____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8920_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50316_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8920_PropertyInfos[] =
{
	&ICollection_1_t8920____Count_PropertyInfo,
	&ICollection_1_t8920____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50315_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::get_Count()
MethodInfo ICollection_1_get_Count_m50315_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8920_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50315_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50316_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50316_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8920_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50316_GenericMethod/* genericMethod */

};
extern Il2CppType GenericTypeParameterBuilder_t1908_0_0_0;
extern Il2CppType GenericTypeParameterBuilder_t1908_0_0_0;
static ParameterInfo ICollection_1_t8920_ICollection_1_Add_m50317_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GenericTypeParameterBuilder_t1908_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50317_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::Add(T)
MethodInfo ICollection_1_Add_m50317_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8920_ICollection_1_Add_m50317_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50317_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50318_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::Clear()
MethodInfo ICollection_1_Clear_m50318_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50318_GenericMethod/* genericMethod */

};
extern Il2CppType GenericTypeParameterBuilder_t1908_0_0_0;
static ParameterInfo ICollection_1_t8920_ICollection_1_Contains_m50319_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GenericTypeParameterBuilder_t1908_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50319_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::Contains(T)
MethodInfo ICollection_1_Contains_m50319_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8920_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8920_ICollection_1_Contains_m50319_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50319_GenericMethod/* genericMethod */

};
extern Il2CppType GenericTypeParameterBuilderU5BU5D_t1909_0_0_0;
extern Il2CppType GenericTypeParameterBuilderU5BU5D_t1909_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8920_ICollection_1_CopyTo_m50320_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GenericTypeParameterBuilderU5BU5D_t1909_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50320_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50320_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8920_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8920_ICollection_1_CopyTo_m50320_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50320_GenericMethod/* genericMethod */

};
extern Il2CppType GenericTypeParameterBuilder_t1908_0_0_0;
static ParameterInfo ICollection_1_t8920_ICollection_1_Remove_m50321_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GenericTypeParameterBuilder_t1908_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50321_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.GenericTypeParameterBuilder>::Remove(T)
MethodInfo ICollection_1_Remove_m50321_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8920_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8920_ICollection_1_Remove_m50321_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50321_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8920_MethodInfos[] =
{
	&ICollection_1_get_Count_m50315_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50316_MethodInfo,
	&ICollection_1_Add_m50317_MethodInfo,
	&ICollection_1_Clear_m50318_MethodInfo,
	&ICollection_1_Contains_m50319_MethodInfo,
	&ICollection_1_CopyTo_m50320_MethodInfo,
	&ICollection_1_Remove_m50321_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8922_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8920_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8922_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8920_0_0_0;
extern Il2CppType ICollection_1_t8920_1_0_0;
struct ICollection_1_t8920;
extern Il2CppGenericClass ICollection_1_t8920_GenericClass;
TypeInfo ICollection_1_t8920_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8920_MethodInfos/* methods */
	, ICollection_1_t8920_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8920_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8920_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8920_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8920_0_0_0/* byval_arg */
	, &ICollection_1_t8920_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8920_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.GenericTypeParameterBuilder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.GenericTypeParameterBuilder>
extern Il2CppType IEnumerator_1_t6979_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50322_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.GenericTypeParameterBuilder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50322_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8922_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6979_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50322_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8922_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50322_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8922_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8922_0_0_0;
extern Il2CppType IEnumerable_1_t8922_1_0_0;
struct IEnumerable_1_t8922;
extern Il2CppGenericClass IEnumerable_1_t8922_GenericClass;
TypeInfo IEnumerable_1_t8922_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8922_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8922_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8922_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8922_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8922_0_0_0/* byval_arg */
	, &IEnumerable_1_t8922_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8922_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8921_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.GenericTypeParameterBuilder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.GenericTypeParameterBuilder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.GenericTypeParameterBuilder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.Emit.GenericTypeParameterBuilder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.GenericTypeParameterBuilder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Emit.GenericTypeParameterBuilder>
extern MethodInfo IList_1_get_Item_m50323_MethodInfo;
extern MethodInfo IList_1_set_Item_m50324_MethodInfo;
static PropertyInfo IList_1_t8921____Item_PropertyInfo = 
{
	&IList_1_t8921_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50323_MethodInfo/* get */
	, &IList_1_set_Item_m50324_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8921_PropertyInfos[] =
{
	&IList_1_t8921____Item_PropertyInfo,
	NULL
};
extern Il2CppType GenericTypeParameterBuilder_t1908_0_0_0;
static ParameterInfo IList_1_t8921_IList_1_IndexOf_m50325_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GenericTypeParameterBuilder_t1908_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50325_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.GenericTypeParameterBuilder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50325_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8921_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8921_IList_1_IndexOf_m50325_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50325_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType GenericTypeParameterBuilder_t1908_0_0_0;
static ParameterInfo IList_1_t8921_IList_1_Insert_m50326_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GenericTypeParameterBuilder_t1908_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50326_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.GenericTypeParameterBuilder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50326_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8921_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8921_IList_1_Insert_m50326_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50326_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8921_IList_1_RemoveAt_m50327_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50327_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.GenericTypeParameterBuilder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50327_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8921_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8921_IList_1_RemoveAt_m50327_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50327_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8921_IList_1_get_Item_m50323_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType GenericTypeParameterBuilder_t1908_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50323_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.Emit.GenericTypeParameterBuilder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50323_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8921_il2cpp_TypeInfo/* declaring_type */
	, &GenericTypeParameterBuilder_t1908_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8921_IList_1_get_Item_m50323_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50323_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType GenericTypeParameterBuilder_t1908_0_0_0;
static ParameterInfo IList_1_t8921_IList_1_set_Item_m50324_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GenericTypeParameterBuilder_t1908_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50324_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.GenericTypeParameterBuilder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50324_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8921_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8921_IList_1_set_Item_m50324_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50324_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8921_MethodInfos[] =
{
	&IList_1_IndexOf_m50325_MethodInfo,
	&IList_1_Insert_m50326_MethodInfo,
	&IList_1_RemoveAt_m50327_MethodInfo,
	&IList_1_get_Item_m50323_MethodInfo,
	&IList_1_set_Item_m50324_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8921_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8920_il2cpp_TypeInfo,
	&IEnumerable_1_t8922_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8921_0_0_0;
extern Il2CppType IList_1_t8921_1_0_0;
struct IList_1_t8921;
extern Il2CppGenericClass IList_1_t8921_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8921_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8921_MethodInfos/* methods */
	, IList_1_t8921_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8921_il2cpp_TypeInfo/* element_class */
	, IList_1_t8921_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8921_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8921_0_0_0/* byval_arg */
	, &IList_1_t8921_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8921_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6981_il2cpp_TypeInfo;

// System.Reflection.Emit.MethodBuilder
#include "mscorlib_System_Reflection_Emit_MethodBuilder.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.MethodBuilder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.MethodBuilder>
extern MethodInfo IEnumerator_1_get_Current_m50328_MethodInfo;
static PropertyInfo IEnumerator_1_t6981____Current_PropertyInfo = 
{
	&IEnumerator_1_t6981_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50328_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6981_PropertyInfos[] =
{
	&IEnumerator_1_t6981____Current_PropertyInfo,
	NULL
};
extern Il2CppType MethodBuilder_t1907_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50328_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.MethodBuilder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50328_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6981_il2cpp_TypeInfo/* declaring_type */
	, &MethodBuilder_t1907_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50328_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6981_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50328_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6981_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6981_0_0_0;
extern Il2CppType IEnumerator_1_t6981_1_0_0;
struct IEnumerator_1_t6981;
extern Il2CppGenericClass IEnumerator_1_t6981_GenericClass;
TypeInfo IEnumerator_1_t6981_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6981_MethodInfos/* methods */
	, IEnumerator_1_t6981_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6981_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6981_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6981_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6981_0_0_0/* byval_arg */
	, &IEnumerator_1_t6981_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6981_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_625.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5008_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_625MethodDeclarations.h"

extern TypeInfo MethodBuilder_t1907_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30626_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMethodBuilder_t1907_m39755_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.MethodBuilder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.MethodBuilder>(System.Int32)
#define Array_InternalArray__get_Item_TisMethodBuilder_t1907_m39755(__this, p0, method) (MethodBuilder_t1907 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5008____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5008_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5008, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5008____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5008_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5008, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5008_FieldInfos[] =
{
	&InternalEnumerator_1_t5008____array_0_FieldInfo,
	&InternalEnumerator_1_t5008____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30623_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5008____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5008_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30623_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5008____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5008_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30626_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5008_PropertyInfos[] =
{
	&InternalEnumerator_1_t5008____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5008____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5008_InternalEnumerator_1__ctor_m30622_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30622_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30622_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5008_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5008_InternalEnumerator_1__ctor_m30622_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30622_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30623_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30623_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5008_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30623_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30624_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30624_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5008_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30624_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30625_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30625_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5008_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30625_GenericMethod/* genericMethod */

};
extern Il2CppType MethodBuilder_t1907_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30626_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.MethodBuilder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30626_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5008_il2cpp_TypeInfo/* declaring_type */
	, &MethodBuilder_t1907_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30626_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5008_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30622_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30623_MethodInfo,
	&InternalEnumerator_1_Dispose_m30624_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30625_MethodInfo,
	&InternalEnumerator_1_get_Current_m30626_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30625_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30624_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5008_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30623_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30625_MethodInfo,
	&InternalEnumerator_1_Dispose_m30624_MethodInfo,
	&InternalEnumerator_1_get_Current_m30626_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5008_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6981_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5008_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6981_il2cpp_TypeInfo, 7},
};
extern TypeInfo MethodBuilder_t1907_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5008_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30626_MethodInfo/* Method Usage */,
	&MethodBuilder_t1907_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMethodBuilder_t1907_m39755_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5008_0_0_0;
extern Il2CppType InternalEnumerator_1_t5008_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5008_GenericClass;
TypeInfo InternalEnumerator_1_t5008_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5008_MethodInfos/* methods */
	, InternalEnumerator_1_t5008_PropertyInfos/* properties */
	, InternalEnumerator_1_t5008_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5008_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5008_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5008_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5008_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5008_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5008_1_0_0/* this_arg */
	, InternalEnumerator_1_t5008_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5008_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5008_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5008)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8923_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>
extern MethodInfo ICollection_1_get_Count_m50329_MethodInfo;
static PropertyInfo ICollection_1_t8923____Count_PropertyInfo = 
{
	&ICollection_1_t8923_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50329_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50330_MethodInfo;
static PropertyInfo ICollection_1_t8923____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8923_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50330_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8923_PropertyInfos[] =
{
	&ICollection_1_t8923____Count_PropertyInfo,
	&ICollection_1_t8923____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50329_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::get_Count()
MethodInfo ICollection_1_get_Count_m50329_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8923_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50329_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50330_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50330_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8923_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50330_GenericMethod/* genericMethod */

};
extern Il2CppType MethodBuilder_t1907_0_0_0;
extern Il2CppType MethodBuilder_t1907_0_0_0;
static ParameterInfo ICollection_1_t8923_ICollection_1_Add_m50331_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodBuilder_t1907_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50331_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::Add(T)
MethodInfo ICollection_1_Add_m50331_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8923_ICollection_1_Add_m50331_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50331_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50332_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::Clear()
MethodInfo ICollection_1_Clear_m50332_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50332_GenericMethod/* genericMethod */

};
extern Il2CppType MethodBuilder_t1907_0_0_0;
static ParameterInfo ICollection_1_t8923_ICollection_1_Contains_m50333_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodBuilder_t1907_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50333_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::Contains(T)
MethodInfo ICollection_1_Contains_m50333_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8923_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8923_ICollection_1_Contains_m50333_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50333_GenericMethod/* genericMethod */

};
extern Il2CppType MethodBuilderU5BU5D_t1912_0_0_0;
extern Il2CppType MethodBuilderU5BU5D_t1912_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8923_ICollection_1_CopyTo_m50334_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MethodBuilderU5BU5D_t1912_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50334_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50334_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8923_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8923_ICollection_1_CopyTo_m50334_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50334_GenericMethod/* genericMethod */

};
extern Il2CppType MethodBuilder_t1907_0_0_0;
static ParameterInfo ICollection_1_t8923_ICollection_1_Remove_m50335_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodBuilder_t1907_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50335_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.MethodBuilder>::Remove(T)
MethodInfo ICollection_1_Remove_m50335_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8923_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8923_ICollection_1_Remove_m50335_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50335_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8923_MethodInfos[] =
{
	&ICollection_1_get_Count_m50329_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50330_MethodInfo,
	&ICollection_1_Add_m50331_MethodInfo,
	&ICollection_1_Clear_m50332_MethodInfo,
	&ICollection_1_Contains_m50333_MethodInfo,
	&ICollection_1_CopyTo_m50334_MethodInfo,
	&ICollection_1_Remove_m50335_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8925_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8923_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8925_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8923_0_0_0;
extern Il2CppType ICollection_1_t8923_1_0_0;
struct ICollection_1_t8923;
extern Il2CppGenericClass ICollection_1_t8923_GenericClass;
TypeInfo ICollection_1_t8923_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8923_MethodInfos/* methods */
	, ICollection_1_t8923_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8923_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8923_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8923_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8923_0_0_0/* byval_arg */
	, &ICollection_1_t8923_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8923_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.MethodBuilder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.MethodBuilder>
extern Il2CppType IEnumerator_1_t6981_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50336_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.MethodBuilder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50336_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8925_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6981_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50336_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8925_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50336_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8925_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8925_0_0_0;
extern Il2CppType IEnumerable_1_t8925_1_0_0;
struct IEnumerable_1_t8925;
extern Il2CppGenericClass IEnumerable_1_t8925_GenericClass;
TypeInfo IEnumerable_1_t8925_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8925_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8925_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8925_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8925_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8925_0_0_0/* byval_arg */
	, &IEnumerable_1_t8925_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8925_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8924_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.MethodBuilder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.MethodBuilder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.MethodBuilder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.Emit.MethodBuilder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.MethodBuilder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Emit.MethodBuilder>
extern MethodInfo IList_1_get_Item_m50337_MethodInfo;
extern MethodInfo IList_1_set_Item_m50338_MethodInfo;
static PropertyInfo IList_1_t8924____Item_PropertyInfo = 
{
	&IList_1_t8924_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50337_MethodInfo/* get */
	, &IList_1_set_Item_m50338_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8924_PropertyInfos[] =
{
	&IList_1_t8924____Item_PropertyInfo,
	NULL
};
extern Il2CppType MethodBuilder_t1907_0_0_0;
static ParameterInfo IList_1_t8924_IList_1_IndexOf_m50339_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MethodBuilder_t1907_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50339_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.MethodBuilder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50339_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8924_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8924_IList_1_IndexOf_m50339_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50339_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MethodBuilder_t1907_0_0_0;
static ParameterInfo IList_1_t8924_IList_1_Insert_m50340_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MethodBuilder_t1907_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50340_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.MethodBuilder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50340_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8924_IList_1_Insert_m50340_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50340_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8924_IList_1_RemoveAt_m50341_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50341_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.MethodBuilder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50341_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8924_IList_1_RemoveAt_m50341_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50341_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8924_IList_1_get_Item_m50337_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType MethodBuilder_t1907_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50337_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.Emit.MethodBuilder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50337_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8924_il2cpp_TypeInfo/* declaring_type */
	, &MethodBuilder_t1907_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8924_IList_1_get_Item_m50337_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50337_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MethodBuilder_t1907_0_0_0;
static ParameterInfo IList_1_t8924_IList_1_set_Item_m50338_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MethodBuilder_t1907_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50338_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.MethodBuilder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50338_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8924_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8924_IList_1_set_Item_m50338_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50338_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8924_MethodInfos[] =
{
	&IList_1_IndexOf_m50339_MethodInfo,
	&IList_1_Insert_m50340_MethodInfo,
	&IList_1_RemoveAt_m50341_MethodInfo,
	&IList_1_get_Item_m50337_MethodInfo,
	&IList_1_set_Item_m50338_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8924_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8923_il2cpp_TypeInfo,
	&IEnumerable_1_t8925_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8924_0_0_0;
extern Il2CppType IList_1_t8924_1_0_0;
struct IList_1_t8924;
extern Il2CppGenericClass IList_1_t8924_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8924_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8924_MethodInfos/* methods */
	, IList_1_t8924_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8924_il2cpp_TypeInfo/* element_class */
	, IList_1_t8924_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8924_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8924_0_0_0/* byval_arg */
	, &IList_1_t8924_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8924_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8926_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>
extern MethodInfo ICollection_1_get_Count_m50342_MethodInfo;
static PropertyInfo ICollection_1_t8926____Count_PropertyInfo = 
{
	&ICollection_1_t8926_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50342_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50343_MethodInfo;
static PropertyInfo ICollection_1_t8926____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8926_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50343_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8926_PropertyInfos[] =
{
	&ICollection_1_t8926____Count_PropertyInfo,
	&ICollection_1_t8926____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50342_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::get_Count()
MethodInfo ICollection_1_get_Count_m50342_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8926_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50342_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50343_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50343_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50343_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodBuilder_t2601_0_0_0;
extern Il2CppType _MethodBuilder_t2601_0_0_0;
static ParameterInfo ICollection_1_t8926_ICollection_1_Add_m50344_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_MethodBuilder_t2601_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50344_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::Add(T)
MethodInfo ICollection_1_Add_m50344_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8926_ICollection_1_Add_m50344_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50344_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50345_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::Clear()
MethodInfo ICollection_1_Clear_m50345_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50345_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodBuilder_t2601_0_0_0;
static ParameterInfo ICollection_1_t8926_ICollection_1_Contains_m50346_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_MethodBuilder_t2601_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50346_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::Contains(T)
MethodInfo ICollection_1_Contains_m50346_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8926_ICollection_1_Contains_m50346_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50346_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodBuilderU5BU5D_t5303_0_0_0;
extern Il2CppType _MethodBuilderU5BU5D_t5303_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8926_ICollection_1_CopyTo_m50347_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &_MethodBuilderU5BU5D_t5303_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50347_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50347_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8926_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8926_ICollection_1_CopyTo_m50347_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50347_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodBuilder_t2601_0_0_0;
static ParameterInfo ICollection_1_t8926_ICollection_1_Remove_m50348_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_MethodBuilder_t2601_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50348_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._MethodBuilder>::Remove(T)
MethodInfo ICollection_1_Remove_m50348_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8926_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8926_ICollection_1_Remove_m50348_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50348_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8926_MethodInfos[] =
{
	&ICollection_1_get_Count_m50342_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50343_MethodInfo,
	&ICollection_1_Add_m50344_MethodInfo,
	&ICollection_1_Clear_m50345_MethodInfo,
	&ICollection_1_Contains_m50346_MethodInfo,
	&ICollection_1_CopyTo_m50347_MethodInfo,
	&ICollection_1_Remove_m50348_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8928_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8926_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8928_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8926_0_0_0;
extern Il2CppType ICollection_1_t8926_1_0_0;
struct ICollection_1_t8926;
extern Il2CppGenericClass ICollection_1_t8926_GenericClass;
TypeInfo ICollection_1_t8926_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8926_MethodInfos/* methods */
	, ICollection_1_t8926_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8926_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8926_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8926_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8926_0_0_0/* byval_arg */
	, &ICollection_1_t8926_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8926_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._MethodBuilder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._MethodBuilder>
extern Il2CppType IEnumerator_1_t6983_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50349_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._MethodBuilder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50349_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8928_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6983_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50349_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8928_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50349_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8928_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8928_0_0_0;
extern Il2CppType IEnumerable_1_t8928_1_0_0;
struct IEnumerable_1_t8928;
extern Il2CppGenericClass IEnumerable_1_t8928_GenericClass;
TypeInfo IEnumerable_1_t8928_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8928_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8928_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8928_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8928_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8928_0_0_0/* byval_arg */
	, &IEnumerable_1_t8928_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8928_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6983_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._MethodBuilder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._MethodBuilder>
extern MethodInfo IEnumerator_1_get_Current_m50350_MethodInfo;
static PropertyInfo IEnumerator_1_t6983____Current_PropertyInfo = 
{
	&IEnumerator_1_t6983_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50350_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6983_PropertyInfos[] =
{
	&IEnumerator_1_t6983____Current_PropertyInfo,
	NULL
};
extern Il2CppType _MethodBuilder_t2601_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50350_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._MethodBuilder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50350_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6983_il2cpp_TypeInfo/* declaring_type */
	, &_MethodBuilder_t2601_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50350_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6983_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50350_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6983_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6983_0_0_0;
extern Il2CppType IEnumerator_1_t6983_1_0_0;
struct IEnumerator_1_t6983;
extern Il2CppGenericClass IEnumerator_1_t6983_GenericClass;
TypeInfo IEnumerator_1_t6983_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6983_MethodInfos/* methods */
	, IEnumerator_1_t6983_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6983_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6983_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6983_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6983_0_0_0/* byval_arg */
	, &IEnumerator_1_t6983_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6983_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_626.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5009_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_626MethodDeclarations.h"

extern TypeInfo _MethodBuilder_t2601_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30631_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_Tis_MethodBuilder_t2601_m39766_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._MethodBuilder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._MethodBuilder>(System.Int32)
#define Array_InternalArray__get_Item_Tis_MethodBuilder_t2601_m39766(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5009____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5009_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5009, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5009____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5009_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5009, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5009_FieldInfos[] =
{
	&InternalEnumerator_1_t5009____array_0_FieldInfo,
	&InternalEnumerator_1_t5009____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30628_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5009____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5009_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30628_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5009____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5009_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30631_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5009_PropertyInfos[] =
{
	&InternalEnumerator_1_t5009____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5009____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5009_InternalEnumerator_1__ctor_m30627_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30627_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30627_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5009_InternalEnumerator_1__ctor_m30627_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30627_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30628_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30628_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5009_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30628_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30629_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30629_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5009_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30629_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30630_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30630_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5009_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30630_GenericMethod/* genericMethod */

};
extern Il2CppType _MethodBuilder_t2601_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30631_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._MethodBuilder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30631_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5009_il2cpp_TypeInfo/* declaring_type */
	, &_MethodBuilder_t2601_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30631_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5009_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30627_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30628_MethodInfo,
	&InternalEnumerator_1_Dispose_m30629_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30630_MethodInfo,
	&InternalEnumerator_1_get_Current_m30631_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30630_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30629_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5009_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30628_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30630_MethodInfo,
	&InternalEnumerator_1_Dispose_m30629_MethodInfo,
	&InternalEnumerator_1_get_Current_m30631_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5009_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6983_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5009_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6983_il2cpp_TypeInfo, 7},
};
extern TypeInfo _MethodBuilder_t2601_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5009_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30631_MethodInfo/* Method Usage */,
	&_MethodBuilder_t2601_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_Tis_MethodBuilder_t2601_m39766_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5009_0_0_0;
extern Il2CppType InternalEnumerator_1_t5009_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5009_GenericClass;
TypeInfo InternalEnumerator_1_t5009_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5009_MethodInfos/* methods */
	, InternalEnumerator_1_t5009_PropertyInfos/* properties */
	, InternalEnumerator_1_t5009_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5009_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5009_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5009_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5009_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5009_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5009_1_0_0/* this_arg */
	, InternalEnumerator_1_t5009_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5009_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5009_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5009)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8927_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBuilder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBuilder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBuilder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBuilder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBuilder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBuilder>
extern MethodInfo IList_1_get_Item_m50351_MethodInfo;
extern MethodInfo IList_1_set_Item_m50352_MethodInfo;
static PropertyInfo IList_1_t8927____Item_PropertyInfo = 
{
	&IList_1_t8927_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50351_MethodInfo/* get */
	, &IList_1_set_Item_m50352_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8927_PropertyInfos[] =
{
	&IList_1_t8927____Item_PropertyInfo,
	NULL
};
extern Il2CppType _MethodBuilder_t2601_0_0_0;
static ParameterInfo IList_1_t8927_IList_1_IndexOf_m50353_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_MethodBuilder_t2601_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50353_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBuilder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50353_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8927_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8927_IList_1_IndexOf_m50353_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50353_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _MethodBuilder_t2601_0_0_0;
static ParameterInfo IList_1_t8927_IList_1_Insert_m50354_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &_MethodBuilder_t2601_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50354_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBuilder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50354_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8927_IList_1_Insert_m50354_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50354_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8927_IList_1_RemoveAt_m50355_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50355_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBuilder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50355_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8927_IList_1_RemoveAt_m50355_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50355_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8927_IList_1_get_Item_m50351_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType _MethodBuilder_t2601_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50351_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBuilder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50351_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8927_il2cpp_TypeInfo/* declaring_type */
	, &_MethodBuilder_t2601_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8927_IList_1_get_Item_m50351_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50351_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _MethodBuilder_t2601_0_0_0;
static ParameterInfo IList_1_t8927_IList_1_set_Item_m50352_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &_MethodBuilder_t2601_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50352_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._MethodBuilder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50352_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8927_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8927_IList_1_set_Item_m50352_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50352_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8927_MethodInfos[] =
{
	&IList_1_IndexOf_m50353_MethodInfo,
	&IList_1_Insert_m50354_MethodInfo,
	&IList_1_RemoveAt_m50355_MethodInfo,
	&IList_1_get_Item_m50351_MethodInfo,
	&IList_1_set_Item_m50352_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8927_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8926_il2cpp_TypeInfo,
	&IEnumerable_1_t8928_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8927_0_0_0;
extern Il2CppType IList_1_t8927_1_0_0;
struct IList_1_t8927;
extern Il2CppGenericClass IList_1_t8927_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8927_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8927_MethodInfos/* methods */
	, IList_1_t8927_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8927_il2cpp_TypeInfo/* element_class */
	, IList_1_t8927_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8927_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8927_0_0_0/* byval_arg */
	, &IList_1_t8927_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8927_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6985_il2cpp_TypeInfo;

// System.Reflection.Emit.ConstructorBuilder
#include "mscorlib_System_Reflection_Emit_ConstructorBuilder.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ConstructorBuilder>
extern MethodInfo IEnumerator_1_get_Current_m50356_MethodInfo;
static PropertyInfo IEnumerator_1_t6985____Current_PropertyInfo = 
{
	&IEnumerator_1_t6985_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50356_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6985_PropertyInfos[] =
{
	&IEnumerator_1_t6985____Current_PropertyInfo,
	NULL
};
extern Il2CppType ConstructorBuilder_t1902_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50356_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50356_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6985_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorBuilder_t1902_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50356_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6985_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50356_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6985_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6985_0_0_0;
extern Il2CppType IEnumerator_1_t6985_1_0_0;
struct IEnumerator_1_t6985;
extern Il2CppGenericClass IEnumerator_1_t6985_GenericClass;
TypeInfo IEnumerator_1_t6985_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6985_MethodInfos/* methods */
	, IEnumerator_1_t6985_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6985_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6985_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6985_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6985_0_0_0/* byval_arg */
	, &IEnumerator_1_t6985_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6985_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_627.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5010_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_627MethodDeclarations.h"

extern TypeInfo ConstructorBuilder_t1902_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30636_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisConstructorBuilder_t1902_m39777_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ConstructorBuilder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ConstructorBuilder>(System.Int32)
#define Array_InternalArray__get_Item_TisConstructorBuilder_t1902_m39777(__this, p0, method) (ConstructorBuilder_t1902 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5010____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5010_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5010, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5010____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5010_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5010, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5010_FieldInfos[] =
{
	&InternalEnumerator_1_t5010____array_0_FieldInfo,
	&InternalEnumerator_1_t5010____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30633_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5010____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5010_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30633_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5010____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5010_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30636_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5010_PropertyInfos[] =
{
	&InternalEnumerator_1_t5010____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5010____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5010_InternalEnumerator_1__ctor_m30632_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30632_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30632_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5010_InternalEnumerator_1__ctor_m30632_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30632_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30633_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30633_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5010_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30633_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30634_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30634_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5010_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30634_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30635_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30635_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5010_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30635_GenericMethod/* genericMethod */

};
extern Il2CppType ConstructorBuilder_t1902_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30636_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ConstructorBuilder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30636_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5010_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorBuilder_t1902_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30636_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5010_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30632_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30633_MethodInfo,
	&InternalEnumerator_1_Dispose_m30634_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30635_MethodInfo,
	&InternalEnumerator_1_get_Current_m30636_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30635_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30634_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5010_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30633_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30635_MethodInfo,
	&InternalEnumerator_1_Dispose_m30634_MethodInfo,
	&InternalEnumerator_1_get_Current_m30636_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5010_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6985_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5010_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6985_il2cpp_TypeInfo, 7},
};
extern TypeInfo ConstructorBuilder_t1902_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5010_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30636_MethodInfo/* Method Usage */,
	&ConstructorBuilder_t1902_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisConstructorBuilder_t1902_m39777_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5010_0_0_0;
extern Il2CppType InternalEnumerator_1_t5010_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5010_GenericClass;
TypeInfo InternalEnumerator_1_t5010_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5010_MethodInfos/* methods */
	, InternalEnumerator_1_t5010_PropertyInfos/* properties */
	, InternalEnumerator_1_t5010_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5010_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5010_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5010_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5010_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5010_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5010_1_0_0/* this_arg */
	, InternalEnumerator_1_t5010_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5010_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5010_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5010)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8929_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>
extern MethodInfo ICollection_1_get_Count_m50357_MethodInfo;
static PropertyInfo ICollection_1_t8929____Count_PropertyInfo = 
{
	&ICollection_1_t8929_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50357_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50358_MethodInfo;
static PropertyInfo ICollection_1_t8929____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8929_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50358_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8929_PropertyInfos[] =
{
	&ICollection_1_t8929____Count_PropertyInfo,
	&ICollection_1_t8929____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50357_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::get_Count()
MethodInfo ICollection_1_get_Count_m50357_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8929_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50357_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50358_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50358_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8929_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50358_GenericMethod/* genericMethod */

};
extern Il2CppType ConstructorBuilder_t1902_0_0_0;
extern Il2CppType ConstructorBuilder_t1902_0_0_0;
static ParameterInfo ICollection_1_t8929_ICollection_1_Add_m50359_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConstructorBuilder_t1902_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50359_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::Add(T)
MethodInfo ICollection_1_Add_m50359_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8929_ICollection_1_Add_m50359_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50359_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50360_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::Clear()
MethodInfo ICollection_1_Clear_m50360_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50360_GenericMethod/* genericMethod */

};
extern Il2CppType ConstructorBuilder_t1902_0_0_0;
static ParameterInfo ICollection_1_t8929_ICollection_1_Contains_m50361_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConstructorBuilder_t1902_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50361_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::Contains(T)
MethodInfo ICollection_1_Contains_m50361_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8929_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8929_ICollection_1_Contains_m50361_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50361_GenericMethod/* genericMethod */

};
extern Il2CppType ConstructorBuilderU5BU5D_t1913_0_0_0;
extern Il2CppType ConstructorBuilderU5BU5D_t1913_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8929_ICollection_1_CopyTo_m50362_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ConstructorBuilderU5BU5D_t1913_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50362_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50362_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8929_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8929_ICollection_1_CopyTo_m50362_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50362_GenericMethod/* genericMethod */

};
extern Il2CppType ConstructorBuilder_t1902_0_0_0;
static ParameterInfo ICollection_1_t8929_ICollection_1_Remove_m50363_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConstructorBuilder_t1902_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50363_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.ConstructorBuilder>::Remove(T)
MethodInfo ICollection_1_Remove_m50363_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8929_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8929_ICollection_1_Remove_m50363_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50363_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8929_MethodInfos[] =
{
	&ICollection_1_get_Count_m50357_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50358_MethodInfo,
	&ICollection_1_Add_m50359_MethodInfo,
	&ICollection_1_Clear_m50360_MethodInfo,
	&ICollection_1_Contains_m50361_MethodInfo,
	&ICollection_1_CopyTo_m50362_MethodInfo,
	&ICollection_1_Remove_m50363_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8931_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8929_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8931_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8929_0_0_0;
extern Il2CppType ICollection_1_t8929_1_0_0;
struct ICollection_1_t8929;
extern Il2CppGenericClass ICollection_1_t8929_GenericClass;
TypeInfo ICollection_1_t8929_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8929_MethodInfos/* methods */
	, ICollection_1_t8929_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8929_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8929_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8929_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8929_0_0_0/* byval_arg */
	, &ICollection_1_t8929_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8929_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ConstructorBuilder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ConstructorBuilder>
extern Il2CppType IEnumerator_1_t6985_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50364_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.ConstructorBuilder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50364_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8931_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6985_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50364_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8931_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50364_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8931_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8931_0_0_0;
extern Il2CppType IEnumerable_1_t8931_1_0_0;
struct IEnumerable_1_t8931;
extern Il2CppGenericClass IEnumerable_1_t8931_GenericClass;
TypeInfo IEnumerable_1_t8931_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8931_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8931_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8931_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8931_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8931_0_0_0/* byval_arg */
	, &IEnumerable_1_t8931_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8931_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8930_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.ConstructorBuilder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ConstructorBuilder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ConstructorBuilder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.Emit.ConstructorBuilder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ConstructorBuilder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Emit.ConstructorBuilder>
extern MethodInfo IList_1_get_Item_m50365_MethodInfo;
extern MethodInfo IList_1_set_Item_m50366_MethodInfo;
static PropertyInfo IList_1_t8930____Item_PropertyInfo = 
{
	&IList_1_t8930_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50365_MethodInfo/* get */
	, &IList_1_set_Item_m50366_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8930_PropertyInfos[] =
{
	&IList_1_t8930____Item_PropertyInfo,
	NULL
};
extern Il2CppType ConstructorBuilder_t1902_0_0_0;
static ParameterInfo IList_1_t8930_IList_1_IndexOf_m50367_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ConstructorBuilder_t1902_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50367_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.ConstructorBuilder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50367_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8930_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8930_IList_1_IndexOf_m50367_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50367_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ConstructorBuilder_t1902_0_0_0;
static ParameterInfo IList_1_t8930_IList_1_Insert_m50368_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ConstructorBuilder_t1902_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50368_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ConstructorBuilder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50368_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8930_IList_1_Insert_m50368_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50368_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8930_IList_1_RemoveAt_m50369_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50369_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ConstructorBuilder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50369_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8930_IList_1_RemoveAt_m50369_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50369_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8930_IList_1_get_Item_m50365_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ConstructorBuilder_t1902_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50365_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.Emit.ConstructorBuilder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50365_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8930_il2cpp_TypeInfo/* declaring_type */
	, &ConstructorBuilder_t1902_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8930_IList_1_get_Item_m50365_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50365_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ConstructorBuilder_t1902_0_0_0;
static ParameterInfo IList_1_t8930_IList_1_set_Item_m50366_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ConstructorBuilder_t1902_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50366_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.ConstructorBuilder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50366_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8930_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8930_IList_1_set_Item_m50366_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50366_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8930_MethodInfos[] =
{
	&IList_1_IndexOf_m50367_MethodInfo,
	&IList_1_Insert_m50368_MethodInfo,
	&IList_1_RemoveAt_m50369_MethodInfo,
	&IList_1_get_Item_m50365_MethodInfo,
	&IList_1_set_Item_m50366_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8930_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8929_il2cpp_TypeInfo,
	&IEnumerable_1_t8931_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8930_0_0_0;
extern Il2CppType IList_1_t8930_1_0_0;
struct IList_1_t8930;
extern Il2CppGenericClass IList_1_t8930_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8930_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8930_MethodInfos/* methods */
	, IList_1_t8930_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8930_il2cpp_TypeInfo/* element_class */
	, IList_1_t8930_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8930_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8930_0_0_0/* byval_arg */
	, &IList_1_t8930_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8930_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8932_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>
extern MethodInfo ICollection_1_get_Count_m50370_MethodInfo;
static PropertyInfo ICollection_1_t8932____Count_PropertyInfo = 
{
	&ICollection_1_t8932_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50370_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50371_MethodInfo;
static PropertyInfo ICollection_1_t8932____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8932_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50371_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8932_PropertyInfos[] =
{
	&ICollection_1_t8932____Count_PropertyInfo,
	&ICollection_1_t8932____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50370_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::get_Count()
MethodInfo ICollection_1_get_Count_m50370_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8932_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50370_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50371_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50371_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8932_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50371_GenericMethod/* genericMethod */

};
extern Il2CppType _ConstructorBuilder_t2595_0_0_0;
extern Il2CppType _ConstructorBuilder_t2595_0_0_0;
static ParameterInfo ICollection_1_t8932_ICollection_1_Add_m50372_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ConstructorBuilder_t2595_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50372_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::Add(T)
MethodInfo ICollection_1_Add_m50372_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8932_ICollection_1_Add_m50372_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50372_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50373_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::Clear()
MethodInfo ICollection_1_Clear_m50373_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50373_GenericMethod/* genericMethod */

};
extern Il2CppType _ConstructorBuilder_t2595_0_0_0;
static ParameterInfo ICollection_1_t8932_ICollection_1_Contains_m50374_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ConstructorBuilder_t2595_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50374_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::Contains(T)
MethodInfo ICollection_1_Contains_m50374_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8932_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8932_ICollection_1_Contains_m50374_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50374_GenericMethod/* genericMethod */

};
extern Il2CppType _ConstructorBuilderU5BU5D_t5304_0_0_0;
extern Il2CppType _ConstructorBuilderU5BU5D_t5304_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8932_ICollection_1_CopyTo_m50375_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &_ConstructorBuilderU5BU5D_t5304_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50375_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50375_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8932_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8932_ICollection_1_CopyTo_m50375_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50375_GenericMethod/* genericMethod */

};
extern Il2CppType _ConstructorBuilder_t2595_0_0_0;
static ParameterInfo ICollection_1_t8932_ICollection_1_Remove_m50376_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ConstructorBuilder_t2595_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50376_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._ConstructorBuilder>::Remove(T)
MethodInfo ICollection_1_Remove_m50376_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8932_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8932_ICollection_1_Remove_m50376_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50376_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8932_MethodInfos[] =
{
	&ICollection_1_get_Count_m50370_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50371_MethodInfo,
	&ICollection_1_Add_m50372_MethodInfo,
	&ICollection_1_Clear_m50373_MethodInfo,
	&ICollection_1_Contains_m50374_MethodInfo,
	&ICollection_1_CopyTo_m50375_MethodInfo,
	&ICollection_1_Remove_m50376_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8934_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8932_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8934_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8932_0_0_0;
extern Il2CppType ICollection_1_t8932_1_0_0;
struct ICollection_1_t8932;
extern Il2CppGenericClass ICollection_1_t8932_GenericClass;
TypeInfo ICollection_1_t8932_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8932_MethodInfos/* methods */
	, ICollection_1_t8932_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8932_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8932_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8932_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8932_0_0_0/* byval_arg */
	, &ICollection_1_t8932_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8932_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ConstructorBuilder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ConstructorBuilder>
extern Il2CppType IEnumerator_1_t6987_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50377_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._ConstructorBuilder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50377_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8934_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6987_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50377_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8934_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50377_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8934_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8934_0_0_0;
extern Il2CppType IEnumerable_1_t8934_1_0_0;
struct IEnumerable_1_t8934;
extern Il2CppGenericClass IEnumerable_1_t8934_GenericClass;
TypeInfo IEnumerable_1_t8934_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8934_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8934_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8934_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8934_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8934_0_0_0/* byval_arg */
	, &IEnumerable_1_t8934_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8934_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6987_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>
extern MethodInfo IEnumerator_1_get_Current_m50378_MethodInfo;
static PropertyInfo IEnumerator_1_t6987____Current_PropertyInfo = 
{
	&IEnumerator_1_t6987_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50378_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6987_PropertyInfos[] =
{
	&IEnumerator_1_t6987____Current_PropertyInfo,
	NULL
};
extern Il2CppType _ConstructorBuilder_t2595_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50378_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50378_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6987_il2cpp_TypeInfo/* declaring_type */
	, &_ConstructorBuilder_t2595_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50378_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6987_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50378_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6987_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6987_0_0_0;
extern Il2CppType IEnumerator_1_t6987_1_0_0;
struct IEnumerator_1_t6987;
extern Il2CppGenericClass IEnumerator_1_t6987_GenericClass;
TypeInfo IEnumerator_1_t6987_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6987_MethodInfos/* methods */
	, IEnumerator_1_t6987_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6987_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6987_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6987_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6987_0_0_0/* byval_arg */
	, &IEnumerator_1_t6987_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6987_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_628.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5011_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_628MethodDeclarations.h"

extern TypeInfo _ConstructorBuilder_t2595_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30641_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_Tis_ConstructorBuilder_t2595_m39788_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._ConstructorBuilder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._ConstructorBuilder>(System.Int32)
#define Array_InternalArray__get_Item_Tis_ConstructorBuilder_t2595_m39788(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5011____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5011_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5011, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5011____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5011_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5011, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5011_FieldInfos[] =
{
	&InternalEnumerator_1_t5011____array_0_FieldInfo,
	&InternalEnumerator_1_t5011____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30638_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5011____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5011_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30638_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5011____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5011_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30641_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5011_PropertyInfos[] =
{
	&InternalEnumerator_1_t5011____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5011____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5011_InternalEnumerator_1__ctor_m30637_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30637_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30637_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5011_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5011_InternalEnumerator_1__ctor_m30637_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30637_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30638_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30638_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5011_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30638_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30639_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30639_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5011_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30639_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30640_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30640_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5011_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30640_GenericMethod/* genericMethod */

};
extern Il2CppType _ConstructorBuilder_t2595_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30641_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._ConstructorBuilder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30641_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5011_il2cpp_TypeInfo/* declaring_type */
	, &_ConstructorBuilder_t2595_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30641_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5011_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30637_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30638_MethodInfo,
	&InternalEnumerator_1_Dispose_m30639_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30640_MethodInfo,
	&InternalEnumerator_1_get_Current_m30641_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30640_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30639_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5011_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30638_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30640_MethodInfo,
	&InternalEnumerator_1_Dispose_m30639_MethodInfo,
	&InternalEnumerator_1_get_Current_m30641_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5011_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6987_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5011_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6987_il2cpp_TypeInfo, 7},
};
extern TypeInfo _ConstructorBuilder_t2595_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5011_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30641_MethodInfo/* Method Usage */,
	&_ConstructorBuilder_t2595_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_Tis_ConstructorBuilder_t2595_m39788_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5011_0_0_0;
extern Il2CppType InternalEnumerator_1_t5011_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5011_GenericClass;
TypeInfo InternalEnumerator_1_t5011_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5011_MethodInfos/* methods */
	, InternalEnumerator_1_t5011_PropertyInfos/* properties */
	, InternalEnumerator_1_t5011_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5011_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5011_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5011_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5011_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5011_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5011_1_0_0/* this_arg */
	, InternalEnumerator_1_t5011_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5011_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5011_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5011)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8933_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._ConstructorBuilder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ConstructorBuilder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ConstructorBuilder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._ConstructorBuilder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ConstructorBuilder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._ConstructorBuilder>
extern MethodInfo IList_1_get_Item_m50379_MethodInfo;
extern MethodInfo IList_1_set_Item_m50380_MethodInfo;
static PropertyInfo IList_1_t8933____Item_PropertyInfo = 
{
	&IList_1_t8933_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50379_MethodInfo/* get */
	, &IList_1_set_Item_m50380_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8933_PropertyInfos[] =
{
	&IList_1_t8933____Item_PropertyInfo,
	NULL
};
extern Il2CppType _ConstructorBuilder_t2595_0_0_0;
static ParameterInfo IList_1_t8933_IList_1_IndexOf_m50381_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_ConstructorBuilder_t2595_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50381_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._ConstructorBuilder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50381_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8933_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8933_IList_1_IndexOf_m50381_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50381_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _ConstructorBuilder_t2595_0_0_0;
static ParameterInfo IList_1_t8933_IList_1_Insert_m50382_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &_ConstructorBuilder_t2595_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50382_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ConstructorBuilder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50382_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8933_IList_1_Insert_m50382_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50382_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8933_IList_1_RemoveAt_m50383_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50383_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ConstructorBuilder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50383_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8933_IList_1_RemoveAt_m50383_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50383_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8933_IList_1_get_Item_m50379_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType _ConstructorBuilder_t2595_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50379_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._ConstructorBuilder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50379_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8933_il2cpp_TypeInfo/* declaring_type */
	, &_ConstructorBuilder_t2595_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8933_IList_1_get_Item_m50379_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50379_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _ConstructorBuilder_t2595_0_0_0;
static ParameterInfo IList_1_t8933_IList_1_set_Item_m50380_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &_ConstructorBuilder_t2595_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50380_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._ConstructorBuilder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50380_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8933_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8933_IList_1_set_Item_m50380_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50380_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8933_MethodInfos[] =
{
	&IList_1_IndexOf_m50381_MethodInfo,
	&IList_1_Insert_m50382_MethodInfo,
	&IList_1_RemoveAt_m50383_MethodInfo,
	&IList_1_get_Item_m50379_MethodInfo,
	&IList_1_set_Item_m50380_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8933_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8932_il2cpp_TypeInfo,
	&IEnumerable_1_t8934_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8933_0_0_0;
extern Il2CppType IList_1_t8933_1_0_0;
struct IList_1_t8933;
extern Il2CppGenericClass IList_1_t8933_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8933_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8933_MethodInfos/* methods */
	, IList_1_t8933_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8933_il2cpp_TypeInfo/* element_class */
	, IList_1_t8933_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8933_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8933_0_0_0/* byval_arg */
	, &IList_1_t8933_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8933_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6989_il2cpp_TypeInfo;

// System.Reflection.Emit.FieldBuilder
#include "mscorlib_System_Reflection_Emit_FieldBuilder.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.FieldBuilder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.FieldBuilder>
extern MethodInfo IEnumerator_1_get_Current_m50384_MethodInfo;
static PropertyInfo IEnumerator_1_t6989____Current_PropertyInfo = 
{
	&IEnumerator_1_t6989_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50384_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6989_PropertyInfos[] =
{
	&IEnumerator_1_t6989____Current_PropertyInfo,
	NULL
};
extern Il2CppType FieldBuilder_t1906_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50384_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.Emit.FieldBuilder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50384_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6989_il2cpp_TypeInfo/* declaring_type */
	, &FieldBuilder_t1906_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50384_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6989_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50384_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6989_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6989_0_0_0;
extern Il2CppType IEnumerator_1_t6989_1_0_0;
struct IEnumerator_1_t6989;
extern Il2CppGenericClass IEnumerator_1_t6989_GenericClass;
TypeInfo IEnumerator_1_t6989_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6989_MethodInfos/* methods */
	, IEnumerator_1_t6989_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6989_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6989_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6989_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6989_0_0_0/* byval_arg */
	, &IEnumerator_1_t6989_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6989_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_629.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5012_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_629MethodDeclarations.h"

extern TypeInfo FieldBuilder_t1906_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30646_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFieldBuilder_t1906_m39799_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.FieldBuilder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.FieldBuilder>(System.Int32)
#define Array_InternalArray__get_Item_TisFieldBuilder_t1906_m39799(__this, p0, method) (FieldBuilder_t1906 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5012____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5012_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5012, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5012____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5012_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5012, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5012_FieldInfos[] =
{
	&InternalEnumerator_1_t5012____array_0_FieldInfo,
	&InternalEnumerator_1_t5012____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30643_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5012____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5012_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30643_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5012____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5012_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30646_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5012_PropertyInfos[] =
{
	&InternalEnumerator_1_t5012____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5012____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5012_InternalEnumerator_1__ctor_m30642_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30642_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30642_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5012_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5012_InternalEnumerator_1__ctor_m30642_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30642_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30643_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30643_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5012_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30643_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30644_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30644_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5012_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30644_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30645_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30645_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5012_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30645_GenericMethod/* genericMethod */

};
extern Il2CppType FieldBuilder_t1906_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30646_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.FieldBuilder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30646_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5012_il2cpp_TypeInfo/* declaring_type */
	, &FieldBuilder_t1906_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30646_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5012_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30642_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30643_MethodInfo,
	&InternalEnumerator_1_Dispose_m30644_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30645_MethodInfo,
	&InternalEnumerator_1_get_Current_m30646_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30645_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30644_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5012_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30643_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30645_MethodInfo,
	&InternalEnumerator_1_Dispose_m30644_MethodInfo,
	&InternalEnumerator_1_get_Current_m30646_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5012_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6989_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5012_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6989_il2cpp_TypeInfo, 7},
};
extern TypeInfo FieldBuilder_t1906_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5012_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30646_MethodInfo/* Method Usage */,
	&FieldBuilder_t1906_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisFieldBuilder_t1906_m39799_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5012_0_0_0;
extern Il2CppType InternalEnumerator_1_t5012_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5012_GenericClass;
TypeInfo InternalEnumerator_1_t5012_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5012_MethodInfos/* methods */
	, InternalEnumerator_1_t5012_PropertyInfos/* properties */
	, InternalEnumerator_1_t5012_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5012_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5012_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5012_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5012_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5012_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5012_1_0_0/* this_arg */
	, InternalEnumerator_1_t5012_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5012_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5012_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5012)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8935_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>
extern MethodInfo ICollection_1_get_Count_m50385_MethodInfo;
static PropertyInfo ICollection_1_t8935____Count_PropertyInfo = 
{
	&ICollection_1_t8935_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50385_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50386_MethodInfo;
static PropertyInfo ICollection_1_t8935____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8935_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50386_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8935_PropertyInfos[] =
{
	&ICollection_1_t8935____Count_PropertyInfo,
	&ICollection_1_t8935____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50385_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::get_Count()
MethodInfo ICollection_1_get_Count_m50385_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8935_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50385_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50386_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50386_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8935_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50386_GenericMethod/* genericMethod */

};
extern Il2CppType FieldBuilder_t1906_0_0_0;
extern Il2CppType FieldBuilder_t1906_0_0_0;
static ParameterInfo ICollection_1_t8935_ICollection_1_Add_m50387_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FieldBuilder_t1906_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50387_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::Add(T)
MethodInfo ICollection_1_Add_m50387_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8935_ICollection_1_Add_m50387_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50387_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50388_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::Clear()
MethodInfo ICollection_1_Clear_m50388_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50388_GenericMethod/* genericMethod */

};
extern Il2CppType FieldBuilder_t1906_0_0_0;
static ParameterInfo ICollection_1_t8935_ICollection_1_Contains_m50389_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FieldBuilder_t1906_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50389_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::Contains(T)
MethodInfo ICollection_1_Contains_m50389_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8935_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8935_ICollection_1_Contains_m50389_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50389_GenericMethod/* genericMethod */

};
extern Il2CppType FieldBuilderU5BU5D_t1914_0_0_0;
extern Il2CppType FieldBuilderU5BU5D_t1914_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8935_ICollection_1_CopyTo_m50390_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FieldBuilderU5BU5D_t1914_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50390_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50390_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8935_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8935_ICollection_1_CopyTo_m50390_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50390_GenericMethod/* genericMethod */

};
extern Il2CppType FieldBuilder_t1906_0_0_0;
static ParameterInfo ICollection_1_t8935_ICollection_1_Remove_m50391_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FieldBuilder_t1906_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50391_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.Emit.FieldBuilder>::Remove(T)
MethodInfo ICollection_1_Remove_m50391_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8935_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8935_ICollection_1_Remove_m50391_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50391_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8935_MethodInfos[] =
{
	&ICollection_1_get_Count_m50385_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50386_MethodInfo,
	&ICollection_1_Add_m50387_MethodInfo,
	&ICollection_1_Clear_m50388_MethodInfo,
	&ICollection_1_Contains_m50389_MethodInfo,
	&ICollection_1_CopyTo_m50390_MethodInfo,
	&ICollection_1_Remove_m50391_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8937_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8935_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8937_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8935_0_0_0;
extern Il2CppType ICollection_1_t8935_1_0_0;
struct ICollection_1_t8935;
extern Il2CppGenericClass ICollection_1_t8935_GenericClass;
TypeInfo ICollection_1_t8935_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8935_MethodInfos/* methods */
	, ICollection_1_t8935_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8935_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8935_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8935_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8935_0_0_0/* byval_arg */
	, &ICollection_1_t8935_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8935_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.FieldBuilder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.FieldBuilder>
extern Il2CppType IEnumerator_1_t6989_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50392_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.Emit.FieldBuilder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50392_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8937_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6989_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50392_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8937_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50392_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8937_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8937_0_0_0;
extern Il2CppType IEnumerable_1_t8937_1_0_0;
struct IEnumerable_1_t8937;
extern Il2CppGenericClass IEnumerable_1_t8937_GenericClass;
TypeInfo IEnumerable_1_t8937_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8937_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8937_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8937_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8937_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8937_0_0_0/* byval_arg */
	, &IEnumerable_1_t8937_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8937_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8936_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.FieldBuilder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.FieldBuilder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.FieldBuilder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.Emit.FieldBuilder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.FieldBuilder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.Emit.FieldBuilder>
extern MethodInfo IList_1_get_Item_m50393_MethodInfo;
extern MethodInfo IList_1_set_Item_m50394_MethodInfo;
static PropertyInfo IList_1_t8936____Item_PropertyInfo = 
{
	&IList_1_t8936_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50393_MethodInfo/* get */
	, &IList_1_set_Item_m50394_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8936_PropertyInfos[] =
{
	&IList_1_t8936____Item_PropertyInfo,
	NULL
};
extern Il2CppType FieldBuilder_t1906_0_0_0;
static ParameterInfo IList_1_t8936_IList_1_IndexOf_m50395_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FieldBuilder_t1906_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50395_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.Emit.FieldBuilder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50395_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8936_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8936_IList_1_IndexOf_m50395_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50395_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FieldBuilder_t1906_0_0_0;
static ParameterInfo IList_1_t8936_IList_1_Insert_m50396_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FieldBuilder_t1906_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50396_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.FieldBuilder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50396_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8936_IList_1_Insert_m50396_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50396_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8936_IList_1_RemoveAt_m50397_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50397_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.FieldBuilder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50397_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8936_IList_1_RemoveAt_m50397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50397_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8936_IList_1_get_Item_m50393_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType FieldBuilder_t1906_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50393_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.Emit.FieldBuilder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50393_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8936_il2cpp_TypeInfo/* declaring_type */
	, &FieldBuilder_t1906_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8936_IList_1_get_Item_m50393_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50393_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FieldBuilder_t1906_0_0_0;
static ParameterInfo IList_1_t8936_IList_1_set_Item_m50394_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FieldBuilder_t1906_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50394_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.Emit.FieldBuilder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50394_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8936_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8936_IList_1_set_Item_m50394_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50394_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8936_MethodInfos[] =
{
	&IList_1_IndexOf_m50395_MethodInfo,
	&IList_1_Insert_m50396_MethodInfo,
	&IList_1_RemoveAt_m50397_MethodInfo,
	&IList_1_get_Item_m50393_MethodInfo,
	&IList_1_set_Item_m50394_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8936_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8935_il2cpp_TypeInfo,
	&IEnumerable_1_t8937_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8936_0_0_0;
extern Il2CppType IList_1_t8936_1_0_0;
struct IList_1_t8936;
extern Il2CppGenericClass IList_1_t8936_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8936_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8936_MethodInfos/* methods */
	, IList_1_t8936_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8936_il2cpp_TypeInfo/* element_class */
	, IList_1_t8936_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8936_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8936_0_0_0/* byval_arg */
	, &IList_1_t8936_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8936_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8938_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>
extern MethodInfo ICollection_1_get_Count_m50398_MethodInfo;
static PropertyInfo ICollection_1_t8938____Count_PropertyInfo = 
{
	&ICollection_1_t8938_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50398_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50399_MethodInfo;
static PropertyInfo ICollection_1_t8938____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8938_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50399_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8938_PropertyInfos[] =
{
	&ICollection_1_t8938____Count_PropertyInfo,
	&ICollection_1_t8938____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50398_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::get_Count()
MethodInfo ICollection_1_get_Count_m50398_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8938_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50398_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50399_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50399_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8938_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50399_GenericMethod/* genericMethod */

};
extern Il2CppType _FieldBuilder_t2599_0_0_0;
extern Il2CppType _FieldBuilder_t2599_0_0_0;
static ParameterInfo ICollection_1_t8938_ICollection_1_Add_m50400_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_FieldBuilder_t2599_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50400_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::Add(T)
MethodInfo ICollection_1_Add_m50400_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8938_ICollection_1_Add_m50400_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50400_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50401_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::Clear()
MethodInfo ICollection_1_Clear_m50401_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50401_GenericMethod/* genericMethod */

};
extern Il2CppType _FieldBuilder_t2599_0_0_0;
static ParameterInfo ICollection_1_t8938_ICollection_1_Contains_m50402_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_FieldBuilder_t2599_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50402_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::Contains(T)
MethodInfo ICollection_1_Contains_m50402_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8938_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8938_ICollection_1_Contains_m50402_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50402_GenericMethod/* genericMethod */

};
extern Il2CppType _FieldBuilderU5BU5D_t5305_0_0_0;
extern Il2CppType _FieldBuilderU5BU5D_t5305_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8938_ICollection_1_CopyTo_m50403_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &_FieldBuilderU5BU5D_t5305_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50403_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50403_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8938_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8938_ICollection_1_CopyTo_m50403_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50403_GenericMethod/* genericMethod */

};
extern Il2CppType _FieldBuilder_t2599_0_0_0;
static ParameterInfo ICollection_1_t8938_ICollection_1_Remove_m50404_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_FieldBuilder_t2599_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50404_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldBuilder>::Remove(T)
MethodInfo ICollection_1_Remove_m50404_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8938_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8938_ICollection_1_Remove_m50404_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50404_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8938_MethodInfos[] =
{
	&ICollection_1_get_Count_m50398_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50399_MethodInfo,
	&ICollection_1_Add_m50400_MethodInfo,
	&ICollection_1_Clear_m50401_MethodInfo,
	&ICollection_1_Contains_m50402_MethodInfo,
	&ICollection_1_CopyTo_m50403_MethodInfo,
	&ICollection_1_Remove_m50404_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8940_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8938_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8940_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8938_0_0_0;
extern Il2CppType ICollection_1_t8938_1_0_0;
struct ICollection_1_t8938;
extern Il2CppGenericClass ICollection_1_t8938_GenericClass;
TypeInfo ICollection_1_t8938_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8938_MethodInfos/* methods */
	, ICollection_1_t8938_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8938_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8938_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8938_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8938_0_0_0/* byval_arg */
	, &ICollection_1_t8938_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8938_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._FieldBuilder>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._FieldBuilder>
extern Il2CppType IEnumerator_1_t6991_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50405_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._FieldBuilder>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50405_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8940_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6991_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50405_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8940_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50405_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8940_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8940_0_0_0;
extern Il2CppType IEnumerable_1_t8940_1_0_0;
struct IEnumerable_1_t8940;
extern Il2CppGenericClass IEnumerable_1_t8940_GenericClass;
TypeInfo IEnumerable_1_t8940_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8940_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8940_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8940_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8940_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8940_0_0_0/* byval_arg */
	, &IEnumerable_1_t8940_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8940_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6991_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._FieldBuilder>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._FieldBuilder>
extern MethodInfo IEnumerator_1_get_Current_m50406_MethodInfo;
static PropertyInfo IEnumerator_1_t6991____Current_PropertyInfo = 
{
	&IEnumerator_1_t6991_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50406_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6991_PropertyInfos[] =
{
	&IEnumerator_1_t6991____Current_PropertyInfo,
	NULL
};
extern Il2CppType _FieldBuilder_t2599_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50406_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._FieldBuilder>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50406_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6991_il2cpp_TypeInfo/* declaring_type */
	, &_FieldBuilder_t2599_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50406_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6991_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50406_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6991_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6991_0_0_0;
extern Il2CppType IEnumerator_1_t6991_1_0_0;
struct IEnumerator_1_t6991;
extern Il2CppGenericClass IEnumerator_1_t6991_GenericClass;
TypeInfo IEnumerator_1_t6991_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6991_MethodInfos/* methods */
	, IEnumerator_1_t6991_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6991_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6991_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6991_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6991_0_0_0/* byval_arg */
	, &IEnumerator_1_t6991_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6991_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_630.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5013_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_630MethodDeclarations.h"

extern TypeInfo _FieldBuilder_t2599_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30651_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_Tis_FieldBuilder_t2599_m39810_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._FieldBuilder>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._FieldBuilder>(System.Int32)
#define Array_InternalArray__get_Item_Tis_FieldBuilder_t2599_m39810(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5013____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5013_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5013, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5013____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5013_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5013, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5013_FieldInfos[] =
{
	&InternalEnumerator_1_t5013____array_0_FieldInfo,
	&InternalEnumerator_1_t5013____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30648_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5013____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5013_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30648_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5013____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5013_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30651_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5013_PropertyInfos[] =
{
	&InternalEnumerator_1_t5013____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5013____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5013_InternalEnumerator_1__ctor_m30647_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30647_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30647_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5013_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5013_InternalEnumerator_1__ctor_m30647_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30647_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30648_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30648_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5013_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30648_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30649_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30649_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5013_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30649_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30650_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30650_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5013_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30650_GenericMethod/* genericMethod */

};
extern Il2CppType _FieldBuilder_t2599_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30651_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldBuilder>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30651_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5013_il2cpp_TypeInfo/* declaring_type */
	, &_FieldBuilder_t2599_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30651_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5013_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30647_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30648_MethodInfo,
	&InternalEnumerator_1_Dispose_m30649_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30650_MethodInfo,
	&InternalEnumerator_1_get_Current_m30651_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30650_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30649_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5013_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30648_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30650_MethodInfo,
	&InternalEnumerator_1_Dispose_m30649_MethodInfo,
	&InternalEnumerator_1_get_Current_m30651_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5013_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6991_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5013_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6991_il2cpp_TypeInfo, 7},
};
extern TypeInfo _FieldBuilder_t2599_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5013_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30651_MethodInfo/* Method Usage */,
	&_FieldBuilder_t2599_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_Tis_FieldBuilder_t2599_m39810_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5013_0_0_0;
extern Il2CppType InternalEnumerator_1_t5013_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5013_GenericClass;
TypeInfo InternalEnumerator_1_t5013_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5013_MethodInfos/* methods */
	, InternalEnumerator_1_t5013_PropertyInfos/* properties */
	, InternalEnumerator_1_t5013_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5013_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5013_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5013_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5013_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5013_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5013_1_0_0/* this_arg */
	, InternalEnumerator_1_t5013_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5013_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5013_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5013)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8939_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldBuilder>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldBuilder>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldBuilder>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldBuilder>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldBuilder>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldBuilder>
extern MethodInfo IList_1_get_Item_m50407_MethodInfo;
extern MethodInfo IList_1_set_Item_m50408_MethodInfo;
static PropertyInfo IList_1_t8939____Item_PropertyInfo = 
{
	&IList_1_t8939_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50407_MethodInfo/* get */
	, &IList_1_set_Item_m50408_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8939_PropertyInfos[] =
{
	&IList_1_t8939____Item_PropertyInfo,
	NULL
};
extern Il2CppType _FieldBuilder_t2599_0_0_0;
static ParameterInfo IList_1_t8939_IList_1_IndexOf_m50409_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_FieldBuilder_t2599_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50409_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldBuilder>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50409_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8939_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8939_IList_1_IndexOf_m50409_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50409_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _FieldBuilder_t2599_0_0_0;
static ParameterInfo IList_1_t8939_IList_1_Insert_m50410_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &_FieldBuilder_t2599_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50410_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldBuilder>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50410_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8939_IList_1_Insert_m50410_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50410_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8939_IList_1_RemoveAt_m50411_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50411_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldBuilder>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50411_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8939_IList_1_RemoveAt_m50411_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50411_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8939_IList_1_get_Item_m50407_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType _FieldBuilder_t2599_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50407_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldBuilder>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50407_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8939_il2cpp_TypeInfo/* declaring_type */
	, &_FieldBuilder_t2599_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8939_IList_1_get_Item_m50407_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50407_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _FieldBuilder_t2599_0_0_0;
static ParameterInfo IList_1_t8939_IList_1_set_Item_m50408_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &_FieldBuilder_t2599_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50408_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldBuilder>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50408_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8939_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8939_IList_1_set_Item_m50408_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50408_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8939_MethodInfos[] =
{
	&IList_1_IndexOf_m50409_MethodInfo,
	&IList_1_Insert_m50410_MethodInfo,
	&IList_1_RemoveAt_m50411_MethodInfo,
	&IList_1_get_Item_m50407_MethodInfo,
	&IList_1_set_Item_m50408_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8939_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8938_il2cpp_TypeInfo,
	&IEnumerable_1_t8940_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8939_0_0_0;
extern Il2CppType IList_1_t8939_1_0_0;
struct IList_1_t8939;
extern Il2CppGenericClass IList_1_t8939_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8939_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8939_MethodInfos/* methods */
	, IList_1_t8939_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8939_il2cpp_TypeInfo/* element_class */
	, IList_1_t8939_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8939_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8939_0_0_0/* byval_arg */
	, &IList_1_t8939_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8939_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8941_il2cpp_TypeInfo;

// System.Reflection.FieldInfo
#include "mscorlib_System_Reflection_FieldInfo.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>
extern MethodInfo ICollection_1_get_Count_m50412_MethodInfo;
static PropertyInfo ICollection_1_t8941____Count_PropertyInfo = 
{
	&ICollection_1_t8941_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50412_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50413_MethodInfo;
static PropertyInfo ICollection_1_t8941____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8941_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50413_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8941_PropertyInfos[] =
{
	&ICollection_1_t8941____Count_PropertyInfo,
	&ICollection_1_t8941____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50412_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::get_Count()
MethodInfo ICollection_1_get_Count_m50412_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8941_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50412_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50413_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50413_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8941_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50413_GenericMethod/* genericMethod */

};
extern Il2CppType FieldInfo_t1704_0_0_0;
extern Il2CppType FieldInfo_t1704_0_0_0;
static ParameterInfo ICollection_1_t8941_ICollection_1_Add_m50414_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FieldInfo_t1704_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50414_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::Add(T)
MethodInfo ICollection_1_Add_m50414_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8941_ICollection_1_Add_m50414_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50414_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50415_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::Clear()
MethodInfo ICollection_1_Clear_m50415_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50415_GenericMethod/* genericMethod */

};
extern Il2CppType FieldInfo_t1704_0_0_0;
static ParameterInfo ICollection_1_t8941_ICollection_1_Contains_m50416_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FieldInfo_t1704_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50416_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::Contains(T)
MethodInfo ICollection_1_Contains_m50416_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8941_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8941_ICollection_1_Contains_m50416_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50416_GenericMethod/* genericMethod */

};
extern Il2CppType FieldInfoU5BU5D_t5306_0_0_0;
extern Il2CppType FieldInfoU5BU5D_t5306_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8941_ICollection_1_CopyTo_m50417_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FieldInfoU5BU5D_t5306_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50417_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50417_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8941_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8941_ICollection_1_CopyTo_m50417_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50417_GenericMethod/* genericMethod */

};
extern Il2CppType FieldInfo_t1704_0_0_0;
static ParameterInfo ICollection_1_t8941_ICollection_1_Remove_m50418_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FieldInfo_t1704_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50418_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.FieldInfo>::Remove(T)
MethodInfo ICollection_1_Remove_m50418_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8941_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8941_ICollection_1_Remove_m50418_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50418_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8941_MethodInfos[] =
{
	&ICollection_1_get_Count_m50412_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50413_MethodInfo,
	&ICollection_1_Add_m50414_MethodInfo,
	&ICollection_1_Clear_m50415_MethodInfo,
	&ICollection_1_Contains_m50416_MethodInfo,
	&ICollection_1_CopyTo_m50417_MethodInfo,
	&ICollection_1_Remove_m50418_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8943_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8941_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8943_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8941_0_0_0;
extern Il2CppType ICollection_1_t8941_1_0_0;
struct ICollection_1_t8941;
extern Il2CppGenericClass ICollection_1_t8941_GenericClass;
TypeInfo ICollection_1_t8941_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8941_MethodInfos/* methods */
	, ICollection_1_t8941_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8941_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8941_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8941_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8941_0_0_0/* byval_arg */
	, &ICollection_1_t8941_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8941_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>
extern Il2CppType IEnumerator_1_t6993_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50419_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50419_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8943_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6993_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50419_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8943_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50419_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8943_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8943_0_0_0;
extern Il2CppType IEnumerable_1_t8943_1_0_0;
struct IEnumerable_1_t8943;
extern Il2CppGenericClass IEnumerable_1_t8943_GenericClass;
TypeInfo IEnumerable_1_t8943_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8943_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8943_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8943_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8943_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8943_0_0_0/* byval_arg */
	, &IEnumerable_1_t8943_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8943_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6993_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>
extern MethodInfo IEnumerator_1_get_Current_m50420_MethodInfo;
static PropertyInfo IEnumerator_1_t6993____Current_PropertyInfo = 
{
	&IEnumerator_1_t6993_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50420_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6993_PropertyInfos[] =
{
	&IEnumerator_1_t6993____Current_PropertyInfo,
	NULL
};
extern Il2CppType FieldInfo_t1704_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50420_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50420_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6993_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfo_t1704_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50420_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6993_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50420_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6993_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6993_0_0_0;
extern Il2CppType IEnumerator_1_t6993_1_0_0;
struct IEnumerator_1_t6993;
extern Il2CppGenericClass IEnumerator_1_t6993_GenericClass;
TypeInfo IEnumerator_1_t6993_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6993_MethodInfos/* methods */
	, IEnumerator_1_t6993_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6993_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6993_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6993_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6993_0_0_0/* byval_arg */
	, &IEnumerator_1_t6993_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6993_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_631.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5014_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_631MethodDeclarations.h"

extern TypeInfo FieldInfo_t1704_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFieldInfo_t1704_m39821_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.FieldInfo>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.FieldInfo>(System.Int32)
#define Array_InternalArray__get_Item_TisFieldInfo_t1704_m39821(__this, p0, method) (FieldInfo_t1704 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5014____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5014_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5014, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5014____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5014_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5014, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5014_FieldInfos[] =
{
	&InternalEnumerator_1_t5014____array_0_FieldInfo,
	&InternalEnumerator_1_t5014____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30653_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5014____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5014_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30653_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5014____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5014_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30656_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5014_PropertyInfos[] =
{
	&InternalEnumerator_1_t5014____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5014____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5014_InternalEnumerator_1__ctor_m30652_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30652_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30652_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5014_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5014_InternalEnumerator_1__ctor_m30652_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30652_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30653_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30653_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5014_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30653_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30654_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30654_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5014_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30654_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30655_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30655_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5014_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30655_GenericMethod/* genericMethod */

};
extern Il2CppType FieldInfo_t1704_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30656_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.FieldInfo>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30656_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5014_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfo_t1704_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30656_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5014_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30652_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30653_MethodInfo,
	&InternalEnumerator_1_Dispose_m30654_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30655_MethodInfo,
	&InternalEnumerator_1_get_Current_m30656_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30655_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30654_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5014_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30653_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30655_MethodInfo,
	&InternalEnumerator_1_Dispose_m30654_MethodInfo,
	&InternalEnumerator_1_get_Current_m30656_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5014_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6993_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5014_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6993_il2cpp_TypeInfo, 7},
};
extern TypeInfo FieldInfo_t1704_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5014_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30656_MethodInfo/* Method Usage */,
	&FieldInfo_t1704_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisFieldInfo_t1704_m39821_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5014_0_0_0;
extern Il2CppType InternalEnumerator_1_t5014_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5014_GenericClass;
TypeInfo InternalEnumerator_1_t5014_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5014_MethodInfos/* methods */
	, InternalEnumerator_1_t5014_PropertyInfos/* properties */
	, InternalEnumerator_1_t5014_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5014_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5014_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5014_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5014_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5014_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5014_1_0_0/* this_arg */
	, InternalEnumerator_1_t5014_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5014_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5014_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5014)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8942_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.FieldInfo>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.FieldInfo>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.FieldInfo>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.FieldInfo>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.FieldInfo>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.FieldInfo>
extern MethodInfo IList_1_get_Item_m50421_MethodInfo;
extern MethodInfo IList_1_set_Item_m50422_MethodInfo;
static PropertyInfo IList_1_t8942____Item_PropertyInfo = 
{
	&IList_1_t8942_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50421_MethodInfo/* get */
	, &IList_1_set_Item_m50422_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8942_PropertyInfos[] =
{
	&IList_1_t8942____Item_PropertyInfo,
	NULL
};
extern Il2CppType FieldInfo_t1704_0_0_0;
static ParameterInfo IList_1_t8942_IList_1_IndexOf_m50423_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FieldInfo_t1704_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50423_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.FieldInfo>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50423_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8942_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8942_IList_1_IndexOf_m50423_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50423_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FieldInfo_t1704_0_0_0;
static ParameterInfo IList_1_t8942_IList_1_Insert_m50424_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FieldInfo_t1704_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50424_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.FieldInfo>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50424_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8942_IList_1_Insert_m50424_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50424_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8942_IList_1_RemoveAt_m50425_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50425_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.FieldInfo>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50425_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8942_IList_1_RemoveAt_m50425_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50425_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8942_IList_1_get_Item_m50421_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType FieldInfo_t1704_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50421_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.FieldInfo>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50421_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8942_il2cpp_TypeInfo/* declaring_type */
	, &FieldInfo_t1704_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8942_IList_1_get_Item_m50421_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50421_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FieldInfo_t1704_0_0_0;
static ParameterInfo IList_1_t8942_IList_1_set_Item_m50422_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FieldInfo_t1704_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50422_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.FieldInfo>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50422_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8942_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8942_IList_1_set_Item_m50422_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50422_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8942_MethodInfos[] =
{
	&IList_1_IndexOf_m50423_MethodInfo,
	&IList_1_Insert_m50424_MethodInfo,
	&IList_1_RemoveAt_m50425_MethodInfo,
	&IList_1_get_Item_m50421_MethodInfo,
	&IList_1_set_Item_m50422_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8942_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8941_il2cpp_TypeInfo,
	&IEnumerable_1_t8943_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8942_0_0_0;
extern Il2CppType IList_1_t8942_1_0_0;
struct IList_1_t8942;
extern Il2CppGenericClass IList_1_t8942_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8942_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8942_MethodInfos/* methods */
	, IList_1_t8942_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8942_il2cpp_TypeInfo/* element_class */
	, IList_1_t8942_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8942_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8942_0_0_0/* byval_arg */
	, &IList_1_t8942_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8942_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8944_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>
extern MethodInfo ICollection_1_get_Count_m50426_MethodInfo;
static PropertyInfo ICollection_1_t8944____Count_PropertyInfo = 
{
	&ICollection_1_t8944_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50426_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50427_MethodInfo;
static PropertyInfo ICollection_1_t8944____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8944_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50427_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8944_PropertyInfos[] =
{
	&ICollection_1_t8944____Count_PropertyInfo,
	&ICollection_1_t8944____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50426_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::get_Count()
MethodInfo ICollection_1_get_Count_m50426_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8944_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50426_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50427_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50427_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8944_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50427_GenericMethod/* genericMethod */

};
extern Il2CppType _FieldInfo_t2600_0_0_0;
extern Il2CppType _FieldInfo_t2600_0_0_0;
static ParameterInfo ICollection_1_t8944_ICollection_1_Add_m50428_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_FieldInfo_t2600_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50428_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::Add(T)
MethodInfo ICollection_1_Add_m50428_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8944_ICollection_1_Add_m50428_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50428_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50429_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::Clear()
MethodInfo ICollection_1_Clear_m50429_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50429_GenericMethod/* genericMethod */

};
extern Il2CppType _FieldInfo_t2600_0_0_0;
static ParameterInfo ICollection_1_t8944_ICollection_1_Contains_m50430_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_FieldInfo_t2600_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50430_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::Contains(T)
MethodInfo ICollection_1_Contains_m50430_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8944_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8944_ICollection_1_Contains_m50430_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50430_GenericMethod/* genericMethod */

};
extern Il2CppType _FieldInfoU5BU5D_t5307_0_0_0;
extern Il2CppType _FieldInfoU5BU5D_t5307_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8944_ICollection_1_CopyTo_m50431_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &_FieldInfoU5BU5D_t5307_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50431_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50431_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8944_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8944_ICollection_1_CopyTo_m50431_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50431_GenericMethod/* genericMethod */

};
extern Il2CppType _FieldInfo_t2600_0_0_0;
static ParameterInfo ICollection_1_t8944_ICollection_1_Remove_m50432_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_FieldInfo_t2600_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50432_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.InteropServices._FieldInfo>::Remove(T)
MethodInfo ICollection_1_Remove_m50432_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8944_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8944_ICollection_1_Remove_m50432_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50432_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8944_MethodInfos[] =
{
	&ICollection_1_get_Count_m50426_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50427_MethodInfo,
	&ICollection_1_Add_m50428_MethodInfo,
	&ICollection_1_Clear_m50429_MethodInfo,
	&ICollection_1_Contains_m50430_MethodInfo,
	&ICollection_1_CopyTo_m50431_MethodInfo,
	&ICollection_1_Remove_m50432_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8946_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8944_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8946_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8944_0_0_0;
extern Il2CppType ICollection_1_t8944_1_0_0;
struct ICollection_1_t8944;
extern Il2CppGenericClass ICollection_1_t8944_GenericClass;
TypeInfo ICollection_1_t8944_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8944_MethodInfos/* methods */
	, ICollection_1_t8944_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8944_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8944_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8944_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8944_0_0_0/* byval_arg */
	, &ICollection_1_t8944_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8944_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._FieldInfo>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._FieldInfo>
extern Il2CppType IEnumerator_1_t6995_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50433_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.InteropServices._FieldInfo>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50433_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8946_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6995_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50433_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8946_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50433_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8946_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8946_0_0_0;
extern Il2CppType IEnumerable_1_t8946_1_0_0;
struct IEnumerable_1_t8946;
extern Il2CppGenericClass IEnumerable_1_t8946_GenericClass;
TypeInfo IEnumerable_1_t8946_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8946_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8946_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8946_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8946_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8946_0_0_0/* byval_arg */
	, &IEnumerable_1_t8946_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8946_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6995_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._FieldInfo>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._FieldInfo>
extern MethodInfo IEnumerator_1_get_Current_m50434_MethodInfo;
static PropertyInfo IEnumerator_1_t6995____Current_PropertyInfo = 
{
	&IEnumerator_1_t6995_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50434_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6995_PropertyInfos[] =
{
	&IEnumerator_1_t6995____Current_PropertyInfo,
	NULL
};
extern Il2CppType _FieldInfo_t2600_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50434_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.InteropServices._FieldInfo>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50434_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6995_il2cpp_TypeInfo/* declaring_type */
	, &_FieldInfo_t2600_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50434_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6995_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50434_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6995_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6995_0_0_0;
extern Il2CppType IEnumerator_1_t6995_1_0_0;
struct IEnumerator_1_t6995;
extern Il2CppGenericClass IEnumerator_1_t6995_GenericClass;
TypeInfo IEnumerator_1_t6995_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6995_MethodInfos/* methods */
	, IEnumerator_1_t6995_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6995_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6995_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6995_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6995_0_0_0/* byval_arg */
	, &IEnumerator_1_t6995_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6995_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_632.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5015_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_632MethodDeclarations.h"

extern TypeInfo _FieldInfo_t2600_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30661_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_Tis_FieldInfo_t2600_m39832_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._FieldInfo>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.InteropServices._FieldInfo>(System.Int32)
#define Array_InternalArray__get_Item_Tis_FieldInfo_t2600_m39832(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5015____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5015_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5015, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5015____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5015_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5015, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5015_FieldInfos[] =
{
	&InternalEnumerator_1_t5015____array_0_FieldInfo,
	&InternalEnumerator_1_t5015____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30658_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5015____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5015_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30658_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5015____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5015_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30661_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5015_PropertyInfos[] =
{
	&InternalEnumerator_1_t5015____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5015____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5015_InternalEnumerator_1__ctor_m30657_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30657_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30657_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5015_InternalEnumerator_1__ctor_m30657_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30657_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30658_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30658_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5015_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30658_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30659_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30659_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5015_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30659_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30660_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30660_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5015_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30660_GenericMethod/* genericMethod */

};
extern Il2CppType _FieldInfo_t2600_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30661_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.InteropServices._FieldInfo>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30661_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5015_il2cpp_TypeInfo/* declaring_type */
	, &_FieldInfo_t2600_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30661_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5015_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30657_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30658_MethodInfo,
	&InternalEnumerator_1_Dispose_m30659_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30660_MethodInfo,
	&InternalEnumerator_1_get_Current_m30661_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30660_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30659_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5015_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30658_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30660_MethodInfo,
	&InternalEnumerator_1_Dispose_m30659_MethodInfo,
	&InternalEnumerator_1_get_Current_m30661_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5015_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6995_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5015_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6995_il2cpp_TypeInfo, 7},
};
extern TypeInfo _FieldInfo_t2600_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5015_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30661_MethodInfo/* Method Usage */,
	&_FieldInfo_t2600_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_Tis_FieldInfo_t2600_m39832_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5015_0_0_0;
extern Il2CppType InternalEnumerator_1_t5015_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5015_GenericClass;
TypeInfo InternalEnumerator_1_t5015_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5015_MethodInfos/* methods */
	, InternalEnumerator_1_t5015_PropertyInfos/* properties */
	, InternalEnumerator_1_t5015_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5015_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5015_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5015_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5015_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5015_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5015_1_0_0/* this_arg */
	, InternalEnumerator_1_t5015_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5015_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5015_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5015)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8945_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldInfo>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldInfo>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldInfo>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldInfo>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldInfo>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldInfo>
extern MethodInfo IList_1_get_Item_m50435_MethodInfo;
extern MethodInfo IList_1_set_Item_m50436_MethodInfo;
static PropertyInfo IList_1_t8945____Item_PropertyInfo = 
{
	&IList_1_t8945_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50435_MethodInfo/* get */
	, &IList_1_set_Item_m50436_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8945_PropertyInfos[] =
{
	&IList_1_t8945____Item_PropertyInfo,
	NULL
};
extern Il2CppType _FieldInfo_t2600_0_0_0;
static ParameterInfo IList_1_t8945_IList_1_IndexOf_m50437_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &_FieldInfo_t2600_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50437_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldInfo>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50437_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8945_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8945_IList_1_IndexOf_m50437_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50437_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _FieldInfo_t2600_0_0_0;
static ParameterInfo IList_1_t8945_IList_1_Insert_m50438_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &_FieldInfo_t2600_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50438_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldInfo>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50438_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8945_IList_1_Insert_m50438_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50438_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8945_IList_1_RemoveAt_m50439_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50439_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldInfo>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50439_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8945_IList_1_RemoveAt_m50439_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50439_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8945_IList_1_get_Item_m50435_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType _FieldInfo_t2600_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50435_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldInfo>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50435_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8945_il2cpp_TypeInfo/* declaring_type */
	, &_FieldInfo_t2600_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8945_IList_1_get_Item_m50435_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50435_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType _FieldInfo_t2600_0_0_0;
static ParameterInfo IList_1_t8945_IList_1_set_Item_m50436_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &_FieldInfo_t2600_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50436_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.InteropServices._FieldInfo>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50436_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8945_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8945_IList_1_set_Item_m50436_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50436_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8945_MethodInfos[] =
{
	&IList_1_IndexOf_m50437_MethodInfo,
	&IList_1_Insert_m50438_MethodInfo,
	&IList_1_RemoveAt_m50439_MethodInfo,
	&IList_1_get_Item_m50435_MethodInfo,
	&IList_1_set_Item_m50436_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8945_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8944_il2cpp_TypeInfo,
	&IEnumerable_1_t8946_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8945_0_0_0;
extern Il2CppType IList_1_t8945_1_0_0;
struct IList_1_t8945;
extern Il2CppGenericClass IList_1_t8945_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8945_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8945_MethodInfos/* methods */
	, IList_1_t8945_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8945_il2cpp_TypeInfo/* element_class */
	, IList_1_t8945_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8945_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8945_0_0_0/* byval_arg */
	, &IList_1_t8945_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8945_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6997_il2cpp_TypeInfo;

// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyCompanyAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyCompanyAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50440_MethodInfo;
static PropertyInfo IEnumerator_1_t6997____Current_PropertyInfo = 
{
	&IEnumerator_1_t6997_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50440_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6997_PropertyInfos[] =
{
	&IEnumerator_1_t6997____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyCompanyAttribute_t532_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50440_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyCompanyAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50440_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6997_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyCompanyAttribute_t532_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50440_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6997_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50440_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6997_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6997_0_0_0;
extern Il2CppType IEnumerator_1_t6997_1_0_0;
struct IEnumerator_1_t6997;
extern Il2CppGenericClass IEnumerator_1_t6997_GenericClass;
TypeInfo IEnumerator_1_t6997_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6997_MethodInfos/* methods */
	, IEnumerator_1_t6997_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6997_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6997_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6997_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6997_0_0_0/* byval_arg */
	, &IEnumerator_1_t6997_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6997_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_633.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5016_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_633MethodDeclarations.h"

extern TypeInfo AssemblyCompanyAttribute_t532_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30666_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyCompanyAttribute_t532_m39843_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyCompanyAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyCompanyAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAssemblyCompanyAttribute_t532_m39843(__this, p0, method) (AssemblyCompanyAttribute_t532 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5016____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5016_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5016, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5016____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5016_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5016, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5016_FieldInfos[] =
{
	&InternalEnumerator_1_t5016____array_0_FieldInfo,
	&InternalEnumerator_1_t5016____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30663_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5016____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5016_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30663_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5016____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5016_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30666_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5016_PropertyInfos[] =
{
	&InternalEnumerator_1_t5016____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5016____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5016_InternalEnumerator_1__ctor_m30662_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30662_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30662_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5016_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5016_InternalEnumerator_1__ctor_m30662_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30662_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30663_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30663_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5016_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30663_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30664_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30664_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5016_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30664_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30665_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30665_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5016_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30665_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyCompanyAttribute_t532_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30666_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyCompanyAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30666_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5016_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyCompanyAttribute_t532_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30666_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5016_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30662_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30663_MethodInfo,
	&InternalEnumerator_1_Dispose_m30664_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30665_MethodInfo,
	&InternalEnumerator_1_get_Current_m30666_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30665_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30664_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5016_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30663_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30665_MethodInfo,
	&InternalEnumerator_1_Dispose_m30664_MethodInfo,
	&InternalEnumerator_1_get_Current_m30666_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5016_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6997_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5016_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6997_il2cpp_TypeInfo, 7},
};
extern TypeInfo AssemblyCompanyAttribute_t532_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5016_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30666_MethodInfo/* Method Usage */,
	&AssemblyCompanyAttribute_t532_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAssemblyCompanyAttribute_t532_m39843_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5016_0_0_0;
extern Il2CppType InternalEnumerator_1_t5016_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5016_GenericClass;
TypeInfo InternalEnumerator_1_t5016_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5016_MethodInfos/* methods */
	, InternalEnumerator_1_t5016_PropertyInfos/* properties */
	, InternalEnumerator_1_t5016_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5016_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5016_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5016_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5016_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5016_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5016_1_0_0/* this_arg */
	, InternalEnumerator_1_t5016_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5016_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5016_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5016)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8947_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>
extern MethodInfo ICollection_1_get_Count_m50441_MethodInfo;
static PropertyInfo ICollection_1_t8947____Count_PropertyInfo = 
{
	&ICollection_1_t8947_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50441_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50442_MethodInfo;
static PropertyInfo ICollection_1_t8947____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8947_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50442_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8947_PropertyInfos[] =
{
	&ICollection_1_t8947____Count_PropertyInfo,
	&ICollection_1_t8947____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50441_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50441_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8947_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50441_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50442_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50442_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8947_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50442_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyCompanyAttribute_t532_0_0_0;
extern Il2CppType AssemblyCompanyAttribute_t532_0_0_0;
static ParameterInfo ICollection_1_t8947_ICollection_1_Add_m50443_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyCompanyAttribute_t532_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50443_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50443_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8947_ICollection_1_Add_m50443_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50443_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50444_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50444_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50444_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyCompanyAttribute_t532_0_0_0;
static ParameterInfo ICollection_1_t8947_ICollection_1_Contains_m50445_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyCompanyAttribute_t532_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50445_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50445_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8947_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8947_ICollection_1_Contains_m50445_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50445_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyCompanyAttributeU5BU5D_t5308_0_0_0;
extern Il2CppType AssemblyCompanyAttributeU5BU5D_t5308_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8947_ICollection_1_CopyTo_m50446_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyCompanyAttributeU5BU5D_t5308_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50446_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50446_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8947_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8947_ICollection_1_CopyTo_m50446_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50446_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyCompanyAttribute_t532_0_0_0;
static ParameterInfo ICollection_1_t8947_ICollection_1_Remove_m50447_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyCompanyAttribute_t532_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50447_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCompanyAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50447_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8947_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8947_ICollection_1_Remove_m50447_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50447_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8947_MethodInfos[] =
{
	&ICollection_1_get_Count_m50441_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50442_MethodInfo,
	&ICollection_1_Add_m50443_MethodInfo,
	&ICollection_1_Clear_m50444_MethodInfo,
	&ICollection_1_Contains_m50445_MethodInfo,
	&ICollection_1_CopyTo_m50446_MethodInfo,
	&ICollection_1_Remove_m50447_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8949_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8947_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8949_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8947_0_0_0;
extern Il2CppType ICollection_1_t8947_1_0_0;
struct ICollection_1_t8947;
extern Il2CppGenericClass ICollection_1_t8947_GenericClass;
TypeInfo ICollection_1_t8947_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8947_MethodInfos/* methods */
	, ICollection_1_t8947_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8947_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8947_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8947_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8947_0_0_0/* byval_arg */
	, &ICollection_1_t8947_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8947_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyCompanyAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyCompanyAttribute>
extern Il2CppType IEnumerator_1_t6997_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50448_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyCompanyAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50448_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8949_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6997_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50448_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8949_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50448_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8949_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8949_0_0_0;
extern Il2CppType IEnumerable_1_t8949_1_0_0;
struct IEnumerable_1_t8949;
extern Il2CppGenericClass IEnumerable_1_t8949_GenericClass;
TypeInfo IEnumerable_1_t8949_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8949_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8949_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8949_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8949_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8949_0_0_0/* byval_arg */
	, &IEnumerable_1_t8949_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8949_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8948_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyCompanyAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyCompanyAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyCompanyAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyCompanyAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyCompanyAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyCompanyAttribute>
extern MethodInfo IList_1_get_Item_m50449_MethodInfo;
extern MethodInfo IList_1_set_Item_m50450_MethodInfo;
static PropertyInfo IList_1_t8948____Item_PropertyInfo = 
{
	&IList_1_t8948_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50449_MethodInfo/* get */
	, &IList_1_set_Item_m50450_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8948_PropertyInfos[] =
{
	&IList_1_t8948____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyCompanyAttribute_t532_0_0_0;
static ParameterInfo IList_1_t8948_IList_1_IndexOf_m50451_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyCompanyAttribute_t532_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50451_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyCompanyAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50451_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8948_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8948_IList_1_IndexOf_m50451_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50451_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyCompanyAttribute_t532_0_0_0;
static ParameterInfo IList_1_t8948_IList_1_Insert_m50452_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyCompanyAttribute_t532_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50452_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyCompanyAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50452_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8948_IList_1_Insert_m50452_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50452_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8948_IList_1_RemoveAt_m50453_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50453_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyCompanyAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50453_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8948_IList_1_RemoveAt_m50453_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50453_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8948_IList_1_get_Item_m50449_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AssemblyCompanyAttribute_t532_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50449_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyCompanyAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50449_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8948_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyCompanyAttribute_t532_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8948_IList_1_get_Item_m50449_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50449_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyCompanyAttribute_t532_0_0_0;
static ParameterInfo IList_1_t8948_IList_1_set_Item_m50450_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyCompanyAttribute_t532_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50450_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyCompanyAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50450_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8948_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8948_IList_1_set_Item_m50450_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50450_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8948_MethodInfos[] =
{
	&IList_1_IndexOf_m50451_MethodInfo,
	&IList_1_Insert_m50452_MethodInfo,
	&IList_1_RemoveAt_m50453_MethodInfo,
	&IList_1_get_Item_m50449_MethodInfo,
	&IList_1_set_Item_m50450_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8948_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8947_il2cpp_TypeInfo,
	&IEnumerable_1_t8949_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8948_0_0_0;
extern Il2CppType IList_1_t8948_1_0_0;
struct IList_1_t8948;
extern Il2CppGenericClass IList_1_t8948_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8948_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8948_MethodInfos/* methods */
	, IList_1_t8948_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8948_il2cpp_TypeInfo/* element_class */
	, IList_1_t8948_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8948_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8948_0_0_0/* byval_arg */
	, &IList_1_t8948_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8948_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6999_il2cpp_TypeInfo;

// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50454_MethodInfo;
static PropertyInfo IEnumerator_1_t6999____Current_PropertyInfo = 
{
	&IEnumerator_1_t6999_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50454_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6999_PropertyInfos[] =
{
	&IEnumerator_1_t6999____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyConfigurationAttribute_t531_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50454_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50454_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6999_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyConfigurationAttribute_t531_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50454_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6999_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50454_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6999_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6999_0_0_0;
extern Il2CppType IEnumerator_1_t6999_1_0_0;
struct IEnumerator_1_t6999;
extern Il2CppGenericClass IEnumerator_1_t6999_GenericClass;
TypeInfo IEnumerator_1_t6999_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6999_MethodInfos/* methods */
	, IEnumerator_1_t6999_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6999_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6999_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6999_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6999_0_0_0/* byval_arg */
	, &IEnumerator_1_t6999_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6999_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_634.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5017_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_634MethodDeclarations.h"

extern TypeInfo AssemblyConfigurationAttribute_t531_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30671_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyConfigurationAttribute_t531_m39854_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyConfigurationAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyConfigurationAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAssemblyConfigurationAttribute_t531_m39854(__this, p0, method) (AssemblyConfigurationAttribute_t531 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5017____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5017_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5017, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5017____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5017_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5017, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5017_FieldInfos[] =
{
	&InternalEnumerator_1_t5017____array_0_FieldInfo,
	&InternalEnumerator_1_t5017____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30668_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5017____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5017_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30668_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5017____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5017_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30671_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5017_PropertyInfos[] =
{
	&InternalEnumerator_1_t5017____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5017____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5017_InternalEnumerator_1__ctor_m30667_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30667_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30667_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5017_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5017_InternalEnumerator_1__ctor_m30667_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30667_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30668_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30668_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5017_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30668_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30669_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30669_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5017_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30669_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30670_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30670_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5017_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30670_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyConfigurationAttribute_t531_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30671_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyConfigurationAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30671_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5017_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyConfigurationAttribute_t531_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30671_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5017_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30667_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30668_MethodInfo,
	&InternalEnumerator_1_Dispose_m30669_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30670_MethodInfo,
	&InternalEnumerator_1_get_Current_m30671_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30670_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30669_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5017_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30668_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30670_MethodInfo,
	&InternalEnumerator_1_Dispose_m30669_MethodInfo,
	&InternalEnumerator_1_get_Current_m30671_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5017_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6999_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5017_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6999_il2cpp_TypeInfo, 7},
};
extern TypeInfo AssemblyConfigurationAttribute_t531_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5017_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30671_MethodInfo/* Method Usage */,
	&AssemblyConfigurationAttribute_t531_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAssemblyConfigurationAttribute_t531_m39854_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5017_0_0_0;
extern Il2CppType InternalEnumerator_1_t5017_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5017_GenericClass;
TypeInfo InternalEnumerator_1_t5017_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5017_MethodInfos/* methods */
	, InternalEnumerator_1_t5017_PropertyInfos/* properties */
	, InternalEnumerator_1_t5017_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5017_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5017_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5017_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5017_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5017_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5017_1_0_0/* this_arg */
	, InternalEnumerator_1_t5017_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5017_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5017_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5017)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8950_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>
extern MethodInfo ICollection_1_get_Count_m50455_MethodInfo;
static PropertyInfo ICollection_1_t8950____Count_PropertyInfo = 
{
	&ICollection_1_t8950_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50455_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50456_MethodInfo;
static PropertyInfo ICollection_1_t8950____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8950_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50456_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8950_PropertyInfos[] =
{
	&ICollection_1_t8950____Count_PropertyInfo,
	&ICollection_1_t8950____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50455_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50455_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8950_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50455_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50456_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50456_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8950_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50456_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyConfigurationAttribute_t531_0_0_0;
extern Il2CppType AssemblyConfigurationAttribute_t531_0_0_0;
static ParameterInfo ICollection_1_t8950_ICollection_1_Add_m50457_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyConfigurationAttribute_t531_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50457_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50457_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8950_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8950_ICollection_1_Add_m50457_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50457_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50458_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50458_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8950_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50458_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyConfigurationAttribute_t531_0_0_0;
static ParameterInfo ICollection_1_t8950_ICollection_1_Contains_m50459_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyConfigurationAttribute_t531_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50459_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50459_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8950_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8950_ICollection_1_Contains_m50459_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50459_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyConfigurationAttributeU5BU5D_t5309_0_0_0;
extern Il2CppType AssemblyConfigurationAttributeU5BU5D_t5309_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8950_ICollection_1_CopyTo_m50460_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyConfigurationAttributeU5BU5D_t5309_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50460_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50460_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8950_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8950_ICollection_1_CopyTo_m50460_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50460_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyConfigurationAttribute_t531_0_0_0;
static ParameterInfo ICollection_1_t8950_ICollection_1_Remove_m50461_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyConfigurationAttribute_t531_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50461_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyConfigurationAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50461_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8950_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8950_ICollection_1_Remove_m50461_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50461_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8950_MethodInfos[] =
{
	&ICollection_1_get_Count_m50455_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50456_MethodInfo,
	&ICollection_1_Add_m50457_MethodInfo,
	&ICollection_1_Clear_m50458_MethodInfo,
	&ICollection_1_Contains_m50459_MethodInfo,
	&ICollection_1_CopyTo_m50460_MethodInfo,
	&ICollection_1_Remove_m50461_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8952_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8950_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8952_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8950_0_0_0;
extern Il2CppType ICollection_1_t8950_1_0_0;
struct ICollection_1_t8950;
extern Il2CppGenericClass ICollection_1_t8950_GenericClass;
TypeInfo ICollection_1_t8950_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8950_MethodInfos/* methods */
	, ICollection_1_t8950_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8950_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8950_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8950_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8950_0_0_0/* byval_arg */
	, &ICollection_1_t8950_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8950_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyConfigurationAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyConfigurationAttribute>
extern Il2CppType IEnumerator_1_t6999_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50462_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyConfigurationAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50462_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8952_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6999_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50462_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8952_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50462_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8952_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8952_0_0_0;
extern Il2CppType IEnumerable_1_t8952_1_0_0;
struct IEnumerable_1_t8952;
extern Il2CppGenericClass IEnumerable_1_t8952_GenericClass;
TypeInfo IEnumerable_1_t8952_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8952_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8952_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8952_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8952_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8952_0_0_0/* byval_arg */
	, &IEnumerable_1_t8952_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8952_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8951_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyConfigurationAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyConfigurationAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyConfigurationAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyConfigurationAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyConfigurationAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyConfigurationAttribute>
extern MethodInfo IList_1_get_Item_m50463_MethodInfo;
extern MethodInfo IList_1_set_Item_m50464_MethodInfo;
static PropertyInfo IList_1_t8951____Item_PropertyInfo = 
{
	&IList_1_t8951_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50463_MethodInfo/* get */
	, &IList_1_set_Item_m50464_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8951_PropertyInfos[] =
{
	&IList_1_t8951____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyConfigurationAttribute_t531_0_0_0;
static ParameterInfo IList_1_t8951_IList_1_IndexOf_m50465_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyConfigurationAttribute_t531_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50465_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyConfigurationAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50465_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8951_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8951_IList_1_IndexOf_m50465_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50465_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyConfigurationAttribute_t531_0_0_0;
static ParameterInfo IList_1_t8951_IList_1_Insert_m50466_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyConfigurationAttribute_t531_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50466_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyConfigurationAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50466_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8951_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8951_IList_1_Insert_m50466_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50466_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8951_IList_1_RemoveAt_m50467_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50467_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyConfigurationAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50467_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8951_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8951_IList_1_RemoveAt_m50467_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50467_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8951_IList_1_get_Item_m50463_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AssemblyConfigurationAttribute_t531_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50463_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyConfigurationAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50463_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8951_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyConfigurationAttribute_t531_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8951_IList_1_get_Item_m50463_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50463_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyConfigurationAttribute_t531_0_0_0;
static ParameterInfo IList_1_t8951_IList_1_set_Item_m50464_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyConfigurationAttribute_t531_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50464_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyConfigurationAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50464_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8951_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8951_IList_1_set_Item_m50464_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50464_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8951_MethodInfos[] =
{
	&IList_1_IndexOf_m50465_MethodInfo,
	&IList_1_Insert_m50466_MethodInfo,
	&IList_1_RemoveAt_m50467_MethodInfo,
	&IList_1_get_Item_m50463_MethodInfo,
	&IList_1_set_Item_m50464_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8951_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8950_il2cpp_TypeInfo,
	&IEnumerable_1_t8952_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8951_0_0_0;
extern Il2CppType IList_1_t8951_1_0_0;
struct IList_1_t8951;
extern Il2CppGenericClass IList_1_t8951_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8951_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8951_MethodInfos/* methods */
	, IList_1_t8951_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8951_il2cpp_TypeInfo/* element_class */
	, IList_1_t8951_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8951_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8951_0_0_0/* byval_arg */
	, &IList_1_t8951_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8951_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7001_il2cpp_TypeInfo;

// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50468_MethodInfo;
static PropertyInfo IEnumerator_1_t7001____Current_PropertyInfo = 
{
	&IEnumerator_1_t7001_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50468_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7001_PropertyInfos[] =
{
	&IEnumerator_1_t7001____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyCopyrightAttribute_t534_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50468_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50468_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7001_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyCopyrightAttribute_t534_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50468_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7001_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50468_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7001_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7001_0_0_0;
extern Il2CppType IEnumerator_1_t7001_1_0_0;
struct IEnumerator_1_t7001;
extern Il2CppGenericClass IEnumerator_1_t7001_GenericClass;
TypeInfo IEnumerator_1_t7001_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7001_MethodInfos/* methods */
	, IEnumerator_1_t7001_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7001_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7001_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7001_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7001_0_0_0/* byval_arg */
	, &IEnumerator_1_t7001_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7001_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_635.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5018_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_635MethodDeclarations.h"

extern TypeInfo AssemblyCopyrightAttribute_t534_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30676_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyCopyrightAttribute_t534_m39865_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyCopyrightAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyCopyrightAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAssemblyCopyrightAttribute_t534_m39865(__this, p0, method) (AssemblyCopyrightAttribute_t534 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5018____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5018_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5018, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5018____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5018_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5018, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5018_FieldInfos[] =
{
	&InternalEnumerator_1_t5018____array_0_FieldInfo,
	&InternalEnumerator_1_t5018____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30673_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5018____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5018_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30673_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5018____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5018_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30676_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5018_PropertyInfos[] =
{
	&InternalEnumerator_1_t5018____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5018____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5018_InternalEnumerator_1__ctor_m30672_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30672_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30672_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5018_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5018_InternalEnumerator_1__ctor_m30672_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30672_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30673_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30673_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5018_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30673_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30674_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30674_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5018_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30674_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30675_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30675_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5018_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30675_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyCopyrightAttribute_t534_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30676_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyCopyrightAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30676_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5018_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyCopyrightAttribute_t534_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30676_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5018_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30672_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30673_MethodInfo,
	&InternalEnumerator_1_Dispose_m30674_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30675_MethodInfo,
	&InternalEnumerator_1_get_Current_m30676_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30675_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30674_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5018_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30673_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30675_MethodInfo,
	&InternalEnumerator_1_Dispose_m30674_MethodInfo,
	&InternalEnumerator_1_get_Current_m30676_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5018_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t7001_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5018_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7001_il2cpp_TypeInfo, 7},
};
extern TypeInfo AssemblyCopyrightAttribute_t534_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5018_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30676_MethodInfo/* Method Usage */,
	&AssemblyCopyrightAttribute_t534_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAssemblyCopyrightAttribute_t534_m39865_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5018_0_0_0;
extern Il2CppType InternalEnumerator_1_t5018_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5018_GenericClass;
TypeInfo InternalEnumerator_1_t5018_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5018_MethodInfos/* methods */
	, InternalEnumerator_1_t5018_PropertyInfos/* properties */
	, InternalEnumerator_1_t5018_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5018_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5018_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5018_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5018_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5018_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5018_1_0_0/* this_arg */
	, InternalEnumerator_1_t5018_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5018_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5018_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5018)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8953_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>
extern MethodInfo ICollection_1_get_Count_m50469_MethodInfo;
static PropertyInfo ICollection_1_t8953____Count_PropertyInfo = 
{
	&ICollection_1_t8953_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50469_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50470_MethodInfo;
static PropertyInfo ICollection_1_t8953____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8953_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50470_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8953_PropertyInfos[] =
{
	&ICollection_1_t8953____Count_PropertyInfo,
	&ICollection_1_t8953____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50469_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50469_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8953_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50469_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50470_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50470_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8953_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50470_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyCopyrightAttribute_t534_0_0_0;
extern Il2CppType AssemblyCopyrightAttribute_t534_0_0_0;
static ParameterInfo ICollection_1_t8953_ICollection_1_Add_m50471_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyCopyrightAttribute_t534_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50471_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50471_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8953_ICollection_1_Add_m50471_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50471_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50472_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50472_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50472_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyCopyrightAttribute_t534_0_0_0;
static ParameterInfo ICollection_1_t8953_ICollection_1_Contains_m50473_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyCopyrightAttribute_t534_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50473_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50473_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8953_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8953_ICollection_1_Contains_m50473_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50473_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyCopyrightAttributeU5BU5D_t5310_0_0_0;
extern Il2CppType AssemblyCopyrightAttributeU5BU5D_t5310_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8953_ICollection_1_CopyTo_m50474_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyCopyrightAttributeU5BU5D_t5310_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50474_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50474_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8953_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8953_ICollection_1_CopyTo_m50474_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50474_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyCopyrightAttribute_t534_0_0_0;
static ParameterInfo ICollection_1_t8953_ICollection_1_Remove_m50475_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyCopyrightAttribute_t534_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50475_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyCopyrightAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50475_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8953_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8953_ICollection_1_Remove_m50475_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50475_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8953_MethodInfos[] =
{
	&ICollection_1_get_Count_m50469_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50470_MethodInfo,
	&ICollection_1_Add_m50471_MethodInfo,
	&ICollection_1_Clear_m50472_MethodInfo,
	&ICollection_1_Contains_m50473_MethodInfo,
	&ICollection_1_CopyTo_m50474_MethodInfo,
	&ICollection_1_Remove_m50475_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8955_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8953_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8955_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8953_0_0_0;
extern Il2CppType ICollection_1_t8953_1_0_0;
struct ICollection_1_t8953;
extern Il2CppGenericClass ICollection_1_t8953_GenericClass;
TypeInfo ICollection_1_t8953_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8953_MethodInfos/* methods */
	, ICollection_1_t8953_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8953_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8953_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8953_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8953_0_0_0/* byval_arg */
	, &ICollection_1_t8953_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8953_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyCopyrightAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyCopyrightAttribute>
extern Il2CppType IEnumerator_1_t7001_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50476_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyCopyrightAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50476_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8955_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7001_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50476_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8955_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50476_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8955_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8955_0_0_0;
extern Il2CppType IEnumerable_1_t8955_1_0_0;
struct IEnumerable_1_t8955;
extern Il2CppGenericClass IEnumerable_1_t8955_GenericClass;
TypeInfo IEnumerable_1_t8955_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8955_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8955_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8955_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8955_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8955_0_0_0/* byval_arg */
	, &IEnumerable_1_t8955_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8955_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8954_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyCopyrightAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyCopyrightAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyCopyrightAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyCopyrightAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyCopyrightAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyCopyrightAttribute>
extern MethodInfo IList_1_get_Item_m50477_MethodInfo;
extern MethodInfo IList_1_set_Item_m50478_MethodInfo;
static PropertyInfo IList_1_t8954____Item_PropertyInfo = 
{
	&IList_1_t8954_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50477_MethodInfo/* get */
	, &IList_1_set_Item_m50478_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8954_PropertyInfos[] =
{
	&IList_1_t8954____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyCopyrightAttribute_t534_0_0_0;
static ParameterInfo IList_1_t8954_IList_1_IndexOf_m50479_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyCopyrightAttribute_t534_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50479_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyCopyrightAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50479_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8954_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8954_IList_1_IndexOf_m50479_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50479_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyCopyrightAttribute_t534_0_0_0;
static ParameterInfo IList_1_t8954_IList_1_Insert_m50480_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyCopyrightAttribute_t534_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50480_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyCopyrightAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50480_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8954_IList_1_Insert_m50480_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50480_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8954_IList_1_RemoveAt_m50481_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50481_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyCopyrightAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50481_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8954_IList_1_RemoveAt_m50481_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50481_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8954_IList_1_get_Item_m50477_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AssemblyCopyrightAttribute_t534_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50477_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyCopyrightAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50477_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8954_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyCopyrightAttribute_t534_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8954_IList_1_get_Item_m50477_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50477_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyCopyrightAttribute_t534_0_0_0;
static ParameterInfo IList_1_t8954_IList_1_set_Item_m50478_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyCopyrightAttribute_t534_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50478_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyCopyrightAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50478_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8954_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8954_IList_1_set_Item_m50478_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50478_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8954_MethodInfos[] =
{
	&IList_1_IndexOf_m50479_MethodInfo,
	&IList_1_Insert_m50480_MethodInfo,
	&IList_1_RemoveAt_m50481_MethodInfo,
	&IList_1_get_Item_m50477_MethodInfo,
	&IList_1_set_Item_m50478_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8954_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8953_il2cpp_TypeInfo,
	&IEnumerable_1_t8955_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8954_0_0_0;
extern Il2CppType IList_1_t8954_1_0_0;
struct IList_1_t8954;
extern Il2CppGenericClass IList_1_t8954_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8954_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8954_MethodInfos/* methods */
	, IList_1_t8954_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8954_il2cpp_TypeInfo/* element_class */
	, IList_1_t8954_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8954_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8954_0_0_0/* byval_arg */
	, &IList_1_t8954_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8954_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7003_il2cpp_TypeInfo;

// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50482_MethodInfo;
static PropertyInfo IEnumerator_1_t7003____Current_PropertyInfo = 
{
	&IEnumerator_1_t7003_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50482_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7003_PropertyInfos[] =
{
	&IEnumerator_1_t7003____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyDefaultAliasAttribute_t1282_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50482_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50482_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7003_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyDefaultAliasAttribute_t1282_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50482_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7003_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50482_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7003_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7003_0_0_0;
extern Il2CppType IEnumerator_1_t7003_1_0_0;
struct IEnumerator_1_t7003;
extern Il2CppGenericClass IEnumerator_1_t7003_GenericClass;
TypeInfo IEnumerator_1_t7003_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7003_MethodInfos/* methods */
	, IEnumerator_1_t7003_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7003_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7003_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7003_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7003_0_0_0/* byval_arg */
	, &IEnumerator_1_t7003_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7003_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_636.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5019_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_636MethodDeclarations.h"

extern TypeInfo AssemblyDefaultAliasAttribute_t1282_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30681_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyDefaultAliasAttribute_t1282_m39876_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyDefaultAliasAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyDefaultAliasAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAssemblyDefaultAliasAttribute_t1282_m39876(__this, p0, method) (AssemblyDefaultAliasAttribute_t1282 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5019____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5019_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5019, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5019____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5019_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5019, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5019_FieldInfos[] =
{
	&InternalEnumerator_1_t5019____array_0_FieldInfo,
	&InternalEnumerator_1_t5019____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30678_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5019____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5019_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30678_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5019____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5019_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30681_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5019_PropertyInfos[] =
{
	&InternalEnumerator_1_t5019____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5019____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5019_InternalEnumerator_1__ctor_m30677_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30677_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30677_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5019_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5019_InternalEnumerator_1__ctor_m30677_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30677_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30678_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30678_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5019_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30678_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30679_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30679_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5019_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30679_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30680_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30680_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5019_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30680_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDefaultAliasAttribute_t1282_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30681_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyDefaultAliasAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30681_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5019_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyDefaultAliasAttribute_t1282_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30681_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5019_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30677_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30678_MethodInfo,
	&InternalEnumerator_1_Dispose_m30679_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30680_MethodInfo,
	&InternalEnumerator_1_get_Current_m30681_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30680_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30679_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5019_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30678_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30680_MethodInfo,
	&InternalEnumerator_1_Dispose_m30679_MethodInfo,
	&InternalEnumerator_1_get_Current_m30681_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5019_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t7003_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5019_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7003_il2cpp_TypeInfo, 7},
};
extern TypeInfo AssemblyDefaultAliasAttribute_t1282_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5019_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30681_MethodInfo/* Method Usage */,
	&AssemblyDefaultAliasAttribute_t1282_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAssemblyDefaultAliasAttribute_t1282_m39876_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5019_0_0_0;
extern Il2CppType InternalEnumerator_1_t5019_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5019_GenericClass;
TypeInfo InternalEnumerator_1_t5019_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5019_MethodInfos/* methods */
	, InternalEnumerator_1_t5019_PropertyInfos/* properties */
	, InternalEnumerator_1_t5019_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5019_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5019_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5019_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5019_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5019_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5019_1_0_0/* this_arg */
	, InternalEnumerator_1_t5019_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5019_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5019_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5019)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8956_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>
extern MethodInfo ICollection_1_get_Count_m50483_MethodInfo;
static PropertyInfo ICollection_1_t8956____Count_PropertyInfo = 
{
	&ICollection_1_t8956_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50483_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50484_MethodInfo;
static PropertyInfo ICollection_1_t8956____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8956_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50484_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8956_PropertyInfos[] =
{
	&ICollection_1_t8956____Count_PropertyInfo,
	&ICollection_1_t8956____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50483_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50483_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8956_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50483_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50484_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50484_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8956_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50484_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDefaultAliasAttribute_t1282_0_0_0;
extern Il2CppType AssemblyDefaultAliasAttribute_t1282_0_0_0;
static ParameterInfo ICollection_1_t8956_ICollection_1_Add_m50485_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDefaultAliasAttribute_t1282_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50485_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50485_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8956_ICollection_1_Add_m50485_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50485_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50486_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50486_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50486_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDefaultAliasAttribute_t1282_0_0_0;
static ParameterInfo ICollection_1_t8956_ICollection_1_Contains_m50487_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDefaultAliasAttribute_t1282_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50487_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50487_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8956_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8956_ICollection_1_Contains_m50487_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50487_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDefaultAliasAttributeU5BU5D_t5311_0_0_0;
extern Il2CppType AssemblyDefaultAliasAttributeU5BU5D_t5311_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8956_ICollection_1_CopyTo_m50488_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDefaultAliasAttributeU5BU5D_t5311_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50488_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50488_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8956_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8956_ICollection_1_CopyTo_m50488_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50488_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDefaultAliasAttribute_t1282_0_0_0;
static ParameterInfo ICollection_1_t8956_ICollection_1_Remove_m50489_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDefaultAliasAttribute_t1282_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50489_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDefaultAliasAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50489_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8956_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8956_ICollection_1_Remove_m50489_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50489_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8956_MethodInfos[] =
{
	&ICollection_1_get_Count_m50483_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50484_MethodInfo,
	&ICollection_1_Add_m50485_MethodInfo,
	&ICollection_1_Clear_m50486_MethodInfo,
	&ICollection_1_Contains_m50487_MethodInfo,
	&ICollection_1_CopyTo_m50488_MethodInfo,
	&ICollection_1_Remove_m50489_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8958_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8956_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8958_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8956_0_0_0;
extern Il2CppType ICollection_1_t8956_1_0_0;
struct ICollection_1_t8956;
extern Il2CppGenericClass ICollection_1_t8956_GenericClass;
TypeInfo ICollection_1_t8956_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8956_MethodInfos/* methods */
	, ICollection_1_t8956_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8956_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8956_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8956_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8956_0_0_0/* byval_arg */
	, &ICollection_1_t8956_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8956_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyDefaultAliasAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyDefaultAliasAttribute>
extern Il2CppType IEnumerator_1_t7003_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50490_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyDefaultAliasAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50490_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8958_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7003_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50490_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8958_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50490_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8958_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8958_0_0_0;
extern Il2CppType IEnumerable_1_t8958_1_0_0;
struct IEnumerable_1_t8958;
extern Il2CppGenericClass IEnumerable_1_t8958_GenericClass;
TypeInfo IEnumerable_1_t8958_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8958_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8958_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8958_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8958_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8958_0_0_0/* byval_arg */
	, &IEnumerable_1_t8958_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8958_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8957_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyDefaultAliasAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDefaultAliasAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDefaultAliasAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyDefaultAliasAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDefaultAliasAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyDefaultAliasAttribute>
extern MethodInfo IList_1_get_Item_m50491_MethodInfo;
extern MethodInfo IList_1_set_Item_m50492_MethodInfo;
static PropertyInfo IList_1_t8957____Item_PropertyInfo = 
{
	&IList_1_t8957_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50491_MethodInfo/* get */
	, &IList_1_set_Item_m50492_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8957_PropertyInfos[] =
{
	&IList_1_t8957____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyDefaultAliasAttribute_t1282_0_0_0;
static ParameterInfo IList_1_t8957_IList_1_IndexOf_m50493_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDefaultAliasAttribute_t1282_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50493_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyDefaultAliasAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50493_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8957_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8957_IList_1_IndexOf_m50493_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50493_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyDefaultAliasAttribute_t1282_0_0_0;
static ParameterInfo IList_1_t8957_IList_1_Insert_m50494_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyDefaultAliasAttribute_t1282_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50494_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDefaultAliasAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50494_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8957_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8957_IList_1_Insert_m50494_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50494_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8957_IList_1_RemoveAt_m50495_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50495_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDefaultAliasAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50495_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8957_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8957_IList_1_RemoveAt_m50495_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50495_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8957_IList_1_get_Item_m50491_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AssemblyDefaultAliasAttribute_t1282_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50491_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyDefaultAliasAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50491_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8957_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyDefaultAliasAttribute_t1282_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8957_IList_1_get_Item_m50491_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50491_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyDefaultAliasAttribute_t1282_0_0_0;
static ParameterInfo IList_1_t8957_IList_1_set_Item_m50492_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyDefaultAliasAttribute_t1282_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50492_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDefaultAliasAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50492_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8957_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8957_IList_1_set_Item_m50492_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50492_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8957_MethodInfos[] =
{
	&IList_1_IndexOf_m50493_MethodInfo,
	&IList_1_Insert_m50494_MethodInfo,
	&IList_1_RemoveAt_m50495_MethodInfo,
	&IList_1_get_Item_m50491_MethodInfo,
	&IList_1_set_Item_m50492_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8957_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8956_il2cpp_TypeInfo,
	&IEnumerable_1_t8958_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8957_0_0_0;
extern Il2CppType IList_1_t8957_1_0_0;
struct IList_1_t8957;
extern Il2CppGenericClass IList_1_t8957_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8957_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8957_MethodInfos/* methods */
	, IList_1_t8957_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8957_il2cpp_TypeInfo/* element_class */
	, IList_1_t8957_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8957_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8957_0_0_0/* byval_arg */
	, &IList_1_t8957_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8957_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7005_il2cpp_TypeInfo;

// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50496_MethodInfo;
static PropertyInfo IEnumerator_1_t7005____Current_PropertyInfo = 
{
	&IEnumerator_1_t7005_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50496_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7005_PropertyInfos[] =
{
	&IEnumerator_1_t7005____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyDelaySignAttribute_t1284_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50496_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50496_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7005_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyDelaySignAttribute_t1284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50496_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7005_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50496_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7005_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7005_0_0_0;
extern Il2CppType IEnumerator_1_t7005_1_0_0;
struct IEnumerator_1_t7005;
extern Il2CppGenericClass IEnumerator_1_t7005_GenericClass;
TypeInfo IEnumerator_1_t7005_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7005_MethodInfos/* methods */
	, IEnumerator_1_t7005_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7005_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7005_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7005_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7005_0_0_0/* byval_arg */
	, &IEnumerator_1_t7005_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7005_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_637.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5020_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_637MethodDeclarations.h"

extern TypeInfo AssemblyDelaySignAttribute_t1284_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30686_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyDelaySignAttribute_t1284_m39887_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyDelaySignAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyDelaySignAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAssemblyDelaySignAttribute_t1284_m39887(__this, p0, method) (AssemblyDelaySignAttribute_t1284 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5020____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5020_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5020, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5020____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5020_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5020, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5020_FieldInfos[] =
{
	&InternalEnumerator_1_t5020____array_0_FieldInfo,
	&InternalEnumerator_1_t5020____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30683_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5020____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5020_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30683_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5020____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5020_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30686_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5020_PropertyInfos[] =
{
	&InternalEnumerator_1_t5020____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5020____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5020_InternalEnumerator_1__ctor_m30682_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30682_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30682_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5020_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5020_InternalEnumerator_1__ctor_m30682_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30682_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30683_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30683_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5020_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30683_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30684_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30684_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5020_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30684_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30685_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30685_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5020_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30685_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDelaySignAttribute_t1284_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30686_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyDelaySignAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30686_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5020_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyDelaySignAttribute_t1284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30686_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5020_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30682_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30683_MethodInfo,
	&InternalEnumerator_1_Dispose_m30684_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30685_MethodInfo,
	&InternalEnumerator_1_get_Current_m30686_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30685_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30684_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5020_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30683_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30685_MethodInfo,
	&InternalEnumerator_1_Dispose_m30684_MethodInfo,
	&InternalEnumerator_1_get_Current_m30686_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5020_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t7005_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5020_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7005_il2cpp_TypeInfo, 7},
};
extern TypeInfo AssemblyDelaySignAttribute_t1284_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5020_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30686_MethodInfo/* Method Usage */,
	&AssemblyDelaySignAttribute_t1284_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAssemblyDelaySignAttribute_t1284_m39887_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5020_0_0_0;
extern Il2CppType InternalEnumerator_1_t5020_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5020_GenericClass;
TypeInfo InternalEnumerator_1_t5020_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5020_MethodInfos/* methods */
	, InternalEnumerator_1_t5020_PropertyInfos/* properties */
	, InternalEnumerator_1_t5020_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5020_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5020_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5020_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5020_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5020_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5020_1_0_0/* this_arg */
	, InternalEnumerator_1_t5020_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5020_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5020_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5020)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8959_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>
extern MethodInfo ICollection_1_get_Count_m50497_MethodInfo;
static PropertyInfo ICollection_1_t8959____Count_PropertyInfo = 
{
	&ICollection_1_t8959_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50497_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50498_MethodInfo;
static PropertyInfo ICollection_1_t8959____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8959_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50498_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8959_PropertyInfos[] =
{
	&ICollection_1_t8959____Count_PropertyInfo,
	&ICollection_1_t8959____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50497_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50497_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8959_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50497_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50498_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50498_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8959_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50498_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDelaySignAttribute_t1284_0_0_0;
extern Il2CppType AssemblyDelaySignAttribute_t1284_0_0_0;
static ParameterInfo ICollection_1_t8959_ICollection_1_Add_m50499_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDelaySignAttribute_t1284_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50499_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50499_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8959_ICollection_1_Add_m50499_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50499_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50500_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50500_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50500_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDelaySignAttribute_t1284_0_0_0;
static ParameterInfo ICollection_1_t8959_ICollection_1_Contains_m50501_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDelaySignAttribute_t1284_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50501_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50501_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8959_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8959_ICollection_1_Contains_m50501_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50501_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDelaySignAttributeU5BU5D_t5312_0_0_0;
extern Il2CppType AssemblyDelaySignAttributeU5BU5D_t5312_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8959_ICollection_1_CopyTo_m50502_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDelaySignAttributeU5BU5D_t5312_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50502_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50502_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8959_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8959_ICollection_1_CopyTo_m50502_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50502_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDelaySignAttribute_t1284_0_0_0;
static ParameterInfo ICollection_1_t8959_ICollection_1_Remove_m50503_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDelaySignAttribute_t1284_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50503_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDelaySignAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50503_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8959_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8959_ICollection_1_Remove_m50503_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50503_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8959_MethodInfos[] =
{
	&ICollection_1_get_Count_m50497_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50498_MethodInfo,
	&ICollection_1_Add_m50499_MethodInfo,
	&ICollection_1_Clear_m50500_MethodInfo,
	&ICollection_1_Contains_m50501_MethodInfo,
	&ICollection_1_CopyTo_m50502_MethodInfo,
	&ICollection_1_Remove_m50503_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8961_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8959_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8961_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8959_0_0_0;
extern Il2CppType ICollection_1_t8959_1_0_0;
struct ICollection_1_t8959;
extern Il2CppGenericClass ICollection_1_t8959_GenericClass;
TypeInfo ICollection_1_t8959_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8959_MethodInfos/* methods */
	, ICollection_1_t8959_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8959_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8959_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8959_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8959_0_0_0/* byval_arg */
	, &ICollection_1_t8959_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8959_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyDelaySignAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyDelaySignAttribute>
extern Il2CppType IEnumerator_1_t7005_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50504_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyDelaySignAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50504_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8961_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7005_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50504_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8961_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50504_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8961_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8961_0_0_0;
extern Il2CppType IEnumerable_1_t8961_1_0_0;
struct IEnumerable_1_t8961;
extern Il2CppGenericClass IEnumerable_1_t8961_GenericClass;
TypeInfo IEnumerable_1_t8961_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8961_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8961_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8961_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8961_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8961_0_0_0/* byval_arg */
	, &IEnumerable_1_t8961_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8961_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8960_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyDelaySignAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDelaySignAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDelaySignAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyDelaySignAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDelaySignAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyDelaySignAttribute>
extern MethodInfo IList_1_get_Item_m50505_MethodInfo;
extern MethodInfo IList_1_set_Item_m50506_MethodInfo;
static PropertyInfo IList_1_t8960____Item_PropertyInfo = 
{
	&IList_1_t8960_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50505_MethodInfo/* get */
	, &IList_1_set_Item_m50506_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8960_PropertyInfos[] =
{
	&IList_1_t8960____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyDelaySignAttribute_t1284_0_0_0;
static ParameterInfo IList_1_t8960_IList_1_IndexOf_m50507_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDelaySignAttribute_t1284_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50507_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyDelaySignAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50507_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8960_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8960_IList_1_IndexOf_m50507_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50507_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyDelaySignAttribute_t1284_0_0_0;
static ParameterInfo IList_1_t8960_IList_1_Insert_m50508_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyDelaySignAttribute_t1284_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50508_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDelaySignAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50508_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8960_IList_1_Insert_m50508_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50508_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8960_IList_1_RemoveAt_m50509_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50509_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDelaySignAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50509_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8960_IList_1_RemoveAt_m50509_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50509_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8960_IList_1_get_Item_m50505_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AssemblyDelaySignAttribute_t1284_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50505_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyDelaySignAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50505_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8960_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyDelaySignAttribute_t1284_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8960_IList_1_get_Item_m50505_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50505_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyDelaySignAttribute_t1284_0_0_0;
static ParameterInfo IList_1_t8960_IList_1_set_Item_m50506_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyDelaySignAttribute_t1284_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50506_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDelaySignAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50506_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8960_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8960_IList_1_set_Item_m50506_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50506_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8960_MethodInfos[] =
{
	&IList_1_IndexOf_m50507_MethodInfo,
	&IList_1_Insert_m50508_MethodInfo,
	&IList_1_RemoveAt_m50509_MethodInfo,
	&IList_1_get_Item_m50505_MethodInfo,
	&IList_1_set_Item_m50506_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8960_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8959_il2cpp_TypeInfo,
	&IEnumerable_1_t8961_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8960_0_0_0;
extern Il2CppType IList_1_t8960_1_0_0;
struct IList_1_t8960;
extern Il2CppGenericClass IList_1_t8960_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8960_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8960_MethodInfos/* methods */
	, IList_1_t8960_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8960_il2cpp_TypeInfo/* element_class */
	, IList_1_t8960_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8960_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8960_0_0_0/* byval_arg */
	, &IList_1_t8960_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8960_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7007_il2cpp_TypeInfo;

// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50510_MethodInfo;
static PropertyInfo IEnumerator_1_t7007____Current_PropertyInfo = 
{
	&IEnumerator_1_t7007_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50510_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7007_PropertyInfos[] =
{
	&IEnumerator_1_t7007____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyDescriptionAttribute_t530_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50510_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50510_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7007_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyDescriptionAttribute_t530_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50510_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7007_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50510_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7007_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7007_0_0_0;
extern Il2CppType IEnumerator_1_t7007_1_0_0;
struct IEnumerator_1_t7007;
extern Il2CppGenericClass IEnumerator_1_t7007_GenericClass;
TypeInfo IEnumerator_1_t7007_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7007_MethodInfos/* methods */
	, IEnumerator_1_t7007_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7007_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7007_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7007_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7007_0_0_0/* byval_arg */
	, &IEnumerator_1_t7007_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7007_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_638.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5021_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_638MethodDeclarations.h"

extern TypeInfo AssemblyDescriptionAttribute_t530_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30691_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyDescriptionAttribute_t530_m39898_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyDescriptionAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyDescriptionAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAssemblyDescriptionAttribute_t530_m39898(__this, p0, method) (AssemblyDescriptionAttribute_t530 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5021____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5021_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5021, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5021____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5021_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5021, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5021_FieldInfos[] =
{
	&InternalEnumerator_1_t5021____array_0_FieldInfo,
	&InternalEnumerator_1_t5021____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30688_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5021____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5021_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30688_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5021____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5021_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30691_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5021_PropertyInfos[] =
{
	&InternalEnumerator_1_t5021____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5021____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5021_InternalEnumerator_1__ctor_m30687_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30687_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30687_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5021_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5021_InternalEnumerator_1__ctor_m30687_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30687_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30688_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30688_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5021_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30688_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30689_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30689_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5021_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30689_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30690_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30690_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5021_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30690_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDescriptionAttribute_t530_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30691_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyDescriptionAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30691_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5021_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyDescriptionAttribute_t530_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30691_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5021_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30687_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30688_MethodInfo,
	&InternalEnumerator_1_Dispose_m30689_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30690_MethodInfo,
	&InternalEnumerator_1_get_Current_m30691_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30690_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30689_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5021_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30688_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30690_MethodInfo,
	&InternalEnumerator_1_Dispose_m30689_MethodInfo,
	&InternalEnumerator_1_get_Current_m30691_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5021_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t7007_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5021_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7007_il2cpp_TypeInfo, 7},
};
extern TypeInfo AssemblyDescriptionAttribute_t530_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5021_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30691_MethodInfo/* Method Usage */,
	&AssemblyDescriptionAttribute_t530_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAssemblyDescriptionAttribute_t530_m39898_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5021_0_0_0;
extern Il2CppType InternalEnumerator_1_t5021_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5021_GenericClass;
TypeInfo InternalEnumerator_1_t5021_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5021_MethodInfos/* methods */
	, InternalEnumerator_1_t5021_PropertyInfos/* properties */
	, InternalEnumerator_1_t5021_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5021_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5021_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5021_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5021_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5021_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5021_1_0_0/* this_arg */
	, InternalEnumerator_1_t5021_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5021_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5021_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5021)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8962_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>
extern MethodInfo ICollection_1_get_Count_m50511_MethodInfo;
static PropertyInfo ICollection_1_t8962____Count_PropertyInfo = 
{
	&ICollection_1_t8962_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50511_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50512_MethodInfo;
static PropertyInfo ICollection_1_t8962____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8962_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50512_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8962_PropertyInfos[] =
{
	&ICollection_1_t8962____Count_PropertyInfo,
	&ICollection_1_t8962____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50511_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50511_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8962_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50511_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50512_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50512_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8962_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50512_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDescriptionAttribute_t530_0_0_0;
extern Il2CppType AssemblyDescriptionAttribute_t530_0_0_0;
static ParameterInfo ICollection_1_t8962_ICollection_1_Add_m50513_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDescriptionAttribute_t530_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50513_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50513_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8962_ICollection_1_Add_m50513_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50513_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50514_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50514_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50514_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDescriptionAttribute_t530_0_0_0;
static ParameterInfo ICollection_1_t8962_ICollection_1_Contains_m50515_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDescriptionAttribute_t530_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50515_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50515_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8962_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8962_ICollection_1_Contains_m50515_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50515_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDescriptionAttributeU5BU5D_t5313_0_0_0;
extern Il2CppType AssemblyDescriptionAttributeU5BU5D_t5313_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8962_ICollection_1_CopyTo_m50516_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDescriptionAttributeU5BU5D_t5313_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50516_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50516_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8962_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8962_ICollection_1_CopyTo_m50516_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50516_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyDescriptionAttribute_t530_0_0_0;
static ParameterInfo ICollection_1_t8962_ICollection_1_Remove_m50517_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDescriptionAttribute_t530_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50517_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyDescriptionAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50517_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8962_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8962_ICollection_1_Remove_m50517_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50517_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8962_MethodInfos[] =
{
	&ICollection_1_get_Count_m50511_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50512_MethodInfo,
	&ICollection_1_Add_m50513_MethodInfo,
	&ICollection_1_Clear_m50514_MethodInfo,
	&ICollection_1_Contains_m50515_MethodInfo,
	&ICollection_1_CopyTo_m50516_MethodInfo,
	&ICollection_1_Remove_m50517_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8964_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8962_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8964_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8962_0_0_0;
extern Il2CppType ICollection_1_t8962_1_0_0;
struct ICollection_1_t8962;
extern Il2CppGenericClass ICollection_1_t8962_GenericClass;
TypeInfo ICollection_1_t8962_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8962_MethodInfos/* methods */
	, ICollection_1_t8962_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8962_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8962_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8962_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8962_0_0_0/* byval_arg */
	, &ICollection_1_t8962_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8962_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyDescriptionAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyDescriptionAttribute>
extern Il2CppType IEnumerator_1_t7007_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50518_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyDescriptionAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50518_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8964_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7007_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50518_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8964_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50518_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8964_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8964_0_0_0;
extern Il2CppType IEnumerable_1_t8964_1_0_0;
struct IEnumerable_1_t8964;
extern Il2CppGenericClass IEnumerable_1_t8964_GenericClass;
TypeInfo IEnumerable_1_t8964_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8964_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8964_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8964_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8964_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8964_0_0_0/* byval_arg */
	, &IEnumerable_1_t8964_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8964_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8963_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyDescriptionAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDescriptionAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDescriptionAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyDescriptionAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDescriptionAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyDescriptionAttribute>
extern MethodInfo IList_1_get_Item_m50519_MethodInfo;
extern MethodInfo IList_1_set_Item_m50520_MethodInfo;
static PropertyInfo IList_1_t8963____Item_PropertyInfo = 
{
	&IList_1_t8963_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50519_MethodInfo/* get */
	, &IList_1_set_Item_m50520_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8963_PropertyInfos[] =
{
	&IList_1_t8963____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyDescriptionAttribute_t530_0_0_0;
static ParameterInfo IList_1_t8963_IList_1_IndexOf_m50521_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyDescriptionAttribute_t530_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50521_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyDescriptionAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50521_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8963_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8963_IList_1_IndexOf_m50521_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50521_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyDescriptionAttribute_t530_0_0_0;
static ParameterInfo IList_1_t8963_IList_1_Insert_m50522_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyDescriptionAttribute_t530_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50522_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDescriptionAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50522_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8963_IList_1_Insert_m50522_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50522_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8963_IList_1_RemoveAt_m50523_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50523_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDescriptionAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50523_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8963_IList_1_RemoveAt_m50523_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50523_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8963_IList_1_get_Item_m50519_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AssemblyDescriptionAttribute_t530_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50519_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyDescriptionAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50519_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8963_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyDescriptionAttribute_t530_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8963_IList_1_get_Item_m50519_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50519_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyDescriptionAttribute_t530_0_0_0;
static ParameterInfo IList_1_t8963_IList_1_set_Item_m50520_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyDescriptionAttribute_t530_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50520_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyDescriptionAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50520_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8963_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8963_IList_1_set_Item_m50520_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50520_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8963_MethodInfos[] =
{
	&IList_1_IndexOf_m50521_MethodInfo,
	&IList_1_Insert_m50522_MethodInfo,
	&IList_1_RemoveAt_m50523_MethodInfo,
	&IList_1_get_Item_m50519_MethodInfo,
	&IList_1_set_Item_m50520_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8963_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8962_il2cpp_TypeInfo,
	&IEnumerable_1_t8964_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8963_0_0_0;
extern Il2CppType IList_1_t8963_1_0_0;
struct IList_1_t8963;
extern Il2CppGenericClass IList_1_t8963_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8963_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8963_MethodInfos/* methods */
	, IList_1_t8963_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8963_il2cpp_TypeInfo/* element_class */
	, IList_1_t8963_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8963_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8963_0_0_0/* byval_arg */
	, &IList_1_t8963_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8963_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7009_il2cpp_TypeInfo;

// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50524_MethodInfo;
static PropertyInfo IEnumerator_1_t7009____Current_PropertyInfo = 
{
	&IEnumerator_1_t7009_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50524_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7009_PropertyInfos[] =
{
	&IEnumerator_1_t7009____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyFileVersionAttribute_t535_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50524_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50524_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7009_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyFileVersionAttribute_t535_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50524_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7009_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50524_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7009_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7009_0_0_0;
extern Il2CppType IEnumerator_1_t7009_1_0_0;
struct IEnumerator_1_t7009;
extern Il2CppGenericClass IEnumerator_1_t7009_GenericClass;
TypeInfo IEnumerator_1_t7009_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7009_MethodInfos/* methods */
	, IEnumerator_1_t7009_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7009_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7009_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7009_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7009_0_0_0/* byval_arg */
	, &IEnumerator_1_t7009_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7009_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_639.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5022_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_639MethodDeclarations.h"

extern TypeInfo AssemblyFileVersionAttribute_t535_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30696_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyFileVersionAttribute_t535_m39909_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyFileVersionAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyFileVersionAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAssemblyFileVersionAttribute_t535_m39909(__this, p0, method) (AssemblyFileVersionAttribute_t535 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5022____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5022_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5022, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5022____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5022_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5022, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5022_FieldInfos[] =
{
	&InternalEnumerator_1_t5022____array_0_FieldInfo,
	&InternalEnumerator_1_t5022____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30693_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5022____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5022_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30693_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5022____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5022_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30696_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5022_PropertyInfos[] =
{
	&InternalEnumerator_1_t5022____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5022____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5022_InternalEnumerator_1__ctor_m30692_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30692_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30692_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5022_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5022_InternalEnumerator_1__ctor_m30692_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30692_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30693_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30693_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5022_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30693_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30694_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30694_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5022_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30694_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30695_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30695_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5022_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30695_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyFileVersionAttribute_t535_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30696_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyFileVersionAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30696_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5022_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyFileVersionAttribute_t535_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30696_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5022_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30692_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30693_MethodInfo,
	&InternalEnumerator_1_Dispose_m30694_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30695_MethodInfo,
	&InternalEnumerator_1_get_Current_m30696_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30695_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30694_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5022_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30693_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30695_MethodInfo,
	&InternalEnumerator_1_Dispose_m30694_MethodInfo,
	&InternalEnumerator_1_get_Current_m30696_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5022_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t7009_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5022_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7009_il2cpp_TypeInfo, 7},
};
extern TypeInfo AssemblyFileVersionAttribute_t535_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5022_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30696_MethodInfo/* Method Usage */,
	&AssemblyFileVersionAttribute_t535_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAssemblyFileVersionAttribute_t535_m39909_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5022_0_0_0;
extern Il2CppType InternalEnumerator_1_t5022_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5022_GenericClass;
TypeInfo InternalEnumerator_1_t5022_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5022_MethodInfos/* methods */
	, InternalEnumerator_1_t5022_PropertyInfos/* properties */
	, InternalEnumerator_1_t5022_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5022_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5022_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5022_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5022_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5022_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5022_1_0_0/* this_arg */
	, InternalEnumerator_1_t5022_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5022_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5022_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5022)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8965_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>
extern MethodInfo ICollection_1_get_Count_m50525_MethodInfo;
static PropertyInfo ICollection_1_t8965____Count_PropertyInfo = 
{
	&ICollection_1_t8965_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50525_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50526_MethodInfo;
static PropertyInfo ICollection_1_t8965____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8965_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50526_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8965_PropertyInfos[] =
{
	&ICollection_1_t8965____Count_PropertyInfo,
	&ICollection_1_t8965____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50525_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50525_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8965_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50525_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50526_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50526_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8965_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50526_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyFileVersionAttribute_t535_0_0_0;
extern Il2CppType AssemblyFileVersionAttribute_t535_0_0_0;
static ParameterInfo ICollection_1_t8965_ICollection_1_Add_m50527_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyFileVersionAttribute_t535_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50527_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50527_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8965_ICollection_1_Add_m50527_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50527_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50528_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50528_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50528_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyFileVersionAttribute_t535_0_0_0;
static ParameterInfo ICollection_1_t8965_ICollection_1_Contains_m50529_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyFileVersionAttribute_t535_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50529_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50529_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8965_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8965_ICollection_1_Contains_m50529_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50529_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyFileVersionAttributeU5BU5D_t5314_0_0_0;
extern Il2CppType AssemblyFileVersionAttributeU5BU5D_t5314_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8965_ICollection_1_CopyTo_m50530_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyFileVersionAttributeU5BU5D_t5314_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50530_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50530_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8965_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8965_ICollection_1_CopyTo_m50530_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50530_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyFileVersionAttribute_t535_0_0_0;
static ParameterInfo ICollection_1_t8965_ICollection_1_Remove_m50531_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyFileVersionAttribute_t535_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50531_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyFileVersionAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50531_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8965_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8965_ICollection_1_Remove_m50531_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50531_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8965_MethodInfos[] =
{
	&ICollection_1_get_Count_m50525_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50526_MethodInfo,
	&ICollection_1_Add_m50527_MethodInfo,
	&ICollection_1_Clear_m50528_MethodInfo,
	&ICollection_1_Contains_m50529_MethodInfo,
	&ICollection_1_CopyTo_m50530_MethodInfo,
	&ICollection_1_Remove_m50531_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8967_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8965_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8967_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8965_0_0_0;
extern Il2CppType ICollection_1_t8965_1_0_0;
struct ICollection_1_t8965;
extern Il2CppGenericClass ICollection_1_t8965_GenericClass;
TypeInfo ICollection_1_t8965_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8965_MethodInfos/* methods */
	, ICollection_1_t8965_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8965_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8965_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8965_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8965_0_0_0/* byval_arg */
	, &ICollection_1_t8965_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8965_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyFileVersionAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyFileVersionAttribute>
extern Il2CppType IEnumerator_1_t7009_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50532_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyFileVersionAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50532_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8967_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7009_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50532_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8967_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50532_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8967_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8967_0_0_0;
extern Il2CppType IEnumerable_1_t8967_1_0_0;
struct IEnumerable_1_t8967;
extern Il2CppGenericClass IEnumerable_1_t8967_GenericClass;
TypeInfo IEnumerable_1_t8967_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8967_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8967_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8967_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8967_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8967_0_0_0/* byval_arg */
	, &IEnumerable_1_t8967_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8967_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8966_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyFileVersionAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyFileVersionAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyFileVersionAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyFileVersionAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyFileVersionAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyFileVersionAttribute>
extern MethodInfo IList_1_get_Item_m50533_MethodInfo;
extern MethodInfo IList_1_set_Item_m50534_MethodInfo;
static PropertyInfo IList_1_t8966____Item_PropertyInfo = 
{
	&IList_1_t8966_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50533_MethodInfo/* get */
	, &IList_1_set_Item_m50534_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8966_PropertyInfos[] =
{
	&IList_1_t8966____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyFileVersionAttribute_t535_0_0_0;
static ParameterInfo IList_1_t8966_IList_1_IndexOf_m50535_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyFileVersionAttribute_t535_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50535_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyFileVersionAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50535_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8966_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8966_IList_1_IndexOf_m50535_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50535_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyFileVersionAttribute_t535_0_0_0;
static ParameterInfo IList_1_t8966_IList_1_Insert_m50536_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyFileVersionAttribute_t535_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50536_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyFileVersionAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50536_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8966_IList_1_Insert_m50536_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50536_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8966_IList_1_RemoveAt_m50537_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50537_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyFileVersionAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50537_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8966_IList_1_RemoveAt_m50537_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50537_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8966_IList_1_get_Item_m50533_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AssemblyFileVersionAttribute_t535_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50533_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyFileVersionAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50533_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8966_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyFileVersionAttribute_t535_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8966_IList_1_get_Item_m50533_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50533_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyFileVersionAttribute_t535_0_0_0;
static ParameterInfo IList_1_t8966_IList_1_set_Item_m50534_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyFileVersionAttribute_t535_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50534_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyFileVersionAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50534_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8966_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8966_IList_1_set_Item_m50534_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50534_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8966_MethodInfos[] =
{
	&IList_1_IndexOf_m50535_MethodInfo,
	&IList_1_Insert_m50536_MethodInfo,
	&IList_1_RemoveAt_m50537_MethodInfo,
	&IList_1_get_Item_m50533_MethodInfo,
	&IList_1_set_Item_m50534_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8966_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8965_il2cpp_TypeInfo,
	&IEnumerable_1_t8967_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8966_0_0_0;
extern Il2CppType IList_1_t8966_1_0_0;
struct IList_1_t8966;
extern Il2CppGenericClass IList_1_t8966_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8966_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8966_MethodInfos/* methods */
	, IList_1_t8966_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8966_il2cpp_TypeInfo/* element_class */
	, IList_1_t8966_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8966_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8966_0_0_0/* byval_arg */
	, &IList_1_t8966_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8966_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7011_il2cpp_TypeInfo;

// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50538_MethodInfo;
static PropertyInfo IEnumerator_1_t7011____Current_PropertyInfo = 
{
	&IEnumerator_1_t7011_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50538_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7011_PropertyInfos[] =
{
	&IEnumerator_1_t7011____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyInformationalVersionAttribute_t1280_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50538_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50538_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7011_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyInformationalVersionAttribute_t1280_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50538_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7011_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50538_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7011_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7011_0_0_0;
extern Il2CppType IEnumerator_1_t7011_1_0_0;
struct IEnumerator_1_t7011;
extern Il2CppGenericClass IEnumerator_1_t7011_GenericClass;
TypeInfo IEnumerator_1_t7011_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7011_MethodInfos/* methods */
	, IEnumerator_1_t7011_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7011_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7011_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7011_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7011_0_0_0/* byval_arg */
	, &IEnumerator_1_t7011_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7011_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_640.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5023_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_640MethodDeclarations.h"

extern TypeInfo AssemblyInformationalVersionAttribute_t1280_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30701_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyInformationalVersionAttribute_t1280_m39920_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyInformationalVersionAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyInformationalVersionAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAssemblyInformationalVersionAttribute_t1280_m39920(__this, p0, method) (AssemblyInformationalVersionAttribute_t1280 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5023____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5023_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5023, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5023____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5023_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5023, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5023_FieldInfos[] =
{
	&InternalEnumerator_1_t5023____array_0_FieldInfo,
	&InternalEnumerator_1_t5023____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30698_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5023____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5023_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30698_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5023____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5023_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30701_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5023_PropertyInfos[] =
{
	&InternalEnumerator_1_t5023____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5023____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5023_InternalEnumerator_1__ctor_m30697_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30697_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30697_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5023_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5023_InternalEnumerator_1__ctor_m30697_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30697_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30698_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30698_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5023_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30698_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30699_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30699_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5023_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30699_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30700_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30700_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5023_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30700_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyInformationalVersionAttribute_t1280_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30701_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyInformationalVersionAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30701_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5023_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyInformationalVersionAttribute_t1280_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30701_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5023_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30697_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30698_MethodInfo,
	&InternalEnumerator_1_Dispose_m30699_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30700_MethodInfo,
	&InternalEnumerator_1_get_Current_m30701_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30700_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30699_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5023_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30698_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30700_MethodInfo,
	&InternalEnumerator_1_Dispose_m30699_MethodInfo,
	&InternalEnumerator_1_get_Current_m30701_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5023_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t7011_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5023_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7011_il2cpp_TypeInfo, 7},
};
extern TypeInfo AssemblyInformationalVersionAttribute_t1280_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5023_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30701_MethodInfo/* Method Usage */,
	&AssemblyInformationalVersionAttribute_t1280_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAssemblyInformationalVersionAttribute_t1280_m39920_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5023_0_0_0;
extern Il2CppType InternalEnumerator_1_t5023_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5023_GenericClass;
TypeInfo InternalEnumerator_1_t5023_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5023_MethodInfos/* methods */
	, InternalEnumerator_1_t5023_PropertyInfos/* properties */
	, InternalEnumerator_1_t5023_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5023_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5023_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5023_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5023_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5023_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5023_1_0_0/* this_arg */
	, InternalEnumerator_1_t5023_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5023_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5023_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5023)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8968_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>
extern MethodInfo ICollection_1_get_Count_m50539_MethodInfo;
static PropertyInfo ICollection_1_t8968____Count_PropertyInfo = 
{
	&ICollection_1_t8968_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50539_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50540_MethodInfo;
static PropertyInfo ICollection_1_t8968____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8968_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50540_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8968_PropertyInfos[] =
{
	&ICollection_1_t8968____Count_PropertyInfo,
	&ICollection_1_t8968____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50539_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50539_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8968_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50539_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50540_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50540_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8968_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50540_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyInformationalVersionAttribute_t1280_0_0_0;
extern Il2CppType AssemblyInformationalVersionAttribute_t1280_0_0_0;
static ParameterInfo ICollection_1_t8968_ICollection_1_Add_m50541_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyInformationalVersionAttribute_t1280_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50541_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50541_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8968_ICollection_1_Add_m50541_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50541_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50542_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50542_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50542_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyInformationalVersionAttribute_t1280_0_0_0;
static ParameterInfo ICollection_1_t8968_ICollection_1_Contains_m50543_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyInformationalVersionAttribute_t1280_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50543_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50543_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8968_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8968_ICollection_1_Contains_m50543_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50543_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyInformationalVersionAttributeU5BU5D_t5315_0_0_0;
extern Il2CppType AssemblyInformationalVersionAttributeU5BU5D_t5315_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8968_ICollection_1_CopyTo_m50544_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyInformationalVersionAttributeU5BU5D_t5315_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50544_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50544_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8968_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8968_ICollection_1_CopyTo_m50544_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50544_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyInformationalVersionAttribute_t1280_0_0_0;
static ParameterInfo ICollection_1_t8968_ICollection_1_Remove_m50545_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyInformationalVersionAttribute_t1280_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50545_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyInformationalVersionAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50545_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8968_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8968_ICollection_1_Remove_m50545_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50545_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8968_MethodInfos[] =
{
	&ICollection_1_get_Count_m50539_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50540_MethodInfo,
	&ICollection_1_Add_m50541_MethodInfo,
	&ICollection_1_Clear_m50542_MethodInfo,
	&ICollection_1_Contains_m50543_MethodInfo,
	&ICollection_1_CopyTo_m50544_MethodInfo,
	&ICollection_1_Remove_m50545_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8970_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8968_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8970_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8968_0_0_0;
extern Il2CppType ICollection_1_t8968_1_0_0;
struct ICollection_1_t8968;
extern Il2CppGenericClass ICollection_1_t8968_GenericClass;
TypeInfo ICollection_1_t8968_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8968_MethodInfos/* methods */
	, ICollection_1_t8968_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8968_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8968_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8968_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8968_0_0_0/* byval_arg */
	, &ICollection_1_t8968_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8968_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyInformationalVersionAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyInformationalVersionAttribute>
extern Il2CppType IEnumerator_1_t7011_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50546_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyInformationalVersionAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50546_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8970_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7011_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50546_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8970_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50546_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8970_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8970_0_0_0;
extern Il2CppType IEnumerable_1_t8970_1_0_0;
struct IEnumerable_1_t8970;
extern Il2CppGenericClass IEnumerable_1_t8970_GenericClass;
TypeInfo IEnumerable_1_t8970_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8970_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8970_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8970_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8970_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8970_0_0_0/* byval_arg */
	, &IEnumerable_1_t8970_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8970_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8969_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyInformationalVersionAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyInformationalVersionAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyInformationalVersionAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyInformationalVersionAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyInformationalVersionAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyInformationalVersionAttribute>
extern MethodInfo IList_1_get_Item_m50547_MethodInfo;
extern MethodInfo IList_1_set_Item_m50548_MethodInfo;
static PropertyInfo IList_1_t8969____Item_PropertyInfo = 
{
	&IList_1_t8969_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50547_MethodInfo/* get */
	, &IList_1_set_Item_m50548_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8969_PropertyInfos[] =
{
	&IList_1_t8969____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyInformationalVersionAttribute_t1280_0_0_0;
static ParameterInfo IList_1_t8969_IList_1_IndexOf_m50549_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyInformationalVersionAttribute_t1280_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50549_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyInformationalVersionAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50549_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8969_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8969_IList_1_IndexOf_m50549_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50549_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyInformationalVersionAttribute_t1280_0_0_0;
static ParameterInfo IList_1_t8969_IList_1_Insert_m50550_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyInformationalVersionAttribute_t1280_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50550_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyInformationalVersionAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50550_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8969_IList_1_Insert_m50550_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50550_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8969_IList_1_RemoveAt_m50551_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50551_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyInformationalVersionAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50551_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8969_IList_1_RemoveAt_m50551_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50551_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8969_IList_1_get_Item_m50547_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AssemblyInformationalVersionAttribute_t1280_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50547_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyInformationalVersionAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50547_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8969_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyInformationalVersionAttribute_t1280_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8969_IList_1_get_Item_m50547_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50547_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyInformationalVersionAttribute_t1280_0_0_0;
static ParameterInfo IList_1_t8969_IList_1_set_Item_m50548_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyInformationalVersionAttribute_t1280_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50548_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyInformationalVersionAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50548_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8969_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8969_IList_1_set_Item_m50548_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50548_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8969_MethodInfos[] =
{
	&IList_1_IndexOf_m50549_MethodInfo,
	&IList_1_Insert_m50550_MethodInfo,
	&IList_1_RemoveAt_m50551_MethodInfo,
	&IList_1_get_Item_m50547_MethodInfo,
	&IList_1_set_Item_m50548_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8969_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8968_il2cpp_TypeInfo,
	&IEnumerable_1_t8970_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8969_0_0_0;
extern Il2CppType IList_1_t8969_1_0_0;
struct IList_1_t8969;
extern Il2CppGenericClass IList_1_t8969_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8969_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8969_MethodInfos/* methods */
	, IList_1_t8969_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8969_il2cpp_TypeInfo/* element_class */
	, IList_1_t8969_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8969_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8969_0_0_0/* byval_arg */
	, &IList_1_t8969_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8969_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7013_il2cpp_TypeInfo;

// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50552_MethodInfo;
static PropertyInfo IEnumerator_1_t7013____Current_PropertyInfo = 
{
	&IEnumerator_1_t7013_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50552_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7013_PropertyInfos[] =
{
	&IEnumerator_1_t7013____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyKeyFileAttribute_t1283_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50552_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50552_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7013_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyKeyFileAttribute_t1283_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50552_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7013_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50552_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7013_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7013_0_0_0;
extern Il2CppType IEnumerator_1_t7013_1_0_0;
struct IEnumerator_1_t7013;
extern Il2CppGenericClass IEnumerator_1_t7013_GenericClass;
TypeInfo IEnumerator_1_t7013_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7013_MethodInfos/* methods */
	, IEnumerator_1_t7013_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7013_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7013_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7013_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7013_0_0_0/* byval_arg */
	, &IEnumerator_1_t7013_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7013_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_641.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5024_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_641MethodDeclarations.h"

extern TypeInfo AssemblyKeyFileAttribute_t1283_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30706_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyKeyFileAttribute_t1283_m39931_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyKeyFileAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyKeyFileAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAssemblyKeyFileAttribute_t1283_m39931(__this, p0, method) (AssemblyKeyFileAttribute_t1283 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5024____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5024, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5024____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5024, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5024_FieldInfos[] =
{
	&InternalEnumerator_1_t5024____array_0_FieldInfo,
	&InternalEnumerator_1_t5024____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30703_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5024____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5024_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30703_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5024____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5024_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30706_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5024_PropertyInfos[] =
{
	&InternalEnumerator_1_t5024____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5024____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5024_InternalEnumerator_1__ctor_m30702_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30702_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30702_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5024_InternalEnumerator_1__ctor_m30702_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30702_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30703_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30703_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30703_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30704_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30704_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30704_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30705_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30705_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30705_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyKeyFileAttribute_t1283_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30706_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyKeyFileAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30706_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyKeyFileAttribute_t1283_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30706_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5024_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30702_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30703_MethodInfo,
	&InternalEnumerator_1_Dispose_m30704_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30705_MethodInfo,
	&InternalEnumerator_1_get_Current_m30706_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30705_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30704_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5024_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30703_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30705_MethodInfo,
	&InternalEnumerator_1_Dispose_m30704_MethodInfo,
	&InternalEnumerator_1_get_Current_m30706_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5024_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t7013_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5024_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7013_il2cpp_TypeInfo, 7},
};
extern TypeInfo AssemblyKeyFileAttribute_t1283_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5024_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30706_MethodInfo/* Method Usage */,
	&AssemblyKeyFileAttribute_t1283_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAssemblyKeyFileAttribute_t1283_m39931_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5024_0_0_0;
extern Il2CppType InternalEnumerator_1_t5024_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5024_GenericClass;
TypeInfo InternalEnumerator_1_t5024_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5024_MethodInfos/* methods */
	, InternalEnumerator_1_t5024_PropertyInfos/* properties */
	, InternalEnumerator_1_t5024_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5024_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5024_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5024_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5024_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5024_1_0_0/* this_arg */
	, InternalEnumerator_1_t5024_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5024_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5024_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5024)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8971_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>
extern MethodInfo ICollection_1_get_Count_m50553_MethodInfo;
static PropertyInfo ICollection_1_t8971____Count_PropertyInfo = 
{
	&ICollection_1_t8971_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50553_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50554_MethodInfo;
static PropertyInfo ICollection_1_t8971____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8971_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50554_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8971_PropertyInfos[] =
{
	&ICollection_1_t8971____Count_PropertyInfo,
	&ICollection_1_t8971____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50553_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50553_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8971_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50553_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50554_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50554_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8971_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50554_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyKeyFileAttribute_t1283_0_0_0;
extern Il2CppType AssemblyKeyFileAttribute_t1283_0_0_0;
static ParameterInfo ICollection_1_t8971_ICollection_1_Add_m50555_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyKeyFileAttribute_t1283_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50555_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50555_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8971_ICollection_1_Add_m50555_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50555_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50556_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50556_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50556_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyKeyFileAttribute_t1283_0_0_0;
static ParameterInfo ICollection_1_t8971_ICollection_1_Contains_m50557_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyKeyFileAttribute_t1283_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50557_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50557_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8971_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8971_ICollection_1_Contains_m50557_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50557_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyKeyFileAttributeU5BU5D_t5316_0_0_0;
extern Il2CppType AssemblyKeyFileAttributeU5BU5D_t5316_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8971_ICollection_1_CopyTo_m50558_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyKeyFileAttributeU5BU5D_t5316_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50558_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50558_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8971_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8971_ICollection_1_CopyTo_m50558_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50558_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyKeyFileAttribute_t1283_0_0_0;
static ParameterInfo ICollection_1_t8971_ICollection_1_Remove_m50559_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyKeyFileAttribute_t1283_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50559_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyKeyFileAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50559_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8971_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8971_ICollection_1_Remove_m50559_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50559_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8971_MethodInfos[] =
{
	&ICollection_1_get_Count_m50553_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50554_MethodInfo,
	&ICollection_1_Add_m50555_MethodInfo,
	&ICollection_1_Clear_m50556_MethodInfo,
	&ICollection_1_Contains_m50557_MethodInfo,
	&ICollection_1_CopyTo_m50558_MethodInfo,
	&ICollection_1_Remove_m50559_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8973_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8971_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8973_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8971_0_0_0;
extern Il2CppType ICollection_1_t8971_1_0_0;
struct ICollection_1_t8971;
extern Il2CppGenericClass ICollection_1_t8971_GenericClass;
TypeInfo ICollection_1_t8971_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8971_MethodInfos/* methods */
	, ICollection_1_t8971_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8971_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8971_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8971_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8971_0_0_0/* byval_arg */
	, &ICollection_1_t8971_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8971_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyKeyFileAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyKeyFileAttribute>
extern Il2CppType IEnumerator_1_t7013_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50560_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyKeyFileAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50560_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8973_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7013_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50560_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8973_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50560_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8973_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8973_0_0_0;
extern Il2CppType IEnumerable_1_t8973_1_0_0;
struct IEnumerable_1_t8973;
extern Il2CppGenericClass IEnumerable_1_t8973_GenericClass;
TypeInfo IEnumerable_1_t8973_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8973_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8973_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8973_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8973_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8973_0_0_0/* byval_arg */
	, &IEnumerable_1_t8973_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8973_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8972_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyKeyFileAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyKeyFileAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyKeyFileAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyKeyFileAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyKeyFileAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyKeyFileAttribute>
extern MethodInfo IList_1_get_Item_m50561_MethodInfo;
extern MethodInfo IList_1_set_Item_m50562_MethodInfo;
static PropertyInfo IList_1_t8972____Item_PropertyInfo = 
{
	&IList_1_t8972_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50561_MethodInfo/* get */
	, &IList_1_set_Item_m50562_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8972_PropertyInfos[] =
{
	&IList_1_t8972____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyKeyFileAttribute_t1283_0_0_0;
static ParameterInfo IList_1_t8972_IList_1_IndexOf_m50563_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyKeyFileAttribute_t1283_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50563_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyKeyFileAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50563_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8972_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8972_IList_1_IndexOf_m50563_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50563_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyKeyFileAttribute_t1283_0_0_0;
static ParameterInfo IList_1_t8972_IList_1_Insert_m50564_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyKeyFileAttribute_t1283_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50564_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyKeyFileAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50564_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8972_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8972_IList_1_Insert_m50564_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50564_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8972_IList_1_RemoveAt_m50565_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50565_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyKeyFileAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50565_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8972_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8972_IList_1_RemoveAt_m50565_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50565_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8972_IList_1_get_Item_m50561_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AssemblyKeyFileAttribute_t1283_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50561_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyKeyFileAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50561_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8972_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyKeyFileAttribute_t1283_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8972_IList_1_get_Item_m50561_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50561_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyKeyFileAttribute_t1283_0_0_0;
static ParameterInfo IList_1_t8972_IList_1_set_Item_m50562_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyKeyFileAttribute_t1283_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50562_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyKeyFileAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50562_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8972_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8972_IList_1_set_Item_m50562_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50562_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8972_MethodInfos[] =
{
	&IList_1_IndexOf_m50563_MethodInfo,
	&IList_1_Insert_m50564_MethodInfo,
	&IList_1_RemoveAt_m50565_MethodInfo,
	&IList_1_get_Item_m50561_MethodInfo,
	&IList_1_set_Item_m50562_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8972_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8971_il2cpp_TypeInfo,
	&IEnumerable_1_t8973_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8972_0_0_0;
extern Il2CppType IList_1_t8972_1_0_0;
struct IList_1_t8972;
extern Il2CppGenericClass IList_1_t8972_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8972_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8972_MethodInfos/* methods */
	, IList_1_t8972_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8972_il2cpp_TypeInfo/* element_class */
	, IList_1_t8972_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8972_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8972_0_0_0/* byval_arg */
	, &IList_1_t8972_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8972_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7015_il2cpp_TypeInfo;

// System.Reflection.AssemblyNameFlags
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyNameFlags>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyNameFlags>
extern MethodInfo IEnumerator_1_get_Current_m50566_MethodInfo;
static PropertyInfo IEnumerator_1_t7015____Current_PropertyInfo = 
{
	&IEnumerator_1_t7015_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50566_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7015_PropertyInfos[] =
{
	&IEnumerator_1_t7015____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyNameFlags_t1920_0_0_0;
extern void* RuntimeInvoker_AssemblyNameFlags_t1920 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50566_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyNameFlags>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50566_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7015_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyNameFlags_t1920_0_0_0/* return_type */
	, RuntimeInvoker_AssemblyNameFlags_t1920/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50566_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7015_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50566_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7015_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7015_0_0_0;
extern Il2CppType IEnumerator_1_t7015_1_0_0;
struct IEnumerator_1_t7015;
extern Il2CppGenericClass IEnumerator_1_t7015_GenericClass;
TypeInfo IEnumerator_1_t7015_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7015_MethodInfos/* methods */
	, IEnumerator_1_t7015_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7015_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7015_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7015_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7015_0_0_0/* byval_arg */
	, &IEnumerator_1_t7015_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7015_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_642.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5025_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_642MethodDeclarations.h"

extern TypeInfo AssemblyNameFlags_t1920_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30711_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyNameFlags_t1920_m39942_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyNameFlags>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyNameFlags>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisAssemblyNameFlags_t1920_m39942 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m30707_MethodInfo;
 void InternalEnumerator_1__ctor_m30707 (InternalEnumerator_1_t5025 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30708_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30708 (InternalEnumerator_1_t5025 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m30711(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m30711_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&AssemblyNameFlags_t1920_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m30709_MethodInfo;
 void InternalEnumerator_1_Dispose_m30709 (InternalEnumerator_1_t5025 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m30710_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m30710 (InternalEnumerator_1_t5025 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30711 (InternalEnumerator_1_t5025 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisAssemblyNameFlags_t1920_m39942(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisAssemblyNameFlags_t1920_m39942_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5025____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5025, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5025____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5025, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5025_FieldInfos[] =
{
	&InternalEnumerator_1_t5025____array_0_FieldInfo,
	&InternalEnumerator_1_t5025____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t5025____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5025_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30708_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5025____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5025_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30711_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5025_PropertyInfos[] =
{
	&InternalEnumerator_1_t5025____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5025____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5025_InternalEnumerator_1__ctor_m30707_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30707_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30707_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m30707/* method */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5025_InternalEnumerator_1__ctor_m30707_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30707_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30708_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30708_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30708/* method */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30708_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30709_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30709_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m30709/* method */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30709_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30710_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30710_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m30710/* method */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30710_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyNameFlags_t1920_0_0_0;
extern void* RuntimeInvoker_AssemblyNameFlags_t1920 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30711_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyNameFlags>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30711_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m30711/* method */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyNameFlags_t1920_0_0_0/* return_type */
	, RuntimeInvoker_AssemblyNameFlags_t1920/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30711_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5025_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30707_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30708_MethodInfo,
	&InternalEnumerator_1_Dispose_m30709_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30710_MethodInfo,
	&InternalEnumerator_1_get_Current_m30711_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t5025_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30708_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30710_MethodInfo,
	&InternalEnumerator_1_Dispose_m30709_MethodInfo,
	&InternalEnumerator_1_get_Current_m30711_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5025_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t7015_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5025_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7015_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5025_0_0_0;
extern Il2CppType InternalEnumerator_1_t5025_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5025_GenericClass;
TypeInfo InternalEnumerator_1_t5025_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5025_MethodInfos/* methods */
	, InternalEnumerator_1_t5025_PropertyInfos/* properties */
	, InternalEnumerator_1_t5025_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5025_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5025_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5025_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5025_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5025_1_0_0/* this_arg */
	, InternalEnumerator_1_t5025_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5025_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5025)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8974_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>
extern MethodInfo ICollection_1_get_Count_m50567_MethodInfo;
static PropertyInfo ICollection_1_t8974____Count_PropertyInfo = 
{
	&ICollection_1_t8974_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50567_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50568_MethodInfo;
static PropertyInfo ICollection_1_t8974____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8974_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50568_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8974_PropertyInfos[] =
{
	&ICollection_1_t8974____Count_PropertyInfo,
	&ICollection_1_t8974____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50567_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::get_Count()
MethodInfo ICollection_1_get_Count_m50567_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8974_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50567_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50568_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50568_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8974_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50568_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyNameFlags_t1920_0_0_0;
extern Il2CppType AssemblyNameFlags_t1920_0_0_0;
static ParameterInfo ICollection_1_t8974_ICollection_1_Add_m50569_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlags_t1920_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50569_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Add(T)
MethodInfo ICollection_1_Add_m50569_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8974_ICollection_1_Add_m50569_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50569_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50570_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Clear()
MethodInfo ICollection_1_Clear_m50570_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50570_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyNameFlags_t1920_0_0_0;
static ParameterInfo ICollection_1_t8974_ICollection_1_Contains_m50571_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlags_t1920_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50571_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Contains(T)
MethodInfo ICollection_1_Contains_m50571_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8974_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8974_ICollection_1_Contains_m50571_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50571_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyNameFlagsU5BU5D_t5317_0_0_0;
extern Il2CppType AssemblyNameFlagsU5BU5D_t5317_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8974_ICollection_1_CopyTo_m50572_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlagsU5BU5D_t5317_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50572_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50572_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8974_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8974_ICollection_1_CopyTo_m50572_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50572_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyNameFlags_t1920_0_0_0;
static ParameterInfo ICollection_1_t8974_ICollection_1_Remove_m50573_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlags_t1920_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50573_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyNameFlags>::Remove(T)
MethodInfo ICollection_1_Remove_m50573_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8974_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8974_ICollection_1_Remove_m50573_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50573_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8974_MethodInfos[] =
{
	&ICollection_1_get_Count_m50567_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50568_MethodInfo,
	&ICollection_1_Add_m50569_MethodInfo,
	&ICollection_1_Clear_m50570_MethodInfo,
	&ICollection_1_Contains_m50571_MethodInfo,
	&ICollection_1_CopyTo_m50572_MethodInfo,
	&ICollection_1_Remove_m50573_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8976_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8974_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8976_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8974_0_0_0;
extern Il2CppType ICollection_1_t8974_1_0_0;
struct ICollection_1_t8974;
extern Il2CppGenericClass ICollection_1_t8974_GenericClass;
TypeInfo ICollection_1_t8974_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8974_MethodInfos/* methods */
	, ICollection_1_t8974_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8974_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8974_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8974_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8974_0_0_0/* byval_arg */
	, &ICollection_1_t8974_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8974_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyNameFlags>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyNameFlags>
extern Il2CppType IEnumerator_1_t7015_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m50574_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Reflection.AssemblyNameFlags>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m50574_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8976_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t7015_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m50574_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8976_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m50574_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8976_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8976_0_0_0;
extern Il2CppType IEnumerable_1_t8976_1_0_0;
struct IEnumerable_1_t8976;
extern Il2CppGenericClass IEnumerable_1_t8976_GenericClass;
TypeInfo IEnumerable_1_t8976_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8976_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8976_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8976_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8976_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8976_0_0_0/* byval_arg */
	, &IEnumerable_1_t8976_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8976_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8975_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>
extern MethodInfo IList_1_get_Item_m50575_MethodInfo;
extern MethodInfo IList_1_set_Item_m50576_MethodInfo;
static PropertyInfo IList_1_t8975____Item_PropertyInfo = 
{
	&IList_1_t8975_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m50575_MethodInfo/* get */
	, &IList_1_set_Item_m50576_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8975_PropertyInfos[] =
{
	&IList_1_t8975____Item_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyNameFlags_t1920_0_0_0;
static ParameterInfo IList_1_t8975_IList_1_IndexOf_m50577_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlags_t1920_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m50577_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::IndexOf(T)
MethodInfo IList_1_IndexOf_m50577_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8975_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8975_IList_1_IndexOf_m50577_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m50577_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyNameFlags_t1920_0_0_0;
static ParameterInfo IList_1_t8975_IList_1_Insert_m50578_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlags_t1920_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m50578_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m50578_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8975_IList_1_Insert_m50578_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m50578_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8975_IList_1_RemoveAt_m50579_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m50579_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m50579_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8975_IList_1_RemoveAt_m50579_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m50579_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8975_IList_1_get_Item_m50575_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AssemblyNameFlags_t1920_0_0_0;
extern void* RuntimeInvoker_AssemblyNameFlags_t1920_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m50575_GenericMethod;
// T System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m50575_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8975_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyNameFlags_t1920_0_0_0/* return_type */
	, RuntimeInvoker_AssemblyNameFlags_t1920_Int32_t93/* invoker_method */
	, IList_1_t8975_IList_1_get_Item_m50575_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m50575_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AssemblyNameFlags_t1920_0_0_0;
static ParameterInfo IList_1_t8975_IList_1_set_Item_m50576_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AssemblyNameFlags_t1920_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m50576_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Reflection.AssemblyNameFlags>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m50576_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8975_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8975_IList_1_set_Item_m50576_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m50576_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8975_MethodInfos[] =
{
	&IList_1_IndexOf_m50577_MethodInfo,
	&IList_1_Insert_m50578_MethodInfo,
	&IList_1_RemoveAt_m50579_MethodInfo,
	&IList_1_get_Item_m50575_MethodInfo,
	&IList_1_set_Item_m50576_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8975_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8974_il2cpp_TypeInfo,
	&IEnumerable_1_t8976_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8975_0_0_0;
extern Il2CppType IList_1_t8975_1_0_0;
struct IList_1_t8975;
extern Il2CppGenericClass IList_1_t8975_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8975_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8975_MethodInfos/* methods */
	, IList_1_t8975_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8975_il2cpp_TypeInfo/* element_class */
	, IList_1_t8975_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8975_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8975_0_0_0/* byval_arg */
	, &IList_1_t8975_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8975_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t7017_il2cpp_TypeInfo;

// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyProductAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyProductAttribute>
extern MethodInfo IEnumerator_1_get_Current_m50580_MethodInfo;
static PropertyInfo IEnumerator_1_t7017____Current_PropertyInfo = 
{
	&IEnumerator_1_t7017_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m50580_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t7017_PropertyInfos[] =
{
	&IEnumerator_1_t7017____Current_PropertyInfo,
	NULL
};
extern Il2CppType AssemblyProductAttribute_t533_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m50580_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Reflection.AssemblyProductAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m50580_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t7017_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyProductAttribute_t533_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m50580_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t7017_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m50580_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t7017_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t7017_0_0_0;
extern Il2CppType IEnumerator_1_t7017_1_0_0;
struct IEnumerator_1_t7017;
extern Il2CppGenericClass IEnumerator_1_t7017_GenericClass;
TypeInfo IEnumerator_1_t7017_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t7017_MethodInfos/* methods */
	, IEnumerator_1_t7017_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t7017_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t7017_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t7017_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t7017_0_0_0/* byval_arg */
	, &IEnumerator_1_t7017_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t7017_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_643.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t5026_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_643MethodDeclarations.h"

extern TypeInfo AssemblyProductAttribute_t533_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m30716_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAssemblyProductAttribute_t533_m39953_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyProductAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.AssemblyProductAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisAssemblyProductAttribute_t533_m39953(__this, p0, method) (AssemblyProductAttribute_t533 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t5026____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5026, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t5026____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t5026, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t5026_FieldInfos[] =
{
	&InternalEnumerator_1_t5026____array_0_FieldInfo,
	&InternalEnumerator_1_t5026____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30713_MethodInfo;
static PropertyInfo InternalEnumerator_1_t5026____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5026_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30713_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t5026____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t5026_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m30716_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t5026_PropertyInfos[] =
{
	&InternalEnumerator_1_t5026____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t5026____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t5026_InternalEnumerator_1__ctor_m30712_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m30712_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m30712_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t5026_InternalEnumerator_1__ctor_m30712_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m30712_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30713_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30713_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30713_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m30714_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m30714_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m30714_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m30715_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m30715_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m30715_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyProductAttribute_t533_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m30716_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Reflection.AssemblyProductAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m30716_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* declaring_type */
	, &AssemblyProductAttribute_t533_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m30716_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t5026_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m30712_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30713_MethodInfo,
	&InternalEnumerator_1_Dispose_m30714_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30715_MethodInfo,
	&InternalEnumerator_1_get_Current_m30716_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m30715_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m30714_MethodInfo;
static MethodInfo* InternalEnumerator_1_t5026_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30713_MethodInfo,
	&InternalEnumerator_1_MoveNext_m30715_MethodInfo,
	&InternalEnumerator_1_Dispose_m30714_MethodInfo,
	&InternalEnumerator_1_get_Current_m30716_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t5026_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t7017_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t5026_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t7017_il2cpp_TypeInfo, 7},
};
extern TypeInfo AssemblyProductAttribute_t533_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t5026_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m30716_MethodInfo/* Method Usage */,
	&AssemblyProductAttribute_t533_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisAssemblyProductAttribute_t533_m39953_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t5026_0_0_0;
extern Il2CppType InternalEnumerator_1_t5026_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t5026_GenericClass;
TypeInfo InternalEnumerator_1_t5026_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t5026_MethodInfos/* methods */
	, InternalEnumerator_1_t5026_PropertyInfos/* properties */
	, InternalEnumerator_1_t5026_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t5026_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t5026_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t5026_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t5026_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t5026_1_0_0/* this_arg */
	, InternalEnumerator_1_t5026_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t5026_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t5026_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t5026)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8977_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>
extern MethodInfo ICollection_1_get_Count_m50581_MethodInfo;
static PropertyInfo ICollection_1_t8977____Count_PropertyInfo = 
{
	&ICollection_1_t8977_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m50581_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m50582_MethodInfo;
static PropertyInfo ICollection_1_t8977____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8977_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m50582_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8977_PropertyInfos[] =
{
	&ICollection_1_t8977____Count_PropertyInfo,
	&ICollection_1_t8977____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m50581_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m50581_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8977_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m50581_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m50582_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m50582_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8977_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m50582_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyProductAttribute_t533_0_0_0;
extern Il2CppType AssemblyProductAttribute_t533_0_0_0;
static ParameterInfo ICollection_1_t8977_ICollection_1_Add_m50583_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyProductAttribute_t533_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m50583_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Add(T)
MethodInfo ICollection_1_Add_m50583_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8977_ICollection_1_Add_m50583_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m50583_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m50584_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Clear()
MethodInfo ICollection_1_Clear_m50584_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m50584_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyProductAttribute_t533_0_0_0;
static ParameterInfo ICollection_1_t8977_ICollection_1_Contains_m50585_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyProductAttribute_t533_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m50585_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m50585_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8977_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8977_ICollection_1_Contains_m50585_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m50585_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyProductAttributeU5BU5D_t5318_0_0_0;
extern Il2CppType AssemblyProductAttributeU5BU5D_t5318_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8977_ICollection_1_CopyTo_m50586_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyProductAttributeU5BU5D_t5318_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m50586_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m50586_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8977_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8977_ICollection_1_CopyTo_m50586_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m50586_GenericMethod/* genericMethod */

};
extern Il2CppType AssemblyProductAttribute_t533_0_0_0;
static ParameterInfo ICollection_1_t8977_ICollection_1_Remove_m50587_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AssemblyProductAttribute_t533_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m50587_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Reflection.AssemblyProductAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m50587_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8977_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8977_ICollection_1_Remove_m50587_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m50587_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8977_MethodInfos[] =
{
	&ICollection_1_get_Count_m50581_MethodInfo,
	&ICollection_1_get_IsReadOnly_m50582_MethodInfo,
	&ICollection_1_Add_m50583_MethodInfo,
	&ICollection_1_Clear_m50584_MethodInfo,
	&ICollection_1_Contains_m50585_MethodInfo,
	&ICollection_1_CopyTo_m50586_MethodInfo,
	&ICollection_1_Remove_m50587_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8979_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8977_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8979_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8977_0_0_0;
extern Il2CppType ICollection_1_t8977_1_0_0;
struct ICollection_1_t8977;
extern Il2CppGenericClass ICollection_1_t8977_GenericClass;
TypeInfo ICollection_1_t8977_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8977_MethodInfos/* methods */
	, ICollection_1_t8977_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8977_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8977_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8977_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8977_0_0_0/* byval_arg */
	, &ICollection_1_t8977_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8977_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
