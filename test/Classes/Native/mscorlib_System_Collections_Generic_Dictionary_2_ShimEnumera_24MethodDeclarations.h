﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct ShimEnumerator_t4297;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct Dictionary_2_t750;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m26204 (ShimEnumerator_t4297 * __this, Dictionary_2_t750 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::MoveNext()
 bool ShimEnumerator_MoveNext_m26205 (ShimEnumerator_t4297 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m26206 (ShimEnumerator_t4297 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Key()
 Object_t * ShimEnumerator_get_Key_m26207 (ShimEnumerator_t4297 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Value()
 Object_t * ShimEnumerator_get_Value_m26208 (ShimEnumerator_t4297 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
 Object_t * ShimEnumerator_get_Current_m26209 (ShimEnumerator_t4297 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
