﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>
struct InternalEnumerator_1_t3908;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Sma.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m22491 (InternalEnumerator_1_t3908 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22492 (InternalEnumerator_1_t3908 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::Dispose()
 void InternalEnumerator_1_Dispose_m22493 (InternalEnumerator_1_t3908 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m22494 (InternalEnumerator_1_t3908 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SmartTerrainRevisionData>::get_Current()
 SmartTerrainRevisionData_t647  InternalEnumerator_1_get_Current_m22495 (InternalEnumerator_1_t3908 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
