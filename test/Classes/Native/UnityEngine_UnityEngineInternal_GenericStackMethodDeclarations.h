﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngineInternal.GenericStack
struct GenericStack_t952;

// System.Void UnityEngineInternal.GenericStack::.ctor()
 void GenericStack__ctor_m6400 (GenericStack_t952 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
