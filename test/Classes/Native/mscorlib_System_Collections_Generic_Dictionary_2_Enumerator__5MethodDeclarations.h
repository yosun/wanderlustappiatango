﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Enumerator_t3277;
// System.Object
struct Object_t;
// UnityEngine.Font
struct Font_t280;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t462;
// System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
struct Dictionary_2_t282;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6MethodDeclarations.h"
#define Enumerator__ctor_m17924(__this, ___dictionary, method) (void)Enumerator__ctor_m17671_gshared((Enumerator_t3283 *)__this, (Dictionary_2_t3275 *)___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17925(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m17672_gshared((Enumerator_t3283 *)__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17926(__this, method) (DictionaryEntry_t1302 )Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17673_gshared((Enumerator_t3283 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17927(__this, method) (Object_t *)Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17674_gshared((Enumerator_t3283 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17928(__this, method) (Object_t *)Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17675_gshared((Enumerator_t3283 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::MoveNext()
#define Enumerator_MoveNext_m17929(__this, method) (bool)Enumerator_MoveNext_m17676_gshared((Enumerator_t3283 *)__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Current()
 KeyValuePair_2_t3273  Enumerator_get_Current_m17930 (Enumerator_t3277 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17931(__this, method) (Font_t280 *)Enumerator_get_CurrentKey_m17678_gshared((Enumerator_t3283 *)__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17932(__this, method) (List_1_t462 *)Enumerator_get_CurrentValue_m17679_gshared((Enumerator_t3283 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::VerifyState()
#define Enumerator_VerifyState_m17933(__this, method) (void)Enumerator_VerifyState_m17680_gshared((Enumerator_t3283 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17934(__this, method) (void)Enumerator_VerifyCurrent_m17681_gshared((Enumerator_t3283 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Dispose()
#define Enumerator_Dispose_m17935(__this, method) (void)Enumerator_Dispose_m17682_gshared((Enumerator_t3283 *)__this, method)
