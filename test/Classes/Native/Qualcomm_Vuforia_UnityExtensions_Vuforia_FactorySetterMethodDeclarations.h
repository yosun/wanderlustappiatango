﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.FactorySetter
struct FactorySetter_t147;

// System.Void Vuforia.FactorySetter::.ctor()
 void FactorySetter__ctor_m462 (FactorySetter_t147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
