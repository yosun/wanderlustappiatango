﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Events.PersistentCall>
struct EqualityComparer_1_t4805;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Events.PersistentCall>
struct EqualityComparer_1_t4805  : public Object_t
{
};
struct EqualityComparer_1_t4805_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Events.PersistentCall>::_default
	EqualityComparer_1_t4805 * ____default_0;
};
