﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUIContent
struct GUIContent_t485;
// System.String
struct String_t;

// System.Void UnityEngine.GUIContent::.ctor()
 void GUIContent__ctor_m5706 (GUIContent_t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor(System.String)
 void GUIContent__ctor_m2271 (GUIContent_t485 * __this, String_t* ___text, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.cctor()
 void GUIContent__cctor_m5707 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GUIContent::get_text()
 String_t* GUIContent_get_text_m2270 (GUIContent_t485 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::set_text(System.String)
 void GUIContent_set_text_m5708 (GUIContent_t485 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent UnityEngine.GUIContent::Temp(System.String)
 GUIContent_t485 * GUIContent_Temp_m5709 (Object_t * __this/* static, unused */, String_t* ___t, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::ClearStaticCache()
 void GUIContent_ClearStaticCache_m5710 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
