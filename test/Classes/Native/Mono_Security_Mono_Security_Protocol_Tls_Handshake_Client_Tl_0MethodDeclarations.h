﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify
struct TlsClientCertificateVerify_t1637;
// Mono.Security.Protocol.Tls.Context
struct Context_t1589;
// System.Security.Cryptography.RSA
struct RSA_t1356;
// System.Byte[]
struct ByteU5BU5D_t609;

// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::.ctor(Mono.Security.Protocol.Tls.Context)
 void TlsClientCertificateVerify__ctor_m8709 (TlsClientCertificateVerify_t1637 * __this, Context_t1589 * ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::Update()
 void TlsClientCertificateVerify_Update_m8710 (TlsClientCertificateVerify_t1637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::ProcessAsSsl3()
 void TlsClientCertificateVerify_ProcessAsSsl3_m8711 (TlsClientCertificateVerify_t1637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::ProcessAsTls1()
 void TlsClientCertificateVerify_ProcessAsTls1_m8712 (TlsClientCertificateVerify_t1637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RSA Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::getClientCertRSA(System.Security.Cryptography.RSA)
 RSA_t1356 * TlsClientCertificateVerify_getClientCertRSA_m8713 (TlsClientCertificateVerify_t1637 * __this, RSA_t1356 * ___privKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Client.TlsClientCertificateVerify::getUnsignedBigInteger(System.Byte[])
 ByteU5BU5D_t609* TlsClientCertificateVerify_getUnsignedBigInteger_m8714 (TlsClientCertificateVerify_t1637 * __this, ByteU5BU5D_t609* ___integer, MethodInfo* method) IL2CPP_METHOD_ATTR;
