﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<Vuforia.Prop>
struct IList_1_t4141;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Prop>
struct ReadOnlyCollection_1_t4138  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Prop>::list
	Object_t* ___list_0;
};
