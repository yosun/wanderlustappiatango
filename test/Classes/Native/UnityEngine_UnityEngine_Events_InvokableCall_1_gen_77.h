﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.GridLayoutGroup>
struct UnityAction_1_t3572;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.GridLayoutGroup>
struct InvokableCall_1_t3571  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.UI.GridLayoutGroup>::Delegate
	UnityAction_1_t3572 * ___Delegate_0;
};
