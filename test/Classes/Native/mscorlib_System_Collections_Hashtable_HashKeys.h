﻿#pragma once
#include <stdint.h>
// System.Collections.Hashtable
struct Hashtable_t1295;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Hashtable/HashKeys
struct HashKeys_t1833  : public Object_t
{
	// System.Collections.Hashtable System.Collections.Hashtable/HashKeys::host
	Hashtable_t1295 * ___host_0;
};
