﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Transform>
struct EqualityComparer_1_t3133;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.Transform>
struct EqualityComparer_1_t3133  : public Object_t
{
};
struct EqualityComparer_1_t3133_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.Transform>::_default
	EqualityComparer_1_t3133 * ____default_0;
};
