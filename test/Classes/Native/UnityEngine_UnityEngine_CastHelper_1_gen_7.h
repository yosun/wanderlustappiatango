﻿#pragma once
#include <stdint.h>
// UnityEngine.MeshFilter
struct MeshFilter_t169;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.MeshFilter>
struct CastHelper_1_t2950 
{
	// T UnityEngine.CastHelper`1<UnityEngine.MeshFilter>::t
	MeshFilter_t169 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.MeshFilter>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
