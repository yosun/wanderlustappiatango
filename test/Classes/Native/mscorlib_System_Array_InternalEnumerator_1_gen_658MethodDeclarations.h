﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>
struct InternalEnumerator_1_t5043;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Reflection.PropertyAttributes
#include "mscorlib_System_Reflection_PropertyAttributes.h"

// System.Void System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30795 (InternalEnumerator_1_t5043 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30796 (InternalEnumerator_1_t5043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::Dispose()
 void InternalEnumerator_1_Dispose_m30797 (InternalEnumerator_1_t5043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30798 (InternalEnumerator_1_t5043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Reflection.PropertyAttributes>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30799 (InternalEnumerator_1_t5043 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
