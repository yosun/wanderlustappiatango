﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo IList_1_t8465_il2cpp_TypeInfo;

// System.Boolean
#include "mscorlib_System_Boolean.h"
// System.Int32
#include "mscorlib_System_Int32.h"
// System.Void
#include "mscorlib_System_Void.h"

// System.Array
#include "mscorlib_System_Array.h"

// System.Int32 System.Collections.Generic.IList`1<System.Boolean>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Boolean>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Boolean>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Boolean>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Boolean>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Boolean>
extern MethodInfo IList_1_get_Item_m48172_MethodInfo;
extern MethodInfo IList_1_set_Item_m48173_MethodInfo;
static PropertyInfo IList_1_t8465____Item_PropertyInfo = 
{
	&IList_1_t8465_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48172_MethodInfo/* get */
	, &IList_1_set_Item_m48173_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8465_PropertyInfos[] =
{
	&IList_1_t8465____Item_PropertyInfo,
	NULL
};
extern Il2CppType Boolean_t106_0_0_0;
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo IList_1_t8465_IList_1_IndexOf_m48174_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_SByte_t129 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48174_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Boolean>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48174_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8465_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_SByte_t129/* invoker_method */
	, IList_1_t8465_IList_1_IndexOf_m48174_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48174_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo IList_1_t8465_IList_1_Insert_m48175_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_SByte_t129 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48175_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Boolean>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48175_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8465_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_SByte_t129/* invoker_method */
	, IList_1_t8465_IList_1_Insert_m48175_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48175_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8465_IList_1_RemoveAt_m48176_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48176_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Boolean>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48176_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8465_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8465_IList_1_RemoveAt_m48176_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48176_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8465_IList_1_get_Item_m48172_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48172_GenericMethod;
// T System.Collections.Generic.IList`1<System.Boolean>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48172_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8465_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, IList_1_t8465_IList_1_get_Item_m48172_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48172_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo IList_1_t8465_IList_1_set_Item_m48173_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_SByte_t129 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48173_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Boolean>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48173_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8465_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_SByte_t129/* invoker_method */
	, IList_1_t8465_IList_1_set_Item_m48173_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48173_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8465_MethodInfos[] =
{
	&IList_1_IndexOf_m48174_MethodInfo,
	&IList_1_Insert_m48175_MethodInfo,
	&IList_1_RemoveAt_m48176_MethodInfo,
	&IList_1_get_Item_m48172_MethodInfo,
	&IList_1_set_Item_m48173_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t8464_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t8466_il2cpp_TypeInfo;
static TypeInfo* IList_1_t8465_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8464_il2cpp_TypeInfo,
	&IEnumerable_1_t8466_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8465_0_0_0;
extern Il2CppType IList_1_t8465_1_0_0;
struct IList_1_t8465;
extern Il2CppGenericClass IList_1_t8465_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8465_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8465_MethodInfos/* methods */
	, IList_1_t8465_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8465_il2cpp_TypeInfo/* element_class */
	, IList_1_t8465_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8465_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8465_0_0_0/* byval_arg */
	, &IList_1_t8465_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8465_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8467_il2cpp_TypeInfo;

#include "mscorlib_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>
extern MethodInfo ICollection_1_get_Count_m48177_MethodInfo;
static PropertyInfo ICollection_1_t8467____Count_PropertyInfo = 
{
	&ICollection_1_t8467_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48177_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48178_MethodInfo;
static PropertyInfo ICollection_1_t8467____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8467_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48178_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8467_PropertyInfos[] =
{
	&ICollection_1_t8467____Count_PropertyInfo,
	&ICollection_1_t8467____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48177_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::get_Count()
MethodInfo ICollection_1_get_Count_m48177_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8467_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48177_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48178_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48178_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8467_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48178_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2336_0_0_0;
extern Il2CppType IComparable_1_t2336_0_0_0;
static ParameterInfo ICollection_1_t8467_ICollection_1_Add_m48179_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2336_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48179_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Add(T)
MethodInfo ICollection_1_Add_m48179_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8467_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8467_ICollection_1_Add_m48179_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48179_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48180_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Clear()
MethodInfo ICollection_1_Clear_m48180_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8467_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48180_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2336_0_0_0;
static ParameterInfo ICollection_1_t8467_ICollection_1_Contains_m48181_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2336_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48181_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Contains(T)
MethodInfo ICollection_1_Contains_m48181_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8467_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8467_ICollection_1_Contains_m48181_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48181_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1U5BU5D_t5235_0_0_0;
extern Il2CppType IComparable_1U5BU5D_t5235_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8467_ICollection_1_CopyTo_m48182_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1U5BU5D_t5235_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48182_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48182_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8467_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8467_ICollection_1_CopyTo_m48182_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48182_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2336_0_0_0;
static ParameterInfo ICollection_1_t8467_ICollection_1_Remove_m48183_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2336_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48183_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IComparable`1<System.Boolean>>::Remove(T)
MethodInfo ICollection_1_Remove_m48183_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8467_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8467_ICollection_1_Remove_m48183_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48183_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8467_MethodInfos[] =
{
	&ICollection_1_get_Count_m48177_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48178_MethodInfo,
	&ICollection_1_Add_m48179_MethodInfo,
	&ICollection_1_Clear_m48180_MethodInfo,
	&ICollection_1_Contains_m48181_MethodInfo,
	&ICollection_1_CopyTo_m48182_MethodInfo,
	&ICollection_1_Remove_m48183_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8469_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8467_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8469_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8467_0_0_0;
extern Il2CppType ICollection_1_t8467_1_0_0;
struct ICollection_1_t8467;
extern Il2CppGenericClass ICollection_1_t8467_GenericClass;
TypeInfo ICollection_1_t8467_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8467_MethodInfos/* methods */
	, ICollection_1_t8467_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8467_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8467_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8467_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8467_0_0_0/* byval_arg */
	, &ICollection_1_t8467_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8467_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Boolean>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Boolean>>
extern Il2CppType IEnumerator_1_t6688_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48184_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IComparable`1<System.Boolean>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48184_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8469_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6688_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48184_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8469_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48184_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8469_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8469_0_0_0;
extern Il2CppType IEnumerable_1_t8469_1_0_0;
struct IEnumerable_1_t8469;
extern Il2CppGenericClass IEnumerable_1_t8469_GenericClass;
TypeInfo IEnumerable_1_t8469_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8469_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8469_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8469_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8469_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8469_0_0_0/* byval_arg */
	, &IEnumerable_1_t8469_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8469_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6688_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Boolean>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Boolean>>
extern MethodInfo IEnumerator_1_get_Current_m48185_MethodInfo;
static PropertyInfo IEnumerator_1_t6688____Current_PropertyInfo = 
{
	&IEnumerator_1_t6688_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48185_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6688_PropertyInfos[] =
{
	&IEnumerator_1_t6688____Current_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2336_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48185_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IComparable`1<System.Boolean>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48185_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6688_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2336_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48185_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6688_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48185_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t6688_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6688_0_0_0;
extern Il2CppType IEnumerator_1_t6688_1_0_0;
struct IEnumerator_1_t6688;
extern Il2CppGenericClass IEnumerator_1_t6688_GenericClass;
TypeInfo IEnumerator_1_t6688_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6688_MethodInfos/* methods */
	, IEnumerator_1_t6688_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6688_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6688_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6688_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6688_0_0_0/* byval_arg */
	, &IEnumerator_1_t6688_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6688_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IComparable_1_t2336_il2cpp_TypeInfo;



// System.Int32 System.IComparable`1<System.Boolean>::CompareTo(T)
// Metadata Definition System.IComparable`1<System.Boolean>
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo IComparable_1_t2336_IComparable_1_CompareTo_m48186_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_SByte_t129 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IComparable_1_CompareTo_m48186_GenericMethod;
// System.Int32 System.IComparable`1<System.Boolean>::CompareTo(T)
MethodInfo IComparable_1_CompareTo_m48186_MethodInfo = 
{
	"CompareTo"/* name */
	, NULL/* method */
	, &IComparable_1_t2336_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_SByte_t129/* invoker_method */
	, IComparable_1_t2336_IComparable_1_CompareTo_m48186_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IComparable_1_CompareTo_m48186_GenericMethod/* genericMethod */

};
static MethodInfo* IComparable_1_t2336_MethodInfos[] =
{
	&IComparable_1_CompareTo_m48186_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IComparable_1_t2336_1_0_0;
struct IComparable_1_t2336;
extern Il2CppGenericClass IComparable_1_t2336_GenericClass;
TypeInfo IComparable_1_t2336_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IComparable`1"/* name */
	, "System"/* namespaze */
	, IComparable_1_t2336_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IComparable_1_t2336_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IComparable_1_t2336_il2cpp_TypeInfo/* cast_class */
	, &IComparable_1_t2336_0_0_0/* byval_arg */
	, &IComparable_1_t2336_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IComparable_1_t2336_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_473.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4824_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_473MethodDeclarations.h"

// System.Object
#include "mscorlib_System_Object.h"
// System.String
#include "mscorlib_System_String.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
extern TypeInfo InvalidOperationException_t1493_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m29653_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7651_MethodInfo;
extern MethodInfo Array_get_Length_m7656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIComparable_1_t2336_m38043_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m31436_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m31436(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.Boolean>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IComparable`1<System.Boolean>>(System.Int32)
#define Array_InternalArray__get_Item_TisIComparable_1_t2336_m38043(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4824____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4824_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4824, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4824____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4824_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4824, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4824_FieldInfos[] =
{
	&InternalEnumerator_1_t4824____array_0_FieldInfo,
	&InternalEnumerator_1_t4824____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29650_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4824____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4824_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29650_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4824____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4824_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29653_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4824_PropertyInfos[] =
{
	&InternalEnumerator_1_t4824____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4824____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4824_InternalEnumerator_1__ctor_m29649_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29649_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29649_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4824_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4824_InternalEnumerator_1__ctor_m29649_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29649_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29650_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29650_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4824_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29650_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29651_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29651_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4824_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29651_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29652_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29652_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4824_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29652_GenericMethod/* genericMethod */

};
extern Il2CppType IComparable_1_t2336_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29653_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IComparable`1<System.Boolean>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29653_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4824_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2336_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29653_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4824_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29649_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29650_MethodInfo,
	&InternalEnumerator_1_Dispose_m29651_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29652_MethodInfo,
	&InternalEnumerator_1_get_Current_m29653_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m29652_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29651_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4824_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29650_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29652_MethodInfo,
	&InternalEnumerator_1_Dispose_m29651_MethodInfo,
	&InternalEnumerator_1_get_Current_m29653_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4824_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6688_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4824_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6688_il2cpp_TypeInfo, 7},
};
extern TypeInfo IComparable_1_t2336_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4824_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29653_MethodInfo/* Method Usage */,
	&IComparable_1_t2336_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIComparable_1_t2336_m38043_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4824_0_0_0;
extern Il2CppType InternalEnumerator_1_t4824_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t4824_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t4824_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4824_MethodInfos/* methods */
	, InternalEnumerator_1_t4824_PropertyInfos/* properties */
	, InternalEnumerator_1_t4824_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4824_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4824_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4824_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4824_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4824_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4824_1_0_0/* this_arg */
	, InternalEnumerator_1_t4824_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4824_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4824_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4824)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8468_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>
extern MethodInfo IList_1_get_Item_m48187_MethodInfo;
extern MethodInfo IList_1_set_Item_m48188_MethodInfo;
static PropertyInfo IList_1_t8468____Item_PropertyInfo = 
{
	&IList_1_t8468_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48187_MethodInfo/* get */
	, &IList_1_set_Item_m48188_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8468_PropertyInfos[] =
{
	&IList_1_t8468____Item_PropertyInfo,
	NULL
};
extern Il2CppType IComparable_1_t2336_0_0_0;
static ParameterInfo IList_1_t8468_IList_1_IndexOf_m48189_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2336_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48189_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48189_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8468_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8468_IList_1_IndexOf_m48189_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48189_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IComparable_1_t2336_0_0_0;
static ParameterInfo IList_1_t8468_IList_1_Insert_m48190_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2336_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48190_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48190_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8468_IList_1_Insert_m48190_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48190_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8468_IList_1_RemoveAt_m48191_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48191_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48191_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8468_IList_1_RemoveAt_m48191_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48191_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8468_IList_1_get_Item_m48187_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IComparable_1_t2336_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48187_GenericMethod;
// T System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48187_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8468_il2cpp_TypeInfo/* declaring_type */
	, &IComparable_1_t2336_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8468_IList_1_get_Item_m48187_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48187_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IComparable_1_t2336_0_0_0;
static ParameterInfo IList_1_t8468_IList_1_set_Item_m48188_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IComparable_1_t2336_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48188_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IComparable`1<System.Boolean>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48188_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8468_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8468_IList_1_set_Item_m48188_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48188_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8468_MethodInfos[] =
{
	&IList_1_IndexOf_m48189_MethodInfo,
	&IList_1_Insert_m48190_MethodInfo,
	&IList_1_RemoveAt_m48191_MethodInfo,
	&IList_1_get_Item_m48187_MethodInfo,
	&IList_1_set_Item_m48188_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8468_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8467_il2cpp_TypeInfo,
	&IEnumerable_1_t8469_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8468_0_0_0;
extern Il2CppType IList_1_t8468_1_0_0;
struct IList_1_t8468;
extern Il2CppGenericClass IList_1_t8468_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8468_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8468_MethodInfos/* methods */
	, IList_1_t8468_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8468_il2cpp_TypeInfo/* element_class */
	, IList_1_t8468_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8468_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8468_0_0_0/* byval_arg */
	, &IList_1_t8468_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8468_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8470_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>
extern MethodInfo ICollection_1_get_Count_m48192_MethodInfo;
static PropertyInfo ICollection_1_t8470____Count_PropertyInfo = 
{
	&ICollection_1_t8470_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48192_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48193_MethodInfo;
static PropertyInfo ICollection_1_t8470____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8470_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48193_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8470_PropertyInfos[] =
{
	&ICollection_1_t8470____Count_PropertyInfo,
	&ICollection_1_t8470____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48192_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::get_Count()
MethodInfo ICollection_1_get_Count_m48192_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8470_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48192_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48193_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48193_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8470_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48193_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2337_0_0_0;
extern Il2CppType IEquatable_1_t2337_0_0_0;
static ParameterInfo ICollection_1_t8470_ICollection_1_Add_m48194_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2337_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48194_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Add(T)
MethodInfo ICollection_1_Add_m48194_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8470_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8470_ICollection_1_Add_m48194_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48194_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48195_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Clear()
MethodInfo ICollection_1_Clear_m48195_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8470_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48195_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2337_0_0_0;
static ParameterInfo ICollection_1_t8470_ICollection_1_Contains_m48196_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2337_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48196_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Contains(T)
MethodInfo ICollection_1_Contains_m48196_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8470_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8470_ICollection_1_Contains_m48196_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48196_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1U5BU5D_t5236_0_0_0;
extern Il2CppType IEquatable_1U5BU5D_t5236_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8470_ICollection_1_CopyTo_m48197_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1U5BU5D_t5236_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48197_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48197_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8470_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8470_ICollection_1_CopyTo_m48197_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48197_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2337_0_0_0;
static ParameterInfo ICollection_1_t8470_ICollection_1_Remove_m48198_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2337_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48198_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.IEquatable`1<System.Boolean>>::Remove(T)
MethodInfo ICollection_1_Remove_m48198_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8470_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8470_ICollection_1_Remove_m48198_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48198_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8470_MethodInfos[] =
{
	&ICollection_1_get_Count_m48192_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48193_MethodInfo,
	&ICollection_1_Add_m48194_MethodInfo,
	&ICollection_1_Clear_m48195_MethodInfo,
	&ICollection_1_Contains_m48196_MethodInfo,
	&ICollection_1_CopyTo_m48197_MethodInfo,
	&ICollection_1_Remove_m48198_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8472_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8470_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8472_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8470_0_0_0;
extern Il2CppType ICollection_1_t8470_1_0_0;
struct ICollection_1_t8470;
extern Il2CppGenericClass ICollection_1_t8470_GenericClass;
TypeInfo ICollection_1_t8470_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8470_MethodInfos/* methods */
	, ICollection_1_t8470_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8470_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8470_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8470_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8470_0_0_0/* byval_arg */
	, &ICollection_1_t8470_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8470_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Boolean>>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Boolean>>
extern Il2CppType IEnumerator_1_t6690_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48199_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.IEquatable`1<System.Boolean>>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48199_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8472_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6690_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48199_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8472_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48199_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8472_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8472_0_0_0;
extern Il2CppType IEnumerable_1_t8472_1_0_0;
struct IEnumerable_1_t8472;
extern Il2CppGenericClass IEnumerable_1_t8472_GenericClass;
TypeInfo IEnumerable_1_t8472_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8472_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8472_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8472_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8472_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8472_0_0_0/* byval_arg */
	, &IEnumerable_1_t8472_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8472_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6690_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Boolean>>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Boolean>>
extern MethodInfo IEnumerator_1_get_Current_m48200_MethodInfo;
static PropertyInfo IEnumerator_1_t6690____Current_PropertyInfo = 
{
	&IEnumerator_1_t6690_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48200_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6690_PropertyInfos[] =
{
	&IEnumerator_1_t6690____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2337_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48200_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IEquatable`1<System.Boolean>>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48200_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6690_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2337_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48200_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6690_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48200_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6690_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6690_0_0_0;
extern Il2CppType IEnumerator_1_t6690_1_0_0;
struct IEnumerator_1_t6690;
extern Il2CppGenericClass IEnumerator_1_t6690_GenericClass;
TypeInfo IEnumerator_1_t6690_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6690_MethodInfos/* methods */
	, IEnumerator_1_t6690_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6690_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6690_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6690_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6690_0_0_0/* byval_arg */
	, &IEnumerator_1_t6690_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6690_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEquatable_1_t2337_il2cpp_TypeInfo;



// System.Boolean System.IEquatable`1<System.Boolean>::Equals(T)
// Metadata Definition System.IEquatable`1<System.Boolean>
extern Il2CppType Boolean_t106_0_0_0;
static ParameterInfo IEquatable_1_t2337_IEquatable_1_Equals_m48201_ParameterInfos[] = 
{
	{"other", 0, 134217728, &EmptyCustomAttributesCache, &Boolean_t106_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_SByte_t129 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEquatable_1_Equals_m48201_GenericMethod;
// System.Boolean System.IEquatable`1<System.Boolean>::Equals(T)
MethodInfo IEquatable_1_Equals_m48201_MethodInfo = 
{
	"Equals"/* name */
	, NULL/* method */
	, &IEquatable_1_t2337_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_SByte_t129/* invoker_method */
	, IEquatable_1_t2337_IEquatable_1_Equals_m48201_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEquatable_1_Equals_m48201_GenericMethod/* genericMethod */

};
static MethodInfo* IEquatable_1_t2337_MethodInfos[] =
{
	&IEquatable_1_Equals_m48201_MethodInfo,
	NULL
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEquatable_1_t2337_1_0_0;
struct IEquatable_1_t2337;
extern Il2CppGenericClass IEquatable_1_t2337_GenericClass;
TypeInfo IEquatable_1_t2337_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEquatable`1"/* name */
	, "System"/* namespaze */
	, IEquatable_1_t2337_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEquatable_1_t2337_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEquatable_1_t2337_il2cpp_TypeInfo/* cast_class */
	, &IEquatable_1_t2337_0_0_0/* byval_arg */
	, &IEquatable_1_t2337_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEquatable_1_t2337_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_474.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4825_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_474MethodDeclarations.h"

extern MethodInfo InternalEnumerator_1_get_Current_m29658_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEquatable_1_t2337_m38054_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.Boolean>>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IEquatable`1<System.Boolean>>(System.Int32)
#define Array_InternalArray__get_Item_TisIEquatable_1_t2337_m38054(__this, p0, method) (Object_t*)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::MoveNext()
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4825____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4825_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4825, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4825____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4825_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4825, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4825_FieldInfos[] =
{
	&InternalEnumerator_1_t4825____array_0_FieldInfo,
	&InternalEnumerator_1_t4825____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29655_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4825____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4825_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29655_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4825____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4825_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29658_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4825_PropertyInfos[] =
{
	&InternalEnumerator_1_t4825____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4825____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4825_InternalEnumerator_1__ctor_m29654_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29654_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29654_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4825_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4825_InternalEnumerator_1__ctor_m29654_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29654_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29655_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29655_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4825_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29655_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29656_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29656_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4825_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29656_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29657_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29657_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4825_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29657_GenericMethod/* genericMethod */

};
extern Il2CppType IEquatable_1_t2337_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29658_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IEquatable`1<System.Boolean>>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29658_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4825_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2337_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29658_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4825_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29654_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29655_MethodInfo,
	&InternalEnumerator_1_Dispose_m29656_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29657_MethodInfo,
	&InternalEnumerator_1_get_Current_m29658_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29657_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29656_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4825_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29655_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29657_MethodInfo,
	&InternalEnumerator_1_Dispose_m29656_MethodInfo,
	&InternalEnumerator_1_get_Current_m29658_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4825_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6690_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4825_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6690_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEquatable_1_t2337_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4825_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29658_MethodInfo/* Method Usage */,
	&IEquatable_1_t2337_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEquatable_1_t2337_m38054_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4825_0_0_0;
extern Il2CppType InternalEnumerator_1_t4825_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4825_GenericClass;
TypeInfo InternalEnumerator_1_t4825_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4825_MethodInfos/* methods */
	, InternalEnumerator_1_t4825_PropertyInfos/* properties */
	, InternalEnumerator_1_t4825_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4825_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4825_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4825_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4825_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4825_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4825_1_0_0/* this_arg */
	, InternalEnumerator_1_t4825_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4825_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4825_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4825)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8471_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>
extern MethodInfo IList_1_get_Item_m48202_MethodInfo;
extern MethodInfo IList_1_set_Item_m48203_MethodInfo;
static PropertyInfo IList_1_t8471____Item_PropertyInfo = 
{
	&IList_1_t8471_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48202_MethodInfo/* get */
	, &IList_1_set_Item_m48203_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8471_PropertyInfos[] =
{
	&IList_1_t8471____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEquatable_1_t2337_0_0_0;
static ParameterInfo IList_1_t8471_IList_1_IndexOf_m48204_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2337_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48204_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48204_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8471_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8471_IList_1_IndexOf_m48204_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48204_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEquatable_1_t2337_0_0_0;
static ParameterInfo IList_1_t8471_IList_1_Insert_m48205_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2337_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48205_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48205_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8471_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8471_IList_1_Insert_m48205_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48205_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8471_IList_1_RemoveAt_m48206_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48206_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48206_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8471_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8471_IList_1_RemoveAt_m48206_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48206_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8471_IList_1_get_Item_m48202_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEquatable_1_t2337_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48202_GenericMethod;
// T System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48202_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8471_il2cpp_TypeInfo/* declaring_type */
	, &IEquatable_1_t2337_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8471_IList_1_get_Item_m48202_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48202_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEquatable_1_t2337_0_0_0;
static ParameterInfo IList_1_t8471_IList_1_set_Item_m48203_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEquatable_1_t2337_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48203_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.IEquatable`1<System.Boolean>>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48203_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8471_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8471_IList_1_set_Item_m48203_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48203_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8471_MethodInfos[] =
{
	&IList_1_IndexOf_m48204_MethodInfo,
	&IList_1_Insert_m48205_MethodInfo,
	&IList_1_RemoveAt_m48206_MethodInfo,
	&IList_1_get_Item_m48202_MethodInfo,
	&IList_1_set_Item_m48203_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8471_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8470_il2cpp_TypeInfo,
	&IEnumerable_1_t8472_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8471_0_0_0;
extern Il2CppType IList_1_t8471_1_0_0;
struct IList_1_t8471;
extern Il2CppGenericClass IList_1_t8471_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8471_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8471_MethodInfos/* methods */
	, IList_1_t8471_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8471_il2cpp_TypeInfo/* element_class */
	, IList_1_t8471_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8471_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8471_0_0_0/* byval_arg */
	, &IList_1_t8471_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8471_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityEvent_2_t4826_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_2_genMethodDeclarations.h"

// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_gen.h"
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo TypeU5BU5D_t878_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_2_t4787_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEventBase
#include "UnityEngine_UnityEngine_Events_UnityEventBaseMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// UnityEngine.Events.InvokableCall`2<System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_2_genMethodDeclarations.h"
extern Il2CppType Object_t_0_0_0;
extern MethodInfo UnityEventBase__ctor_m6379_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m395_MethodInfo;
extern MethodInfo UnityEventBase_GetValidMethodInfo_m6387_MethodInfo;
extern MethodInfo InvokableCall_2__ctor_m29321_MethodInfo;


// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::.ctor()
extern MethodInfo UnityEvent_2__ctor_m29659_MethodInfo;
 void UnityEvent_2__ctor_m29659_gshared (UnityEvent_2_t4826 * __this, MethodInfo* method)
{
	{
		__this->___m_InvokeArray_4 = ((ObjectU5BU5D_t115*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t115_il2cpp_TypeInfo), 2));
		UnityEventBase__ctor_m6379(__this, /*hidden argument*/&UnityEventBase__ctor_m6379_MethodInfo);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern MethodInfo UnityEvent_2_FindMethod_Impl_m29660_MethodInfo;
 MethodInfo_t142 * UnityEvent_2_FindMethod_Impl_m29660_gshared (UnityEvent_2_t4826 * __this, String_t* ___name, Object_t * ___targetObj, MethodInfo* method)
{
	{
		TypeU5BU5D_t878* L_0 = ((TypeU5BU5D_t878*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t878_il2cpp_TypeInfo), 2));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_0, 0)) = (Type_t *)L_1;
		TypeU5BU5D_t878* L_2 = L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_2, 1)) = (Type_t *)L_3;
		MethodInfo_t142 * L_4 = UnityEventBase_GetValidMethodInfo_m6387(NULL /*static, unused*/, ___targetObj, ___name, L_2, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6387_MethodInfo);
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern MethodInfo UnityEvent_2_GetDelegate_m29661_MethodInfo;
 BaseInvokableCall_t1075 * UnityEvent_2_GetDelegate_m29661_gshared (UnityEvent_2_t4826 * __this, Object_t * ___target, MethodInfo_t142 * ___theFunction, MethodInfo* method)
{
	{
		InvokableCall_2_t4787 * L_0 = (InvokableCall_2_t4787 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		(( void (*) (InvokableCall_2_t4787 * __this, Object_t * p0, MethodInfo_t142 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(L_0, ___target, ___theFunction, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_0;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo UnityEvent_2_t4826____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &UnityEvent_2_t4826_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEvent_2_t4826, ___m_InvokeArray_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_2_t4826_FieldInfos[] =
{
	&UnityEvent_2_t4826____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_2__ctor_m29659_GenericMethod;
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::.ctor()
MethodInfo UnityEvent_2__ctor_m29659_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent_2__ctor_m29659_gshared/* method */
	, &UnityEvent_2_t4826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_2__ctor_m29659_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_2_t4826_UnityEvent_2_FindMethod_Impl_m29660_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t142_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_2_FindMethod_Impl_m29660_GenericMethod;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_2_FindMethod_Impl_m29660_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_2_FindMethod_Impl_m29660_gshared/* method */
	, &UnityEvent_2_t4826_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t142_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_2_t4826_UnityEvent_2_FindMethod_Impl_m29660_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_2_FindMethod_Impl_m29660_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo UnityEvent_2_t4826_UnityEvent_2_GetDelegate_m29661_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_2_GetDelegate_m29661_GenericMethod;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_2_GetDelegate_m29661_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_2_GetDelegate_m29661_gshared/* method */
	, &UnityEvent_2_t4826_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_2_t4826_UnityEvent_2_GetDelegate_m29661_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_2_GetDelegate_m29661_GenericMethod/* genericMethod */

};
static MethodInfo* UnityEvent_2_t4826_MethodInfos[] =
{
	&UnityEvent_2__ctor_m29659_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m29660_MethodInfo,
	&UnityEvent_2_GetDelegate_m29661_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
extern MethodInfo UnityEventBase_ToString_m1903_MethodInfo;
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1904_MethodInfo;
extern MethodInfo UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1905_MethodInfo;
static MethodInfo* UnityEvent_2_t4826_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&UnityEventBase_ToString_m1903_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1904_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1905_MethodInfo,
	&UnityEvent_2_FindMethod_Impl_m29660_MethodInfo,
	&UnityEvent_2_GetDelegate_m29661_MethodInfo,
};
extern TypeInfo ISerializationCallbackReceiver_t422_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityEvent_2_t4826_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t422_il2cpp_TypeInfo, 4},
};
extern TypeInfo InvokableCall_2_t4787_il2cpp_TypeInfo;
static Il2CppRGCTXData UnityEvent_2_t4826_RGCTXData[4] = 
{
	&Object_t_0_0_0/* Type Usage */,
	&Object_t_0_0_0/* Type Usage */,
	&InvokableCall_2_t4787_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_2__ctor_m29321_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_2_t4826_0_0_0;
extern Il2CppType UnityEvent_2_t4826_1_0_0;
extern TypeInfo UnityEventBase_t1084_il2cpp_TypeInfo;
struct UnityEvent_2_t4826;
extern Il2CppGenericClass UnityEvent_2_t4826_GenericClass;
TypeInfo UnityEvent_2_t4826_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`2"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_2_t4826_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_2_t4826_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_2_t4826_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityEvent_2_t4826_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityEvent_2_t4826_il2cpp_TypeInfo/* cast_class */
	, &UnityEvent_2_t4826_0_0_0/* byval_arg */
	, &UnityEvent_2_t4826_1_0_0/* this_arg */
	, UnityEvent_2_t4826_InterfacesOffsets/* interface_offsets */
	, &UnityEvent_2_t4826_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, UnityEvent_2_t4826_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_2_t4826)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityEvent_3_t4827_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_3_genMethodDeclarations.h"

// UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_gen.h"
extern TypeInfo InvokableCall_3_t4789_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`3<System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_3_genMethodDeclarations.h"
extern MethodInfo InvokableCall_3__ctor_m29328_MethodInfo;


// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::.ctor()
extern MethodInfo UnityEvent_3__ctor_m29662_MethodInfo;
 void UnityEvent_3__ctor_m29662_gshared (UnityEvent_3_t4827 * __this, MethodInfo* method)
{
	{
		__this->___m_InvokeArray_4 = ((ObjectU5BU5D_t115*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t115_il2cpp_TypeInfo), 3));
		UnityEventBase__ctor_m6379(__this, /*hidden argument*/&UnityEventBase__ctor_m6379_MethodInfo);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern MethodInfo UnityEvent_3_FindMethod_Impl_m29663_MethodInfo;
 MethodInfo_t142 * UnityEvent_3_FindMethod_Impl_m29663_gshared (UnityEvent_3_t4827 * __this, String_t* ___name, Object_t * ___targetObj, MethodInfo* method)
{
	{
		TypeU5BU5D_t878* L_0 = ((TypeU5BU5D_t878*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t878_il2cpp_TypeInfo), 3));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_0, 0)) = (Type_t *)L_1;
		TypeU5BU5D_t878* L_2 = L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_2, 1)) = (Type_t *)L_3;
		TypeU5BU5D_t878* L_4 = L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 2)) = (Type_t *)L_5;
		MethodInfo_t142 * L_6 = UnityEventBase_GetValidMethodInfo_m6387(NULL /*static, unused*/, ___targetObj, ___name, L_4, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6387_MethodInfo);
		return L_6;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern MethodInfo UnityEvent_3_GetDelegate_m29664_MethodInfo;
 BaseInvokableCall_t1075 * UnityEvent_3_GetDelegate_m29664_gshared (UnityEvent_3_t4827 * __this, Object_t * ___target, MethodInfo_t142 * ___theFunction, MethodInfo* method)
{
	{
		InvokableCall_3_t4789 * L_0 = (InvokableCall_3_t4789 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		(( void (*) (InvokableCall_3_t4789 * __this, Object_t * p0, MethodInfo_t142 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(L_0, ___target, ___theFunction, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		return L_0;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo UnityEvent_3_t4827____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &UnityEvent_3_t4827_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEvent_3_t4827, ___m_InvokeArray_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_3_t4827_FieldInfos[] =
{
	&UnityEvent_3_t4827____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_3__ctor_m29662_GenericMethod;
// System.Void UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::.ctor()
MethodInfo UnityEvent_3__ctor_m29662_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent_3__ctor_m29662_gshared/* method */
	, &UnityEvent_3_t4827_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_3__ctor_m29662_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_3_t4827_UnityEvent_3_FindMethod_Impl_m29663_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t142_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_3_FindMethod_Impl_m29663_GenericMethod;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_3_FindMethod_Impl_m29663_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_3_FindMethod_Impl_m29663_gshared/* method */
	, &UnityEvent_3_t4827_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t142_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_3_t4827_UnityEvent_3_FindMethod_Impl_m29663_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_3_FindMethod_Impl_m29663_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo UnityEvent_3_t4827_UnityEvent_3_GetDelegate_m29664_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_3_GetDelegate_m29664_GenericMethod;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3<System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_3_GetDelegate_m29664_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_3_GetDelegate_m29664_gshared/* method */
	, &UnityEvent_3_t4827_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_3_t4827_UnityEvent_3_GetDelegate_m29664_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_3_GetDelegate_m29664_GenericMethod/* genericMethod */

};
static MethodInfo* UnityEvent_3_t4827_MethodInfos[] =
{
	&UnityEvent_3__ctor_m29662_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m29663_MethodInfo,
	&UnityEvent_3_GetDelegate_m29664_MethodInfo,
	NULL
};
static MethodInfo* UnityEvent_3_t4827_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&UnityEventBase_ToString_m1903_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1904_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1905_MethodInfo,
	&UnityEvent_3_FindMethod_Impl_m29663_MethodInfo,
	&UnityEvent_3_GetDelegate_m29664_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityEvent_3_t4827_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t422_il2cpp_TypeInfo, 4},
};
extern TypeInfo InvokableCall_3_t4789_il2cpp_TypeInfo;
static Il2CppRGCTXData UnityEvent_3_t4827_RGCTXData[5] = 
{
	&Object_t_0_0_0/* Type Usage */,
	&Object_t_0_0_0/* Type Usage */,
	&Object_t_0_0_0/* Type Usage */,
	&InvokableCall_3_t4789_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_3__ctor_m29328_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_3_t4827_0_0_0;
extern Il2CppType UnityEvent_3_t4827_1_0_0;
struct UnityEvent_3_t4827;
extern Il2CppGenericClass UnityEvent_3_t4827_GenericClass;
TypeInfo UnityEvent_3_t4827_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`3"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_3_t4827_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_3_t4827_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_3_t4827_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityEvent_3_t4827_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityEvent_3_t4827_il2cpp_TypeInfo/* cast_class */
	, &UnityEvent_3_t4827_0_0_0/* byval_arg */
	, &UnityEvent_3_t4827_1_0_0/* this_arg */
	, UnityEvent_3_t4827_InterfacesOffsets/* interface_offsets */
	, &UnityEvent_3_t4827_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, UnityEvent_3_t4827_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_3_t4827)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo UnityEvent_4_t4828_il2cpp_TypeInfo;
// UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_UnityEvent_4_genMethodDeclarations.h"

// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_gen.h"
extern TypeInfo InvokableCall_4_t4791_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`4<System.Object,System.Object,System.Object,System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_4_genMethodDeclarations.h"
extern MethodInfo InvokableCall_4__ctor_m29335_MethodInfo;


// System.Void UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern MethodInfo UnityEvent_4__ctor_m29665_MethodInfo;
 void UnityEvent_4__ctor_m29665_gshared (UnityEvent_4_t4828 * __this, MethodInfo* method)
{
	{
		__this->___m_InvokeArray_4 = ((ObjectU5BU5D_t115*)SZArrayNew(InitializedTypeInfo(&ObjectU5BU5D_t115_il2cpp_TypeInfo), 4));
		UnityEventBase__ctor_m6379(__this, /*hidden argument*/&UnityEventBase__ctor_m6379_MethodInfo);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
extern MethodInfo UnityEvent_4_FindMethod_Impl_m29666_MethodInfo;
 MethodInfo_t142 * UnityEvent_4_FindMethod_Impl_m29666_gshared (UnityEvent_4_t4828 * __this, String_t* ___name, Object_t * ___targetObj, MethodInfo* method)
{
	{
		TypeU5BU5D_t878* L_0 = ((TypeU5BU5D_t878*)SZArrayNew(InitializedTypeInfo(&TypeU5BU5D_t878_il2cpp_TypeInfo), 4));
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_0, 0)) = (Type_t *)L_1;
		TypeU5BU5D_t878* L_2 = L_0;
		Type_t * L_3 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_2, 1)) = (Type_t *)L_3;
		TypeU5BU5D_t878* L_4 = L_2;
		Type_t * L_5 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_5);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_4, 2)) = (Type_t *)L_5;
		TypeU5BU5D_t878* L_6 = L_4;
		Type_t * L_7 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_7);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_6, 3)) = (Type_t *)L_7;
		MethodInfo_t142 * L_8 = UnityEventBase_GetValidMethodInfo_m6387(NULL /*static, unused*/, ___targetObj, ___name, L_6, /*hidden argument*/&UnityEventBase_GetValidMethodInfo_m6387_MethodInfo);
		return L_8;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern MethodInfo UnityEvent_4_GetDelegate_m29667_MethodInfo;
 BaseInvokableCall_t1075 * UnityEvent_4_GetDelegate_m29667_gshared (UnityEvent_4_t4828 * __this, Object_t * ___target, MethodInfo_t142 * ___theFunction, MethodInfo* method)
{
	{
		InvokableCall_4_t4791 * L_0 = (InvokableCall_4_t4791 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		(( void (*) (InvokableCall_4_t4791 * __this, Object_t * p0, MethodInfo_t142 * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, ___target, ___theFunction, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		return L_0;
	}
}
// Metadata Definition UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo UnityEvent_4_t4828____m_InvokeArray_4_FieldInfo = 
{
	"m_InvokeArray"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &UnityEvent_4_t4828_il2cpp_TypeInfo/* parent */
	, offsetof(UnityEvent_4_t4828, ___m_InvokeArray_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* UnityEvent_4_t4828_FieldInfos[] =
{
	&UnityEvent_4_t4828____m_InvokeArray_4_FieldInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_4__ctor_m29665_GenericMethod;
// System.Void UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
MethodInfo UnityEvent_4__ctor_m29665_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityEvent_4__ctor_m29665_gshared/* method */
	, &UnityEvent_4_t4828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_4__ctor_m29665_GenericMethod/* genericMethod */

};
extern Il2CppType String_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityEvent_4_t4828_UnityEvent_4_FindMethod_Impl_m29666_ParameterInfos[] = 
{
	{"name", 0, 134217728, &EmptyCustomAttributesCache, &String_t_0_0_0},
	{"targetObj", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType MethodInfo_t142_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_4_FindMethod_Impl_m29666_GenericMethod;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::FindMethod_Impl(System.String,System.Object)
MethodInfo UnityEvent_4_FindMethod_Impl_m29666_MethodInfo = 
{
	"FindMethod_Impl"/* name */
	, (methodPointerType)&UnityEvent_4_FindMethod_Impl_m29666_gshared/* method */
	, &UnityEvent_4_t4828_il2cpp_TypeInfo/* declaring_type */
	, &MethodInfo_t142_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_4_t4828_UnityEvent_4_FindMethod_Impl_m29666_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 196/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_4_FindMethod_Impl_m29666_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo UnityEvent_4_t4828_UnityEvent_4_GetDelegate_m29667_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType BaseInvokableCall_t1075_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityEvent_4_GetDelegate_m29667_GenericMethod;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4<System.Object,System.Object,System.Object,System.Object>::GetDelegate(System.Object,System.Reflection.MethodInfo)
MethodInfo UnityEvent_4_GetDelegate_m29667_MethodInfo = 
{
	"GetDelegate"/* name */
	, (methodPointerType)&UnityEvent_4_GetDelegate_m29667_gshared/* method */
	, &UnityEvent_4_t4828_il2cpp_TypeInfo/* declaring_type */
	, &BaseInvokableCall_t1075_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t/* invoker_method */
	, UnityEvent_4_t4828_UnityEvent_4_GetDelegate_m29667_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 195/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityEvent_4_GetDelegate_m29667_GenericMethod/* genericMethod */

};
static MethodInfo* UnityEvent_4_t4828_MethodInfos[] =
{
	&UnityEvent_4__ctor_m29665_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m29666_MethodInfo,
	&UnityEvent_4_GetDelegate_m29667_MethodInfo,
	NULL
};
static MethodInfo* UnityEvent_4_t4828_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&UnityEventBase_ToString_m1903_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m1904_MethodInfo,
	&UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m1905_MethodInfo,
	&UnityEvent_4_FindMethod_Impl_m29666_MethodInfo,
	&UnityEvent_4_GetDelegate_m29667_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityEvent_4_t4828_InterfacesOffsets[] = 
{
	{ &ISerializationCallbackReceiver_t422_il2cpp_TypeInfo, 4},
};
extern TypeInfo InvokableCall_4_t4791_il2cpp_TypeInfo;
static Il2CppRGCTXData UnityEvent_4_t4828_RGCTXData[6] = 
{
	&Object_t_0_0_0/* Type Usage */,
	&Object_t_0_0_0/* Type Usage */,
	&Object_t_0_0_0/* Type Usage */,
	&Object_t_0_0_0/* Type Usage */,
	&InvokableCall_4_t4791_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_4__ctor_m29335_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityEvent_4_t4828_0_0_0;
extern Il2CppType UnityEvent_4_t4828_1_0_0;
struct UnityEvent_4_t4828;
extern Il2CppGenericClass UnityEvent_4_t4828_GenericClass;
TypeInfo UnityEvent_4_t4828_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityEvent`4"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityEvent_4_t4828_MethodInfos/* methods */
	, NULL/* properties */
	, UnityEvent_4_t4828_FieldInfos/* fields */
	, NULL/* events */
	, &UnityEventBase_t1084_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityEvent_4_t4828_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityEvent_4_t4828_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityEvent_4_t4828_il2cpp_TypeInfo/* cast_class */
	, &UnityEvent_4_t4828_0_0_0/* byval_arg */
	, &UnityEvent_4_t4828_1_0_0/* this_arg */
	, UnityEvent_4_t4828_InterfacesOffsets/* interface_offsets */
	, &UnityEvent_4_t4828_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, UnityEvent_4_t4828_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityEvent_4_t4828)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056897/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 3/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 0/* interfaces_count */
	, 1/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6692_il2cpp_TypeInfo;

// UnityEngine.UserAuthorizationDialog
#include "UnityEngine_UnityEngine_UserAuthorizationDialog.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.UserAuthorizationDialog>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.UserAuthorizationDialog>
extern MethodInfo IEnumerator_1_get_Current_m48207_MethodInfo;
static PropertyInfo IEnumerator_1_t6692____Current_PropertyInfo = 
{
	&IEnumerator_1_t6692_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48207_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6692_PropertyInfos[] =
{
	&IEnumerator_1_t6692____Current_PropertyInfo,
	NULL
};
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48207_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.UserAuthorizationDialog>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48207_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6692_il2cpp_TypeInfo/* declaring_type */
	, &UserAuthorizationDialog_t1093_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48207_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6692_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48207_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6692_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6692_0_0_0;
extern Il2CppType IEnumerator_1_t6692_1_0_0;
struct IEnumerator_1_t6692;
extern Il2CppGenericClass IEnumerator_1_t6692_GenericClass;
TypeInfo IEnumerator_1_t6692_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6692_MethodInfos/* methods */
	, IEnumerator_1_t6692_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6692_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6692_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6692_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6692_0_0_0/* byval_arg */
	, &IEnumerator_1_t6692_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6692_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_475.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4829_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_475MethodDeclarations.h"

extern TypeInfo UserAuthorizationDialog_t1093_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29672_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisUserAuthorizationDialog_t1093_m38065_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.UserAuthorizationDialog>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UserAuthorizationDialog>(System.Int32)
#define Array_InternalArray__get_Item_TisUserAuthorizationDialog_t1093_m38065(__this, p0, method) (UserAuthorizationDialog_t1093 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4829____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4829_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4829, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4829____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4829_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4829, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4829_FieldInfos[] =
{
	&InternalEnumerator_1_t4829____array_0_FieldInfo,
	&InternalEnumerator_1_t4829____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29669_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4829____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4829_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29669_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4829____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4829_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29672_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4829_PropertyInfos[] =
{
	&InternalEnumerator_1_t4829____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4829____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4829_InternalEnumerator_1__ctor_m29668_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29668_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29668_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4829_InternalEnumerator_1__ctor_m29668_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29668_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29669_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29669_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4829_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29669_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29670_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29670_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29670_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29671_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29671_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29671_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29672_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.UserAuthorizationDialog>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29672_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4829_il2cpp_TypeInfo/* declaring_type */
	, &UserAuthorizationDialog_t1093_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29672_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4829_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29668_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29669_MethodInfo,
	&InternalEnumerator_1_Dispose_m29670_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29671_MethodInfo,
	&InternalEnumerator_1_get_Current_m29672_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29671_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29670_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4829_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29669_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29671_MethodInfo,
	&InternalEnumerator_1_Dispose_m29670_MethodInfo,
	&InternalEnumerator_1_get_Current_m29672_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4829_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6692_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4829_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6692_il2cpp_TypeInfo, 7},
};
extern TypeInfo UserAuthorizationDialog_t1093_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4829_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29672_MethodInfo/* Method Usage */,
	&UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisUserAuthorizationDialog_t1093_m38065_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4829_0_0_0;
extern Il2CppType InternalEnumerator_1_t4829_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4829_GenericClass;
TypeInfo InternalEnumerator_1_t4829_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4829_MethodInfos/* methods */
	, InternalEnumerator_1_t4829_PropertyInfos/* properties */
	, InternalEnumerator_1_t4829_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4829_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4829_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4829_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4829_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4829_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4829_1_0_0/* this_arg */
	, InternalEnumerator_1_t4829_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4829_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4829_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4829)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8473_il2cpp_TypeInfo;

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>
extern MethodInfo ICollection_1_get_Count_m48208_MethodInfo;
static PropertyInfo ICollection_1_t8473____Count_PropertyInfo = 
{
	&ICollection_1_t8473_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48208_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48209_MethodInfo;
static PropertyInfo ICollection_1_t8473____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8473_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48209_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8473_PropertyInfos[] =
{
	&ICollection_1_t8473____Count_PropertyInfo,
	&ICollection_1_t8473____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48208_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::get_Count()
MethodInfo ICollection_1_get_Count_m48208_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8473_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48208_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48209_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48209_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8473_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48209_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
static ParameterInfo ICollection_1_t8473_ICollection_1_Add_m48210_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1093_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48210_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Add(T)
MethodInfo ICollection_1_Add_m48210_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8473_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8473_ICollection_1_Add_m48210_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48210_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48211_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Clear()
MethodInfo ICollection_1_Clear_m48211_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8473_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48211_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
static ParameterInfo ICollection_1_t8473_ICollection_1_Contains_m48212_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1093_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48212_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Contains(T)
MethodInfo ICollection_1_Contains_m48212_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8473_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8473_ICollection_1_Contains_m48212_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48212_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialogU5BU5D_t5486_0_0_0;
extern Il2CppType UserAuthorizationDialogU5BU5D_t5486_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8473_ICollection_1_CopyTo_m48213_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialogU5BU5D_t5486_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48213_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48213_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8473_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8473_ICollection_1_CopyTo_m48213_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48213_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
static ParameterInfo ICollection_1_t8473_ICollection_1_Remove_m48214_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1093_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48214_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.UserAuthorizationDialog>::Remove(T)
MethodInfo ICollection_1_Remove_m48214_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8473_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8473_ICollection_1_Remove_m48214_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48214_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8473_MethodInfos[] =
{
	&ICollection_1_get_Count_m48208_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48209_MethodInfo,
	&ICollection_1_Add_m48210_MethodInfo,
	&ICollection_1_Clear_m48211_MethodInfo,
	&ICollection_1_Contains_m48212_MethodInfo,
	&ICollection_1_CopyTo_m48213_MethodInfo,
	&ICollection_1_Remove_m48214_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8475_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8473_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8475_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8473_0_0_0;
extern Il2CppType ICollection_1_t8473_1_0_0;
struct ICollection_1_t8473;
extern Il2CppGenericClass ICollection_1_t8473_GenericClass;
TypeInfo ICollection_1_t8473_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8473_MethodInfos/* methods */
	, ICollection_1_t8473_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8473_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8473_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8473_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8473_0_0_0/* byval_arg */
	, &ICollection_1_t8473_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8473_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UserAuthorizationDialog>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType IEnumerator_1_t6692_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48215_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.UserAuthorizationDialog>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48215_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8475_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6692_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48215_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8475_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48215_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8475_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8475_0_0_0;
extern Il2CppType IEnumerable_1_t8475_1_0_0;
struct IEnumerable_1_t8475;
extern Il2CppGenericClass IEnumerable_1_t8475_GenericClass;
TypeInfo IEnumerable_1_t8475_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8475_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8475_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8475_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8475_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8475_0_0_0/* byval_arg */
	, &IEnumerable_1_t8475_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8475_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8474_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>
extern MethodInfo IList_1_get_Item_m48216_MethodInfo;
extern MethodInfo IList_1_set_Item_m48217_MethodInfo;
static PropertyInfo IList_1_t8474____Item_PropertyInfo = 
{
	&IList_1_t8474_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48216_MethodInfo/* get */
	, &IList_1_set_Item_m48217_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8474_PropertyInfos[] =
{
	&IList_1_t8474____Item_PropertyInfo,
	NULL
};
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
static ParameterInfo IList_1_t8474_IList_1_IndexOf_m48218_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1093_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48218_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48218_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8474_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8474_IList_1_IndexOf_m48218_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48218_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
static ParameterInfo IList_1_t8474_IList_1_Insert_m48219_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1093_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48219_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48219_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8474_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8474_IList_1_Insert_m48219_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48219_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8474_IList_1_RemoveAt_m48220_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48220_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48220_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8474_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8474_IList_1_RemoveAt_m48220_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48220_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8474_IList_1_get_Item_m48216_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48216_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48216_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8474_il2cpp_TypeInfo/* declaring_type */
	, &UserAuthorizationDialog_t1093_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8474_IList_1_get_Item_m48216_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48216_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
static ParameterInfo IList_1_t8474_IList_1_set_Item_m48217_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1093_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48217_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.UserAuthorizationDialog>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48217_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8474_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8474_IList_1_set_Item_m48217_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48217_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8474_MethodInfos[] =
{
	&IList_1_IndexOf_m48218_MethodInfo,
	&IList_1_Insert_m48219_MethodInfo,
	&IList_1_RemoveAt_m48220_MethodInfo,
	&IList_1_get_Item_m48216_MethodInfo,
	&IList_1_set_Item_m48217_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8474_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8473_il2cpp_TypeInfo,
	&IEnumerable_1_t8475_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8474_0_0_0;
extern Il2CppType IList_1_t8474_1_0_0;
struct IList_1_t8474;
extern Il2CppGenericClass IList_1_t8474_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8474_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8474_MethodInfos/* methods */
	, IList_1_t8474_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8474_il2cpp_TypeInfo/* element_class */
	, IList_1_t8474_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8474_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8474_0_0_0/* byval_arg */
	, &IList_1_t8474_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8474_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_155.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t4830_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_155MethodDeclarations.h"

// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_158.h"
extern TypeInfo InvokableCall_1_t4831_il2cpp_TypeInfo;
extern TypeInfo Void_t99_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_158MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m29675_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m29677_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t4830____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t4830_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t4830, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t4830_FieldInfos[] =
{
	&CachedInvokableCall_1_t4830____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4830_CachedInvokableCall_1__ctor_m29673_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1093_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m29673_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m29673_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t4830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4830_CachedInvokableCall_1__ctor_m29673_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m29673_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t4830_CachedInvokableCall_1_Invoke_m29674_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m29674_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UserAuthorizationDialog>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m29674_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t4830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t4830_CachedInvokableCall_1_Invoke_m29674_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m29674_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t4830_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m29673_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29674_MethodInfo,
	NULL
};
extern MethodInfo Object_ToString_m306_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m29674_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m29678_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t4830_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m29674_MethodInfo,
	&InvokableCall_1_Find_m29678_MethodInfo,
};
extern Il2CppType UnityAction_1_t4832_0_0_0;
extern TypeInfo UnityAction_1_t4832_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisUserAuthorizationDialog_t1093_m38075_MethodInfo;
extern TypeInfo UserAuthorizationDialog_t1093_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m29680_MethodInfo;
extern TypeInfo UserAuthorizationDialog_t1093_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t4830_RGCTXData[8] = 
{
	&UnityAction_1_t4832_0_0_0/* Type Usage */,
	&UnityAction_1_t4832_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisUserAuthorizationDialog_t1093_m38075_MethodInfo/* Method Usage */,
	&UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29680_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m29675_MethodInfo/* Method Usage */,
	&UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m29677_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t4830_0_0_0;
extern Il2CppType CachedInvokableCall_1_t4830_1_0_0;
struct CachedInvokableCall_1_t4830;
extern Il2CppGenericClass CachedInvokableCall_1_t4830_GenericClass;
TypeInfo CachedInvokableCall_1_t4830_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t4830_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t4830_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t4831_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t4830_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t4830_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t4830_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t4830_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t4830_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t4830_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t4830_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t4830)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_162.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
extern TypeInfo UnityAction_1_t4832_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t507_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_162MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6358_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m456_MethodInfo;
extern MethodInfo Delegate_Combine_m2149_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6357_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2468_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6359_MethodInfo;
extern MethodInfo Delegate_get_Target_m6536_MethodInfo;
extern MethodInfo Delegate_get_Method_m6534_MethodInfo;
struct BaseInvokableCall_t1075;
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UserAuthorizationDialog>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<UnityEngine.UserAuthorizationDialog>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisUserAuthorizationDialog_t1093_m38075(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType UnityAction_1_t4832_0_0_1;
FieldInfo InvokableCall_1_t4831____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t4832_0_0_1/* type */
	, &InvokableCall_1_t4831_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t4831, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t4831_FieldInfos[] =
{
	&InvokableCall_1_t4831____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4831_InvokableCall_1__ctor_m29675_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29675_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m29675_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t4831_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4831_InvokableCall_1__ctor_m29675_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29675_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t4832_0_0_0;
static ParameterInfo InvokableCall_1_t4831_InvokableCall_1__ctor_m29676_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t4832_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m29676_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m29676_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t4831_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4831_InvokableCall_1__ctor_m29676_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m29676_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t4831_InvokableCall_1_Invoke_m29677_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m29677_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m29677_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t4831_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t4831_InvokableCall_1_Invoke_m29677_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m29677_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t4831_InvokableCall_1_Find_m29678_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m29678_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.UserAuthorizationDialog>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m29678_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t4831_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t4831_InvokableCall_1_Find_m29678_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m29678_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t4831_MethodInfos[] =
{
	&InvokableCall_1__ctor_m29675_MethodInfo,
	&InvokableCall_1__ctor_m29676_MethodInfo,
	&InvokableCall_1_Invoke_m29677_MethodInfo,
	&InvokableCall_1_Find_m29678_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t4831_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m29677_MethodInfo,
	&InvokableCall_1_Find_m29678_MethodInfo,
};
extern TypeInfo UnityAction_1_t4832_il2cpp_TypeInfo;
extern TypeInfo UserAuthorizationDialog_t1093_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t4831_RGCTXData[5] = 
{
	&UnityAction_1_t4832_0_0_0/* Type Usage */,
	&UnityAction_1_t4832_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisUserAuthorizationDialog_t1093_m38075_MethodInfo/* Method Usage */,
	&UserAuthorizationDialog_t1093_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m29680_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t4831_0_0_0;
extern Il2CppType InvokableCall_1_t4831_1_0_0;
extern TypeInfo BaseInvokableCall_t1075_il2cpp_TypeInfo;
struct InvokableCall_1_t4831;
extern Il2CppGenericClass InvokableCall_1_t4831_GenericClass;
TypeInfo InvokableCall_1_t4831_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t4831_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t4831_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t4831_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t4831_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t4831_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t4831_0_0_0/* byval_arg */
	, &InvokableCall_1_t4831_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t4831_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t4831_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t4831)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t4832_UnityAction_1__ctor_m29679_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m29679_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m29679_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t4832_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t4832_UnityAction_1__ctor_m29679_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m29679_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
static ParameterInfo UnityAction_1_t4832_UnityAction_1_Invoke_m29680_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1093_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m29680_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m29680_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t4832_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4832_UnityAction_1_Invoke_m29680_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m29680_GenericMethod/* genericMethod */

};
extern Il2CppType UserAuthorizationDialog_t1093_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t4832_UnityAction_1_BeginInvoke_m29681_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &UserAuthorizationDialog_t1093_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m29681_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m29681_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t4832_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t4832_UnityAction_1_BeginInvoke_m29681_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m29681_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t4832_UnityAction_1_EndInvoke_m29682_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m29682_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.UserAuthorizationDialog>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m29682_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t4832_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t4832_UnityAction_1_EndInvoke_m29682_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m29682_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t4832_MethodInfos[] =
{
	&UnityAction_1__ctor_m29679_MethodInfo,
	&UnityAction_1_Invoke_m29680_MethodInfo,
	&UnityAction_1_BeginInvoke_m29681_MethodInfo,
	&UnityAction_1_EndInvoke_m29682_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2241_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2242_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2243_MethodInfo;
extern MethodInfo Delegate_Clone_m2244_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2245_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2246_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2247_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m29681_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m29682_MethodInfo;
static MethodInfo* UnityAction_1_t4832_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m29680_MethodInfo,
	&UnityAction_1_BeginInvoke_m29681_MethodInfo,
	&UnityAction_1_EndInvoke_m29682_MethodInfo,
};
extern TypeInfo ICloneable_t481_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t482_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t4832_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t4832_1_0_0;
extern TypeInfo MulticastDelegate_t325_il2cpp_TypeInfo;
struct UnityAction_1_t4832;
extern Il2CppGenericClass UnityAction_1_t4832_GenericClass;
TypeInfo UnityAction_1_t4832_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t4832_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t4832_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t4832_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t4832_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t4832_0_0_0/* byval_arg */
	, &UnityAction_1_t4832_1_0_0/* this_arg */
	, UnityAction_1_t4832_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t4832_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t4832)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6694_il2cpp_TypeInfo;

// UnityEngine.Internal.DefaultValueAttribute
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttribute.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>
extern MethodInfo IEnumerator_1_get_Current_m48221_MethodInfo;
static PropertyInfo IEnumerator_1_t6694____Current_PropertyInfo = 
{
	&IEnumerator_1_t6694_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48221_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6694_PropertyInfos[] =
{
	&IEnumerator_1_t6694____Current_PropertyInfo,
	NULL
};
extern Il2CppType DefaultValueAttribute_t1094_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48221_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48221_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6694_il2cpp_TypeInfo/* declaring_type */
	, &DefaultValueAttribute_t1094_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48221_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6694_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48221_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6694_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6694_0_0_0;
extern Il2CppType IEnumerator_1_t6694_1_0_0;
struct IEnumerator_1_t6694;
extern Il2CppGenericClass IEnumerator_1_t6694_GenericClass;
TypeInfo IEnumerator_1_t6694_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6694_MethodInfos/* methods */
	, IEnumerator_1_t6694_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6694_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6694_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6694_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6694_0_0_0/* byval_arg */
	, &IEnumerator_1_t6694_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6694_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_476.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4833_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_476MethodDeclarations.h"

extern TypeInfo DefaultValueAttribute_t1094_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29687_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDefaultValueAttribute_t1094_m38077_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Internal.DefaultValueAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Internal.DefaultValueAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisDefaultValueAttribute_t1094_m38077(__this, p0, method) (DefaultValueAttribute_t1094 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4833____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4833_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4833, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4833____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4833_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4833, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4833_FieldInfos[] =
{
	&InternalEnumerator_1_t4833____array_0_FieldInfo,
	&InternalEnumerator_1_t4833____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29684_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4833____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4833_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29684_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4833____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4833_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29687_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4833_PropertyInfos[] =
{
	&InternalEnumerator_1_t4833____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4833____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4833_InternalEnumerator_1__ctor_m29683_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29683_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29683_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4833_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4833_InternalEnumerator_1__ctor_m29683_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29683_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29684_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29684_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4833_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29684_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29685_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29685_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4833_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29685_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29686_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29686_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4833_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29686_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultValueAttribute_t1094_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29687_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Internal.DefaultValueAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29687_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4833_il2cpp_TypeInfo/* declaring_type */
	, &DefaultValueAttribute_t1094_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29687_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4833_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29683_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29684_MethodInfo,
	&InternalEnumerator_1_Dispose_m29685_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29686_MethodInfo,
	&InternalEnumerator_1_get_Current_m29687_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29686_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29685_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4833_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29684_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29686_MethodInfo,
	&InternalEnumerator_1_Dispose_m29685_MethodInfo,
	&InternalEnumerator_1_get_Current_m29687_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4833_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6694_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4833_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6694_il2cpp_TypeInfo, 7},
};
extern TypeInfo DefaultValueAttribute_t1094_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4833_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29687_MethodInfo/* Method Usage */,
	&DefaultValueAttribute_t1094_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDefaultValueAttribute_t1094_m38077_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4833_0_0_0;
extern Il2CppType InternalEnumerator_1_t4833_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4833_GenericClass;
TypeInfo InternalEnumerator_1_t4833_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4833_MethodInfos/* methods */
	, InternalEnumerator_1_t4833_PropertyInfos/* properties */
	, InternalEnumerator_1_t4833_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4833_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4833_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4833_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4833_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4833_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4833_1_0_0/* this_arg */
	, InternalEnumerator_1_t4833_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4833_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4833_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4833)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8476_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>
extern MethodInfo ICollection_1_get_Count_m48222_MethodInfo;
static PropertyInfo ICollection_1_t8476____Count_PropertyInfo = 
{
	&ICollection_1_t8476_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48222_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48223_MethodInfo;
static PropertyInfo ICollection_1_t8476____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8476_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48223_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8476_PropertyInfos[] =
{
	&ICollection_1_t8476____Count_PropertyInfo,
	&ICollection_1_t8476____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48222_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m48222_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8476_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48222_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48223_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48223_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8476_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48223_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultValueAttribute_t1094_0_0_0;
extern Il2CppType DefaultValueAttribute_t1094_0_0_0;
static ParameterInfo ICollection_1_t8476_ICollection_1_Add_m48224_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttribute_t1094_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48224_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Add(T)
MethodInfo ICollection_1_Add_m48224_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8476_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8476_ICollection_1_Add_m48224_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48224_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48225_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Clear()
MethodInfo ICollection_1_Clear_m48225_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8476_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48225_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultValueAttribute_t1094_0_0_0;
static ParameterInfo ICollection_1_t8476_ICollection_1_Contains_m48226_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttribute_t1094_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48226_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m48226_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8476_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8476_ICollection_1_Contains_m48226_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48226_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultValueAttributeU5BU5D_t5487_0_0_0;
extern Il2CppType DefaultValueAttributeU5BU5D_t5487_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8476_ICollection_1_CopyTo_m48227_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttributeU5BU5D_t5487_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48227_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48227_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8476_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8476_ICollection_1_CopyTo_m48227_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48227_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultValueAttribute_t1094_0_0_0;
static ParameterInfo ICollection_1_t8476_ICollection_1_Remove_m48228_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttribute_t1094_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48228_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.DefaultValueAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m48228_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8476_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8476_ICollection_1_Remove_m48228_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48228_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8476_MethodInfos[] =
{
	&ICollection_1_get_Count_m48222_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48223_MethodInfo,
	&ICollection_1_Add_m48224_MethodInfo,
	&ICollection_1_Clear_m48225_MethodInfo,
	&ICollection_1_Contains_m48226_MethodInfo,
	&ICollection_1_CopyTo_m48227_MethodInfo,
	&ICollection_1_Remove_m48228_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8478_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8476_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8478_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8476_0_0_0;
extern Il2CppType ICollection_1_t8476_1_0_0;
struct ICollection_1_t8476;
extern Il2CppGenericClass ICollection_1_t8476_GenericClass;
TypeInfo ICollection_1_t8476_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8476_MethodInfos/* methods */
	, ICollection_1_t8476_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8476_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8476_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8476_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8476_0_0_0/* byval_arg */
	, &ICollection_1_t8476_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8476_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.DefaultValueAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.DefaultValueAttribute>
extern Il2CppType IEnumerator_1_t6694_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48229_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.DefaultValueAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48229_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8478_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6694_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48229_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8478_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48229_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8478_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8478_0_0_0;
extern Il2CppType IEnumerable_1_t8478_1_0_0;
struct IEnumerable_1_t8478;
extern Il2CppGenericClass IEnumerable_1_t8478_GenericClass;
TypeInfo IEnumerable_1_t8478_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8478_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8478_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8478_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8478_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8478_0_0_0/* byval_arg */
	, &IEnumerable_1_t8478_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8478_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8477_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>
extern MethodInfo IList_1_get_Item_m48230_MethodInfo;
extern MethodInfo IList_1_set_Item_m48231_MethodInfo;
static PropertyInfo IList_1_t8477____Item_PropertyInfo = 
{
	&IList_1_t8477_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48230_MethodInfo/* get */
	, &IList_1_set_Item_m48231_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8477_PropertyInfos[] =
{
	&IList_1_t8477____Item_PropertyInfo,
	NULL
};
extern Il2CppType DefaultValueAttribute_t1094_0_0_0;
static ParameterInfo IList_1_t8477_IList_1_IndexOf_m48232_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttribute_t1094_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48232_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48232_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8477_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8477_IList_1_IndexOf_m48232_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48232_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DefaultValueAttribute_t1094_0_0_0;
static ParameterInfo IList_1_t8477_IList_1_Insert_m48233_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttribute_t1094_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48233_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48233_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8477_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8477_IList_1_Insert_m48233_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48233_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8477_IList_1_RemoveAt_m48234_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48234_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48234_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8477_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8477_IList_1_RemoveAt_m48234_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48234_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8477_IList_1_get_Item_m48230_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType DefaultValueAttribute_t1094_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48230_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48230_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8477_il2cpp_TypeInfo/* declaring_type */
	, &DefaultValueAttribute_t1094_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8477_IList_1_get_Item_m48230_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48230_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DefaultValueAttribute_t1094_0_0_0;
static ParameterInfo IList_1_t8477_IList_1_set_Item_m48231_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DefaultValueAttribute_t1094_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48231_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.DefaultValueAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48231_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8477_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8477_IList_1_set_Item_m48231_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48231_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8477_MethodInfos[] =
{
	&IList_1_IndexOf_m48232_MethodInfo,
	&IList_1_Insert_m48233_MethodInfo,
	&IList_1_RemoveAt_m48234_MethodInfo,
	&IList_1_get_Item_m48230_MethodInfo,
	&IList_1_set_Item_m48231_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8477_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8476_il2cpp_TypeInfo,
	&IEnumerable_1_t8478_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8477_0_0_0;
extern Il2CppType IList_1_t8477_1_0_0;
struct IList_1_t8477;
extern Il2CppGenericClass IList_1_t8477_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8477_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8477_MethodInfos/* methods */
	, IList_1_t8477_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8477_il2cpp_TypeInfo/* element_class */
	, IList_1_t8477_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8477_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8477_0_0_0/* byval_arg */
	, &IList_1_t8477_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8477_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6696_il2cpp_TypeInfo;

// UnityEngine.Internal.ExcludeFromDocsAttribute
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAttribute.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern MethodInfo IEnumerator_1_get_Current_m48235_MethodInfo;
static PropertyInfo IEnumerator_1_t6696____Current_PropertyInfo = 
{
	&IEnumerator_1_t6696_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48235_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6696_PropertyInfos[] =
{
	&IEnumerator_1_t6696____Current_PropertyInfo,
	NULL
};
extern Il2CppType ExcludeFromDocsAttribute_t1095_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48235_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48235_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6696_il2cpp_TypeInfo/* declaring_type */
	, &ExcludeFromDocsAttribute_t1095_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48235_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6696_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48235_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6696_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6696_0_0_0;
extern Il2CppType IEnumerator_1_t6696_1_0_0;
struct IEnumerator_1_t6696;
extern Il2CppGenericClass IEnumerator_1_t6696_GenericClass;
TypeInfo IEnumerator_1_t6696_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6696_MethodInfos/* methods */
	, IEnumerator_1_t6696_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6696_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6696_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6696_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6696_0_0_0/* byval_arg */
	, &IEnumerator_1_t6696_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6696_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_477.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4834_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_477MethodDeclarations.h"

extern TypeInfo ExcludeFromDocsAttribute_t1095_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29692_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisExcludeFromDocsAttribute_t1095_m38088_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Internal.ExcludeFromDocsAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Internal.ExcludeFromDocsAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisExcludeFromDocsAttribute_t1095_m38088(__this, p0, method) (ExcludeFromDocsAttribute_t1095 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4834____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4834_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4834, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4834____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4834_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4834, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4834_FieldInfos[] =
{
	&InternalEnumerator_1_t4834____array_0_FieldInfo,
	&InternalEnumerator_1_t4834____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29689_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4834____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4834_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29689_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4834____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4834_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29692_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4834_PropertyInfos[] =
{
	&InternalEnumerator_1_t4834____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4834____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4834_InternalEnumerator_1__ctor_m29688_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29688_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29688_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4834_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4834_InternalEnumerator_1__ctor_m29688_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29688_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29689_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29689_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4834_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29689_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29690_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29690_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4834_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29690_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29691_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29691_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4834_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29691_GenericMethod/* genericMethod */

};
extern Il2CppType ExcludeFromDocsAttribute_t1095_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29692_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29692_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4834_il2cpp_TypeInfo/* declaring_type */
	, &ExcludeFromDocsAttribute_t1095_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29692_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4834_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29688_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29689_MethodInfo,
	&InternalEnumerator_1_Dispose_m29690_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29691_MethodInfo,
	&InternalEnumerator_1_get_Current_m29692_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29691_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29690_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4834_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29689_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29691_MethodInfo,
	&InternalEnumerator_1_Dispose_m29690_MethodInfo,
	&InternalEnumerator_1_get_Current_m29692_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4834_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6696_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4834_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6696_il2cpp_TypeInfo, 7},
};
extern TypeInfo ExcludeFromDocsAttribute_t1095_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4834_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29692_MethodInfo/* Method Usage */,
	&ExcludeFromDocsAttribute_t1095_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisExcludeFromDocsAttribute_t1095_m38088_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4834_0_0_0;
extern Il2CppType InternalEnumerator_1_t4834_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4834_GenericClass;
TypeInfo InternalEnumerator_1_t4834_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4834_MethodInfos/* methods */
	, InternalEnumerator_1_t4834_PropertyInfos/* properties */
	, InternalEnumerator_1_t4834_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4834_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4834_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4834_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4834_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4834_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4834_1_0_0/* this_arg */
	, InternalEnumerator_1_t4834_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4834_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4834_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4834)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8479_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern MethodInfo ICollection_1_get_Count_m48236_MethodInfo;
static PropertyInfo ICollection_1_t8479____Count_PropertyInfo = 
{
	&ICollection_1_t8479_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48236_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48237_MethodInfo;
static PropertyInfo ICollection_1_t8479____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8479_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48237_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8479_PropertyInfos[] =
{
	&ICollection_1_t8479____Count_PropertyInfo,
	&ICollection_1_t8479____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48236_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m48236_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8479_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48236_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48237_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48237_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8479_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48237_GenericMethod/* genericMethod */

};
extern Il2CppType ExcludeFromDocsAttribute_t1095_0_0_0;
extern Il2CppType ExcludeFromDocsAttribute_t1095_0_0_0;
static ParameterInfo ICollection_1_t8479_ICollection_1_Add_m48238_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttribute_t1095_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48238_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Add(T)
MethodInfo ICollection_1_Add_m48238_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8479_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8479_ICollection_1_Add_m48238_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48238_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48239_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Clear()
MethodInfo ICollection_1_Clear_m48239_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8479_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48239_GenericMethod/* genericMethod */

};
extern Il2CppType ExcludeFromDocsAttribute_t1095_0_0_0;
static ParameterInfo ICollection_1_t8479_ICollection_1_Contains_m48240_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttribute_t1095_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48240_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m48240_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8479_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8479_ICollection_1_Contains_m48240_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48240_GenericMethod/* genericMethod */

};
extern Il2CppType ExcludeFromDocsAttributeU5BU5D_t5488_0_0_0;
extern Il2CppType ExcludeFromDocsAttributeU5BU5D_t5488_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8479_ICollection_1_CopyTo_m48241_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttributeU5BU5D_t5488_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48241_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48241_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8479_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8479_ICollection_1_CopyTo_m48241_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48241_GenericMethod/* genericMethod */

};
extern Il2CppType ExcludeFromDocsAttribute_t1095_0_0_0;
static ParameterInfo ICollection_1_t8479_ICollection_1_Remove_m48242_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttribute_t1095_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48242_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m48242_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8479_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8479_ICollection_1_Remove_m48242_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48242_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8479_MethodInfos[] =
{
	&ICollection_1_get_Count_m48236_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48237_MethodInfo,
	&ICollection_1_Add_m48238_MethodInfo,
	&ICollection_1_Clear_m48239_MethodInfo,
	&ICollection_1_Contains_m48240_MethodInfo,
	&ICollection_1_CopyTo_m48241_MethodInfo,
	&ICollection_1_Remove_m48242_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8481_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8479_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8481_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8479_0_0_0;
extern Il2CppType ICollection_1_t8479_1_0_0;
struct ICollection_1_t8479;
extern Il2CppGenericClass ICollection_1_t8479_GenericClass;
TypeInfo ICollection_1_t8479_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8479_MethodInfos/* methods */
	, ICollection_1_t8479_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8479_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8479_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8479_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8479_0_0_0/* byval_arg */
	, &ICollection_1_t8479_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8479_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern Il2CppType IEnumerator_1_t6696_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48243_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48243_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8481_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6696_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48243_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8481_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48243_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8481_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8481_0_0_0;
extern Il2CppType IEnumerable_1_t8481_1_0_0;
struct IEnumerable_1_t8481;
extern Il2CppGenericClass IEnumerable_1_t8481_GenericClass;
TypeInfo IEnumerable_1_t8481_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8481_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8481_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8481_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8481_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8481_0_0_0/* byval_arg */
	, &IEnumerable_1_t8481_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8481_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8480_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>
extern MethodInfo IList_1_get_Item_m48244_MethodInfo;
extern MethodInfo IList_1_set_Item_m48245_MethodInfo;
static PropertyInfo IList_1_t8480____Item_PropertyInfo = 
{
	&IList_1_t8480_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48244_MethodInfo/* get */
	, &IList_1_set_Item_m48245_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8480_PropertyInfos[] =
{
	&IList_1_t8480____Item_PropertyInfo,
	NULL
};
extern Il2CppType ExcludeFromDocsAttribute_t1095_0_0_0;
static ParameterInfo IList_1_t8480_IList_1_IndexOf_m48246_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttribute_t1095_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48246_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48246_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8480_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8480_IList_1_IndexOf_m48246_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48246_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ExcludeFromDocsAttribute_t1095_0_0_0;
static ParameterInfo IList_1_t8480_IList_1_Insert_m48247_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttribute_t1095_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48247_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48247_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8480_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8480_IList_1_Insert_m48247_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48247_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8480_IList_1_RemoveAt_m48248_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48248_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48248_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8480_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8480_IList_1_RemoveAt_m48248_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48248_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8480_IList_1_get_Item_m48244_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ExcludeFromDocsAttribute_t1095_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48244_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48244_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8480_il2cpp_TypeInfo/* declaring_type */
	, &ExcludeFromDocsAttribute_t1095_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8480_IList_1_get_Item_m48244_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48244_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ExcludeFromDocsAttribute_t1095_0_0_0;
static ParameterInfo IList_1_t8480_IList_1_set_Item_m48245_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ExcludeFromDocsAttribute_t1095_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48245_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Internal.ExcludeFromDocsAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48245_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8480_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8480_IList_1_set_Item_m48245_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48245_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8480_MethodInfos[] =
{
	&IList_1_IndexOf_m48246_MethodInfo,
	&IList_1_Insert_m48247_MethodInfo,
	&IList_1_RemoveAt_m48248_MethodInfo,
	&IList_1_get_Item_m48244_MethodInfo,
	&IList_1_set_Item_m48245_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8480_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8479_il2cpp_TypeInfo,
	&IEnumerable_1_t8481_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8480_0_0_0;
extern Il2CppType IList_1_t8480_1_0_0;
struct IList_1_t8480;
extern Il2CppGenericClass IList_1_t8480_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8480_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8480_MethodInfos/* methods */
	, IList_1_t8480_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8480_il2cpp_TypeInfo/* element_class */
	, IList_1_t8480_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8480_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8480_0_0_0/* byval_arg */
	, &IList_1_t8480_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8480_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6698_il2cpp_TypeInfo;

// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern MethodInfo IEnumerator_1_get_Current_m48249_MethodInfo;
static PropertyInfo IEnumerator_1_t6698____Current_PropertyInfo = 
{
	&IEnumerator_1_t6698_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48249_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6698_PropertyInfos[] =
{
	&IEnumerator_1_t6698____Current_PropertyInfo,
	NULL
};
extern Il2CppType FormerlySerializedAsAttribute_t420_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48249_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48249_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6698_il2cpp_TypeInfo/* declaring_type */
	, &FormerlySerializedAsAttribute_t420_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48249_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6698_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48249_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6698_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6698_0_0_0;
extern Il2CppType IEnumerator_1_t6698_1_0_0;
struct IEnumerator_1_t6698;
extern Il2CppGenericClass IEnumerator_1_t6698_GenericClass;
TypeInfo IEnumerator_1_t6698_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6698_MethodInfos/* methods */
	, IEnumerator_1_t6698_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6698_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6698_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6698_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6698_0_0_0/* byval_arg */
	, &IEnumerator_1_t6698_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6698_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_478.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4835_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_478MethodDeclarations.h"

extern TypeInfo FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29697_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFormerlySerializedAsAttribute_t420_m38099_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Serialization.FormerlySerializedAsAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Serialization.FormerlySerializedAsAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisFormerlySerializedAsAttribute_t420_m38099(__this, p0, method) (FormerlySerializedAsAttribute_t420 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4835____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4835_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4835, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4835____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4835_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4835, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4835_FieldInfos[] =
{
	&InternalEnumerator_1_t4835____array_0_FieldInfo,
	&InternalEnumerator_1_t4835____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29694_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4835____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4835_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29694_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4835____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4835_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29697_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4835_PropertyInfos[] =
{
	&InternalEnumerator_1_t4835____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4835____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4835_InternalEnumerator_1__ctor_m29693_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29693_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29693_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4835_InternalEnumerator_1__ctor_m29693_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29693_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29694_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29694_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4835_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29694_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29695_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29695_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4835_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29695_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29696_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29696_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4835_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29696_GenericMethod/* genericMethod */

};
extern Il2CppType FormerlySerializedAsAttribute_t420_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29697_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29697_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4835_il2cpp_TypeInfo/* declaring_type */
	, &FormerlySerializedAsAttribute_t420_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29697_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4835_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29693_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29694_MethodInfo,
	&InternalEnumerator_1_Dispose_m29695_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29696_MethodInfo,
	&InternalEnumerator_1_get_Current_m29697_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29696_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29695_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4835_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29694_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29696_MethodInfo,
	&InternalEnumerator_1_Dispose_m29695_MethodInfo,
	&InternalEnumerator_1_get_Current_m29697_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4835_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6698_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4835_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6698_il2cpp_TypeInfo, 7},
};
extern TypeInfo FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4835_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29697_MethodInfo/* Method Usage */,
	&FormerlySerializedAsAttribute_t420_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisFormerlySerializedAsAttribute_t420_m38099_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4835_0_0_0;
extern Il2CppType InternalEnumerator_1_t4835_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4835_GenericClass;
TypeInfo InternalEnumerator_1_t4835_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4835_MethodInfos/* methods */
	, InternalEnumerator_1_t4835_PropertyInfos/* properties */
	, InternalEnumerator_1_t4835_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4835_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4835_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4835_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4835_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4835_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4835_1_0_0/* this_arg */
	, InternalEnumerator_1_t4835_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4835_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4835_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4835)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8482_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern MethodInfo ICollection_1_get_Count_m48250_MethodInfo;
static PropertyInfo ICollection_1_t8482____Count_PropertyInfo = 
{
	&ICollection_1_t8482_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48250_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48251_MethodInfo;
static PropertyInfo ICollection_1_t8482____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8482_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48251_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8482_PropertyInfos[] =
{
	&ICollection_1_t8482____Count_PropertyInfo,
	&ICollection_1_t8482____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48250_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m48250_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8482_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48250_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48251_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48251_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8482_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48251_GenericMethod/* genericMethod */

};
extern Il2CppType FormerlySerializedAsAttribute_t420_0_0_0;
extern Il2CppType FormerlySerializedAsAttribute_t420_0_0_0;
static ParameterInfo ICollection_1_t8482_ICollection_1_Add_m48252_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttribute_t420_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48252_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Add(T)
MethodInfo ICollection_1_Add_m48252_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8482_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8482_ICollection_1_Add_m48252_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48252_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48253_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Clear()
MethodInfo ICollection_1_Clear_m48253_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8482_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48253_GenericMethod/* genericMethod */

};
extern Il2CppType FormerlySerializedAsAttribute_t420_0_0_0;
static ParameterInfo ICollection_1_t8482_ICollection_1_Contains_m48254_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttribute_t420_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48254_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m48254_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8482_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8482_ICollection_1_Contains_m48254_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48254_GenericMethod/* genericMethod */

};
extern Il2CppType FormerlySerializedAsAttributeU5BU5D_t5489_0_0_0;
extern Il2CppType FormerlySerializedAsAttributeU5BU5D_t5489_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8482_ICollection_1_CopyTo_m48255_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttributeU5BU5D_t5489_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48255_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48255_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8482_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8482_ICollection_1_CopyTo_m48255_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48255_GenericMethod/* genericMethod */

};
extern Il2CppType FormerlySerializedAsAttribute_t420_0_0_0;
static ParameterInfo ICollection_1_t8482_ICollection_1_Remove_m48256_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttribute_t420_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48256_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m48256_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8482_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8482_ICollection_1_Remove_m48256_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48256_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8482_MethodInfos[] =
{
	&ICollection_1_get_Count_m48250_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48251_MethodInfo,
	&ICollection_1_Add_m48252_MethodInfo,
	&ICollection_1_Clear_m48253_MethodInfo,
	&ICollection_1_Contains_m48254_MethodInfo,
	&ICollection_1_CopyTo_m48255_MethodInfo,
	&ICollection_1_Remove_m48256_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8484_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8482_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8484_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8482_0_0_0;
extern Il2CppType ICollection_1_t8482_1_0_0;
struct ICollection_1_t8482;
extern Il2CppGenericClass ICollection_1_t8482_GenericClass;
TypeInfo ICollection_1_t8482_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8482_MethodInfos/* methods */
	, ICollection_1_t8482_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8482_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8482_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8482_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8482_0_0_0/* byval_arg */
	, &ICollection_1_t8482_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8482_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern Il2CppType IEnumerator_1_t6698_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48257_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48257_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8484_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6698_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48257_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8484_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48257_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8484_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8484_0_0_0;
extern Il2CppType IEnumerable_1_t8484_1_0_0;
struct IEnumerable_1_t8484;
extern Il2CppGenericClass IEnumerable_1_t8484_GenericClass;
TypeInfo IEnumerable_1_t8484_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8484_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8484_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8484_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8484_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8484_0_0_0/* byval_arg */
	, &IEnumerable_1_t8484_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8484_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8483_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>
extern MethodInfo IList_1_get_Item_m48258_MethodInfo;
extern MethodInfo IList_1_set_Item_m48259_MethodInfo;
static PropertyInfo IList_1_t8483____Item_PropertyInfo = 
{
	&IList_1_t8483_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48258_MethodInfo/* get */
	, &IList_1_set_Item_m48259_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8483_PropertyInfos[] =
{
	&IList_1_t8483____Item_PropertyInfo,
	NULL
};
extern Il2CppType FormerlySerializedAsAttribute_t420_0_0_0;
static ParameterInfo IList_1_t8483_IList_1_IndexOf_m48260_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttribute_t420_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48260_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48260_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8483_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8483_IList_1_IndexOf_m48260_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48260_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FormerlySerializedAsAttribute_t420_0_0_0;
static ParameterInfo IList_1_t8483_IList_1_Insert_m48261_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttribute_t420_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48261_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48261_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8483_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8483_IList_1_Insert_m48261_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48261_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8483_IList_1_RemoveAt_m48262_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48262_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48262_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8483_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8483_IList_1_RemoveAt_m48262_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48262_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8483_IList_1_get_Item_m48258_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType FormerlySerializedAsAttribute_t420_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48258_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48258_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8483_il2cpp_TypeInfo/* declaring_type */
	, &FormerlySerializedAsAttribute_t420_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8483_IList_1_get_Item_m48258_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48258_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType FormerlySerializedAsAttribute_t420_0_0_0;
static ParameterInfo IList_1_t8483_IList_1_set_Item_m48259_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &FormerlySerializedAsAttribute_t420_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48259_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Serialization.FormerlySerializedAsAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48259_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8483_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8483_IList_1_set_Item_m48259_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48259_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8483_MethodInfos[] =
{
	&IList_1_IndexOf_m48260_MethodInfo,
	&IList_1_Insert_m48261_MethodInfo,
	&IList_1_RemoveAt_m48262_MethodInfo,
	&IList_1_get_Item_m48258_MethodInfo,
	&IList_1_set_Item_m48259_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8483_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8482_il2cpp_TypeInfo,
	&IEnumerable_1_t8484_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8483_0_0_0;
extern Il2CppType IList_1_t8483_1_0_0;
struct IList_1_t8483;
extern Il2CppGenericClass IList_1_t8483_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8483_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8483_MethodInfos/* methods */
	, IList_1_t8483_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8483_il2cpp_TypeInfo/* element_class */
	, IList_1_t8483_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8483_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8483_0_0_0/* byval_arg */
	, &IList_1_t8483_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8483_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6700_il2cpp_TypeInfo;

// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRules>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRules>
extern MethodInfo IEnumerator_1_get_Current_m48263_MethodInfo;
static PropertyInfo IEnumerator_1_t6700____Current_PropertyInfo = 
{
	&IEnumerator_1_t6700_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48263_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6700_PropertyInfos[] =
{
	&IEnumerator_1_t6700____Current_PropertyInfo,
	NULL
};
extern Il2CppType TypeInferenceRules_t1096_0_0_0;
extern void* RuntimeInvoker_TypeInferenceRules_t1096 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48263_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRules>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48263_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6700_il2cpp_TypeInfo/* declaring_type */
	, &TypeInferenceRules_t1096_0_0_0/* return_type */
	, RuntimeInvoker_TypeInferenceRules_t1096/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48263_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6700_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48263_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6700_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6700_0_0_0;
extern Il2CppType IEnumerator_1_t6700_1_0_0;
struct IEnumerator_1_t6700;
extern Il2CppGenericClass IEnumerator_1_t6700_GenericClass;
TypeInfo IEnumerator_1_t6700_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6700_MethodInfos/* methods */
	, IEnumerator_1_t6700_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6700_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6700_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6700_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6700_0_0_0/* byval_arg */
	, &IEnumerator_1_t6700_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6700_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_479.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4836_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_479MethodDeclarations.h"

extern TypeInfo TypeInferenceRules_t1096_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29702_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTypeInferenceRules_t1096_m38110_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngineInternal.TypeInferenceRules>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngineInternal.TypeInferenceRules>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisTypeInferenceRules_t1096_m38110 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29698_MethodInfo;
 void InternalEnumerator_1__ctor_m29698 (InternalEnumerator_1_t4836 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29699_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29699 (InternalEnumerator_1_t4836 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29702(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29702_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&TypeInferenceRules_t1096_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29700_MethodInfo;
 void InternalEnumerator_1_Dispose_m29700 (InternalEnumerator_1_t4836 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29701_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29701 (InternalEnumerator_1_t4836 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29702 (InternalEnumerator_1_t4836 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisTypeInferenceRules_t1096_m38110(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisTypeInferenceRules_t1096_m38110_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4836____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4836_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4836, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4836____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4836_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4836, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4836_FieldInfos[] =
{
	&InternalEnumerator_1_t4836____array_0_FieldInfo,
	&InternalEnumerator_1_t4836____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4836____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4836_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29699_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4836____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4836_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29702_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4836_PropertyInfos[] =
{
	&InternalEnumerator_1_t4836____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4836____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4836_InternalEnumerator_1__ctor_m29698_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29698_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29698_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29698/* method */
	, &InternalEnumerator_1_t4836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4836_InternalEnumerator_1__ctor_m29698_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29698_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29699_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29699_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29699/* method */
	, &InternalEnumerator_1_t4836_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29699_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29700_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29700_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29700/* method */
	, &InternalEnumerator_1_t4836_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29700_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29701_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29701_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29701/* method */
	, &InternalEnumerator_1_t4836_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29701_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRules_t1096_0_0_0;
extern void* RuntimeInvoker_TypeInferenceRules_t1096 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29702_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRules>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29702_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29702/* method */
	, &InternalEnumerator_1_t4836_il2cpp_TypeInfo/* declaring_type */
	, &TypeInferenceRules_t1096_0_0_0/* return_type */
	, RuntimeInvoker_TypeInferenceRules_t1096/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29702_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4836_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29698_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29699_MethodInfo,
	&InternalEnumerator_1_Dispose_m29700_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29701_MethodInfo,
	&InternalEnumerator_1_get_Current_m29702_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4836_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29699_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29701_MethodInfo,
	&InternalEnumerator_1_Dispose_m29700_MethodInfo,
	&InternalEnumerator_1_get_Current_m29702_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4836_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6700_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4836_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6700_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4836_0_0_0;
extern Il2CppType InternalEnumerator_1_t4836_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4836_GenericClass;
TypeInfo InternalEnumerator_1_t4836_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4836_MethodInfos/* methods */
	, InternalEnumerator_1_t4836_PropertyInfos/* properties */
	, InternalEnumerator_1_t4836_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4836_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4836_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4836_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4836_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4836_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4836_1_0_0/* this_arg */
	, InternalEnumerator_1_t4836_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4836_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4836)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8485_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>
extern MethodInfo ICollection_1_get_Count_m48264_MethodInfo;
static PropertyInfo ICollection_1_t8485____Count_PropertyInfo = 
{
	&ICollection_1_t8485_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48264_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48265_MethodInfo;
static PropertyInfo ICollection_1_t8485____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8485_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48265_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8485_PropertyInfos[] =
{
	&ICollection_1_t8485____Count_PropertyInfo,
	&ICollection_1_t8485____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48264_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::get_Count()
MethodInfo ICollection_1_get_Count_m48264_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8485_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48264_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48265_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48265_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8485_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48265_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRules_t1096_0_0_0;
extern Il2CppType TypeInferenceRules_t1096_0_0_0;
static ParameterInfo ICollection_1_t8485_ICollection_1_Add_m48266_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRules_t1096_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48266_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Add(T)
MethodInfo ICollection_1_Add_m48266_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8485_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8485_ICollection_1_Add_m48266_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48266_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48267_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Clear()
MethodInfo ICollection_1_Clear_m48267_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8485_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48267_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRules_t1096_0_0_0;
static ParameterInfo ICollection_1_t8485_ICollection_1_Contains_m48268_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRules_t1096_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48268_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Contains(T)
MethodInfo ICollection_1_Contains_m48268_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8485_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8485_ICollection_1_Contains_m48268_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48268_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRulesU5BU5D_t5490_0_0_0;
extern Il2CppType TypeInferenceRulesU5BU5D_t5490_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8485_ICollection_1_CopyTo_m48269_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRulesU5BU5D_t5490_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48269_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48269_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8485_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8485_ICollection_1_CopyTo_m48269_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48269_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRules_t1096_0_0_0;
static ParameterInfo ICollection_1_t8485_ICollection_1_Remove_m48270_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRules_t1096_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48270_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRules>::Remove(T)
MethodInfo ICollection_1_Remove_m48270_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8485_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8485_ICollection_1_Remove_m48270_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48270_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8485_MethodInfos[] =
{
	&ICollection_1_get_Count_m48264_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48265_MethodInfo,
	&ICollection_1_Add_m48266_MethodInfo,
	&ICollection_1_Clear_m48267_MethodInfo,
	&ICollection_1_Contains_m48268_MethodInfo,
	&ICollection_1_CopyTo_m48269_MethodInfo,
	&ICollection_1_Remove_m48270_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8487_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8485_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8487_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8485_0_0_0;
extern Il2CppType ICollection_1_t8485_1_0_0;
struct ICollection_1_t8485;
extern Il2CppGenericClass ICollection_1_t8485_GenericClass;
TypeInfo ICollection_1_t8485_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8485_MethodInfos/* methods */
	, ICollection_1_t8485_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8485_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8485_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8485_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8485_0_0_0/* byval_arg */
	, &ICollection_1_t8485_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8485_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRules>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRules>
extern Il2CppType IEnumerator_1_t6700_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48271_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRules>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48271_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8487_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6700_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48271_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8487_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48271_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8487_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8487_0_0_0;
extern Il2CppType IEnumerable_1_t8487_1_0_0;
struct IEnumerable_1_t8487;
extern Il2CppGenericClass IEnumerable_1_t8487_GenericClass;
TypeInfo IEnumerable_1_t8487_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8487_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8487_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8487_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8487_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8487_0_0_0/* byval_arg */
	, &IEnumerable_1_t8487_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8487_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8486_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>
extern MethodInfo IList_1_get_Item_m48272_MethodInfo;
extern MethodInfo IList_1_set_Item_m48273_MethodInfo;
static PropertyInfo IList_1_t8486____Item_PropertyInfo = 
{
	&IList_1_t8486_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48272_MethodInfo/* get */
	, &IList_1_set_Item_m48273_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8486_PropertyInfos[] =
{
	&IList_1_t8486____Item_PropertyInfo,
	NULL
};
extern Il2CppType TypeInferenceRules_t1096_0_0_0;
static ParameterInfo IList_1_t8486_IList_1_IndexOf_m48274_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRules_t1096_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48274_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48274_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8486_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8486_IList_1_IndexOf_m48274_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48274_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TypeInferenceRules_t1096_0_0_0;
static ParameterInfo IList_1_t8486_IList_1_Insert_m48275_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRules_t1096_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48275_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48275_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8486_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8486_IList_1_Insert_m48275_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48275_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8486_IList_1_RemoveAt_m48276_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48276_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48276_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8486_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8486_IList_1_RemoveAt_m48276_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48276_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8486_IList_1_get_Item_m48272_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType TypeInferenceRules_t1096_0_0_0;
extern void* RuntimeInvoker_TypeInferenceRules_t1096_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48272_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48272_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8486_il2cpp_TypeInfo/* declaring_type */
	, &TypeInferenceRules_t1096_0_0_0/* return_type */
	, RuntimeInvoker_TypeInferenceRules_t1096_Int32_t93/* invoker_method */
	, IList_1_t8486_IList_1_get_Item_m48272_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48272_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TypeInferenceRules_t1096_0_0_0;
static ParameterInfo IList_1_t8486_IList_1_set_Item_m48273_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRules_t1096_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48273_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRules>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48273_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8486_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8486_IList_1_set_Item_m48273_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48273_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8486_MethodInfos[] =
{
	&IList_1_IndexOf_m48274_MethodInfo,
	&IList_1_Insert_m48275_MethodInfo,
	&IList_1_RemoveAt_m48276_MethodInfo,
	&IList_1_get_Item_m48272_MethodInfo,
	&IList_1_set_Item_m48273_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8486_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8485_il2cpp_TypeInfo,
	&IEnumerable_1_t8487_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8486_0_0_0;
extern Il2CppType IList_1_t8486_1_0_0;
struct IList_1_t8486;
extern Il2CppGenericClass IList_1_t8486_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8486_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8486_MethodInfos/* methods */
	, IList_1_t8486_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8486_il2cpp_TypeInfo/* element_class */
	, IList_1_t8486_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8486_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8486_0_0_0/* byval_arg */
	, &IList_1_t8486_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8486_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6702_il2cpp_TypeInfo;

// UnityEngineInternal.TypeInferenceRuleAttribute
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAttribute.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern MethodInfo IEnumerator_1_get_Current_m48277_MethodInfo;
static PropertyInfo IEnumerator_1_t6702____Current_PropertyInfo = 
{
	&IEnumerator_1_t6702_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48277_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6702_PropertyInfos[] =
{
	&IEnumerator_1_t6702____Current_PropertyInfo,
	NULL
};
extern Il2CppType TypeInferenceRuleAttribute_t1097_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48277_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48277_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6702_il2cpp_TypeInfo/* declaring_type */
	, &TypeInferenceRuleAttribute_t1097_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48277_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6702_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48277_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6702_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6702_0_0_0;
extern Il2CppType IEnumerator_1_t6702_1_0_0;
struct IEnumerator_1_t6702;
extern Il2CppGenericClass IEnumerator_1_t6702_GenericClass;
TypeInfo IEnumerator_1_t6702_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6702_MethodInfos/* methods */
	, IEnumerator_1_t6702_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6702_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6702_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6702_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6702_0_0_0/* byval_arg */
	, &IEnumerator_1_t6702_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6702_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_480.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4837_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_480MethodDeclarations.h"

extern TypeInfo TypeInferenceRuleAttribute_t1097_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29707_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTypeInferenceRuleAttribute_t1097_m38121_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngineInternal.TypeInferenceRuleAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngineInternal.TypeInferenceRuleAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisTypeInferenceRuleAttribute_t1097_m38121(__this, p0, method) (TypeInferenceRuleAttribute_t1097 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4837____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4837_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4837, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4837____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4837_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4837, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4837_FieldInfos[] =
{
	&InternalEnumerator_1_t4837____array_0_FieldInfo,
	&InternalEnumerator_1_t4837____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29704_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4837____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4837_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29704_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4837____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4837_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29707_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4837_PropertyInfos[] =
{
	&InternalEnumerator_1_t4837____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4837____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4837_InternalEnumerator_1__ctor_m29703_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29703_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29703_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4837_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4837_InternalEnumerator_1__ctor_m29703_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29703_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29704_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29704_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4837_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29704_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29705_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29705_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4837_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29705_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29706_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29706_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4837_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29706_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRuleAttribute_t1097_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29707_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29707_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4837_il2cpp_TypeInfo/* declaring_type */
	, &TypeInferenceRuleAttribute_t1097_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29707_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4837_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29703_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29704_MethodInfo,
	&InternalEnumerator_1_Dispose_m29705_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29706_MethodInfo,
	&InternalEnumerator_1_get_Current_m29707_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29706_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29705_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4837_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29704_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29706_MethodInfo,
	&InternalEnumerator_1_Dispose_m29705_MethodInfo,
	&InternalEnumerator_1_get_Current_m29707_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4837_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6702_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4837_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6702_il2cpp_TypeInfo, 7},
};
extern TypeInfo TypeInferenceRuleAttribute_t1097_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4837_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29707_MethodInfo/* Method Usage */,
	&TypeInferenceRuleAttribute_t1097_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTypeInferenceRuleAttribute_t1097_m38121_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4837_0_0_0;
extern Il2CppType InternalEnumerator_1_t4837_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4837_GenericClass;
TypeInfo InternalEnumerator_1_t4837_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4837_MethodInfos/* methods */
	, InternalEnumerator_1_t4837_PropertyInfos/* properties */
	, InternalEnumerator_1_t4837_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4837_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4837_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4837_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4837_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4837_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4837_1_0_0/* this_arg */
	, InternalEnumerator_1_t4837_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4837_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4837_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4837)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8488_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern MethodInfo ICollection_1_get_Count_m48278_MethodInfo;
static PropertyInfo ICollection_1_t8488____Count_PropertyInfo = 
{
	&ICollection_1_t8488_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48278_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48279_MethodInfo;
static PropertyInfo ICollection_1_t8488____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8488_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48279_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8488_PropertyInfos[] =
{
	&ICollection_1_t8488____Count_PropertyInfo,
	&ICollection_1_t8488____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48278_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m48278_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8488_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48278_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48279_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48279_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8488_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48279_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRuleAttribute_t1097_0_0_0;
extern Il2CppType TypeInferenceRuleAttribute_t1097_0_0_0;
static ParameterInfo ICollection_1_t8488_ICollection_1_Add_m48280_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttribute_t1097_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48280_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Add(T)
MethodInfo ICollection_1_Add_m48280_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8488_ICollection_1_Add_m48280_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48280_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48281_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Clear()
MethodInfo ICollection_1_Clear_m48281_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48281_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRuleAttribute_t1097_0_0_0;
static ParameterInfo ICollection_1_t8488_ICollection_1_Contains_m48282_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttribute_t1097_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48282_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m48282_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8488_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8488_ICollection_1_Contains_m48282_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48282_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRuleAttributeU5BU5D_t5491_0_0_0;
extern Il2CppType TypeInferenceRuleAttributeU5BU5D_t5491_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8488_ICollection_1_CopyTo_m48283_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttributeU5BU5D_t5491_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48283_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48283_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8488_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8488_ICollection_1_CopyTo_m48283_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48283_GenericMethod/* genericMethod */

};
extern Il2CppType TypeInferenceRuleAttribute_t1097_0_0_0;
static ParameterInfo ICollection_1_t8488_ICollection_1_Remove_m48284_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttribute_t1097_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48284_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m48284_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8488_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8488_ICollection_1_Remove_m48284_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48284_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8488_MethodInfos[] =
{
	&ICollection_1_get_Count_m48278_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48279_MethodInfo,
	&ICollection_1_Add_m48280_MethodInfo,
	&ICollection_1_Clear_m48281_MethodInfo,
	&ICollection_1_Contains_m48282_MethodInfo,
	&ICollection_1_CopyTo_m48283_MethodInfo,
	&ICollection_1_Remove_m48284_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8490_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8488_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8490_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8488_0_0_0;
extern Il2CppType ICollection_1_t8488_1_0_0;
struct ICollection_1_t8488;
extern Il2CppGenericClass ICollection_1_t8488_GenericClass;
TypeInfo ICollection_1_t8488_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8488_MethodInfos/* methods */
	, ICollection_1_t8488_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8488_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8488_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8488_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8488_0_0_0/* byval_arg */
	, &ICollection_1_t8488_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8488_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRuleAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern Il2CppType IEnumerator_1_t6702_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48285_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngineInternal.TypeInferenceRuleAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48285_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8490_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6702_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48285_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8490_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48285_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8490_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8490_0_0_0;
extern Il2CppType IEnumerable_1_t8490_1_0_0;
struct IEnumerable_1_t8490;
extern Il2CppGenericClass IEnumerable_1_t8490_GenericClass;
TypeInfo IEnumerable_1_t8490_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8490_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8490_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8490_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8490_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8490_0_0_0/* byval_arg */
	, &IEnumerable_1_t8490_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8490_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8489_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>
extern MethodInfo IList_1_get_Item_m48286_MethodInfo;
extern MethodInfo IList_1_set_Item_m48287_MethodInfo;
static PropertyInfo IList_1_t8489____Item_PropertyInfo = 
{
	&IList_1_t8489_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48286_MethodInfo/* get */
	, &IList_1_set_Item_m48287_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8489_PropertyInfos[] =
{
	&IList_1_t8489____Item_PropertyInfo,
	NULL
};
extern Il2CppType TypeInferenceRuleAttribute_t1097_0_0_0;
static ParameterInfo IList_1_t8489_IList_1_IndexOf_m48288_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttribute_t1097_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48288_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48288_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8489_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8489_IList_1_IndexOf_m48288_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48288_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TypeInferenceRuleAttribute_t1097_0_0_0;
static ParameterInfo IList_1_t8489_IList_1_Insert_m48289_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttribute_t1097_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48289_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48289_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8489_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8489_IList_1_Insert_m48289_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48289_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8489_IList_1_RemoveAt_m48290_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48290_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48290_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8489_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8489_IList_1_RemoveAt_m48290_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48290_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8489_IList_1_get_Item_m48286_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType TypeInferenceRuleAttribute_t1097_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48286_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48286_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8489_il2cpp_TypeInfo/* declaring_type */
	, &TypeInferenceRuleAttribute_t1097_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8489_IList_1_get_Item_m48286_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48286_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TypeInferenceRuleAttribute_t1097_0_0_0;
static ParameterInfo IList_1_t8489_IList_1_set_Item_m48287_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypeInferenceRuleAttribute_t1097_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48287_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngineInternal.TypeInferenceRuleAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48287_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8489_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8489_IList_1_set_Item_m48287_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48287_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8489_MethodInfos[] =
{
	&IList_1_IndexOf_m48288_MethodInfo,
	&IList_1_Insert_m48289_MethodInfo,
	&IList_1_RemoveAt_m48290_MethodInfo,
	&IList_1_get_Item_m48286_MethodInfo,
	&IList_1_set_Item_m48287_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8489_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8488_il2cpp_TypeInfo,
	&IEnumerable_1_t8490_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8489_0_0_0;
extern Il2CppType IList_1_t8489_1_0_0;
struct IList_1_t8489;
extern Il2CppGenericClass IList_1_t8489_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8489_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8489_MethodInfos/* methods */
	, IList_1_t8489_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8489_il2cpp_TypeInfo/* element_class */
	, IList_1_t8489_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8489_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8489_0_0_0/* byval_arg */
	, &IList_1_t8489_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8489_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6704_il2cpp_TypeInfo;

// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"


// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern MethodInfo IEnumerator_1_get_Current_m48291_MethodInfo;
static PropertyInfo IEnumerator_1_t6704____Current_PropertyInfo = 
{
	&IEnumerator_1_t6704_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48291_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6704_PropertyInfos[] =
{
	&IEnumerator_1_t6704____Current_PropertyInfo,
	NULL
};
extern Il2CppType ExtensionAttribute_t779_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48291_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48291_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6704_il2cpp_TypeInfo/* declaring_type */
	, &ExtensionAttribute_t779_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48291_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6704_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48291_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6704_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6704_0_0_0;
extern Il2CppType IEnumerator_1_t6704_1_0_0;
struct IEnumerator_1_t6704;
extern Il2CppGenericClass IEnumerator_1_t6704_GenericClass;
TypeInfo IEnumerator_1_t6704_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6704_MethodInfos/* methods */
	, IEnumerator_1_t6704_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6704_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6704_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6704_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6704_0_0_0/* byval_arg */
	, &IEnumerator_1_t6704_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6704_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_481.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4838_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_481MethodDeclarations.h"

extern TypeInfo ExtensionAttribute_t779_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29712_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisExtensionAttribute_t779_m38132_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.ExtensionAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.CompilerServices.ExtensionAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisExtensionAttribute_t779_m38132(__this, p0, method) (ExtensionAttribute_t779 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4838____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4838_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4838, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4838____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4838_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4838, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4838_FieldInfos[] =
{
	&InternalEnumerator_1_t4838____array_0_FieldInfo,
	&InternalEnumerator_1_t4838____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29709_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4838____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4838_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29709_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4838____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4838_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29712_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4838_PropertyInfos[] =
{
	&InternalEnumerator_1_t4838____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4838____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4838_InternalEnumerator_1__ctor_m29708_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29708_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29708_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4838_InternalEnumerator_1__ctor_m29708_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29708_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29709_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29709_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4838_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29709_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29710_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29710_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4838_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29710_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29711_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29711_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4838_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29711_GenericMethod/* genericMethod */

};
extern Il2CppType ExtensionAttribute_t779_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29712_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29712_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4838_il2cpp_TypeInfo/* declaring_type */
	, &ExtensionAttribute_t779_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29712_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4838_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29708_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29709_MethodInfo,
	&InternalEnumerator_1_Dispose_m29710_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29711_MethodInfo,
	&InternalEnumerator_1_get_Current_m29712_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29711_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29710_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4838_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29709_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29711_MethodInfo,
	&InternalEnumerator_1_Dispose_m29710_MethodInfo,
	&InternalEnumerator_1_get_Current_m29712_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4838_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6704_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4838_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6704_il2cpp_TypeInfo, 7},
};
extern TypeInfo ExtensionAttribute_t779_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4838_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29712_MethodInfo/* Method Usage */,
	&ExtensionAttribute_t779_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisExtensionAttribute_t779_m38132_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4838_0_0_0;
extern Il2CppType InternalEnumerator_1_t4838_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4838_GenericClass;
TypeInfo InternalEnumerator_1_t4838_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4838_MethodInfos/* methods */
	, InternalEnumerator_1_t4838_PropertyInfos/* properties */
	, InternalEnumerator_1_t4838_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4838_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4838_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4838_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4838_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4838_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4838_1_0_0/* this_arg */
	, InternalEnumerator_1_t4838_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4838_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4838_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4838)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8491_il2cpp_TypeInfo;

#include "System.Core_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern MethodInfo ICollection_1_get_Count_m48292_MethodInfo;
static PropertyInfo ICollection_1_t8491____Count_PropertyInfo = 
{
	&ICollection_1_t8491_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48292_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48293_MethodInfo;
static PropertyInfo ICollection_1_t8491____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8491_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48293_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8491_PropertyInfos[] =
{
	&ICollection_1_t8491____Count_PropertyInfo,
	&ICollection_1_t8491____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48292_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m48292_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8491_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48292_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48293_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48293_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8491_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48293_GenericMethod/* genericMethod */

};
extern Il2CppType ExtensionAttribute_t779_0_0_0;
extern Il2CppType ExtensionAttribute_t779_0_0_0;
static ParameterInfo ICollection_1_t8491_ICollection_1_Add_m48294_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExtensionAttribute_t779_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48294_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Add(T)
MethodInfo ICollection_1_Add_m48294_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8491_ICollection_1_Add_m48294_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48294_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48295_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Clear()
MethodInfo ICollection_1_Clear_m48295_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48295_GenericMethod/* genericMethod */

};
extern Il2CppType ExtensionAttribute_t779_0_0_0;
static ParameterInfo ICollection_1_t8491_ICollection_1_Contains_m48296_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExtensionAttribute_t779_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48296_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m48296_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8491_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8491_ICollection_1_Contains_m48296_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48296_GenericMethod/* genericMethod */

};
extern Il2CppType ExtensionAttributeU5BU5D_t5647_0_0_0;
extern Il2CppType ExtensionAttributeU5BU5D_t5647_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8491_ICollection_1_CopyTo_m48297_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ExtensionAttributeU5BU5D_t5647_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48297_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48297_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8491_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8491_ICollection_1_CopyTo_m48297_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48297_GenericMethod/* genericMethod */

};
extern Il2CppType ExtensionAttribute_t779_0_0_0;
static ParameterInfo ICollection_1_t8491_ICollection_1_Remove_m48298_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExtensionAttribute_t779_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48298_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Runtime.CompilerServices.ExtensionAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m48298_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8491_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8491_ICollection_1_Remove_m48298_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48298_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8491_MethodInfos[] =
{
	&ICollection_1_get_Count_m48292_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48293_MethodInfo,
	&ICollection_1_Add_m48294_MethodInfo,
	&ICollection_1_Clear_m48295_MethodInfo,
	&ICollection_1_Contains_m48296_MethodInfo,
	&ICollection_1_CopyTo_m48297_MethodInfo,
	&ICollection_1_Remove_m48298_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8493_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8491_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8493_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8491_0_0_0;
extern Il2CppType ICollection_1_t8491_1_0_0;
struct ICollection_1_t8491;
extern Il2CppGenericClass ICollection_1_t8491_GenericClass;
TypeInfo ICollection_1_t8491_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8491_MethodInfos/* methods */
	, ICollection_1_t8491_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8491_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8491_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8491_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8491_0_0_0/* byval_arg */
	, &ICollection_1_t8491_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8491_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.ExtensionAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern Il2CppType IEnumerator_1_t6704_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48299_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Runtime.CompilerServices.ExtensionAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48299_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8493_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6704_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48299_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8493_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48299_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8493_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8493_0_0_0;
extern Il2CppType IEnumerable_1_t8493_1_0_0;
struct IEnumerable_1_t8493;
extern Il2CppGenericClass IEnumerable_1_t8493_GenericClass;
TypeInfo IEnumerable_1_t8493_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8493_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8493_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8493_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8493_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8493_0_0_0/* byval_arg */
	, &IEnumerable_1_t8493_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8493_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8492_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>
extern MethodInfo IList_1_get_Item_m48300_MethodInfo;
extern MethodInfo IList_1_set_Item_m48301_MethodInfo;
static PropertyInfo IList_1_t8492____Item_PropertyInfo = 
{
	&IList_1_t8492_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48300_MethodInfo/* get */
	, &IList_1_set_Item_m48301_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8492_PropertyInfos[] =
{
	&IList_1_t8492____Item_PropertyInfo,
	NULL
};
extern Il2CppType ExtensionAttribute_t779_0_0_0;
static ParameterInfo IList_1_t8492_IList_1_IndexOf_m48302_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ExtensionAttribute_t779_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48302_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48302_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8492_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8492_IList_1_IndexOf_m48302_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48302_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ExtensionAttribute_t779_0_0_0;
static ParameterInfo IList_1_t8492_IList_1_Insert_m48303_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ExtensionAttribute_t779_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48303_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48303_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8492_IList_1_Insert_m48303_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48303_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8492_IList_1_RemoveAt_m48304_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48304_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48304_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8492_IList_1_RemoveAt_m48304_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48304_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8492_IList_1_get_Item_m48300_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ExtensionAttribute_t779_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48300_GenericMethod;
// T System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48300_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8492_il2cpp_TypeInfo/* declaring_type */
	, &ExtensionAttribute_t779_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8492_IList_1_get_Item_m48300_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48300_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ExtensionAttribute_t779_0_0_0;
static ParameterInfo IList_1_t8492_IList_1_set_Item_m48301_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ExtensionAttribute_t779_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48301_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Runtime.CompilerServices.ExtensionAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48301_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8492_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8492_IList_1_set_Item_m48301_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48301_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8492_MethodInfos[] =
{
	&IList_1_IndexOf_m48302_MethodInfo,
	&IList_1_Insert_m48303_MethodInfo,
	&IList_1_RemoveAt_m48304_MethodInfo,
	&IList_1_get_Item_m48300_MethodInfo,
	&IList_1_set_Item_m48301_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8492_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8491_il2cpp_TypeInfo,
	&IEnumerable_1_t8493_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8492_0_0_0;
extern Il2CppType IList_1_t8492_1_0_0;
struct IList_1_t8492;
extern Il2CppGenericClass IList_1_t8492_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8492_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8492_MethodInfos/* methods */
	, IList_1_t8492_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8492_il2cpp_TypeInfo/* element_class */
	, IList_1_t8492_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8492_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8492_0_0_0/* byval_arg */
	, &IList_1_t8492_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8492_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6706_il2cpp_TypeInfo;

// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>
extern MethodInfo IEnumerator_1_get_Current_m48305_MethodInfo;
static PropertyInfo IEnumerator_1_t6706____Current_PropertyInfo = 
{
	&IEnumerator_1_t6706_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48305_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6706_PropertyInfos[] =
{
	&IEnumerator_1_t6706____Current_PropertyInfo,
	NULL
};
extern Il2CppType MonoTODOAttribute_t1218_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48305_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48305_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6706_il2cpp_TypeInfo/* declaring_type */
	, &MonoTODOAttribute_t1218_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48305_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6706_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48305_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6706_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6706_0_0_0;
extern Il2CppType IEnumerator_1_t6706_1_0_0;
struct IEnumerator_1_t6706;
extern Il2CppGenericClass IEnumerator_1_t6706_GenericClass;
TypeInfo IEnumerator_1_t6706_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6706_MethodInfos/* methods */
	, IEnumerator_1_t6706_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6706_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6706_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6706_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6706_0_0_0/* byval_arg */
	, &IEnumerator_1_t6706_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6706_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_482.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4839_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_482MethodDeclarations.h"

extern TypeInfo MonoTODOAttribute_t1218_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29717_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMonoTODOAttribute_t1218_m38143_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.MonoTODOAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.MonoTODOAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisMonoTODOAttribute_t1218_m38143(__this, p0, method) (MonoTODOAttribute_t1218 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4839____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4839_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4839, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4839____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4839_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4839, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4839_FieldInfos[] =
{
	&InternalEnumerator_1_t4839____array_0_FieldInfo,
	&InternalEnumerator_1_t4839____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29714_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4839____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4839_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29714_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4839____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4839_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29717_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4839_PropertyInfos[] =
{
	&InternalEnumerator_1_t4839____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4839____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4839_InternalEnumerator_1__ctor_m29713_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29713_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29713_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4839_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4839_InternalEnumerator_1__ctor_m29713_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29713_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29714_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29714_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4839_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29714_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29715_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29715_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4839_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29715_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29716_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29716_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4839_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29716_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1218_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29717_GenericMethod;
// T System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29717_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4839_il2cpp_TypeInfo/* declaring_type */
	, &MonoTODOAttribute_t1218_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29717_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4839_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29713_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29714_MethodInfo,
	&InternalEnumerator_1_Dispose_m29715_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29716_MethodInfo,
	&InternalEnumerator_1_get_Current_m29717_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29716_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29715_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4839_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29714_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29716_MethodInfo,
	&InternalEnumerator_1_Dispose_m29715_MethodInfo,
	&InternalEnumerator_1_get_Current_m29717_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4839_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6706_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4839_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6706_il2cpp_TypeInfo, 7},
};
extern TypeInfo MonoTODOAttribute_t1218_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4839_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29717_MethodInfo/* Method Usage */,
	&MonoTODOAttribute_t1218_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMonoTODOAttribute_t1218_m38143_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4839_0_0_0;
extern Il2CppType InternalEnumerator_1_t4839_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4839_GenericClass;
TypeInfo InternalEnumerator_1_t4839_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4839_MethodInfos/* methods */
	, InternalEnumerator_1_t4839_PropertyInfos/* properties */
	, InternalEnumerator_1_t4839_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4839_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4839_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4839_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4839_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4839_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4839_1_0_0/* this_arg */
	, InternalEnumerator_1_t4839_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4839_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4839_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4839)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8494_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>
extern MethodInfo ICollection_1_get_Count_m48306_MethodInfo;
static PropertyInfo ICollection_1_t8494____Count_PropertyInfo = 
{
	&ICollection_1_t8494_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48306_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48307_MethodInfo;
static PropertyInfo ICollection_1_t8494____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8494_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48307_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8494_PropertyInfos[] =
{
	&ICollection_1_t8494____Count_PropertyInfo,
	&ICollection_1_t8494____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48306_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m48306_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8494_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48306_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48307_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48307_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8494_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48307_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1218_0_0_0;
extern Il2CppType MonoTODOAttribute_t1218_0_0_0;
static ParameterInfo ICollection_1_t8494_ICollection_1_Add_m48308_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1218_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48308_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Add(T)
MethodInfo ICollection_1_Add_m48308_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8494_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8494_ICollection_1_Add_m48308_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48308_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48309_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Clear()
MethodInfo ICollection_1_Clear_m48309_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8494_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48309_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1218_0_0_0;
static ParameterInfo ICollection_1_t8494_ICollection_1_Contains_m48310_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1218_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48310_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m48310_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8494_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8494_ICollection_1_Contains_m48310_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48310_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttributeU5BU5D_t5648_0_0_0;
extern Il2CppType MonoTODOAttributeU5BU5D_t5648_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8494_ICollection_1_CopyTo_m48311_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttributeU5BU5D_t5648_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48311_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48311_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8494_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8494_ICollection_1_CopyTo_m48311_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48311_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1218_0_0_0;
static ParameterInfo ICollection_1_t8494_ICollection_1_Remove_m48312_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1218_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48312_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m48312_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8494_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8494_ICollection_1_Remove_m48312_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48312_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8494_MethodInfos[] =
{
	&ICollection_1_get_Count_m48306_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48307_MethodInfo,
	&ICollection_1_Add_m48308_MethodInfo,
	&ICollection_1_Clear_m48309_MethodInfo,
	&ICollection_1_Contains_m48310_MethodInfo,
	&ICollection_1_CopyTo_m48311_MethodInfo,
	&ICollection_1_Remove_m48312_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8496_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8494_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8496_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8494_0_0_0;
extern Il2CppType ICollection_1_t8494_1_0_0;
struct ICollection_1_t8494;
extern Il2CppGenericClass ICollection_1_t8494_GenericClass;
TypeInfo ICollection_1_t8494_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8494_MethodInfos/* methods */
	, ICollection_1_t8494_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8494_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8494_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8494_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8494_0_0_0/* byval_arg */
	, &ICollection_1_t8494_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8494_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>
extern Il2CppType IEnumerator_1_t6706_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48313_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48313_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8496_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6706_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48313_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8496_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48313_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8496_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8496_0_0_0;
extern Il2CppType IEnumerable_1_t8496_1_0_0;
struct IEnumerable_1_t8496;
extern Il2CppGenericClass IEnumerable_1_t8496_GenericClass;
TypeInfo IEnumerable_1_t8496_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8496_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8496_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8496_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8496_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8496_0_0_0/* byval_arg */
	, &IEnumerable_1_t8496_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8496_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8495_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.MonoTODOAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.MonoTODOAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.MonoTODOAttribute>
extern MethodInfo IList_1_get_Item_m48314_MethodInfo;
extern MethodInfo IList_1_set_Item_m48315_MethodInfo;
static PropertyInfo IList_1_t8495____Item_PropertyInfo = 
{
	&IList_1_t8495_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48314_MethodInfo/* get */
	, &IList_1_set_Item_m48315_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8495_PropertyInfos[] =
{
	&IList_1_t8495____Item_PropertyInfo,
	NULL
};
extern Il2CppType MonoTODOAttribute_t1218_0_0_0;
static ParameterInfo IList_1_t8495_IList_1_IndexOf_m48316_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1218_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48316_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.MonoTODOAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48316_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8495_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8495_IList_1_IndexOf_m48316_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48316_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MonoTODOAttribute_t1218_0_0_0;
static ParameterInfo IList_1_t8495_IList_1_Insert_m48317_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1218_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48317_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48317_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8495_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8495_IList_1_Insert_m48317_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48317_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8495_IList_1_RemoveAt_m48318_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48318_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48318_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8495_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8495_IList_1_RemoveAt_m48318_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48318_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8495_IList_1_get_Item_m48314_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType MonoTODOAttribute_t1218_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48314_GenericMethod;
// T System.Collections.Generic.IList`1<System.MonoTODOAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48314_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8495_il2cpp_TypeInfo/* declaring_type */
	, &MonoTODOAttribute_t1218_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8495_IList_1_get_Item_m48314_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48314_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MonoTODOAttribute_t1218_0_0_0;
static ParameterInfo IList_1_t8495_IList_1_set_Item_m48315_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1218_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48315_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48315_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8495_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8495_IList_1_set_Item_m48315_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48315_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8495_MethodInfos[] =
{
	&IList_1_IndexOf_m48316_MethodInfo,
	&IList_1_Insert_m48317_MethodInfo,
	&IList_1_RemoveAt_m48318_MethodInfo,
	&IList_1_get_Item_m48314_MethodInfo,
	&IList_1_set_Item_m48315_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8495_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8494_il2cpp_TypeInfo,
	&IEnumerable_1_t8496_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8495_0_0_0;
extern Il2CppType IList_1_t8495_1_0_0;
struct IList_1_t8495;
extern Il2CppGenericClass IList_1_t8495_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8495_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8495_MethodInfos/* methods */
	, IList_1_t8495_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8495_il2cpp_TypeInfo/* element_class */
	, IList_1_t8495_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8495_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8495_0_0_0/* byval_arg */
	, &IList_1_t8495_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8495_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Func`2<System.Object,System.Object>
#include "System_Core_System_Func_2_gen_2.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Func_2_t4840_il2cpp_TypeInfo;
// System.Func`2<System.Object,System.Object>
#include "System_Core_System_Func_2_gen_2MethodDeclarations.h"



// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern MethodInfo Func_2__ctor_m29718_MethodInfo;
 void Func_2__ctor_m29718_gshared (Func_2_t4840 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
extern MethodInfo Func_2_Invoke_m29719_MethodInfo;
 Object_t * Func_2_Invoke_m29719_gshared (Func_2_t4840 * __this, Object_t * ___arg1, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Func_2_Invoke_m29719((Func_2_t4840 *)__this->___prev_9,___arg1, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___arg1, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, Object_t * ___arg1, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___arg1,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern MethodInfo Func_2_BeginInvoke_m29720_MethodInfo;
 Object_t * Func_2_BeginInvoke_m29720_gshared (Func_2_t4840 * __this, Object_t * ___arg1, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg1;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// TResult System.Func`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern MethodInfo Func_2_EndInvoke_m29721_MethodInfo;
 Object_t * Func_2_EndInvoke_m29721_gshared (Func_2_t4840 * __this, Object_t * ___result, MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// Metadata Definition System.Func`2<System.Object,System.Object>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Func_2_t4840_Func_2__ctor_m29718_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Func_2__ctor_m29718_GenericMethod;
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
MethodInfo Func_2__ctor_m29718_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Func_2__ctor_m29718_gshared/* method */
	, &Func_2_t4840_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, Func_2_t4840_Func_2__ctor_m29718_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Func_2__ctor_m29718_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Func_2_t4840_Func_2_Invoke_m29719_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Func_2_Invoke_m29719_GenericMethod;
// TResult System.Func`2<System.Object,System.Object>::Invoke(T)
MethodInfo Func_2_Invoke_m29719_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Func_2_Invoke_m29719_gshared/* method */
	, &Func_2_t4840_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Func_2_t4840_Func_2_Invoke_m29719_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Func_2_Invoke_m29719_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Func_2_t4840_Func_2_BeginInvoke_m29720_ParameterInfos[] = 
{
	{"arg1", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Func_2_BeginInvoke_m29720_GenericMethod;
// System.IAsyncResult System.Func`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Func_2_BeginInvoke_m29720_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Func_2_BeginInvoke_m29720_gshared/* method */
	, &Func_2_t4840_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Func_2_t4840_Func_2_BeginInvoke_m29720_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Func_2_BeginInvoke_m29720_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo Func_2_t4840_Func_2_EndInvoke_m29721_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Func_2_EndInvoke_m29721_GenericMethod;
// TResult System.Func`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
MethodInfo Func_2_EndInvoke_m29721_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Func_2_EndInvoke_m29721_gshared/* method */
	, &Func_2_t4840_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, Func_2_t4840_Func_2_EndInvoke_m29721_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Func_2_EndInvoke_m29721_GenericMethod/* genericMethod */

};
static MethodInfo* Func_2_t4840_MethodInfos[] =
{
	&Func_2__ctor_m29718_MethodInfo,
	&Func_2_Invoke_m29719_MethodInfo,
	&Func_2_BeginInvoke_m29720_MethodInfo,
	&Func_2_EndInvoke_m29721_MethodInfo,
	NULL
};
static MethodInfo* Func_2_t4840_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&Func_2_Invoke_m29719_MethodInfo,
	&Func_2_BeginInvoke_m29720_MethodInfo,
	&Func_2_EndInvoke_m29721_MethodInfo,
};
static Il2CppInterfaceOffsetPair Func_2_t4840_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_System_Core_dll_Image;
extern Il2CppType Func_2_t4840_0_0_0;
extern Il2CppType Func_2_t4840_1_0_0;
struct Func_2_t4840;
extern Il2CppGenericClass Func_2_t4840_GenericClass;
TypeInfo Func_2_t4840_il2cpp_TypeInfo = 
{
	&g_System_Core_dll_Image/* image */
	, NULL/* gc_desc */
	, "Func`2"/* name */
	, "System"/* namespaze */
	, Func_2_t4840_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Func_2_t4840_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Func_2_t4840_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Func_2_t4840_il2cpp_TypeInfo/* cast_class */
	, &Func_2_t4840_0_0_0/* byval_arg */
	, &Func_2_t4840_1_0_0/* this_arg */
	, Func_2_t4840_InterfacesOffsets/* interface_offsets */
	, &Func_2_t4840_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Func_2_t4840)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6708_il2cpp_TypeInfo;

// System.MonoTODOAttribute
#include "System_System_MonoTODOAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>
extern MethodInfo IEnumerator_1_get_Current_m48319_MethodInfo;
static PropertyInfo IEnumerator_1_t6708____Current_PropertyInfo = 
{
	&IEnumerator_1_t6708_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48319_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6708_PropertyInfos[] =
{
	&IEnumerator_1_t6708____Current_PropertyInfo,
	NULL
};
extern Il2CppType MonoTODOAttribute_t1289_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48319_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.MonoTODOAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48319_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6708_il2cpp_TypeInfo/* declaring_type */
	, &MonoTODOAttribute_t1289_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48319_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6708_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48319_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6708_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6708_0_0_0;
extern Il2CppType IEnumerator_1_t6708_1_0_0;
struct IEnumerator_1_t6708;
extern Il2CppGenericClass IEnumerator_1_t6708_GenericClass;
TypeInfo IEnumerator_1_t6708_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6708_MethodInfos/* methods */
	, IEnumerator_1_t6708_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6708_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6708_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6708_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6708_0_0_0/* byval_arg */
	, &IEnumerator_1_t6708_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6708_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_483.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4841_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_483MethodDeclarations.h"

extern TypeInfo MonoTODOAttribute_t1289_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29726_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisMonoTODOAttribute_t1289_m38154_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.MonoTODOAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.MonoTODOAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisMonoTODOAttribute_t1289_m38154(__this, p0, method) (MonoTODOAttribute_t1289 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.MonoTODOAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4841____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4841_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4841, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4841____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4841_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4841, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4841_FieldInfos[] =
{
	&InternalEnumerator_1_t4841____array_0_FieldInfo,
	&InternalEnumerator_1_t4841____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29723_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4841____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4841_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29723_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4841____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4841_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29726_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4841_PropertyInfos[] =
{
	&InternalEnumerator_1_t4841____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4841____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4841_InternalEnumerator_1__ctor_m29722_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29722_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29722_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4841_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4841_InternalEnumerator_1__ctor_m29722_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29722_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29723_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29723_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4841_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29723_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29724_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29724_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4841_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29724_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29725_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29725_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4841_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29725_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1289_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29726_GenericMethod;
// T System.Array/InternalEnumerator`1<System.MonoTODOAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29726_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4841_il2cpp_TypeInfo/* declaring_type */
	, &MonoTODOAttribute_t1289_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29726_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4841_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29722_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29723_MethodInfo,
	&InternalEnumerator_1_Dispose_m29724_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29725_MethodInfo,
	&InternalEnumerator_1_get_Current_m29726_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29725_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29724_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4841_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29723_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29725_MethodInfo,
	&InternalEnumerator_1_Dispose_m29724_MethodInfo,
	&InternalEnumerator_1_get_Current_m29726_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4841_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6708_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4841_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6708_il2cpp_TypeInfo, 7},
};
extern TypeInfo MonoTODOAttribute_t1289_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4841_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29726_MethodInfo/* Method Usage */,
	&MonoTODOAttribute_t1289_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisMonoTODOAttribute_t1289_m38154_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4841_0_0_0;
extern Il2CppType InternalEnumerator_1_t4841_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4841_GenericClass;
TypeInfo InternalEnumerator_1_t4841_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4841_MethodInfos/* methods */
	, InternalEnumerator_1_t4841_PropertyInfos/* properties */
	, InternalEnumerator_1_t4841_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4841_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4841_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4841_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4841_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4841_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4841_1_0_0/* this_arg */
	, InternalEnumerator_1_t4841_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4841_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4841_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4841)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8497_il2cpp_TypeInfo;

#include "System_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>
extern MethodInfo ICollection_1_get_Count_m48320_MethodInfo;
static PropertyInfo ICollection_1_t8497____Count_PropertyInfo = 
{
	&ICollection_1_t8497_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48320_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48321_MethodInfo;
static PropertyInfo ICollection_1_t8497____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8497_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48321_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8497_PropertyInfos[] =
{
	&ICollection_1_t8497____Count_PropertyInfo,
	&ICollection_1_t8497____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48320_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m48320_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8497_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48320_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48321_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48321_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8497_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48321_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1289_0_0_0;
extern Il2CppType MonoTODOAttribute_t1289_0_0_0;
static ParameterInfo ICollection_1_t8497_ICollection_1_Add_m48322_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1289_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48322_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Add(T)
MethodInfo ICollection_1_Add_m48322_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8497_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8497_ICollection_1_Add_m48322_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48322_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48323_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Clear()
MethodInfo ICollection_1_Clear_m48323_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8497_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48323_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1289_0_0_0;
static ParameterInfo ICollection_1_t8497_ICollection_1_Contains_m48324_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1289_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48324_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m48324_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8497_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8497_ICollection_1_Contains_m48324_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48324_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttributeU5BU5D_t5649_0_0_0;
extern Il2CppType MonoTODOAttributeU5BU5D_t5649_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8497_ICollection_1_CopyTo_m48325_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttributeU5BU5D_t5649_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48325_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48325_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8497_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8497_ICollection_1_CopyTo_m48325_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48325_GenericMethod/* genericMethod */

};
extern Il2CppType MonoTODOAttribute_t1289_0_0_0;
static ParameterInfo ICollection_1_t8497_ICollection_1_Remove_m48326_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1289_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48326_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.MonoTODOAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m48326_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8497_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8497_ICollection_1_Remove_m48326_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48326_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8497_MethodInfos[] =
{
	&ICollection_1_get_Count_m48320_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48321_MethodInfo,
	&ICollection_1_Add_m48322_MethodInfo,
	&ICollection_1_Clear_m48323_MethodInfo,
	&ICollection_1_Contains_m48324_MethodInfo,
	&ICollection_1_CopyTo_m48325_MethodInfo,
	&ICollection_1_Remove_m48326_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8499_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8497_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8499_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8497_0_0_0;
extern Il2CppType ICollection_1_t8497_1_0_0;
struct ICollection_1_t8497;
extern Il2CppGenericClass ICollection_1_t8497_GenericClass;
TypeInfo ICollection_1_t8497_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8497_MethodInfos/* methods */
	, ICollection_1_t8497_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8497_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8497_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8497_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8497_0_0_0/* byval_arg */
	, &ICollection_1_t8497_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8497_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>
extern Il2CppType IEnumerator_1_t6708_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48327_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.MonoTODOAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48327_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8499_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6708_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48327_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8499_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48327_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8499_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8499_0_0_0;
extern Il2CppType IEnumerable_1_t8499_1_0_0;
struct IEnumerable_1_t8499;
extern Il2CppGenericClass IEnumerable_1_t8499_GenericClass;
TypeInfo IEnumerable_1_t8499_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8499_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8499_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8499_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8499_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8499_0_0_0/* byval_arg */
	, &IEnumerable_1_t8499_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8499_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8498_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.MonoTODOAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.MonoTODOAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.MonoTODOAttribute>
extern MethodInfo IList_1_get_Item_m48328_MethodInfo;
extern MethodInfo IList_1_set_Item_m48329_MethodInfo;
static PropertyInfo IList_1_t8498____Item_PropertyInfo = 
{
	&IList_1_t8498_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48328_MethodInfo/* get */
	, &IList_1_set_Item_m48329_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8498_PropertyInfos[] =
{
	&IList_1_t8498____Item_PropertyInfo,
	NULL
};
extern Il2CppType MonoTODOAttribute_t1289_0_0_0;
static ParameterInfo IList_1_t8498_IList_1_IndexOf_m48330_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1289_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48330_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.MonoTODOAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48330_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8498_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8498_IList_1_IndexOf_m48330_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48330_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MonoTODOAttribute_t1289_0_0_0;
static ParameterInfo IList_1_t8498_IList_1_Insert_m48331_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1289_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48331_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48331_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8498_IList_1_Insert_m48331_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48331_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8498_IList_1_RemoveAt_m48332_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48332_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48332_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8498_IList_1_RemoveAt_m48332_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48332_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8498_IList_1_get_Item_m48328_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType MonoTODOAttribute_t1289_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48328_GenericMethod;
// T System.Collections.Generic.IList`1<System.MonoTODOAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48328_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8498_il2cpp_TypeInfo/* declaring_type */
	, &MonoTODOAttribute_t1289_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8498_IList_1_get_Item_m48328_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48328_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType MonoTODOAttribute_t1289_0_0_0;
static ParameterInfo IList_1_t8498_IList_1_set_Item_m48329_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &MonoTODOAttribute_t1289_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48329_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.MonoTODOAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48329_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8498_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8498_IList_1_set_Item_m48329_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48329_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8498_MethodInfos[] =
{
	&IList_1_IndexOf_m48330_MethodInfo,
	&IList_1_Insert_m48331_MethodInfo,
	&IList_1_RemoveAt_m48332_MethodInfo,
	&IList_1_get_Item_m48328_MethodInfo,
	&IList_1_set_Item_m48329_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8498_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8497_il2cpp_TypeInfo,
	&IEnumerable_1_t8499_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8498_0_0_0;
extern Il2CppType IList_1_t8498_1_0_0;
struct IList_1_t8498;
extern Il2CppGenericClass IList_1_t8498_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8498_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8498_MethodInfos/* methods */
	, IList_1_t8498_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8498_il2cpp_TypeInfo/* element_class */
	, IList_1_t8498_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8498_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8498_0_0_0/* byval_arg */
	, &IList_1_t8498_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8498_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Collections.Generic.LinkedList`1<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_gen_0.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo LinkedList_1_t4843_il2cpp_TypeInfo;
// System.Collections.Generic.LinkedList`1<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_gen_0MethodDeclarations.h"

// System.UInt32
#include "mscorlib_System_UInt32.h"
// System.Collections.Generic.LinkedListNode`1<System.Object>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_0.h"
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullException.h"
extern TypeInfo Int32_t93_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t4844_il2cpp_TypeInfo;
extern TypeInfo ArgumentNullException_t1171_il2cpp_TypeInfo;
extern TypeInfo LinkedListNode_1_t4842_il2cpp_TypeInfo;
extern TypeInfo Boolean_t106_il2cpp_TypeInfo;
extern TypeInfo ArgumentOutOfRangeException_t1494_il2cpp_TypeInfo;
// System.Object
#include "mscorlib_System_ObjectMethodDeclarations.h"
// System.ArgumentNullException
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
// System.Collections.Generic.LinkedListNode`1<System.Object>
#include "System_System_Collections_Generic_LinkedListNode_1_gen_0MethodDeclarations.h"
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
// System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_LinkedList_1_Enumerator_ge_0MethodDeclarations.h"
// System.Runtime.Serialization.SerializationInfo
#include "mscorlib_System_Runtime_Serialization_SerializationInfoMethodDeclarations.h"
extern MethodInfo Object__ctor_m271_MethodInfo;
extern MethodInfo LinkedList_1__ctor_m29727_MethodInfo;
extern MethodInfo LinkedList_1_AddLast_m29737_MethodInfo;
extern MethodInfo LinkedList_1_CopyTo_m29740_MethodInfo;
extern MethodInfo LinkedList_1_GetEnumerator_m29742_MethodInfo;
extern MethodInfo ArgumentNullException__ctor_m6533_MethodInfo;
extern MethodInfo LinkedListNode_1_get_List_m29752_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7659_MethodInfo;
extern MethodInfo LinkedListNode_1__ctor_m29749_MethodInfo;
extern MethodInfo LinkedListNode_1__ctor_m29750_MethodInfo;
extern MethodInfo LinkedListNode_1_get_Value_m29754_MethodInfo;
extern MethodInfo Array_GetLowerBound_m9585_MethodInfo;
extern MethodInfo ArgumentOutOfRangeException__ctor_m7660_MethodInfo;
extern MethodInfo Array_get_Rank_m7662_MethodInfo;
extern MethodInfo ArgumentException__ctor_m7658_MethodInfo;
extern MethodInfo Enumerator__ctor_m29755_MethodInfo;
extern MethodInfo SerializationInfo_AddValue_m7667_MethodInfo;
extern MethodInfo SerializationInfo_AddValue_m11825_MethodInfo;
extern MethodInfo SerializationInfo_GetValue_m7670_MethodInfo;
extern MethodInfo SerializationInfo_GetUInt32_m11828_MethodInfo;
extern MethodInfo LinkedList_1_Find_m29741_MethodInfo;
extern MethodInfo LinkedList_1_Remove_m29746_MethodInfo;
extern MethodInfo LinkedList_1_VerifyReferencedNode_m29736_MethodInfo;
extern MethodInfo LinkedListNode_1_Detach_m29751_MethodInfo;


// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
 void LinkedList_1__ctor_m29727_gshared (LinkedList_1_t4843 * __this, MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		Object_t * L_0 = (Object_t *)il2cpp_codegen_object_new (InitializedTypeInfo(&Object_t_il2cpp_TypeInfo));
		Object__ctor_m271(L_0, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		__this->___syncRoot_2 = L_0;
		__this->___first_3 = (LinkedListNode_1_t4842 *)NULL;
		int32_t L_1 = 0;
		V_0 = L_1;
		__this->___version_1 = L_1;
		__this->___count_0 = V_0;
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern MethodInfo LinkedList_1__ctor_m29728_MethodInfo;
 void LinkedList_1__ctor_m29728_gshared (LinkedList_1_t4843 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___context, MethodInfo* method)
{
	{
		(( void (*) (LinkedList_1_t4843 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->___si_4 = ___info;
		Object_t * L_0 = (Object_t *)il2cpp_codegen_object_new (InitializedTypeInfo(&Object_t_il2cpp_TypeInfo));
		Object__ctor_m271(L_0, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		__this->___syncRoot_2 = L_0;
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern MethodInfo LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29729_MethodInfo;
 void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29729_gshared (LinkedList_1_t4843 * __this, Object_t * ___value, MethodInfo* method)
{
	{
		(( LinkedListNode_1_t4842 * (*) (LinkedList_1_t4843 * __this, Object_t * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(__this, ___value, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern MethodInfo LinkedList_1_System_Collections_ICollection_CopyTo_m29730_MethodInfo;
 void LinkedList_1_System_Collections_ICollection_CopyTo_m29730_gshared (LinkedList_1_t4843 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method)
{
	ObjectU5BU5D_t115* V_0 = {0};
	{
		V_0 = ((ObjectU5BU5D_t115*)IsInst(___array, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		if (V_0)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t507 * L_0 = (ArgumentException_t507 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t507_il2cpp_TypeInfo));
		ArgumentException__ctor_m2468(L_0, (String_t*) &_stringLiteral442, /*hidden argument*/&ArgumentException__ctor_m2468_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0018:
	{
		VirtActionInvoker2< ObjectU5BU5D_t115*, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), __this, V_0, ___index);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern MethodInfo LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29731_MethodInfo;
 Object_t* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29731_gshared (LinkedList_1_t4843 * __this, MethodInfo* method)
{
	{
		Enumerator_t4844  L_0 = (( Enumerator_t4844  (*) (LinkedList_1_t4843 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Enumerator_t4844  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return (Object_t*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern MethodInfo LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29732_MethodInfo;
 Object_t * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29732_gshared (LinkedList_1_t4843 * __this, MethodInfo* method)
{
	{
		Enumerator_t4844  L_0 = (( Enumerator_t4844  (*) (LinkedList_1_t4843 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)(__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		Enumerator_t4844  L_1 = L_0;
		Object_t * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_1);
		return (Object_t *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern MethodInfo LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29733_MethodInfo;
 bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29733_gshared (LinkedList_1_t4843 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern MethodInfo LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29734_MethodInfo;
 bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29734_gshared (LinkedList_1_t4843 * __this, MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern MethodInfo LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29735_MethodInfo;
 Object_t * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29735_gshared (LinkedList_1_t4843 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___syncRoot_2);
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
 void LinkedList_1_VerifyReferencedNode_m29736_gshared (LinkedList_1_t4843 * __this, LinkedListNode_1_t4842 * ___node, MethodInfo* method)
{
	{
		if (___node)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1171 * L_0 = (ArgumentNullException_t1171 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1171_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6533(L_0, (String_t*) &_stringLiteral450, /*hidden argument*/&ArgumentNullException__ctor_m6533_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0011:
	{
		NullCheck(___node);
		LinkedList_1_t4843 * L_1 = (( LinkedList_1_t4843 * (*) (LinkedListNode_1_t4842 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(___node, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		if ((((LinkedList_1_t4843 *)L_1) == ((LinkedList_1_t4843 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1493 * L_2 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7659(L_2, /*hidden argument*/&InvalidOperationException__ctor_m7659_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
 LinkedListNode_1_t4842 * LinkedList_1_AddLast_m29737_gshared (LinkedList_1_t4843 * __this, Object_t * ___value, MethodInfo* method)
{
	LinkedListNode_1_t4842 * V_0 = {0};
	{
		LinkedListNode_1_t4842 * L_0 = (__this->___first_3);
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		LinkedListNode_1_t4842 * L_1 = (LinkedListNode_1_t4842 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (LinkedListNode_1_t4842 * __this, LinkedList_1_t4843 * p0, Object_t * p1, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8)->method)(L_1, __this, ___value, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 8));
		V_0 = L_1;
		__this->___first_3 = V_0;
		goto IL_0038;
	}

IL_001f:
	{
		LinkedListNode_1_t4842 * L_2 = (__this->___first_3);
		NullCheck(L_2);
		LinkedListNode_1_t4842 * L_3 = (L_2->___back_3);
		LinkedListNode_1_t4842 * L_4 = (__this->___first_3);
		LinkedListNode_1_t4842 * L_5 = (LinkedListNode_1_t4842 *)il2cpp_codegen_object_new (IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		(( void (*) (LinkedListNode_1_t4842 * __this, LinkedList_1_t4843 * p0, Object_t * p1, LinkedListNode_1_t4842 * p2, LinkedListNode_1_t4842 * p3, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)(L_5, __this, ___value, L_3, L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		V_0 = L_5;
	}

IL_0038:
	{
		uint32_t L_6 = (__this->___count_0);
		__this->___count_0 = ((int32_t)(L_6+1));
		uint32_t L_7 = (__this->___version_1);
		__this->___version_1 = ((int32_t)(L_7+1));
		return V_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern MethodInfo LinkedList_1_Clear_m29738_MethodInfo;
 void LinkedList_1_Clear_m29738_gshared (LinkedList_1_t4843 * __this, MethodInfo* method)
{
	{
		__this->___count_0 = 0;
		__this->___first_3 = (LinkedListNode_1_t4842 *)NULL;
		uint32_t L_0 = (__this->___version_1);
		__this->___version_1 = ((int32_t)(L_0+1));
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern MethodInfo LinkedList_1_Contains_m29739_MethodInfo;
 bool LinkedList_1_Contains_m29739_gshared (LinkedList_1_t4843 * __this, Object_t * ___value, MethodInfo* method)
{
	LinkedListNode_1_t4842 * V_0 = {0};
	{
		LinkedListNode_1_t4842 * L_0 = (__this->___first_3);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}

IL_000f:
	{
		NullCheck(V_0);
		Object_t * L_1 = (( Object_t * (*) (LinkedListNode_1_t4842 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(V_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Object_t * L_2 = L_1;
		NullCheck((*(&___value)));
		bool L_3 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m304_MethodInfo, (*(&___value)), ((Object_t *)L_2));
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		return 1;
	}

IL_002e:
	{
		NullCheck(V_0);
		LinkedListNode_1_t4842 * L_4 = (V_0->___forward_2);
		V_0 = L_4;
		LinkedListNode_1_t4842 * L_5 = (__this->___first_3);
		if ((((LinkedListNode_1_t4842 *)V_0) != ((LinkedListNode_1_t4842 *)L_5)))
		{
			goto IL_000f;
		}
	}
	{
		return 0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
 void LinkedList_1_CopyTo_m29740_gshared (LinkedList_1_t4843 * __this, ObjectU5BU5D_t115* ___array, int32_t ___index, MethodInfo* method)
{
	LinkedListNode_1_t4842 * V_0 = {0};
	{
		if (___array)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1171 * L_0 = (ArgumentNullException_t1171 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentNullException_t1171_il2cpp_TypeInfo));
		ArgumentNullException__ctor_m6533(L_0, (String_t*) &_stringLiteral442, /*hidden argument*/&ArgumentNullException__ctor_m6533_MethodInfo);
		il2cpp_codegen_raise_exception(L_0);
	}

IL_0011:
	{
		NullCheck(___array);
		int32_t L_1 = Array_GetLowerBound_m9585(___array, 0, /*hidden argument*/&Array_GetLowerBound_m9585_MethodInfo);
		if ((((uint32_t)___index) >= ((uint32_t)L_1)))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t1494 * L_2 = (ArgumentOutOfRangeException_t1494 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentOutOfRangeException_t1494_il2cpp_TypeInfo));
		ArgumentOutOfRangeException__ctor_m7660(L_2, (String_t*) &_stringLiteral443, /*hidden argument*/&ArgumentOutOfRangeException__ctor_m7660_MethodInfo);
		il2cpp_codegen_raise_exception(L_2);
	}

IL_0029:
	{
		NullCheck(___array);
		int32_t L_3 = Array_get_Rank_m7662(___array, /*hidden argument*/&Array_get_Rank_m7662_MethodInfo);
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t507 * L_4 = (ArgumentException_t507 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t507_il2cpp_TypeInfo));
		ArgumentException__ctor_m7658(L_4, (String_t*) &_stringLiteral442, (String_t*) &_stringLiteral451, /*hidden argument*/&ArgumentException__ctor_m7658_MethodInfo);
		il2cpp_codegen_raise_exception(L_4);
	}

IL_0045:
	{
		NullCheck(___array);
		NullCheck(___array);
		int32_t L_5 = Array_GetLowerBound_m9585(___array, 0, /*hidden argument*/&Array_GetLowerBound_m9585_MethodInfo);
		uint32_t L_6 = (__this->___count_0);
		if ((((int64_t)(((int64_t)((int32_t)(((int32_t)((((int32_t)(((Array_t *)___array)->max_length)))-___index))+L_5))))) >= ((int64_t)(((uint64_t)L_6)))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t507 * L_7 = (ArgumentException_t507 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ArgumentException_t507_il2cpp_TypeInfo));
		ArgumentException__ctor_m2468(L_7, (String_t*) &_stringLiteral452, /*hidden argument*/&ArgumentException__ctor_m2468_MethodInfo);
		il2cpp_codegen_raise_exception(L_7);
	}

IL_006a:
	{
		LinkedListNode_1_t4842 * L_8 = (__this->___first_3);
		V_0 = L_8;
		LinkedListNode_1_t4842 * L_9 = (__this->___first_3);
		if (L_9)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		NullCheck(V_0);
		Object_t * L_10 = (( Object_t * (*) (LinkedListNode_1_t4842 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(V_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		NullCheck(___array);
		IL2CPP_ARRAY_BOUNDS_CHECK(___array, ___index);
		*((Object_t **)(Object_t **)SZArrayLdElema(___array, ___index)) = (Object_t *)L_10;
		___index = ((int32_t)(___index+1));
		NullCheck(V_0);
		LinkedListNode_1_t4842 * L_11 = (V_0->___forward_2);
		V_0 = L_11;
		LinkedListNode_1_t4842 * L_12 = (__this->___first_3);
		if ((((LinkedListNode_1_t4842 *)V_0) != ((LinkedListNode_1_t4842 *)L_12)))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
 LinkedListNode_1_t4842 * LinkedList_1_Find_m29741_gshared (LinkedList_1_t4843 * __this, Object_t * ___value, MethodInfo* method)
{
	LinkedListNode_1_t4842 * V_0 = {0};
	{
		LinkedListNode_1_t4842 * L_0 = (__this->___first_3);
		V_0 = L_0;
		if (V_0)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t4842 *)NULL;
	}

IL_000f:
	{
		Object_t * L_1 = ___value;
		if (((Object_t *)L_1))
		{
			goto IL_002a;
		}
	}
	{
		NullCheck(V_0);
		Object_t * L_2 = (( Object_t * (*) (LinkedListNode_1_t4842 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(V_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Object_t * L_3 = L_2;
		if (!((Object_t *)L_3))
		{
			goto IL_0052;
		}
	}

IL_002a:
	{
		Object_t * L_4 = ___value;
		if (!((Object_t *)L_4))
		{
			goto IL_0054;
		}
	}
	{
		NullCheck(V_0);
		Object_t * L_5 = (( Object_t * (*) (LinkedListNode_1_t4842 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)(V_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		Object_t * L_6 = L_5;
		NullCheck((*(&___value)));
		bool L_7 = (bool)VirtFuncInvoker1< bool, Object_t * >::Invoke(&Object_Equals_m304_MethodInfo, (*(&___value)), ((Object_t *)L_6));
		if (!L_7)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		return V_0;
	}

IL_0054:
	{
		NullCheck(V_0);
		LinkedListNode_1_t4842 * L_8 = (V_0->___forward_2);
		V_0 = L_8;
		LinkedListNode_1_t4842 * L_9 = (__this->___first_3);
		if ((((LinkedListNode_1_t4842 *)V_0) != ((LinkedListNode_1_t4842 *)L_9)))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t4842 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
 Enumerator_t4844  LinkedList_1_GetEnumerator_m29742 (LinkedList_1_t4843 * __this, MethodInfo* method){
	{
		Enumerator_t4844  L_0 = {0};
		Enumerator__ctor_m29755(&L_0, __this, /*hidden argument*/&Enumerator__ctor_m29755_MethodInfo);
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern MethodInfo LinkedList_1_GetObjectData_m29743_MethodInfo;
 void LinkedList_1_GetObjectData_m29743_gshared (LinkedList_1_t4843 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___context, MethodInfo* method)
{
	ObjectU5BU5D_t115* V_0 = {0};
	{
		uint32_t L_0 = (__this->___count_0);
		V_0 = ((ObjectU5BU5D_t115*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 13), (((uintptr_t)L_0))));
		VirtActionInvoker2< ObjectU5BU5D_t115*, int32_t >::Invoke(IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), __this, V_0, 0);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_1 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(___info);
		SerializationInfo_AddValue_m7667(___info, (String_t*) &_stringLiteral453, (Object_t *)(Object_t *)V_0, L_1, /*hidden argument*/&SerializationInfo_AddValue_m7667_MethodInfo);
		uint32_t L_2 = (__this->___version_1);
		NullCheck(___info);
		SerializationInfo_AddValue_m11825(___info, (String_t*) &_stringLiteral454, L_2, /*hidden argument*/&SerializationInfo_AddValue_m11825_MethodInfo);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern MethodInfo LinkedList_1_OnDeserialization_m29744_MethodInfo;
 void LinkedList_1_OnDeserialization_m29744_gshared (LinkedList_1_t4843 * __this, Object_t * ___sender, MethodInfo* method)
{
	ObjectU5BU5D_t115* V_0 = {0};
	Object_t * V_1 = {0};
	ObjectU5BU5D_t115* V_2 = {0};
	int32_t V_3 = 0;
	{
		SerializationInfo_t1066 * L_0 = (__this->___si_4);
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t1066 * L_1 = (__this->___si_4);
		IL2CPP_RUNTIME_CLASS_INIT(InitializedTypeInfo(&Type_t_il2cpp_TypeInfo));
		Type_t * L_2 = Type_GetTypeFromHandle_m395(NULL /*static, unused*/, LoadTypeToken(IL2CPP_RGCTX_TYPE(InitializedTypeInfo(method->declaring_type)->rgctx_data, 14)), /*hidden argument*/&Type_GetTypeFromHandle_m395_MethodInfo);
		NullCheck(L_1);
		Object_t * L_3 = SerializationInfo_GetValue_m7670(L_1, (String_t*) &_stringLiteral453, L_2, /*hidden argument*/&SerializationInfo_GetValue_m7670_MethodInfo);
		V_0 = ((ObjectU5BU5D_t115*)Castclass(L_3, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)));
		if (!V_0)
		{
			goto IL_0057;
		}
	}
	{
		V_2 = V_0;
		V_3 = 0;
		goto IL_004e;
	}

IL_003a:
	{
		NullCheck(V_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(V_2, V_3);
		int32_t L_4 = V_3;
		V_1 = (*(Object_t **)(Object_t **)SZArrayLdElema(V_2, L_4));
		(( LinkedListNode_1_t4842 * (*) (LinkedList_1_t4843 * __this, Object_t * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(__this, V_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_3 = ((int32_t)(V_3+1));
	}

IL_004e:
	{
		NullCheck(V_2);
		if ((((int32_t)V_3) < ((int32_t)(((int32_t)(((Array_t *)V_2)->max_length))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t1066 * L_5 = (__this->___si_4);
		NullCheck(L_5);
		uint32_t L_6 = SerializationInfo_GetUInt32_m11828(L_5, (String_t*) &_stringLiteral454, /*hidden argument*/&SerializationInfo_GetUInt32_m11828_MethodInfo);
		__this->___version_1 = L_6;
		__this->___si_4 = (SerializationInfo_t1066 *)NULL;
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern MethodInfo LinkedList_1_Remove_m29745_MethodInfo;
 bool LinkedList_1_Remove_m29745_gshared (LinkedList_1_t4843 * __this, Object_t * ___value, MethodInfo* method)
{
	LinkedListNode_1_t4842 * V_0 = {0};
	{
		LinkedListNode_1_t4842 * L_0 = (( LinkedListNode_1_t4842 * (*) (LinkedList_1_t4843 * __this, Object_t * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15)->method)(__this, ___value, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 15));
		V_0 = L_0;
		if (V_0)
		{
			goto IL_0010;
		}
	}
	{
		return 0;
	}

IL_0010:
	{
		(( void (*) (LinkedList_1_t4843 * __this, LinkedListNode_1_t4842 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16)->method)(__this, V_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 16));
		return 1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
 void LinkedList_1_Remove_m29746_gshared (LinkedList_1_t4843 * __this, LinkedListNode_1_t4842 * ___node, MethodInfo* method)
{
	{
		(( void (*) (LinkedList_1_t4843 * __this, LinkedListNode_1_t4842 * p0, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17)->method)(__this, ___node, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 17));
		uint32_t L_0 = (__this->___count_0);
		__this->___count_0 = ((uint32_t)(L_0-1));
		uint32_t L_1 = (__this->___count_0);
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		__this->___first_3 = (LinkedListNode_1_t4842 *)NULL;
	}

IL_0027:
	{
		LinkedListNode_1_t4842 * L_2 = (__this->___first_3);
		if ((((LinkedListNode_1_t4842 *)___node) != ((LinkedListNode_1_t4842 *)L_2)))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t4842 * L_3 = (__this->___first_3);
		NullCheck(L_3);
		LinkedListNode_1_t4842 * L_4 = (L_3->___forward_2);
		__this->___first_3 = L_4;
	}

IL_0044:
	{
		uint32_t L_5 = (__this->___version_1);
		__this->___version_1 = ((int32_t)(L_5+1));
		NullCheck(___node);
		(( void (*) (LinkedListNode_1_t4842 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18)->method)(___node, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 18));
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern MethodInfo LinkedList_1_get_Count_m29747_MethodInfo;
 int32_t LinkedList_1_get_Count_m29747_gshared (LinkedList_1_t4843 * __this, MethodInfo* method)
{
	{
		uint32_t L_0 = (__this->___count_0);
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern MethodInfo LinkedList_1_get_First_m29748_MethodInfo;
 LinkedListNode_1_t4842 * LinkedList_1_get_First_m29748_gshared (LinkedList_1_t4843 * __this, MethodInfo* method)
{
	{
		LinkedListNode_1_t4842 * L_0 = (__this->___first_3);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.LinkedList`1<System.Object>
extern Il2CppType UInt32_t1114_0_0_1;
FieldInfo LinkedList_1_t4843____count_0_FieldInfo = 
{
	"count"/* name */
	, &UInt32_t1114_0_0_1/* type */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedList_1_t4843, ___count_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UInt32_t1114_0_0_1;
FieldInfo LinkedList_1_t4843____version_1_FieldInfo = 
{
	"version"/* name */
	, &UInt32_t1114_0_0_1/* type */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedList_1_t4843, ___version_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Object_t_0_0_1;
FieldInfo LinkedList_1_t4843____syncRoot_2_FieldInfo = 
{
	"syncRoot"/* name */
	, &Object_t_0_0_1/* type */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedList_1_t4843, ___syncRoot_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType LinkedListNode_1_t4842_0_0_3;
FieldInfo LinkedList_1_t4843____first_3_FieldInfo = 
{
	"first"/* name */
	, &LinkedListNode_1_t4842_0_0_3/* type */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedList_1_t4843, ___first_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType SerializationInfo_t1066_0_0_3;
FieldInfo LinkedList_1_t4843____si_4_FieldInfo = 
{
	"si"/* name */
	, &SerializationInfo_t1066_0_0_3/* type */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedList_1_t4843, ___si_4)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* LinkedList_1_t4843_FieldInfos[] =
{
	&LinkedList_1_t4843____count_0_FieldInfo,
	&LinkedList_1_t4843____version_1_FieldInfo,
	&LinkedList_1_t4843____syncRoot_2_FieldInfo,
	&LinkedList_1_t4843____first_3_FieldInfo,
	&LinkedList_1_t4843____si_4_FieldInfo,
	NULL
};
static PropertyInfo LinkedList_1_t4843____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo = 
{
	&LinkedList_1_t4843_il2cpp_TypeInfo/* parent */
	, "System.Collections.Generic.ICollection<T>.IsReadOnly"/* name */
	, &LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29733_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo LinkedList_1_t4843____System_Collections_ICollection_IsSynchronized_PropertyInfo = 
{
	&LinkedList_1_t4843_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.IsSynchronized"/* name */
	, &LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29734_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo LinkedList_1_t4843____System_Collections_ICollection_SyncRoot_PropertyInfo = 
{
	&LinkedList_1_t4843_il2cpp_TypeInfo/* parent */
	, "System.Collections.ICollection.SyncRoot"/* name */
	, &LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29735_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo LinkedList_1_t4843____Count_PropertyInfo = 
{
	&LinkedList_1_t4843_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &LinkedList_1_get_Count_m29747_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo LinkedList_1_t4843____First_PropertyInfo = 
{
	&LinkedList_1_t4843_il2cpp_TypeInfo/* parent */
	, "First"/* name */
	, &LinkedList_1_get_First_m29748_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* LinkedList_1_t4843_PropertyInfos[] =
{
	&LinkedList_1_t4843____System_Collections_Generic_ICollectionU3CTU3E_IsReadOnly_PropertyInfo,
	&LinkedList_1_t4843____System_Collections_ICollection_IsSynchronized_PropertyInfo,
	&LinkedList_1_t4843____System_Collections_ICollection_SyncRoot_PropertyInfo,
	&LinkedList_1_t4843____Count_PropertyInfo,
	&LinkedList_1_t4843____First_PropertyInfo,
	NULL
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1__ctor_m29727_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
MethodInfo LinkedList_1__ctor_m29727_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkedList_1__ctor_m29727_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1__ctor_m29727_GenericMethod/* genericMethod */

};
extern Il2CppType SerializationInfo_t1066_0_0_0;
extern Il2CppType SerializationInfo_t1066_0_0_0;
extern Il2CppType StreamingContext_t1067_0_0_0;
extern Il2CppType StreamingContext_t1067_0_0_0;
static ParameterInfo LinkedList_1_t4843_LinkedList_1__ctor_m29728_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &SerializationInfo_t1066_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &StreamingContext_t1067_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_StreamingContext_t1067 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1__ctor_m29728_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo LinkedList_1__ctor_m29728_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkedList_1__ctor_m29728_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_StreamingContext_t1067/* invoker_method */
	, LinkedList_1_t4843_LinkedList_1__ctor_m29728_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6276/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1__ctor_m29728_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedList_1_t4843_LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29729_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29729_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
MethodInfo LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29729_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.Add"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29729_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, LinkedList_1_t4843_LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29729_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 13/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29729_GenericMethod/* genericMethod */

};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo LinkedList_1_t4843_LinkedList_1_System_Collections_ICollection_CopyTo_m29730_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_ICollection_CopyTo_m29730_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
MethodInfo LinkedList_1_System_Collections_ICollection_CopyTo_m29730_MethodInfo = 
{
	"System.Collections.ICollection.CopyTo"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_ICollection_CopyTo_m29730_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, LinkedList_1_t4843_LinkedList_1_System_Collections_ICollection_CopyTo_m29730_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 8/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_ICollection_CopyTo_m29730_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_1_t455_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29731_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
MethodInfo LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29731_MethodInfo = 
{
	"System.Collections.Generic.IEnumerable<T>.GetEnumerator"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29731_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t455_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29731_GenericMethod/* genericMethod */

};
extern Il2CppType IEnumerator_t266_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29732_GenericMethod;
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
MethodInfo LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29732_MethodInfo = 
{
	"System.Collections.IEnumerable.GetEnumerator"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29732_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_t266_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 481/* flags */
	, 0/* iflags */
	, 10/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29732_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29733_GenericMethod;
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
MethodInfo LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29733_MethodInfo = 
{
	"System.Collections.Generic.ICollection<T>.get_IsReadOnly"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29733_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 12/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29733_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29734_GenericMethod;
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
MethodInfo LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29734_MethodInfo = 
{
	"System.Collections.ICollection.get_IsSynchronized"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29734_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29734_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29735_GenericMethod;
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
MethodInfo LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29735_MethodInfo = 
{
	"System.Collections.ICollection.get_SyncRoot"/* name */
	, (methodPointerType)&LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29735_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29735_GenericMethod/* genericMethod */

};
extern Il2CppType LinkedListNode_1_t4842_0_0_0;
extern Il2CppType LinkedListNode_1_t4842_0_0_0;
static ParameterInfo LinkedList_1_t4843_LinkedList_1_VerifyReferencedNode_m29736_ParameterInfos[] = 
{
	{"node", 0, 134217728, &EmptyCustomAttributesCache, &LinkedListNode_1_t4842_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_VerifyReferencedNode_m29736_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
MethodInfo LinkedList_1_VerifyReferencedNode_m29736_MethodInfo = 
{
	"VerifyReferencedNode"/* name */
	, (methodPointerType)&LinkedList_1_VerifyReferencedNode_m29736_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, LinkedList_1_t4843_LinkedList_1_VerifyReferencedNode_m29736_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 129/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_VerifyReferencedNode_m29736_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedList_1_t4843_LinkedList_1_AddLast_m29737_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType LinkedListNode_1_t4842_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_AddLast_m29737_GenericMethod;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
MethodInfo LinkedList_1_AddLast_m29737_MethodInfo = 
{
	"AddLast"/* name */
	, (methodPointerType)&LinkedList_1_AddLast_m29737_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &LinkedListNode_1_t4842_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, LinkedList_1_t4843_LinkedList_1_AddLast_m29737_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_AddLast_m29737_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_Clear_m29738_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
MethodInfo LinkedList_1_Clear_m29738_MethodInfo = 
{
	"Clear"/* name */
	, (methodPointerType)&LinkedList_1_Clear_m29738_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 14/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_Clear_m29738_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedList_1_t4843_LinkedList_1_Contains_m29739_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_Contains_m29739_GenericMethod;
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
MethodInfo LinkedList_1_Contains_m29739_MethodInfo = 
{
	"Contains"/* name */
	, (methodPointerType)&LinkedList_1_Contains_m29739_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, LinkedList_1_t4843_LinkedList_1_Contains_m29739_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 15/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_Contains_m29739_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo LinkedList_1_t4843_LinkedList_1_CopyTo_m29740_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
	{"index", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_CopyTo_m29740_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
MethodInfo LinkedList_1_CopyTo_m29740_MethodInfo = 
{
	"CopyTo"/* name */
	, (methodPointerType)&LinkedList_1_CopyTo_m29740_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, LinkedList_1_t4843_LinkedList_1_CopyTo_m29740_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 16/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_CopyTo_m29740_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedList_1_t4843_LinkedList_1_Find_m29741_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType LinkedListNode_1_t4842_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_Find_m29741_GenericMethod;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
MethodInfo LinkedList_1_Find_m29741_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&LinkedList_1_Find_m29741_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &LinkedListNode_1_t4842_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t/* invoker_method */
	, LinkedList_1_t4843_LinkedList_1_Find_m29741_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_Find_m29741_GenericMethod/* genericMethod */

};
extern Il2CppType Enumerator_t4844_0_0_0;
extern void* RuntimeInvoker_Enumerator_t4844 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_GetEnumerator_m29742_GenericMethod;
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
MethodInfo LinkedList_1_GetEnumerator_m29742_MethodInfo = 
{
	"GetEnumerator"/* name */
	, (methodPointerType)&LinkedList_1_GetEnumerator_m29742/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Enumerator_t4844_0_0_0/* return_type */
	, RuntimeInvoker_Enumerator_t4844/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_GetEnumerator_m29742_GenericMethod/* genericMethod */

};
extern Il2CppType SerializationInfo_t1066_0_0_0;
extern Il2CppType StreamingContext_t1067_0_0_0;
static ParameterInfo LinkedList_1_t4843_LinkedList_1_GetObjectData_m29743_ParameterInfos[] = 
{
	{"info", 0, 134217728, &EmptyCustomAttributesCache, &SerializationInfo_t1066_0_0_0},
	{"context", 1, 134217728, &EmptyCustomAttributesCache, &StreamingContext_t1067_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_StreamingContext_t1067 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_GetObjectData_m29743_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
MethodInfo LinkedList_1_GetObjectData_m29743_MethodInfo = 
{
	"GetObjectData"/* name */
	, (methodPointerType)&LinkedList_1_GetObjectData_m29743_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_StreamingContext_t1067/* invoker_method */
	, LinkedList_1_t4843_LinkedList_1_GetObjectData_m29743_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 19/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_GetObjectData_m29743_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedList_1_t4843_LinkedList_1_OnDeserialization_m29744_ParameterInfos[] = 
{
	{"sender", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_OnDeserialization_m29744_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
MethodInfo LinkedList_1_OnDeserialization_m29744_MethodInfo = 
{
	"OnDeserialization"/* name */
	, (methodPointerType)&LinkedList_1_OnDeserialization_m29744_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, LinkedList_1_t4843_LinkedList_1_OnDeserialization_m29744_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 0/* iflags */
	, 20/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_OnDeserialization_m29744_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedList_1_t4843_LinkedList_1_Remove_m29745_ParameterInfos[] = 
{
	{"value", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_Remove_m29745_GenericMethod;
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
MethodInfo LinkedList_1_Remove_m29745_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&LinkedList_1_Remove_m29745_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, LinkedList_1_t4843_LinkedList_1_Remove_m29745_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 17/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_Remove_m29745_GenericMethod/* genericMethod */

};
extern Il2CppType LinkedListNode_1_t4842_0_0_0;
static ParameterInfo LinkedList_1_t4843_LinkedList_1_Remove_m29746_ParameterInfos[] = 
{
	{"node", 0, 134217728, &EmptyCustomAttributesCache, &LinkedListNode_1_t4842_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_Remove_m29746_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
MethodInfo LinkedList_1_Remove_m29746_MethodInfo = 
{
	"Remove"/* name */
	, (methodPointerType)&LinkedList_1_Remove_m29746_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, LinkedList_1_t4843_LinkedList_1_Remove_m29746_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 134/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_Remove_m29746_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_get_Count_m29747_GenericMethod;
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
MethodInfo LinkedList_1_get_Count_m29747_MethodInfo = 
{
	"get_Count"/* name */
	, (methodPointerType)&LinkedList_1_get_Count_m29747_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 11/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_get_Count_m29747_GenericMethod/* genericMethod */

};
extern Il2CppType LinkedListNode_1_t4842_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedList_1_get_First_m29748_GenericMethod;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
MethodInfo LinkedList_1_get_First_m29748_MethodInfo = 
{
	"get_First"/* name */
	, (methodPointerType)&LinkedList_1_get_First_m29748_gshared/* method */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* declaring_type */
	, &LinkedListNode_1_t4842_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedList_1_get_First_m29748_GenericMethod/* genericMethod */

};
static MethodInfo* LinkedList_1_t4843_MethodInfos[] =
{
	&LinkedList_1__ctor_m29727_MethodInfo,
	&LinkedList_1__ctor_m29728_MethodInfo,
	&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29729_MethodInfo,
	&LinkedList_1_System_Collections_ICollection_CopyTo_m29730_MethodInfo,
	&LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29731_MethodInfo,
	&LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29732_MethodInfo,
	&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29733_MethodInfo,
	&LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29734_MethodInfo,
	&LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29735_MethodInfo,
	&LinkedList_1_VerifyReferencedNode_m29736_MethodInfo,
	&LinkedList_1_AddLast_m29737_MethodInfo,
	&LinkedList_1_Clear_m29738_MethodInfo,
	&LinkedList_1_Contains_m29739_MethodInfo,
	&LinkedList_1_CopyTo_m29740_MethodInfo,
	&LinkedList_1_Find_m29741_MethodInfo,
	&LinkedList_1_GetEnumerator_m29742_MethodInfo,
	&LinkedList_1_GetObjectData_m29743_MethodInfo,
	&LinkedList_1_OnDeserialization_m29744_MethodInfo,
	&LinkedList_1_Remove_m29745_MethodInfo,
	&LinkedList_1_Remove_m29746_MethodInfo,
	&LinkedList_1_get_Count_m29747_MethodInfo,
	&LinkedList_1_get_First_m29748_MethodInfo,
	NULL
};
static MethodInfo* LinkedList_1_t4843_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m29731_MethodInfo,
	&LinkedList_1_get_Count_m29747_MethodInfo,
	&LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m29734_MethodInfo,
	&LinkedList_1_System_Collections_ICollection_get_SyncRoot_m29735_MethodInfo,
	&LinkedList_1_System_Collections_ICollection_CopyTo_m29730_MethodInfo,
	&LinkedList_1_OnDeserialization_m29744_MethodInfo,
	&LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m29732_MethodInfo,
	&LinkedList_1_get_Count_m29747_MethodInfo,
	&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m29733_MethodInfo,
	&LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m29729_MethodInfo,
	&LinkedList_1_Clear_m29738_MethodInfo,
	&LinkedList_1_Contains_m29739_MethodInfo,
	&LinkedList_1_CopyTo_m29740_MethodInfo,
	&LinkedList_1_Remove_m29745_MethodInfo,
	&LinkedList_1_GetObjectData_m29743_MethodInfo,
	&LinkedList_1_GetObjectData_m29743_MethodInfo,
	&LinkedList_1_OnDeserialization_m29744_MethodInfo,
};
extern TypeInfo IEnumerable_1_t2847_il2cpp_TypeInfo;
extern TypeInfo ICollection_t1206_il2cpp_TypeInfo;
extern TypeInfo IDeserializationCallback_t1497_il2cpp_TypeInfo;
extern TypeInfo ICollection_1_t2848_il2cpp_TypeInfo;
static TypeInfo* LinkedList_1_t4843_InterfacesTypeInfos[] = 
{
	&IEnumerable_1_t2847_il2cpp_TypeInfo,
	&ICollection_t1206_il2cpp_TypeInfo,
	&IDeserializationCallback_t1497_il2cpp_TypeInfo,
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t2848_il2cpp_TypeInfo,
	&ISerializable_t482_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair LinkedList_1_t4843_InterfacesOffsets[] = 
{
	{ &IEnumerable_1_t2847_il2cpp_TypeInfo, 4},
	{ &ICollection_t1206_il2cpp_TypeInfo, 5},
	{ &IDeserializationCallback_t1497_il2cpp_TypeInfo, 9},
	{ &IEnumerable_t1126_il2cpp_TypeInfo, 10},
	{ &ICollection_1_t2848_il2cpp_TypeInfo, 11},
	{ &ISerializable_t482_il2cpp_TypeInfo, 18},
};
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t4844_il2cpp_TypeInfo;
extern TypeInfo LinkedListNode_1_t4842_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
static Il2CppRGCTXData LinkedList_1_t4843_RGCTXData[19] = 
{
	&LinkedList_1__ctor_m29727_MethodInfo/* Method Usage */,
	&LinkedList_1_AddLast_m29737_MethodInfo/* Method Usage */,
	&ObjectU5BU5D_t115_il2cpp_TypeInfo/* Class Usage */,
	&LinkedList_1_CopyTo_m29740_MethodInfo/* Method Usage */,
	&LinkedList_1_GetEnumerator_m29742_MethodInfo/* Method Usage */,
	&Enumerator_t4844_il2cpp_TypeInfo/* Class Usage */,
	&LinkedListNode_1_get_List_m29752_MethodInfo/* Method Usage */,
	&LinkedListNode_1_t4842_il2cpp_TypeInfo/* Class Usage */,
	&LinkedListNode_1__ctor_m29749_MethodInfo/* Method Usage */,
	&LinkedListNode_1__ctor_m29750_MethodInfo/* Method Usage */,
	&LinkedListNode_1_get_Value_m29754_MethodInfo/* Method Usage */,
	&Object_t_il2cpp_TypeInfo/* Class Usage */,
	&Enumerator__ctor_m29755_MethodInfo/* Method Usage */,
	&ObjectU5BU5D_t115_il2cpp_TypeInfo/* Array Usage */,
	&ObjectU5BU5D_t115_0_0_0/* Type Usage */,
	&LinkedList_1_Find_m29741_MethodInfo/* Method Usage */,
	&LinkedList_1_Remove_m29746_MethodInfo/* Method Usage */,
	&LinkedList_1_VerifyReferencedNode_m29736_MethodInfo/* Method Usage */,
	&LinkedListNode_1_Detach_m29751_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType LinkedList_1_t4843_0_0_0;
extern Il2CppType LinkedList_1_t4843_1_0_0;
struct LinkedList_1_t4843;
extern Il2CppGenericClass LinkedList_1_t4843_GenericClass;
extern CustomAttributesCache LinkedList_1_t1291__CustomAttributeCache;
TypeInfo LinkedList_1_t4843_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkedList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, LinkedList_1_t4843_MethodInfos/* methods */
	, LinkedList_1_t4843_PropertyInfos/* properties */
	, LinkedList_1_t4843_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* element_class */
	, LinkedList_1_t4843_InterfacesTypeInfos/* implemented_interfaces */
	, LinkedList_1_t4843_VTable/* vtable */
	, &LinkedList_1_t1291__CustomAttributeCache/* custom_attributes_cache */
	, &LinkedList_1_t4843_il2cpp_TypeInfo/* cast_class */
	, &LinkedList_1_t4843_0_0_0/* byval_arg */
	, &LinkedList_1_t4843_1_0_0/* this_arg */
	, LinkedList_1_t4843_InterfacesOffsets/* interface_offsets */
	, &LinkedList_1_t4843_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, LinkedList_1_t4843_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkedList_1_t4843)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1056769/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 22/* method_count */
	, 5/* property_count */
	, 5/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 21/* vtable_count */
	, 6/* interfaces_count */
	, 6/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
 void LinkedListNode_1__ctor_m29749_gshared (LinkedListNode_1_t4842 * __this, LinkedList_1_t4843 * ___list, Object_t * ___value, MethodInfo* method)
{
	LinkedListNode_1_t4842 * V_0 = {0};
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		__this->___container_1 = ___list;
		__this->___item_0 = ___value;
		V_0 = __this;
		__this->___forward_2 = __this;
		__this->___back_3 = V_0;
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
 void LinkedListNode_1__ctor_m29750_gshared (LinkedListNode_1_t4842 * __this, LinkedList_1_t4843 * ___list, Object_t * ___value, LinkedListNode_1_t4842 * ___previousNode, LinkedListNode_1_t4842 * ___nextNode, MethodInfo* method)
{
	{
		Object__ctor_m271(__this, /*hidden argument*/&Object__ctor_m271_MethodInfo);
		__this->___container_1 = ___list;
		__this->___item_0 = ___value;
		__this->___back_3 = ___previousNode;
		__this->___forward_2 = ___nextNode;
		NullCheck(___previousNode);
		___previousNode->___forward_2 = __this;
		NullCheck(___nextNode);
		___nextNode->___back_3 = __this;
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
 void LinkedListNode_1_Detach_m29751_gshared (LinkedListNode_1_t4842 * __this, MethodInfo* method)
{
	LinkedListNode_1_t4842 * V_0 = {0};
	{
		LinkedListNode_1_t4842 * L_0 = (__this->___back_3);
		LinkedListNode_1_t4842 * L_1 = (__this->___forward_2);
		NullCheck(L_0);
		L_0->___forward_2 = L_1;
		LinkedListNode_1_t4842 * L_2 = (__this->___forward_2);
		LinkedListNode_1_t4842 * L_3 = (__this->___back_3);
		NullCheck(L_2);
		L_2->___back_3 = L_3;
		V_0 = (LinkedListNode_1_t4842 *)NULL;
		__this->___back_3 = (LinkedListNode_1_t4842 *)NULL;
		__this->___forward_2 = V_0;
		__this->___container_1 = (LinkedList_1_t4843 *)NULL;
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
 LinkedList_1_t4843 * LinkedListNode_1_get_List_m29752_gshared (LinkedListNode_1_t4842 * __this, MethodInfo* method)
{
	{
		LinkedList_1_t4843 * L_0 = (__this->___container_1);
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
extern MethodInfo LinkedListNode_1_get_Next_m29753_MethodInfo;
 LinkedListNode_1_t4842 * LinkedListNode_1_get_Next_m29753_gshared (LinkedListNode_1_t4842 * __this, MethodInfo* method)
{
	LinkedListNode_1_t4842 * G_B4_0 = {0};
	{
		LinkedList_1_t4843 * L_0 = (__this->___container_1);
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t4842 * L_1 = (__this->___forward_2);
		LinkedList_1_t4843 * L_2 = (__this->___container_1);
		NullCheck(L_2);
		LinkedListNode_1_t4842 * L_3 = (L_2->___first_3);
		if ((((LinkedListNode_1_t4842 *)L_1) == ((LinkedListNode_1_t4842 *)L_3)))
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t4842 * L_4 = (__this->___forward_2);
		G_B4_0 = L_4;
		goto IL_002d;
	}

IL_002c:
	{
		G_B4_0 = ((LinkedListNode_1_t4842 *)(NULL));
	}

IL_002d:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
 Object_t * LinkedListNode_1_get_Value_m29754_gshared (LinkedListNode_1_t4842 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___item_0);
		return L_0;
	}
}
// Metadata Definition System.Collections.Generic.LinkedListNode`1<System.Object>
extern Il2CppType Object_t_0_0_1;
FieldInfo LinkedListNode_1_t4842____item_0_FieldInfo = 
{
	"item"/* name */
	, &Object_t_0_0_1/* type */
	, &LinkedListNode_1_t4842_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedListNode_1_t4842, ___item_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType LinkedList_1_t4843_0_0_1;
FieldInfo LinkedListNode_1_t4842____container_1_FieldInfo = 
{
	"container"/* name */
	, &LinkedList_1_t4843_0_0_1/* type */
	, &LinkedListNode_1_t4842_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedListNode_1_t4842, ___container_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType LinkedListNode_1_t4842_0_0_3;
FieldInfo LinkedListNode_1_t4842____forward_2_FieldInfo = 
{
	"forward"/* name */
	, &LinkedListNode_1_t4842_0_0_3/* type */
	, &LinkedListNode_1_t4842_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedListNode_1_t4842, ___forward_2)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType LinkedListNode_1_t4842_0_0_3;
FieldInfo LinkedListNode_1_t4842____back_3_FieldInfo = 
{
	"back"/* name */
	, &LinkedListNode_1_t4842_0_0_3/* type */
	, &LinkedListNode_1_t4842_il2cpp_TypeInfo/* parent */
	, offsetof(LinkedListNode_1_t4842, ___back_3)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* LinkedListNode_1_t4842_FieldInfos[] =
{
	&LinkedListNode_1_t4842____item_0_FieldInfo,
	&LinkedListNode_1_t4842____container_1_FieldInfo,
	&LinkedListNode_1_t4842____forward_2_FieldInfo,
	&LinkedListNode_1_t4842____back_3_FieldInfo,
	NULL
};
static PropertyInfo LinkedListNode_1_t4842____List_PropertyInfo = 
{
	&LinkedListNode_1_t4842_il2cpp_TypeInfo/* parent */
	, "List"/* name */
	, &LinkedListNode_1_get_List_m29752_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo LinkedListNode_1_t4842____Next_PropertyInfo = 
{
	&LinkedListNode_1_t4842_il2cpp_TypeInfo/* parent */
	, "Next"/* name */
	, &LinkedListNode_1_get_Next_m29753_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo LinkedListNode_1_t4842____Value_PropertyInfo = 
{
	&LinkedListNode_1_t4842_il2cpp_TypeInfo/* parent */
	, "Value"/* name */
	, &LinkedListNode_1_get_Value_m29754_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* LinkedListNode_1_t4842_PropertyInfos[] =
{
	&LinkedListNode_1_t4842____List_PropertyInfo,
	&LinkedListNode_1_t4842____Next_PropertyInfo,
	&LinkedListNode_1_t4842____Value_PropertyInfo,
	NULL
};
extern Il2CppType LinkedList_1_t4843_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo LinkedListNode_1_t4842_LinkedListNode_1__ctor_m29749_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &LinkedList_1_t4843_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedListNode_1__ctor_m29749_GenericMethod;
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
MethodInfo LinkedListNode_1__ctor_m29749_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkedListNode_1__ctor_m29749_gshared/* method */
	, &LinkedListNode_1_t4842_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, LinkedListNode_1_t4842_LinkedListNode_1__ctor_m29749_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedListNode_1__ctor_m29749_GenericMethod/* genericMethod */

};
extern Il2CppType LinkedList_1_t4843_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType LinkedListNode_1_t4842_0_0_0;
extern Il2CppType LinkedListNode_1_t4842_0_0_0;
static ParameterInfo LinkedListNode_1_t4842_LinkedListNode_1__ctor_m29750_ParameterInfos[] = 
{
	{"list", 0, 134217728, &EmptyCustomAttributesCache, &LinkedList_1_t4843_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"previousNode", 2, 134217728, &EmptyCustomAttributesCache, &LinkedListNode_1_t4842_0_0_0},
	{"nextNode", 3, 134217728, &EmptyCustomAttributesCache, &LinkedListNode_1_t4842_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedListNode_1__ctor_m29750_GenericMethod;
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
MethodInfo LinkedListNode_1__ctor_m29750_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&LinkedListNode_1__ctor_m29750_gshared/* method */
	, &LinkedListNode_1_t4842_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, LinkedListNode_1_t4842_LinkedListNode_1__ctor_m29750_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 4/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedListNode_1__ctor_m29750_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedListNode_1_Detach_m29751_GenericMethod;
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
MethodInfo LinkedListNode_1_Detach_m29751_MethodInfo = 
{
	"Detach"/* name */
	, (methodPointerType)&LinkedListNode_1_Detach_m29751_gshared/* method */
	, &LinkedListNode_1_t4842_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 131/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedListNode_1_Detach_m29751_GenericMethod/* genericMethod */

};
extern Il2CppType LinkedList_1_t4843_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedListNode_1_get_List_m29752_GenericMethod;
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
MethodInfo LinkedListNode_1_get_List_m29752_MethodInfo = 
{
	"get_List"/* name */
	, (methodPointerType)&LinkedListNode_1_get_List_m29752_gshared/* method */
	, &LinkedListNode_1_t4842_il2cpp_TypeInfo/* declaring_type */
	, &LinkedList_1_t4843_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedListNode_1_get_List_m29752_GenericMethod/* genericMethod */

};
extern Il2CppType LinkedListNode_1_t4842_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedListNode_1_get_Next_m29753_GenericMethod;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
MethodInfo LinkedListNode_1_get_Next_m29753_MethodInfo = 
{
	"get_Next"/* name */
	, (methodPointerType)&LinkedListNode_1_get_Next_m29753_gshared/* method */
	, &LinkedListNode_1_t4842_il2cpp_TypeInfo/* declaring_type */
	, &LinkedListNode_1_t4842_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedListNode_1_get_Next_m29753_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod LinkedListNode_1_get_Value_m29754_GenericMethod;
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
MethodInfo LinkedListNode_1_get_Value_m29754_MethodInfo = 
{
	"get_Value"/* name */
	, (methodPointerType)&LinkedListNode_1_get_Value_m29754_gshared/* method */
	, &LinkedListNode_1_t4842_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2182/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &LinkedListNode_1_get_Value_m29754_GenericMethod/* genericMethod */

};
static MethodInfo* LinkedListNode_1_t4842_MethodInfos[] =
{
	&LinkedListNode_1__ctor_m29749_MethodInfo,
	&LinkedListNode_1__ctor_m29750_MethodInfo,
	&LinkedListNode_1_Detach_m29751_MethodInfo,
	&LinkedListNode_1_get_List_m29752_MethodInfo,
	&LinkedListNode_1_get_Next_m29753_MethodInfo,
	&LinkedListNode_1_get_Value_m29754_MethodInfo,
	NULL
};
static MethodInfo* LinkedListNode_1_t4842_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType LinkedListNode_1_t4842_1_0_0;
struct LinkedListNode_1_t4842;
extern Il2CppGenericClass LinkedListNode_1_t4842_GenericClass;
extern CustomAttributesCache LinkedListNode_1_t1292__CustomAttributeCache;
TypeInfo LinkedListNode_1_t4842_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "LinkedListNode`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, LinkedListNode_1_t4842_MethodInfos/* methods */
	, LinkedListNode_1_t4842_PropertyInfos/* properties */
	, LinkedListNode_1_t4842_FieldInfos/* fields */
	, NULL/* events */
	, &Object_t_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &LinkedListNode_1_t4842_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, LinkedListNode_1_t4842_VTable/* vtable */
	, &LinkedListNode_1_t1292__CustomAttributeCache/* custom_attributes_cache */
	, &LinkedListNode_1_t4842_il2cpp_TypeInfo/* cast_class */
	, &LinkedListNode_1_t4842_0_0_0/* byval_arg */
	, &LinkedListNode_1_t4842_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &LinkedListNode_1_t4842_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (LinkedListNode_1_t4842)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048833/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 6/* method_count */
	, 3/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedException.h"
extern TypeInfo ObjectDisposedException_t1665_il2cpp_TypeInfo;
// System.ObjectDisposedException
#include "mscorlib_System_ObjectDisposedExceptionMethodDeclarations.h"
extern MethodInfo Enumerator_get_Current_m29757_MethodInfo;
extern MethodInfo ObjectDisposedException__ctor_m8829_MethodInfo;


// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
 void Enumerator__ctor_m29755_gshared (Enumerator_t4844 * __this, LinkedList_1_t4843 * ___parent, MethodInfo* method)
{
	{
		__this->___list_0 = ___parent;
		__this->___current_1 = (LinkedListNode_1_t4842 *)NULL;
		__this->___index_2 = (-1);
		NullCheck(___parent);
		uint32_t L_0 = (___parent->___version_1);
		__this->___version_3 = L_0;
		return;
	}
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m29756_MethodInfo;
 Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m29756_gshared (Enumerator_t4844 * __this, MethodInfo* method)
{
	{
		Object_t * L_0 = (( Object_t * (*) (Enumerator_t4844 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)(__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Object_t * L_1 = L_0;
		return ((Object_t *)L_1);
	}
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
 Object_t * Enumerator_get_Current_m29757_gshared (Enumerator_t4844 * __this, MethodInfo* method)
{
	{
		LinkedList_1_t4843 * L_0 = (__this->___list_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1665 * L_1 = (ObjectDisposedException_t1665 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ObjectDisposedException_t1665_il2cpp_TypeInfo));
		ObjectDisposedException__ctor_m8829(L_1, (String_t*)NULL, /*hidden argument*/&ObjectDisposedException__ctor_m8829_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t4842 * L_2 = (__this->___current_1);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7659(L_3, /*hidden argument*/&InvalidOperationException__ctor_m7659_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t4842 * L_4 = (__this->___current_1);
		NullCheck(L_4);
		Object_t * L_5 = (( Object_t * (*) (LinkedListNode_1_t4842 * __this, MethodInfo* method))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)(L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern MethodInfo Enumerator_MoveNext_m29758_MethodInfo;
 bool Enumerator_MoveNext_m29758_gshared (Enumerator_t4844 * __this, MethodInfo* method)
{
	{
		LinkedList_1_t4843 * L_0 = (__this->___list_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1665 * L_1 = (ObjectDisposedException_t1665 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ObjectDisposedException_t1665_il2cpp_TypeInfo));
		ObjectDisposedException__ctor_m8829(L_1, (String_t*)NULL, /*hidden argument*/&ObjectDisposedException__ctor_m8829_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (__this->___version_3);
		LinkedList_1_t4843 * L_3 = (__this->___list_0);
		NullCheck(L_3);
		uint32_t L_4 = (L_3->___version_1);
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t1493 * L_5 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_5, (String_t*) &_stringLiteral455, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t4842 * L_6 = (__this->___current_1);
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t4843 * L_7 = (__this->___list_0);
		NullCheck(L_7);
		LinkedListNode_1_t4842 * L_8 = (L_7->___first_3);
		__this->___current_1 = L_8;
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t4842 * L_9 = (__this->___current_1);
		NullCheck(L_9);
		LinkedListNode_1_t4842 * L_10 = (L_9->___forward_2);
		__this->___current_1 = L_10;
		LinkedListNode_1_t4842 * L_11 = (__this->___current_1);
		LinkedList_1_t4843 * L_12 = (__this->___list_0);
		NullCheck(L_12);
		LinkedListNode_1_t4842 * L_13 = (L_12->___first_3);
		if ((((LinkedListNode_1_t4842 *)L_11) != ((LinkedListNode_1_t4842 *)L_13)))
		{
			goto IL_0082;
		}
	}
	{
		__this->___current_1 = (LinkedListNode_1_t4842 *)NULL;
	}

IL_0082:
	{
		LinkedListNode_1_t4842 * L_14 = (__this->___current_1);
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->___index_2 = (-1);
		return 0;
	}

IL_0096:
	{
		int32_t L_15 = (__this->___index_2);
		__this->___index_2 = ((int32_t)(L_15+1));
		return 1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern MethodInfo Enumerator_Dispose_m29759_MethodInfo;
 void Enumerator_Dispose_m29759_gshared (Enumerator_t4844 * __this, MethodInfo* method)
{
	{
		LinkedList_1_t4843 * L_0 = (__this->___list_0);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1665 * L_1 = (ObjectDisposedException_t1665 *)il2cpp_codegen_object_new (InitializedTypeInfo(&ObjectDisposedException_t1665_il2cpp_TypeInfo));
		ObjectDisposedException__ctor_m8829(L_1, (String_t*)NULL, /*hidden argument*/&ObjectDisposedException__ctor_m8829_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0012:
	{
		__this->___current_1 = (LinkedListNode_1_t4842 *)NULL;
		__this->___list_0 = (LinkedList_1_t4843 *)NULL;
		return;
	}
}
// Metadata Definition System.Collections.Generic.LinkedList`1/Enumerator<System.Object>
extern Il2CppType LinkedList_1_t4843_0_0_1;
FieldInfo Enumerator_t4844____list_0_FieldInfo = 
{
	"list"/* name */
	, &LinkedList_1_t4843_0_0_1/* type */
	, &Enumerator_t4844_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t4844, ___list_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType LinkedListNode_1_t4842_0_0_1;
FieldInfo Enumerator_t4844____current_1_FieldInfo = 
{
	"current"/* name */
	, &LinkedListNode_1_t4842_0_0_1/* type */
	, &Enumerator_t4844_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t4844, ___current_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo Enumerator_t4844____index_2_FieldInfo = 
{
	"index"/* name */
	, &Int32_t93_0_0_1/* type */
	, &Enumerator_t4844_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t4844, ___index_2) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType UInt32_t1114_0_0_1;
FieldInfo Enumerator_t4844____version_3_FieldInfo = 
{
	"version"/* name */
	, &UInt32_t1114_0_0_1/* type */
	, &Enumerator_t4844_il2cpp_TypeInfo/* parent */
	, offsetof(Enumerator_t4844, ___version_3) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* Enumerator_t4844_FieldInfos[] =
{
	&Enumerator_t4844____list_0_FieldInfo,
	&Enumerator_t4844____current_1_FieldInfo,
	&Enumerator_t4844____index_2_FieldInfo,
	&Enumerator_t4844____version_3_FieldInfo,
	NULL
};
static PropertyInfo Enumerator_t4844____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&Enumerator_t4844_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m29756_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo Enumerator_t4844____Current_PropertyInfo = 
{
	&Enumerator_t4844_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &Enumerator_get_Current_m29757_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* Enumerator_t4844_PropertyInfos[] =
{
	&Enumerator_t4844____System_Collections_IEnumerator_Current_PropertyInfo,
	&Enumerator_t4844____Current_PropertyInfo,
	NULL
};
extern Il2CppType LinkedList_1_t4843_0_0_0;
static ParameterInfo Enumerator_t4844_Enumerator__ctor_m29755_ParameterInfos[] = 
{
	{"parent", 0, 134217728, &EmptyCustomAttributesCache, &LinkedList_1_t4843_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator__ctor_m29755_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
MethodInfo Enumerator__ctor_m29755_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Enumerator__ctor_m29755_gshared/* method */
	, &Enumerator_t4844_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, Enumerator_t4844_Enumerator__ctor_m29755_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator__ctor_m29755_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_System_Collections_IEnumerator_get_Current_m29756_GenericMethod;
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
MethodInfo Enumerator_System_Collections_IEnumerator_get_Current_m29756_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&Enumerator_System_Collections_IEnumerator_get_Current_m29756_gshared/* method */
	, &Enumerator_t4844_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_System_Collections_IEnumerator_get_Current_m29756_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_get_Current_m29757_GenericMethod;
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
MethodInfo Enumerator_get_Current_m29757_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&Enumerator_get_Current_m29757_gshared/* method */
	, &Enumerator_t4844_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_get_Current_m29757_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_MoveNext_m29758_GenericMethod;
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
MethodInfo Enumerator_MoveNext_m29758_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&Enumerator_MoveNext_m29758_gshared/* method */
	, &Enumerator_t4844_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_MoveNext_m29758_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Enumerator_Dispose_m29759_GenericMethod;
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
MethodInfo Enumerator_Dispose_m29759_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&Enumerator_Dispose_m29759_gshared/* method */
	, &Enumerator_t4844_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Enumerator_Dispose_m29759_GenericMethod/* genericMethod */

};
static MethodInfo* Enumerator_t4844_MethodInfos[] =
{
	&Enumerator__ctor_m29755_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m29756_MethodInfo,
	&Enumerator_get_Current_m29757_MethodInfo,
	&Enumerator_MoveNext_m29758_MethodInfo,
	&Enumerator_Dispose_m29759_MethodInfo,
	NULL
};
static MethodInfo* Enumerator_t4844_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&Enumerator_get_Current_m29757_MethodInfo,
	&Enumerator_System_Collections_IEnumerator_get_Current_m29756_MethodInfo,
	&Enumerator_MoveNext_m29758_MethodInfo,
	&Enumerator_Dispose_m29759_MethodInfo,
};
extern TypeInfo IEnumerator_1_t455_il2cpp_TypeInfo;
static TypeInfo* Enumerator_t4844_InterfacesTypeInfos[] = 
{
	&IEnumerator_1_t455_il2cpp_TypeInfo,
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair Enumerator_t4844_InterfacesOffsets[] = 
{
	{ &IEnumerator_1_t455_il2cpp_TypeInfo, 4},
	{ &IEnumerator_t266_il2cpp_TypeInfo, 5},
	{ &IDisposable_t139_il2cpp_TypeInfo, 7},
};
extern TypeInfo Object_t_il2cpp_TypeInfo;
static Il2CppRGCTXData Enumerator_t4844_RGCTXData[3] = 
{
	&Enumerator_get_Current_m29757_MethodInfo/* Method Usage */,
	&Object_t_il2cpp_TypeInfo/* Class Usage */,
	&LinkedListNode_1_get_Value_m29754_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_System_dll_Image;
extern Il2CppType Enumerator_t4844_0_0_0;
extern Il2CppType Enumerator_t4844_1_0_0;
extern Il2CppGenericClass Enumerator_t4844_GenericClass;
extern TypeInfo LinkedList_1_t1291_il2cpp_TypeInfo;
TypeInfo Enumerator_t4844_il2cpp_TypeInfo = 
{
	&g_System_dll_Image/* image */
	, NULL/* gc_desc */
	, "Enumerator"/* name */
	, ""/* namespaze */
	, Enumerator_t4844_MethodInfos/* methods */
	, Enumerator_t4844_PropertyInfos/* properties */
	, Enumerator_t4844_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &LinkedList_1_t1291_il2cpp_TypeInfo/* nested_in */
	, &Enumerator_t4844_il2cpp_TypeInfo/* element_class */
	, Enumerator_t4844_InterfacesTypeInfos/* implemented_interfaces */
	, Enumerator_t4844_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Enumerator_t4844_il2cpp_TypeInfo/* cast_class */
	, &Enumerator_t4844_0_0_0/* byval_arg */
	, &Enumerator_t4844_1_0_0/* this_arg */
	, Enumerator_t4844_InterfacesOffsets/* interface_offsets */
	, &Enumerator_t4844_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, Enumerator_t4844_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Enumerator_t4844)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1057034/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 4/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6710_il2cpp_TypeInfo;

// System.ComponentModel.EditorBrowsableAttribute
#include "System_System_ComponentModel_EditorBrowsableAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>
extern MethodInfo IEnumerator_1_get_Current_m48333_MethodInfo;
static PropertyInfo IEnumerator_1_t6710____Current_PropertyInfo = 
{
	&IEnumerator_1_t6710_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48333_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6710_PropertyInfos[] =
{
	&IEnumerator_1_t6710____Current_PropertyInfo,
	NULL
};
extern Il2CppType EditorBrowsableAttribute_t1128_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48333_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48333_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6710_il2cpp_TypeInfo/* declaring_type */
	, &EditorBrowsableAttribute_t1128_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48333_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6710_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48333_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6710_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6710_0_0_0;
extern Il2CppType IEnumerator_1_t6710_1_0_0;
struct IEnumerator_1_t6710;
extern Il2CppGenericClass IEnumerator_1_t6710_GenericClass;
TypeInfo IEnumerator_1_t6710_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6710_MethodInfos/* methods */
	, IEnumerator_1_t6710_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6710_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6710_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6710_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6710_0_0_0/* byval_arg */
	, &IEnumerator_1_t6710_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6710_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_484.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4845_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_484MethodDeclarations.h"

extern TypeInfo EditorBrowsableAttribute_t1128_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29764_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEditorBrowsableAttribute_t1128_m38165_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.ComponentModel.EditorBrowsableAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.ComponentModel.EditorBrowsableAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisEditorBrowsableAttribute_t1128_m38165(__this, p0, method) (EditorBrowsableAttribute_t1128 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4845____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4845_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4845, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4845____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4845_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4845, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4845_FieldInfos[] =
{
	&InternalEnumerator_1_t4845____array_0_FieldInfo,
	&InternalEnumerator_1_t4845____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29761_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4845____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4845_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29761_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4845____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4845_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29764_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4845_PropertyInfos[] =
{
	&InternalEnumerator_1_t4845____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4845____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4845_InternalEnumerator_1__ctor_m29760_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29760_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29760_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4845_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4845_InternalEnumerator_1__ctor_m29760_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29760_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29761_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29761_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4845_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29761_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29762_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29762_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4845_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29762_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29763_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29763_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4845_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29763_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableAttribute_t1128_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29764_GenericMethod;
// T System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29764_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4845_il2cpp_TypeInfo/* declaring_type */
	, &EditorBrowsableAttribute_t1128_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29764_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4845_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29760_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29761_MethodInfo,
	&InternalEnumerator_1_Dispose_m29762_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29763_MethodInfo,
	&InternalEnumerator_1_get_Current_m29764_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29763_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29762_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4845_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29761_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29763_MethodInfo,
	&InternalEnumerator_1_Dispose_m29762_MethodInfo,
	&InternalEnumerator_1_get_Current_m29764_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4845_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6710_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4845_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6710_il2cpp_TypeInfo, 7},
};
extern TypeInfo EditorBrowsableAttribute_t1128_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4845_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29764_MethodInfo/* Method Usage */,
	&EditorBrowsableAttribute_t1128_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisEditorBrowsableAttribute_t1128_m38165_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4845_0_0_0;
extern Il2CppType InternalEnumerator_1_t4845_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4845_GenericClass;
TypeInfo InternalEnumerator_1_t4845_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4845_MethodInfos/* methods */
	, InternalEnumerator_1_t4845_PropertyInfos/* properties */
	, InternalEnumerator_1_t4845_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4845_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4845_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4845_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4845_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4845_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4845_1_0_0/* this_arg */
	, InternalEnumerator_1_t4845_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4845_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4845_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4845)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8500_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>
extern MethodInfo ICollection_1_get_Count_m48334_MethodInfo;
static PropertyInfo ICollection_1_t8500____Count_PropertyInfo = 
{
	&ICollection_1_t8500_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48334_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48335_MethodInfo;
static PropertyInfo ICollection_1_t8500____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8500_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48335_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8500_PropertyInfos[] =
{
	&ICollection_1_t8500____Count_PropertyInfo,
	&ICollection_1_t8500____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48334_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m48334_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8500_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48334_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48335_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48335_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8500_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48335_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableAttribute_t1128_0_0_0;
extern Il2CppType EditorBrowsableAttribute_t1128_0_0_0;
static ParameterInfo ICollection_1_t8500_ICollection_1_Add_m48336_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttribute_t1128_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48336_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Add(T)
MethodInfo ICollection_1_Add_m48336_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8500_ICollection_1_Add_m48336_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48336_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48337_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Clear()
MethodInfo ICollection_1_Clear_m48337_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48337_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableAttribute_t1128_0_0_0;
static ParameterInfo ICollection_1_t8500_ICollection_1_Contains_m48338_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttribute_t1128_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48338_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m48338_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8500_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8500_ICollection_1_Contains_m48338_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48338_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableAttributeU5BU5D_t5650_0_0_0;
extern Il2CppType EditorBrowsableAttributeU5BU5D_t5650_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8500_ICollection_1_CopyTo_m48339_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttributeU5BU5D_t5650_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48339_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48339_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8500_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8500_ICollection_1_CopyTo_m48339_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48339_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableAttribute_t1128_0_0_0;
static ParameterInfo ICollection_1_t8500_ICollection_1_Remove_m48340_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttribute_t1128_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48340_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m48340_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8500_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8500_ICollection_1_Remove_m48340_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48340_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8500_MethodInfos[] =
{
	&ICollection_1_get_Count_m48334_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48335_MethodInfo,
	&ICollection_1_Add_m48336_MethodInfo,
	&ICollection_1_Clear_m48337_MethodInfo,
	&ICollection_1_Contains_m48338_MethodInfo,
	&ICollection_1_CopyTo_m48339_MethodInfo,
	&ICollection_1_Remove_m48340_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8502_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8500_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8502_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8500_0_0_0;
extern Il2CppType ICollection_1_t8500_1_0_0;
struct ICollection_1_t8500;
extern Il2CppGenericClass ICollection_1_t8500_GenericClass;
TypeInfo ICollection_1_t8500_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8500_MethodInfos/* methods */
	, ICollection_1_t8500_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8500_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8500_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8500_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8500_0_0_0/* byval_arg */
	, &ICollection_1_t8500_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8500_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableAttribute>
extern Il2CppType IEnumerator_1_t6710_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48341_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48341_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8502_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6710_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48341_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8502_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48341_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8502_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8502_0_0_0;
extern Il2CppType IEnumerable_1_t8502_1_0_0;
struct IEnumerable_1_t8502;
extern Il2CppGenericClass IEnumerable_1_t8502_GenericClass;
TypeInfo IEnumerable_1_t8502_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8502_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8502_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8502_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8502_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8502_0_0_0/* byval_arg */
	, &IEnumerable_1_t8502_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8502_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8501_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>
extern MethodInfo IList_1_get_Item_m48342_MethodInfo;
extern MethodInfo IList_1_set_Item_m48343_MethodInfo;
static PropertyInfo IList_1_t8501____Item_PropertyInfo = 
{
	&IList_1_t8501_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48342_MethodInfo/* get */
	, &IList_1_set_Item_m48343_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8501_PropertyInfos[] =
{
	&IList_1_t8501____Item_PropertyInfo,
	NULL
};
extern Il2CppType EditorBrowsableAttribute_t1128_0_0_0;
static ParameterInfo IList_1_t8501_IList_1_IndexOf_m48344_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttribute_t1128_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48344_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48344_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8501_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8501_IList_1_IndexOf_m48344_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48344_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType EditorBrowsableAttribute_t1128_0_0_0;
static ParameterInfo IList_1_t8501_IList_1_Insert_m48345_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttribute_t1128_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48345_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48345_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8501_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8501_IList_1_Insert_m48345_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48345_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8501_IList_1_RemoveAt_m48346_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48346_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48346_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8501_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8501_IList_1_RemoveAt_m48346_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48346_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8501_IList_1_get_Item_m48342_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType EditorBrowsableAttribute_t1128_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48342_GenericMethod;
// T System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48342_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8501_il2cpp_TypeInfo/* declaring_type */
	, &EditorBrowsableAttribute_t1128_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8501_IList_1_get_Item_m48342_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48342_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType EditorBrowsableAttribute_t1128_0_0_0;
static ParameterInfo IList_1_t8501_IList_1_set_Item_m48343_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableAttribute_t1128_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48343_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48343_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8501_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8501_IList_1_set_Item_m48343_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48343_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8501_MethodInfos[] =
{
	&IList_1_IndexOf_m48344_MethodInfo,
	&IList_1_Insert_m48345_MethodInfo,
	&IList_1_RemoveAt_m48346_MethodInfo,
	&IList_1_get_Item_m48342_MethodInfo,
	&IList_1_set_Item_m48343_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8501_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8500_il2cpp_TypeInfo,
	&IEnumerable_1_t8502_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8501_0_0_0;
extern Il2CppType IList_1_t8501_1_0_0;
struct IList_1_t8501;
extern Il2CppGenericClass IList_1_t8501_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8501_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8501_MethodInfos/* methods */
	, IList_1_t8501_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8501_il2cpp_TypeInfo/* element_class */
	, IList_1_t8501_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8501_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8501_0_0_0/* byval_arg */
	, &IList_1_t8501_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8501_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6712_il2cpp_TypeInfo;

// System.ComponentModel.EditorBrowsableState
#include "System_System_ComponentModel_EditorBrowsableState.h"


// T System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableState>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableState>
extern MethodInfo IEnumerator_1_get_Current_m48347_MethodInfo;
static PropertyInfo IEnumerator_1_t6712____Current_PropertyInfo = 
{
	&IEnumerator_1_t6712_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48347_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6712_PropertyInfos[] =
{
	&IEnumerator_1_t6712____Current_PropertyInfo,
	NULL
};
extern Il2CppType EditorBrowsableState_t1312_0_0_0;
extern void* RuntimeInvoker_EditorBrowsableState_t1312 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48347_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.ComponentModel.EditorBrowsableState>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48347_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6712_il2cpp_TypeInfo/* declaring_type */
	, &EditorBrowsableState_t1312_0_0_0/* return_type */
	, RuntimeInvoker_EditorBrowsableState_t1312/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48347_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6712_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48347_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6712_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6712_0_0_0;
extern Il2CppType IEnumerator_1_t6712_1_0_0;
struct IEnumerator_1_t6712;
extern Il2CppGenericClass IEnumerator_1_t6712_GenericClass;
TypeInfo IEnumerator_1_t6712_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6712_MethodInfos/* methods */
	, IEnumerator_1_t6712_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6712_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6712_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6712_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6712_0_0_0/* byval_arg */
	, &IEnumerator_1_t6712_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6712_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_485.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4846_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_485MethodDeclarations.h"

extern TypeInfo EditorBrowsableState_t1312_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29769_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisEditorBrowsableState_t1312_m38176_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.ComponentModel.EditorBrowsableState>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.ComponentModel.EditorBrowsableState>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisEditorBrowsableState_t1312_m38176 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29765_MethodInfo;
 void InternalEnumerator_1__ctor_m29765 (InternalEnumerator_1_t4846 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29766_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29766 (InternalEnumerator_1_t4846 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29769(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29769_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&EditorBrowsableState_t1312_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29767_MethodInfo;
 void InternalEnumerator_1_Dispose_m29767 (InternalEnumerator_1_t4846 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29768_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29768 (InternalEnumerator_1_t4846 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29769 (InternalEnumerator_1_t4846 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisEditorBrowsableState_t1312_m38176(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisEditorBrowsableState_t1312_m38176_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4846____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4846_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4846, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4846____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4846_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4846, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4846_FieldInfos[] =
{
	&InternalEnumerator_1_t4846____array_0_FieldInfo,
	&InternalEnumerator_1_t4846____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4846____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4846_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29766_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4846____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4846_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29769_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4846_PropertyInfos[] =
{
	&InternalEnumerator_1_t4846____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4846____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4846_InternalEnumerator_1__ctor_m29765_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29765_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29765_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29765/* method */
	, &InternalEnumerator_1_t4846_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4846_InternalEnumerator_1__ctor_m29765_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29765_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29766_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29766_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29766/* method */
	, &InternalEnumerator_1_t4846_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29766_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29767_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29767_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29767/* method */
	, &InternalEnumerator_1_t4846_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29767_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29768_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29768_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29768/* method */
	, &InternalEnumerator_1_t4846_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29768_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableState_t1312_0_0_0;
extern void* RuntimeInvoker_EditorBrowsableState_t1312 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29769_GenericMethod;
// T System.Array/InternalEnumerator`1<System.ComponentModel.EditorBrowsableState>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29769_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29769/* method */
	, &InternalEnumerator_1_t4846_il2cpp_TypeInfo/* declaring_type */
	, &EditorBrowsableState_t1312_0_0_0/* return_type */
	, RuntimeInvoker_EditorBrowsableState_t1312/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29769_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4846_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29765_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29766_MethodInfo,
	&InternalEnumerator_1_Dispose_m29767_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29768_MethodInfo,
	&InternalEnumerator_1_get_Current_m29769_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4846_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29766_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29768_MethodInfo,
	&InternalEnumerator_1_Dispose_m29767_MethodInfo,
	&InternalEnumerator_1_get_Current_m29769_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4846_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6712_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4846_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6712_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4846_0_0_0;
extern Il2CppType InternalEnumerator_1_t4846_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4846_GenericClass;
TypeInfo InternalEnumerator_1_t4846_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4846_MethodInfos/* methods */
	, InternalEnumerator_1_t4846_PropertyInfos/* properties */
	, InternalEnumerator_1_t4846_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4846_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4846_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4846_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4846_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4846_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4846_1_0_0/* this_arg */
	, InternalEnumerator_1_t4846_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4846_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4846)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8503_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>
extern MethodInfo ICollection_1_get_Count_m48348_MethodInfo;
static PropertyInfo ICollection_1_t8503____Count_PropertyInfo = 
{
	&ICollection_1_t8503_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48348_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48349_MethodInfo;
static PropertyInfo ICollection_1_t8503____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8503_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48349_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8503_PropertyInfos[] =
{
	&ICollection_1_t8503____Count_PropertyInfo,
	&ICollection_1_t8503____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48348_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::get_Count()
MethodInfo ICollection_1_get_Count_m48348_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8503_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48348_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48349_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48349_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8503_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48349_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableState_t1312_0_0_0;
extern Il2CppType EditorBrowsableState_t1312_0_0_0;
static ParameterInfo ICollection_1_t8503_ICollection_1_Add_m48350_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableState_t1312_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48350_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Add(T)
MethodInfo ICollection_1_Add_m48350_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8503_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8503_ICollection_1_Add_m48350_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48350_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48351_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Clear()
MethodInfo ICollection_1_Clear_m48351_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8503_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48351_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableState_t1312_0_0_0;
static ParameterInfo ICollection_1_t8503_ICollection_1_Contains_m48352_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableState_t1312_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48352_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Contains(T)
MethodInfo ICollection_1_Contains_m48352_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8503_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8503_ICollection_1_Contains_m48352_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48352_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableStateU5BU5D_t5651_0_0_0;
extern Il2CppType EditorBrowsableStateU5BU5D_t5651_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8503_ICollection_1_CopyTo_m48353_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableStateU5BU5D_t5651_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48353_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48353_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8503_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8503_ICollection_1_CopyTo_m48353_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48353_GenericMethod/* genericMethod */

};
extern Il2CppType EditorBrowsableState_t1312_0_0_0;
static ParameterInfo ICollection_1_t8503_ICollection_1_Remove_m48354_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableState_t1312_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48354_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.EditorBrowsableState>::Remove(T)
MethodInfo ICollection_1_Remove_m48354_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8503_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8503_ICollection_1_Remove_m48354_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48354_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8503_MethodInfos[] =
{
	&ICollection_1_get_Count_m48348_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48349_MethodInfo,
	&ICollection_1_Add_m48350_MethodInfo,
	&ICollection_1_Clear_m48351_MethodInfo,
	&ICollection_1_Contains_m48352_MethodInfo,
	&ICollection_1_CopyTo_m48353_MethodInfo,
	&ICollection_1_Remove_m48354_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8505_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8503_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8505_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8503_0_0_0;
extern Il2CppType ICollection_1_t8503_1_0_0;
struct ICollection_1_t8503;
extern Il2CppGenericClass ICollection_1_t8503_GenericClass;
TypeInfo ICollection_1_t8503_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8503_MethodInfos/* methods */
	, ICollection_1_t8503_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8503_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8503_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8503_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8503_0_0_0/* byval_arg */
	, &ICollection_1_t8503_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8503_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableState>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableState>
extern Il2CppType IEnumerator_1_t6712_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48355_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ComponentModel.EditorBrowsableState>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48355_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8505_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6712_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48355_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8505_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48355_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8505_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8505_0_0_0;
extern Il2CppType IEnumerable_1_t8505_1_0_0;
struct IEnumerable_1_t8505;
extern Il2CppGenericClass IEnumerable_1_t8505_GenericClass;
TypeInfo IEnumerable_1_t8505_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8505_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8505_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8505_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8505_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8505_0_0_0/* byval_arg */
	, &IEnumerable_1_t8505_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8505_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8504_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>
extern MethodInfo IList_1_get_Item_m48356_MethodInfo;
extern MethodInfo IList_1_set_Item_m48357_MethodInfo;
static PropertyInfo IList_1_t8504____Item_PropertyInfo = 
{
	&IList_1_t8504_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48356_MethodInfo/* get */
	, &IList_1_set_Item_m48357_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8504_PropertyInfos[] =
{
	&IList_1_t8504____Item_PropertyInfo,
	NULL
};
extern Il2CppType EditorBrowsableState_t1312_0_0_0;
static ParameterInfo IList_1_t8504_IList_1_IndexOf_m48358_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableState_t1312_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48358_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48358_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8504_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8504_IList_1_IndexOf_m48358_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48358_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType EditorBrowsableState_t1312_0_0_0;
static ParameterInfo IList_1_t8504_IList_1_Insert_m48359_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableState_t1312_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48359_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48359_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8504_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8504_IList_1_Insert_m48359_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48359_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8504_IList_1_RemoveAt_m48360_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48360_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48360_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8504_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8504_IList_1_RemoveAt_m48360_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48360_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8504_IList_1_get_Item_m48356_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType EditorBrowsableState_t1312_0_0_0;
extern void* RuntimeInvoker_EditorBrowsableState_t1312_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48356_GenericMethod;
// T System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48356_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8504_il2cpp_TypeInfo/* declaring_type */
	, &EditorBrowsableState_t1312_0_0_0/* return_type */
	, RuntimeInvoker_EditorBrowsableState_t1312_Int32_t93/* invoker_method */
	, IList_1_t8504_IList_1_get_Item_m48356_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48356_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType EditorBrowsableState_t1312_0_0_0;
static ParameterInfo IList_1_t8504_IList_1_set_Item_m48357_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &EditorBrowsableState_t1312_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48357_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.EditorBrowsableState>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48357_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8504_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8504_IList_1_set_Item_m48357_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48357_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8504_MethodInfos[] =
{
	&IList_1_IndexOf_m48358_MethodInfo,
	&IList_1_Insert_m48359_MethodInfo,
	&IList_1_RemoveAt_m48360_MethodInfo,
	&IList_1_get_Item_m48356_MethodInfo,
	&IList_1_set_Item_m48357_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8504_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8503_il2cpp_TypeInfo,
	&IEnumerable_1_t8505_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8504_0_0_0;
extern Il2CppType IList_1_t8504_1_0_0;
struct IList_1_t8504;
extern Il2CppGenericClass IList_1_t8504_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8504_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8504_MethodInfos/* methods */
	, IList_1_t8504_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8504_il2cpp_TypeInfo/* element_class */
	, IList_1_t8504_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8504_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8504_0_0_0/* byval_arg */
	, &IList_1_t8504_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8504_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6714_il2cpp_TypeInfo;

// System.ComponentModel.TypeConverterAttribute
#include "System_System_ComponentModel_TypeConverterAttribute.h"


// T System.Collections.Generic.IEnumerator`1<System.ComponentModel.TypeConverterAttribute>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.ComponentModel.TypeConverterAttribute>
extern MethodInfo IEnumerator_1_get_Current_m48361_MethodInfo;
static PropertyInfo IEnumerator_1_t6714____Current_PropertyInfo = 
{
	&IEnumerator_1_t6714_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48361_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6714_PropertyInfos[] =
{
	&IEnumerator_1_t6714____Current_PropertyInfo,
	NULL
};
extern Il2CppType TypeConverterAttribute_t1314_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48361_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.ComponentModel.TypeConverterAttribute>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48361_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6714_il2cpp_TypeInfo/* declaring_type */
	, &TypeConverterAttribute_t1314_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48361_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6714_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48361_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6714_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6714_0_0_0;
extern Il2CppType IEnumerator_1_t6714_1_0_0;
struct IEnumerator_1_t6714;
extern Il2CppGenericClass IEnumerator_1_t6714_GenericClass;
TypeInfo IEnumerator_1_t6714_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6714_MethodInfos/* methods */
	, IEnumerator_1_t6714_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6714_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6714_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6714_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6714_0_0_0/* byval_arg */
	, &IEnumerator_1_t6714_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6714_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_486.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4847_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_486MethodDeclarations.h"

extern TypeInfo TypeConverterAttribute_t1314_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29774_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisTypeConverterAttribute_t1314_m38187_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.ComponentModel.TypeConverterAttribute>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.ComponentModel.TypeConverterAttribute>(System.Int32)
#define Array_InternalArray__get_Item_TisTypeConverterAttribute_t1314_m38187(__this, p0, method) (TypeConverterAttribute_t1314 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::MoveNext()
// T System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4847____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4847_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4847, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4847____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4847_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4847, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4847_FieldInfos[] =
{
	&InternalEnumerator_1_t4847____array_0_FieldInfo,
	&InternalEnumerator_1_t4847____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29771_MethodInfo;
static PropertyInfo InternalEnumerator_1_t4847____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4847_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29771_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4847____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4847_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29774_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4847_PropertyInfos[] =
{
	&InternalEnumerator_1_t4847____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4847____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4847_InternalEnumerator_1__ctor_m29770_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29770_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29770_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t4847_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4847_InternalEnumerator_1__ctor_m29770_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29770_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29771_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29771_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t4847_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29771_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29772_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29772_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t4847_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29772_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29773_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29773_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t4847_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29773_GenericMethod/* genericMethod */

};
extern Il2CppType TypeConverterAttribute_t1314_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29774_GenericMethod;
// T System.Array/InternalEnumerator`1<System.ComponentModel.TypeConverterAttribute>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29774_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t4847_il2cpp_TypeInfo/* declaring_type */
	, &TypeConverterAttribute_t1314_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29774_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4847_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29770_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29771_MethodInfo,
	&InternalEnumerator_1_Dispose_m29772_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29773_MethodInfo,
	&InternalEnumerator_1_get_Current_m29774_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m29773_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m29772_MethodInfo;
static MethodInfo* InternalEnumerator_1_t4847_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29771_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29773_MethodInfo,
	&InternalEnumerator_1_Dispose_m29772_MethodInfo,
	&InternalEnumerator_1_get_Current_m29774_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4847_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6714_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4847_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6714_il2cpp_TypeInfo, 7},
};
extern TypeInfo TypeConverterAttribute_t1314_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t4847_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m29774_MethodInfo/* Method Usage */,
	&TypeConverterAttribute_t1314_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisTypeConverterAttribute_t1314_m38187_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4847_0_0_0;
extern Il2CppType InternalEnumerator_1_t4847_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4847_GenericClass;
TypeInfo InternalEnumerator_1_t4847_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4847_MethodInfos/* methods */
	, InternalEnumerator_1_t4847_PropertyInfos/* properties */
	, InternalEnumerator_1_t4847_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4847_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4847_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4847_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4847_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4847_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4847_1_0_0/* this_arg */
	, InternalEnumerator_1_t4847_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4847_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t4847_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4847)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8506_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>
extern MethodInfo ICollection_1_get_Count_m48362_MethodInfo;
static PropertyInfo ICollection_1_t8506____Count_PropertyInfo = 
{
	&ICollection_1_t8506_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48362_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48363_MethodInfo;
static PropertyInfo ICollection_1_t8506____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8506_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48363_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8506_PropertyInfos[] =
{
	&ICollection_1_t8506____Count_PropertyInfo,
	&ICollection_1_t8506____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48362_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::get_Count()
MethodInfo ICollection_1_get_Count_m48362_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8506_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48362_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48363_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48363_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8506_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48363_GenericMethod/* genericMethod */

};
extern Il2CppType TypeConverterAttribute_t1314_0_0_0;
extern Il2CppType TypeConverterAttribute_t1314_0_0_0;
static ParameterInfo ICollection_1_t8506_ICollection_1_Add_m48364_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttribute_t1314_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48364_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Add(T)
MethodInfo ICollection_1_Add_m48364_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8506_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t8506_ICollection_1_Add_m48364_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48364_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48365_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Clear()
MethodInfo ICollection_1_Clear_m48365_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8506_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48365_GenericMethod/* genericMethod */

};
extern Il2CppType TypeConverterAttribute_t1314_0_0_0;
static ParameterInfo ICollection_1_t8506_ICollection_1_Contains_m48366_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttribute_t1314_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48366_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Contains(T)
MethodInfo ICollection_1_Contains_m48366_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8506_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8506_ICollection_1_Contains_m48366_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48366_GenericMethod/* genericMethod */

};
extern Il2CppType TypeConverterAttributeU5BU5D_t5652_0_0_0;
extern Il2CppType TypeConverterAttributeU5BU5D_t5652_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8506_ICollection_1_CopyTo_m48367_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttributeU5BU5D_t5652_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48367_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48367_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8506_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8506_ICollection_1_CopyTo_m48367_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48367_GenericMethod/* genericMethod */

};
extern Il2CppType TypeConverterAttribute_t1314_0_0_0;
static ParameterInfo ICollection_1_t8506_ICollection_1_Remove_m48368_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttribute_t1314_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48368_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.ComponentModel.TypeConverterAttribute>::Remove(T)
MethodInfo ICollection_1_Remove_m48368_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8506_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t8506_ICollection_1_Remove_m48368_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48368_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8506_MethodInfos[] =
{
	&ICollection_1_get_Count_m48362_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48363_MethodInfo,
	&ICollection_1_Add_m48364_MethodInfo,
	&ICollection_1_Clear_m48365_MethodInfo,
	&ICollection_1_Contains_m48366_MethodInfo,
	&ICollection_1_CopyTo_m48367_MethodInfo,
	&ICollection_1_Remove_m48368_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8508_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8506_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8508_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8506_0_0_0;
extern Il2CppType ICollection_1_t8506_1_0_0;
struct ICollection_1_t8506;
extern Il2CppGenericClass ICollection_1_t8506_GenericClass;
TypeInfo ICollection_1_t8506_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8506_MethodInfos/* methods */
	, ICollection_1_t8506_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8506_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8506_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8506_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8506_0_0_0/* byval_arg */
	, &ICollection_1_t8506_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8506_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ComponentModel.TypeConverterAttribute>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.ComponentModel.TypeConverterAttribute>
extern Il2CppType IEnumerator_1_t6714_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48369_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.ComponentModel.TypeConverterAttribute>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48369_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8508_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6714_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48369_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8508_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48369_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8508_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8508_0_0_0;
extern Il2CppType IEnumerable_1_t8508_1_0_0;
struct IEnumerable_1_t8508;
extern Il2CppGenericClass IEnumerable_1_t8508_GenericClass;
TypeInfo IEnumerable_1_t8508_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8508_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8508_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8508_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8508_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8508_0_0_0/* byval_arg */
	, &IEnumerable_1_t8508_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8508_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8507_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>
extern MethodInfo IList_1_get_Item_m48370_MethodInfo;
extern MethodInfo IList_1_set_Item_m48371_MethodInfo;
static PropertyInfo IList_1_t8507____Item_PropertyInfo = 
{
	&IList_1_t8507_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48370_MethodInfo/* get */
	, &IList_1_set_Item_m48371_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8507_PropertyInfos[] =
{
	&IList_1_t8507____Item_PropertyInfo,
	NULL
};
extern Il2CppType TypeConverterAttribute_t1314_0_0_0;
static ParameterInfo IList_1_t8507_IList_1_IndexOf_m48372_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttribute_t1314_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48372_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48372_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8507_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8507_IList_1_IndexOf_m48372_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48372_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TypeConverterAttribute_t1314_0_0_0;
static ParameterInfo IList_1_t8507_IList_1_Insert_m48373_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttribute_t1314_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48373_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48373_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8507_IList_1_Insert_m48373_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48373_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8507_IList_1_RemoveAt_m48374_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48374_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48374_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8507_IList_1_RemoveAt_m48374_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48374_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8507_IList_1_get_Item_m48370_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType TypeConverterAttribute_t1314_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48370_GenericMethod;
// T System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48370_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8507_il2cpp_TypeInfo/* declaring_type */
	, &TypeConverterAttribute_t1314_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t8507_IList_1_get_Item_m48370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48370_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType TypeConverterAttribute_t1314_0_0_0;
static ParameterInfo IList_1_t8507_IList_1_set_Item_m48371_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &TypeConverterAttribute_t1314_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48371_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.ComponentModel.TypeConverterAttribute>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48371_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8507_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t8507_IList_1_set_Item_m48371_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48371_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8507_MethodInfos[] =
{
	&IList_1_IndexOf_m48372_MethodInfo,
	&IList_1_Insert_m48373_MethodInfo,
	&IList_1_RemoveAt_m48374_MethodInfo,
	&IList_1_get_Item_m48370_MethodInfo,
	&IList_1_set_Item_m48371_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8507_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8506_il2cpp_TypeInfo,
	&IEnumerable_1_t8508_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8507_0_0_0;
extern Il2CppType IList_1_t8507_1_0_0;
struct IList_1_t8507;
extern Il2CppGenericClass IList_1_t8507_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8507_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8507_MethodInfos/* methods */
	, IList_1_t8507_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8507_il2cpp_TypeInfo/* element_class */
	, IList_1_t8507_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8507_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8507_0_0_0/* byval_arg */
	, &IList_1_t8507_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8507_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6716_il2cpp_TypeInfo;

// System.Net.Security.AuthenticationLevel
#include "System_System_Net_Security_AuthenticationLevel.h"


// T System.Collections.Generic.IEnumerator`1<System.Net.Security.AuthenticationLevel>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Net.Security.AuthenticationLevel>
extern MethodInfo IEnumerator_1_get_Current_m48375_MethodInfo;
static PropertyInfo IEnumerator_1_t6716____Current_PropertyInfo = 
{
	&IEnumerator_1_t6716_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48375_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6716_PropertyInfos[] =
{
	&IEnumerator_1_t6716____Current_PropertyInfo,
	NULL
};
extern Il2CppType AuthenticationLevel_t1315_0_0_0;
extern void* RuntimeInvoker_AuthenticationLevel_t1315 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48375_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Net.Security.AuthenticationLevel>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48375_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6716_il2cpp_TypeInfo/* declaring_type */
	, &AuthenticationLevel_t1315_0_0_0/* return_type */
	, RuntimeInvoker_AuthenticationLevel_t1315/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48375_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6716_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48375_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6716_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6716_0_0_0;
extern Il2CppType IEnumerator_1_t6716_1_0_0;
struct IEnumerator_1_t6716;
extern Il2CppGenericClass IEnumerator_1_t6716_GenericClass;
TypeInfo IEnumerator_1_t6716_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6716_MethodInfos/* methods */
	, IEnumerator_1_t6716_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6716_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6716_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6716_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6716_0_0_0/* byval_arg */
	, &IEnumerator_1_t6716_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6716_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_487.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4848_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_487MethodDeclarations.h"

extern TypeInfo AuthenticationLevel_t1315_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29779_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAuthenticationLevel_t1315_m38198_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Net.Security.AuthenticationLevel>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Net.Security.AuthenticationLevel>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisAuthenticationLevel_t1315_m38198 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29775_MethodInfo;
 void InternalEnumerator_1__ctor_m29775 (InternalEnumerator_1_t4848 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29776_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29776 (InternalEnumerator_1_t4848 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29779(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29779_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&AuthenticationLevel_t1315_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29777_MethodInfo;
 void InternalEnumerator_1_Dispose_m29777 (InternalEnumerator_1_t4848 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29778_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29778 (InternalEnumerator_1_t4848 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29779 (InternalEnumerator_1_t4848 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisAuthenticationLevel_t1315_m38198(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisAuthenticationLevel_t1315_m38198_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4848____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4848_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4848, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4848____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4848_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4848, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4848_FieldInfos[] =
{
	&InternalEnumerator_1_t4848____array_0_FieldInfo,
	&InternalEnumerator_1_t4848____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4848____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4848_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29776_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4848____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4848_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29779_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4848_PropertyInfos[] =
{
	&InternalEnumerator_1_t4848____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4848____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4848_InternalEnumerator_1__ctor_m29775_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29775_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29775_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29775/* method */
	, &InternalEnumerator_1_t4848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4848_InternalEnumerator_1__ctor_m29775_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29775_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29776_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29776_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29776/* method */
	, &InternalEnumerator_1_t4848_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29776_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29777_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29777_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29777/* method */
	, &InternalEnumerator_1_t4848_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29777_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29778_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29778_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29778/* method */
	, &InternalEnumerator_1_t4848_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29778_GenericMethod/* genericMethod */

};
extern Il2CppType AuthenticationLevel_t1315_0_0_0;
extern void* RuntimeInvoker_AuthenticationLevel_t1315 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29779_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Net.Security.AuthenticationLevel>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29779_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29779/* method */
	, &InternalEnumerator_1_t4848_il2cpp_TypeInfo/* declaring_type */
	, &AuthenticationLevel_t1315_0_0_0/* return_type */
	, RuntimeInvoker_AuthenticationLevel_t1315/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29779_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4848_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29775_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29776_MethodInfo,
	&InternalEnumerator_1_Dispose_m29777_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29778_MethodInfo,
	&InternalEnumerator_1_get_Current_m29779_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4848_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29776_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29778_MethodInfo,
	&InternalEnumerator_1_Dispose_m29777_MethodInfo,
	&InternalEnumerator_1_get_Current_m29779_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4848_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6716_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4848_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6716_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4848_0_0_0;
extern Il2CppType InternalEnumerator_1_t4848_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4848_GenericClass;
TypeInfo InternalEnumerator_1_t4848_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4848_MethodInfos/* methods */
	, InternalEnumerator_1_t4848_PropertyInfos/* properties */
	, InternalEnumerator_1_t4848_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4848_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4848_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4848_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4848_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4848_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4848_1_0_0/* this_arg */
	, InternalEnumerator_1_t4848_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4848_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4848)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8509_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>
extern MethodInfo ICollection_1_get_Count_m48376_MethodInfo;
static PropertyInfo ICollection_1_t8509____Count_PropertyInfo = 
{
	&ICollection_1_t8509_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48376_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48377_MethodInfo;
static PropertyInfo ICollection_1_t8509____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8509_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48377_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8509_PropertyInfos[] =
{
	&ICollection_1_t8509____Count_PropertyInfo,
	&ICollection_1_t8509____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48376_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::get_Count()
MethodInfo ICollection_1_get_Count_m48376_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8509_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48376_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48377_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48377_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8509_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48377_GenericMethod/* genericMethod */

};
extern Il2CppType AuthenticationLevel_t1315_0_0_0;
extern Il2CppType AuthenticationLevel_t1315_0_0_0;
static ParameterInfo ICollection_1_t8509_ICollection_1_Add_m48378_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevel_t1315_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48378_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Add(T)
MethodInfo ICollection_1_Add_m48378_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8509_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8509_ICollection_1_Add_m48378_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48378_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48379_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Clear()
MethodInfo ICollection_1_Clear_m48379_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8509_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48379_GenericMethod/* genericMethod */

};
extern Il2CppType AuthenticationLevel_t1315_0_0_0;
static ParameterInfo ICollection_1_t8509_ICollection_1_Contains_m48380_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevel_t1315_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48380_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Contains(T)
MethodInfo ICollection_1_Contains_m48380_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8509_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8509_ICollection_1_Contains_m48380_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48380_GenericMethod/* genericMethod */

};
extern Il2CppType AuthenticationLevelU5BU5D_t5653_0_0_0;
extern Il2CppType AuthenticationLevelU5BU5D_t5653_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8509_ICollection_1_CopyTo_m48381_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevelU5BU5D_t5653_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48381_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48381_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8509_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8509_ICollection_1_CopyTo_m48381_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48381_GenericMethod/* genericMethod */

};
extern Il2CppType AuthenticationLevel_t1315_0_0_0;
static ParameterInfo ICollection_1_t8509_ICollection_1_Remove_m48382_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevel_t1315_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48382_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.AuthenticationLevel>::Remove(T)
MethodInfo ICollection_1_Remove_m48382_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8509_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8509_ICollection_1_Remove_m48382_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48382_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8509_MethodInfos[] =
{
	&ICollection_1_get_Count_m48376_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48377_MethodInfo,
	&ICollection_1_Add_m48378_MethodInfo,
	&ICollection_1_Clear_m48379_MethodInfo,
	&ICollection_1_Contains_m48380_MethodInfo,
	&ICollection_1_CopyTo_m48381_MethodInfo,
	&ICollection_1_Remove_m48382_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8511_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8509_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8511_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8509_0_0_0;
extern Il2CppType ICollection_1_t8509_1_0_0;
struct ICollection_1_t8509;
extern Il2CppGenericClass ICollection_1_t8509_GenericClass;
TypeInfo ICollection_1_t8509_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8509_MethodInfos/* methods */
	, ICollection_1_t8509_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8509_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8509_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8509_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8509_0_0_0/* byval_arg */
	, &ICollection_1_t8509_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8509_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Net.Security.AuthenticationLevel>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Net.Security.AuthenticationLevel>
extern Il2CppType IEnumerator_1_t6716_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48383_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Net.Security.AuthenticationLevel>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48383_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8511_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6716_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48383_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8511_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48383_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8511_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8511_0_0_0;
extern Il2CppType IEnumerable_1_t8511_1_0_0;
struct IEnumerable_1_t8511;
extern Il2CppGenericClass IEnumerable_1_t8511_GenericClass;
TypeInfo IEnumerable_1_t8511_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8511_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8511_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8511_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8511_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8511_0_0_0/* byval_arg */
	, &IEnumerable_1_t8511_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8511_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8510_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>
extern MethodInfo IList_1_get_Item_m48384_MethodInfo;
extern MethodInfo IList_1_set_Item_m48385_MethodInfo;
static PropertyInfo IList_1_t8510____Item_PropertyInfo = 
{
	&IList_1_t8510_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48384_MethodInfo/* get */
	, &IList_1_set_Item_m48385_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8510_PropertyInfos[] =
{
	&IList_1_t8510____Item_PropertyInfo,
	NULL
};
extern Il2CppType AuthenticationLevel_t1315_0_0_0;
static ParameterInfo IList_1_t8510_IList_1_IndexOf_m48386_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevel_t1315_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48386_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48386_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8510_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8510_IList_1_IndexOf_m48386_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48386_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AuthenticationLevel_t1315_0_0_0;
static ParameterInfo IList_1_t8510_IList_1_Insert_m48387_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevel_t1315_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48387_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48387_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8510_IList_1_Insert_m48387_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48387_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8510_IList_1_RemoveAt_m48388_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48388_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48388_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8510_IList_1_RemoveAt_m48388_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48388_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8510_IList_1_get_Item_m48384_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AuthenticationLevel_t1315_0_0_0;
extern void* RuntimeInvoker_AuthenticationLevel_t1315_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48384_GenericMethod;
// T System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48384_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8510_il2cpp_TypeInfo/* declaring_type */
	, &AuthenticationLevel_t1315_0_0_0/* return_type */
	, RuntimeInvoker_AuthenticationLevel_t1315_Int32_t93/* invoker_method */
	, IList_1_t8510_IList_1_get_Item_m48384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48384_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AuthenticationLevel_t1315_0_0_0;
static ParameterInfo IList_1_t8510_IList_1_set_Item_m48385_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AuthenticationLevel_t1315_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48385_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Net.Security.AuthenticationLevel>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48385_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8510_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8510_IList_1_set_Item_m48385_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48385_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8510_MethodInfos[] =
{
	&IList_1_IndexOf_m48386_MethodInfo,
	&IList_1_Insert_m48387_MethodInfo,
	&IList_1_RemoveAt_m48388_MethodInfo,
	&IList_1_get_Item_m48384_MethodInfo,
	&IList_1_set_Item_m48385_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8510_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8509_il2cpp_TypeInfo,
	&IEnumerable_1_t8511_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8510_0_0_0;
extern Il2CppType IList_1_t8510_1_0_0;
struct IList_1_t8510;
extern Il2CppGenericClass IList_1_t8510_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8510_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8510_MethodInfos/* methods */
	, IList_1_t8510_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8510_il2cpp_TypeInfo/* element_class */
	, IList_1_t8510_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8510_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8510_0_0_0/* byval_arg */
	, &IList_1_t8510_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8510_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6718_il2cpp_TypeInfo;

// System.Net.Security.SslPolicyErrors
#include "System_System_Net_Security_SslPolicyErrors.h"


// T System.Collections.Generic.IEnumerator`1<System.Net.Security.SslPolicyErrors>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Net.Security.SslPolicyErrors>
extern MethodInfo IEnumerator_1_get_Current_m48389_MethodInfo;
static PropertyInfo IEnumerator_1_t6718____Current_PropertyInfo = 
{
	&IEnumerator_1_t6718_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48389_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6718_PropertyInfos[] =
{
	&IEnumerator_1_t6718____Current_PropertyInfo,
	NULL
};
extern Il2CppType SslPolicyErrors_t1316_0_0_0;
extern void* RuntimeInvoker_SslPolicyErrors_t1316 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48389_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Net.Security.SslPolicyErrors>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48389_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6718_il2cpp_TypeInfo/* declaring_type */
	, &SslPolicyErrors_t1316_0_0_0/* return_type */
	, RuntimeInvoker_SslPolicyErrors_t1316/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48389_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6718_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48389_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6718_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6718_0_0_0;
extern Il2CppType IEnumerator_1_t6718_1_0_0;
struct IEnumerator_1_t6718;
extern Il2CppGenericClass IEnumerator_1_t6718_GenericClass;
TypeInfo IEnumerator_1_t6718_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6718_MethodInfos/* methods */
	, IEnumerator_1_t6718_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6718_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6718_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6718_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6718_0_0_0/* byval_arg */
	, &IEnumerator_1_t6718_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6718_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_488.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4849_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_488MethodDeclarations.h"

extern TypeInfo SslPolicyErrors_t1316_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29784_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisSslPolicyErrors_t1316_m38209_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Net.Security.SslPolicyErrors>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Net.Security.SslPolicyErrors>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisSslPolicyErrors_t1316_m38209 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29780_MethodInfo;
 void InternalEnumerator_1__ctor_m29780 (InternalEnumerator_1_t4849 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29781_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29781 (InternalEnumerator_1_t4849 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29784(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29784_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&SslPolicyErrors_t1316_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29782_MethodInfo;
 void InternalEnumerator_1_Dispose_m29782 (InternalEnumerator_1_t4849 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29783_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29783 (InternalEnumerator_1_t4849 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29784 (InternalEnumerator_1_t4849 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisSslPolicyErrors_t1316_m38209(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisSslPolicyErrors_t1316_m38209_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4849____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4849_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4849, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4849____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4849_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4849, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4849_FieldInfos[] =
{
	&InternalEnumerator_1_t4849____array_0_FieldInfo,
	&InternalEnumerator_1_t4849____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4849____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4849_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29781_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4849____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4849_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29784_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4849_PropertyInfos[] =
{
	&InternalEnumerator_1_t4849____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4849____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4849_InternalEnumerator_1__ctor_m29780_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29780_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29780_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29780/* method */
	, &InternalEnumerator_1_t4849_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4849_InternalEnumerator_1__ctor_m29780_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29780_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29781_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29781_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29781/* method */
	, &InternalEnumerator_1_t4849_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29781_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29782_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29782_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29782/* method */
	, &InternalEnumerator_1_t4849_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29782_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29783_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29783_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29783/* method */
	, &InternalEnumerator_1_t4849_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29783_GenericMethod/* genericMethod */

};
extern Il2CppType SslPolicyErrors_t1316_0_0_0;
extern void* RuntimeInvoker_SslPolicyErrors_t1316 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29784_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Net.Security.SslPolicyErrors>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29784_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29784/* method */
	, &InternalEnumerator_1_t4849_il2cpp_TypeInfo/* declaring_type */
	, &SslPolicyErrors_t1316_0_0_0/* return_type */
	, RuntimeInvoker_SslPolicyErrors_t1316/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29784_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4849_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29780_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29781_MethodInfo,
	&InternalEnumerator_1_Dispose_m29782_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29783_MethodInfo,
	&InternalEnumerator_1_get_Current_m29784_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4849_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29781_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29783_MethodInfo,
	&InternalEnumerator_1_Dispose_m29782_MethodInfo,
	&InternalEnumerator_1_get_Current_m29784_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4849_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6718_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4849_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6718_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4849_0_0_0;
extern Il2CppType InternalEnumerator_1_t4849_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4849_GenericClass;
TypeInfo InternalEnumerator_1_t4849_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4849_MethodInfos/* methods */
	, InternalEnumerator_1_t4849_PropertyInfos/* properties */
	, InternalEnumerator_1_t4849_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4849_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4849_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4849_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4849_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4849_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4849_1_0_0/* this_arg */
	, InternalEnumerator_1_t4849_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4849_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4849)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8512_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>
extern MethodInfo ICollection_1_get_Count_m48390_MethodInfo;
static PropertyInfo ICollection_1_t8512____Count_PropertyInfo = 
{
	&ICollection_1_t8512_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48390_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48391_MethodInfo;
static PropertyInfo ICollection_1_t8512____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8512_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48391_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8512_PropertyInfos[] =
{
	&ICollection_1_t8512____Count_PropertyInfo,
	&ICollection_1_t8512____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48390_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::get_Count()
MethodInfo ICollection_1_get_Count_m48390_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8512_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48390_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48391_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48391_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8512_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48391_GenericMethod/* genericMethod */

};
extern Il2CppType SslPolicyErrors_t1316_0_0_0;
extern Il2CppType SslPolicyErrors_t1316_0_0_0;
static ParameterInfo ICollection_1_t8512_ICollection_1_Add_m48392_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SslPolicyErrors_t1316_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48392_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Add(T)
MethodInfo ICollection_1_Add_m48392_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8512_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8512_ICollection_1_Add_m48392_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48392_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48393_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Clear()
MethodInfo ICollection_1_Clear_m48393_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8512_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48393_GenericMethod/* genericMethod */

};
extern Il2CppType SslPolicyErrors_t1316_0_0_0;
static ParameterInfo ICollection_1_t8512_ICollection_1_Contains_m48394_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SslPolicyErrors_t1316_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48394_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Contains(T)
MethodInfo ICollection_1_Contains_m48394_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8512_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8512_ICollection_1_Contains_m48394_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48394_GenericMethod/* genericMethod */

};
extern Il2CppType SslPolicyErrorsU5BU5D_t5654_0_0_0;
extern Il2CppType SslPolicyErrorsU5BU5D_t5654_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8512_ICollection_1_CopyTo_m48395_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &SslPolicyErrorsU5BU5D_t5654_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48395_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48395_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8512_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8512_ICollection_1_CopyTo_m48395_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48395_GenericMethod/* genericMethod */

};
extern Il2CppType SslPolicyErrors_t1316_0_0_0;
static ParameterInfo ICollection_1_t8512_ICollection_1_Remove_m48396_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SslPolicyErrors_t1316_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48396_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Security.SslPolicyErrors>::Remove(T)
MethodInfo ICollection_1_Remove_m48396_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8512_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8512_ICollection_1_Remove_m48396_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48396_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8512_MethodInfos[] =
{
	&ICollection_1_get_Count_m48390_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48391_MethodInfo,
	&ICollection_1_Add_m48392_MethodInfo,
	&ICollection_1_Clear_m48393_MethodInfo,
	&ICollection_1_Contains_m48394_MethodInfo,
	&ICollection_1_CopyTo_m48395_MethodInfo,
	&ICollection_1_Remove_m48396_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8514_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8512_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8514_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8512_0_0_0;
extern Il2CppType ICollection_1_t8512_1_0_0;
struct ICollection_1_t8512;
extern Il2CppGenericClass ICollection_1_t8512_GenericClass;
TypeInfo ICollection_1_t8512_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8512_MethodInfos/* methods */
	, ICollection_1_t8512_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8512_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8512_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8512_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8512_0_0_0/* byval_arg */
	, &ICollection_1_t8512_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8512_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Net.Security.SslPolicyErrors>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Net.Security.SslPolicyErrors>
extern Il2CppType IEnumerator_1_t6718_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48397_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Net.Security.SslPolicyErrors>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48397_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8514_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6718_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48397_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8514_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48397_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8514_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8514_0_0_0;
extern Il2CppType IEnumerable_1_t8514_1_0_0;
struct IEnumerable_1_t8514;
extern Il2CppGenericClass IEnumerable_1_t8514_GenericClass;
TypeInfo IEnumerable_1_t8514_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8514_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8514_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8514_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8514_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8514_0_0_0/* byval_arg */
	, &IEnumerable_1_t8514_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8514_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8513_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Net.Security.SslPolicyErrors>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Net.Security.SslPolicyErrors>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Net.Security.SslPolicyErrors>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Net.Security.SslPolicyErrors>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Net.Security.SslPolicyErrors>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Net.Security.SslPolicyErrors>
extern MethodInfo IList_1_get_Item_m48398_MethodInfo;
extern MethodInfo IList_1_set_Item_m48399_MethodInfo;
static PropertyInfo IList_1_t8513____Item_PropertyInfo = 
{
	&IList_1_t8513_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48398_MethodInfo/* get */
	, &IList_1_set_Item_m48399_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8513_PropertyInfos[] =
{
	&IList_1_t8513____Item_PropertyInfo,
	NULL
};
extern Il2CppType SslPolicyErrors_t1316_0_0_0;
static ParameterInfo IList_1_t8513_IList_1_IndexOf_m48400_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &SslPolicyErrors_t1316_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48400_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Net.Security.SslPolicyErrors>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48400_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8513_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8513_IList_1_IndexOf_m48400_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48400_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SslPolicyErrors_t1316_0_0_0;
static ParameterInfo IList_1_t8513_IList_1_Insert_m48401_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &SslPolicyErrors_t1316_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48401_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Net.Security.SslPolicyErrors>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48401_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8513_IList_1_Insert_m48401_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48401_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8513_IList_1_RemoveAt_m48402_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48402_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Net.Security.SslPolicyErrors>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48402_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8513_IList_1_RemoveAt_m48402_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48402_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8513_IList_1_get_Item_m48398_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType SslPolicyErrors_t1316_0_0_0;
extern void* RuntimeInvoker_SslPolicyErrors_t1316_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48398_GenericMethod;
// T System.Collections.Generic.IList`1<System.Net.Security.SslPolicyErrors>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48398_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8513_il2cpp_TypeInfo/* declaring_type */
	, &SslPolicyErrors_t1316_0_0_0/* return_type */
	, RuntimeInvoker_SslPolicyErrors_t1316_Int32_t93/* invoker_method */
	, IList_1_t8513_IList_1_get_Item_m48398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48398_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType SslPolicyErrors_t1316_0_0_0;
static ParameterInfo IList_1_t8513_IList_1_set_Item_m48399_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &SslPolicyErrors_t1316_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48399_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Net.Security.SslPolicyErrors>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48399_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8513_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8513_IList_1_set_Item_m48399_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48399_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8513_MethodInfos[] =
{
	&IList_1_IndexOf_m48400_MethodInfo,
	&IList_1_Insert_m48401_MethodInfo,
	&IList_1_RemoveAt_m48402_MethodInfo,
	&IList_1_get_Item_m48398_MethodInfo,
	&IList_1_set_Item_m48399_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8513_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8512_il2cpp_TypeInfo,
	&IEnumerable_1_t8514_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8513_0_0_0;
extern Il2CppType IList_1_t8513_1_0_0;
struct IList_1_t8513;
extern Il2CppGenericClass IList_1_t8513_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8513_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8513_MethodInfos/* methods */
	, IList_1_t8513_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8513_il2cpp_TypeInfo/* element_class */
	, IList_1_t8513_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8513_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8513_0_0_0/* byval_arg */
	, &IList_1_t8513_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8513_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6720_il2cpp_TypeInfo;

// System.Net.Sockets.AddressFamily
#include "System_System_Net_Sockets_AddressFamily.h"


// T System.Collections.Generic.IEnumerator`1<System.Net.Sockets.AddressFamily>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.Net.Sockets.AddressFamily>
extern MethodInfo IEnumerator_1_get_Current_m48403_MethodInfo;
static PropertyInfo IEnumerator_1_t6720____Current_PropertyInfo = 
{
	&IEnumerator_1_t6720_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48403_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6720_PropertyInfos[] =
{
	&IEnumerator_1_t6720____Current_PropertyInfo,
	NULL
};
extern Il2CppType AddressFamily_t1317_0_0_0;
extern void* RuntimeInvoker_AddressFamily_t1317 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48403_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.Net.Sockets.AddressFamily>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48403_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6720_il2cpp_TypeInfo/* declaring_type */
	, &AddressFamily_t1317_0_0_0/* return_type */
	, RuntimeInvoker_AddressFamily_t1317/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48403_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6720_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48403_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6720_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6720_0_0_0;
extern Il2CppType IEnumerator_1_t6720_1_0_0;
struct IEnumerator_1_t6720;
extern Il2CppGenericClass IEnumerator_1_t6720_GenericClass;
TypeInfo IEnumerator_1_t6720_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6720_MethodInfos/* methods */
	, IEnumerator_1_t6720_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6720_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6720_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6720_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6720_0_0_0/* byval_arg */
	, &IEnumerator_1_t6720_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6720_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_489.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4850_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_489MethodDeclarations.h"

extern TypeInfo AddressFamily_t1317_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29789_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisAddressFamily_t1317_m38220_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Net.Sockets.AddressFamily>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Net.Sockets.AddressFamily>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisAddressFamily_t1317_m38220 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29785_MethodInfo;
 void InternalEnumerator_1__ctor_m29785 (InternalEnumerator_1_t4850 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29786_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29786 (InternalEnumerator_1_t4850 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29789(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29789_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&AddressFamily_t1317_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29787_MethodInfo;
 void InternalEnumerator_1_Dispose_m29787 (InternalEnumerator_1_t4850 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29788_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29788 (InternalEnumerator_1_t4850 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29789 (InternalEnumerator_1_t4850 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisAddressFamily_t1317_m38220(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisAddressFamily_t1317_m38220_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4850____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4850_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4850, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4850____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4850_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4850, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4850_FieldInfos[] =
{
	&InternalEnumerator_1_t4850____array_0_FieldInfo,
	&InternalEnumerator_1_t4850____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4850____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4850_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29786_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4850____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4850_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29789_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4850_PropertyInfos[] =
{
	&InternalEnumerator_1_t4850____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4850____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4850_InternalEnumerator_1__ctor_m29785_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29785_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29785_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29785/* method */
	, &InternalEnumerator_1_t4850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4850_InternalEnumerator_1__ctor_m29785_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29785_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29786_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29786_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29786/* method */
	, &InternalEnumerator_1_t4850_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29786_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29787_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29787_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29787/* method */
	, &InternalEnumerator_1_t4850_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29787_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29788_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29788_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29788/* method */
	, &InternalEnumerator_1_t4850_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29788_GenericMethod/* genericMethod */

};
extern Il2CppType AddressFamily_t1317_0_0_0;
extern void* RuntimeInvoker_AddressFamily_t1317 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29789_GenericMethod;
// T System.Array/InternalEnumerator`1<System.Net.Sockets.AddressFamily>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29789_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29789/* method */
	, &InternalEnumerator_1_t4850_il2cpp_TypeInfo/* declaring_type */
	, &AddressFamily_t1317_0_0_0/* return_type */
	, RuntimeInvoker_AddressFamily_t1317/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29789_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4850_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29785_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29786_MethodInfo,
	&InternalEnumerator_1_Dispose_m29787_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29788_MethodInfo,
	&InternalEnumerator_1_get_Current_m29789_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4850_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29786_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29788_MethodInfo,
	&InternalEnumerator_1_Dispose_m29787_MethodInfo,
	&InternalEnumerator_1_get_Current_m29789_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4850_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6720_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4850_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6720_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4850_0_0_0;
extern Il2CppType InternalEnumerator_1_t4850_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4850_GenericClass;
TypeInfo InternalEnumerator_1_t4850_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4850_MethodInfos/* methods */
	, InternalEnumerator_1_t4850_PropertyInfos/* properties */
	, InternalEnumerator_1_t4850_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4850_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4850_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4850_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4850_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4850_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4850_1_0_0/* this_arg */
	, InternalEnumerator_1_t4850_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4850_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4850)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t8515_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>
extern MethodInfo ICollection_1_get_Count_m48404_MethodInfo;
static PropertyInfo ICollection_1_t8515____Count_PropertyInfo = 
{
	&ICollection_1_t8515_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m48404_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m48405_MethodInfo;
static PropertyInfo ICollection_1_t8515____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t8515_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m48405_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t8515_PropertyInfos[] =
{
	&ICollection_1_t8515____Count_PropertyInfo,
	&ICollection_1_t8515____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m48404_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::get_Count()
MethodInfo ICollection_1_get_Count_m48404_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t8515_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m48404_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m48405_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m48405_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t8515_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m48405_GenericMethod/* genericMethod */

};
extern Il2CppType AddressFamily_t1317_0_0_0;
extern Il2CppType AddressFamily_t1317_0_0_0;
static ParameterInfo ICollection_1_t8515_ICollection_1_Add_m48406_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AddressFamily_t1317_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m48406_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::Add(T)
MethodInfo ICollection_1_Add_m48406_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t8515_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, ICollection_1_t8515_ICollection_1_Add_m48406_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m48406_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m48407_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::Clear()
MethodInfo ICollection_1_Clear_m48407_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t8515_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m48407_GenericMethod/* genericMethod */

};
extern Il2CppType AddressFamily_t1317_0_0_0;
static ParameterInfo ICollection_1_t8515_ICollection_1_Contains_m48408_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AddressFamily_t1317_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m48408_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::Contains(T)
MethodInfo ICollection_1_Contains_m48408_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t8515_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8515_ICollection_1_Contains_m48408_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m48408_GenericMethod/* genericMethod */

};
extern Il2CppType AddressFamilyU5BU5D_t5655_0_0_0;
extern Il2CppType AddressFamilyU5BU5D_t5655_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t8515_ICollection_1_CopyTo_m48409_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &AddressFamilyU5BU5D_t5655_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m48409_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m48409_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t8515_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t8515_ICollection_1_CopyTo_m48409_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m48409_GenericMethod/* genericMethod */

};
extern Il2CppType AddressFamily_t1317_0_0_0;
static ParameterInfo ICollection_1_t8515_ICollection_1_Remove_m48410_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AddressFamily_t1317_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m48410_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<System.Net.Sockets.AddressFamily>::Remove(T)
MethodInfo ICollection_1_Remove_m48410_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t8515_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Int32_t93/* invoker_method */
	, ICollection_1_t8515_ICollection_1_Remove_m48410_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m48410_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t8515_MethodInfos[] =
{
	&ICollection_1_get_Count_m48404_MethodInfo,
	&ICollection_1_get_IsReadOnly_m48405_MethodInfo,
	&ICollection_1_Add_m48406_MethodInfo,
	&ICollection_1_Clear_m48407_MethodInfo,
	&ICollection_1_Contains_m48408_MethodInfo,
	&ICollection_1_CopyTo_m48409_MethodInfo,
	&ICollection_1_Remove_m48410_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t8517_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t8515_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t8517_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t8515_0_0_0;
extern Il2CppType ICollection_1_t8515_1_0_0;
struct ICollection_1_t8515;
extern Il2CppGenericClass ICollection_1_t8515_GenericClass;
TypeInfo ICollection_1_t8515_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t8515_MethodInfos/* methods */
	, ICollection_1_t8515_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t8515_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t8515_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t8515_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t8515_0_0_0/* byval_arg */
	, &ICollection_1_t8515_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t8515_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Net.Sockets.AddressFamily>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<System.Net.Sockets.AddressFamily>
extern Il2CppType IEnumerator_1_t6720_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m48411_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<System.Net.Sockets.AddressFamily>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m48411_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t8517_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t6720_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m48411_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t8517_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m48411_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t8517_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t8517_0_0_0;
extern Il2CppType IEnumerable_1_t8517_1_0_0;
struct IEnumerable_1_t8517;
extern Il2CppGenericClass IEnumerable_1_t8517_GenericClass;
TypeInfo IEnumerable_1_t8517_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t8517_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t8517_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t8517_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t8517_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t8517_0_0_0/* byval_arg */
	, &IEnumerable_1_t8517_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t8517_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t8516_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<System.Net.Sockets.AddressFamily>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<System.Net.Sockets.AddressFamily>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<System.Net.Sockets.AddressFamily>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<System.Net.Sockets.AddressFamily>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<System.Net.Sockets.AddressFamily>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<System.Net.Sockets.AddressFamily>
extern MethodInfo IList_1_get_Item_m48412_MethodInfo;
extern MethodInfo IList_1_set_Item_m48413_MethodInfo;
static PropertyInfo IList_1_t8516____Item_PropertyInfo = 
{
	&IList_1_t8516_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m48412_MethodInfo/* get */
	, &IList_1_set_Item_m48413_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t8516_PropertyInfos[] =
{
	&IList_1_t8516____Item_PropertyInfo,
	NULL
};
extern Il2CppType AddressFamily_t1317_0_0_0;
static ParameterInfo IList_1_t8516_IList_1_IndexOf_m48414_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &AddressFamily_t1317_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m48414_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<System.Net.Sockets.AddressFamily>::IndexOf(T)
MethodInfo IList_1_IndexOf_m48414_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t8516_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8516_IList_1_IndexOf_m48414_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m48414_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AddressFamily_t1317_0_0_0;
static ParameterInfo IList_1_t8516_IList_1_Insert_m48415_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &AddressFamily_t1317_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m48415_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Net.Sockets.AddressFamily>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m48415_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t8516_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8516_IList_1_Insert_m48415_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m48415_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8516_IList_1_RemoveAt_m48416_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m48416_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Net.Sockets.AddressFamily>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m48416_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t8516_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t8516_IList_1_RemoveAt_m48416_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m48416_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t8516_IList_1_get_Item_m48412_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType AddressFamily_t1317_0_0_0;
extern void* RuntimeInvoker_AddressFamily_t1317_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m48412_GenericMethod;
// T System.Collections.Generic.IList`1<System.Net.Sockets.AddressFamily>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m48412_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t8516_il2cpp_TypeInfo/* declaring_type */
	, &AddressFamily_t1317_0_0_0/* return_type */
	, RuntimeInvoker_AddressFamily_t1317_Int32_t93/* invoker_method */
	, IList_1_t8516_IList_1_get_Item_m48412_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m48412_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType AddressFamily_t1317_0_0_0;
static ParameterInfo IList_1_t8516_IList_1_set_Item_m48413_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &AddressFamily_t1317_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m48413_GenericMethod;
// System.Void System.Collections.Generic.IList`1<System.Net.Sockets.AddressFamily>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m48413_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t8516_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Int32_t93/* invoker_method */
	, IList_1_t8516_IList_1_set_Item_m48413_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m48413_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t8516_MethodInfos[] =
{
	&IList_1_IndexOf_m48414_MethodInfo,
	&IList_1_Insert_m48415_MethodInfo,
	&IList_1_RemoveAt_m48416_MethodInfo,
	&IList_1_get_Item_m48412_MethodInfo,
	&IList_1_set_Item_m48413_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t8516_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t8515_il2cpp_TypeInfo,
	&IEnumerable_1_t8517_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t8516_0_0_0;
extern Il2CppType IList_1_t8516_1_0_0;
struct IList_1_t8516;
extern Il2CppGenericClass IList_1_t8516_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t8516_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t8516_MethodInfos/* methods */
	, IList_1_t8516_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t8516_il2cpp_TypeInfo/* element_class */
	, IList_1_t8516_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t8516_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t8516_0_0_0/* byval_arg */
	, &IList_1_t8516_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t8516_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t6722_il2cpp_TypeInfo;

// System.IO.FileAccess
#include "mscorlib_System_IO_FileAccess.h"


// T System.Collections.Generic.IEnumerator`1<System.IO.FileAccess>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<System.IO.FileAccess>
extern MethodInfo IEnumerator_1_get_Current_m48417_MethodInfo;
static PropertyInfo IEnumerator_1_t6722____Current_PropertyInfo = 
{
	&IEnumerator_1_t6722_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m48417_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t6722_PropertyInfos[] =
{
	&IEnumerator_1_t6722____Current_PropertyInfo,
	NULL
};
extern Il2CppType FileAccess_t1498_0_0_0;
extern void* RuntimeInvoker_FileAccess_t1498 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m48417_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<System.IO.FileAccess>::get_Current()
MethodInfo IEnumerator_1_get_Current_m48417_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t6722_il2cpp_TypeInfo/* declaring_type */
	, &FileAccess_t1498_0_0_0/* return_type */
	, RuntimeInvoker_FileAccess_t1498/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m48417_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t6722_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m48417_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t6722_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t6722_0_0_0;
extern Il2CppType IEnumerator_1_t6722_1_0_0;
struct IEnumerator_1_t6722;
extern Il2CppGenericClass IEnumerator_1_t6722_GenericClass;
TypeInfo IEnumerator_1_t6722_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t6722_MethodInfos/* methods */
	, IEnumerator_1_t6722_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t6722_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t6722_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t6722_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t6722_0_0_0/* byval_arg */
	, &IEnumerator_1_t6722_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t6722_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<System.IO.FileAccess>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_490.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t4851_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<System.IO.FileAccess>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_490MethodDeclarations.h"

extern TypeInfo FileAccess_t1498_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m29794_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisFileAccess_t1498_m38231_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.IO.FileAccess>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.IO.FileAccess>(System.Int32)
 int32_t Array_InternalArray__get_Item_TisFileAccess_t1498_m38231 (Array_t * __this, int32_t p0, MethodInfo* method) IL2CPP_METHOD_ATTR;


// System.Void System.Array/InternalEnumerator`1<System.IO.FileAccess>::.ctor(System.Array)
extern MethodInfo InternalEnumerator_1__ctor_m29790_MethodInfo;
 void InternalEnumerator_1__ctor_m29790 (InternalEnumerator_1_t4851 * __this, Array_t * ___array, MethodInfo* method){
	{
		__this->___array_0 = ___array;
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IO.FileAccess>::System.Collections.IEnumerator.get_Current()
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29791_MethodInfo;
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29791 (InternalEnumerator_1_t4851 * __this, MethodInfo* method){
	{
		int32_t L_0 = InternalEnumerator_1_get_Current_m29794(__this, /*hidden argument*/&InternalEnumerator_1_get_Current_m29794_MethodInfo);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(InitializedTypeInfo(&FileAccess_t1498_il2cpp_TypeInfo), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IO.FileAccess>::Dispose()
extern MethodInfo InternalEnumerator_1_Dispose_m29792_MethodInfo;
 void InternalEnumerator_1_Dispose_m29792 (InternalEnumerator_1_t4851 * __this, MethodInfo* method){
	{
		__this->___idx_1 = ((int32_t)-2);
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileAccess>::MoveNext()
extern MethodInfo InternalEnumerator_1_MoveNext_m29793_MethodInfo;
 bool InternalEnumerator_1_MoveNext_m29793 (InternalEnumerator_1_t4851 * __this, MethodInfo* method){
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_001b;
		}
	}
	{
		Array_t * L_1 = (__this->___array_0);
		NullCheck(L_1);
		int32_t L_2 = Array_get_Length_m7656(L_1, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		__this->___idx_1 = L_2;
	}

IL_001b:
	{
		int32_t L_3 = (__this->___idx_1);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_4 = (__this->___idx_1);
		int32_t L_5 = ((int32_t)(L_4-1));
		V_0 = L_5;
		__this->___idx_1 = L_5;
		G_B5_0 = ((((int32_t)((((int32_t)V_0) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B5_0 = 0;
	}

IL_003e:
	{
		return G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IO.FileAccess>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29794 (InternalEnumerator_1_t4851 * __this, MethodInfo* method){
	{
		int32_t L_0 = (__this->___idx_1);
		if ((((uint32_t)L_0) != ((uint32_t)((int32_t)-2))))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1493 * L_1 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_1, (String_t*) &_stringLiteral1291, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_1);
	}

IL_0015:
	{
		int32_t L_2 = (__this->___idx_1);
		if ((((uint32_t)L_2) != ((uint32_t)(-1))))
		{
			goto IL_0029;
		}
	}
	{
		InvalidOperationException_t1493 * L_3 = (InvalidOperationException_t1493 *)il2cpp_codegen_object_new (InitializedTypeInfo(&InvalidOperationException_t1493_il2cpp_TypeInfo));
		InvalidOperationException__ctor_m7651(L_3, (String_t*) &_stringLiteral1292, /*hidden argument*/&InvalidOperationException__ctor_m7651_MethodInfo);
		il2cpp_codegen_raise_exception(L_3);
	}

IL_0029:
	{
		Array_t * L_4 = (__this->___array_0);
		Array_t * L_5 = (__this->___array_0);
		NullCheck(L_5);
		int32_t L_6 = Array_get_Length_m7656(L_5, /*hidden argument*/&Array_get_Length_m7656_MethodInfo);
		int32_t L_7 = (__this->___idx_1);
		NullCheck(L_4);
		int32_t L_8 = Array_InternalArray__get_Item_TisFileAccess_t1498_m38231(L_4, ((int32_t)(((int32_t)(L_6-1))-L_7)), /*hidden argument*/&Array_InternalArray__get_Item_TisFileAccess_t1498_m38231_MethodInfo);
		return L_8;
	}
}
// Metadata Definition System.Array/InternalEnumerator`1<System.IO.FileAccess>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t4851____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t4851_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4851, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t4851____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t4851_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t4851, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t4851_FieldInfos[] =
{
	&InternalEnumerator_1_t4851____array_0_FieldInfo,
	&InternalEnumerator_1_t4851____idx_1_FieldInfo,
	NULL
};
static PropertyInfo InternalEnumerator_1_t4851____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4851_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29791_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t4851____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t4851_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m29794_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t4851_PropertyInfos[] =
{
	&InternalEnumerator_1_t4851____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t4851____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t4851_InternalEnumerator_1__ctor_m29790_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m29790_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileAccess>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m29790_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m29790/* method */
	, &InternalEnumerator_1_t4851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t4851_InternalEnumerator_1__ctor_m29790_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m29790_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29791_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<System.IO.FileAccess>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29791_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29791/* method */
	, &InternalEnumerator_1_t4851_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29791_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m29792_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<System.IO.FileAccess>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m29792_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m29792/* method */
	, &InternalEnumerator_1_t4851_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m29792_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m29793_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<System.IO.FileAccess>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m29793_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m29793/* method */
	, &InternalEnumerator_1_t4851_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m29793_GenericMethod/* genericMethod */

};
extern Il2CppType FileAccess_t1498_0_0_0;
extern void* RuntimeInvoker_FileAccess_t1498 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m29794_GenericMethod;
// T System.Array/InternalEnumerator`1<System.IO.FileAccess>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m29794_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m29794/* method */
	, &InternalEnumerator_1_t4851_il2cpp_TypeInfo/* declaring_type */
	, &FileAccess_t1498_0_0_0/* return_type */
	, RuntimeInvoker_FileAccess_t1498/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m29794_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t4851_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m29790_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29791_MethodInfo,
	&InternalEnumerator_1_Dispose_m29792_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29793_MethodInfo,
	&InternalEnumerator_1_get_Current_m29794_MethodInfo,
	NULL
};
static MethodInfo* InternalEnumerator_1_t4851_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29791_MethodInfo,
	&InternalEnumerator_1_MoveNext_m29793_MethodInfo,
	&InternalEnumerator_1_Dispose_m29792_MethodInfo,
	&InternalEnumerator_1_get_Current_m29794_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t4851_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t6722_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t4851_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t6722_il2cpp_TypeInfo, 7},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t4851_0_0_0;
extern Il2CppType InternalEnumerator_1_t4851_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t4851_GenericClass;
TypeInfo InternalEnumerator_1_t4851_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t4851_MethodInfos/* methods */
	, InternalEnumerator_1_t4851_PropertyInfos/* properties */
	, InternalEnumerator_1_t4851_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t4851_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t4851_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t4851_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t4851_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t4851_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t4851_1_0_0/* this_arg */
	, InternalEnumerator_1_t4851_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t4851_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t4851)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
