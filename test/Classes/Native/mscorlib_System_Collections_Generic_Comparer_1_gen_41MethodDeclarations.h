﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>
struct Comparer_1_t4230;
// System.Object
struct Object_t;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
 void Comparer_1__ctor_m25635 (Comparer_1_t4230 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::.cctor()
 void Comparer_1__cctor_m25636 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IComparer.Compare(System.Object,System.Object)
 int32_t Comparer_1_System_Collections_IComparer_Compare_m25637 (Comparer_1_t4230 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.TargetFinder/TargetSearchResult>::get_Default()
 Comparer_1_t4230 * Comparer_1_get_Default_m25638 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
