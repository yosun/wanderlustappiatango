﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.VirtualButtonAbstractBehaviour>
struct Comparer_1_t4215;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.VirtualButtonAbstractBehaviour>
struct Comparer_1_t4215  : public Object_t
{
};
struct Comparer_1_t4215_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.VirtualButtonAbstractBehaviour>::_default
	Comparer_1_t4215 * ____default_0;
};
