﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// Input2
struct Input2_t14;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Input2>
struct UnityAction_1_t2729  : public MulticastDelegate_t325
{
};
