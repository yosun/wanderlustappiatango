﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>
struct KeyValuePair_2_t4508;
// UnityEngine.GUILayoutUtility/LayoutCache
struct LayoutCache_t957;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m27590 (KeyValuePair_2_t4508 * __this, int32_t ___key, LayoutCache_t957 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Key()
 int32_t KeyValuePair_2_get_Key_m27591 (KeyValuePair_2_t4508 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m27592 (KeyValuePair_2_t4508 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Value()
 LayoutCache_t957 * KeyValuePair_2_get_Value_m27593 (KeyValuePair_2_t4508 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m27594 (KeyValuePair_2_t4508 * __this, LayoutCache_t957 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::ToString()
 String_t* KeyValuePair_2_ToString_m27595 (KeyValuePair_2_t4508 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
