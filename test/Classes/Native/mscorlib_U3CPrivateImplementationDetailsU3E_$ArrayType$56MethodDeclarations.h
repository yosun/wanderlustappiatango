﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$56
struct $ArrayType$56_t2261;
struct $ArrayType$56_t2261_marshaled;

void $ArrayType$56_t2261_marshal(const $ArrayType$56_t2261& unmarshaled, $ArrayType$56_t2261_marshaled& marshaled);
void $ArrayType$56_t2261_marshal_back(const $ArrayType$56_t2261_marshaled& marshaled, $ArrayType$56_t2261& unmarshaled);
void $ArrayType$56_t2261_marshal_cleanup($ArrayType$56_t2261_marshaled& marshaled);
