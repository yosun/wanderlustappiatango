﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARRendererImpl/RenderEvent>
struct InternalEnumerator_1_t3912;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARRendererImpl/RenderEvent
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRendererImpl_Re.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARRendererImpl/RenderEvent>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m22511 (InternalEnumerator_1_t3912 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARRendererImpl/RenderEvent>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22512 (InternalEnumerator_1_t3912 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARRendererImpl/RenderEvent>::Dispose()
 void InternalEnumerator_1_Dispose_m22513 (InternalEnumerator_1_t3912 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARRendererImpl/RenderEvent>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m22514 (InternalEnumerator_1_t3912 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.QCARRendererImpl/RenderEvent>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m22515 (InternalEnumerator_1_t3912 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
