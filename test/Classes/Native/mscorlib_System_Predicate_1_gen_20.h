﻿#pragma once
#include <stdint.h>
// UnityEngine.RectTransform
struct RectTransform_t287;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.RectTransform>
struct Predicate_1_t3578  : public MulticastDelegate_t325
{
};
