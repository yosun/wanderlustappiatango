﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.TouchScreenKeyboardType>
struct InternalEnumerator_1_t4558;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.TouchScreenKeyboardType
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.TouchScreenKeyboardType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m27990 (InternalEnumerator_1_t4558 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TouchScreenKeyboardType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27991 (InternalEnumerator_1_t4558 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TouchScreenKeyboardType>::Dispose()
 void InternalEnumerator_1_Dispose_m27992 (InternalEnumerator_1_t4558 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TouchScreenKeyboardType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m27993 (InternalEnumerator_1_t4558 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.TouchScreenKeyboardType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m27994 (InternalEnumerator_1_t4558 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
