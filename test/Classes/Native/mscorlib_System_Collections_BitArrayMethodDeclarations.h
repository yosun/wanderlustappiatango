﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.BitArray
struct BitArray_t1426;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;

// System.Void System.Collections.BitArray::.ctor(System.Int32)
 void BitArray__ctor_m7955 (BitArray_t1426 * __this, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.Collections.BitArray::getByte(System.Int32)
 uint8_t BitArray_getByte_m10333 (BitArray_t1426 * __this, int32_t ___byteIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.BitArray::get_Count()
 int32_t BitArray_get_Count_m10334 (BitArray_t1426 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray::get_IsSynchronized()
 bool BitArray_get_IsSynchronized_m10335 (BitArray_t1426 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray::get_Item(System.Int32)
 bool BitArray_get_Item_m7936 (BitArray_t1426 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::set_Item(System.Int32,System.Boolean)
 void BitArray_set_Item_m7956 (BitArray_t1426 * __this, int32_t ___index, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.BitArray::get_Length()
 int32_t BitArray_get_Length_m7935 (BitArray_t1426 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.BitArray::get_SyncRoot()
 Object_t * BitArray_get_SyncRoot_m10336 (BitArray_t1426 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::CopyTo(System.Array,System.Int32)
 void BitArray_CopyTo_m10337 (BitArray_t1426 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.BitArray::Get(System.Int32)
 bool BitArray_Get_m10338 (BitArray_t1426 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.BitArray::Set(System.Int32,System.Boolean)
 void BitArray_Set_m10339 (BitArray_t1426 * __this, int32_t ___index, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Collections.BitArray::GetEnumerator()
 Object_t * BitArray_GetEnumerator_m10340 (BitArray_t1426 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
