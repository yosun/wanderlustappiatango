﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Outline>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_85.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Outline>
struct CachedInvokableCall_1_t3635  : public InvokableCall_1_t3636
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Outline>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
