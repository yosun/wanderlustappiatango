﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerDisplayAttribute
struct DebuggerDisplayAttribute_t1844;
// System.String
struct String_t;

// System.Void System.Diagnostics.DebuggerDisplayAttribute::.ctor(System.String)
 void DebuggerDisplayAttribute__ctor_m10443 (DebuggerDisplayAttribute_t1844 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Diagnostics.DebuggerDisplayAttribute::set_Name(System.String)
 void DebuggerDisplayAttribute_set_Name_m10444 (DebuggerDisplayAttribute_t1844 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
