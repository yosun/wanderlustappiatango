﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Outline
struct Outline_t412;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t295;

// System.Void UnityEngine.UI.Outline::.ctor()
 void Outline__ctor_m1843 (Outline_t412 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Outline::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
 void Outline_ModifyVertices_m1844 (Outline_t412 * __this, List_1_t295 * ___verts, MethodInfo* method) IL2CPP_METHOD_ATTR;
