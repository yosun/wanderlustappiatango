﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t320;

// System.Void UnityEngine.UI.InputField/SubmitEvent::.ctor()
 void SubmitEvent__ctor_m1162 (SubmitEvent_t320 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
