﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Collider2D>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_146.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Collider2D>
struct CachedInvokableCall_1_t4659  : public InvokableCall_1_t4660
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Collider2D>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
