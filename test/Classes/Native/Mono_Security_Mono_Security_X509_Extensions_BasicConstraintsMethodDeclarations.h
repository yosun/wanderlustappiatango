﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.X509.Extensions.BasicConstraintsExtension
struct BasicConstraintsExtension_t1574;
// Mono.Security.X509.X509Extension
struct X509Extension_t1378;
// System.String
struct String_t;

// System.Void Mono.Security.X509.Extensions.BasicConstraintsExtension::.ctor(Mono.Security.X509.X509Extension)
 void BasicConstraintsExtension__ctor_m8244 (BasicConstraintsExtension_t1574 * __this, X509Extension_t1378 * ___extension, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.BasicConstraintsExtension::Decode()
 void BasicConstraintsExtension_Decode_m8245 (BasicConstraintsExtension_t1574 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.X509.Extensions.BasicConstraintsExtension::Encode()
 void BasicConstraintsExtension_Encode_m8246 (BasicConstraintsExtension_t1574 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mono.Security.X509.Extensions.BasicConstraintsExtension::get_CertificateAuthority()
 bool BasicConstraintsExtension_get_CertificateAuthority_m8247 (BasicConstraintsExtension_t1574 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mono.Security.X509.Extensions.BasicConstraintsExtension::ToString()
 String_t* BasicConstraintsExtension_ToString_m8248 (BasicConstraintsExtension_t1574 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
