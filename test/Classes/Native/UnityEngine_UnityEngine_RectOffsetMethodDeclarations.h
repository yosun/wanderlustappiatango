﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RectOffset
struct RectOffset_t391;
// UnityEngine.GUIStyle
struct GUIStyle_t953;
// System.String
struct String_t;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void UnityEngine.RectOffset::.ctor()
 void RectOffset__ctor_m2521 (RectOffset_t391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::.ctor(UnityEngine.GUIStyle,System.IntPtr)
 void RectOffset__ctor_m5719 (RectOffset_t391 * __this, GUIStyle_t953 * ___sourceStyle, IntPtr_t121 ___source, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
 void RectOffset__ctor_m5720 (RectOffset_t391 * __this, int32_t ___left, int32_t ___right, int32_t ___top, int32_t ___bottom, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Finalize()
 void RectOffset_Finalize_m5721 (RectOffset_t391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Init()
 void RectOffset_Init_m5722 (RectOffset_t391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Cleanup()
 void RectOffset_Cleanup_m5723 (RectOffset_t391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_left()
 int32_t RectOffset_get_left_m2501 (RectOffset_t391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
 void RectOffset_set_left_m5724 (RectOffset_t391 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_right()
 int32_t RectOffset_get_right_m5725 (RectOffset_t391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
 void RectOffset_set_right_m5726 (RectOffset_t391 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_top()
 int32_t RectOffset_get_top_m2502 (RectOffset_t391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
 void RectOffset_set_top_m5727 (RectOffset_t391 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_bottom()
 int32_t RectOffset_get_bottom_m5728 (RectOffset_t391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
 void RectOffset_set_bottom_m5729 (RectOffset_t391 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_horizontal()
 int32_t RectOffset_get_horizontal_m2495 (RectOffset_t391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_vertical()
 int32_t RectOffset_get_vertical_m2496 (RectOffset_t391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectOffset::Remove(UnityEngine.Rect)
 Rect_t118  RectOffset_Remove_m5730 (RectOffset_t391 * __this, Rect_t118  ___rect, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectOffset::INTERNAL_CALL_Remove(UnityEngine.RectOffset,UnityEngine.Rect&)
 Rect_t118  RectOffset_INTERNAL_CALL_Remove_m5731 (Object_t * __this/* static, unused */, RectOffset_t391 * ___self, Rect_t118 * ___rect, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.RectOffset::ToString()
 String_t* RectOffset_ToString_m5732 (RectOffset_t391 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
