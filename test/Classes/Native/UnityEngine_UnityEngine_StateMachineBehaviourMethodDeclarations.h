﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.StateMachineBehaviour
struct StateMachineBehaviour_t1069;
// UnityEngine.Animator
struct Animator_t356;
// UnityEngine.AnimatorStateInfo
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"

// System.Void UnityEngine.StateMachineBehaviour::.ctor()
 void StateMachineBehaviour__ctor_m6330 (StateMachineBehaviour_t1069 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
 void StateMachineBehaviour_OnStateEnter_m6331 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, AnimatorStateInfo_t1015  ___stateInfo, int32_t ___layerIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
 void StateMachineBehaviour_OnStateUpdate_m6332 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, AnimatorStateInfo_t1015  ___stateInfo, int32_t ___layerIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
 void StateMachineBehaviour_OnStateExit_m6333 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, AnimatorStateInfo_t1015  ___stateInfo, int32_t ___layerIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
 void StateMachineBehaviour_OnStateMove_m6334 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, AnimatorStateInfo_t1015  ___stateInfo, int32_t ___layerIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
 void StateMachineBehaviour_OnStateIK_m6335 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, AnimatorStateInfo_t1015  ___stateInfo, int32_t ___layerIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
 void StateMachineBehaviour_OnStateMachineEnter_m6336 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, int32_t ___stateMachinePathHash, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
 void StateMachineBehaviour_OnStateMachineExit_m6337 (StateMachineBehaviour_t1069 * __this, Animator_t356 * ___animator, int32_t ___stateMachinePathHash, MethodInfo* method) IL2CPP_METHOD_ATTR;
