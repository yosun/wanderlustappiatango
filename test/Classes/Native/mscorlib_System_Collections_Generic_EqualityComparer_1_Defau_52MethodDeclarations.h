﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VirtualButtonAbstractBehaviour>
struct DefaultComparer_t4214;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t56;

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VirtualButtonAbstractBehaviour>::.ctor()
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_0MethodDeclarations.h"
#define DefaultComparer__ctor_m25471(__this, method) (void)DefaultComparer__ctor_m14747_gshared((DefaultComparer_t2860 *)__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VirtualButtonAbstractBehaviour>::GetHashCode(T)
#define DefaultComparer_GetHashCode_m25472(__this, ___obj, method) (int32_t)DefaultComparer_GetHashCode_m14748_gshared((DefaultComparer_t2860 *)__this, (Object_t *)___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.VirtualButtonAbstractBehaviour>::Equals(T,T)
#define DefaultComparer_Equals_m25473(__this, ___x, ___y, method) (bool)DefaultComparer_Equals_m14749_gshared((DefaultComparer_t2860 *)__this, (Object_t *)___x, (Object_t *)___y, method)
