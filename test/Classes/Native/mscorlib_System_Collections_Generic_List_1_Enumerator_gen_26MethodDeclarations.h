﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>
struct Enumerator_t3010;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t183;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t182;

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m15452(__this, ___l, method) (void)Enumerator__ctor_m14640_gshared((Enumerator_t2850 *)__this, (List_1_t150 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15453(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14641_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>::Dispose()
#define Enumerator_Dispose_m15454(__this, method) (void)Enumerator_Dispose_m14642_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>::VerifyState()
#define Enumerator_VerifyState_m15455(__this, method) (void)Enumerator_VerifyState_m14643_gshared((Enumerator_t2850 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>::MoveNext()
#define Enumerator_MoveNext_m15456(__this, method) (bool)Enumerator_MoveNext_m14644_gshared((Enumerator_t2850 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.BaseInputModule>::get_Current()
#define Enumerator_get_Current_m15457(__this, method) (BaseInputModule_t183 *)Enumerator_get_Current_m14645_gshared((Enumerator_t2850 *)__this, method)
