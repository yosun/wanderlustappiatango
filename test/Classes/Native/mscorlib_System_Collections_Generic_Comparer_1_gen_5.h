﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>
struct Comparer_1_t3083;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>
struct Comparer_1_t3083  : public Object_t
{
};
struct Comparer_1_t3083_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.EventSystems.BaseRaycaster>::_default
	Comparer_1_t3083 * ____default_0;
};
