﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct Enumerator_t3332;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t295;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct Stack_1_t3329;

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor(System.Collections.Generic.Stack`1<T>)
// System.Collections.Generic.Stack`1/Enumerator<System.Object>
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"
#define Enumerator__ctor_m18164(__this, ___t, method) (void)Enumerator__ctor_m15728_gshared((Enumerator_t3036 *)__this, (Stack_1_t3035 *)___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18165(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m15729_gshared((Enumerator_t3036 *)__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Dispose()
#define Enumerator_Dispose_m18166(__this, method) (void)Enumerator_Dispose_m15730_gshared((Enumerator_t3036 *)__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::MoveNext()
#define Enumerator_MoveNext_m18167(__this, method) (bool)Enumerator_MoveNext_m15731_gshared((Enumerator_t3036 *)__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_Current()
#define Enumerator_get_Current_m18168(__this, method) (List_1_t295 *)Enumerator_get_Current_m15732_gshared((Enumerator_t3036 *)__this, method)
