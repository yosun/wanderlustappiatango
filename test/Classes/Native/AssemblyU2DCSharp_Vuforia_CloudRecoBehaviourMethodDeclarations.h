﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CloudRecoBehaviour
struct CloudRecoBehaviour_t31;

// System.Void Vuforia.CloudRecoBehaviour::.ctor()
 void CloudRecoBehaviour__ctor_m86 (CloudRecoBehaviour_t31 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
