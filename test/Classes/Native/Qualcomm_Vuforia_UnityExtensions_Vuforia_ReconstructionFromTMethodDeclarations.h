﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t74;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t72;
// Vuforia.ReconstructionFromTarget
struct ReconstructionFromTarget_t553;

// Vuforia.ReconstructionAbstractBehaviour Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionBehaviour()
 ReconstructionAbstractBehaviour_t72 * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionBehaviour_m2733 (ReconstructionFromTargetAbstractBehaviour_t74 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ReconstructionFromTarget Vuforia.ReconstructionFromTargetAbstractBehaviour::get_ReconstructionFromTarget()
 Object_t * ReconstructionFromTargetAbstractBehaviour_get_ReconstructionFromTarget_m2734 (ReconstructionFromTargetAbstractBehaviour_t74 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Awake()
 void ReconstructionFromTargetAbstractBehaviour_Awake_m2735 (ReconstructionFromTargetAbstractBehaviour_t74 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnDestroy()
 void ReconstructionFromTargetAbstractBehaviour_OnDestroy_m2736 (ReconstructionFromTargetAbstractBehaviour_t74 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::Initialize()
 void ReconstructionFromTargetAbstractBehaviour_Initialize_m2737 (ReconstructionFromTargetAbstractBehaviour_t74 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::OnTrackerStarted()
 void ReconstructionFromTargetAbstractBehaviour_OnTrackerStarted_m2738 (ReconstructionFromTargetAbstractBehaviour_t74 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionFromTargetAbstractBehaviour::.ctor()
 void ReconstructionFromTargetAbstractBehaviour__ctor_m570 (ReconstructionFromTargetAbstractBehaviour_t74 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
