﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SurfaceUtilities
struct SurfaceUtilities_t137;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"

// System.Void Vuforia.SurfaceUtilities::OnSurfaceCreated()
 void SurfaceUtilities_OnSurfaceCreated_m447 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::OnSurfaceDeinit()
 void SurfaceUtilities_OnSurfaceDeinit_m4168 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SurfaceUtilities::HasSurfaceBeenRecreated()
 bool SurfaceUtilities_HasSurfaceBeenRecreated_m442 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::OnSurfaceChanged(System.Int32,System.Int32)
 void SurfaceUtilities_OnSurfaceChanged_m4169 (Object_t * __this/* static, unused */, int32_t ___screenWidth, int32_t ___screenHeight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::SetSurfaceOrientation(UnityEngine.ScreenOrientation)
 void SurfaceUtilities_SetSurfaceOrientation_m448 (Object_t * __this/* static, unused */, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScreenOrientation Vuforia.SurfaceUtilities::GetSurfaceOrientation()
 int32_t SurfaceUtilities_GetSurfaceOrientation_m4170 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceUtilities::.cctor()
 void SurfaceUtilities__cctor_m4171 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
