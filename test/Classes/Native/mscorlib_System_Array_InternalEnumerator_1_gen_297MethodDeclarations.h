﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Color32>
struct InternalEnumerator_1_t3827;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Color32
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m21631 (InternalEnumerator_1_t3827 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21632 (InternalEnumerator_1_t3827 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
 void InternalEnumerator_1_Dispose_m21633 (InternalEnumerator_1_t3827 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m21634 (InternalEnumerator_1_t3827 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
 Color32_t415  InternalEnumerator_1_get_Current_m21635 (InternalEnumerator_1_t3827 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
