﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RC2
struct RC2_t1677;
// System.String
struct String_t;

// System.Void System.Security.Cryptography.RC2::.ctor()
 void RC2__ctor_m11958 (RC2_t1677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RC2 System.Security.Cryptography.RC2::Create()
 RC2_t1677 * RC2_Create_m8878 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Security.Cryptography.RC2 System.Security.Cryptography.RC2::Create(System.String)
 RC2_t1677 * RC2_Create_m11959 (Object_t * __this/* static, unused */, String_t* ___AlgName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RC2::get_EffectiveKeySize()
 int32_t RC2_get_EffectiveKeySize_m11960 (RC2_t1677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RC2::get_KeySize()
 int32_t RC2_get_KeySize_m11961 (RC2_t1677 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RC2::set_KeySize(System.Int32)
 void RC2_set_KeySize_m11962 (RC2_t1677 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
