﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,Vuforia.VirtualButton>
struct Transform_1_t3838;
// System.Object
struct Object_t;
// Vuforia.VirtualButton
struct VirtualButton_t595;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,Vuforia.VirtualButton>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m21742 (Transform_1_t3838 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,Vuforia.VirtualButton>::Invoke(TKey,TValue)
 VirtualButton_t595 * Transform_1_Invoke_m21743 (Transform_1_t3838 * __this, int32_t ___key, VirtualButton_t595 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,Vuforia.VirtualButton>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m21744 (Transform_1_t3838 * __this, int32_t ___key, VirtualButton_t595 * ___value, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VirtualButton,Vuforia.VirtualButton>::EndInvoke(System.IAsyncResult)
 VirtualButton_t595 * Transform_1_EndInvoke_m21745 (Transform_1_t3838 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
