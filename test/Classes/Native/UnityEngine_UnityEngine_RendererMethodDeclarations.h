﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Renderer
struct Renderer_t7;
// UnityEngine.Material
struct Material_t4;
// UnityEngine.Material[]
struct MaterialU5BU5D_t114;

// System.Boolean UnityEngine.Renderer::get_enabled()
 bool Renderer_get_enabled_m238 (Renderer_t7 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
 void Renderer_set_enabled_m416 (Renderer_t7 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Renderer::get_material()
 Material_t4 * Renderer_get_material_m239 (Renderer_t7 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
 void Renderer_set_material_m4453 (Renderer_t7 * __this, Material_t4 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
 void Renderer_set_sharedMaterial_m488 (Renderer_t7 * __this, Material_t4 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
 MaterialU5BU5D_t114* Renderer_get_materials_m308 (Renderer_t7 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
 void Renderer_set_sharedMaterials_m489 (Renderer_t7 * __this, MaterialU5BU5D_t114* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
 int32_t Renderer_get_sortingLayerID_m2031 (Renderer_t7 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
 int32_t Renderer_get_sortingOrder_m2032 (Renderer_t7 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
