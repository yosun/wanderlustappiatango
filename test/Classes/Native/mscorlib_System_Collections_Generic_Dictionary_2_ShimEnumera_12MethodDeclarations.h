﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordResult>
struct ShimEnumerator_t3973;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.WordResult>
struct Dictionary_2_t693;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m23073 (ShimEnumerator_t3973 * __this, Dictionary_2_t693 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordResult>::MoveNext()
 bool ShimEnumerator_MoveNext_m23074 (ShimEnumerator_t3973 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordResult>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m23075 (ShimEnumerator_t3973 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordResult>::get_Key()
 Object_t * ShimEnumerator_get_Key_m23076 (ShimEnumerator_t3973 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordResult>::get_Value()
 Object_t * ShimEnumerator_get_Value_m23077 (ShimEnumerator_t3973 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.WordResult>::get_Current()
 Object_t * ShimEnumerator_get_Current_m23078 (ShimEnumerator_t3973 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
