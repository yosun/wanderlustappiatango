﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t2;
// UnityEngine.Camera
struct Camera_t3;
// UnityEngine.Material
struct Material_t4;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// ARVRModes/TheCurrentMode
#include "AssemblyU2DCSharp_ARVRModes_TheCurrentMode.h"
// ARVRModes
struct ARVRModes_t5  : public MonoBehaviour_t6
{
	// UnityEngine.GameObject ARVRModes::goMaskThese
	GameObject_t2 * ___goMaskThese_2;
	// UnityEngine.GameObject ARVRModes::goARModeOnlyStuff
	GameObject_t2 * ___goARModeOnlyStuff_3;
	// UnityEngine.Camera ARVRModes::camAR
	Camera_t3 * ___camAR_4;
	// UnityEngine.Camera ARVRModes::camVR
	Camera_t3 * ___camVR_5;
	// UnityEngine.GameObject ARVRModes::goVRSet
	GameObject_t2 * ___goVRSet_6;
	// UnityEngine.Material ARVRModes::matTouchMeLipstick
	Material_t4 * ___matTouchMeLipstick_7;
	// ARVRModes/TheCurrentMode ARVRModes::tcm
	int32_t ___tcm_8;
};
