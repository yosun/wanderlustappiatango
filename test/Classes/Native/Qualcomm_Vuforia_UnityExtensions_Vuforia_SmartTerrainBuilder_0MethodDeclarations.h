﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainBuilderImpl
struct SmartTerrainBuilderImpl_t670;
// System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour>
struct IEnumerable_1_t575;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t72;
// Vuforia.Reconstruction
struct Reconstruction_t576;
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData[]
struct SmartTerrainRevisionDataU5BU5D_t671;
// Vuforia.QCARManagerImpl/SurfaceData[]
struct SurfaceDataU5BU5D_t672;
// Vuforia.QCARManagerImpl/PropData[]
struct PropDataU5BU5D_t673;

// System.Boolean Vuforia.SmartTerrainBuilderImpl::Init()
 bool SmartTerrainBuilderImpl_Init_m3051 (SmartTerrainBuilderImpl_t670 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainBuilderImpl::Deinit()
 bool SmartTerrainBuilderImpl_Deinit_m3052 (SmartTerrainBuilderImpl_t670 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.ReconstructionAbstractBehaviour> Vuforia.SmartTerrainBuilderImpl::GetReconstructions()
 Object_t* SmartTerrainBuilderImpl_GetReconstructions_m3053 (SmartTerrainBuilderImpl_t670 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainBuilderImpl::AddReconstruction(Vuforia.ReconstructionAbstractBehaviour)
 bool SmartTerrainBuilderImpl_AddReconstruction_m3054 (SmartTerrainBuilderImpl_t670 * __this, ReconstructionAbstractBehaviour_t72 * ___reconstructionBehaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainBuilderImpl::RemoveReconstruction(Vuforia.ReconstructionAbstractBehaviour)
 bool SmartTerrainBuilderImpl_RemoveReconstruction_m3055 (SmartTerrainBuilderImpl_t670 * __this, ReconstructionAbstractBehaviour_t72 * ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainBuilderImpl::DestroyReconstruction(Vuforia.Reconstruction)
 bool SmartTerrainBuilderImpl_DestroyReconstruction_m3056 (SmartTerrainBuilderImpl_t670 * __this, Object_t * ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainBuilderImpl::UpdateSmartTerrainData(Vuforia.QCARManagerImpl/SmartTerrainRevisionData[],Vuforia.QCARManagerImpl/SurfaceData[],Vuforia.QCARManagerImpl/PropData[])
 void SmartTerrainBuilderImpl_UpdateSmartTerrainData_m3057 (SmartTerrainBuilderImpl_t670 * __this, SmartTerrainRevisionDataU5BU5D_t671* ___smartTerrainRevisions, SurfaceDataU5BU5D_t672* ___updatedSurfaces, PropDataU5BU5D_t673* ___updatedProps, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainBuilderImpl::.ctor()
 void SmartTerrainBuilderImpl__ctor_m3058 (SmartTerrainBuilderImpl_t670 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
