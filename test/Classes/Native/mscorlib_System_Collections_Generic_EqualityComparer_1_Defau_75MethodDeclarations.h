﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>
struct DefaultComparer_t5132;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::.ctor()
 void DefaultComparer__ctor_m31317 (DefaultComparer_t5132 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::GetHashCode(T)
 int32_t DefaultComparer_GetHashCode_m31318 (DefaultComparer_t5132 * __this, DateTime_t110  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.DateTime>::Equals(T,T)
 bool DefaultComparer_Equals_m31319 (DefaultComparer_t5132 * __this, DateTime_t110  ___x, DateTime_t110  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
