﻿#pragma once
#include <stdint.h>
// System.Collections.Comparer
struct Comparer_t1828;
// System.Globalization.CompareInfo
struct CompareInfo_t1680;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Comparer
struct Comparer_t1828  : public Object_t
{
	// System.Globalization.CompareInfo System.Collections.Comparer::m_compareInfo
	CompareInfo_t1680 * ___m_compareInfo_2;
};
struct Comparer_t1828_StaticFields{
	// System.Collections.Comparer System.Collections.Comparer::Default
	Comparer_t1828 * ___Default_0;
	// System.Collections.Comparer System.Collections.Comparer::DefaultInvariant
	Comparer_t1828 * ___DefaultInvariant_1;
};
