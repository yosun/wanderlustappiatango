﻿#pragma once
#include <stdint.h>
// Vuforia.IEyewearComponentFactory
struct IEyewearComponentFactory_t541;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.EyewearComponentFactory
struct EyewearComponentFactory_t542  : public Object_t
{
};
struct EyewearComponentFactory_t542_StaticFields{
	// Vuforia.IEyewearComponentFactory Vuforia.EyewearComponentFactory::sInstance
	Object_t * ___sInstance_0;
};
