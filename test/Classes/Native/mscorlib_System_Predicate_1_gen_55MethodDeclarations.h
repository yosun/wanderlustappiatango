﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<UnityEngine.Events.PersistentCall>
struct Predicate_1_t4800;
// System.Object
struct Object_t;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t1083;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Predicate`1<UnityEngine.Events.PersistentCall>::.ctor(System.Object,System.IntPtr)
// System.Predicate`1<System.Object>
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"
#define Predicate_1__ctor_m29490(__this, ___object, ___method, method) (void)Predicate_1__ctor_m14750_gshared((Predicate_1_t2845 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// System.Boolean System.Predicate`1<UnityEngine.Events.PersistentCall>::Invoke(T)
#define Predicate_1_Invoke_m29491(__this, ___obj, method) (bool)Predicate_1_Invoke_m14751_gshared((Predicate_1_t2845 *)__this, (Object_t *)___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.Events.PersistentCall>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m29492(__this, ___obj, ___callback, ___object, method) (Object_t *)Predicate_1_BeginInvoke_m14752_gshared((Predicate_1_t2845 *)__this, (Object_t *)___obj, (AsyncCallback_t200 *)___callback, (Object_t *)___object, method)
// System.Boolean System.Predicate`1<UnityEngine.Events.PersistentCall>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m29493(__this, ___result, method) (bool)Predicate_1_EndInvoke_m14753_gshared((Predicate_1_t2845 *)__this, (Object_t *)___result, method)
