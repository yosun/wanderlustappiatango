﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t815;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t656;

// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
 void LinkedListNode_1__ctor_m22352 (LinkedListNode_1_t815 * __this, LinkedList_1_t656 * ___list, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
 void LinkedListNode_1__ctor_m22353 (LinkedListNode_1_t815 * __this, LinkedList_1_t656 * ___list, int32_t ___value, LinkedListNode_1_t815 * ___previousNode, LinkedListNode_1_t815 * ___nextNode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::Detach()
 void LinkedListNode_1_Detach_m22354 (LinkedListNode_1_t815 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_List()
 LinkedList_1_t656 * LinkedListNode_1_get_List_m22355 (LinkedListNode_1_t815 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Next()
 LinkedListNode_1_t815 * LinkedListNode_1_get_Next_m5237 (LinkedListNode_1_t815 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Value()
 int32_t LinkedListNode_1_get_Value_m4866 (LinkedListNode_1_t815 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
