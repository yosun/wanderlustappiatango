﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>
struct DefaultComparer_t5142;
// System.Guid
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::.ctor()
 void DefaultComparer__ctor_m31366 (DefaultComparer_t5142 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Guid>::Compare(T,T)
 int32_t DefaultComparer_Compare_m31367 (DefaultComparer_t5142 * __this, Guid_t107  ___x, Guid_t107  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
