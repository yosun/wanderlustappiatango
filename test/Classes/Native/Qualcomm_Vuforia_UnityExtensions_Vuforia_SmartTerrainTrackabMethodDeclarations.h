﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackableBehaviour
struct SmartTerrainTrackableBehaviour_t583;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t581;

// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableBehaviour::get_SmartTerrainTrackable()
 Object_t * SmartTerrainTrackableBehaviour_get_SmartTerrainTrackable_m2755 (SmartTerrainTrackableBehaviour_t583 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackableBehaviour::get_AutomaticUpdatesDisabled()
 bool SmartTerrainTrackableBehaviour_get_AutomaticUpdatesDisabled_m2756 (SmartTerrainTrackableBehaviour_t583 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::UpdateMeshAndColliders()
 void SmartTerrainTrackableBehaviour_UpdateMeshAndColliders_m582 (SmartTerrainTrackableBehaviour_t583 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::SetAutomaticUpdatesDisabled(System.Boolean)
 void SmartTerrainTrackableBehaviour_SetAutomaticUpdatesDisabled_m2757 (SmartTerrainTrackableBehaviour_t583 * __this, bool ___disabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::Start()
 void SmartTerrainTrackableBehaviour_Start_m583 (SmartTerrainTrackableBehaviour_t583 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableBehaviour::.ctor()
 void SmartTerrainTrackableBehaviour__ctor_m2758 (SmartTerrainTrackableBehaviour_t583 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
