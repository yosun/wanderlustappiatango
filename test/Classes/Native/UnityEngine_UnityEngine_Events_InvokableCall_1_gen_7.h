﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<WorldLoop>
struct UnityAction_1_t2758;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<WorldLoop>
struct InvokableCall_1_t2757  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<WorldLoop>::Delegate
	UnityAction_1_t2758 * ___Delegate_0;
};
