﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>
struct ShimEnumerator_t4169;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.TrackableBehaviour>
struct Dictionary_2_t726;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m25073 (ShimEnumerator_t4169 * __this, Dictionary_2_t726 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>::MoveNext()
 bool ShimEnumerator_MoveNext_m25074 (ShimEnumerator_t4169 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m25075 (ShimEnumerator_t4169 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>::get_Key()
 Object_t * ShimEnumerator_get_Key_m25076 (ShimEnumerator_t4169 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>::get_Value()
 Object_t * ShimEnumerator_get_Value_m25077 (ShimEnumerator_t4169 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.TrackableBehaviour>::get_Current()
 Object_t * ShimEnumerator_get_Current_m25078 (ShimEnumerator_t4169 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
