﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PremiumObjectFactory
struct PremiumObjectFactory_t637;
// Vuforia.IPremiumObjectFactory
struct IPremiumObjectFactory_t636;

// Vuforia.IPremiumObjectFactory Vuforia.PremiumObjectFactory::get_Instance()
 Object_t * PremiumObjectFactory_get_Instance_m2974 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PremiumObjectFactory::set_Instance(Vuforia.IPremiumObjectFactory)
 void PremiumObjectFactory_set_Instance_m2975 (Object_t * __this/* static, unused */, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PremiumObjectFactory::.ctor()
 void PremiumObjectFactory__ctor_m2976 (PremiumObjectFactory_t637 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
