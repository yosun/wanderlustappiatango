﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<UnityEngine.UI.Toggle>
struct List_1_t371;
// System.Object
struct Object_t;
// UnityEngine.UI.Toggle
struct Toggle_t370;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UI.Toggle>
struct IEnumerable_1_t374;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UI.Toggle>
struct IEnumerator_1_t3535;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.ICollection`1<UnityEngine.UI.Toggle>
struct ICollection_1_t3536;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.Toggle>
struct ReadOnlyCollection_1_t3537;
// UnityEngine.UI.Toggle[]
struct ToggleU5BU5D_t3534;
// System.Predicate`1<UnityEngine.UI.Toggle>
struct Predicate_1_t372;
// System.Comparison`1<UnityEngine.UI.Toggle>
struct Comparison_1_t3538;
// System.Collections.Generic.List`1/Enumerator<UnityEngine.UI.Toggle>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_42.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
#define List_1__ctor_m2465(__this, method) (void)List_1__ctor_m14543_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m19509(__this, ___collection, method) (void)List_1__ctor_m14545_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::.ctor(System.Int32)
#define List_1__ctor_m19510(__this, ___capacity, method) (void)List_1__ctor_m14547_gshared((List_1_t150 *)__this, (int32_t)___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::.cctor()
#define List_1__cctor_m19511(__this/* static, unused */, method) (void)List_1__cctor_m14549_gshared((Object_t *)__this/* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19512(__this, method) (Object_t*)List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14551_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19513(__this, ___array, ___arrayIndex, method) (void)List_1_System_Collections_ICollection_CopyTo_m14553_gshared((List_1_t150 *)__this, (Array_t *)___array, (int32_t)___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19514(__this, method) (Object_t *)List_1_System_Collections_IEnumerable_GetEnumerator_m14555_gshared((List_1_t150 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19515(__this, ___item, method) (int32_t)List_1_System_Collections_IList_Add_m14557_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19516(__this, ___item, method) (bool)List_1_System_Collections_IList_Contains_m14559_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19517(__this, ___item, method) (int32_t)List_1_System_Collections_IList_IndexOf_m14561_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19518(__this, ___index, ___item, method) (void)List_1_System_Collections_IList_Insert_m14563_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19519(__this, ___item, method) (void)List_1_System_Collections_IList_Remove_m14565_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19520(__this, method) (bool)List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14567_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19521(__this, method) (bool)List_1_System_Collections_ICollection_get_IsSynchronized_m14569_gshared((List_1_t150 *)__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19522(__this, method) (Object_t *)List_1_System_Collections_ICollection_get_SyncRoot_m14571_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19523(__this, method) (bool)List_1_System_Collections_IList_get_IsFixedSize_m14573_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19524(__this, method) (bool)List_1_System_Collections_IList_get_IsReadOnly_m14575_gshared((List_1_t150 *)__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19525(__this, ___index, method) (Object_t *)List_1_System_Collections_IList_get_Item_m14577_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19526(__this, ___index, ___value, method) (void)List_1_System_Collections_IList_set_Item_m14579_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Add(T)
#define List_1_Add_m2472(__this, ___item, method) (void)List_1_Add_m14581_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19527(__this, ___newCount, method) (void)List_1_GrowIfNeeded_m14583_gshared((List_1_t150 *)__this, (int32_t)___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19528(__this, ___collection, method) (void)List_1_AddCollection_m14585_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19529(__this, ___enumerable, method) (void)List_1_AddEnumerable_m14587_gshared((List_1_t150 *)__this, (Object_t*)___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19530(__this, ___collection, method) (void)List_1_AddRange_m14588_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::AsReadOnly()
#define List_1_AsReadOnly_m19531(__this, method) (ReadOnlyCollection_1_t3537 *)List_1_AsReadOnly_m14590_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Clear()
#define List_1_Clear_m19532(__this, method) (void)List_1_Clear_m14592_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Contains(T)
#define List_1_Contains_m2466(__this, ___item, method) (bool)List_1_Contains_m14594_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19533(__this, ___array, ___arrayIndex, method) (void)List_1_CopyTo_m14596_gshared((List_1_t150 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Find(System.Predicate`1<T>)
#define List_1_Find_m2474(__this, ___match, method) (Toggle_t370 *)List_1_Find_m14598_gshared((List_1_t150 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19534(__this/* static, unused */, ___match, method) (void)List_1_CheckMatch_m14600_gshared((Object_t *)__this/* static, unused */, (Predicate_1_t2845 *)___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19535(__this, ___startIndex, ___count, ___match, method) (int32_t)List_1_GetIndex_m14602_gshared((List_1_t150 *)__this, (int32_t)___startIndex, (int32_t)___count, (Predicate_1_t2845 *)___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::GetEnumerator()
 Enumerator_t3539  List_1_GetEnumerator_m19536 (List_1_t371 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::IndexOf(T)
#define List_1_IndexOf_m19537(__this, ___item, method) (int32_t)List_1_IndexOf_m14604_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19538(__this, ___start, ___delta, method) (void)List_1_Shift_m14606_gshared((List_1_t150 *)__this, (int32_t)___start, (int32_t)___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19539(__this, ___index, method) (void)List_1_CheckIndex_m14608_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Insert(System.Int32,T)
#define List_1_Insert_m19540(__this, ___index, ___item, method) (void)List_1_Insert_m14610_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19541(__this, ___collection, method) (void)List_1_CheckCollection_m14612_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Remove(T)
#define List_1_Remove_m2471(__this, ___item, method) (bool)List_1_Remove_m14614_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19542(__this, ___match, method) (int32_t)List_1_RemoveAll_m14616_gshared((List_1_t150 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19543(__this, ___index, method) (void)List_1_RemoveAt_m14618_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Reverse()
#define List_1_Reverse_m19544(__this, method) (void)List_1_Reverse_m14620_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Sort()
#define List_1_Sort_m19545(__this, method) (void)List_1_Sort_m14622_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19546(__this, ___comparison, method) (void)List_1_Sort_m14624_gshared((List_1_t150 *)__this, (Comparison_1_t2846 *)___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::ToArray()
#define List_1_ToArray_m19547(__this, method) (ToggleU5BU5D_t3534*)List_1_ToArray_m14626_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::TrimExcess()
#define List_1_TrimExcess_m19548(__this, method) (void)List_1_TrimExcess_m14628_gshared((List_1_t150 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::get_Capacity()
#define List_1_get_Capacity_m19549(__this, method) (int32_t)List_1_get_Capacity_m14630_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19550(__this, ___value, method) (void)List_1_set_Capacity_m14632_gshared((List_1_t150 *)__this, (int32_t)___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::get_Count()
#define List_1_get_Count_m2470(__this, method) (int32_t)List_1_get_Count_m14634_gshared((List_1_t150 *)__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::get_Item(System.Int32)
#define List_1_get_Item_m2469(__this, ___index, method) (Toggle_t370 *)List_1_get_Item_m14636_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Toggle>::set_Item(System.Int32,T)
#define List_1_set_Item_m19551(__this, ___index, ___value, method) (void)List_1_set_Item_m14638_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___value, method)
