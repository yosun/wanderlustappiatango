﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t3275;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Object>
struct KeyCollection_t3279;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Object>
struct ValueCollection_t3280;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t3225;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1066;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t3276;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t3282;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1299;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__6.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
 void Dictionary_2__ctor_m17554_gshared (Dictionary_2_t3275 * __this, MethodInfo* method);
#define Dictionary_2__ctor_m17554(__this, method) (void)Dictionary_2__ctor_m17554_gshared((Dictionary_2_t3275 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
 void Dictionary_2__ctor_m17556_gshared (Dictionary_2_t3275 * __this, Object_t* ___comparer, MethodInfo* method);
#define Dictionary_2__ctor_m17556(__this, ___comparer, method) (void)Dictionary_2__ctor_m17556_gshared((Dictionary_2_t3275 *)__this, (Object_t*)___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Int32)
 void Dictionary_2__ctor_m17558_gshared (Dictionary_2_t3275 * __this, int32_t ___capacity, MethodInfo* method);
#define Dictionary_2__ctor_m17558(__this, ___capacity, method) (void)Dictionary_2__ctor_m17558_gshared((Dictionary_2_t3275 *)__this, (int32_t)___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void Dictionary_2__ctor_m17560_gshared (Dictionary_2_t3275 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___context, MethodInfo* method);
#define Dictionary_2__ctor_m17560(__this, ___info, ___context, method) (void)Dictionary_2__ctor_m17560_gshared((Dictionary_2_t3275 *)__this, (SerializationInfo_t1066 *)___info, (StreamingContext_t1067 )___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
 Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m17562_gshared (Dictionary_2_t3275 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m17562(__this, ___key, method) (Object_t *)Dictionary_2_System_Collections_IDictionary_get_Item_m17562_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
 void Dictionary_2_System_Collections_IDictionary_set_Item_m17564_gshared (Dictionary_2_t3275 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m17564(__this, ___key, ___value, method) (void)Dictionary_2_System_Collections_IDictionary_set_Item_m17564_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
 void Dictionary_2_System_Collections_IDictionary_Add_m17566_gshared (Dictionary_2_t3275 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m17566(__this, ___key, ___value, method) (void)Dictionary_2_System_Collections_IDictionary_Add_m17566_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.Remove(System.Object)
 void Dictionary_2_System_Collections_IDictionary_Remove_m17568_gshared (Dictionary_2_t3275 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m17568(__this, ___key, method) (void)Dictionary_2_System_Collections_IDictionary_Remove_m17568_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_IsSynchronized()
 bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17570_gshared (Dictionary_2_t3275 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17570(__this, method) (bool)Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17570_gshared((Dictionary_2_t3275 *)__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.get_SyncRoot()
 Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17572_gshared (Dictionary_2_t3275 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17572(__this, method) (Object_t *)Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17572_gshared((Dictionary_2_t3275 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
 bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17574_gshared (Dictionary_2_t3275 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17574(__this, method) (bool)Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17574_gshared((Dictionary_2_t3275 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
 void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17635 (Dictionary_2_t3275 * __this, KeyValuePair_2_t3281  ___keyValuePair, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
 bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17636 (Dictionary_2_t3275 * __this, KeyValuePair_2_t3281  ___keyValuePair, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
 void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17578_gshared (Dictionary_2_t3275 * __this, KeyValuePair_2U5BU5D_t3276* ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17578(__this, ___array, ___index, method) (void)Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17578_gshared((Dictionary_2_t3275 *)__this, (KeyValuePair_2U5BU5D_t3276*)___array, (int32_t)___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
 bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17637 (Dictionary_2_t3275 * __this, KeyValuePair_2_t3281  ___keyValuePair, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
 void Dictionary_2_System_Collections_ICollection_CopyTo_m17581_gshared (Dictionary_2_t3275 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m17581(__this, ___array, ___index, method) (void)Dictionary_2_System_Collections_ICollection_CopyTo_m17581_gshared((Dictionary_2_t3275 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17583_gshared (Dictionary_2_t3275 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17583(__this, method) (Object_t *)Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17583_gshared((Dictionary_2_t3275 *)__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
 Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17585_gshared (Dictionary_2_t3275 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17585(__this, method) (Object_t*)Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17585_gshared((Dictionary_2_t3275 *)__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Object>::System.Collections.IDictionary.GetEnumerator()
 Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17587_gshared (Dictionary_2_t3275 * __this, MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17587(__this, method) (Object_t *)Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17587_gshared((Dictionary_2_t3275 *)__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Count()
 int32_t Dictionary_2_get_Count_m17589_gshared (Dictionary_2_t3275 * __this, MethodInfo* method);
#define Dictionary_2_get_Count_m17589(__this, method) (int32_t)Dictionary_2_get_Count_m17589_gshared((Dictionary_2_t3275 *)__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(TKey)
 Object_t * Dictionary_2_get_Item_m17591_gshared (Dictionary_2_t3275 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_get_Item_m17591(__this, ___key, method) (Object_t *)Dictionary_2_get_Item_m17591_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
 void Dictionary_2_set_Item_m17593_gshared (Dictionary_2_t3275 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_set_Item_m17593(__this, ___key, ___value, method) (void)Dictionary_2_set_Item_m17593_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
 void Dictionary_2_Init_m17595_gshared (Dictionary_2_t3275 * __this, int32_t ___capacity, Object_t* ___hcp, MethodInfo* method);
#define Dictionary_2_Init_m17595(__this, ___capacity, ___hcp, method) (void)Dictionary_2_Init_m17595_gshared((Dictionary_2_t3275 *)__this, (int32_t)___capacity, (Object_t*)___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::InitArrays(System.Int32)
 void Dictionary_2_InitArrays_m17597_gshared (Dictionary_2_t3275 * __this, int32_t ___size, MethodInfo* method);
#define Dictionary_2_InitArrays_m17597(__this, ___size, method) (void)Dictionary_2_InitArrays_m17597_gshared((Dictionary_2_t3275 *)__this, (int32_t)___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::CopyToCheck(System.Array,System.Int32)
 void Dictionary_2_CopyToCheck_m17599_gshared (Dictionary_2_t3275 * __this, Array_t * ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_CopyToCheck_m17599(__this, ___array, ___index, method) (void)Dictionary_2_CopyToCheck_m17599_gshared((Dictionary_2_t3275 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::make_pair(TKey,TValue)
 KeyValuePair_2_t3281  Dictionary_2_make_pair_m17638 (Object_t * __this/* static, unused */, Object_t * ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Object>::pick_key(TKey,TValue)
 Object_t * Dictionary_2_pick_key_m17602_gshared (Object_t * __this/* static, unused */, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_pick_key_m17602(__this/* static, unused */, ___key, ___value, method) (Object_t *)Dictionary_2_pick_key_m17602_gshared((Object_t *)__this/* static, unused */, (Object_t *)___key, (Object_t *)___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::pick_value(TKey,TValue)
 Object_t * Dictionary_2_pick_value_m17604_gshared (Object_t * __this/* static, unused */, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_pick_value_m17604(__this/* static, unused */, ___key, ___value, method) (Object_t *)Dictionary_2_pick_value_m17604_gshared((Object_t *)__this/* static, unused */, (Object_t *)___key, (Object_t *)___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
 void Dictionary_2_CopyTo_m17606_gshared (Dictionary_2_t3275 * __this, KeyValuePair_2U5BU5D_t3276* ___array, int32_t ___index, MethodInfo* method);
#define Dictionary_2_CopyTo_m17606(__this, ___array, ___index, method) (void)Dictionary_2_CopyTo_m17606_gshared((Dictionary_2_t3275 *)__this, (KeyValuePair_2U5BU5D_t3276*)___array, (int32_t)___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Resize()
 void Dictionary_2_Resize_m17608_gshared (Dictionary_2_t3275 * __this, MethodInfo* method);
#define Dictionary_2_Resize_m17608(__this, method) (void)Dictionary_2_Resize_m17608_gshared((Dictionary_2_t3275 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(TKey,TValue)
 void Dictionary_2_Add_m17609_gshared (Dictionary_2_t3275 * __this, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_Add_m17609(__this, ___key, ___value, method) (void)Dictionary_2_Add_m17609_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___key, (Object_t *)___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Clear()
 void Dictionary_2_Clear_m17611_gshared (Dictionary_2_t3275 * __this, MethodInfo* method);
#define Dictionary_2_Clear_m17611(__this, method) (void)Dictionary_2_Clear_m17611_gshared((Dictionary_2_t3275 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKey(TKey)
 bool Dictionary_2_ContainsKey_m17613_gshared (Dictionary_2_t3275 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_ContainsKey_m17613(__this, ___key, method) (bool)Dictionary_2_ContainsKey_m17613_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsValue(TValue)
 bool Dictionary_2_ContainsValue_m17615_gshared (Dictionary_2_t3275 * __this, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_ContainsValue_m17615(__this, ___value, method) (bool)Dictionary_2_ContainsValue_m17615_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void Dictionary_2_GetObjectData_m17617_gshared (Dictionary_2_t3275 * __this, SerializationInfo_t1066 * ___info, StreamingContext_t1067  ___context, MethodInfo* method);
#define Dictionary_2_GetObjectData_m17617(__this, ___info, ___context, method) (void)Dictionary_2_GetObjectData_m17617_gshared((Dictionary_2_t3275 *)__this, (SerializationInfo_t1066 *)___info, (StreamingContext_t1067 )___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::OnDeserialization(System.Object)
 void Dictionary_2_OnDeserialization_m17619_gshared (Dictionary_2_t3275 * __this, Object_t * ___sender, MethodInfo* method);
#define Dictionary_2_OnDeserialization_m17619(__this, ___sender, method) (void)Dictionary_2_OnDeserialization_m17619_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Remove(TKey)
 bool Dictionary_2_Remove_m17621_gshared (Dictionary_2_t3275 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_Remove_m17621(__this, ___key, method) (bool)Dictionary_2_Remove_m17621_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
 bool Dictionary_2_TryGetValue_m17622_gshared (Dictionary_2_t3275 * __this, Object_t * ___key, Object_t ** ___value, MethodInfo* method);
#define Dictionary_2_TryGetValue_m17622(__this, ___key, ___value, method) (bool)Dictionary_2_TryGetValue_m17622_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___key, (Object_t **)___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Keys()
 KeyCollection_t3279 * Dictionary_2_get_Keys_m17624_gshared (Dictionary_2_t3275 * __this, MethodInfo* method);
#define Dictionary_2_get_Keys_m17624(__this, method) (KeyCollection_t3279 *)Dictionary_2_get_Keys_m17624_gshared((Dictionary_2_t3275 *)__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Values()
 ValueCollection_t3280 * Dictionary_2_get_Values_m17626_gshared (Dictionary_2_t3275 * __this, MethodInfo* method);
#define Dictionary_2_get_Values_m17626(__this, method) (ValueCollection_t3280 *)Dictionary_2_get_Values_m17626_gshared((Dictionary_2_t3275 *)__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ToTKey(System.Object)
 Object_t * Dictionary_2_ToTKey_m17628_gshared (Dictionary_2_t3275 * __this, Object_t * ___key, MethodInfo* method);
#define Dictionary_2_ToTKey_m17628(__this, ___key, method) (Object_t *)Dictionary_2_ToTKey_m17628_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ToTValue(System.Object)
 Object_t * Dictionary_2_ToTValue_m17630_gshared (Dictionary_2_t3275 * __this, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_ToTValue_m17630(__this, ___value, method) (Object_t *)Dictionary_2_ToTValue_m17630_gshared((Dictionary_2_t3275 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
 bool Dictionary_2_ContainsKeyValuePair_m17639 (Dictionary_2_t3275 * __this, KeyValuePair_2_t3281  ___pair, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
 Enumerator_t3283  Dictionary_2_GetEnumerator_m17640 (Dictionary_2_t3275 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Object>::<CopyTo>m__0(TKey,TValue)
 DictionaryEntry_t1302  Dictionary_2_U3CCopyToU3Em__0_m17634_gshared (Object_t * __this/* static, unused */, Object_t * ___key, Object_t * ___value, MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m17634(__this/* static, unused */, ___key, ___value, method) (DictionaryEntry_t1302 )Dictionary_2_U3CCopyToU3Em__0_m17634_gshared((Object_t *)__this/* static, unused */, (Object_t *)___key, (Object_t *)___value, method)
