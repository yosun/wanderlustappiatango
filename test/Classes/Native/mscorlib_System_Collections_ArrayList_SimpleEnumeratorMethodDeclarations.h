﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ArrayList/SimpleEnumerator
struct SimpleEnumerator_t1821;
// System.Object
struct Object_t;
// System.Collections.ArrayList
struct ArrayList_t1308;

// System.Void System.Collections.ArrayList/SimpleEnumerator::.ctor(System.Collections.ArrayList)
 void SimpleEnumerator__ctor_m10243 (SimpleEnumerator_t1821 * __this, ArrayList_t1308 * ___list, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.ArrayList/SimpleEnumerator::.cctor()
 void SimpleEnumerator__cctor_m10244 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.ArrayList/SimpleEnumerator::MoveNext()
 bool SimpleEnumerator_MoveNext_m10245 (SimpleEnumerator_t1821 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.ArrayList/SimpleEnumerator::get_Current()
 Object_t * SimpleEnumerator_get_Current_m10246 (SimpleEnumerator_t1821 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
