﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_94.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>
struct CachedInvokableCall_1_t3704  : public InvokableCall_1_t3705
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.ReconstructionFromTargetAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
