﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngineInternal.TypeInferenceRuleAttribute
struct TypeInferenceRuleAttribute_t1097;
// System.String
struct String_t;
// UnityEngineInternal.TypeInferenceRules
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules.h"

// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
 void TypeInferenceRuleAttribute__ctor_m6397 (TypeInferenceRuleAttribute_t1097 * __this, int32_t ___rule, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
 void TypeInferenceRuleAttribute__ctor_m6398 (TypeInferenceRuleAttribute_t1097 * __this, String_t* ___rule, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
 String_t* TypeInferenceRuleAttribute_ToString_m6399 (TypeInferenceRuleAttribute_t1097 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
