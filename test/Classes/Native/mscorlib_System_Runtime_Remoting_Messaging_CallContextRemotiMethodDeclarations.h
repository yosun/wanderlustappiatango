﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Messaging.CallContextRemotingData
struct CallContextRemotingData_t2014;

// System.Void System.Runtime.Remoting.Messaging.CallContextRemotingData::.ctor()
 void CallContextRemotingData__ctor_m11534 (CallContextRemotingData_t2014 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
