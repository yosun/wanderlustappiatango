﻿#pragma once
#include <stdint.h>
// Vuforia.Marker
struct Marker_t623;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.Marker>
struct Comparison_1_t3890  : public MulticastDelegate_t325
{
};
