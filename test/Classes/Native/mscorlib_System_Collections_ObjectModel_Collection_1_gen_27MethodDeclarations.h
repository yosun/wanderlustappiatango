﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>
struct Collection_1_t3863;
// System.Object
struct Object_t;
// Vuforia.DataSet
struct DataSet_t568;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// Vuforia.DataSet[]
struct DataSetU5BU5D_t3856;
// System.Collections.Generic.IEnumerator`1<Vuforia.DataSet>
struct IEnumerator_1_t801;
// System.Collections.Generic.IList`1<Vuforia.DataSet>
struct IList_1_t3862;

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::.ctor()
// System.Collections.ObjectModel.Collection`1<System.Object>
#include "mscorlib_System_Collections_ObjectModel_Collection_1_genMethodDeclarations.h"
#define Collection_1__ctor_m21986(__this, method) (void)Collection_1__ctor_m14676_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m21987(__this, method) (bool)Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14677_gshared((Collection_1_t2852 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m21988(__this, ___array, ___index, method) (void)Collection_1_System_Collections_ICollection_CopyTo_m14678_gshared((Collection_1_t2852 *)__this, (Array_t *)___array, (int32_t)___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m21989(__this, method) (Object_t *)Collection_1_System_Collections_IEnumerable_GetEnumerator_m14679_gshared((Collection_1_t2852 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m21990(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_Add_m14680_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m21991(__this, ___value, method) (bool)Collection_1_System_Collections_IList_Contains_m14681_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m21992(__this, ___value, method) (int32_t)Collection_1_System_Collections_IList_IndexOf_m14682_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m21993(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_Insert_m14683_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m21994(__this, ___value, method) (void)Collection_1_System_Collections_IList_Remove_m14684_gshared((Collection_1_t2852 *)__this, (Object_t *)___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m21995(__this, method) (bool)Collection_1_System_Collections_ICollection_get_IsSynchronized_m14685_gshared((Collection_1_t2852 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m21996(__this, method) (Object_t *)Collection_1_System_Collections_ICollection_get_SyncRoot_m14686_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m21997(__this, method) (bool)Collection_1_System_Collections_IList_get_IsFixedSize_m14687_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m21998(__this, method) (bool)Collection_1_System_Collections_IList_get_IsReadOnly_m14688_gshared((Collection_1_t2852 *)__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m21999(__this, ___index, method) (Object_t *)Collection_1_System_Collections_IList_get_Item_m14689_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m22000(__this, ___index, ___value, method) (void)Collection_1_System_Collections_IList_set_Item_m14690_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::Add(T)
#define Collection_1_Add_m22001(__this, ___item, method) (void)Collection_1_Add_m14691_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::Clear()
#define Collection_1_Clear_m22002(__this, method) (void)Collection_1_Clear_m14692_gshared((Collection_1_t2852 *)__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::ClearItems()
#define Collection_1_ClearItems_m22003(__this, method) (void)Collection_1_ClearItems_m14693_gshared((Collection_1_t2852 *)__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::Contains(T)
#define Collection_1_Contains_m22004(__this, ___item, method) (bool)Collection_1_Contains_m14694_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m22005(__this, ___array, ___index, method) (void)Collection_1_CopyTo_m14695_gshared((Collection_1_t2852 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::GetEnumerator()
#define Collection_1_GetEnumerator_m22006(__this, method) (Object_t*)Collection_1_GetEnumerator_m14696_gshared((Collection_1_t2852 *)__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::IndexOf(T)
#define Collection_1_IndexOf_m22007(__this, ___item, method) (int32_t)Collection_1_IndexOf_m14697_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::Insert(System.Int32,T)
#define Collection_1_Insert_m22008(__this, ___index, ___item, method) (void)Collection_1_Insert_m14698_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m22009(__this, ___index, ___item, method) (void)Collection_1_InsertItem_m14699_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::Remove(T)
#define Collection_1_Remove_m22010(__this, ___item, method) (bool)Collection_1_Remove_m14700_gshared((Collection_1_t2852 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m22011(__this, ___index, method) (void)Collection_1_RemoveAt_m14701_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m22012(__this, ___index, method) (void)Collection_1_RemoveItem_m14702_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::get_Count()
#define Collection_1_get_Count_m22013(__this, method) (int32_t)Collection_1_get_Count_m14703_gshared((Collection_1_t2852 *)__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::get_Item(System.Int32)
#define Collection_1_get_Item_m22014(__this, ___index, method) (DataSet_t568 *)Collection_1_get_Item_m14704_gshared((Collection_1_t2852 *)__this, (int32_t)___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m22015(__this, ___index, ___value, method) (void)Collection_1_set_Item_m14705_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m22016(__this, ___index, ___item, method) (void)Collection_1_SetItem_m14706_gshared((Collection_1_t2852 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m22017(__this/* static, unused */, ___item, method) (bool)Collection_1_IsValidItem_m14707_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m22018(__this/* static, unused */, ___item, method) (DataSet_t568 *)Collection_1_ConvertItem_m14708_gshared((Object_t *)__this/* static, unused */, (Object_t *)___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m22019(__this/* static, unused */, ___list, method) (void)Collection_1_CheckWritable_m14709_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m22020(__this/* static, unused */, ___list, method) (bool)Collection_1_IsSynchronized_m14710_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.DataSet>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m22021(__this/* static, unused */, ___list, method) (bool)Collection_1_IsFixedSize_m14711_gshared((Object_t *)__this/* static, unused */, (Object_t*)___list, method)
