﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Texture>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_122.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Texture>
struct CachedInvokableCall_1_t4479  : public InvokableCall_1_t4480
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Texture>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
