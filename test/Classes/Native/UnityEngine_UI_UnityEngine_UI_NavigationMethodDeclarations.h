﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Navigation
struct Navigation_t337;
// UnityEngine.UI.Selectable
struct Selectable_t272;
// UnityEngine.UI.Navigation/Mode
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
// UnityEngine.UI.Navigation
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"

// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::get_mode()
 int32_t Navigation_get_mode_m1314 (Navigation_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_mode(UnityEngine.UI.Navigation/Mode)
 void Navigation_set_mode_m1315 (Navigation_t337 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnUp()
 Selectable_t272 * Navigation_get_selectOnUp_m1316 (Navigation_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnUp(UnityEngine.UI.Selectable)
 void Navigation_set_selectOnUp_m1317 (Navigation_t337 * __this, Selectable_t272 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnDown()
 Selectable_t272 * Navigation_get_selectOnDown_m1318 (Navigation_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnDown(UnityEngine.UI.Selectable)
 void Navigation_set_selectOnDown_m1319 (Navigation_t337 * __this, Selectable_t272 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnLeft()
 Selectable_t272 * Navigation_get_selectOnLeft_m1320 (Navigation_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnLeft(UnityEngine.UI.Selectable)
 void Navigation_set_selectOnLeft_m1321 (Navigation_t337 * __this, Selectable_t272 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnRight()
 Selectable_t272 * Navigation_get_selectOnRight_m1322 (Navigation_t337 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnRight(UnityEngine.UI.Selectable)
 void Navigation_set_selectOnRight_m1323 (Navigation_t337 * __this, Selectable_t272 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Navigation UnityEngine.UI.Navigation::get_defaultNavigation()
 Navigation_t337  Navigation_get_defaultNavigation_m1324 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
