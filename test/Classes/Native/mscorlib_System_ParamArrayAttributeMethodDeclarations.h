﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.ParamArrayAttribute
struct ParamArrayAttribute_t494;

// System.Void System.ParamArrayAttribute::.ctor()
 void ParamArrayAttribute__ctor_m2352 (ParamArrayAttribute_t494 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
