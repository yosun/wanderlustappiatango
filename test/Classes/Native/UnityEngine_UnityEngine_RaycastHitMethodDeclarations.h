﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.RaycastHit
struct RaycastHit_t16;
// UnityEngine.Collider
struct Collider_t132;
// UnityEngine.Rigidbody
struct Rigidbody_t1006;
// UnityEngine.Transform
struct Transform_t10;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
 Vector3_t13  RaycastHit_get_point_m223 (RaycastHit_t16 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RaycastHit::set_point(UnityEngine.Vector3)
 void RaycastHit_set_point_m278 (RaycastHit_t16 * __this, Vector3_t13  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
 Vector3_t13  RaycastHit_get_normal_m2039 (RaycastHit_t16 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit::get_distance()
 float RaycastHit_get_distance_m2041 (RaycastHit_t16 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
 Collider_t132 * RaycastHit_get_collider_m2040 (RaycastHit_t16 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
 Rigidbody_t1006 * RaycastHit_get_rigidbody_m6099 (RaycastHit_t16 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
 Transform_t10 * RaycastHit_get_transform_m225 (RaycastHit_t16 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
