﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUI
struct GUI_t120;
// UnityEngine.GUISkin
struct GUISkin_t951;
// System.String
struct String_t;
// UnityEngine.GUIContent
struct GUIContent_t485;
// UnityEngine.GUIStyle
struct GUIStyle_t953;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t119;
// System.DateTime
#include "mscorlib_System_DateTime.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void UnityEngine.GUI::.cctor()
 void GUI__cctor_m5580 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_nextScrollStepTime(System.DateTime)
 void GUI_set_nextScrollStepTime_m5581 (Object_t * __this/* static, unused */, DateTime_t110  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_skin(UnityEngine.GUISkin)
 void GUI_set_skin_m5582 (Object_t * __this/* static, unused */, GUISkin_t951 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
 GUISkin_t951 * GUI_get_skin_m5583 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::set_changed(System.Boolean)
 void GUI_set_changed_m5584 (Object_t * __this/* static, unused */, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,System.String)
 void GUI_Label_m339 (Object_t * __this/* static, unused */, Rect_t118  ___position, String_t* ___text, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::Label(UnityEngine.Rect,UnityEngine.GUIContent,UnityEngine.GUIStyle)
 void GUI_Label_m5585 (Object_t * __this/* static, unused */, Rect_t118  ___position, GUIContent_t485 * ___content, GUIStyle_t953 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::DoLabel(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
 void GUI_DoLabel_m5586 (Object_t * __this/* static, unused */, Rect_t118  ___position, GUIContent_t485 * ___content, IntPtr_t121 ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::INTERNAL_CALL_DoLabel(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
 void GUI_INTERNAL_CALL_DoLabel_m5587 (Object_t * __this/* static, unused */, Rect_t118 * ___position, GUIContent_t485 * ___content, IntPtr_t121 ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::Button(UnityEngine.Rect,System.String)
 bool GUI_Button_m401 (Object_t * __this/* static, unused */, Rect_t118  ___position, String_t* ___text, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::DoButton(UnityEngine.Rect,UnityEngine.GUIContent,System.IntPtr)
 bool GUI_DoButton_m5588 (Object_t * __this/* static, unused */, Rect_t118  ___position, GUIContent_t485 * ___content, IntPtr_t121 ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI::INTERNAL_CALL_DoButton(UnityEngine.Rect&,UnityEngine.GUIContent,System.IntPtr)
 bool GUI_INTERNAL_CALL_DoButton_m5589 (Object_t * __this/* static, unused */, Rect_t118 * ___position, GUIContent_t485 * ___content, IntPtr_t121 ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::Window(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,System.String)
 Rect_t118  GUI_Window_m334 (Object_t * __this/* static, unused */, int32_t ___id, Rect_t118  ___clientRect, WindowFunction_t119 * ___func, String_t* ___text, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI::CallWindowDelegate(UnityEngine.GUI/WindowFunction,System.Int32,UnityEngine.GUISkin,System.Int32,System.Single,System.Single,UnityEngine.GUIStyle)
 void GUI_CallWindowDelegate_m5590 (Object_t * __this/* static, unused */, WindowFunction_t119 * ___func, int32_t ___id, GUISkin_t951 * ____skin, int32_t ___forceRect, float ___width, float ___height, GUIStyle_t953 * ___style, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::DoWindow(System.Int32,UnityEngine.Rect,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
 Rect_t118  GUI_DoWindow_m5591 (Object_t * __this/* static, unused */, int32_t ___id, Rect_t118  ___clientRect, WindowFunction_t119 * ___func, GUIContent_t485 * ___title, GUIStyle_t953 * ___style, GUISkin_t951 * ___skin, bool ___forceRectOnLayout, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.GUI::INTERNAL_CALL_DoWindow(System.Int32,UnityEngine.Rect&,UnityEngine.GUI/WindowFunction,UnityEngine.GUIContent,UnityEngine.GUIStyle,UnityEngine.GUISkin,System.Boolean)
 Rect_t118  GUI_INTERNAL_CALL_DoWindow_m5592 (Object_t * __this/* static, unused */, int32_t ___id, Rect_t118 * ___clientRect, WindowFunction_t119 * ___func, GUIContent_t485 * ___title, GUIStyle_t953 * ___style, GUISkin_t951 * ___skin, bool ___forceRectOnLayout, MethodInfo* method) IL2CPP_METHOD_ATTR;
