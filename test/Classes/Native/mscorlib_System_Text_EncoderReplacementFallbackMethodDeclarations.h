﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.EncoderReplacementFallback
struct EncoderReplacementFallback_t2153;
// System.String
struct String_t;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t2141;
// System.Object
struct Object_t;

// System.Void System.Text.EncoderReplacementFallback::.ctor()
 void EncoderReplacementFallback__ctor_m12276 (EncoderReplacementFallback_t2153 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.EncoderReplacementFallback::.ctor(System.String)
 void EncoderReplacementFallback__ctor_m12277 (EncoderReplacementFallback_t2153 * __this, String_t* ___replacement, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.EncoderReplacementFallback::get_DefaultString()
 String_t* EncoderReplacementFallback_get_DefaultString_m12278 (EncoderReplacementFallback_t2153 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallbackBuffer System.Text.EncoderReplacementFallback::CreateFallbackBuffer()
 EncoderFallbackBuffer_t2141 * EncoderReplacementFallback_CreateFallbackBuffer_m12279 (EncoderReplacementFallback_t2153 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.EncoderReplacementFallback::Equals(System.Object)
 bool EncoderReplacementFallback_Equals_m12280 (EncoderReplacementFallback_t2153 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.EncoderReplacementFallback::GetHashCode()
 int32_t EncoderReplacementFallback_GetHashCode_m12281 (EncoderReplacementFallback_t2153 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
