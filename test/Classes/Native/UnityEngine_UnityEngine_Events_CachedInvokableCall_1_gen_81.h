﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Mask>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_83.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Mask>
struct CachedInvokableCall_1_t3615  : public InvokableCall_1_t3616
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Mask>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
