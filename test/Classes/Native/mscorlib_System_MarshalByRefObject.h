﻿#pragma once
#include <stdint.h>
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t1719;
// System.Object
#include "mscorlib_System_Object.h"
// System.MarshalByRefObject
struct MarshalByRefObject_t1348  : public Object_t
{
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t1719 * ____identity_0;
};
