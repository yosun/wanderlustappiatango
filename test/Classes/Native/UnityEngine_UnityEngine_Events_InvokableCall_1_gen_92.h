﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.CloudRecoAbstractBehaviour>
struct UnityAction_1_t3685;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoAbstractBehaviour>
struct InvokableCall_1_t3684  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.CloudRecoAbstractBehaviour>::Delegate
	UnityAction_1_t3685 * ___Delegate_0;
};
