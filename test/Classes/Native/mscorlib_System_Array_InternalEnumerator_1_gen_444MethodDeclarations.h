﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>
struct InternalEnumerator_1_t4735;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.ScreenOrientation
#include "UnityEngine_UnityEngine_ScreenOrientation.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29047 (InternalEnumerator_1_t4735 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29048 (InternalEnumerator_1_t4735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::Dispose()
 void InternalEnumerator_1_Dispose_m29049 (InternalEnumerator_1_t4735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29050 (InternalEnumerator_1_t4735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.ScreenOrientation>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29051 (InternalEnumerator_1_t4735 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
