﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>
struct InternalEnumerator_1_t4919;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Mono.Security.Protocol.Tls.SecurityProtocolType
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"

// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30177 (InternalEnumerator_1_t4919 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30178 (InternalEnumerator_1_t4919 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::Dispose()
 void InternalEnumerator_1_Dispose_m30179 (InternalEnumerator_1_t4919 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30180 (InternalEnumerator_1_t4919 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.SecurityProtocolType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30181 (InternalEnumerator_1_t4919 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
