﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct UnityAction_1_t3702;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct InvokableCall_1_t3701  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaAbstractBehaviour>::Delegate
	UnityAction_1_t3702 * ___Delegate_0;
};
