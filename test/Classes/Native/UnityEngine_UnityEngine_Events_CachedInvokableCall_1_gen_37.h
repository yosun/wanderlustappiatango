﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.TextRecoBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_33.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>
struct CachedInvokableCall_1_t2941  : public InvokableCall_1_t2942
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.TextRecoBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
