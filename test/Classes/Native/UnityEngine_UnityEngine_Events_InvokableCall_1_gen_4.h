﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Raycaster>
struct UnityAction_1_t2741;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Raycaster>
struct InvokableCall_1_t2740  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Raycaster>::Delegate
	UnityAction_1_t2741 * ___Delegate_0;
};
