﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$96
struct $ArrayType$96_t2274;
struct $ArrayType$96_t2274_marshaled;

void $ArrayType$96_t2274_marshal(const $ArrayType$96_t2274& unmarshaled, $ArrayType$96_t2274_marshaled& marshaled);
void $ArrayType$96_t2274_marshal_back(const $ArrayType$96_t2274_marshaled& marshaled, $ArrayType$96_t2274& unmarshaled);
void $ArrayType$96_t2274_marshal_cleanup($ArrayType$96_t2274_marshaled& marshaled);
