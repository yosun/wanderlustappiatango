﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>>
struct InternalEnumerator_1_t4395;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>
#include "System_Core_System_Collections_Generic_HashSet_1_Link_gen_0.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m26938 (InternalEnumerator_1_t4395 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m26939 (InternalEnumerator_1_t4395 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>>::Dispose()
 void InternalEnumerator_1_Dispose_m26940 (InternalEnumerator_1_t4395 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m26941 (InternalEnumerator_1_t4395 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>>::get_Current()
 Link_t4385  InternalEnumerator_1_get_Current_m26942 (InternalEnumerator_1_t4395 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
