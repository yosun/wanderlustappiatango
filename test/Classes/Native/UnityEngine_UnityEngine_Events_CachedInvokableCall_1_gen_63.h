﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.MaskableGraphic>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_62.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.MaskableGraphic>
struct CachedInvokableCall_1_t3440  : public InvokableCall_1_t3441
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.MaskableGraphic>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
