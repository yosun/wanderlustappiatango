﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
struct KeyValuePair_2_t3230;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m17196 (KeyValuePair_2_t3230 * __this, Object_t * ___key, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
 Object_t * KeyValuePair_2_get_Key_m17197 (KeyValuePair_2_t3230 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m17198 (KeyValuePair_2_t3230 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
 int32_t KeyValuePair_2_get_Value_m17199 (KeyValuePair_2_t3230 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m17200 (KeyValuePair_2_t3230 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
 String_t* KeyValuePair_2_ToString_m17201 (KeyValuePair_2_t3230 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
