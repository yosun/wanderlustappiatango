﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.AspectRatioFitter>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_74.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.AspectRatioFitter>
struct CachedInvokableCall_1_t3551  : public InvokableCall_1_t3552
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.AspectRatioFitter>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
