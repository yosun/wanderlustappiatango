﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CameraDeviceImpl
struct CameraDeviceImpl_t605;
// Vuforia.WebCamImpl
struct WebCamImpl_t604;
// Vuforia.Image
struct Image_t560;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct Dictionary_2_t602;
// Vuforia.CameraDevice/CameraDirection
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera_0.h"
// Vuforia.CameraDevice/VideoModeData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_VideoM.h"
// Vuforia.CameraDevice/CameraDeviceMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_Camera.h"
// Vuforia.CameraDevice/FocusMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_CameraDevice_FocusM.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// Vuforia.WebCamImpl Vuforia.CameraDeviceImpl::get_WebCam()
 WebCamImpl_t604 * CameraDeviceImpl_get_WebCam_m2837 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::get_CameraReady()
 bool CameraDeviceImpl_get_CameraReady_m2838 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::Init(Vuforia.CameraDevice/CameraDirection)
 bool CameraDeviceImpl_Init_m2839 (CameraDeviceImpl_t605 * __this, int32_t ___cameraDirection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::Deinit()
 bool CameraDeviceImpl_Deinit_m2840 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::Start()
 bool CameraDeviceImpl_Start_m2841 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::Stop()
 bool CameraDeviceImpl_Stop_m2842 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDeviceImpl::GetVideoMode()
 VideoModeData_t558  CameraDeviceImpl_GetVideoMode_m2843 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/VideoModeData Vuforia.CameraDeviceImpl::GetVideoMode(Vuforia.CameraDevice/CameraDeviceMode)
 VideoModeData_t558  CameraDeviceImpl_GetVideoMode_m2844 (CameraDeviceImpl_t605 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::SelectVideoMode(Vuforia.CameraDevice/CameraDeviceMode)
 bool CameraDeviceImpl_SelectVideoMode_m2845 (CameraDeviceImpl_t605 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::GetSelectedVideoMode(Vuforia.CameraDevice/CameraDeviceMode&)
 bool CameraDeviceImpl_GetSelectedVideoMode_m2846 (CameraDeviceImpl_t605 * __this, int32_t* ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::SetFlashTorchMode(System.Boolean)
 bool CameraDeviceImpl_SetFlashTorchMode_m2847 (CameraDeviceImpl_t605 * __this, bool ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::SetFocusMode(Vuforia.CameraDevice/FocusMode)
 bool CameraDeviceImpl_SetFocusMode_m2848 (CameraDeviceImpl_t605 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::SetFrameFormat(Vuforia.Image/PIXEL_FORMAT,System.Boolean)
 bool CameraDeviceImpl_SetFrameFormat_m2849 (CameraDeviceImpl_t605 * __this, int32_t ___format, bool ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.Image Vuforia.CameraDeviceImpl::GetCameraImage(Vuforia.Image/PIXEL_FORMAT)
 Image_t560 * CameraDeviceImpl_GetCameraImage_m2850 (CameraDeviceImpl_t605 * __this, int32_t ___format, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CameraDevice/CameraDirection Vuforia.CameraDeviceImpl::GetCameraDirection()
 int32_t CameraDeviceImpl_GetCameraDirection_m2851 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image> Vuforia.CameraDeviceImpl::GetAllImages()
 Dictionary_2_t602 * CameraDeviceImpl_GetAllImages_m2852 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CameraDeviceImpl::IsDirty()
 bool CameraDeviceImpl_IsDirty_m2853 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CameraDeviceImpl::ResetDirtyFlag()
 void CameraDeviceImpl_ResetDirtyFlag_m2854 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CameraDeviceImpl::.ctor()
 void CameraDeviceImpl__ctor_m2855 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CameraDeviceImpl::ForceFrameFormat(Vuforia.Image/PIXEL_FORMAT,System.Boolean)
 void CameraDeviceImpl_ForceFrameFormat_m2856 (CameraDeviceImpl_t605 * __this, int32_t ___format, bool ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.CameraDeviceImpl::InitCameraDevice(System.Int32)
 int32_t CameraDeviceImpl_InitCameraDevice_m2857 (CameraDeviceImpl_t605 * __this, int32_t ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.CameraDeviceImpl::DeinitCameraDevice()
 int32_t CameraDeviceImpl_DeinitCameraDevice_m2858 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.CameraDeviceImpl::StartCameraDevice()
 int32_t CameraDeviceImpl_StartCameraDevice_m2859 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.CameraDeviceImpl::StopCameraDevice()
 int32_t CameraDeviceImpl_StopCameraDevice_m2860 (CameraDeviceImpl_t605 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CameraDeviceImpl::.cctor()
 void CameraDeviceImpl__cctor_m2861 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
