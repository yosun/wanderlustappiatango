﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SharedBetweenAnimatorsAttribute
struct SharedBetweenAnimatorsAttribute_t1068;

// System.Void UnityEngine.SharedBetweenAnimatorsAttribute::.ctor()
 void SharedBetweenAnimatorsAttribute__ctor_m6329 (SharedBetweenAnimatorsAttribute_t1068 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
