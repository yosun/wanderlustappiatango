﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>
struct ShimEnumerator_t4134;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.PropAbstractBehaviour>
struct Dictionary_2_t717;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m24672 (ShimEnumerator_t4134 * __this, Dictionary_2_t717 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>::MoveNext()
 bool ShimEnumerator_MoveNext_m24673 (ShimEnumerator_t4134 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m24674 (ShimEnumerator_t4134 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_Key()
 Object_t * ShimEnumerator_get_Key_m24675 (ShimEnumerator_t4134 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_Value()
 Object_t * ShimEnumerator_get_Value_m24676 (ShimEnumerator_t4134 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_Current()
 Object_t * ShimEnumerator_get_Current_m24677 (ShimEnumerator_t4134 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
