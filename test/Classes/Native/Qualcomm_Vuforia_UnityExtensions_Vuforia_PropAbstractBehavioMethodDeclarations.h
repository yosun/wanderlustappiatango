﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t69;
// Vuforia.Prop
struct Prop_t42;
// UnityEngine.MeshFilter
struct MeshFilter_t169;
// UnityEngine.MeshCollider
struct MeshCollider_t582;
// UnityEngine.BoxCollider
struct BoxCollider_t723;
// UnityEngine.Transform
struct Transform_t10;
// UnityEngine.GameObject
struct GameObject_t2;

// Vuforia.Prop Vuforia.PropAbstractBehaviour::get_Prop()
 Object_t * PropAbstractBehaviour_get_Prop_m3981 (PropAbstractBehaviour_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::UpdateMeshAndColliders()
 void PropAbstractBehaviour_UpdateMeshAndColliders_m528 (PropAbstractBehaviour_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Start()
 void PropAbstractBehaviour_Start_m529 (PropAbstractBehaviour_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::InternalUnregisterTrackable()
 void PropAbstractBehaviour_InternalUnregisterTrackable_m527 (PropAbstractBehaviour_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.InitializeProp(Vuforia.Prop)
 void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_InitializeProp_m530 (PropAbstractBehaviour_t69 * __this, Object_t * ___prop, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.SetMeshFilterToUpdate(UnityEngine.MeshFilter)
 void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshFilterToUpdate_m531 (PropAbstractBehaviour_t69 * __this, MeshFilter_t169 * ___meshFilterToUpdate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshFilter Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.get_MeshFilterToUpdate()
 MeshFilter_t169 * PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshFilterToUpdate_m532 (PropAbstractBehaviour_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.SetMeshColliderToUpdate(UnityEngine.MeshCollider)
 void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetMeshColliderToUpdate_m533 (PropAbstractBehaviour_t69 * __this, MeshCollider_t582 * ___meshColliderToUpdate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshCollider Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.get_MeshColliderToUpdate()
 MeshCollider_t582 * PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_MeshColliderToUpdate_m534 (PropAbstractBehaviour_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.SetBoxColliderToUpdate(UnityEngine.BoxCollider)
 void PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_SetBoxColliderToUpdate_m535 (PropAbstractBehaviour_t69 * __this, BoxCollider_t723 * ___boxColliderToUpdate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.BoxCollider Vuforia.PropAbstractBehaviour::Vuforia.IEditorPropBehaviour.get_BoxColliderToUpdate()
 BoxCollider_t723 * PropAbstractBehaviour_Vuforia_IEditorPropBehaviour_get_BoxColliderToUpdate_m536 (PropAbstractBehaviour_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::.ctor()
 void PropAbstractBehaviour__ctor_m522 (PropAbstractBehaviour_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
 bool PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m523 (PropAbstractBehaviour_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
 void PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m524 (PropAbstractBehaviour_t69 * __this, bool p0, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
 Transform_t10 * PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m525 (PropAbstractBehaviour_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.PropAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
 GameObject_t2 * PropAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m526 (PropAbstractBehaviour_t69 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
