﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>
struct InternalEnumerator_1_t4272;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi_0.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m25993 (InternalEnumerator_1_t4272 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m25994 (InternalEnumerator_1_t4272 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::Dispose()
 void InternalEnumerator_1_Dispose_m25995 (InternalEnumerator_1_t4272 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m25996 (InternalEnumerator_1_t4272 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.WebCamProfile/ProfileData>::get_Current()
 ProfileData_t747  InternalEnumerator_1_get_Current_m25997 (InternalEnumerator_1_t4272 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
