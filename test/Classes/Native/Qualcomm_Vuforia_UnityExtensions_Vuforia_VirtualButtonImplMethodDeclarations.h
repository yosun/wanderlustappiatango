﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.VirtualButtonImpl
struct VirtualButtonImpl_t746;
// System.String
struct String_t;
// Vuforia.ImageTarget
struct ImageTarget_t572;
// Vuforia.DataSet
struct DataSet_t568;
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"
// Vuforia.VirtualButton/Sensitivity
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_VirtualButton_Sensi.h"

// System.String Vuforia.VirtualButtonImpl::get_Name()
 String_t* VirtualButtonImpl_get_Name_m4046 (VirtualButtonImpl_t746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VirtualButtonImpl::get_ID()
 int32_t VirtualButtonImpl_get_ID_m4047 (VirtualButtonImpl_t746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonImpl::get_Enabled()
 bool VirtualButtonImpl_get_Enabled_m4048 (VirtualButtonImpl_t746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.RectangleData Vuforia.VirtualButtonImpl::get_Area()
 RectangleData_t588  VirtualButtonImpl_get_Area_m4049 (VirtualButtonImpl_t746 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VirtualButtonImpl::.ctor(System.String,System.Int32,Vuforia.RectangleData,Vuforia.ImageTarget,Vuforia.DataSet)
 void VirtualButtonImpl__ctor_m4050 (VirtualButtonImpl_t746 * __this, String_t* ___name, int32_t ___id, RectangleData_t588  ___area, Object_t * ___imageTarget, DataSet_t568 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonImpl::SetArea(Vuforia.RectangleData)
 bool VirtualButtonImpl_SetArea_m4051 (VirtualButtonImpl_t746 * __this, RectangleData_t588  ___area, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonImpl::SetSensitivity(Vuforia.VirtualButton/Sensitivity)
 bool VirtualButtonImpl_SetSensitivity_m4052 (VirtualButtonImpl_t746 * __this, int32_t ___sensitivity, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VirtualButtonImpl::SetEnabled(System.Boolean)
 bool VirtualButtonImpl_SetEnabled_m4053 (VirtualButtonImpl_t746 * __this, bool ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
