﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.ValidationResult
struct ValidationResult_t1625;

// System.Boolean Mono.Security.Protocol.Tls.ValidationResult::get_Trusted()
 bool ValidationResult_get_Trusted_m8517 (ValidationResult_t1625 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mono.Security.Protocol.Tls.ValidationResult::get_ErrorCode()
 int32_t ValidationResult_get_ErrorCode_m8518 (ValidationResult_t1625 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
