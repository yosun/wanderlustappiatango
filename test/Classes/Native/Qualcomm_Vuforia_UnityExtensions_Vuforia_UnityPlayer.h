﻿#pragma once
#include <stdint.h>
// Vuforia.IUnityPlayer
struct IUnityPlayer_t140;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.UnityPlayer
struct UnityPlayer_t569  : public Object_t
{
};
struct UnityPlayer_t569_StaticFields{
	// Vuforia.IUnityPlayer Vuforia.UnityPlayer::sPlayer
	Object_t * ___sPlayer_0;
};
