﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.AppDomain
struct AppDomain_t2180;
// System.String
struct String_t;
// System.Reflection.Assembly
struct Assembly_t1503;
// System.Security.Policy.Evidence
struct Evidence_t1917;
// System.Runtime.Remoting.Contexts.Context
struct Context_t1990;

// System.String System.AppDomain::getFriendlyName()
 String_t* AppDomain_getFriendlyName_m12513 (AppDomain_t2180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::getCurDomain()
 AppDomain_t2180 * AppDomain_getCurDomain_m12514 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::get_CurrentDomain()
 AppDomain_t2180 * AppDomain_get_CurrentDomain_m12515 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::LoadAssembly(System.String,System.Security.Policy.Evidence,System.Boolean)
 Assembly_t1503 * AppDomain_LoadAssembly_m12516 (AppDomain_t2180 * __this, String_t* ___assemblyRef, Evidence_t1917 * ___securityEvidence, bool ___refOnly, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::Load(System.String)
 Assembly_t1503 * AppDomain_Load_m12517 (AppDomain_t2180 * __this, String_t* ___assemblyString, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Assembly System.AppDomain::Load(System.String,System.Security.Policy.Evidence,System.Boolean)
 Assembly_t1503 * AppDomain_Load_m12518 (AppDomain_t2180 * __this, String_t* ___assemblyString, Evidence_t1917 * ___assemblySecurity, bool ___refonly, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Contexts.Context System.AppDomain::InternalGetContext()
 Context_t1990 * AppDomain_InternalGetContext_m12519 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Remoting.Contexts.Context System.AppDomain::InternalGetDefaultContext()
 Context_t1990 * AppDomain_InternalGetDefaultContext_m12520 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::InternalGetProcessGuid(System.String)
 String_t* AppDomain_InternalGetProcessGuid_m12521 (Object_t * __this/* static, unused */, String_t* ___newguid, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::GetProcessGuid()
 String_t* AppDomain_GetProcessGuid_m12522 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.AppDomain::ToString()
 String_t* AppDomain_ToString_m12523 (AppDomain_t2180 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
