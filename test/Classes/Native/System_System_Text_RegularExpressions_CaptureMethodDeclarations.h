﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.Capture
struct Capture_t1400;
// System.String
struct String_t;

// System.Void System.Text.RegularExpressions.Capture::.ctor(System.String)
 void Capture__ctor_m7154 (Capture_t1400 * __this, String_t* ___text, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Capture::.ctor(System.String,System.Int32,System.Int32)
 void Capture__ctor_m7155 (Capture_t1400 * __this, String_t* ___text, int32_t ___index, int32_t ___length, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.RegularExpressions.Capture::get_Index()
 int32_t Capture_get_Index_m7156 (Capture_t1400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.RegularExpressions.Capture::get_Length()
 int32_t Capture_get_Length_m7157 (Capture_t1400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.RegularExpressions.Capture::get_Value()
 String_t* Capture_get_Value_m7158 (Capture_t1400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.RegularExpressions.Capture::ToString()
 String_t* Capture_ToString_m7159 (Capture_t1400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.RegularExpressions.Capture::get_Text()
 String_t* Capture_get_Text_m7160 (Capture_t1400 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
