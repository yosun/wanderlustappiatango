﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.SByte>
struct InternalEnumerator_1_t4932;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30242 (InternalEnumerator_1_t4932 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30243 (InternalEnumerator_1_t4932 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
 void InternalEnumerator_1_Dispose_m30244 (InternalEnumerator_1_t4932 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30245 (InternalEnumerator_1_t4932 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
 int8_t InternalEnumerator_1_get_Current_m30246 (InternalEnumerator_1_t4932 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
