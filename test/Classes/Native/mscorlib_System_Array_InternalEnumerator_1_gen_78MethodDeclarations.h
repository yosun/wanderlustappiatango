﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>
struct InternalEnumerator_1_t2877;
// System.Object
struct Object_t;
// Vuforia.KeepAliveAbstractBehaviour
struct KeepAliveAbstractBehaviour_t64;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m14896(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14897(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m14898(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14899(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.KeepAliveAbstractBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m14900(__this, method) (KeepAliveAbstractBehaviour_t64 *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
