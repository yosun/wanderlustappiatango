﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackerAbstractBehaviour
struct SmartTerrainTrackerAbstractBehaviour_t76;
// System.Action
struct Action_t148;

// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Awake()
 void SmartTerrainTrackerAbstractBehaviour_Awake_m2759 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnEnable()
 void SmartTerrainTrackerAbstractBehaviour_OnEnable_m2760 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDisable()
 void SmartTerrainTrackerAbstractBehaviour_OnDisable_m2761 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnDestroy()
 void SmartTerrainTrackerAbstractBehaviour_OnDestroy_m2762 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::RegisterTrackerStartedCallback(System.Action)
 void SmartTerrainTrackerAbstractBehaviour_RegisterTrackerStartedCallback_m2763 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, Action_t148 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::UnregisterTrackerStartedCallback(System.Action)
 void SmartTerrainTrackerAbstractBehaviour_UnregisterTrackerStartedCallback_m2764 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, Action_t148 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StartSmartTerrainTracker()
 void SmartTerrainTrackerAbstractBehaviour_StartSmartTerrainTracker_m2765 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::StopSmartTerrainTracker()
 void SmartTerrainTrackerAbstractBehaviour_StopSmartTerrainTracker_m2766 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::InitSmartTerrainTracker()
 void SmartTerrainTrackerAbstractBehaviour_InitSmartTerrainTracker_m2767 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnQCARInitialized()
 void SmartTerrainTrackerAbstractBehaviour_OnQCARInitialized_m2768 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnQCARStarted()
 void SmartTerrainTrackerAbstractBehaviour_OnQCARStarted_m2769 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::OnPause(System.Boolean)
 void SmartTerrainTrackerAbstractBehaviour_OnPause_m2770 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, bool ___pause, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetAutomaticStart(System.Boolean)
 void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetAutomaticStart_m572 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, bool ___autoStart, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_AutomaticStart()
 bool SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_AutomaticStart_m573 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.SetSmartTerrainScaleToMM(System.Single)
 void SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_SetSmartTerrainScaleToMM_m574 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, float ___scaleToMM, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.SmartTerrainTrackerAbstractBehaviour::Vuforia.IEditorSmartTerrainTrackerBehaviour.get_ScaleToMM()
 float SmartTerrainTrackerAbstractBehaviour_Vuforia_IEditorSmartTerrainTrackerBehaviour_get_ScaleToMM_m575 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackerAbstractBehaviour::.ctor()
 void SmartTerrainTrackerAbstractBehaviour__ctor_m571 (SmartTerrainTrackerAbstractBehaviour_t76 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
