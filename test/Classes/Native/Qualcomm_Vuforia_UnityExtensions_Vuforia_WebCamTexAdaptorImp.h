﻿#pragma once
#include <stdint.h>
// UnityEngine.WebCamTexture
struct WebCamTexture_t683;
// Vuforia.WebCamTexAdaptor
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor.h"
// Vuforia.WebCamTexAdaptorImpl
struct WebCamTexAdaptorImpl_t684  : public WebCamTexAdaptor_t628
{
	// UnityEngine.WebCamTexture Vuforia.WebCamTexAdaptorImpl::mWebCamTexture
	WebCamTexture_t683 * ___mWebCamTexture_0;
};
