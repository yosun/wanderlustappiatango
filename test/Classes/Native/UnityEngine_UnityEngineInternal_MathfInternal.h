﻿#pragma once
#include <stdint.h>
// System.Single modreq(System.Runtime.CompilerServices.IsVolatile)
struct Single_t105;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngineInternal.MathfInternal
struct MathfInternal_t978 
{
};
struct MathfInternal_t978_StaticFields{
	// System.Single modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngineInternal.MathfInternal::FloatMinNormal
	float ___FloatMinNormal_0;
	// System.Single modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngineInternal.MathfInternal::FloatMinDenormal
	float ___FloatMinDenormal_1;
	// System.Boolean UnityEngineInternal.MathfInternal::IsFlushToZeroEnabled
	bool ___IsFlushToZeroEnabled_2;
};
