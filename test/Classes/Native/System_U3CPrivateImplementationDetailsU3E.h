﻿#pragma once
#include <stdint.h>
// System.Object
#include "mscorlib_System_Object.h"
// <PrivateImplementationDetails>/$ArrayType$128
#include "System_U3CPrivateImplementationDetailsU3E_$ArrayType$128.h"
// <PrivateImplementationDetails>/$ArrayType$12
#include "System_U3CPrivateImplementationDetailsU3E_$ArrayType$12.h"
// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t1473  : public Object_t
{
};
struct U3CPrivateImplementationDetailsU3E_t1473_StaticFields{
	// <PrivateImplementationDetails>/$ArrayType$128 <PrivateImplementationDetails>::$$field-2
	$ArrayType$128_t1471  ___$$fieldU2D2_0;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-3
	$ArrayType$12_t1472  ___$$fieldU2D3_1;
	// <PrivateImplementationDetails>/$ArrayType$12 <PrivateImplementationDetails>::$$field-4
	$ArrayType$12_t1472  ___$$fieldU2D4_2;
};
