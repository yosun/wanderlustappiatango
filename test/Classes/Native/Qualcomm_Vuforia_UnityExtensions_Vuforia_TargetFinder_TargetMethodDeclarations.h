﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TargetFinder/TargetSearchResult
struct TargetSearchResult_t732;
struct TargetSearchResult_t732_marshaled;

void TargetSearchResult_t732_marshal(const TargetSearchResult_t732& unmarshaled, TargetSearchResult_t732_marshaled& marshaled);
void TargetSearchResult_t732_marshal_back(const TargetSearchResult_t732_marshaled& marshaled, TargetSearchResult_t732& unmarshaled);
void TargetSearchResult_t732_marshal_cleanup(TargetSearchResult_t732_marshaled& marshaled);
