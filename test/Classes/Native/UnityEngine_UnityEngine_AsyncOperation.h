﻿#pragma once
#include <stdint.h>
// UnityEngine.YieldInstruction
#include "UnityEngine_UnityEngine_YieldInstruction.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.AsyncOperation
struct AsyncOperation_t909  : public YieldInstruction_t917
{
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	IntPtr_t121 ___m_Ptr_0;
};
// Native definition for marshalling of: UnityEngine.AsyncOperation
struct AsyncOperation_t909_marshaled
{
	IntPtr_t121 ___m_Ptr_0;
};
