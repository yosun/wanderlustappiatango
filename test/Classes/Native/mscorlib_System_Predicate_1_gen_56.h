﻿#pragma once
#include <stdint.h>
// System.Security.Policy.StrongName
struct StrongName_t2133;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<System.Security.Policy.StrongName>
struct Predicate_1_t5114  : public MulticastDelegate_t325
{
};
