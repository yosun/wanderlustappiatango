﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Slider>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_69.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Slider>
struct CachedInvokableCall_1_t3499  : public InvokableCall_1_t3500
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Slider>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
