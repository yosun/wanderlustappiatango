﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUISettings
struct GUISettings_t967;

// System.Void UnityEngine.GUISettings::.ctor()
 void GUISettings__ctor_m5647 (GUISettings_t967 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
