﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>
struct KeyValuePair_2_t4268;
// System.String
struct String_t;
// Vuforia.WebCamProfile/ProfileData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WebCamProfile_Profi_0.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m25982 (KeyValuePair_2_t4268 * __this, String_t* ___key, ProfileData_t747  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Key()
 String_t* KeyValuePair_2_get_Key_m25983 (KeyValuePair_2_t4268 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m25984 (KeyValuePair_2_t4268 * __this, String_t* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::get_Value()
 ProfileData_t747  KeyValuePair_2_get_Value_m25985 (KeyValuePair_2_t4268 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m25986 (KeyValuePair_2_t4268 * __this, ProfileData_t747  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>::ToString()
 String_t* KeyValuePair_2_ToString_m25987 (KeyValuePair_2_t4268 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
