﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<Vuforia.DataSetImpl>
struct List_1_t618;
// System.Collections.Generic.List`1<Vuforia.DataSet>
struct List_1_t619;
// Vuforia.ImageTargetBuilder
struct ImageTargetBuilder_t601;
// Vuforia.TargetFinder
struct TargetFinder_t616;
// Vuforia.ObjectTracker
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ObjectTracker.h"
// Vuforia.ObjectTrackerImpl
struct ObjectTrackerImpl_t620  : public ObjectTracker_t561
{
	// System.Collections.Generic.List`1<Vuforia.DataSetImpl> Vuforia.ObjectTrackerImpl::mActiveDataSets
	List_1_t618 * ___mActiveDataSets_1;
	// System.Collections.Generic.List`1<Vuforia.DataSet> Vuforia.ObjectTrackerImpl::mDataSets
	List_1_t619 * ___mDataSets_2;
	// Vuforia.ImageTargetBuilder Vuforia.ObjectTrackerImpl::mImageTargetBuilder
	ImageTargetBuilder_t601 * ___mImageTargetBuilder_3;
	// Vuforia.TargetFinder Vuforia.ObjectTrackerImpl::mTargetFinder
	TargetFinder_t616 * ___mTargetFinder_4;
};
