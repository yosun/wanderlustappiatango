﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Gyroscope
struct Gyroscope_t102;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void UnityEngine.Gyroscope::.ctor(System.Int32)
 void Gyroscope__ctor_m6017 (Gyroscope_t102 * __this, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Gyroscope::rotationRate_Internal(System.Int32)
 Vector3_t13  Gyroscope_rotationRate_Internal_m6018 (Object_t * __this/* static, unused */, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Gyroscope::attitude_Internal(System.Int32)
 Quaternion_t12  Gyroscope_attitude_Internal_m6019 (Object_t * __this/* static, unused */, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::setEnabled_Internal(System.Int32,System.Boolean)
 void Gyroscope_setEnabled_Internal_m6020 (Object_t * __this/* static, unused */, int32_t ___idx, bool ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::setUpdateInterval_Internal(System.Int32,System.Single)
 void Gyroscope_setUpdateInterval_Internal_m6021 (Object_t * __this/* static, unused */, int32_t ___idx, float ___interval, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Gyroscope::get_rotationRate()
 Vector3_t13  Gyroscope_get_rotationRate_m253 (Gyroscope_t102 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Gyroscope::get_attitude()
 Quaternion_t12  Gyroscope_get_attitude_m250 (Gyroscope_t102 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::set_enabled(System.Boolean)
 void Gyroscope_set_enabled_m246 (Gyroscope_t102 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gyroscope::set_updateInterval(System.Single)
 void Gyroscope_set_updateInterval_m247 (Gyroscope_t102 * __this, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
