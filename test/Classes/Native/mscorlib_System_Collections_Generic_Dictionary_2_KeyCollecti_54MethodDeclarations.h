﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.GUIStyle>
struct Enumerator_t4549;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t970;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_13MethodDeclarations.h"
#define Enumerator__ctor_m27920(__this, ___host, method) (void)Enumerator__ctor_m17666_gshared((Enumerator_t3285 *)__this, (Dictionary_2_t3275 *)___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m27921(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m17667_gshared((Enumerator_t3285 *)__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m27922(__this, method) (void)Enumerator_Dispose_m17668_gshared((Enumerator_t3285 *)__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m27923(__this, method) (bool)Enumerator_MoveNext_m17669_gshared((Enumerator_t3285 *)__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m27924(__this, method) (String_t*)Enumerator_get_Current_m17670_gshared((Enumerator_t3285 *)__this, method)
