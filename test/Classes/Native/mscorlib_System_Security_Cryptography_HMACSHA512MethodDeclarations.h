﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.HMACSHA512
struct HMACSHA512_t2100;
// System.Byte[]
struct ByteU5BU5D_t609;

// System.Void System.Security.Cryptography.HMACSHA512::.ctor()
 void HMACSHA512__ctor_m11927 (HMACSHA512_t2100 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA512::.ctor(System.Byte[])
 void HMACSHA512__ctor_m11928 (HMACSHA512_t2100 * __this, ByteU5BU5D_t609* ___key, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA512::.cctor()
 void HMACSHA512__cctor_m11929 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.HMACSHA512::set_ProduceLegacyHmacValues(System.Boolean)
 void HMACSHA512_set_ProduceLegacyHmacValues_m11930 (HMACSHA512_t2100 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
