﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ObjectTargetImpl
struct ObjectTargetImpl_t567;
// Vuforia.DataSetImpl
struct DataSetImpl_t566;
// System.String
struct String_t;
// Vuforia.DataSet
struct DataSet_t568;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void Vuforia.ObjectTargetImpl::.ctor(System.String,System.Int32,Vuforia.DataSet)
 void ObjectTargetImpl__ctor_m2693 (ObjectTargetImpl_t567 * __this, String_t* ___name, int32_t ___id, DataSet_t568 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.ObjectTargetImpl::GetSize()
 Vector3_t13  ObjectTargetImpl_GetSize_m2694 (ObjectTargetImpl_t567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ObjectTargetImpl::SetSize(UnityEngine.Vector3)
 void ObjectTargetImpl_SetSize_m2695 (ObjectTargetImpl_t567 * __this, Vector3_t13  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTargetImpl::StartExtendedTracking()
 bool ObjectTargetImpl_StartExtendedTracking_m2696 (ObjectTargetImpl_t567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ObjectTargetImpl::StopExtendedTracking()
 bool ObjectTargetImpl_StopExtendedTracking_m2697 (ObjectTargetImpl_t567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.DataSetImpl Vuforia.ObjectTargetImpl::get_DataSet()
 DataSetImpl_t566 * ObjectTargetImpl_get_DataSet_m2698 (ObjectTargetImpl_t567 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
