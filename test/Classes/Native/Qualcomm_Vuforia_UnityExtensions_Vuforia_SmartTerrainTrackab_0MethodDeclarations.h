﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.SmartTerrainTrackableImpl
struct SmartTerrainTrackableImpl_t666;
// System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackable>
struct IEnumerable_1_t667;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t581;
// System.String
struct String_t;
// UnityEngine.Mesh
struct Mesh_t174;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Vuforia.QCARManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Pos.h"

// System.Void Vuforia.SmartTerrainTrackableImpl::.ctor(System.String,System.Int32,Vuforia.SmartTerrainTrackable)
 void SmartTerrainTrackableImpl__ctor_m3032 (SmartTerrainTrackableImpl_t666 * __this, String_t* ___name, int32_t ___id, Object_t * ___parent, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.SmartTerrainTrackable> Vuforia.SmartTerrainTrackableImpl::get_Children()
 Object_t* SmartTerrainTrackableImpl_get_Children_m3033 (SmartTerrainTrackableImpl_t666 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.SmartTerrainTrackableImpl::get_MeshRevision()
 int32_t SmartTerrainTrackableImpl_get_MeshRevision_m3034 (SmartTerrainTrackableImpl_t666 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SmartTerrainTrackable Vuforia.SmartTerrainTrackableImpl::get_Parent()
 Object_t * SmartTerrainTrackableImpl_get_Parent_m3035 (SmartTerrainTrackableImpl_t666 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableImpl::set_Parent(Vuforia.SmartTerrainTrackable)
 void SmartTerrainTrackableImpl_set_Parent_m3036 (SmartTerrainTrackableImpl_t666 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh Vuforia.SmartTerrainTrackableImpl::GetMesh()
 Mesh_t174 * SmartTerrainTrackableImpl_GetMesh_m3037 (SmartTerrainTrackableImpl_t666 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.SmartTerrainTrackableImpl::get_LocalPosition()
 Vector3_t13  SmartTerrainTrackableImpl_get_LocalPosition_m3038 (SmartTerrainTrackableImpl_t666 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableImpl::SetLocalPose(Vuforia.QCARManagerImpl/PoseData)
 void SmartTerrainTrackableImpl_SetLocalPose_m3039 (SmartTerrainTrackableImpl_t666 * __this, PoseData_t638  ___localPose, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableImpl::DestroyMesh()
 void SmartTerrainTrackableImpl_DestroyMesh_m3040 (SmartTerrainTrackableImpl_t666 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableImpl::AddChild(Vuforia.SmartTerrainTrackable)
 void SmartTerrainTrackableImpl_AddChild_m3041 (SmartTerrainTrackableImpl_t666 * __this, Object_t * ___newChild, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SmartTerrainTrackableImpl::RemoveChild(Vuforia.SmartTerrainTrackable)
 void SmartTerrainTrackableImpl_RemoveChild_m3042 (SmartTerrainTrackableImpl_t666 * __this, Object_t * ___removedChild, MethodInfo* method) IL2CPP_METHOD_ATTR;
