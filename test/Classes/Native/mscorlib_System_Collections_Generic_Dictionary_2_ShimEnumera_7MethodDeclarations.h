﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct ShimEnumerator_t3783;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>
struct Dictionary_2_t602;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m21190 (ShimEnumerator_t3783 * __this, Dictionary_2_t602 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::MoveNext()
 bool ShimEnumerator_MoveNext_m21191 (ShimEnumerator_t3783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m21192 (ShimEnumerator_t3783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Key()
 Object_t * ShimEnumerator_get_Key_m21193 (ShimEnumerator_t3783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Value()
 Object_t * ShimEnumerator_get_Value_m21194 (ShimEnumerator_t3783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>::get_Current()
 Object_t * ShimEnumerator_get_Current_m21195 (ShimEnumerator_t3783 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
