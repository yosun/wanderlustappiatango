﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
struct UnityAction_1_t4366;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>
struct InvokableCall_1_t4365  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.UserDefinedTargetBuildingAbstractBehaviour>::Delegate
	UnityAction_1_t4366 * ___Delegate_0;
};
