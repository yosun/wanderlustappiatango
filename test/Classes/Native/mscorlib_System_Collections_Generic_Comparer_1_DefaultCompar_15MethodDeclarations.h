﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UI.Selectable>
struct DefaultComparer_t3478;
// UnityEngine.UI.Selectable
struct Selectable_t272;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UI.Selectable>::.ctor()
// System.Collections.Generic.Comparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_DefaultCompar_0MethodDeclarations.h"
#define DefaultComparer__ctor_m19122(__this, method) (void)DefaultComparer__ctor_m14758_gshared((DefaultComparer_t2862 *)__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UI.Selectable>::Compare(T,T)
#define DefaultComparer_Compare_m19123(__this, ___x, ___y, method) (int32_t)DefaultComparer_Compare_m14759_gshared((DefaultComparer_t2862 *)__this, (Object_t *)___x, (Object_t *)___y, method)
