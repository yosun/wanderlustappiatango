﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Collider2D
struct Collider2D_t448;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1008;

// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
 Rigidbody2D_t1008 * Collider2D_get_attachedRigidbody_m6106 (Collider2D_t448 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
