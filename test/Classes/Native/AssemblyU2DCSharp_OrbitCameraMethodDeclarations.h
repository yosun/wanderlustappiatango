﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// OrbitCamera
struct OrbitCamera_t26;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void OrbitCamera::.ctor()
 void OrbitCamera__ctor_m68 (OrbitCamera_t26 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::Start()
 void OrbitCamera_Start_m69 (OrbitCamera_t26 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::Update()
 void OrbitCamera_Update_m70 (OrbitCamera_t26 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::LateUpdate()
 void OrbitCamera_LateUpdate_m71 (OrbitCamera_t26 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::HandlePlayerInput()
 void OrbitCamera_HandlePlayerInput_m72 (OrbitCamera_t26 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::CalculateDesiredPosition()
 void OrbitCamera_CalculateDesiredPosition_m73 (OrbitCamera_t26 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 OrbitCamera::CalculatePosition(System.Single,System.Single,System.Single)
 Vector3_t13  OrbitCamera_CalculatePosition_m74 (OrbitCamera_t26 * __this, float ___rotationX, float ___rotationY, float ___distance, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::UpdatePosition()
 void OrbitCamera_UpdatePosition_m75 (OrbitCamera_t26 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::Reset()
 void OrbitCamera_Reset_m76 (OrbitCamera_t26 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OrbitCamera::ClampAngle(System.Single,System.Single,System.Single)
 float OrbitCamera_ClampAngle_m77 (OrbitCamera_t26 * __this, float ___angle, float ___min, float ___max, MethodInfo* method) IL2CPP_METHOD_ATTR;
