﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.Texture>
struct UnityAction_1_t4481;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.Texture>
struct InvokableCall_1_t4480  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.Texture>::Delegate
	UnityAction_1_t4481 * ___Delegate_0;
};
