﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$24
struct $ArrayType$24_t2262;
struct $ArrayType$24_t2262_marshaled;

void $ArrayType$24_t2262_marshal(const $ArrayType$24_t2262& unmarshaled, $ArrayType$24_t2262_marshaled& marshaled);
void $ArrayType$24_t2262_marshal_back(const $ArrayType$24_t2262_marshaled& marshaled, $ArrayType$24_t2262& unmarshaled);
void $ArrayType$24_t2262_marshal_cleanup($ArrayType$24_t2262_marshaled& marshaled);
