﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// Vuforia.BackgroundPlaneAbstractBehaviour[]
// Vuforia.BackgroundPlaneAbstractBehaviour[]
struct BackgroundPlaneAbstractBehaviourU5BU5D_t894  : public Array_t
{
};
struct BackgroundPlaneAbstractBehaviourU5BU5D_t894_StaticFields{
};
// Vuforia.IVideoBackgroundEventHandler[]
// Vuforia.IVideoBackgroundEventHandler[]
struct IVideoBackgroundEventHandlerU5BU5D_t4327  : public Array_t
{
};
// Vuforia.CloudRecoAbstractBehaviour[]
// Vuforia.CloudRecoAbstractBehaviour[]
struct CloudRecoAbstractBehaviourU5BU5D_t5492  : public Array_t
{
};
// Vuforia.CylinderTargetAbstractBehaviour[]
// Vuforia.CylinderTargetAbstractBehaviour[]
struct CylinderTargetAbstractBehaviourU5BU5D_t5493  : public Array_t
{
};
// Vuforia.IEditorCylinderTargetBehaviour[]
// Vuforia.IEditorCylinderTargetBehaviour[]
struct IEditorCylinderTargetBehaviourU5BU5D_t5494  : public Array_t
{
};
// Vuforia.IEditorDataSetTrackableBehaviour[]
// Vuforia.IEditorDataSetTrackableBehaviour[]
struct IEditorDataSetTrackableBehaviourU5BU5D_t5495  : public Array_t
{
};
// Vuforia.IEditorTrackableBehaviour[]
// Vuforia.IEditorTrackableBehaviour[]
struct IEditorTrackableBehaviourU5BU5D_t5496  : public Array_t
{
};
// Vuforia.DataSetTrackableBehaviour[]
// Vuforia.DataSetTrackableBehaviour[]
struct DataSetTrackableBehaviourU5BU5D_t865  : public Array_t
{
};
// Vuforia.WorldCenterTrackableBehaviour[]
// Vuforia.WorldCenterTrackableBehaviour[]
struct WorldCenterTrackableBehaviourU5BU5D_t5497  : public Array_t
{
};
// Vuforia.TrackableBehaviour[]
// Vuforia.TrackableBehaviour[]
struct TrackableBehaviourU5BU5D_t813  : public Array_t
{
};
// Vuforia.DataSetLoadAbstractBehaviour[]
// Vuforia.DataSetLoadAbstractBehaviour[]
struct DataSetLoadAbstractBehaviourU5BU5D_t5498  : public Array_t
{
};
// Vuforia.QCARAbstractBehaviour[]
// Vuforia.QCARAbstractBehaviour[]
struct QCARAbstractBehaviourU5BU5D_t5499  : public Array_t
{
};
// Vuforia.IEditorQCARBehaviour[]
// Vuforia.IEditorQCARBehaviour[]
struct IEditorQCARBehaviourU5BU5D_t5500  : public Array_t
{
};
// Vuforia.ITrackableEventHandler[]
// Vuforia.ITrackableEventHandler[]
struct ITrackableEventHandlerU5BU5D_t3658  : public Array_t
{
};
// Vuforia.HideExcessAreaAbstractBehaviour[]
// Vuforia.HideExcessAreaAbstractBehaviour[]
struct HideExcessAreaAbstractBehaviourU5BU5D_t5501  : public Array_t
{
};
// Vuforia.ImageTargetAbstractBehaviour[]
// Vuforia.ImageTargetAbstractBehaviour[]
struct ImageTargetAbstractBehaviourU5BU5D_t5502  : public Array_t
{
};
// Vuforia.IEditorImageTargetBehaviour[]
// Vuforia.IEditorImageTargetBehaviour[]
struct IEditorImageTargetBehaviourU5BU5D_t5503  : public Array_t
{
};
// Vuforia.KeepAliveAbstractBehaviour[]
// Vuforia.KeepAliveAbstractBehaviour[]
struct KeepAliveAbstractBehaviourU5BU5D_t5504  : public Array_t
{
};
struct KeepAliveAbstractBehaviourU5BU5D_t5504_StaticFields{
};
// Vuforia.MarkerAbstractBehaviour[]
// Vuforia.MarkerAbstractBehaviour[]
struct MarkerAbstractBehaviourU5BU5D_t864  : public Array_t
{
};
// Vuforia.IEditorMarkerBehaviour[]
// Vuforia.IEditorMarkerBehaviour[]
struct IEditorMarkerBehaviourU5BU5D_t5505  : public Array_t
{
};
// Vuforia.MaskOutAbstractBehaviour[]
// Vuforia.MaskOutAbstractBehaviour[]
struct MaskOutAbstractBehaviourU5BU5D_t5506  : public Array_t
{
};
// Vuforia.MultiTargetAbstractBehaviour[]
// Vuforia.MultiTargetAbstractBehaviour[]
struct MultiTargetAbstractBehaviourU5BU5D_t5507  : public Array_t
{
};
// Vuforia.IEditorMultiTargetBehaviour[]
// Vuforia.IEditorMultiTargetBehaviour[]
struct IEditorMultiTargetBehaviourU5BU5D_t5508  : public Array_t
{
};
// Vuforia.ObjectTargetAbstractBehaviour[]
// Vuforia.ObjectTargetAbstractBehaviour[]
struct ObjectTargetAbstractBehaviourU5BU5D_t5509  : public Array_t
{
};
// Vuforia.IEditorObjectTargetBehaviour[]
// Vuforia.IEditorObjectTargetBehaviour[]
struct IEditorObjectTargetBehaviourU5BU5D_t5510  : public Array_t
{
};
// Vuforia.PropAbstractBehaviour[]
// Vuforia.PropAbstractBehaviour[]
struct PropAbstractBehaviourU5BU5D_t857  : public Array_t
{
};
// Vuforia.IEditorPropBehaviour[]
// Vuforia.IEditorPropBehaviour[]
struct IEditorPropBehaviourU5BU5D_t5511  : public Array_t
{
};
// Vuforia.SmartTerrainTrackableBehaviour[]
// Vuforia.SmartTerrainTrackableBehaviour[]
struct SmartTerrainTrackableBehaviourU5BU5D_t820  : public Array_t
{
};
// Vuforia.ReconstructionAbstractBehaviour[]
// Vuforia.ReconstructionAbstractBehaviour[]
struct ReconstructionAbstractBehaviourU5BU5D_t821  : public Array_t
{
};
// Vuforia.IEditorReconstructionBehaviour[]
// Vuforia.IEditorReconstructionBehaviour[]
struct IEditorReconstructionBehaviourU5BU5D_t5512  : public Array_t
{
};
// Vuforia.ReconstructionFromTargetAbstractBehaviour[]
// Vuforia.ReconstructionFromTargetAbstractBehaviour[]
struct ReconstructionFromTargetAbstractBehaviourU5BU5D_t5513  : public Array_t
{
};
// Vuforia.SmartTerrainTrackerAbstractBehaviour[]
// Vuforia.SmartTerrainTrackerAbstractBehaviour[]
struct SmartTerrainTrackerAbstractBehaviourU5BU5D_t5514  : public Array_t
{
};
// Vuforia.IEditorSmartTerrainTrackerBehaviour[]
// Vuforia.IEditorSmartTerrainTrackerBehaviour[]
struct IEditorSmartTerrainTrackerBehaviourU5BU5D_t5515  : public Array_t
{
};
// Vuforia.SurfaceAbstractBehaviour[]
// Vuforia.SurfaceAbstractBehaviour[]
struct SurfaceAbstractBehaviourU5BU5D_t4092  : public Array_t
{
};
// Vuforia.IEditorSurfaceBehaviour[]
// Vuforia.IEditorSurfaceBehaviour[]
struct IEditorSurfaceBehaviourU5BU5D_t5516  : public Array_t
{
};
// Vuforia.TextRecoAbstractBehaviour[]
// Vuforia.TextRecoAbstractBehaviour[]
struct TextRecoAbstractBehaviourU5BU5D_t5517  : public Array_t
{
};
// Vuforia.IEditorTextRecoBehaviour[]
// Vuforia.IEditorTextRecoBehaviour[]
struct IEditorTextRecoBehaviourU5BU5D_t5518  : public Array_t
{
};
// Vuforia.TurnOffAbstractBehaviour[]
// Vuforia.TurnOffAbstractBehaviour[]
struct TurnOffAbstractBehaviourU5BU5D_t5519  : public Array_t
{
};
// Vuforia.UserDefinedTargetBuildingAbstractBehaviour[]
// Vuforia.UserDefinedTargetBuildingAbstractBehaviour[]
struct UserDefinedTargetBuildingAbstractBehaviourU5BU5D_t5520  : public Array_t
{
};
// Vuforia.VideoBackgroundAbstractBehaviour[]
// Vuforia.VideoBackgroundAbstractBehaviour[]
struct VideoBackgroundAbstractBehaviourU5BU5D_t759  : public Array_t
{
};
// Vuforia.VideoTextureRendererAbstractBehaviour[]
// Vuforia.VideoTextureRendererAbstractBehaviour[]
struct VideoTextureRendererAbstractBehaviourU5BU5D_t5521  : public Array_t
{
};
// Vuforia.VirtualButtonAbstractBehaviour[]
// Vuforia.VirtualButtonAbstractBehaviour[]
struct VirtualButtonAbstractBehaviourU5BU5D_t729  : public Array_t
{
};
// Vuforia.IEditorVirtualButtonBehaviour[]
// Vuforia.IEditorVirtualButtonBehaviour[]
struct IEditorVirtualButtonBehaviourU5BU5D_t5522  : public Array_t
{
};
// Vuforia.WebCamAbstractBehaviour[]
// Vuforia.WebCamAbstractBehaviour[]
struct WebCamAbstractBehaviourU5BU5D_t5523  : public Array_t
{
};
// Vuforia.WordAbstractBehaviour[]
// Vuforia.WordAbstractBehaviour[]
struct WordAbstractBehaviourU5BU5D_t827  : public Array_t
{
};
// Vuforia.IEditorWordBehaviour[]
// Vuforia.IEditorWordBehaviour[]
struct IEditorWordBehaviourU5BU5D_t5524  : public Array_t
{
};
// Vuforia.FactorySetter[]
// Vuforia.FactorySetter[]
struct FactorySetterU5BU5D_t5525  : public Array_t
{
};
// Vuforia.InternalEyewear/EyewearCalibrationReading[]
// Vuforia.InternalEyewear/EyewearCalibrationReading[]
#pragma pack(push, tp, 1)
struct EyewearCalibrationReadingU5BU5D_t546  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.TrackableBehaviour/Status[]
// Vuforia.TrackableBehaviour/Status[]
struct StatusU5BU5D_t5526  : public Array_t
{
};
// Vuforia.CameraDevice/CameraDeviceMode[]
// Vuforia.CameraDevice/CameraDeviceMode[]
struct CameraDeviceModeU5BU5D_t5527  : public Array_t
{
};
// Vuforia.CameraDevice/FocusMode[]
// Vuforia.CameraDevice/FocusMode[]
struct FocusModeU5BU5D_t5528  : public Array_t
{
};
// Vuforia.CameraDevice/CameraDirection[]
// Vuforia.CameraDevice/CameraDirection[]
struct CameraDirectionU5BU5D_t5529  : public Array_t
{
};
// Vuforia.ICloudRecoEventHandler[]
// Vuforia.ICloudRecoEventHandler[]
struct ICloudRecoEventHandlerU5BU5D_t3686  : public Array_t
{
};
// Vuforia.InternalEyewear/EyeID[]
// Vuforia.InternalEyewear/EyeID[]
struct EyeIDU5BU5D_t5530  : public Array_t
{
};
// Vuforia.DataSet/StorageType[]
// Vuforia.DataSet/StorageType[]
struct StorageTypeU5BU5D_t5531  : public Array_t
{
};
// Vuforia.VirtualButton[]
// Vuforia.VirtualButton[]
struct VirtualButtonU5BU5D_t3750  : public Array_t
{
};
// Vuforia.ImageTargetType[]
// Vuforia.ImageTargetType[]
struct ImageTargetTypeU5BU5D_t5532  : public Array_t
{
};
// Vuforia.ImageTargetBuilder/FrameQuality[]
// Vuforia.ImageTargetBuilder/FrameQuality[]
struct FrameQualityU5BU5D_t5533  : public Array_t
{
};
// Vuforia.Image/PIXEL_FORMAT[]
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t3765  : public Array_t
{
};
// Vuforia.Image[]
// Vuforia.Image[]
struct ImageU5BU5D_t3766  : public Array_t
{
};
// Vuforia.Trackable[]
// Vuforia.Trackable[]
struct TrackableU5BU5D_t3798  : public Array_t
{
};
// Vuforia.WordTemplateMode[]
// Vuforia.WordTemplateMode[]
struct WordTemplateModeU5BU5D_t5534  : public Array_t
{
};
// Vuforia.DataSetImpl[]
// Vuforia.DataSetImpl[]
struct DataSetImplU5BU5D_t3841  : public Array_t
{
};
// Vuforia.DataSet[]
// Vuforia.DataSet[]
struct DataSetU5BU5D_t3856  : public Array_t
{
};
// Vuforia.Marker[]
// Vuforia.Marker[]
struct MarkerU5BU5D_t3870  : public Array_t
{
};
// Vuforia.QCARManagerImpl/TrackableResultData[]
// Vuforia.QCARManagerImpl/TrackableResultData[]
#pragma pack(push, tp, 1)
struct TrackableResultDataU5BU5D_t653  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/WordData[]
// Vuforia.QCARManagerImpl/WordData[]
#pragma pack(push, tp, 1)
struct WordDataU5BU5D_t654  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/WordResultData[]
// Vuforia.QCARManagerImpl/WordResultData[]
#pragma pack(push, tp, 1)
struct WordResultDataU5BU5D_t655  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData[]
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData[]
#pragma pack(push, tp, 1)
struct SmartTerrainRevisionDataU5BU5D_t671  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/SurfaceData[]
// Vuforia.QCARManagerImpl/SurfaceData[]
#pragma pack(push, tp, 1)
struct SurfaceDataU5BU5D_t672  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARManagerImpl/PropData[]
// Vuforia.QCARManagerImpl/PropData[]
#pragma pack(push, tp, 1)
struct PropDataU5BU5D_t673  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.QCARRenderer/VideoBackgroundReflection[]
// Vuforia.QCARRenderer/VideoBackgroundReflection[]
struct VideoBackgroundReflectionU5BU5D_t5535  : public Array_t
{
};
// Vuforia.QCARRendererImpl/RenderEvent[]
// Vuforia.QCARRendererImpl/RenderEvent[]
struct RenderEventU5BU5D_t5536  : public Array_t
{
};
// Vuforia.SmartTerrainTrackable[]
// Vuforia.SmartTerrainTrackable[]
struct SmartTerrainTrackableU5BU5D_t3913  : public Array_t
{
};
// Vuforia.TextTrackerImpl/UpDirection[]
// Vuforia.TextTrackerImpl/UpDirection[]
struct UpDirectionU5BU5D_t5537  : public Array_t
{
};
// Vuforia.RectangleData[]
// Vuforia.RectangleData[]
#pragma pack(push, tp, 1)
struct RectangleDataU5BU5D_t685  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.WordPrefabCreationMode[]
// Vuforia.WordPrefabCreationMode[]
struct WordPrefabCreationModeU5BU5D_t5538  : public Array_t
{
};
// Vuforia.WordResult[]
// Vuforia.WordResult[]
struct WordResultU5BU5D_t3960  : public Array_t
{
};
// Vuforia.Word[]
// Vuforia.Word[]
struct WordU5BU5D_t3984  : public Array_t
{
};
// Vuforia.ILoadLevelEventHandler[]
// Vuforia.ILoadLevelEventHandler[]
struct ILoadLevelEventHandlerU5BU5D_t4036  : public Array_t
{
};
// Vuforia.ISmartTerrainEventHandler[]
// Vuforia.ISmartTerrainEventHandler[]
struct ISmartTerrainEventHandlerU5BU5D_t4064  : public Array_t
{
};
// Vuforia.Surface[]
// Vuforia.Surface[]
struct SurfaceU5BU5D_t4078  : public Array_t
{
};
// Vuforia.Prop[]
// Vuforia.Prop[]
struct PropU5BU5D_t4108  : public Array_t
{
};
// Vuforia.QCARManagerImpl/VirtualButtonData[]
// Vuforia.QCARManagerImpl/VirtualButtonData[]
#pragma pack(push, tp, 1)
struct VirtualButtonDataU5BU5D_t4188  : public Array_t
{
};
#pragma pack(pop, tp)
// Vuforia.TargetFinder/InitState[]
// Vuforia.TargetFinder/InitState[]
struct InitStateU5BU5D_t5539  : public Array_t
{
};
// Vuforia.TargetFinder/UpdateState[]
// Vuforia.TargetFinder/UpdateState[]
struct UpdateStateU5BU5D_t5540  : public Array_t
{
};
// Vuforia.TargetFinder/TargetSearchResult[]
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t4219  : public Array_t
{
};
// Vuforia.ImageTarget[]
// Vuforia.ImageTarget[]
struct ImageTargetU5BU5D_t876  : public Array_t
{
};
// Vuforia.ObjectTarget[]
// Vuforia.ObjectTarget[]
struct ObjectTargetU5BU5D_t5541  : public Array_t
{
};
// Vuforia.ExtendedTrackable[]
// Vuforia.ExtendedTrackable[]
struct ExtendedTrackableU5BU5D_t5542  : public Array_t
{
};
// Vuforia.VirtualButton/Sensitivity[]
// Vuforia.VirtualButton/Sensitivity[]
struct SensitivityU5BU5D_t5543  : public Array_t
{
};
// Vuforia.WebCamProfile/ProfileData[]
// Vuforia.WebCamProfile/ProfileData[]
struct ProfileDataU5BU5D_t4263  : public Array_t
{
};
// Vuforia.QCARUnity/InitError[]
// Vuforia.QCARUnity/InitError[]
struct InitErrorU5BU5D_t5544  : public Array_t
{
};
// Vuforia.QCARUnity/QCARHint[]
// Vuforia.QCARUnity/QCARHint[]
struct QCARHintU5BU5D_t5545  : public Array_t
{
};
// Vuforia.QCARUnity/StorageType[]
// Vuforia.QCARUnity/StorageType[]
struct StorageTypeU5BU5D_t5546  : public Array_t
{
};
// Vuforia.ITrackerEventHandler[]
// Vuforia.ITrackerEventHandler[]
struct ITrackerEventHandlerU5BU5D_t4313  : public Array_t
{
};
// Vuforia.QCARAbstractBehaviour/WorldCenterMode[]
// Vuforia.QCARAbstractBehaviour/WorldCenterMode[]
struct WorldCenterModeU5BU5D_t5547  : public Array_t
{
};
// Vuforia.QCARRuntimeUtilities/WebCamUsed[]
// Vuforia.QCARRuntimeUtilities/WebCamUsed[]
struct WebCamUsedU5BU5D_t5548  : public Array_t
{
};
// Vuforia.ITextRecoEventHandler[]
// Vuforia.ITextRecoEventHandler[]
struct ITextRecoEventHandlerU5BU5D_t4347  : public Array_t
{
};
// Vuforia.IUserDefinedTargetEventHandler[]
// Vuforia.IUserDefinedTargetEventHandler[]
struct IUserDefinedTargetEventHandlerU5BU5D_t4367  : public Array_t
{
};
// Vuforia.IVirtualButtonEventHandler[]
// Vuforia.IVirtualButtonEventHandler[]
struct IVirtualButtonEventHandlerU5BU5D_t4406  : public Array_t
{
};
// Vuforia.WordFilterMode[]
// Vuforia.WordFilterMode[]
struct WordFilterModeU5BU5D_t5549  : public Array_t
{
};
