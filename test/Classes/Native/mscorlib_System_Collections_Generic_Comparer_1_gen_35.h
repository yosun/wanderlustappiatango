﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.ILoadLevelEventHandler>
struct Comparer_1_t4048;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.ILoadLevelEventHandler>
struct Comparer_1_t4048  : public Object_t
{
};
struct Comparer_1_t4048_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.ILoadLevelEventHandler>::_default
	Comparer_1_t4048 * ____default_0;
};
