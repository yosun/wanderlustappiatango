﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.GraphicRaycaster>
struct UnityAction_1_t3365;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.GraphicRaycaster>
struct InvokableCall_1_t3364  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.UI.GraphicRaycaster>::Delegate
	UnityAction_1_t3365 * ___Delegate_0;
};
