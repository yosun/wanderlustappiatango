﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,Vuforia.SurfaceAbstractBehaviour>
struct Transform_1_t4103;
// System.Object
struct Object_t;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t77;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,Vuforia.SurfaceAbstractBehaviour>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m24407 (Transform_1_t4103 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,Vuforia.SurfaceAbstractBehaviour>::Invoke(TKey,TValue)
 SurfaceAbstractBehaviour_t77 * Transform_1_Invoke_m24408 (Transform_1_t4103 * __this, int32_t ___key, SurfaceAbstractBehaviour_t77 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,Vuforia.SurfaceAbstractBehaviour>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m24409 (Transform_1_t4103 * __this, int32_t ___key, SurfaceAbstractBehaviour_t77 * ___value, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.SurfaceAbstractBehaviour,Vuforia.SurfaceAbstractBehaviour>::EndInvoke(System.IAsyncResult)
 SurfaceAbstractBehaviour_t77 * Transform_1_EndInvoke_m24410 (Transform_1_t4103 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
