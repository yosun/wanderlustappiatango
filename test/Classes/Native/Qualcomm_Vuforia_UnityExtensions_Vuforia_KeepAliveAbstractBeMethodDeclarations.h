﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.KeepAliveAbstractBehaviour
struct KeepAliveAbstractBehaviour_t64;
// Vuforia.ILoadLevelEventHandler
struct ILoadLevelEventHandler_t711;

// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepARCameraAlive()
 bool KeepAliveAbstractBehaviour_get_KeepARCameraAlive_m3927 (KeepAliveAbstractBehaviour_t64 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepARCameraAlive(System.Boolean)
 void KeepAliveAbstractBehaviour_set_KeepARCameraAlive_m3928 (KeepAliveAbstractBehaviour_t64 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTrackableBehavioursAlive()
 bool KeepAliveAbstractBehaviour_get_KeepTrackableBehavioursAlive_m3929 (KeepAliveAbstractBehaviour_t64 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTrackableBehavioursAlive(System.Boolean)
 void KeepAliveAbstractBehaviour_set_KeepTrackableBehavioursAlive_m3930 (KeepAliveAbstractBehaviour_t64 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepTextRecoBehaviourAlive()
 bool KeepAliveAbstractBehaviour_get_KeepTextRecoBehaviourAlive_m3931 (KeepAliveAbstractBehaviour_t64 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepTextRecoBehaviourAlive(System.Boolean)
 void KeepAliveAbstractBehaviour_set_KeepTextRecoBehaviourAlive_m3932 (KeepAliveAbstractBehaviour_t64 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepUDTBuildingBehaviourAlive()
 bool KeepAliveAbstractBehaviour_get_KeepUDTBuildingBehaviourAlive_m3933 (KeepAliveAbstractBehaviour_t64 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepUDTBuildingBehaviourAlive(System.Boolean)
 void KeepAliveAbstractBehaviour_set_KeepUDTBuildingBehaviourAlive_m3934 (KeepAliveAbstractBehaviour_t64 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepSmartTerrainAlive()
 bool KeepAliveAbstractBehaviour_get_KeepSmartTerrainAlive_m3935 (KeepAliveAbstractBehaviour_t64 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepSmartTerrainAlive(System.Boolean)
 void KeepAliveAbstractBehaviour_set_KeepSmartTerrainAlive_m3936 (KeepAliveAbstractBehaviour_t64 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::get_KeepCloudRecoBehaviourAlive()
 bool KeepAliveAbstractBehaviour_get_KeepCloudRecoBehaviourAlive_m3937 (KeepAliveAbstractBehaviour_t64 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::set_KeepCloudRecoBehaviourAlive(System.Boolean)
 void KeepAliveAbstractBehaviour_set_KeepCloudRecoBehaviourAlive_m3938 (KeepAliveAbstractBehaviour_t64 * __this, bool ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.KeepAliveAbstractBehaviour Vuforia.KeepAliveAbstractBehaviour::get_Instance()
 KeepAliveAbstractBehaviour_t64 * KeepAliveAbstractBehaviour_get_Instance_m3939 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::RegisterEventHandler(Vuforia.ILoadLevelEventHandler)
 void KeepAliveAbstractBehaviour_RegisterEventHandler_m3940 (KeepAliveAbstractBehaviour_t64 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.KeepAliveAbstractBehaviour::UnregisterEventHandler(Vuforia.ILoadLevelEventHandler)
 bool KeepAliveAbstractBehaviour_UnregisterEventHandler_m3941 (KeepAliveAbstractBehaviour_t64 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::OnLevelWasLoaded()
 void KeepAliveAbstractBehaviour_OnLevelWasLoaded_m3942 (KeepAliveAbstractBehaviour_t64 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.KeepAliveAbstractBehaviour::.ctor()
 void KeepAliveAbstractBehaviour__ctor_m473 (KeepAliveAbstractBehaviour_t64 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
