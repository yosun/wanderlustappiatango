﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription
struct RSAPKCS1SHA1SignatureDescription_t2124;

// System.Void System.Security.Cryptography.RSAPKCS1SHA1SignatureDescription::.ctor()
 void RSAPKCS1SHA1SignatureDescription__ctor_m12121 (RSAPKCS1SHA1SignatureDescription_t2124 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
