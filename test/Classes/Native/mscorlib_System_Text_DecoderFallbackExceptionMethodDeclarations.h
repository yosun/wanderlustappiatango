﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.DecoderFallbackException
struct DecoderFallbackException_t2146;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t609;

// System.Void System.Text.DecoderFallbackException::.ctor()
 void DecoderFallbackException__ctor_m12243 (DecoderFallbackException_t2146 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.DecoderFallbackException::.ctor(System.String)
 void DecoderFallbackException__ctor_m12244 (DecoderFallbackException_t2146 * __this, String_t* ___message, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.DecoderFallbackException::.ctor(System.String,System.Byte[],System.Int32)
 void DecoderFallbackException__ctor_m12245 (DecoderFallbackException_t2146 * __this, String_t* ___message, ByteU5BU5D_t609* ___bytesUnknown, int32_t ___index, MethodInfo* method) IL2CPP_METHOD_ATTR;
