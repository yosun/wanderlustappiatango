﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Button
struct Button_t270;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t268;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t188;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t184;
// System.Collections.IEnumerator
struct IEnumerator_t266;

// System.Void UnityEngine.UI.Button::.ctor()
 void Button__ctor_m978 (Button_t270 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
 ButtonClickedEvent_t268 * Button_get_onClick_m979 (Button_t270 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::set_onClick(UnityEngine.UI.Button/ButtonClickedEvent)
 void Button_set_onClick_m980 (Button_t270 * __this, ButtonClickedEvent_t268 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::Press()
 void Button_Press_m981 (Button_t270 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
 void Button_OnPointerClick_m982 (Button_t270 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Button::OnSubmit(UnityEngine.EventSystems.BaseEventData)
 void Button_OnSubmit_m983 (Button_t270 * __this, BaseEventData_t184 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.UI.Button::OnFinishSubmit()
 Object_t * Button_OnFinishSubmit_m984 (Button_t270 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
