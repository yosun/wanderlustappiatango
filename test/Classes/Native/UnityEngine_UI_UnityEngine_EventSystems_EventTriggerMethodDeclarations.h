﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.EventTrigger
struct EventTrigger_t195;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t184;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t188;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t196;
// UnityEngine.EventSystems.EventTriggerType
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTriggerType.h"

// System.Void UnityEngine.EventSystems.EventTrigger::.ctor()
 void EventTrigger__ctor_m708 (EventTrigger_t195 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::Execute(UnityEngine.EventSystems.EventTriggerType,UnityEngine.EventSystems.BaseEventData)
 void EventTrigger_Execute_m709 (EventTrigger_t195 * __this, int32_t ___id, BaseEventData_t184 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnPointerEnter_m710 (EventTrigger_t195 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnPointerExit_m711 (EventTrigger_t195 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnDrag(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnDrag_m712 (EventTrigger_t195 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnDrop(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnDrop_m713 (EventTrigger_t195 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnPointerDown_m714 (EventTrigger_t195 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnPointerUp_m715 (EventTrigger_t195 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnPointerClick_m716 (EventTrigger_t195 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnSelect(UnityEngine.EventSystems.BaseEventData)
 void EventTrigger_OnSelect_m717 (EventTrigger_t195 * __this, BaseEventData_t184 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnDeselect(UnityEngine.EventSystems.BaseEventData)
 void EventTrigger_OnDeselect_m718 (EventTrigger_t195 * __this, BaseEventData_t184 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnScroll(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnScroll_m719 (EventTrigger_t195 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnMove(UnityEngine.EventSystems.AxisEventData)
 void EventTrigger_OnMove_m720 (EventTrigger_t195 * __this, AxisEventData_t196 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnUpdateSelected(UnityEngine.EventSystems.BaseEventData)
 void EventTrigger_OnUpdateSelected_m721 (EventTrigger_t195 * __this, BaseEventData_t184 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnInitializePotentialDrag_m722 (EventTrigger_t195 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnBeginDrag_m723 (EventTrigger_t195 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
 void EventTrigger_OnEndDrag_m724 (EventTrigger_t195 * __this, PointerEventData_t188 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnSubmit(UnityEngine.EventSystems.BaseEventData)
 void EventTrigger_OnSubmit_m725 (EventTrigger_t195 * __this, BaseEventData_t184 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.EventTrigger::OnCancel(UnityEngine.EventSystems.BaseEventData)
 void EventTrigger_OnCancel_m726 (EventTrigger_t195 * __this, BaseEventData_t184 * ___eventData, MethodInfo* method) IL2CPP_METHOD_ATTR;
