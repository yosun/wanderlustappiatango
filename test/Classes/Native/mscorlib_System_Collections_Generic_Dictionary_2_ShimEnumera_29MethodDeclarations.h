﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>
struct ShimEnumerator_t4867;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_t1344;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m29920 (ShimEnumerator_t4867 * __this, Dictionary_2_t1344 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>::MoveNext()
 bool ShimEnumerator_MoveNext_m29921 (ShimEnumerator_t4867 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m29922 (ShimEnumerator_t4867 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>::get_Key()
 Object_t * ShimEnumerator_get_Key_m29923 (ShimEnumerator_t4867 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>::get_Value()
 Object_t * ShimEnumerator_get_Value_m29924 (ShimEnumerator_t4867 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.String,System.Boolean>::get_Current()
 Object_t * ShimEnumerator_get_Current_m29925 (ShimEnumerator_t4867 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
