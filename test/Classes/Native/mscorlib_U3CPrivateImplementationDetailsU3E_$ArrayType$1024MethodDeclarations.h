﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$1024
struct $ArrayType$1024_t2277;
struct $ArrayType$1024_t2277_marshaled;

void $ArrayType$1024_t2277_marshal(const $ArrayType$1024_t2277& unmarshaled, $ArrayType$1024_t2277_marshaled& marshaled);
void $ArrayType$1024_t2277_marshal_back(const $ArrayType$1024_t2277_marshaled& marshaled, $ArrayType$1024_t2277& unmarshaled);
void $ArrayType$1024_t2277_marshal_cleanup($ArrayType$1024_t2277_marshaled& marshaled);
