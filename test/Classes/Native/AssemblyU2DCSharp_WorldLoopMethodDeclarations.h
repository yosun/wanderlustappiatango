﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// WorldLoop
struct WorldLoop_t24;

// System.Void WorldLoop::.ctor()
 void WorldLoop__ctor_m60 (WorldLoop_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldLoop::Awake()
 void WorldLoop_Awake_m61 (WorldLoop_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldLoop::Start()
 void WorldLoop_Start_m62 (WorldLoop_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldLoop::Update()
 void WorldLoop_Update_m63 (WorldLoop_t24 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
