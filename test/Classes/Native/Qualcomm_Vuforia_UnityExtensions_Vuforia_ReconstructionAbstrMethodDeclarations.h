﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t72;
// Vuforia.Reconstruction
struct Reconstruction_t576;
// Vuforia.ISmartTerrainEventHandler
struct ISmartTerrainEventHandler_t718;
// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t713;
// System.Action`1<Vuforia.Prop>
struct Action_1_t130;
// System.Action`1<Vuforia.Surface>
struct Action_1_t131;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t69;
// Vuforia.Prop
struct Prop_t42;
// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t77;
// Vuforia.Surface
struct Surface_t43;
// System.Collections.Generic.IEnumerable`1<Vuforia.Prop>
struct IEnumerable_1_t719;
// System.Collections.Generic.IEnumerable`1<Vuforia.Surface>
struct IEnumerable_1_t720;
// Vuforia.QCARManagerImpl/SmartTerrainRevisionData[]
struct SmartTerrainRevisionDataU5BU5D_t671;
// Vuforia.QCARManagerImpl/SurfaceData[]
struct SurfaceDataU5BU5D_t672;
// Vuforia.QCARManagerImpl/PropData[]
struct PropDataU5BU5D_t673;
// Vuforia.SmartTerrainTrackable
struct SmartTerrainTrackable_t581;
// UnityEngine.Mesh
struct Mesh_t174;
// System.Int32[]
struct Int32U5BU5D_t21;
// System.Collections.Generic.List`1<Vuforia.Prop>
struct List_1_t721;
// System.Collections.Generic.List`1<Vuforia.Surface>
struct List_1_t722;
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"
// Vuforia.QCARManagerImpl/MeshData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Mes.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// Vuforia.Reconstruction Vuforia.ReconstructionAbstractBehaviour::get_Reconstruction()
 Object_t * ReconstructionAbstractBehaviour_get_Reconstruction_m3943 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Start()
 void ReconstructionAbstractBehaviour_Start_m3944 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::OnDrawGizmos()
 void ReconstructionAbstractBehaviour_OnDrawGizmos_m3945 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterSmartTerrainEventHandler(Vuforia.ISmartTerrainEventHandler)
 void ReconstructionAbstractBehaviour_RegisterSmartTerrainEventHandler_m3946 (ReconstructionAbstractBehaviour_t72 * __this, Object_t * ___trackableEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::UnregisterSmartTerrainEventHandler(Vuforia.ISmartTerrainEventHandler)
 bool ReconstructionAbstractBehaviour_UnregisterSmartTerrainEventHandler_m3947 (ReconstructionAbstractBehaviour_t72 * __this, Object_t * ___trackableEventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterInitializedCallback(System.Action`1<Vuforia.SmartTerrainInitializationInfo>)
 void ReconstructionAbstractBehaviour_RegisterInitializedCallback_m3948 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t713 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterInitializedCallback(System.Action`1<Vuforia.SmartTerrainInitializationInfo>)
 void ReconstructionAbstractBehaviour_UnregisterInitializedCallback_m3949 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t713 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterPropCreatedCallback(System.Action`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_RegisterPropCreatedCallback_m406 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t130 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterPropCreatedCallback(System.Action`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_UnregisterPropCreatedCallback_m409 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t130 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterPropUpdatedCallback(System.Action`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_RegisterPropUpdatedCallback_m3950 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t130 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterPropUpdatedCallback(System.Action`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_UnregisterPropUpdatedCallback_m3951 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t130 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterPropDeletedCallback(System.Action`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_RegisterPropDeletedCallback_m3952 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t130 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterPropDeletedCallback(System.Action`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_UnregisterPropDeletedCallback_m3953 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t130 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterSurfaceCreatedCallback(System.Action`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_RegisterSurfaceCreatedCallback_m408 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t131 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterSurfaceCreatedCallback(System.Action`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_UnregisterSurfaceCreatedCallback_m410 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t131 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterSurfaceUpdatedCallback(System.Action`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_RegisterSurfaceUpdatedCallback_m3954 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t131 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterSurfaceUpdatedCallback(System.Action`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_UnregisterSurfaceUpdatedCallback_m3955 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t131 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::RegisterSurfaceDeletedCallback(System.Action`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_RegisterSurfaceDeletedCallback_m3956 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t131 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterSurfaceDeletedCallback(System.Action`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_UnregisterSurfaceDeletedCallback_m3957 (ReconstructionAbstractBehaviour_t72 * __this, Action_1_t131 * ___callback, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.PropAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::AssociateProp(Vuforia.PropAbstractBehaviour,Vuforia.Prop)
 PropAbstractBehaviour_t69 * ReconstructionAbstractBehaviour_AssociateProp_m411 (ReconstructionAbstractBehaviour_t72 * __this, PropAbstractBehaviour_t69 * ___templateBehaviour, Object_t * ___newProp, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SurfaceAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::AssociateSurface(Vuforia.SurfaceAbstractBehaviour,Vuforia.Surface)
 SurfaceAbstractBehaviour_t77 * ReconstructionAbstractBehaviour_AssociateSurface_m412 (ReconstructionAbstractBehaviour_t72 * __this, SurfaceAbstractBehaviour_t77 * ___templateBehaviour, Object_t * ___newSurface, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.Prop> Vuforia.ReconstructionAbstractBehaviour::GetActiveProps()
 Object_t* ReconstructionAbstractBehaviour_GetActiveProps_m3958 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::TryGetPropBehaviour(Vuforia.Prop,Vuforia.PropAbstractBehaviour&)
 bool ReconstructionAbstractBehaviour_TryGetPropBehaviour_m3959 (ReconstructionAbstractBehaviour_t72 * __this, Object_t * ___prop, PropAbstractBehaviour_t69 ** ___behaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.Surface> Vuforia.ReconstructionAbstractBehaviour::GetActiveSurfaces()
 Object_t* ReconstructionAbstractBehaviour_GetActiveSurfaces_m3960 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::TryGetSurfaceBehaviour(Vuforia.Surface,Vuforia.SurfaceAbstractBehaviour&)
 bool ReconstructionAbstractBehaviour_TryGetSurfaceBehaviour_m3961 (ReconstructionAbstractBehaviour_t72 * __this, Object_t * ___surface, SurfaceAbstractBehaviour_t77 ** ___behaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Initialize(Vuforia.Reconstruction)
 void ReconstructionAbstractBehaviour_Initialize_m3962 (ReconstructionAbstractBehaviour_t72 * __this, Object_t * ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Deinitialize()
 void ReconstructionAbstractBehaviour_Deinitialize_m3963 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UpdateSmartTerrainData(Vuforia.QCARManagerImpl/SmartTerrainRevisionData[],Vuforia.QCARManagerImpl/SurfaceData[],Vuforia.QCARManagerImpl/PropData[])
 void ReconstructionAbstractBehaviour_UpdateSmartTerrainData_m3964 (ReconstructionAbstractBehaviour_t72 * __this, SmartTerrainRevisionDataU5BU5D_t671* ___smartTerrainRevisions, SurfaceDataU5BU5D_t672* ___updatedSurfaces, PropDataU5BU5D_t673* ___updatedProps, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::SetBehavioursToNotFound()
 void ReconstructionAbstractBehaviour_SetBehavioursToNotFound_m3965 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::ClearOnReset()
 void ReconstructionAbstractBehaviour_ClearOnReset_m3966 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::OnReconstructionRemoved()
 void ReconstructionAbstractBehaviour_OnReconstructionRemoved_m3967 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.PropAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::InstantiatePropBehaviour(Vuforia.PropAbstractBehaviour)
 PropAbstractBehaviour_t69 * ReconstructionAbstractBehaviour_InstantiatePropBehaviour_m3968 (Object_t * __this/* static, unused */, PropAbstractBehaviour_t69 * ___input, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::AssociatePropBehaviour(Vuforia.Prop,Vuforia.PropAbstractBehaviour)
 void ReconstructionAbstractBehaviour_AssociatePropBehaviour_m3969 (ReconstructionAbstractBehaviour_t72 * __this, Object_t * ___trackable, PropAbstractBehaviour_t69 * ___behaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SurfaceAbstractBehaviour Vuforia.ReconstructionAbstractBehaviour::InstantiateSurfaceBehaviour(Vuforia.SurfaceAbstractBehaviour)
 SurfaceAbstractBehaviour_t77 * ReconstructionAbstractBehaviour_InstantiateSurfaceBehaviour_m3970 (Object_t * __this/* static, unused */, SurfaceAbstractBehaviour_t77 * ___input, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::AssociateSurfaceBehaviour(Vuforia.Surface,Vuforia.SurfaceAbstractBehaviour)
 void ReconstructionAbstractBehaviour_AssociateSurfaceBehaviour_m3971 (ReconstructionAbstractBehaviour_t72 * __this, Object_t * ___trackable, SurfaceAbstractBehaviour_t77 * ___behaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.SmartTerrainTrackable Vuforia.ReconstructionAbstractBehaviour::FindSmartTerrainTrackable(System.Int32)
 Object_t * ReconstructionAbstractBehaviour_FindSmartTerrainTrackable_m3972 (ReconstructionAbstractBehaviour_t72 * __this, int32_t ___id, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::NotifySurfaceEventHandlers(System.Collections.Generic.IEnumerable`1<Vuforia.Surface>,System.Collections.Generic.IEnumerable`1<Vuforia.Surface>,System.Collections.Generic.IEnumerable`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_NotifySurfaceEventHandlers_m3973 (ReconstructionAbstractBehaviour_t72 * __this, Object_t* ___newSurfaces, Object_t* ___updatedSurfaces, Object_t* ___deletedSurfaces, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::NotifyPropEventHandlers(System.Collections.Generic.IEnumerable`1<Vuforia.Prop>,System.Collections.Generic.IEnumerable`1<Vuforia.Prop>,System.Collections.Generic.IEnumerable`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_NotifyPropEventHandlers_m3974 (ReconstructionAbstractBehaviour_t72 * __this, Object_t* ___newProps, Object_t* ___updatedProps, Object_t* ___deletedProps, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Mesh Vuforia.ReconstructionAbstractBehaviour::UpdateMesh(Vuforia.QCARManagerImpl/MeshData,UnityEngine.Mesh,System.Boolean)
 Mesh_t174 * ReconstructionAbstractBehaviour_UpdateMesh_m3975 (Object_t * __this/* static, unused */, MeshData_t646  ___meshData, Mesh_t174 * ___oldMesh, bool ___setNormalsUpwards, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Vuforia.ReconstructionAbstractBehaviour::ReadMeshBoundaries(System.Int32,System.IntPtr)
 Int32U5BU5D_t21* ReconstructionAbstractBehaviour_ReadMeshBoundaries_m3976 (Object_t * __this/* static, unused */, int32_t ___numBoundaries, IntPtr_t121 ___boundaryArray, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterDeletedProps(System.Collections.Generic.List`1<Vuforia.Prop>)
 void ReconstructionAbstractBehaviour_UnregisterDeletedProps_m3977 (ReconstructionAbstractBehaviour_t72 * __this, List_1_t721 * ___deletedProps, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UnregisterDeletedSurfaces(System.Collections.Generic.List`1<Vuforia.Surface>)
 void ReconstructionAbstractBehaviour_UnregisterDeletedSurfaces_m3978 (ReconstructionAbstractBehaviour_t72 * __this, List_1_t722 * ___deletedSurfaces, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UpdateSurfaces(Vuforia.QCARManagerImpl/SmartTerrainRevisionData[],Vuforia.QCARManagerImpl/SurfaceData[])
 void ReconstructionAbstractBehaviour_UpdateSurfaces_m3979 (ReconstructionAbstractBehaviour_t72 * __this, SmartTerrainRevisionDataU5BU5D_t671* ___smartTerrainRevisions, SurfaceDataU5BU5D_t672* ___updatedSurfaceData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::UpdateProps(Vuforia.QCARManagerImpl/SmartTerrainRevisionData[],Vuforia.QCARManagerImpl/PropData[])
 void ReconstructionAbstractBehaviour_UpdateProps_m3980 (ReconstructionAbstractBehaviour_t72 * __this, SmartTerrainRevisionDataU5BU5D_t671* ___smartTerrainRevisions, PropDataU5BU5D_t673* ___updatedPropData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_InitializedInEditor()
 bool ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_InitializedInEditor_m556 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetInitializedInEditor(System.Boolean)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetInitializedInEditor_m557 (ReconstructionAbstractBehaviour_t72 * __this, bool ___initializedInEditor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetMaximumExtentEnabled(System.Boolean)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtentEnabled_m558 (ReconstructionAbstractBehaviour_t72 * __this, bool ___maxExtendEnabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_MaximumExtentEnabled()
 bool ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtentEnabled_m559 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetMaximumExtent(UnityEngine.Rect)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetMaximumExtent_m560 (ReconstructionAbstractBehaviour_t72 * __this, Rect_t118  ___rectangle, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_MaximumExtent()
 Rect_t118  ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_MaximumExtent_m561 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetAutomaticStart(System.Boolean)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetAutomaticStart_m562 (ReconstructionAbstractBehaviour_t72 * __this, bool ___autoStart, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_AutomaticStart()
 bool ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_AutomaticStart_m563 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetNavMeshUpdates(System.Boolean)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshUpdates_m564 (ReconstructionAbstractBehaviour_t72 * __this, bool ___navMeshUpdates, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_NavMeshUpdates()
 bool ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshUpdates_m565 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.SetNavMeshPadding(System.Single)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_SetNavMeshPadding_m566 (ReconstructionAbstractBehaviour_t72 * __this, float ___navMeshPadding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.get_NavMeshPadding()
 float ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_get_NavMeshPadding_m567 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.ScaleEditorMeshesByFactor(System.Single)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorMeshesByFactor_m568 (ReconstructionAbstractBehaviour_t72 * __this, float ___scaleFactor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::Vuforia.IEditorReconstructionBehaviour.ScaleEditorPropPositionsByFactor(System.Single)
 void ReconstructionAbstractBehaviour_Vuforia_IEditorReconstructionBehaviour_ScaleEditorPropPositionsByFactor_m569 (ReconstructionAbstractBehaviour_t72 * __this, float ___scaleFactor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.ReconstructionAbstractBehaviour::.ctor()
 void ReconstructionAbstractBehaviour__ctor_m555 (ReconstructionAbstractBehaviour_t72 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
