﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<TestParticles>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_10.h"
// UnityEngine.Events.CachedInvokableCall`1<TestParticles>
struct CachedInvokableCall_1_t2770  : public InvokableCall_1_t2771
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<TestParticles>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
