﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<UnityEngine.GUILayoutUtility/LayoutCache>
struct EqualityComparer_1_t4520;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<UnityEngine.GUILayoutUtility/LayoutCache>
struct EqualityComparer_1_t4520  : public Object_t
{
};
struct EqualityComparer_1_t4520_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.GUILayoutUtility/LayoutCache>::_default
	EqualityComparer_1_t4520 * ____default_0;
};
