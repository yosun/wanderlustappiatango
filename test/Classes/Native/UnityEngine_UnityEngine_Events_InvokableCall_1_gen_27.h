﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.PropBehaviour>
struct UnityAction_1_t2910;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>
struct InvokableCall_1_t2909  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.PropBehaviour>::Delegate
	UnityAction_1_t2910 * ___Delegate_0;
};
