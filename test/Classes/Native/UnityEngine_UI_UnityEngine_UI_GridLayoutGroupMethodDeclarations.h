﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t386;
// UnityEngine.UI.GridLayoutGroup/Corner
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
// UnityEngine.UI.GridLayoutGroup/Axis
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// UnityEngine.UI.GridLayoutGroup/Constraint
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"

// System.Void UnityEngine.UI.GridLayoutGroup::.ctor()
 void GridLayoutGroup__ctor_m1689 (GridLayoutGroup_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::get_startCorner()
 int32_t GridLayoutGroup_get_startCorner_m1690 (GridLayoutGroup_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_startCorner(UnityEngine.UI.GridLayoutGroup/Corner)
 void GridLayoutGroup_set_startCorner_m1691 (GridLayoutGroup_t386 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::get_startAxis()
 int32_t GridLayoutGroup_get_startAxis_m1692 (GridLayoutGroup_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_startAxis(UnityEngine.UI.GridLayoutGroup/Axis)
 void GridLayoutGroup_set_startAxis_m1693 (GridLayoutGroup_t386 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_cellSize()
 Vector2_t9  GridLayoutGroup_get_cellSize_m1694 (GridLayoutGroup_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_cellSize(UnityEngine.Vector2)
 void GridLayoutGroup_set_cellSize_m1695 (GridLayoutGroup_t386 * __this, Vector2_t9  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::get_spacing()
 Vector2_t9  GridLayoutGroup_get_spacing_m1696 (GridLayoutGroup_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_spacing(UnityEngine.Vector2)
 void GridLayoutGroup_set_spacing_m1697 (GridLayoutGroup_t386 * __this, Vector2_t9  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::get_constraint()
 int32_t GridLayoutGroup_get_constraint_m1698 (GridLayoutGroup_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraint(UnityEngine.UI.GridLayoutGroup/Constraint)
 void GridLayoutGroup_set_constraint_m1699 (GridLayoutGroup_t386 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.UI.GridLayoutGroup::get_constraintCount()
 int32_t GridLayoutGroup_get_constraintCount_m1700 (GridLayoutGroup_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::set_constraintCount(System.Int32)
 void GridLayoutGroup_set_constraintCount_m1701 (GridLayoutGroup_t386 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputHorizontal()
 void GridLayoutGroup_CalculateLayoutInputHorizontal_m1702 (GridLayoutGroup_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::CalculateLayoutInputVertical()
 void GridLayoutGroup_CalculateLayoutInputVertical_m1703 (GridLayoutGroup_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutHorizontal()
 void GridLayoutGroup_SetLayoutHorizontal_m1704 (GridLayoutGroup_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::SetLayoutVertical()
 void GridLayoutGroup_SetLayoutVertical_m1705 (GridLayoutGroup_t386 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.GridLayoutGroup::SetCellsAlongAxis(System.Int32)
 void GridLayoutGroup_SetCellsAlongAxis_m1706 (GridLayoutGroup_t386 * __this, int32_t ___axis, MethodInfo* method) IL2CPP_METHOD_ATTR;
