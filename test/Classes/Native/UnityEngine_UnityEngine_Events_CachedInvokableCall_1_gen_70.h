﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.Toggle>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_71.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Toggle>
struct CachedInvokableCall_1_t3524  : public InvokableCall_1_t3525
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.Toggle>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
