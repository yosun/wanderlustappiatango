﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<Vuforia.ImageTarget>
struct Comparison_1_t4254;
// System.Object
struct Object_t;
// Vuforia.ImageTarget
struct ImageTarget_t572;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<Vuforia.ImageTarget>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"
#define Comparison_1__ctor_m25920(__this, ___object, ___method, method) (void)Comparison_1__ctor_m14775_gshared((Comparison_1_t2846 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// System.Int32 System.Comparison`1<Vuforia.ImageTarget>::Invoke(T,T)
#define Comparison_1_Invoke_m25921(__this, ___x, ___y, method) (int32_t)Comparison_1_Invoke_m14776_gshared((Comparison_1_t2846 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.IAsyncResult System.Comparison`1<Vuforia.ImageTarget>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m25922(__this, ___x, ___y, ___callback, ___object, method) (Object_t *)Comparison_1_BeginInvoke_m14777_gshared((Comparison_1_t2846 *)__this, (Object_t *)___x, (Object_t *)___y, (AsyncCallback_t200 *)___callback, (Object_t *)___object, method)
// System.Int32 System.Comparison`1<Vuforia.ImageTarget>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m25923(__this, ___result, method) (int32_t)Comparison_1_EndInvoke_m14778_gshared((Comparison_1_t2846 *)__this, (Object_t *)___result, method)
