﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<SetRenderQueueChildren>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_6.h"
// UnityEngine.Events.CachedInvokableCall`1<SetRenderQueueChildren>
struct CachedInvokableCall_1_t2751  : public InvokableCall_1_t2752
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<SetRenderQueueChildren>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
