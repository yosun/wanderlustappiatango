﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.VirtualButtonAbstractBehaviour>
struct EqualityComparer_1_t4213;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.VirtualButtonAbstractBehaviour>
struct EqualityComparer_1_t4213  : public Object_t
{
};
struct EqualityComparer_1_t4213_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.VirtualButtonAbstractBehaviour>::_default
	EqualityComparer_1_t4213 * ____default_0;
};
