﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo RaycastResult_t190_il2cpp_TypeInfo;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t185  : public MulticastDelegate_t325
{
};
