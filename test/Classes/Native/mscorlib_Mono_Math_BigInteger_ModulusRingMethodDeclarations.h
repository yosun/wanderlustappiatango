﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t1756;
// Mono.Math.BigInteger
struct BigInteger_t1751;

// System.Void Mono.Math.BigInteger/ModulusRing::.ctor(Mono.Math.BigInteger)
 void ModulusRing__ctor_m9879 (ModulusRing_t1756 * __this, BigInteger_t1751 * ___modulus, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/ModulusRing::BarrettReduction(Mono.Math.BigInteger)
 void ModulusRing_BarrettReduction_m9880 (ModulusRing_t1756 * __this, BigInteger_t1751 * ___x, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
 BigInteger_t1751 * ModulusRing_Multiply_m9881 (ModulusRing_t1756 * __this, BigInteger_t1751 * ___a, BigInteger_t1751 * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Difference(Mono.Math.BigInteger,Mono.Math.BigInteger)
 BigInteger_t1751 * ModulusRing_Difference_m9882 (ModulusRing_t1756 * __this, BigInteger_t1751 * ___a, BigInteger_t1751 * ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(Mono.Math.BigInteger,Mono.Math.BigInteger)
 BigInteger_t1751 * ModulusRing_Pow_m9883 (ModulusRing_t1756 * __this, BigInteger_t1751 * ___a, BigInteger_t1751 * ___k, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(System.UInt32,Mono.Math.BigInteger)
 BigInteger_t1751 * ModulusRing_Pow_m9884 (ModulusRing_t1756 * __this, uint32_t ___b, BigInteger_t1751 * ___exp, MethodInfo* method) IL2CPP_METHOD_ATTR;
