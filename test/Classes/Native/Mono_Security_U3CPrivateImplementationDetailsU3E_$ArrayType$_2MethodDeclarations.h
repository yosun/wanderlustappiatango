﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$32
struct $ArrayType$32_t1650;
struct $ArrayType$32_t1650_marshaled;

void $ArrayType$32_t1650_marshal(const $ArrayType$32_t1650& unmarshaled, $ArrayType$32_t1650_marshaled& marshaled);
void $ArrayType$32_t1650_marshal_back(const $ArrayType$32_t1650_marshaled& marshaled, $ArrayType$32_t1650& unmarshaled);
void $ArrayType$32_t1650_marshal_cleanup($ArrayType$32_t1650_marshaled& marshaled);
