﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct $ArrayType$12_t2270;
struct $ArrayType$12_t2270_marshaled;

void $ArrayType$12_t2270_marshal(const $ArrayType$12_t2270& unmarshaled, $ArrayType$12_t2270_marshaled& marshaled);
void $ArrayType$12_t2270_marshal_back(const $ArrayType$12_t2270_marshaled& marshaled, $ArrayType$12_t2270& unmarshaled);
void $ArrayType$12_t2270_marshal_cleanup($ArrayType$12_t2270_marshaled& marshaled);
