﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
struct SafeHandleZeroOrMinusOneIsInvalid_t1725;

// System.Void Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid::.ctor(System.Boolean)
 void SafeHandleZeroOrMinusOneIsInvalid__ctor_m9756 (SafeHandleZeroOrMinusOneIsInvalid_t1725 * __this, bool ___ownsHandle, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid::get_IsInvalid()
 bool SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m9757 (SafeHandleZeroOrMinusOneIsInvalid_t1725 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
