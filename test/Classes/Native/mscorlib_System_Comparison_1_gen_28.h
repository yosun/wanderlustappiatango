﻿#pragma once
#include <stdint.h>
// Vuforia.DataSet
struct DataSet_t568;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.DataSet>
struct Comparison_1_t3860  : public MulticastDelegate_t325
{
};
