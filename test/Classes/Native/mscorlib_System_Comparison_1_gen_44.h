﻿#pragma once
#include <stdint.h>
// Vuforia.ITrackerEventHandler
struct ITrackerEventHandler_t761;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.ITrackerEventHandler>
struct Comparison_1_t4319  : public MulticastDelegate_t325
{
};
