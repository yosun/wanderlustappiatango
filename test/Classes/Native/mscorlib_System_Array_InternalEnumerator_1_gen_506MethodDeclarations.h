﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm>
struct InternalEnumerator_1_t4884;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm
#include "System_System_Security_Cryptography_X509Certificates_X509Sub_0.h"

// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30002 (InternalEnumerator_1_t4884 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30003 (InternalEnumerator_1_t4884 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm>::Dispose()
 void InternalEnumerator_1_Dispose_m30004 (InternalEnumerator_1_t4884 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30005 (InternalEnumerator_1_t4884 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m30006 (InternalEnumerator_1_t4884 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
