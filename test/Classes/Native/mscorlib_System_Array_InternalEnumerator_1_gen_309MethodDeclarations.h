﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>
struct InternalEnumerator_1_t3910;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/PropData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Pro.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m22501 (InternalEnumerator_1_t3910 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22502 (InternalEnumerator_1_t3910 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::Dispose()
 void InternalEnumerator_1_Dispose_m22503 (InternalEnumerator_1_t3910 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m22504 (InternalEnumerator_1_t3910 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/PropData>::get_Current()
 PropData_t649  InternalEnumerator_1_get_Current_m22505 (InternalEnumerator_1_t3910 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
