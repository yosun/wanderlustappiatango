﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$256
struct $ArrayType$256_t1648;
struct $ArrayType$256_t1648_marshaled;

void $ArrayType$256_t1648_marshal(const $ArrayType$256_t1648& unmarshaled, $ArrayType$256_t1648_marshaled& marshaled);
void $ArrayType$256_t1648_marshal_back(const $ArrayType$256_t1648_marshaled& marshaled, $ArrayType$256_t1648& unmarshaled);
void $ArrayType$256_t1648_marshal_cleanup($ArrayType$256_t1648_marshaled& marshaled);
