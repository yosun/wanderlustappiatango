﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1<UnityEngine.UI.Graphic>
struct Comparer_1_t3376;
// System.Object
struct Object_t;
// UnityEngine.UI.Graphic
struct Graphic_t293;

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UI.Graphic>::.ctor()
// System.Collections.Generic.Comparer`1<System.Object>
#include "mscorlib_System_Collections_Generic_Comparer_1_genMethodDeclarations.h"
#define Comparer_1__ctor_m18468(__this, method) (void)Comparer_1__ctor_m14754_gshared((Comparer_1_t2861 *)__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UI.Graphic>::.cctor()
#define Comparer_1__cctor_m18469(__this/* static, unused */, method) (void)Comparer_1__cctor_m14755_gshared((Object_t *)__this/* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UI.Graphic>::System.Collections.IComparer.Compare(System.Object,System.Object)
#define Comparer_1_System_Collections_IComparer_Compare_m18470(__this, ___x, ___y, method) (int32_t)Comparer_1_System_Collections_IComparer_Compare_m14756_gshared((Comparer_1_t2861 *)__this, (Object_t *)___x, (Object_t *)___y, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UI.Graphic>::Compare(T,T)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UI.Graphic>::get_Default()
#define Comparer_1_get_Default_m18471(__this/* static, unused */, method) (Comparer_1_t3376 *)Comparer_1_get_Default_m14757_gshared((Object_t *)__this/* static, unused */, method)
