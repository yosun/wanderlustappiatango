﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>
struct Enumerator_t902;
// System.Object
struct Object_t;
// UnityEngine.MeshRenderer
struct MeshRenderer_t168;
// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
struct HashSet_1_t769;

// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::.ctor(System.Collections.Generic.HashSet`1<T>)
// System.Collections.Generic.HashSet`1/Enumerator<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator__0MethodDeclarations.h"
#define Enumerator__ctor_m26943(__this, ___hashset, method) (void)Enumerator__ctor_m26928_gshared((Enumerator_t4392 *)__this, (HashSet_1_t4389 *)___hashset, method)
// System.Object System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m26944(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m26929_gshared((Enumerator_t4392 *)__this, method)
// System.Boolean System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::MoveNext()
#define Enumerator_MoveNext_m5396(__this, method) (bool)Enumerator_MoveNext_m26930_gshared((Enumerator_t4392 *)__this, method)
// T System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::get_Current()
#define Enumerator_get_Current_m5395(__this, method) (MeshRenderer_t168 *)Enumerator_get_Current_m26931_gshared((Enumerator_t4392 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::Dispose()
#define Enumerator_Dispose_m26945(__this, method) (void)Enumerator_Dispose_m26932_gshared((Enumerator_t4392 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>::CheckState()
#define Enumerator_CheckState_m26946(__this, method) (void)Enumerator_CheckState_m26933_gshared((Enumerator_t4392 *)__this, method)
