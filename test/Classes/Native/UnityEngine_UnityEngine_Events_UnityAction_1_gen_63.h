﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// UnityEngine.UI.Graphic
struct Graphic_t293;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.UI.Graphic>
struct UnityAction_1_t3326  : public MulticastDelegate_t325
{
};
