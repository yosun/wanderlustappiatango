﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneAbstractBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_88.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneAbstractBehaviour>
struct CachedInvokableCall_1_t3651  : public InvokableCall_1_t3652
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.BackgroundPlaneAbstractBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
