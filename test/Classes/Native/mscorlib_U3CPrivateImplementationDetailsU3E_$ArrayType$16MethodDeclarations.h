﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$16
struct $ArrayType$16_t2263;
struct $ArrayType$16_t2263_marshaled;

void $ArrayType$16_t2263_marshal(const $ArrayType$16_t2263& unmarshaled, $ArrayType$16_t2263_marshaled& marshaled);
void $ArrayType$16_t2263_marshal_back(const $ArrayType$16_t2263_marshaled& marshaled, $ArrayType$16_t2263& unmarshaled);
void $ArrayType$16_t2263_marshal_cleanup($ArrayType$16_t2263_marshaled& marshaled);
