﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// UnityEngine.ReflectionProbe
struct ReflectionProbe_t944;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.ReflectionProbe>
struct UnityAction_1_t4493  : public MulticastDelegate_t325
{
};
