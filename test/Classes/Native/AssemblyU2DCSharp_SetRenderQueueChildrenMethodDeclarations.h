﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SetRenderQueueChildren
struct SetRenderQueueChildren_t23;

// System.Void SetRenderQueueChildren::.ctor()
 void SetRenderQueueChildren__ctor_m57 (SetRenderQueueChildren_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetRenderQueueChildren::Start()
 void SetRenderQueueChildren_Start_m58 (SetRenderQueueChildren_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetRenderQueueChildren::Awake()
 void SetRenderQueueChildren_Awake_m59 (SetRenderQueueChildren_t23 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
