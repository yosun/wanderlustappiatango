﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t5039;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
 void Getter_2__ctor_m30777_gshared (Getter_2_t5039 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method);
#define Getter_2__ctor_m30777(__this, ___object, ___method, method) (void)Getter_2__ctor_m30777_gshared((Getter_2_t5039 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
 Object_t * Getter_2_Invoke_m30778_gshared (Getter_2_t5039 * __this, Object_t * ____this, MethodInfo* method);
#define Getter_2_Invoke_m30778(__this, ____this, method) (Object_t *)Getter_2_Invoke_m30778_gshared((Getter_2_t5039 *)__this, (Object_t *)____this, method)
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
 Object_t * Getter_2_BeginInvoke_m30779_gshared (Getter_2_t5039 * __this, Object_t * ____this, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method);
#define Getter_2_BeginInvoke_m30779(__this, ____this, ___callback, ___object, method) (Object_t *)Getter_2_BeginInvoke_m30779_gshared((Getter_2_t5039 *)__this, (Object_t *)____this, (AsyncCallback_t200 *)___callback, (Object_t *)___object, method)
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
 Object_t * Getter_2_EndInvoke_m30780_gshared (Getter_2_t5039 * __this, Object_t * ___result, MethodInfo* method);
#define Getter_2_EndInvoke_m30780(__this, ___result, method) (Object_t *)Getter_2_EndInvoke_m30780_gshared((Getter_2_t5039 *)__this, (Object_t *)___result, method)
