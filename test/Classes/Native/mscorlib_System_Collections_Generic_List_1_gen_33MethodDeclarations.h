﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t697;
// System.Object
struct Object_t;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t60;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour>
struct IEnumerable_1_t692;
// System.Collections.Generic.IEnumerator`1<Vuforia.WordAbstractBehaviour>
struct IEnumerator_1_t840;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.ICollection`1<Vuforia.WordAbstractBehaviour>
struct ICollection_1_t4008;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.WordAbstractBehaviour>
struct ReadOnlyCollection_1_t4009;
// Vuforia.WordAbstractBehaviour[]
struct WordAbstractBehaviourU5BU5D_t827;
// System.Predicate`1<Vuforia.WordAbstractBehaviour>
struct Predicate_1_t4010;
// System.Comparison`1<Vuforia.WordAbstractBehaviour>
struct Comparison_1_t4011;
// System.Collections.Generic.List`1/Enumerator<Vuforia.WordAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_8.h"

// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_0MethodDeclarations.h"
#define List_1__ctor_m4968(__this, method) (void)List_1__ctor_m14543_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m23478(__this, ___collection, method) (void)List_1__ctor_m14545_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::.ctor(System.Int32)
#define List_1__ctor_m23479(__this, ___capacity, method) (void)List_1__ctor_m14547_gshared((List_1_t150 *)__this, (int32_t)___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::.cctor()
#define List_1__cctor_m23480(__this/* static, unused */, method) (void)List_1__cctor_m14549_gshared((Object_t *)__this/* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23481(__this, method) (Object_t*)List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m14551_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m23482(__this, ___array, ___arrayIndex, method) (void)List_1_System_Collections_ICollection_CopyTo_m14553_gshared((List_1_t150 *)__this, (Array_t *)___array, (int32_t)___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m23483(__this, method) (Object_t *)List_1_System_Collections_IEnumerable_GetEnumerator_m14555_gshared((List_1_t150 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m23484(__this, ___item, method) (int32_t)List_1_System_Collections_IList_Add_m14557_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m23485(__this, ___item, method) (bool)List_1_System_Collections_IList_Contains_m14559_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m23486(__this, ___item, method) (int32_t)List_1_System_Collections_IList_IndexOf_m14561_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m23487(__this, ___index, ___item, method) (void)List_1_System_Collections_IList_Insert_m14563_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m23488(__this, ___item, method) (void)List_1_System_Collections_IList_Remove_m14565_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23489(__this, method) (bool)List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14567_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m23490(__this, method) (bool)List_1_System_Collections_ICollection_get_IsSynchronized_m14569_gshared((List_1_t150 *)__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m23491(__this, method) (Object_t *)List_1_System_Collections_ICollection_get_SyncRoot_m14571_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m23492(__this, method) (bool)List_1_System_Collections_IList_get_IsFixedSize_m14573_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m23493(__this, method) (bool)List_1_System_Collections_IList_get_IsReadOnly_m14575_gshared((List_1_t150 *)__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m23494(__this, ___index, method) (Object_t *)List_1_System_Collections_IList_get_Item_m14577_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m23495(__this, ___index, ___value, method) (void)List_1_System_Collections_IList_set_Item_m14579_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::Add(T)
#define List_1_Add_m4981(__this, ___item, method) (void)List_1_Add_m14581_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m23496(__this, ___newCount, method) (void)List_1_GrowIfNeeded_m14583_gshared((List_1_t150 *)__this, (int32_t)___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m23497(__this, ___collection, method) (void)List_1_AddCollection_m14585_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m23498(__this, ___enumerable, method) (void)List_1_AddEnumerable_m14587_gshared((List_1_t150 *)__this, (Object_t*)___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m4972(__this, ___collection, method) (void)List_1_AddRange_m14588_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::AsReadOnly()
#define List_1_AsReadOnly_m23499(__this, method) (ReadOnlyCollection_1_t4009 *)List_1_AsReadOnly_m14590_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::Clear()
#define List_1_Clear_m4990(__this, method) (void)List_1_Clear_m14592_gshared((List_1_t150 *)__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::Contains(T)
#define List_1_Contains_m4977(__this, ___item, method) (bool)List_1_Contains_m14594_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m23500(__this, ___array, ___arrayIndex, method) (void)List_1_CopyTo_m14596_gshared((List_1_t150 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::Find(System.Predicate`1<T>)
#define List_1_Find_m23501(__this, ___match, method) (WordAbstractBehaviour_t60 *)List_1_Find_m14598_gshared((List_1_t150 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m23502(__this/* static, unused */, ___match, method) (void)List_1_CheckMatch_m14600_gshared((Object_t *)__this/* static, unused */, (Predicate_1_t2845 *)___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m23503(__this, ___startIndex, ___count, ___match, method) (int32_t)List_1_GetIndex_m14602_gshared((List_1_t150 *)__this, (int32_t)___startIndex, (int32_t)___count, (Predicate_1_t2845 *)___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::GetEnumerator()
 Enumerator_t826  List_1_GetEnumerator_m4984 (List_1_t697 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::IndexOf(T)
#define List_1_IndexOf_m23504(__this, ___item, method) (int32_t)List_1_IndexOf_m14604_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m23505(__this, ___start, ___delta, method) (void)List_1_Shift_m14606_gshared((List_1_t150 *)__this, (int32_t)___start, (int32_t)___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m23506(__this, ___index, method) (void)List_1_CheckIndex_m14608_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::Insert(System.Int32,T)
#define List_1_Insert_m23507(__this, ___index, ___item, method) (void)List_1_Insert_m14610_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m23508(__this, ___collection, method) (void)List_1_CheckCollection_m14612_gshared((List_1_t150 *)__this, (Object_t*)___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::Remove(T)
#define List_1_Remove_m4978(__this, ___item, method) (bool)List_1_Remove_m14614_gshared((List_1_t150 *)__this, (Object_t *)___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m23509(__this, ___match, method) (int32_t)List_1_RemoveAll_m14616_gshared((List_1_t150 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m4992(__this, ___index, method) (void)List_1_RemoveAt_m14618_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::Reverse()
#define List_1_Reverse_m23510(__this, method) (void)List_1_Reverse_m14620_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::Sort()
#define List_1_Sort_m23511(__this, method) (void)List_1_Sort_m14622_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m23512(__this, ___comparison, method) (void)List_1_Sort_m14624_gshared((List_1_t150 *)__this, (Comparison_1_t2846 *)___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::ToArray()
#define List_1_ToArray_m23513(__this, method) (WordAbstractBehaviourU5BU5D_t827*)List_1_ToArray_m14626_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::TrimExcess()
#define List_1_TrimExcess_m23514(__this, method) (void)List_1_TrimExcess_m14628_gshared((List_1_t150 *)__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::get_Capacity()
#define List_1_get_Capacity_m23515(__this, method) (int32_t)List_1_get_Capacity_m14630_gshared((List_1_t150 *)__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m23516(__this, ___value, method) (void)List_1_set_Capacity_m14632_gshared((List_1_t150 *)__this, (int32_t)___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::get_Count()
#define List_1_get_Count_m4979(__this, method) (int32_t)List_1_get_Count_m14634_gshared((List_1_t150 *)__this, method)
// T System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::get_Item(System.Int32)
#define List_1_get_Item_m4991(__this, ___index, method) (WordAbstractBehaviour_t60 *)List_1_get_Item_m14636_gshared((List_1_t150 *)__this, (int32_t)___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>::set_Item(System.Int32,T)
#define List_1_set_Item_m23517(__this, ___index, ___value, method) (void)List_1_set_Item_m14638_gshared((List_1_t150 *)__this, (int32_t)___index, (Object_t *)___value, method)
