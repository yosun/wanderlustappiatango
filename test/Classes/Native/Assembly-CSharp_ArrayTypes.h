﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// ARVRModes[]
// ARVRModes[]
struct ARVRModesU5BU5D_t5157  : public Array_t
{
};
// ARVRModes/TheCurrentMode[]
// ARVRModes/TheCurrentMode[]
struct TheCurrentModeU5BU5D_t5158  : public Array_t
{
};
// AnimateTexture[]
// AnimateTexture[]
struct AnimateTextureU5BU5D_t5159  : public Array_t
{
};
// GyroCameraYo[]
// GyroCameraYo[]
struct GyroCameraYoU5BU5D_t5160  : public Array_t
{
};
// Input2[]
// Input2[]
struct Input2U5BU5D_t5161  : public Array_t
{
};
struct Input2U5BU5D_t5161_StaticFields{
};
// Raycaster[]
// Raycaster[]
struct RaycasterU5BU5D_t5162  : public Array_t
{
};
// SetRenderQueue[]
// SetRenderQueue[]
struct SetRenderQueueU5BU5D_t5163  : public Array_t
{
};
// SetRenderQueueChildren[]
// SetRenderQueueChildren[]
struct SetRenderQueueChildrenU5BU5D_t5164  : public Array_t
{
};
// WorldLoop[]
// WorldLoop[]
struct WorldLoopU5BU5D_t5165  : public Array_t
{
};
// WorldNav[]
// WorldNav[]
struct WorldNavU5BU5D_t5166  : public Array_t
{
};
struct WorldNavU5BU5D_t5166_StaticFields{
};
// OrbitCamera[]
// OrbitCamera[]
struct OrbitCameraU5BU5D_t5167  : public Array_t
{
};
// TestParticles[]
// TestParticles[]
struct TestParticlesU5BU5D_t5168  : public Array_t
{
};
// Vuforia.BackgroundPlaneBehaviour[]
// Vuforia.BackgroundPlaneBehaviour[]
struct BackgroundPlaneBehaviourU5BU5D_t5169  : public Array_t
{
};
// Vuforia.CloudRecoBehaviour[]
// Vuforia.CloudRecoBehaviour[]
struct CloudRecoBehaviourU5BU5D_t5170  : public Array_t
{
};
// Vuforia.CylinderTargetBehaviour[]
// Vuforia.CylinderTargetBehaviour[]
struct CylinderTargetBehaviourU5BU5D_t5171  : public Array_t
{
};
// Vuforia.DataSetLoadBehaviour[]
// Vuforia.DataSetLoadBehaviour[]
struct DataSetLoadBehaviourU5BU5D_t5172  : public Array_t
{
};
// Vuforia.DefaultInitializationErrorHandler[]
// Vuforia.DefaultInitializationErrorHandler[]
struct DefaultInitializationErrorHandlerU5BU5D_t5173  : public Array_t
{
};
// Vuforia.DefaultSmartTerrainEventHandler[]
// Vuforia.DefaultSmartTerrainEventHandler[]
struct DefaultSmartTerrainEventHandlerU5BU5D_t5174  : public Array_t
{
};
// Vuforia.DefaultTrackableEventHandler[]
// Vuforia.DefaultTrackableEventHandler[]
struct DefaultTrackableEventHandlerU5BU5D_t5175  : public Array_t
{
};
// Vuforia.GLErrorHandler[]
// Vuforia.GLErrorHandler[]
struct GLErrorHandlerU5BU5D_t5176  : public Array_t
{
};
struct GLErrorHandlerU5BU5D_t5176_StaticFields{
};
// Vuforia.HideExcessAreaBehaviour[]
// Vuforia.HideExcessAreaBehaviour[]
struct HideExcessAreaBehaviourU5BU5D_t5177  : public Array_t
{
};
// Vuforia.ImageTargetBehaviour[]
// Vuforia.ImageTargetBehaviour[]
struct ImageTargetBehaviourU5BU5D_t5178  : public Array_t
{
};
// Vuforia.ComponentFactoryStarterBehaviour[]
// Vuforia.ComponentFactoryStarterBehaviour[]
struct ComponentFactoryStarterBehaviourU5BU5D_t5179  : public Array_t
{
};
// Vuforia.KeepAliveBehaviour[]
// Vuforia.KeepAliveBehaviour[]
struct KeepAliveBehaviourU5BU5D_t5180  : public Array_t
{
};
// Vuforia.MarkerBehaviour[]
// Vuforia.MarkerBehaviour[]
struct MarkerBehaviourU5BU5D_t5181  : public Array_t
{
};
// Vuforia.MaskOutBehaviour[]
// Vuforia.MaskOutBehaviour[]
struct MaskOutBehaviourU5BU5D_t5182  : public Array_t
{
};
// Vuforia.MultiTargetBehaviour[]
// Vuforia.MultiTargetBehaviour[]
struct MultiTargetBehaviourU5BU5D_t5183  : public Array_t
{
};
// Vuforia.ObjectTargetBehaviour[]
// Vuforia.ObjectTargetBehaviour[]
struct ObjectTargetBehaviourU5BU5D_t5184  : public Array_t
{
};
// Vuforia.PropBehaviour[]
// Vuforia.PropBehaviour[]
struct PropBehaviourU5BU5D_t5185  : public Array_t
{
};
// Vuforia.QCARBehaviour[]
// Vuforia.QCARBehaviour[]
struct QCARBehaviourU5BU5D_t5186  : public Array_t
{
};
// Vuforia.ReconstructionBehaviour[]
// Vuforia.ReconstructionBehaviour[]
struct ReconstructionBehaviourU5BU5D_t5187  : public Array_t
{
};
// Vuforia.ReconstructionFromTargetBehaviour[]
// Vuforia.ReconstructionFromTargetBehaviour[]
struct ReconstructionFromTargetBehaviourU5BU5D_t5188  : public Array_t
{
};
// Vuforia.SmartTerrainTrackerBehaviour[]
// Vuforia.SmartTerrainTrackerBehaviour[]
struct SmartTerrainTrackerBehaviourU5BU5D_t5189  : public Array_t
{
};
// Vuforia.SurfaceBehaviour[]
// Vuforia.SurfaceBehaviour[]
struct SurfaceBehaviourU5BU5D_t5190  : public Array_t
{
};
// Vuforia.TextRecoBehaviour[]
// Vuforia.TextRecoBehaviour[]
struct TextRecoBehaviourU5BU5D_t5191  : public Array_t
{
};
// Vuforia.TurnOffBehaviour[]
// Vuforia.TurnOffBehaviour[]
struct TurnOffBehaviourU5BU5D_t5192  : public Array_t
{
};
// Vuforia.TurnOffWordBehaviour[]
// Vuforia.TurnOffWordBehaviour[]
struct TurnOffWordBehaviourU5BU5D_t5193  : public Array_t
{
};
// Vuforia.UserDefinedTargetBuildingBehaviour[]
// Vuforia.UserDefinedTargetBuildingBehaviour[]
struct UserDefinedTargetBuildingBehaviourU5BU5D_t5194  : public Array_t
{
};
// Vuforia.VideoBackgroundBehaviour[]
// Vuforia.VideoBackgroundBehaviour[]
struct VideoBackgroundBehaviourU5BU5D_t5195  : public Array_t
{
};
// Vuforia.VideoTextureRenderer[]
// Vuforia.VideoTextureRenderer[]
struct VideoTextureRendererU5BU5D_t5196  : public Array_t
{
};
// Vuforia.VirtualButtonBehaviour[]
// Vuforia.VirtualButtonBehaviour[]
struct VirtualButtonBehaviourU5BU5D_t5197  : public Array_t
{
};
// Vuforia.WebCamBehaviour[]
// Vuforia.WebCamBehaviour[]
struct WebCamBehaviourU5BU5D_t5198  : public Array_t
{
};
// Vuforia.WireframeBehaviour[]
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t177  : public Array_t
{
};
// Vuforia.WireframeTrackableEventHandler[]
// Vuforia.WireframeTrackableEventHandler[]
struct WireframeTrackableEventHandlerU5BU5D_t5199  : public Array_t
{
};
// Vuforia.WordBehaviour[]
// Vuforia.WordBehaviour[]
struct WordBehaviourU5BU5D_t5200  : public Array_t
{
};
