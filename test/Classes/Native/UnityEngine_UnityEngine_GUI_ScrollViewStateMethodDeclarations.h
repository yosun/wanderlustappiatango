﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.GUI/ScrollViewState
struct ScrollViewState_t950;

// System.Void UnityEngine.GUI/ScrollViewState::.ctor()
 void ScrollViewState__ctor_m5576 (ScrollViewState_t950 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
