﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<Vuforia.Marker>
struct EqualityComparer_1_t3885;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<Vuforia.Marker>
struct EqualityComparer_1_t3885  : public Object_t
{
};
struct EqualityComparer_1_t3885_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.Marker>::_default
	EqualityComparer_1_t3885 * ____default_0;
};
