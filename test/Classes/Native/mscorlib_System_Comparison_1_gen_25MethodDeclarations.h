﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparison_1_t3792;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"

// System.Void System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Object,System.IntPtr)
 void Comparison_1__ctor_m21340 (Comparison_1_t3792 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>::Invoke(T,T)
 int32_t Comparison_1_Invoke_m21341 (Comparison_1_t3792 * __this, int32_t ___x, int32_t ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
 Object_t * Comparison_1_BeginInvoke_m21342 (Comparison_1_t3792 * __this, int32_t ___x, int32_t ___y, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>::EndInvoke(System.IAsyncResult)
 int32_t Comparison_1_EndInvoke_m21343 (Comparison_1_t3792 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
