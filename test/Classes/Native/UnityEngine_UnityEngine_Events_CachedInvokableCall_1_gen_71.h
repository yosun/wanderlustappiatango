﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.UI.ToggleGroup>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_73.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.ToggleGroup>
struct CachedInvokableCall_1_t3531  : public InvokableCall_1_t3532
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.UI.ToggleGroup>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
