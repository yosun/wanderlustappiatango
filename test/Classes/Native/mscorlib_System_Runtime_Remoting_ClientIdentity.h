﻿#pragma once
#include <stdint.h>
// System.WeakReference
struct WeakReference_t2041;
// System.Runtime.Remoting.Identity
#include "mscorlib_System_Runtime_Remoting_Identity.h"
// System.Runtime.Remoting.ClientIdentity
struct ClientIdentity_t2034  : public Identity_t2033
{
	// System.WeakReference System.Runtime.Remoting.ClientIdentity::_proxyReference
	WeakReference_t2041 * ____proxyReference_5;
};
