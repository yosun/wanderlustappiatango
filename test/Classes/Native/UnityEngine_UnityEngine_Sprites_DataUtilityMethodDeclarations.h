﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Sprites.DataUtility
struct DataUtility_t986;
// UnityEngine.Sprite
struct Sprite_t310;
// UnityEngine.Vector4
#include "UnityEngine_UnityEngine_Vector4.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
 Vector4_t314  DataUtility_GetInnerUV_m2210 (Object_t * __this/* static, unused */, Sprite_t310 * ___sprite, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
 Vector4_t314  DataUtility_GetOuterUV_m2209 (Object_t * __this/* static, unused */, Sprite_t310 * ___sprite, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
 Vector4_t314  DataUtility_GetPadding_m2199 (Object_t * __this/* static, unused */, Sprite_t310 * ___sprite, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
 Vector2_t9  DataUtility_GetMinSize_m2195 (Object_t * __this/* static, unused */, Sprite_t310 * ___sprite, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Sprites.DataUtility::Internal_GetMinSize(UnityEngine.Sprite,UnityEngine.Vector2&)
 void DataUtility_Internal_GetMinSize_m5944 (Object_t * __this/* static, unused */, Sprite_t310 * ___sprite, Vector2_t9 * ___output, MethodInfo* method) IL2CPP_METHOD_ATTR;
