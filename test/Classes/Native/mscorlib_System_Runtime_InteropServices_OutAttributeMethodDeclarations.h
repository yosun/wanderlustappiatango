﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.OutAttribute
struct OutAttribute_t1708;

// System.Void System.Runtime.InteropServices.OutAttribute::.ctor()
 void OutAttribute__ctor_m9733 (OutAttribute_t1708 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
