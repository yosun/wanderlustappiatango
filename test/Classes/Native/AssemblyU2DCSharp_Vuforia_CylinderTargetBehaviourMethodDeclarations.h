﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_t33;

// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
 void CylinderTargetBehaviour__ctor_m87 (CylinderTargetBehaviour_t33 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
