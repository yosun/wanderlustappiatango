﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<WorldNav>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_8.h"
// UnityEngine.Events.CachedInvokableCall`1<WorldNav>
struct CachedInvokableCall_1_t2761  : public InvokableCall_1_t2762
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<WorldNav>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
