﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished
struct TlsClientFinished_t1638;
// Mono.Security.Protocol.Tls.Context
struct Context_t1589;

// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished::.ctor(Mono.Security.Protocol.Tls.Context)
 void TlsClientFinished__ctor_m8715 (TlsClientFinished_t1638 * __this, Context_t1589 * ___context, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished::.cctor()
 void TlsClientFinished__cctor_m8716 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished::Update()
 void TlsClientFinished_Update_m8717 (TlsClientFinished_t1638 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished::ProcessAsSsl3()
 void TlsClientFinished_ProcessAsSsl3_m8718 (TlsClientFinished_t1638 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished::ProcessAsTls1()
 void TlsClientFinished_ProcessAsTls1_m8719 (TlsClientFinished_t1638 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
