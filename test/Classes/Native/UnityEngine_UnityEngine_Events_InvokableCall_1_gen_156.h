﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<UnityEngine.StateMachineBehaviour>
struct UnityAction_1_t4758;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<UnityEngine.StateMachineBehaviour>
struct InvokableCall_1_t4757  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<UnityEngine.StateMachineBehaviour>::Delegate
	UnityAction_1_t4758 * ___Delegate_0;
};
