﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.ImageTarget,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>>
struct Transform_1_t4246;
// System.Object
struct Object_t;
// Vuforia.ImageTarget
struct ImageTarget_t572;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.ImageTarget,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m25770 (Transform_1_t4246 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.ImageTarget,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>>::Invoke(TKey,TValue)
 KeyValuePair_2_t4235  Transform_1_Invoke_m25771 (Transform_1_t4246 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.ImageTarget,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m25772 (Transform_1_t4246 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.ImageTarget,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>>::EndInvoke(System.IAsyncResult)
 KeyValuePair_2_t4235  Transform_1_EndInvoke_m25773 (Transform_1_t4246 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
