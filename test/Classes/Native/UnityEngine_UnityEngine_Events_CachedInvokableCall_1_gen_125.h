﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.GUILayer>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_127.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUILayer>
struct CachedInvokableCall_1_t4499  : public InvokableCall_1_t4500
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.GUILayer>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
