﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Diagnostics.DebuggableAttribute>
struct InternalEnumerator_1_t4981;
// System.Object
struct Object_t;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_t905;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.DebuggableAttribute>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m30487(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Diagnostics.DebuggableAttribute>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30488(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Diagnostics.DebuggableAttribute>::Dispose()
#define InternalEnumerator_1_Dispose_m30489(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Diagnostics.DebuggableAttribute>::MoveNext()
#define InternalEnumerator_1_MoveNext_m30490(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<System.Diagnostics.DebuggableAttribute>::get_Current()
#define InternalEnumerator_1_get_Current_m30491(__this, method) (DebuggableAttribute_t905 *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
