﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.SimpleCollator/Escape
struct Escape_t1743;
struct Escape_t1743_marshaled;

void Escape_t1743_marshal(const Escape_t1743& unmarshaled, Escape_t1743_marshaled& marshaled);
void Escape_t1743_marshal_back(const Escape_t1743_marshaled& marshaled, Escape_t1743& unmarshaled);
void Escape_t1743_marshal_cleanup(Escape_t1743_marshaled& marshaled);
