﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Proxies.RemotingProxy
struct RemotingProxy_t2035;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Runtime.Remoting.ClientIdentity
struct ClientIdentity_t2034;
// System.Object[]
struct ObjectU5BU5D_t115;

// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.ctor(System.Type,System.Runtime.Remoting.ClientIdentity)
 void RemotingProxy__ctor_m11630 (RemotingProxy_t2035 * __this, Type_t * ___type, ClientIdentity_t2034 * ___identity, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.ctor(System.Type,System.String,System.Object[])
 void RemotingProxy__ctor_m11631 (RemotingProxy_t2035 * __this, Type_t * ___type, String_t* ___activationUrl, ObjectU5BU5D_t115* ___activationAttributes, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::.cctor()
 void RemotingProxy__cctor_m11632 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Runtime.Remoting.Proxies.RemotingProxy::get_TypeName()
 String_t* RemotingProxy_get_TypeName_m11633 (RemotingProxy_t2035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.Proxies.RemotingProxy::Finalize()
 void RemotingProxy_Finalize_m11634 (RemotingProxy_t2035 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
