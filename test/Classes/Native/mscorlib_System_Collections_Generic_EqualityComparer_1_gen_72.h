﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.Boolean>
struct EqualityComparer_1_t4868;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.Boolean>
struct EqualityComparer_1_t4868  : public Object_t
{
};
struct EqualityComparer_1_t4868_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Boolean>::_default
	EqualityComparer_1_t4868 * ____default_0;
};
