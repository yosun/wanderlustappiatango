﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARNativeWrapper
struct QCARNativeWrapper_t707;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t418;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Int32 Vuforia.QCARNativeWrapper::CameraDeviceInitCamera(System.Int32)
 int32_t QCARNativeWrapper_CameraDeviceInitCamera_m3614 (QCARNativeWrapper_t707 * __this, int32_t ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::CameraDeviceDeinitCamera()
 int32_t QCARNativeWrapper_CameraDeviceDeinitCamera_m3615 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::CameraDeviceStartCamera()
 int32_t QCARNativeWrapper_CameraDeviceStartCamera_m3616 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::CameraDeviceStopCamera()
 int32_t QCARNativeWrapper_CameraDeviceStopCamera_m3617 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::CameraDeviceGetNumVideoModes()
 int32_t QCARNativeWrapper_CameraDeviceGetNumVideoModes_m3618 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::CameraDeviceGetVideoMode(System.Int32,System.IntPtr)
 void QCARNativeWrapper_CameraDeviceGetVideoMode_m3619 (QCARNativeWrapper_t707 * __this, int32_t ___idx, IntPtr_t121 ___videoMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::CameraDeviceSelectVideoMode(System.Int32)
 int32_t QCARNativeWrapper_CameraDeviceSelectVideoMode_m3620 (QCARNativeWrapper_t707 * __this, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::CameraDeviceSetFlashTorchMode(System.Int32)
 int32_t QCARNativeWrapper_CameraDeviceSetFlashTorchMode_m3621 (QCARNativeWrapper_t707 * __this, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::CameraDeviceSetFocusMode(System.Int32)
 int32_t QCARNativeWrapper_CameraDeviceSetFocusMode_m3622 (QCARNativeWrapper_t707 * __this, int32_t ___focusMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::CameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
 int32_t QCARNativeWrapper_CameraDeviceSetCameraConfiguration_m3623 (QCARNativeWrapper_t707 * __this, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::QcarSetFrameFormat(System.Int32,System.Int32)
 int32_t QCARNativeWrapper_QcarSetFrameFormat_m3624 (QCARNativeWrapper_t707 * __this, int32_t ___format, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::DataSetExists(System.String,System.Int32)
 int32_t QCARNativeWrapper_DataSetExists_m3625 (QCARNativeWrapper_t707 * __this, String_t* ___relativePath, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::DataSetLoad(System.String,System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_DataSetLoad_m3626 (QCARNativeWrapper_t707 * __this, String_t* ___relativePath, int32_t ___storageType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_DataSetGetNumTrackableType_m3627 (QCARNativeWrapper_t707 * __this, int32_t ___trackableType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_DataSetGetTrackablesOfType_m3628 (QCARNativeWrapper_t707 * __this, int32_t ___trackableType, IntPtr_t121 ___trackableDataArray, int32_t ___trackableDataArrayLength, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNativeWrapper_DataSetGetTrackableName_m3629 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, StringBuilder_t418 * ___trackableName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::DataSetCreateTrackable(System.IntPtr,System.IntPtr,System.Text.StringBuilder,System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_DataSetCreateTrackable_m3630 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, IntPtr_t121 ___trackableSourcePtr, StringBuilder_t418 * ___trackableName, int32_t ___nameMaxLength, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::DataSetDestroyTrackable(System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_DataSetDestroyTrackable_m3631 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::DataSetHasReachedTrackableLimit(System.IntPtr)
 int32_t QCARNativeWrapper_DataSetHasReachedTrackableLimit_m3632 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::GetCameraThreadID()
 int32_t QCARNativeWrapper_GetCameraThreadID_m3633 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ImageTargetBuilderBuild(System.String,System.Single)
 int32_t QCARNativeWrapper_ImageTargetBuilderBuild_m3634 (QCARNativeWrapper_t707 * __this, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::FrameCounterGetBenchmarkingData(System.IntPtr,System.Boolean)
 void QCARNativeWrapper_FrameCounterGetBenchmarkingData_m3635 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___benchmarkingData, bool ___isStereoRendering, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::ImageTargetBuilderStartScan()
 void QCARNativeWrapper_ImageTargetBuilderStartScan_m3636 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::ImageTargetBuilderStopScan()
 void QCARNativeWrapper_ImageTargetBuilderStopScan_m3637 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ImageTargetBuilderGetFrameQuality()
 int32_t QCARNativeWrapper_ImageTargetBuilderGetFrameQuality_m3638 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeWrapper::ImageTargetBuilderGetTrackableSource()
 IntPtr_t121 QCARNativeWrapper_ImageTargetBuilderGetTrackableSource_m3639 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ImageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNativeWrapper_ImageTargetCreateVirtualButton_m3640 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ImageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
 int32_t QCARNativeWrapper_ImageTargetDestroyVirtualButton_m3641 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::VirtualButtonGetId(System.IntPtr,System.String,System.String)
 int32_t QCARNativeWrapper_VirtualButtonGetId_m3642 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ImageTargetGetNumVirtualButtons(System.IntPtr,System.String)
 int32_t QCARNativeWrapper_ImageTargetGetNumVirtualButtons_m3643 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ImageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
 int32_t QCARNativeWrapper_ImageTargetGetVirtualButtons_m3644 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___virtualButtonDataArray, IntPtr_t121 ___rectangleDataArray, int32_t ___virtualButtonDataArrayLength, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ImageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNativeWrapper_ImageTargetGetVirtualButtonName_m3645 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, int32_t ___idx, StringBuilder_t418 * ___vbName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::CylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeWrapper_CylinderTargetGetDimensions_m3646 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___dimensions, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::CylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeWrapper_CylinderTargetSetSideLength_m3647 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___sideLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::CylinderTargetSetTopDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeWrapper_CylinderTargetSetTopDiameter_m3648 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___topDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::CylinderTargetSetBottomDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeWrapper_CylinderTargetSetBottomDiameter_m3649 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___bottomDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ObjectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeWrapper_ObjectTargetSetSize_m3650 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ObjectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeWrapper_ObjectTargetGetSize_m3651 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ObjectTrackerStart()
 int32_t QCARNativeWrapper_ObjectTrackerStart_m3652 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::ObjectTrackerStop()
 void QCARNativeWrapper_ObjectTrackerStop_m3653 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeWrapper::ObjectTrackerCreateDataSet()
 IntPtr_t121 QCARNativeWrapper_ObjectTrackerCreateDataSet_m3654 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ObjectTrackerDestroyDataSet(System.IntPtr)
 int32_t QCARNativeWrapper_ObjectTrackerDestroyDataSet_m3655 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ObjectTrackerActivateDataSet(System.IntPtr)
 int32_t QCARNativeWrapper_ObjectTrackerActivateDataSet_m3656 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ObjectTrackerDeactivateDataSet(System.IntPtr)
 int32_t QCARNativeWrapper_ObjectTrackerDeactivateDataSet_m3657 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ObjectTrackerPersistExtendedTracking(System.Int32)
 int32_t QCARNativeWrapper_ObjectTrackerPersistExtendedTracking_m3658 (QCARNativeWrapper_t707 * __this, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ObjectTrackerResetExtendedTracking()
 int32_t QCARNativeWrapper_ObjectTrackerResetExtendedTracking_m3659 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::MarkerSetSize(System.Int32,System.Single)
 int32_t QCARNativeWrapper_MarkerSetSize_m3660 (QCARNativeWrapper_t707 * __this, int32_t ___trackableIndex, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::MarkerTrackerStart()
 int32_t QCARNativeWrapper_MarkerTrackerStart_m3661 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::MarkerTrackerStop()
 void QCARNativeWrapper_MarkerTrackerStop_m3662 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::MarkerTrackerCreateMarker(System.Int32,System.String,System.Single)
 int32_t QCARNativeWrapper_MarkerTrackerCreateMarker_m3663 (QCARNativeWrapper_t707 * __this, int32_t ___id, String_t* ___trackableName, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::MarkerTrackerDestroyMarker(System.Int32)
 int32_t QCARNativeWrapper_MarkerTrackerDestroyMarker_m3664 (QCARNativeWrapper_t707 * __this, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::InitPlatformNative()
 void QCARNativeWrapper_InitPlatformNative_m3665 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::InitFrameState(System.IntPtr)
 void QCARNativeWrapper_InitFrameState_m3666 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::DeinitFrameState(System.IntPtr)
 void QCARNativeWrapper_DeinitFrameState_m3667 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::OnSurfaceChanged(System.Int32,System.Int32)
 void QCARNativeWrapper_OnSurfaceChanged_m3668 (QCARNativeWrapper_t707 * __this, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::OnPause()
 void QCARNativeWrapper_OnPause_m3669 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::OnResume()
 void QCARNativeWrapper_OnResume_m3670 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::HasSurfaceBeenRecreated()
 bool QCARNativeWrapper_HasSurfaceBeenRecreated_m3671 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::UpdateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_UpdateQCAR_m3672 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___imageHeaderDataArray, int32_t ___imageHeaderArrayLength, IntPtr_t121 ___frameIndex, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::RendererEnd()
 void QCARNativeWrapper_RendererEnd_m3673 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeWrapper_QcarGetBufferSize_m3674 (QCARNativeWrapper_t707 * __this, int32_t ___width, int32_t ___height, int32_t ___format, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::QcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 void QCARNativeWrapper_QcarAddCameraFrame_m3675 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___pixels, int32_t ___width, int32_t ___height, int32_t ___format, int32_t ___stride, int32_t ___frameIdx, int32_t ___flipHorizontally, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::RendererSetVideoBackgroundCfg(System.IntPtr)
 void QCARNativeWrapper_RendererSetVideoBackgroundCfg_m3676 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::RendererGetVideoBackgroundCfg(System.IntPtr)
 void QCARNativeWrapper_RendererGetVideoBackgroundCfg_m3677 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::RendererGetVideoBackgroundTextureInfo(System.IntPtr)
 void QCARNativeWrapper_RendererGetVideoBackgroundTextureInfo_m3678 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___texInfo, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::RendererSetVideoBackgroundTextureID(System.Int32)
 int32_t QCARNativeWrapper_RendererSetVideoBackgroundTextureID_m3679 (QCARNativeWrapper_t707 * __this, int32_t ___textureID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::RendererIsVideoBackgroundTextureInfoAvailable()
 int32_t QCARNativeWrapper_RendererIsVideoBackgroundTextureInfoAvailable_m3680 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::QcarSetHint(System.Int32,System.Int32)
 int32_t QCARNativeWrapper_QcarSetHint_m3681 (QCARNativeWrapper_t707 * __this, int32_t ___hint, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::GetProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_GetProjectionGL_m3682 (QCARNativeWrapper_t707 * __this, float ___nearClip, float ___farClip, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::SetApplicationEnvironment(System.Int32,System.Int32,System.Int32)
 void QCARNativeWrapper_SetApplicationEnvironment_m3683 (QCARNativeWrapper_t707 * __this, int32_t ___unityVersionMajor, int32_t ___unityVersionMinor, int32_t ___unityVersionChange, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::SetStateBufferSize(System.Int32)
 void QCARNativeWrapper_SetStateBufferSize_m3684 (QCARNativeWrapper_t707 * __this, int32_t ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::SmartTerrainTrackerStart()
 int32_t QCARNativeWrapper_SmartTerrainTrackerStart_m3685 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::SmartTerrainTrackerStop()
 void QCARNativeWrapper_SmartTerrainTrackerStop_m3686 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::SmartTerrainTrackerSetScaleToMillimeter(System.Single)
 bool QCARNativeWrapper_SmartTerrainTrackerSetScaleToMillimeter_m3687 (QCARNativeWrapper_t707 * __this, float ___scaleFactor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::SmartTerrainTrackerInitBuilder()
 bool QCARNativeWrapper_SmartTerrainTrackerInitBuilder_m3688 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::SmartTerrainTrackerDeinitBuilder()
 bool QCARNativeWrapper_SmartTerrainTrackerDeinitBuilder_m3689 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeWrapper::SmartTerrainBuilderCreateReconstructionFromTarget()
 IntPtr_t121 QCARNativeWrapper_SmartTerrainBuilderCreateReconstructionFromTarget_m3690 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeWrapper::SmartTerrainBuilderCreateReconstructionFromEnvironment()
 IntPtr_t121 QCARNativeWrapper_SmartTerrainBuilderCreateReconstructionFromEnvironment_m3691 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::SmartTerrainBuilderAddReconstruction(System.IntPtr)
 bool QCARNativeWrapper_SmartTerrainBuilderAddReconstruction_m3692 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::SmartTerrainBuilderRemoveReconstruction(System.IntPtr)
 bool QCARNativeWrapper_SmartTerrainBuilderRemoveReconstruction_m3693 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::SmartTerrainBuilderDestroyReconstruction(System.IntPtr)
 bool QCARNativeWrapper_SmartTerrainBuilderDestroyReconstruction_m3694 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::ReconstructionStart(System.IntPtr)
 bool QCARNativeWrapper_ReconstructionStart_m3695 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::ReconstructionStop(System.IntPtr)
 bool QCARNativeWrapper_ReconstructionStop_m3696 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::ReconstructionIsReconstructing(System.IntPtr)
 bool QCARNativeWrapper_ReconstructionIsReconstructing_m3697 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::ReconstructionReset(System.IntPtr)
 bool QCARNativeWrapper_ReconstructionReset_m3698 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::ReconstructionSetNavMeshPadding(System.IntPtr,System.Single)
 void QCARNativeWrapper_ReconstructionSetNavMeshPadding_m3699 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___reconstruction, float ___padding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::ReconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
 bool QCARNativeWrapper_ReconstructionFromTargetSetInitializationTarget_m3700 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___reconstruction, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, IntPtr_t121 ___occluderMin, IntPtr_t121 ___occluderMax, IntPtr_t121 ___offsetToOccluder, IntPtr_t121 ___rotationAxisToOccluder, float ___rotationAngleToOccluder, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::ReconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
 bool QCARNativeWrapper_ReconstructionSetMaximumArea_m3701 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___reconstruction, IntPtr_t121 ___maximumArea, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::ReconstructioFromEnvironmentGetReconstructionState(System.IntPtr)
 int32_t QCARNativeWrapper_ReconstructioFromEnvironmentGetReconstructionState_m3702 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::TargetFinderStartInit(System.String,System.String)
 int32_t QCARNativeWrapper_TargetFinderStartInit_m3703 (QCARNativeWrapper_t707 * __this, String_t* ___userKey, String_t* ___secretKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::TargetFinderGetInitState()
 int32_t QCARNativeWrapper_TargetFinderGetInitState_m3704 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::TargetFinderDeinit()
 int32_t QCARNativeWrapper_TargetFinderDeinit_m3705 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::TargetFinderStartRecognition()
 int32_t QCARNativeWrapper_TargetFinderStartRecognition_m3706 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::TargetFinderStop()
 int32_t QCARNativeWrapper_TargetFinderStop_m3707 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::TargetFinderSetUIScanlineColor(System.Single,System.Single,System.Single)
 void QCARNativeWrapper_TargetFinderSetUIScanlineColor_m3708 (QCARNativeWrapper_t707 * __this, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::TargetFinderSetUIPointColor(System.Single,System.Single,System.Single)
 void QCARNativeWrapper_TargetFinderSetUIPointColor_m3709 (QCARNativeWrapper_t707 * __this, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::TargetFinderUpdate(System.IntPtr)
 void QCARNativeWrapper_TargetFinderUpdate_m3710 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___targetFinderState, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::TargetFinderGetResults(System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_TargetFinderGetResults_m3711 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___searchResultArray, int32_t ___searchResultArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::TargetFinderEnableTracking(System.IntPtr,System.IntPtr)
 int32_t QCARNativeWrapper_TargetFinderEnableTracking_m3712 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___searchResult, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::TargetFinderGetImageTargets(System.IntPtr,System.Int32)
 void QCARNativeWrapper_TargetFinderGetImageTargets_m3713 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___trackableIdArray, int32_t ___trackableIdArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::TargetFinderClearTrackables()
 void QCARNativeWrapper_TargetFinderClearTrackables_m3714 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::TextTrackerStart()
 int32_t QCARNativeWrapper_TextTrackerStart_m3715 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::TextTrackerStop()
 void QCARNativeWrapper_TextTrackerStop_m3716 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::TextTrackerSetRegionOfInterest(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeWrapper_TextTrackerSetRegionOfInterest_m3717 (QCARNativeWrapper_t707 * __this, int32_t ___detectionLeftTopX, int32_t ___detectionLeftTopY, int32_t ___detectionRightBottomX, int32_t ___detectionRightBottomY, int32_t ___trackingLeftTopX, int32_t ___trackingLeftTopY, int32_t ___trackingRightBottomX, int32_t ___trackingRightBottomY, int32_t ___upDirection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::TextTrackerGetRegionOfInterest(System.IntPtr,System.IntPtr)
 void QCARNativeWrapper_TextTrackerGetRegionOfInterest_m3718 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___detectionROI, IntPtr_t121 ___trackingROI, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordListLoadWordList(System.String,System.Int32)
 int32_t QCARNativeWrapper_WordListLoadWordList_m3719 (QCARNativeWrapper_t707 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordListAddWordsFromFile(System.String,System.Int32)
 int32_t QCARNativeWrapper_WordListAddWordsFromFile_m3720 (QCARNativeWrapper_t707 * __this, String_t* ___path, int32_t ___storagetType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordListAddWordU(System.IntPtr)
 int32_t QCARNativeWrapper_WordListAddWordU_m3721 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordListRemoveWordU(System.IntPtr)
 int32_t QCARNativeWrapper_WordListRemoveWordU_m3722 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordListContainsWordU(System.IntPtr)
 int32_t QCARNativeWrapper_WordListContainsWordU_m3723 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordListUnloadAllLists()
 int32_t QCARNativeWrapper_WordListUnloadAllLists_m3724 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordListSetFilterMode(System.Int32)
 int32_t QCARNativeWrapper_WordListSetFilterMode_m3725 (QCARNativeWrapper_t707 * __this, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordListGetFilterMode()
 int32_t QCARNativeWrapper_WordListGetFilterMode_m3726 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordListLoadFilterList(System.String,System.Int32)
 int32_t QCARNativeWrapper_WordListLoadFilterList_m3727 (QCARNativeWrapper_t707 * __this, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordListAddWordToFilterListU(System.IntPtr)
 int32_t QCARNativeWrapper_WordListAddWordToFilterListU_m3728 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordListRemoveWordFromFilterListU(System.IntPtr)
 int32_t QCARNativeWrapper_WordListRemoveWordFromFilterListU_m3729 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordListClearFilterList()
 int32_t QCARNativeWrapper_WordListClearFilterList_m3730 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordListGetFilterListWordCount()
 int32_t QCARNativeWrapper_WordListGetFilterListWordCount_m3731 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeWrapper::WordListGetFilterListWordU(System.Int32)
 IntPtr_t121 QCARNativeWrapper_WordListGetFilterListWordU_m3732 (QCARNativeWrapper_t707 * __this, int32_t ___i, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordGetLetterMask(System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_WordGetLetterMask_m3733 (QCARNativeWrapper_t707 * __this, int32_t ___wordID, IntPtr_t121 ___letterMaskImage, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::WordGetLetterBoundingBoxes(System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_WordGetLetterBoundingBoxes_m3734 (QCARNativeWrapper_t707 * __this, int32_t ___wordID, IntPtr_t121 ___letterBoundingBoxes, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::TrackerManagerInitTracker(System.Int32)
 int32_t QCARNativeWrapper_TrackerManagerInitTracker_m3735 (QCARNativeWrapper_t707 * __this, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::TrackerManagerDeinitTracker(System.Int32)
 int32_t QCARNativeWrapper_TrackerManagerDeinitTracker_m3736 (QCARNativeWrapper_t707 * __this, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::VirtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNativeWrapper_VirtualButtonSetEnabled_m3737 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::VirtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNativeWrapper_VirtualButtonSetSensitivity_m3738 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___sensitivity, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::VirtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNativeWrapper_VirtualButtonSetAreaRectangle_m3739 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::QcarInit(System.String)
 int32_t QCARNativeWrapper_QcarInit_m3740 (QCARNativeWrapper_t707 * __this, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::QcarDeinit()
 int32_t QCARNativeWrapper_QcarDeinit_m3741 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::StartExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_StartExtendedTracking_m3742 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::StopExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_StopExtendedTracking_m3743 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearIsSupportedDeviceDetected()
 bool QCARNativeWrapper_EyewearIsSupportedDeviceDetected_m3744 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearIsSeeThru()
 bool QCARNativeWrapper_EyewearIsSeeThru_m3745 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::EyewearGetScreenOrientation()
 int32_t QCARNativeWrapper_EyewearGetScreenOrientation_m3746 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearIsStereoCapable()
 bool QCARNativeWrapper_EyewearIsStereoCapable_m3747 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearIsStereoEnabled()
 bool QCARNativeWrapper_EyewearIsStereoEnabled_m3748 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearIsStereoGLOnly()
 bool QCARNativeWrapper_EyewearIsStereoGLOnly_m3749 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearSetStereo(System.Boolean)
 bool QCARNativeWrapper_EyewearSetStereo_m3750 (QCARNativeWrapper_t707 * __this, bool ___enable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::EyewearGetDefaultSceneScale(System.IntPtr)
 int32_t QCARNativeWrapper_EyewearGetDefaultSceneScale_m3751 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::EyewearGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_EyewearGetProjectionMatrix_m3752 (QCARNativeWrapper_t707 * __this, int32_t ___eyeID, int32_t ___profileID, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::EyewearCPMGetMaxCount()
 int32_t QCARNativeWrapper_EyewearCPMGetMaxCount_m3753 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::EyewearCPMGetUsedCount()
 int32_t QCARNativeWrapper_EyewearCPMGetUsedCount_m3754 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearCPMIsProfileUsed(System.Int32)
 bool QCARNativeWrapper_EyewearCPMIsProfileUsed_m3755 (QCARNativeWrapper_t707 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::EyewearCPMGetActiveProfile()
 int32_t QCARNativeWrapper_EyewearCPMGetActiveProfile_m3756 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearCPMSetActiveProfile(System.Int32)
 bool QCARNativeWrapper_EyewearCPMSetActiveProfile_m3757 (QCARNativeWrapper_t707 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::EyewearCPMGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_EyewearCPMGetProjectionMatrix_m3758 (QCARNativeWrapper_t707 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearCPMSetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 bool QCARNativeWrapper_EyewearCPMSetProjectionMatrix_m3759 (QCARNativeWrapper_t707 * __this, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeWrapper::EyewearCPMGetProfileName(System.Int32)
 IntPtr_t121 QCARNativeWrapper_EyewearCPMGetProfileName_m3760 (QCARNativeWrapper_t707 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearCPMSetProfileName(System.Int32,System.IntPtr)
 bool QCARNativeWrapper_EyewearCPMSetProfileName_m3761 (QCARNativeWrapper_t707 * __this, int32_t ___profileID, IntPtr_t121 ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearCPMClearProfile(System.Int32)
 bool QCARNativeWrapper_EyewearCPMClearProfile_m3762 (QCARNativeWrapper_t707 * __this, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearUserCalibratorInit(System.Int32,System.Int32,System.Int32,System.Int32)
 bool QCARNativeWrapper_EyewearUserCalibratorInit_m3763 (QCARNativeWrapper_t707 * __this, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeWrapper::EyewearUserCalibratorGetMinScaleHint()
 float QCARNativeWrapper_EyewearUserCalibratorGetMinScaleHint_m3764 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeWrapper::EyewearUserCalibratorGetMaxScaleHint()
 float QCARNativeWrapper_EyewearUserCalibratorGetMaxScaleHint_m3765 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearUserCalibratorIsStereoStretched()
 bool QCARNativeWrapper_EyewearUserCalibratorIsStereoStretched_m3766 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::EyewearUserCalibratorGetProjectionMatrix(System.IntPtr,System.Int32,System.IntPtr)
 bool QCARNativeWrapper_EyewearUserCalibratorGetProjectionMatrix_m3767 (QCARNativeWrapper_t707 * __this, IntPtr_t121 ___readingsArray, int32_t ___numReadings, IntPtr_t121 ___calibrationResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::cameraDeviceInitCamera(System.Int32)
 int32_t QCARNativeWrapper_cameraDeviceInitCamera_m3768 (Object_t * __this/* static, unused */, int32_t ___camera, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::cameraDeviceDeinitCamera()
 int32_t QCARNativeWrapper_cameraDeviceDeinitCamera_m3769 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::cameraDeviceStartCamera()
 int32_t QCARNativeWrapper_cameraDeviceStartCamera_m3770 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::cameraDeviceStopCamera()
 int32_t QCARNativeWrapper_cameraDeviceStopCamera_m3771 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::cameraDeviceGetNumVideoModes()
 int32_t QCARNativeWrapper_cameraDeviceGetNumVideoModes_m3772 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::cameraDeviceGetVideoMode(System.Int32,System.IntPtr)
 void QCARNativeWrapper_cameraDeviceGetVideoMode_m3773 (Object_t * __this/* static, unused */, int32_t ___idx, IntPtr_t121 ___videoMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::cameraDeviceSelectVideoMode(System.Int32)
 int32_t QCARNativeWrapper_cameraDeviceSelectVideoMode_m3774 (Object_t * __this/* static, unused */, int32_t ___idx, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::cameraDeviceSetFlashTorchMode(System.Int32)
 int32_t QCARNativeWrapper_cameraDeviceSetFlashTorchMode_m3775 (Object_t * __this/* static, unused */, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::cameraDeviceSetFocusMode(System.Int32)
 int32_t QCARNativeWrapper_cameraDeviceSetFocusMode_m3776 (Object_t * __this/* static, unused */, int32_t ___focusMode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::cameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
 int32_t QCARNativeWrapper_cameraDeviceSetCameraConfiguration_m3777 (Object_t * __this/* static, unused */, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::qcarSetFrameFormat(System.Int32,System.Int32)
 int32_t QCARNativeWrapper_qcarSetFrameFormat_m3778 (Object_t * __this/* static, unused */, int32_t ___format, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::dataSetExists(System.String,System.Int32)
 int32_t QCARNativeWrapper_dataSetExists_m3779 (Object_t * __this/* static, unused */, String_t* ___relativePath, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::dataSetLoad(System.String,System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_dataSetLoad_m3780 (Object_t * __this/* static, unused */, String_t* ___relativePath, int32_t ___storageType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::dataSetGetNumTrackableType(System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_dataSetGetNumTrackableType_m3781 (Object_t * __this/* static, unused */, int32_t ___trackableType, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::dataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_dataSetGetTrackablesOfType_m3782 (Object_t * __this/* static, unused */, int32_t ___trackableType, IntPtr_t121 ___trackableDataArray, int32_t ___trackableDataArrayLength, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::dataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNativeWrapper_dataSetGetTrackableName_m3783 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, StringBuilder_t418 * ___trackableName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::dataSetCreateTrackable(System.IntPtr,System.IntPtr,System.Text.StringBuilder,System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_dataSetCreateTrackable_m3784 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, IntPtr_t121 ___trackableSourcePtr, StringBuilder_t418 * ___trackableName, int32_t ___nameMaxLength, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::dataSetDestroyTrackable(System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_dataSetDestroyTrackable_m3785 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::dataSetHasReachedTrackableLimit(System.IntPtr)
 int32_t QCARNativeWrapper_dataSetHasReachedTrackableLimit_m3786 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::getCameraThreadID()
 int32_t QCARNativeWrapper_getCameraThreadID_m3787 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::imageTargetBuilderBuild(System.String,System.Single)
 int32_t QCARNativeWrapper_imageTargetBuilderBuild_m3788 (Object_t * __this/* static, unused */, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::frameCounterGetBenchmarkingData(System.IntPtr,System.Boolean)
 void QCARNativeWrapper_frameCounterGetBenchmarkingData_m3789 (Object_t * __this/* static, unused */, IntPtr_t121 ___benchmarkingData, bool ___isStereoRendering, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::imageTargetBuilderStartScan()
 void QCARNativeWrapper_imageTargetBuilderStartScan_m3790 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::imageTargetBuilderStopScan()
 void QCARNativeWrapper_imageTargetBuilderStopScan_m3791 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::imageTargetBuilderGetFrameQuality()
 int32_t QCARNativeWrapper_imageTargetBuilderGetFrameQuality_m3792 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeWrapper::imageTargetBuilderGetTrackableSource()
 IntPtr_t121 QCARNativeWrapper_imageTargetBuilderGetTrackableSource_m3793 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::imageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNativeWrapper_imageTargetCreateVirtualButton_m3794 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::imageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
 int32_t QCARNativeWrapper_imageTargetDestroyVirtualButton_m3795 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::virtualButtonGetId(System.IntPtr,System.String,System.String)
 int32_t QCARNativeWrapper_virtualButtonGetId_m3796 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::imageTargetGetNumVirtualButtons(System.IntPtr,System.String)
 int32_t QCARNativeWrapper_imageTargetGetNumVirtualButtons_m3797 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::imageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
 int32_t QCARNativeWrapper_imageTargetGetVirtualButtons_m3798 (Object_t * __this/* static, unused */, IntPtr_t121 ___virtualButtonDataArray, IntPtr_t121 ___rectangleDataArray, int32_t ___virtualButtonDataArrayLength, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::imageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
 int32_t QCARNativeWrapper_imageTargetGetVirtualButtonName_m3799 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, int32_t ___idx, StringBuilder_t418 * ___vbName, int32_t ___nameMaxLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::cylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeWrapper_cylinderTargetGetDimensions_m3800 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___dimensions, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::cylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeWrapper_cylinderTargetSetSideLength_m3801 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___sideLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::cylinderTargetSetTopDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeWrapper_cylinderTargetSetTopDiameter_m3802 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___topDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::cylinderTargetSetBottomDiameter(System.IntPtr,System.String,System.Single)
 int32_t QCARNativeWrapper_cylinderTargetSetBottomDiameter_m3803 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, float ___bottomDiameter, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::objectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeWrapper_objectTargetSetSize_m3804 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::objectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
 int32_t QCARNativeWrapper_objectTargetGetSize_m3805 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, IntPtr_t121 ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::objectTrackerStart()
 int32_t QCARNativeWrapper_objectTrackerStart_m3806 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::objectTrackerStop()
 void QCARNativeWrapper_objectTrackerStop_m3807 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeWrapper::objectTrackerCreateDataSet()
 IntPtr_t121 QCARNativeWrapper_objectTrackerCreateDataSet_m3808 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::objectTrackerDestroyDataSet(System.IntPtr)
 int32_t QCARNativeWrapper_objectTrackerDestroyDataSet_m3809 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::objectTrackerActivateDataSet(System.IntPtr)
 int32_t QCARNativeWrapper_objectTrackerActivateDataSet_m3810 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::objectTrackerDeactivateDataSet(System.IntPtr)
 int32_t QCARNativeWrapper_objectTrackerDeactivateDataSet_m3811 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::objectTrackerPersistExtendedTracking(System.Int32)
 int32_t QCARNativeWrapper_objectTrackerPersistExtendedTracking_m3812 (Object_t * __this/* static, unused */, int32_t ___on, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::objectTrackerResetExtendedTracking()
 int32_t QCARNativeWrapper_objectTrackerResetExtendedTracking_m3813 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::markerSetSize(System.Int32,System.Single)
 int32_t QCARNativeWrapper_markerSetSize_m3814 (Object_t * __this/* static, unused */, int32_t ___trackableIndex, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::markerTrackerStart()
 int32_t QCARNativeWrapper_markerTrackerStart_m3815 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::markerTrackerStop()
 void QCARNativeWrapper_markerTrackerStop_m3816 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::markerTrackerCreateMarker(System.Int32,System.String,System.Single)
 int32_t QCARNativeWrapper_markerTrackerCreateMarker_m3817 (Object_t * __this/* static, unused */, int32_t ___id, String_t* ___trackableName, float ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::initPlatformNative()
 void QCARNativeWrapper_initPlatformNative_m3818 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::markerTrackerDestroyMarker(System.Int32)
 int32_t QCARNativeWrapper_markerTrackerDestroyMarker_m3819 (Object_t * __this/* static, unused */, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::initFrameState(System.IntPtr)
 void QCARNativeWrapper_initFrameState_m3820 (Object_t * __this/* static, unused */, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::deinitFrameState(System.IntPtr)
 void QCARNativeWrapper_deinitFrameState_m3821 (Object_t * __this/* static, unused */, IntPtr_t121 ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::onSurfaceChanged(System.Int32,System.Int32)
 void QCARNativeWrapper_onSurfaceChanged_m3822 (Object_t * __this/* static, unused */, int32_t ___width, int32_t ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::onPause()
 void QCARNativeWrapper_onPause_m3823 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::onResume()
 void QCARNativeWrapper_onResume_m3824 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::hasSurfaceBeenRecreated()
 bool QCARNativeWrapper_hasSurfaceBeenRecreated_m3825 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::updateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_updateQCAR_m3826 (Object_t * __this/* static, unused */, IntPtr_t121 ___imageHeaderDataArray, int32_t ___imageHeaderArrayLength, IntPtr_t121 ___frameIndex, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::rendererEnd()
 void QCARNativeWrapper_rendererEnd_m3827 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::qcarGetBufferSize(System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeWrapper_qcarGetBufferSize_m3828 (Object_t * __this/* static, unused */, int32_t ___width, int32_t ___height, int32_t ___format, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::qcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 void QCARNativeWrapper_qcarAddCameraFrame_m3829 (Object_t * __this/* static, unused */, IntPtr_t121 ___pixels, int32_t ___width, int32_t ___height, int32_t ___format, int32_t ___stride, int32_t ___frameIdx, int32_t ___flipHorizontally, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::rendererSetVideoBackgroundCfg(System.IntPtr)
 void QCARNativeWrapper_rendererSetVideoBackgroundCfg_m3830 (Object_t * __this/* static, unused */, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::rendererGetVideoBackgroundCfg(System.IntPtr)
 void QCARNativeWrapper_rendererGetVideoBackgroundCfg_m3831 (Object_t * __this/* static, unused */, IntPtr_t121 ___bgCfg, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::rendererGetVideoBackgroundTextureInfo(System.IntPtr)
 void QCARNativeWrapper_rendererGetVideoBackgroundTextureInfo_m3832 (Object_t * __this/* static, unused */, IntPtr_t121 ___texInfo, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::rendererSetVideoBackgroundTextureID(System.Int32)
 int32_t QCARNativeWrapper_rendererSetVideoBackgroundTextureID_m3833 (Object_t * __this/* static, unused */, int32_t ___textureID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::rendererIsVideoBackgroundTextureInfoAvailable()
 int32_t QCARNativeWrapper_rendererIsVideoBackgroundTextureInfoAvailable_m3834 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::qcarInit(System.String)
 int32_t QCARNativeWrapper_qcarInit_m3835 (Object_t * __this/* static, unused */, String_t* ___licenseKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::qcarSetHint(System.Int32,System.Int32)
 int32_t QCARNativeWrapper_qcarSetHint_m3836 (Object_t * __this/* static, unused */, int32_t ___hint, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::getProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_getProjectionGL_m3837 (Object_t * __this/* static, unused */, float ___nearClip, float ___farClip, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::setApplicationEnvironment(System.Int32,System.Int32,System.Int32)
 void QCARNativeWrapper_setApplicationEnvironment_m3838 (Object_t * __this/* static, unused */, int32_t ___unityVersionMajor, int32_t ___unityVersionMinor, int32_t ___unityVersionChange, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::setStateBufferSize(System.Int32)
 void QCARNativeWrapper_setStateBufferSize_m3839 (Object_t * __this/* static, unused */, int32_t ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::smartTerrainTrackerStart()
 int32_t QCARNativeWrapper_smartTerrainTrackerStart_m3840 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::smartTerrainTrackerStop()
 void QCARNativeWrapper_smartTerrainTrackerStop_m3841 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::smartTerrainTrackerSetScaleToMillimeter(System.Single)
 bool QCARNativeWrapper_smartTerrainTrackerSetScaleToMillimeter_m3842 (Object_t * __this/* static, unused */, float ___scaleFactor, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::smartTerrainTrackerInitBuilder()
 bool QCARNativeWrapper_smartTerrainTrackerInitBuilder_m3843 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::smartTerrainTrackerDeinitBuilder()
 bool QCARNativeWrapper_smartTerrainTrackerDeinitBuilder_m3844 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeWrapper::smartTerrainBuilderCreateReconstructionFromTarget()
 IntPtr_t121 QCARNativeWrapper_smartTerrainBuilderCreateReconstructionFromTarget_m3845 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeWrapper::smartTerrainBuilderCreateReconstructionFromEnvironment()
 IntPtr_t121 QCARNativeWrapper_smartTerrainBuilderCreateReconstructionFromEnvironment_m3846 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::smartTerrainBuilderAddReconstruction(System.IntPtr)
 bool QCARNativeWrapper_smartTerrainBuilderAddReconstruction_m3847 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::smartTerrainBuilderRemoveReconstruction(System.IntPtr)
 bool QCARNativeWrapper_smartTerrainBuilderRemoveReconstruction_m3848 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::smartTerrainBuilderDestroyReconstruction(System.IntPtr)
 bool QCARNativeWrapper_smartTerrainBuilderDestroyReconstruction_m3849 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::reconstructionStart(System.IntPtr)
 bool QCARNativeWrapper_reconstructionStart_m3850 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::reconstructionStop(System.IntPtr)
 bool QCARNativeWrapper_reconstructionStop_m3851 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::reconstructionIsReconstructing(System.IntPtr)
 bool QCARNativeWrapper_reconstructionIsReconstructing_m3852 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::reconstructionReset(System.IntPtr)
 bool QCARNativeWrapper_reconstructionReset_m3853 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::reconstructionSetNavMeshPadding(System.IntPtr,System.Single)
 void QCARNativeWrapper_reconstructionSetNavMeshPadding_m3854 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, float ___padding, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::reconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
 bool QCARNativeWrapper_reconstructionFromTargetSetInitializationTarget_m3855 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, IntPtr_t121 ___occluderMin, IntPtr_t121 ___occluderMax, IntPtr_t121 ___offsetToOccluder, IntPtr_t121 ___rotationAxisToOccluder, float ___rotationAngleToOccluder, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.QCARNativeWrapper::reconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
 bool QCARNativeWrapper_reconstructionSetMaximumArea_m3856 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, IntPtr_t121 ___maximumArea, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::reconstructioFromEnvironmentGetReconstructionState(System.IntPtr)
 int32_t QCARNativeWrapper_reconstructioFromEnvironmentGetReconstructionState_m3857 (Object_t * __this/* static, unused */, IntPtr_t121 ___reconstruction, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::targetFinderStartInit(System.String,System.String)
 int32_t QCARNativeWrapper_targetFinderStartInit_m3858 (Object_t * __this/* static, unused */, String_t* ___userKey, String_t* ___secretKey, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::targetFinderGetInitState()
 int32_t QCARNativeWrapper_targetFinderGetInitState_m3859 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::targetFinderDeinit()
 int32_t QCARNativeWrapper_targetFinderDeinit_m3860 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::targetFinderStartRecognition()
 int32_t QCARNativeWrapper_targetFinderStartRecognition_m3861 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::targetFinderStop()
 int32_t QCARNativeWrapper_targetFinderStop_m3862 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::targetFinderSetUIScanlineColor(System.Single,System.Single,System.Single)
 void QCARNativeWrapper_targetFinderSetUIScanlineColor_m3863 (Object_t * __this/* static, unused */, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::targetFinderSetUIPointColor(System.Single,System.Single,System.Single)
 void QCARNativeWrapper_targetFinderSetUIPointColor_m3864 (Object_t * __this/* static, unused */, float ___r, float ___g, float ___b, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::targetFinderUpdate(System.IntPtr)
 void QCARNativeWrapper_targetFinderUpdate_m3865 (Object_t * __this/* static, unused */, IntPtr_t121 ___targetFinderState, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::targetFinderGetResults(System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_targetFinderGetResults_m3866 (Object_t * __this/* static, unused */, IntPtr_t121 ___searchResultArray, int32_t ___searchResultArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::targetFinderEnableTracking(System.IntPtr,System.IntPtr)
 int32_t QCARNativeWrapper_targetFinderEnableTracking_m3867 (Object_t * __this/* static, unused */, IntPtr_t121 ___searchResult, IntPtr_t121 ___trackableData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::targetFinderGetImageTargets(System.IntPtr,System.Int32)
 void QCARNativeWrapper_targetFinderGetImageTargets_m3868 (Object_t * __this/* static, unused */, IntPtr_t121 ___trackableIdArray, int32_t ___trackableIdArrayLength, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::targetFinderClearTrackables()
 void QCARNativeWrapper_targetFinderClearTrackables_m3869 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::textTrackerStart()
 int32_t QCARNativeWrapper_textTrackerStart_m3870 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::textTrackerStop()
 void QCARNativeWrapper_textTrackerStop_m3871 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::textTrackerSetRegionOfInterest(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeWrapper_textTrackerSetRegionOfInterest_m3872 (Object_t * __this/* static, unused */, int32_t ___detectionLeftTopX, int32_t ___detectionLeftTopY, int32_t ___detectionRightBottomX, int32_t ___detectionRightBottomY, int32_t ___trackingLeftTopX, int32_t ___trackingLeftTopY, int32_t ___trackingRightBottomX, int32_t ___trackingRightBottomY, int32_t ___upDirection, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::textTrackerGetRegionOfInterest(System.IntPtr,System.IntPtr)
 int32_t QCARNativeWrapper_textTrackerGetRegionOfInterest_m3873 (Object_t * __this/* static, unused */, IntPtr_t121 ___detectionROI, IntPtr_t121 ___trackingROI, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordListLoadWordList(System.String,System.Int32)
 int32_t QCARNativeWrapper_wordListLoadWordList_m3874 (Object_t * __this/* static, unused */, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordListAddWordsFromFile(System.String,System.Int32)
 int32_t QCARNativeWrapper_wordListAddWordsFromFile_m3875 (Object_t * __this/* static, unused */, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordListAddWordU(System.IntPtr)
 int32_t QCARNativeWrapper_wordListAddWordU_m3876 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordListRemoveWordU(System.IntPtr)
 int32_t QCARNativeWrapper_wordListRemoveWordU_m3877 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordListContainsWordU(System.IntPtr)
 int32_t QCARNativeWrapper_wordListContainsWordU_m3878 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordListUnloadAllLists()
 int32_t QCARNativeWrapper_wordListUnloadAllLists_m3879 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordListSetFilterMode(System.Int32)
 int32_t QCARNativeWrapper_wordListSetFilterMode_m3880 (Object_t * __this/* static, unused */, int32_t ___mode, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordListGetFilterMode()
 int32_t QCARNativeWrapper_wordListGetFilterMode_m3881 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordListAddWordToFilterListU(System.IntPtr)
 int32_t QCARNativeWrapper_wordListAddWordToFilterListU_m3882 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordListRemoveWordFromFilterListU(System.IntPtr)
 int32_t QCARNativeWrapper_wordListRemoveWordFromFilterListU_m3883 (Object_t * __this/* static, unused */, IntPtr_t121 ___word, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordListClearFilterList()
 int32_t QCARNativeWrapper_wordListClearFilterList_m3884 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordListLoadFilterList(System.String,System.Int32)
 int32_t QCARNativeWrapper_wordListLoadFilterList_m3885 (Object_t * __this/* static, unused */, String_t* ___path, int32_t ___storageType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordListGetFilterListWordCount()
 int32_t QCARNativeWrapper_wordListGetFilterListWordCount_m3886 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeWrapper::wordListGetFilterListWordU(System.Int32)
 IntPtr_t121 QCARNativeWrapper_wordListGetFilterListWordU_m3887 (Object_t * __this/* static, unused */, int32_t ___i, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordGetLetterMask(System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_wordGetLetterMask_m3888 (Object_t * __this/* static, unused */, int32_t ___wordID, IntPtr_t121 ___letterMaskImage, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::wordGetLetterBoundingBoxes(System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_wordGetLetterBoundingBoxes_m3889 (Object_t * __this/* static, unused */, int32_t ___wordID, IntPtr_t121 ___letterBoundingBoxes, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::trackerManagerInitTracker(System.Int32)
 int32_t QCARNativeWrapper_trackerManagerInitTracker_m3890 (Object_t * __this/* static, unused */, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::trackerManagerDeinitTracker(System.Int32)
 int32_t QCARNativeWrapper_trackerManagerDeinitTracker_m3891 (Object_t * __this/* static, unused */, int32_t ___trackerType, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::virtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNativeWrapper_virtualButtonSetEnabled_m3892 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::virtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
 int32_t QCARNativeWrapper_virtualButtonSetSensitivity_m3893 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, int32_t ___sensitivity, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::virtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
 int32_t QCARNativeWrapper_virtualButtonSetAreaRectangle_m3894 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, String_t* ___trackableName, String_t* ___virtualButtonName, IntPtr_t121 ___rectData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::qcarDeinit()
 int32_t QCARNativeWrapper_qcarDeinit_m3895 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::startExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_startExtendedTracking_m3896 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::stopExtendedTracking(System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_stopExtendedTracking_m3897 (Object_t * __this/* static, unused */, IntPtr_t121 ___dataSetPtr, int32_t ___trackableId, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearIsSupportedDeviceDetected()
 int32_t QCARNativeWrapper_eyewearIsSupportedDeviceDetected_m3898 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearIsSeeThru()
 int32_t QCARNativeWrapper_eyewearIsSeeThru_m3899 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearGetScreenOrientation()
 int32_t QCARNativeWrapper_eyewearGetScreenOrientation_m3900 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearIsStereoCapable()
 int32_t QCARNativeWrapper_eyewearIsStereoCapable_m3901 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearIsStereoEnabled()
 int32_t QCARNativeWrapper_eyewearIsStereoEnabled_m3902 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearIsStereoGLOnly()
 int32_t QCARNativeWrapper_eyewearIsStereoGLOnly_m3903 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearSetStereo(System.Boolean)
 int32_t QCARNativeWrapper_eyewearSetStereo_m3904 (Object_t * __this/* static, unused */, bool ___enable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearGetDefaultSceneScale(System.IntPtr)
 int32_t QCARNativeWrapper_eyewearGetDefaultSceneScale_m3905 (Object_t * __this/* static, unused */, IntPtr_t121 ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr,System.Int32)
 int32_t QCARNativeWrapper_eyewearGetProjectionMatrix_m3906 (Object_t * __this/* static, unused */, int32_t ___eyeID, int32_t ___profileID, IntPtr_t121 ___projMatrix, int32_t ___screenOrientation, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearCPMGetMaxCount()
 int32_t QCARNativeWrapper_eyewearCPMGetMaxCount_m3907 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearCPMGetUsedCount()
 int32_t QCARNativeWrapper_eyewearCPMGetUsedCount_m3908 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearCPMIsProfileUsed(System.Int32)
 int32_t QCARNativeWrapper_eyewearCPMIsProfileUsed_m3909 (Object_t * __this/* static, unused */, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearCPMGetActiveProfile()
 int32_t QCARNativeWrapper_eyewearCPMGetActiveProfile_m3910 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearCPMSetActiveProfile(System.Int32)
 int32_t QCARNativeWrapper_eyewearCPMSetActiveProfile_m3911 (Object_t * __this/* static, unused */, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearCPMGetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_eyewearCPMGetProjectionMatrix_m3912 (Object_t * __this/* static, unused */, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearCPMSetProjectionMatrix(System.Int32,System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_eyewearCPMSetProjectionMatrix_m3913 (Object_t * __this/* static, unused */, int32_t ___profileID, int32_t ___eyeID, IntPtr_t121 ___projMatrix, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.QCARNativeWrapper::eyewearCPMGetProfileName(System.Int32)
 IntPtr_t121 QCARNativeWrapper_eyewearCPMGetProfileName_m3914 (Object_t * __this/* static, unused */, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearCPMSetProfileName(System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_eyewearCPMSetProfileName_m3915 (Object_t * __this/* static, unused */, int32_t ___profileID, IntPtr_t121 ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearCPMClearProfile(System.Int32)
 int32_t QCARNativeWrapper_eyewearCPMClearProfile_m3916 (Object_t * __this/* static, unused */, int32_t ___profileID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearUserCalibratorInit(System.Int32,System.Int32,System.Int32,System.Int32)
 int32_t QCARNativeWrapper_eyewearUserCalibratorInit_m3917 (Object_t * __this/* static, unused */, int32_t ___surfaceWidth, int32_t ___surfaceHeight, int32_t ___targetWidth, int32_t ___targetHeight, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeWrapper::eyewearUserCalibratorGetMinScaleHint()
 float QCARNativeWrapper_eyewearUserCalibratorGetMinScaleHint_m3918 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.QCARNativeWrapper::eyewearUserCalibratorGetMaxScaleHint()
 float QCARNativeWrapper_eyewearUserCalibratorGetMaxScaleHint_m3919 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearUserCalibratorIsStereoStretched()
 int32_t QCARNativeWrapper_eyewearUserCalibratorIsStereoStretched_m3920 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.QCARNativeWrapper::eyewearUserCalibratorGetProjectionMatrix(System.IntPtr,System.Int32,System.IntPtr)
 int32_t QCARNativeWrapper_eyewearUserCalibratorGetProjectionMatrix_m3921 (Object_t * __this/* static, unused */, IntPtr_t121 ___readingsArray, int32_t ___numReadings, IntPtr_t121 ___calibrationResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARNativeWrapper::.ctor()
 void QCARNativeWrapper__ctor_m3922 (QCARNativeWrapper_t707 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
