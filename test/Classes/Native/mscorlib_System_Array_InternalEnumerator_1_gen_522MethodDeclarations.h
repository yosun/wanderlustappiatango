﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.UInt32>
struct InternalEnumerator_1_t4900;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m30082 (InternalEnumerator_1_t4900 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m30083 (InternalEnumerator_1_t4900 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
 void InternalEnumerator_1_Dispose_m30084 (InternalEnumerator_1_t4900 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m30085 (InternalEnumerator_1_t4900 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
 uint32_t InternalEnumerator_1_get_Current_m30086 (InternalEnumerator_1_t4900 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
