﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo UILineInfo_t487_il2cpp_TypeInfo;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t4705  : public MulticastDelegate_t325
{
};
