﻿#pragma once
#include <stdint.h>
// System.Byte[]
struct ByteU5BU5D_t609;
// Mono.Security.Protocol.Tls.Handshake.HandshakeMessage
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake_0.h"
// Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished
struct TlsClientFinished_t1638  : public HandshakeMessage_t1600
{
};
struct TlsClientFinished_t1638_StaticFields{
	// System.Byte[] Mono.Security.Protocol.Tls.Handshake.Client.TlsClientFinished::Ssl3Marker
	ByteU5BU5D_t609* ___Ssl3Marker_9;
};
