﻿#pragma once
#include <stdint.h>
// System.Int32[]
struct Int32U5BU5D_t21;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.HashSet`1/PrimeHelper<UnityEngine.MeshRenderer>
struct PrimeHelper_t4398  : public Object_t
{
};
struct PrimeHelper_t4398_StaticFields{
	// System.Int32[] System.Collections.Generic.HashSet`1/PrimeHelper<UnityEngine.MeshRenderer>::primes_table
	Int32U5BU5D_t21* ___primes_table_0;
};
