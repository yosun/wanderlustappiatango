﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.MonoProperty/StaticGetter`1<System.Object>
struct StaticGetter_1_t5040;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Reflection.MonoProperty/StaticGetter`1<System.Object>::.ctor(System.Object,System.IntPtr)
 void StaticGetter_1__ctor_m30781_gshared (StaticGetter_1_t5040 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method);
#define StaticGetter_1__ctor_m30781(__this, ___object, ___method, method) (void)StaticGetter_1__ctor_m30781_gshared((StaticGetter_1_t5040 *)__this, (Object_t *)___object, (IntPtr_t121)___method, method)
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::Invoke()
 Object_t * StaticGetter_1_Invoke_m30782_gshared (StaticGetter_1_t5040 * __this, MethodInfo* method);
#define StaticGetter_1_Invoke_m30782(__this, method) (Object_t *)StaticGetter_1_Invoke_m30782_gshared((StaticGetter_1_t5040 *)__this, method)
// System.IAsyncResult System.Reflection.MonoProperty/StaticGetter`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
 Object_t * StaticGetter_1_BeginInvoke_m30783_gshared (StaticGetter_1_t5040 * __this, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method);
#define StaticGetter_1_BeginInvoke_m30783(__this, ___callback, ___object, method) (Object_t *)StaticGetter_1_BeginInvoke_m30783_gshared((StaticGetter_1_t5040 *)__this, (AsyncCallback_t200 *)___callback, (Object_t *)___object, method)
// R System.Reflection.MonoProperty/StaticGetter`1<System.Object>::EndInvoke(System.IAsyncResult)
 Object_t * StaticGetter_1_EndInvoke_m30784_gshared (StaticGetter_1_t5040 * __this, Object_t * ___result, MethodInfo* method);
#define StaticGetter_1_EndInvoke_m30784(__this, ___result, method) (Object_t *)StaticGetter_1_EndInvoke_m30784_gshared((StaticGetter_1_t5040 *)__this, (Object_t *)___result, method)
