﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.KeyedHashAlgorithm
struct KeyedHashAlgorithm_t1583;
// System.Byte[]
struct ByteU5BU5D_t609;

// System.Void System.Security.Cryptography.KeyedHashAlgorithm::.ctor()
 void KeyedHashAlgorithm__ctor_m8871 (KeyedHashAlgorithm_t1583 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::Finalize()
 void KeyedHashAlgorithm_Finalize_m8872 (KeyedHashAlgorithm_t1583 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::get_Key()
 ByteU5BU5D_t609* KeyedHashAlgorithm_get_Key_m11936 (KeyedHashAlgorithm_t1583 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::set_Key(System.Byte[])
 void KeyedHashAlgorithm_set_Key_m11937 (KeyedHashAlgorithm_t1583 * __this, ByteU5BU5D_t609* ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::Dispose(System.Boolean)
 void KeyedHashAlgorithm_Dispose_m8873 (KeyedHashAlgorithm_t1583 * __this, bool ___disposing, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::ZeroizeKey()
 void KeyedHashAlgorithm_ZeroizeKey_m11938 (KeyedHashAlgorithm_t1583 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
