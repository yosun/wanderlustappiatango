﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// System.MonoTODOAttribute[]
// System.MonoTODOAttribute[]
struct MonoTODOAttributeU5BU5D_t5649  : public Array_t
{
};
// System.ComponentModel.EditorBrowsableAttribute[]
// System.ComponentModel.EditorBrowsableAttribute[]
struct EditorBrowsableAttributeU5BU5D_t5650  : public Array_t
{
};
// System.ComponentModel.EditorBrowsableState[]
// System.ComponentModel.EditorBrowsableState[]
struct EditorBrowsableStateU5BU5D_t5651  : public Array_t
{
};
// System.ComponentModel.TypeConverterAttribute[]
// System.ComponentModel.TypeConverterAttribute[]
struct TypeConverterAttributeU5BU5D_t5652  : public Array_t
{
};
struct TypeConverterAttributeU5BU5D_t5652_StaticFields{
};
// System.Net.Security.AuthenticationLevel[]
// System.Net.Security.AuthenticationLevel[]
struct AuthenticationLevelU5BU5D_t5653  : public Array_t
{
};
// System.Net.Security.SslPolicyErrors[]
// System.Net.Security.SslPolicyErrors[]
struct SslPolicyErrorsU5BU5D_t5654  : public Array_t
{
};
// System.Net.Sockets.AddressFamily[]
// System.Net.Sockets.AddressFamily[]
struct AddressFamilyU5BU5D_t5655  : public Array_t
{
};
// System.Net.SecurityProtocolType[]
// System.Net.SecurityProtocolType[]
struct SecurityProtocolTypeU5BU5D_t5656  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.OpenFlags[]
// System.Security.Cryptography.X509Certificates.OpenFlags[]
struct OpenFlagsU5BU5D_t5657  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.StoreLocation[]
// System.Security.Cryptography.X509Certificates.StoreLocation[]
struct StoreLocationU5BU5D_t5658  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.StoreName[]
// System.Security.Cryptography.X509Certificates.StoreName[]
struct StoreNameU5BU5D_t5659  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags[]
// System.Security.Cryptography.X509Certificates.X500DistinguishedNameFlags[]
struct X500DistinguishedNameFlagsU5BU5D_t5660  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
// System.Security.Cryptography.X509Certificates.X509ChainStatus[]
struct X509ChainStatusU5BU5D_t1373  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags[]
// System.Security.Cryptography.X509Certificates.X509ChainStatusFlags[]
struct X509ChainStatusFlagsU5BU5D_t5661  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509FindType[]
// System.Security.Cryptography.X509Certificates.X509FindType[]
struct X509FindTypeU5BU5D_t5662  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags[]
// System.Security.Cryptography.X509Certificates.X509KeyUsageFlags[]
struct X509KeyUsageFlagsU5BU5D_t5663  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509NameType[]
// System.Security.Cryptography.X509Certificates.X509NameType[]
struct X509NameTypeU5BU5D_t5664  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509RevocationFlag[]
// System.Security.Cryptography.X509Certificates.X509RevocationFlag[]
struct X509RevocationFlagU5BU5D_t5665  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509RevocationMode[]
// System.Security.Cryptography.X509Certificates.X509RevocationMode[]
struct X509RevocationModeU5BU5D_t5666  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm[]
// System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierHashAlgorithm[]
struct X509SubjectKeyIdentifierHashAlgorithmU5BU5D_t5667  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509VerificationFlags[]
// System.Security.Cryptography.X509Certificates.X509VerificationFlags[]
struct X509VerificationFlagsU5BU5D_t5668  : public Array_t
{
};
// System.Security.Cryptography.AsnDecodeStatus[]
// System.Security.Cryptography.AsnDecodeStatus[]
struct AsnDecodeStatusU5BU5D_t5669  : public Array_t
{
};
// System.Text.RegularExpressions.Capture[]
// System.Text.RegularExpressions.Capture[]
struct CaptureU5BU5D_t1401  : public Array_t
{
};
// System.Text.RegularExpressions.Group[]
// System.Text.RegularExpressions.Group[]
struct GroupU5BU5D_t1404  : public Array_t
{
};
struct GroupU5BU5D_t1404_StaticFields{
};
// System.Text.RegularExpressions.RegexOptions[]
// System.Text.RegularExpressions.RegexOptions[]
struct RegexOptionsU5BU5D_t5670  : public Array_t
{
};
// System.Text.RegularExpressions.OpCode[]
// System.Text.RegularExpressions.OpCode[]
struct OpCodeU5BU5D_t5671  : public Array_t
{
};
// System.Text.RegularExpressions.OpFlags[]
// System.Text.RegularExpressions.OpFlags[]
struct OpFlagsU5BU5D_t5672  : public Array_t
{
};
// System.Text.RegularExpressions.Position[]
// System.Text.RegularExpressions.Position[]
struct PositionU5BU5D_t5673  : public Array_t
{
};
// System.Text.RegularExpressions.Category[]
// System.Text.RegularExpressions.Category[]
struct CategoryU5BU5D_t5674  : public Array_t
{
};
// System.Text.RegularExpressions.Mark[]
// System.Text.RegularExpressions.Mark[]
struct MarkU5BU5D_t1432  : public Array_t
{
};
// System.Text.RegularExpressions.Interpreter/Mode[]
// System.Text.RegularExpressions.Interpreter/Mode[]
struct ModeU5BU5D_t5675  : public Array_t
{
};
// System.Uri/UriScheme[]
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_t1464  : public Array_t
{
};
// System.UriHostNameType[]
// System.UriHostNameType[]
struct UriHostNameTypeU5BU5D_t5676  : public Array_t
{
};
// System.UriKind[]
// System.UriKind[]
struct UriKindU5BU5D_t5677  : public Array_t
{
};
// System.UriPartial[]
// System.UriPartial[]
struct UriPartialU5BU5D_t5678  : public Array_t
{
};
