﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic
struct Graphic_t293;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.UI.Graphic>
struct CastHelper_1_t3495 
{
	// T UnityEngine.CastHelper`1<UnityEngine.UI.Graphic>::t
	Graphic_t293 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.UI.Graphic>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
