﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.ImageTargetAbstractBehaviour>
struct UnityAction_1_t4284;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.ImageTargetAbstractBehaviour>
struct InvokableCall_1_t4283  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.ImageTargetAbstractBehaviour>::Delegate
	UnityAction_1_t4284 * ___Delegate_0;
};
