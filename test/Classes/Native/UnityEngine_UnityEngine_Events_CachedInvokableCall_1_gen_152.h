﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.CanvasGroup>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_154.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.CanvasGroup>
struct CachedInvokableCall_1_t4717  : public InvokableCall_1_t4718
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.CanvasGroup>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
