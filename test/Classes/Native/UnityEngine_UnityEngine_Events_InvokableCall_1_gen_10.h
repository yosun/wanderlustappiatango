﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<TestParticles>
struct UnityAction_1_t2772;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<TestParticles>
struct InvokableCall_1_t2771  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<TestParticles>::Delegate
	UnityAction_1_t2772 * ___Delegate_0;
};
