﻿#pragma once
#include <stdint.h>
// Vuforia.ImageTarget
struct ImageTarget_t572;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.ImageTarget>
struct Predicate_1_t4253  : public MulticastDelegate_t325
{
};
