﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct KeyValuePair_2_t4175;
// System.String
struct String_t;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m25121 (KeyValuePair_2_t4175 * __this, int32_t ___key, TrackableResultData_t639  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Key()
 int32_t KeyValuePair_2_get_Key_m25122 (KeyValuePair_2_t4175 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m25123 (KeyValuePair_2_t4175 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Value()
 TrackableResultData_t639  KeyValuePair_2_get_Value_m25124 (KeyValuePair_2_t4175 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m25125 (KeyValuePair_2_t4175 * __this, TrackableResultData_t639  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::ToString()
 String_t* KeyValuePair_2_ToString_m25126 (KeyValuePair_2_t4175 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
