﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.ImageTargetType>
struct InternalEnumerator_1_t3763;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.ImageTargetType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetType.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetType>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m21059 (InternalEnumerator_1_t3763 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ImageTargetType>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21060 (InternalEnumerator_1_t3763 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetType>::Dispose()
 void InternalEnumerator_1_Dispose_m21061 (InternalEnumerator_1_t3763 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ImageTargetType>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m21062 (InternalEnumerator_1_t3763 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.ImageTargetType>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m21063 (InternalEnumerator_1_t3763 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
