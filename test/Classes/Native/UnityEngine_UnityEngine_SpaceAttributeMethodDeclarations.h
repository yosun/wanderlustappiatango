﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SpaceAttribute
struct SpaceAttribute_t496;

// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
 void SpaceAttribute__ctor_m2371 (SpaceAttribute_t496 * __this, float ___height, MethodInfo* method) IL2CPP_METHOD_ATTR;
