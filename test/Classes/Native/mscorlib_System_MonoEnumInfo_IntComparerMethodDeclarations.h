﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoEnumInfo/IntComparer
struct IntComparer_t2211;
// System.Object
struct Object_t;

// System.Void System.MonoEnumInfo/IntComparer::.ctor()
 void IntComparer__ctor_m12902 (IntComparer_t2211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MonoEnumInfo/IntComparer::Compare(System.Object,System.Object)
 int32_t IntComparer_Compare_m12903 (IntComparer_t2211 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MonoEnumInfo/IntComparer::Compare(System.Int32,System.Int32)
 int32_t IntComparer_Compare_m12904 (IntComparer_t2211 * __this, int32_t ___ix, int32_t ___iy, MethodInfo* method) IL2CPP_METHOD_ATTR;
