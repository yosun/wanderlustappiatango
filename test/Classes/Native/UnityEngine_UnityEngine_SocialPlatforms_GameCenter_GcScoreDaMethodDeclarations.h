﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
struct GcScoreData_t933;
struct GcScoreData_t933_marshaled;
// UnityEngine.SocialPlatforms.Impl.Score
struct Score_t1045;

// UnityEngine.SocialPlatforms.Impl.Score UnityEngine.SocialPlatforms.GameCenter.GcScoreData::ToScore()
 Score_t1045 * GcScoreData_ToScore_m6248 (GcScoreData_t933 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
void GcScoreData_t933_marshal(const GcScoreData_t933& unmarshaled, GcScoreData_t933_marshaled& marshaled);
void GcScoreData_t933_marshal_back(const GcScoreData_t933_marshaled& marshaled, GcScoreData_t933& unmarshaled);
void GcScoreData_t933_marshal_cleanup(GcScoreData_t933_marshaled& marshaled);
