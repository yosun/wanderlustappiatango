﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.EventTrigger/Entry
struct Entry_t193;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.EventSystems.EventTrigger/Entry>
struct Predicate_1_t3111  : public MulticastDelegate_t325
{
};
