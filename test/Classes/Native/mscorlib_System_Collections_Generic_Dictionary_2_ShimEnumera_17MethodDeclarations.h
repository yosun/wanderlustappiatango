﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>
struct ShimEnumerator_t4119;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.Prop>
struct Dictionary_2_t716;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m24546 (ShimEnumerator_t4119 * __this, Dictionary_2_t716 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>::MoveNext()
 bool ShimEnumerator_MoveNext_m24547 (ShimEnumerator_t4119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m24548 (ShimEnumerator_t4119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>::get_Key()
 Object_t * ShimEnumerator_get_Key_m24549 (ShimEnumerator_t4119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>::get_Value()
 Object_t * ShimEnumerator_get_Value_m24550 (ShimEnumerator_t4119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.Prop>::get_Current()
 Object_t * ShimEnumerator_get_Current_m24551 (ShimEnumerator_t4119 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
