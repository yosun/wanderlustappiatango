﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// Vuforia.SmartTerrainInitializationInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainInitial.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
extern TypeInfo SmartTerrainInitializationInfo_t580_il2cpp_TypeInfo;
// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t713  : public MulticastDelegate_t325
{
};
