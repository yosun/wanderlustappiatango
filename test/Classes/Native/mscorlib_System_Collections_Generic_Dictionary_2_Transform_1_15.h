﻿#pragma once
#include <stdint.h>
// UnityEngine.Font
struct Font_t280;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t462;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,UnityEngine.Font>
struct Transform_1_t3314  : public MulticastDelegate_t325
{
};
