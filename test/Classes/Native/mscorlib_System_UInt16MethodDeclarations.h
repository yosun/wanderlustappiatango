﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.UInt16
struct UInt16_t823;
// System.IFormatProvider
struct IFormatProvider_t1660;
// System.Object
struct Object_t;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.DateTime
#include "mscorlib_System_DateTime.h"
// System.Decimal
#include "mscorlib_System_Decimal.h"
// System.Globalization.NumberStyles
#include "mscorlib_System_Globalization_NumberStyles.h"

// System.Boolean System.UInt16::System.IConvertible.ToBoolean(System.IFormatProvider)
 bool UInt16_System_IConvertible_ToBoolean_m9171 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System.UInt16::System.IConvertible.ToByte(System.IFormatProvider)
 uint8_t UInt16_System_IConvertible_ToByte_m9172 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System.UInt16::System.IConvertible.ToChar(System.IFormatProvider)
 uint16_t UInt16_System_IConvertible_ToChar_m9173 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime System.UInt16::System.IConvertible.ToDateTime(System.IFormatProvider)
 DateTime_t110  UInt16_System_IConvertible_ToDateTime_m9174 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal System.UInt16::System.IConvertible.ToDecimal(System.IFormatProvider)
 Decimal_t1687  UInt16_System_IConvertible_ToDecimal_m9175 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System.UInt16::System.IConvertible.ToDouble(System.IFormatProvider)
 double UInt16_System_IConvertible_ToDouble_m9176 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System.UInt16::System.IConvertible.ToInt16(System.IFormatProvider)
 int16_t UInt16_System_IConvertible_ToInt16_m9177 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::System.IConvertible.ToInt32(System.IFormatProvider)
 int32_t UInt16_System_IConvertible_ToInt32_m9178 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.UInt16::System.IConvertible.ToInt64(System.IFormatProvider)
 int64_t UInt16_System_IConvertible_ToInt64_m9179 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte System.UInt16::System.IConvertible.ToSByte(System.IFormatProvider)
 int8_t UInt16_System_IConvertible_ToSByte_m9180 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System.UInt16::System.IConvertible.ToSingle(System.IFormatProvider)
 float UInt16_System_IConvertible_ToSingle_m9181 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.UInt16::System.IConvertible.ToType(System.Type,System.IFormatProvider)
 Object_t * UInt16_System_IConvertible_ToType_m9182 (uint16_t* __this, Type_t * ___targetType, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.UInt16::System.IConvertible.ToUInt16(System.IFormatProvider)
 uint16_t UInt16_System_IConvertible_ToUInt16_m9183 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.UInt16::System.IConvertible.ToUInt32(System.IFormatProvider)
 uint32_t UInt16_System_IConvertible_ToUInt32_m9184 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System.UInt16::System.IConvertible.ToUInt64(System.IFormatProvider)
 uint64_t UInt16_System_IConvertible_ToUInt64_m9185 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::CompareTo(System.Object)
 int32_t UInt16_CompareTo_m9186 (uint16_t* __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt16::Equals(System.Object)
 bool UInt16_Equals_m9187 (uint16_t* __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::GetHashCode()
 int32_t UInt16_GetHashCode_m9188 (uint16_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.UInt16::CompareTo(System.UInt16)
 int32_t UInt16_CompareTo_m9189 (uint16_t* __this, uint16_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt16::Equals(System.UInt16)
 bool UInt16_Equals_m9190 (uint16_t* __this, uint16_t ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.UInt16::Parse(System.String,System.IFormatProvider)
 uint16_t UInt16_Parse_m9191 (Object_t * __this/* static, unused */, String_t* ___s, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System.UInt16::Parse(System.String,System.Globalization.NumberStyles,System.IFormatProvider)
 uint16_t UInt16_Parse_m9192 (Object_t * __this/* static, unused */, String_t* ___s, int32_t ___style, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt16::TryParse(System.String,System.UInt16&)
 bool UInt16_TryParse_m9193 (Object_t * __this/* static, unused */, String_t* ___s, uint16_t* ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.UInt16::TryParse(System.String,System.Globalization.NumberStyles,System.IFormatProvider,System.UInt16&)
 bool UInt16_TryParse_m9194 (Object_t * __this/* static, unused */, String_t* ___s, int32_t ___style, Object_t * ___provider, uint16_t* ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UInt16::ToString()
 String_t* UInt16_ToString_m9195 (uint16_t* __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UInt16::ToString(System.IFormatProvider)
 String_t* UInt16_ToString_m9196 (uint16_t* __this, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UInt16::ToString(System.String)
 String_t* UInt16_ToString_m9197 (uint16_t* __this, String_t* ___format, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.UInt16::ToString(System.String,System.IFormatProvider)
 String_t* UInt16_ToString_m9198 (uint16_t* __this, String_t* ___format, Object_t * ___provider, MethodInfo* method) IL2CPP_METHOD_ATTR;
