﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct ShimEnumerator_t4185;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Dictionary_2_t866;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
 void ShimEnumerator__ctor_m25198 (ShimEnumerator_t4185 * __this, Dictionary_2_t866 * ___host, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::MoveNext()
 bool ShimEnumerator_MoveNext_m25199 (ShimEnumerator_t4185 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Entry()
 DictionaryEntry_t1302  ShimEnumerator_get_Entry_m25200 (ShimEnumerator_t4185 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Key()
 Object_t * ShimEnumerator_get_Key_m25201 (ShimEnumerator_t4185 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Value()
 Object_t * ShimEnumerator_get_Value_m25202 (ShimEnumerator_t4185 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Current()
 Object_t * ShimEnumerator_get_Current_m25203 (ShimEnumerator_t4185 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
