﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>
struct InternalEnumerator_1_t3211;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.RaycastHit
#include "UnityEngine_UnityEngine_RaycastHit.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m17081 (InternalEnumerator_1_t3211 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17082 (InternalEnumerator_1_t3211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::Dispose()
 void InternalEnumerator_1_Dispose_m17083 (InternalEnumerator_1_t3211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m17084 (InternalEnumerator_1_t3211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::get_Current()
 RaycastHit_t16  InternalEnumerator_1_get_Current_m17085 (InternalEnumerator_1_t3211 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
