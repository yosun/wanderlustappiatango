﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.Canvas>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_153.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Canvas>
struct CachedInvokableCall_1_t4714  : public InvokableCall_1_t4715
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.Canvas>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
