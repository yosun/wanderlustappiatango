﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.WebCamBehaviour>
struct UnityAction_1_t2980;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>
struct InvokableCall_1_t2979  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.WebCamBehaviour>::Delegate
	UnityAction_1_t2980 * ___Delegate_0;
};
