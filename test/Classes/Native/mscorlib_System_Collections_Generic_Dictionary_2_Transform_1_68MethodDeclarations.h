﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Prop,Vuforia.Prop>
struct Transform_1_t4117;
// System.Object
struct Object_t;
// Vuforia.Prop
struct Prop_t42;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Prop,Vuforia.Prop>::.ctor(System.Object,System.IntPtr)
 void Transform_1__ctor_m24534 (Transform_1_t4117 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Prop,Vuforia.Prop>::Invoke(TKey,TValue)
 Object_t * Transform_1_Invoke_m24535 (Transform_1_t4117 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Prop,Vuforia.Prop>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
 Object_t * Transform_1_BeginInvoke_m24536 (Transform_1_t4117 * __this, int32_t ___key, Object_t * ___value, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.Prop,Vuforia.Prop>::EndInvoke(System.IAsyncResult)
 Object_t * Transform_1_EndInvoke_m24537 (Transform_1_t4117 * __this, Object_t * ___result, MethodInfo* method) IL2CPP_METHOD_ATTR;
