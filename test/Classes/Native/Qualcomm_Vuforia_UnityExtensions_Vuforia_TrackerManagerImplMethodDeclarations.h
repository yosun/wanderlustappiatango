﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.TrackerManagerImpl
struct TrackerManagerImpl_t744;
// Vuforia.StateManager
struct StateManager_t724;

// Vuforia.StateManager Vuforia.TrackerManagerImpl::GetStateManager()
 StateManager_t724 * TrackerManagerImpl_GetStateManager_m4043 (TrackerManagerImpl_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.TrackerManagerImpl::.ctor()
 void TrackerManagerImpl__ctor_m4044 (TrackerManagerImpl_t744 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
