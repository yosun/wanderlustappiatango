﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>
struct KeyValuePair_2_t834;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t60;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m23394 (KeyValuePair_2_t834 * __this, int32_t ___key, WordAbstractBehaviour_t60 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::get_Key()
 int32_t KeyValuePair_2_get_Key_m5028 (KeyValuePair_2_t834 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m23395 (KeyValuePair_2_t834 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::get_Value()
 WordAbstractBehaviour_t60 * KeyValuePair_2_get_Value_m5031 (KeyValuePair_2_t834 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m23396 (KeyValuePair_2_t834 * __this, WordAbstractBehaviour_t60 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>::ToString()
 String_t* KeyValuePair_2_ToString_m23397 (KeyValuePair_2_t834 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
