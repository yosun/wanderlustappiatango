﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CacheIndex
struct CacheIndex_t987;
struct CacheIndex_t987_marshaled;

void CacheIndex_t987_marshal(const CacheIndex_t987& unmarshaled, CacheIndex_t987_marshaled& marshaled);
void CacheIndex_t987_marshal_back(const CacheIndex_t987_marshaled& marshaled, CacheIndex_t987& unmarshaled);
void CacheIndex_t987_marshal_cleanup(CacheIndex_t987_marshaled& marshaled);
