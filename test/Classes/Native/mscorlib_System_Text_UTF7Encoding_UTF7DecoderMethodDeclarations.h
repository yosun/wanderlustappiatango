﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.UTF7Encoding/UTF7Decoder
struct UTF7Decoder_t2159;
// System.Byte[]
struct ByteU5BU5D_t609;
// System.Char[]
struct CharU5BU5D_t108;

// System.Void System.Text.UTF7Encoding/UTF7Decoder::.ctor()
 void UTF7Decoder__ctor_m12374 (UTF7Decoder_t2159 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.UTF7Encoding/UTF7Decoder::GetChars(System.Byte[],System.Int32,System.Int32,System.Char[],System.Int32)
 int32_t UTF7Decoder_GetChars_m12375 (UTF7Decoder_t2159 * __this, ByteU5BU5D_t609* ___bytes, int32_t ___byteIndex, int32_t ___byteCount, CharU5BU5D_t108* ___chars, int32_t ___charIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
