﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>
struct GenericEqualityComparer_1_t2658;
// System.DateTime
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::.ctor()
 void GenericEqualityComparer_1__ctor_m13946 (GenericEqualityComparer_1_t2658 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::GetHashCode(T)
 int32_t GenericEqualityComparer_1_GetHashCode_m31310 (GenericEqualityComparer_1_t2658 * __this, DateTime_t110  ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::Equals(T,T)
 bool GenericEqualityComparer_1_Equals_m31311 (GenericEqualityComparer_1_t2658 * __this, DateTime_t110  ___x, DateTime_t110  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
