﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.RuntimeAnimatorController>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_150.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RuntimeAnimatorController>
struct CachedInvokableCall_1_t4677  : public InvokableCall_1_t4678
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.RuntimeAnimatorController>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
