﻿#pragma once
#include <stdint.h>
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t60;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<Vuforia.WordAbstractBehaviour>
struct Comparison_1_t4011  : public MulticastDelegate_t325
{
};
