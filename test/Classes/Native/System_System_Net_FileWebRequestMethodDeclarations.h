﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.FileWebRequest
struct FileWebRequest_t1325;
// System.Uri
struct Uri_t1322;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1066;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Net.FileWebRequest::.ctor(System.Uri)
 void FileWebRequest__ctor_m6808 (FileWebRequest_t1325 * __this, Uri_t1322 * ___uri, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void FileWebRequest__ctor_m6809 (FileWebRequest_t1325 * __this, SerializationInfo_t1066 * ___serializationInfo, StreamingContext_t1067  ___streamingContext, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m6810 (FileWebRequest_t1325 * __this, SerializationInfo_t1066 * ___serializationInfo, StreamingContext_t1067  ___streamingContext, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.FileWebRequest::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
 void FileWebRequest_GetObjectData_m6811 (FileWebRequest_t1325 * __this, SerializationInfo_t1066 * ___serializationInfo, StreamingContext_t1067  ___streamingContext, MethodInfo* method) IL2CPP_METHOD_ATTR;
