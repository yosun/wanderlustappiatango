﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t2;
// UnityEngine.Camera
struct Camera_t3;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// WorldNav
struct WorldNav_t25  : public MonoBehaviour_t6
{
	// UnityEngine.GameObject WorldNav::mainCharacter
	GameObject_t2 * ___mainCharacter_3;
};
struct WorldNav_t25_StaticFields{
	// System.Single WorldNav::coeff
	float ___coeff_2;
	// UnityEngine.GameObject WorldNav::goMainCharacter
	GameObject_t2 * ___goMainCharacter_4;
	// UnityEngine.Camera WorldNav::camCharacter
	Camera_t3 * ___camCharacter_5;
};
