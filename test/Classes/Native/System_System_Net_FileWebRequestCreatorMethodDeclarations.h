﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Net.FileWebRequestCreator
struct FileWebRequestCreator_t1326;
// System.Net.WebRequest
struct WebRequest_t1321;
// System.Uri
struct Uri_t1322;

// System.Void System.Net.FileWebRequestCreator::.ctor()
 void FileWebRequestCreator__ctor_m6812 (FileWebRequestCreator_t1326 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Net.WebRequest System.Net.FileWebRequestCreator::Create(System.Uri)
 WebRequest_t1321 * FileWebRequestCreator_Create_m6813 (FileWebRequestCreator_t1326 * __this, Uri_t1322 * ___uri, MethodInfo* method) IL2CPP_METHOD_ATTR;
