﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WebCamTexAdaptorImpl
struct WebCamTexAdaptorImpl_t684;
// UnityEngine.Texture
struct Texture_t294;
// System.String
struct String_t;
// Vuforia.QCARRenderer/Vec2I
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARRenderer_Vec2I.h"

// System.Boolean Vuforia.WebCamTexAdaptorImpl::get_DidUpdateThisFrame()
 bool WebCamTexAdaptorImpl_get_DidUpdateThisFrame_m3081 (WebCamTexAdaptorImpl_t684 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WebCamTexAdaptorImpl::get_IsPlaying()
 bool WebCamTexAdaptorImpl_get_IsPlaying_m3082 (WebCamTexAdaptorImpl_t684 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture Vuforia.WebCamTexAdaptorImpl::get_Texture()
 Texture_t294 * WebCamTexAdaptorImpl_get_Texture_m3083 (WebCamTexAdaptorImpl_t684 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamTexAdaptorImpl::.ctor(System.String,System.Int32,Vuforia.QCARRenderer/Vec2I)
 void WebCamTexAdaptorImpl__ctor_m3084 (WebCamTexAdaptorImpl_t684 * __this, String_t* ___deviceName, int32_t ___requestedFPS, Vec2I_t630  ___requestedTextureSize, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamTexAdaptorImpl::Play()
 void WebCamTexAdaptorImpl_Play_m3085 (WebCamTexAdaptorImpl_t684 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WebCamTexAdaptorImpl::Stop()
 void WebCamTexAdaptorImpl_Stop_m3086 (WebCamTexAdaptorImpl_t684 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
