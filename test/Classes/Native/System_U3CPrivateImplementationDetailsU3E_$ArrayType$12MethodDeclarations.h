﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$12
struct $ArrayType$12_t1472;
struct $ArrayType$12_t1472_marshaled;

void $ArrayType$12_t1472_marshal(const $ArrayType$12_t1472& unmarshaled, $ArrayType$12_t1472_marshaled& marshaled);
void $ArrayType$12_t1472_marshal_back(const $ArrayType$12_t1472_marshaled& marshaled, $ArrayType$12_t1472& unmarshaled);
void $ArrayType$12_t1472_marshal_cleanup($ArrayType$12_t1472_marshaled& marshaled);
