﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t5136;
// System.DateTimeOffset
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
 void DefaultComparer__ctor_m31335 (DefaultComparer_t5136 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::Compare(T,T)
 int32_t DefaultComparer_Compare_m31336 (DefaultComparer_t5136 * __this, DateTimeOffset_t2198  ___x, DateTimeOffset_t2198  ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
