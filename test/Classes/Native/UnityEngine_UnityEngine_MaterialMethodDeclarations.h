﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Material
struct Material_t4;
// UnityEngine.Shader
struct Shader_t173;
// UnityEngine.Texture
struct Texture_t294;
// System.String
struct String_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void UnityEngine.Material::.ctor(System.String)
 void Material__ctor_m637 (Material_t4 * __this, String_t* ___contents, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
 void Material__ctor_m4451 (Material_t4 * __this, Shader_t173 * ___shader, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
 void Material__ctor_m2440 (Material_t4 * __this, Material_t4 * ___source, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader UnityEngine.Material::get_shader()
 Shader_t173 * Material_get_shader_m639 (Material_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
 void Material_set_color_m218 (Material_t4 * __this, Color_t19  ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Material::get_mainTexture()
 Texture_t294 * Material_get_mainTexture_m2448 (Material_t4 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
 void Material_set_mainTexture_m5289 (Material_t4 * __this, Texture_t294 * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
 void Material_SetColor_m5912 (Material_t4 * __this, String_t* ___propertyName, Color_t19  ___color, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
 void Material_SetColor_m5913 (Material_t4 * __this, int32_t ___nameID, Color_t19  ___color, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::INTERNAL_CALL_SetColor(UnityEngine.Material,System.Int32,UnityEngine.Color&)
 void Material_INTERNAL_CALL_SetColor_m5914 (Object_t * __this/* static, unused */, Material_t4 * ___self, int32_t ___nameID, Color_t19 * ___color, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
 void Material_SetTexture_m5915 (Material_t4 * __this, String_t* ___propertyName, Texture_t294 * ___texture, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
 void Material_SetTexture_m5916 (Material_t4 * __this, int32_t ___nameID, Texture_t294 * ___texture, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
 Texture_t294 * Material_GetTexture_m5917 (Material_t4 * __this, String_t* ___propertyName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
 Texture_t294 * Material_GetTexture_m5918 (Material_t4 * __this, int32_t ___nameID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetTextureOffset(System.String,UnityEngine.Vector2)
 void Material_SetTextureOffset_m240 (Material_t4 * __this, String_t* ___propertyName, Vector2_t9  ___offset, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::INTERNAL_CALL_SetTextureOffset(UnityEngine.Material,System.String,UnityEngine.Vector2&)
 void Material_INTERNAL_CALL_SetTextureOffset_m5919 (Object_t * __this/* static, unused */, Material_t4 * ___self, String_t* ___propertyName, Vector2_t9 * ___offset, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
 void Material_SetFloat_m5920 (Material_t4 * __this, String_t* ___propertyName, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
 void Material_SetFloat_m5921 (Material_t4 * __this, int32_t ___nameID, float ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
 void Material_SetInt_m2442 (Material_t4 * __this, String_t* ___propertyName, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::HasProperty(System.String)
 bool Material_HasProperty_m2436 (Material_t4 * __this, String_t* ___propertyName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::HasProperty(System.Int32)
 bool Material_HasProperty_m5922 (Material_t4 * __this, int32_t ___nameID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Material::SetPass(System.Int32)
 bool Material_SetPass_m650 (Material_t4 * __this, int32_t ___pass, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_renderQueue(System.Int32)
 void Material_set_renderQueue_m309 (Material_t4 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::Internal_CreateWithString(UnityEngine.Material,System.String)
 void Material_Internal_CreateWithString_m5923 (Object_t * __this/* static, unused */, Material_t4 * ___mono, String_t* ___contents, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::Internal_CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
 void Material_Internal_CreateWithShader_m5924 (Object_t * __this/* static, unused */, Material_t4 * ___mono, Shader_t173 * ___shader, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::Internal_CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
 void Material_Internal_CreateWithMaterial_m5925 (Object_t * __this/* static, unused */, Material_t4 * ___mono, Material_t4 * ___source, MethodInfo* method) IL2CPP_METHOD_ATTR;
