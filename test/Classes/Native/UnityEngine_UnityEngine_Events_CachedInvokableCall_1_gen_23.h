﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_19.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>
struct CachedInvokableCall_1_t2826  : public InvokableCall_1_t2827
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
