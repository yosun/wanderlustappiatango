﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$4
struct $ArrayType$4_t1655;
struct $ArrayType$4_t1655_marshaled;

void $ArrayType$4_t1655_marshal(const $ArrayType$4_t1655& unmarshaled, $ArrayType$4_t1655_marshaled& marshaled);
void $ArrayType$4_t1655_marshal_back(const $ArrayType$4_t1655_marshaled& marshaled, $ArrayType$4_t1655& unmarshaled);
void $ArrayType$4_t1655_marshal_cleanup($ArrayType$4_t1655_marshaled& marshaled);
