﻿#pragma once
#include <stdint.h>
// UnityEngine.Texture2D
struct Texture2D_t285;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Vuforia.VideoTextureRendererAbstractBehaviour
struct VideoTextureRendererAbstractBehaviour_t86  : public MonoBehaviour_t6
{
	// UnityEngine.Texture2D Vuforia.VideoTextureRendererAbstractBehaviour::mTexture
	Texture2D_t285 * ___mTexture_2;
	// System.Boolean Vuforia.VideoTextureRendererAbstractBehaviour::mVideoBgConfigChanged
	bool ___mVideoBgConfigChanged_3;
	// System.Boolean Vuforia.VideoTextureRendererAbstractBehaviour::mTextureAppliedToMaterial
	bool ___mTextureAppliedToMaterial_4;
	// System.Int32 Vuforia.VideoTextureRendererAbstractBehaviour::mNativeTextureID
	int32_t ___mNativeTextureID_5;
};
