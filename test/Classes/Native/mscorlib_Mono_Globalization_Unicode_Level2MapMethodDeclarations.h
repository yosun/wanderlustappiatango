﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Globalization.Unicode.Level2Map
struct Level2Map_t1734;

// System.Void Mono.Globalization.Unicode.Level2Map::.ctor(System.Byte,System.Byte)
 void Level2Map__ctor_m9768 (Level2Map_t1734 * __this, uint8_t ___source, uint8_t ___replace, MethodInfo* method) IL2CPP_METHOD_ATTR;
