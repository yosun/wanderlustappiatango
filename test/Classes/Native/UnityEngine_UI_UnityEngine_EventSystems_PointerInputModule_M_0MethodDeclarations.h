﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData
struct MouseButtonEventData_t247;

// System.Void UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::.ctor()
 void MouseButtonEventData__ctor_m871 (MouseButtonEventData_t247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::PressedThisFrame()
 bool MouseButtonEventData_PressedThisFrame_m872 (MouseButtonEventData_t247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.PointerInputModule/MouseButtonEventData::ReleasedThisFrame()
 bool MouseButtonEventData_ReleasedThisFrame_m873 (MouseButtonEventData_t247 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
