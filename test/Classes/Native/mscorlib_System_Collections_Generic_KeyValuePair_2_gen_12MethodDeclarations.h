﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>
struct KeyValuePair_2_t3874;
// Vuforia.Marker
struct Marker_t623;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m22097 (KeyValuePair_2_t3874 * __this, int32_t ___key, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::get_Key()
 int32_t KeyValuePair_2_get_Key_m22098 (KeyValuePair_2_t3874 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m22099 (KeyValuePair_2_t3874 * __this, int32_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::get_Value()
 Object_t * KeyValuePair_2_get_Value_m22100 (KeyValuePair_2_t3874 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m22101 (KeyValuePair_2_t3874 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>::ToString()
 String_t* KeyValuePair_2_ToString_m22102 (KeyValuePair_2_t3874 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
