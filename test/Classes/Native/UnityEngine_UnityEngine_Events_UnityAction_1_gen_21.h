﻿#pragma once
#include <stdint.h>
// System.Void
struct Void_t99;
// Vuforia.DataSetLoadBehaviour
struct DataSetLoadBehaviour_t35;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>
struct UnityAction_1_t2800  : public MulticastDelegate_t325
{
};
