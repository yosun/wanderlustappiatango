﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.EyewearComponentFactory
struct EyewearComponentFactory_t542;
// Vuforia.IEyewearComponentFactory
struct IEyewearComponentFactory_t541;

// Vuforia.IEyewearComponentFactory Vuforia.EyewearComponentFactory::get_Instance()
 Object_t * EyewearComponentFactory_get_Instance_m2592 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.EyewearComponentFactory::set_Instance(Vuforia.IEyewearComponentFactory)
 void EyewearComponentFactory_set_Instance_m2593 (Object_t * __this/* static, unused */, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.EyewearComponentFactory::.ctor()
 void EyewearComponentFactory__ctor_m2594 (EyewearComponentFactory_t542 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
