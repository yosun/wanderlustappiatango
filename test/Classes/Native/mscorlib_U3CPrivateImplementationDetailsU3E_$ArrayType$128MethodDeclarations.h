﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$128
struct $ArrayType$128_t2279;
struct $ArrayType$128_t2279_marshaled;

void $ArrayType$128_t2279_marshal(const $ArrayType$128_t2279& unmarshaled, $ArrayType$128_t2279_marshaled& marshaled);
void $ArrayType$128_t2279_marshal_back(const $ArrayType$128_t2279_marshaled& marshaled, $ArrayType$128_t2279& unmarshaled);
void $ArrayType$128_t2279_marshal_cleanup($ArrayType$128_t2279_marshaled& marshaled);
