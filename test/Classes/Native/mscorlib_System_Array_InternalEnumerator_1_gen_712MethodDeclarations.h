﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnDeserializedAttribute>
struct InternalEnumerator_1_t5097;
// System.Object
struct Object_t;
// System.Runtime.Serialization.OnDeserializedAttribute
struct OnDeserializedAttribute_t2079;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnDeserializedAttribute>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m31065(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnDeserializedAttribute>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31066(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnDeserializedAttribute>::Dispose()
#define InternalEnumerator_1_Dispose_m31067(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnDeserializedAttribute>::MoveNext()
#define InternalEnumerator_1_MoveNext_m31068(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.OnDeserializedAttribute>::get_Current()
#define InternalEnumerator_1_get_Current_m31069(__this, method) (OnDeserializedAttribute_t2079 *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
