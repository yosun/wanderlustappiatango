﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.SecuritySafeCriticalAttribute
struct SecuritySafeCriticalAttribute_t1143;

// System.Void System.Security.SecuritySafeCriticalAttribute::.ctor()
 void SecuritySafeCriticalAttribute__ctor_m6476 (SecuritySafeCriticalAttribute_t1143 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
