﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t10;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.CastHelper`1<UnityEngine.Transform>
struct CastHelper_1_t2725 
{
	// T UnityEngine.CastHelper`1<UnityEngine.Transform>::t
	Transform_t10 * ___t_0;
	// System.IntPtr UnityEngine.CastHelper`1<UnityEngine.Transform>::onePointerFurtherThanT
	IntPtr_t121 ___onePointerFurtherThanT_1;
};
