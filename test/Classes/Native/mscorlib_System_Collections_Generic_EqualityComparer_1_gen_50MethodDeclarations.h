﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/VirtualButtonData>
struct EqualityComparer_1_t4205;
// System.Object
struct Object_t;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor()
 void EqualityComparer_1__ctor_m25342 (EqualityComparer_1_t4205 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/VirtualButtonData>::.cctor()
 void EqualityComparer_1__cctor_m25343 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
 int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25344 (EqualityComparer_1_t4205 * __this, Object_t * ___obj, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
 bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25345 (EqualityComparer_1_t4205 * __this, Object_t * ___x, Object_t * ___y, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/VirtualButtonData>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/VirtualButtonData>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<Vuforia.QCARManagerImpl/VirtualButtonData>::get_Default()
 EqualityComparer_1_t4205 * EqualityComparer_1_get_Default_m25346 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
