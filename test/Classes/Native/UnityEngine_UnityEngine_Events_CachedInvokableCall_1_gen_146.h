﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t115;
// UnityEngine.Events.InvokableCall`1<UnityEngine.WebCamTexture>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_148.h"
// UnityEngine.Events.CachedInvokableCall`1<UnityEngine.WebCamTexture>
struct CachedInvokableCall_1_t4667  : public InvokableCall_1_t4668
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<UnityEngine.WebCamTexture>::m_Arg1
	ObjectU5BU5D_t115* ___m_Arg1_1;
};
