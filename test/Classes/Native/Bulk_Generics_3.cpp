﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#include "stringLiterals.h"
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_17.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
extern TypeInfo CachedInvokableCall_1_t2793_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_17MethodDeclarations.h"

// System.Void
#include "mscorlib_System_Void.h"
// UnityEngine.Object
#include "UnityEngine_UnityEngine_Object.h"
// System.Reflection.MethodInfo
#include "mscorlib_System_Reflection_MethodInfo.h"
// Vuforia.CylinderTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour.h"
#include "mscorlib_ArrayTypes.h"
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_13.h"
extern TypeInfo ObjectU5BU5D_t115_il2cpp_TypeInfo;
extern TypeInfo Object_t_il2cpp_TypeInfo;
extern TypeInfo CylinderTargetBehaviour_t33_il2cpp_TypeInfo;
extern TypeInfo InvokableCall_1_t2794_il2cpp_TypeInfo;
extern TypeInfo Void_t99_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_13MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14364_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14366_MethodInfo;

// System.Array
#include "mscorlib_System_Array.h"

// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2793____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2793_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2793, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2793_FieldInfos[] =
{
	&CachedInvokableCall_1_t2793____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2793_CachedInvokableCall_1__ctor_m14362_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t33_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14362_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14362_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2793_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2793_CachedInvokableCall_1__ctor_m14362_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14362_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2793_CachedInvokableCall_1_Invoke_m14363_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14363_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.CylinderTargetBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14363_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2793_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2793_CachedInvokableCall_1_Invoke_m14363_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14363_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2793_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14362_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14363_MethodInfo,
	NULL
};
extern MethodInfo Object_Equals_m304_MethodInfo;
extern MethodInfo Object_Finalize_m192_MethodInfo;
extern MethodInfo Object_GetHashCode_m305_MethodInfo;
extern MethodInfo Object_ToString_m306_MethodInfo;
extern MethodInfo CachedInvokableCall_1_Invoke_m14363_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14367_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2793_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14363_MethodInfo,
	&InvokableCall_1_Find_m14367_MethodInfo,
};
extern Il2CppType UnityAction_1_t2795_0_0_0;
extern TypeInfo UnityAction_1_t2795_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisCylinderTargetBehaviour_t33_m31990_MethodInfo;
extern TypeInfo CylinderTargetBehaviour_t33_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14369_MethodInfo;
extern TypeInfo CylinderTargetBehaviour_t33_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2793_RGCTXData[8] = 
{
	&UnityAction_1_t2795_0_0_0/* Type Usage */,
	&UnityAction_1_t2795_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCylinderTargetBehaviour_t33_m31990_MethodInfo/* Method Usage */,
	&CylinderTargetBehaviour_t33_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14369_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14364_MethodInfo/* Method Usage */,
	&CylinderTargetBehaviour_t33_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14366_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2793_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2793_1_0_0;
struct CachedInvokableCall_1_t2793;
extern Il2CppGenericClass CachedInvokableCall_1_t2793_GenericClass;
TypeInfo CachedInvokableCall_1_t2793_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2793_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2793_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2793_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2793_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2793_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2793_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2793_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2793_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2793_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2793)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_20.h"
// System.Type
#include "mscorlib_System_Type.h"
// System.RuntimeTypeHandle
#include "mscorlib_System_RuntimeTypeHandle.h"
// System.Delegate
#include "mscorlib_System_Delegate.h"
// System.String
#include "mscorlib_System_String.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentException.h"
// System.Boolean
#include "mscorlib_System_Boolean.h"
extern TypeInfo UnityAction_1_t2795_il2cpp_TypeInfo;
extern TypeInfo Type_t_il2cpp_TypeInfo;
extern TypeInfo ArgumentException_t507_il2cpp_TypeInfo;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCallMethodDeclarations.h"
// System.Type
#include "mscorlib_System_TypeMethodDeclarations.h"
// System.Delegate
#include "mscorlib_System_DelegateMethodDeclarations.h"
// System.ArgumentException
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
// UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_20MethodDeclarations.h"
extern MethodInfo BaseInvokableCall__ctor_m6358_MethodInfo;
extern MethodInfo Type_GetTypeFromHandle_m395_MethodInfo;
extern MethodInfo Delegate_CreateDelegate_m456_MethodInfo;
extern MethodInfo Delegate_Combine_m2149_MethodInfo;
extern MethodInfo BaseInvokableCall__ctor_m6357_MethodInfo;
extern MethodInfo ArgumentException__ctor_m2468_MethodInfo;
extern MethodInfo BaseInvokableCall_AllowInvoke_m6359_MethodInfo;
extern MethodInfo Delegate_get_Target_m6536_MethodInfo;
extern MethodInfo Delegate_get_Method_m6534_MethodInfo;
struct BaseInvokableCall_t1075;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<System.Object>(System.Object)
 void BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared (Object_t * __this/* static, unused */, Object_t * p0, MethodInfo* method);
#define BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.CylinderTargetBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.CylinderTargetBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisCylinderTargetBehaviour_t33_m31990(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>
extern Il2CppType UnityAction_1_t2795_0_0_1;
FieldInfo InvokableCall_1_t2794____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2795_0_0_1/* type */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2794, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2794_FieldInfos[] =
{
	&InvokableCall_1_t2794____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2794_InvokableCall_1__ctor_m14364_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14364_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14364_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2794_InvokableCall_1__ctor_m14364_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14364_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2795_0_0_0;
static ParameterInfo InvokableCall_1_t2794_InvokableCall_1__ctor_m14365_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2795_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14365_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14365_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2794_InvokableCall_1__ctor_m14365_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14365_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2794_InvokableCall_1_Invoke_m14366_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14366_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14366_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2794_InvokableCall_1_Invoke_m14366_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14366_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2794_InvokableCall_1_Find_m14367_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14367_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.CylinderTargetBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14367_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2794_InvokableCall_1_Find_m14367_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14367_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2794_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14364_MethodInfo,
	&InvokableCall_1__ctor_m14365_MethodInfo,
	&InvokableCall_1_Invoke_m14366_MethodInfo,
	&InvokableCall_1_Find_m14367_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2794_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14366_MethodInfo,
	&InvokableCall_1_Find_m14367_MethodInfo,
};
extern TypeInfo UnityAction_1_t2795_il2cpp_TypeInfo;
extern TypeInfo CylinderTargetBehaviour_t33_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2794_RGCTXData[5] = 
{
	&UnityAction_1_t2795_0_0_0/* Type Usage */,
	&UnityAction_1_t2795_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisCylinderTargetBehaviour_t33_m31990_MethodInfo/* Method Usage */,
	&CylinderTargetBehaviour_t33_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14369_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2794_0_0_0;
extern Il2CppType InvokableCall_1_t2794_1_0_0;
extern TypeInfo BaseInvokableCall_t1075_il2cpp_TypeInfo;
struct InvokableCall_1_t2794;
extern Il2CppGenericClass InvokableCall_1_t2794_GenericClass;
TypeInfo InvokableCall_1_t2794_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2794_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2794_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2794_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2794_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2794_0_0_0/* byval_arg */
	, &InvokableCall_1_t2794_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2794_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2794_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2794)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// System.AsyncCallback
#include "mscorlib_System_AsyncCallback.h"


// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2795_UnityAction_1__ctor_m14368_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14368_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14368_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2795_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2795_UnityAction_1__ctor_m14368_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14368_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
static ParameterInfo UnityAction_1_t2795_UnityAction_1_Invoke_m14369_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t33_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14369_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14369_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2795_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2795_UnityAction_1_Invoke_m14369_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14369_GenericMethod/* genericMethod */

};
extern Il2CppType CylinderTargetBehaviour_t33_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2795_UnityAction_1_BeginInvoke_m14370_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &CylinderTargetBehaviour_t33_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14370_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14370_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2795_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2795_UnityAction_1_BeginInvoke_m14370_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14370_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2795_UnityAction_1_EndInvoke_m14371_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14371_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.CylinderTargetBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14371_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2795_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2795_UnityAction_1_EndInvoke_m14371_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14371_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2795_MethodInfos[] =
{
	&UnityAction_1__ctor_m14368_MethodInfo,
	&UnityAction_1_Invoke_m14369_MethodInfo,
	&UnityAction_1_BeginInvoke_m14370_MethodInfo,
	&UnityAction_1_EndInvoke_m14371_MethodInfo,
	NULL
};
extern MethodInfo MulticastDelegate_Equals_m2241_MethodInfo;
extern MethodInfo MulticastDelegate_GetHashCode_m2242_MethodInfo;
extern MethodInfo MulticastDelegate_GetObjectData_m2243_MethodInfo;
extern MethodInfo Delegate_Clone_m2244_MethodInfo;
extern MethodInfo MulticastDelegate_GetInvocationList_m2245_MethodInfo;
extern MethodInfo MulticastDelegate_CombineImpl_m2246_MethodInfo;
extern MethodInfo MulticastDelegate_RemoveImpl_m2247_MethodInfo;
extern MethodInfo UnityAction_1_BeginInvoke_m14370_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14371_MethodInfo;
static MethodInfo* UnityAction_1_t2795_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14369_MethodInfo,
	&UnityAction_1_BeginInvoke_m14370_MethodInfo,
	&UnityAction_1_EndInvoke_m14371_MethodInfo,
};
extern TypeInfo ICloneable_t481_il2cpp_TypeInfo;
extern TypeInfo ISerializable_t482_il2cpp_TypeInfo;
static Il2CppInterfaceOffsetPair UnityAction_1_t2795_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2795_1_0_0;
extern TypeInfo MulticastDelegate_t325_il2cpp_TypeInfo;
struct UnityAction_1_t2795;
extern Il2CppGenericClass UnityAction_1_t2795_GenericClass;
TypeInfo UnityAction_1_t2795_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2795_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2795_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2795_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2795_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2795_0_0_0/* byval_arg */
	, &UnityAction_1_t2795_1_0_0/* this_arg */
	, UnityAction_1_t2795_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2795_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2795)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5782_il2cpp_TypeInfo;

// Vuforia.DataSetLoadBehaviour
#include "AssemblyU2DCSharp_Vuforia_DataSetLoadBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.DataSetLoadBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.DataSetLoadBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41759_MethodInfo;
static PropertyInfo IEnumerator_1_t5782____Current_PropertyInfo = 
{
	&IEnumerator_1_t5782_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41759_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5782_PropertyInfos[] =
{
	&IEnumerator_1_t5782____Current_PropertyInfo,
	NULL
};
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41759_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.DataSetLoadBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41759_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5782_il2cpp_TypeInfo/* declaring_type */
	, &DataSetLoadBehaviour_t35_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41759_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5782_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41759_MethodInfo,
	NULL
};
extern TypeInfo IEnumerator_t266_il2cpp_TypeInfo;
extern TypeInfo IDisposable_t139_il2cpp_TypeInfo;
static TypeInfo* IEnumerator_1_t5782_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5782_0_0_0;
extern Il2CppType IEnumerator_1_t5782_1_0_0;
struct IEnumerator_1_t5782;
extern Il2CppGenericClass IEnumerator_1_t5782_GenericClass;
TypeInfo IEnumerator_1_t5782_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5782_MethodInfos/* methods */
	, IEnumerator_1_t5782_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5782_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5782_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5782_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5782_0_0_0/* byval_arg */
	, &IEnumerator_1_t5782_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5782_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_48.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2796_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_48MethodDeclarations.h"

// System.Int32
#include "mscorlib_System_Int32.h"
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationException.h"
extern TypeInfo DataSetLoadBehaviour_t35_il2cpp_TypeInfo;
extern TypeInfo InvalidOperationException_t1493_il2cpp_TypeInfo;
// System.InvalidOperationException
#include "mscorlib_System_InvalidOperationExceptionMethodDeclarations.h"
// System.Array
#include "mscorlib_System_ArrayMethodDeclarations.h"
extern MethodInfo InternalEnumerator_1_get_Current_m14376_MethodInfo;
extern MethodInfo InvalidOperationException__ctor_m7651_MethodInfo;
extern MethodInfo Array_get_Length_m7656_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDataSetLoadBehaviour_t35_m31992_MethodInfo;
struct Array_t;
// System.ArgumentOutOfRangeException
#include "mscorlib_System_ArgumentOutOfRangeException.h"
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
 Object_t * Array_InternalArray__get_Item_TisObject_t_m31436_gshared (Array_t * __this, int32_t p0, MethodInfo* method);
#define Array_InternalArray__get_Item_TisObject_t_m31436(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.DataSetLoadBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.DataSetLoadBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisDataSetLoadBehaviour_t35_m31992(__this, p0, method) (DataSetLoadBehaviour_t35 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2796____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2796_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2796, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2796____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2796_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2796, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2796_FieldInfos[] =
{
	&InternalEnumerator_1_t2796____array_0_FieldInfo,
	&InternalEnumerator_1_t2796____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14373_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2796____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2796_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14373_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2796____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2796_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14376_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2796_PropertyInfos[] =
{
	&InternalEnumerator_1_t2796____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2796____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2796_InternalEnumerator_1__ctor_m14372_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14372_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14372_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2796_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2796_InternalEnumerator_1__ctor_m14372_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14372_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14373_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14373_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2796_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14373_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14374_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14374_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2796_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14374_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14375_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14375_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2796_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14375_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14376_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.DataSetLoadBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14376_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2796_il2cpp_TypeInfo/* declaring_type */
	, &DataSetLoadBehaviour_t35_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14376_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2796_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14372_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14373_MethodInfo,
	&InternalEnumerator_1_Dispose_m14374_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14375_MethodInfo,
	&InternalEnumerator_1_get_Current_m14376_MethodInfo,
	NULL
};
extern MethodInfo ValueType_Equals_m1961_MethodInfo;
extern MethodInfo ValueType_GetHashCode_m1962_MethodInfo;
extern MethodInfo ValueType_ToString_m2052_MethodInfo;
extern MethodInfo InternalEnumerator_1_MoveNext_m14375_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14374_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2796_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14373_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14375_MethodInfo,
	&InternalEnumerator_1_Dispose_m14374_MethodInfo,
	&InternalEnumerator_1_get_Current_m14376_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2796_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5782_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2796_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5782_il2cpp_TypeInfo, 7},
};
extern TypeInfo DataSetLoadBehaviour_t35_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2796_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14376_MethodInfo/* Method Usage */,
	&DataSetLoadBehaviour_t35_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDataSetLoadBehaviour_t35_m31992_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2796_0_0_0;
extern Il2CppType InternalEnumerator_1_t2796_1_0_0;
extern TypeInfo ValueType_t436_il2cpp_TypeInfo;
extern Il2CppGenericClass InternalEnumerator_1_t2796_GenericClass;
extern TypeInfo Array_t_il2cpp_TypeInfo;
TypeInfo InternalEnumerator_1_t2796_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2796_MethodInfos/* methods */
	, InternalEnumerator_1_t2796_PropertyInfos/* properties */
	, InternalEnumerator_1_t2796_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2796_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2796_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2796_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2796_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2796_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2796_1_0_0/* this_arg */
	, InternalEnumerator_1_t2796_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2796_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2796_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2796)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7344_il2cpp_TypeInfo;

#include "Assembly-CSharp_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>
extern MethodInfo ICollection_1_get_Count_m41760_MethodInfo;
static PropertyInfo ICollection_1_t7344____Count_PropertyInfo = 
{
	&ICollection_1_t7344_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41760_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41761_MethodInfo;
static PropertyInfo ICollection_1_t7344____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7344_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41761_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7344_PropertyInfos[] =
{
	&ICollection_1_t7344____Count_PropertyInfo,
	&ICollection_1_t7344____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41760_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41760_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7344_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41760_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41761_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41761_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7344_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41761_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
static ParameterInfo ICollection_1_t7344_ICollection_1_Add_m41762_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t35_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41762_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41762_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7344_ICollection_1_Add_m41762_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41762_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41763_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41763_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41763_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
static ParameterInfo ICollection_1_t7344_ICollection_1_Contains_m41764_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t35_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41764_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41764_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7344_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7344_ICollection_1_Contains_m41764_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41764_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviourU5BU5D_t5172_0_0_0;
extern Il2CppType DataSetLoadBehaviourU5BU5D_t5172_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7344_ICollection_1_CopyTo_m41765_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviourU5BU5D_t5172_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41765_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41765_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7344_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7344_ICollection_1_CopyTo_m41765_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41765_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
static ParameterInfo ICollection_1_t7344_ICollection_1_Remove_m41766_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t35_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41766_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41766_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7344_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7344_ICollection_1_Remove_m41766_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41766_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7344_MethodInfos[] =
{
	&ICollection_1_get_Count_m41760_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41761_MethodInfo,
	&ICollection_1_Add_m41762_MethodInfo,
	&ICollection_1_Clear_m41763_MethodInfo,
	&ICollection_1_Contains_m41764_MethodInfo,
	&ICollection_1_CopyTo_m41765_MethodInfo,
	&ICollection_1_Remove_m41766_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_t1126_il2cpp_TypeInfo;
extern TypeInfo IEnumerable_1_t7346_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7344_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7346_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7344_0_0_0;
extern Il2CppType ICollection_1_t7344_1_0_0;
struct ICollection_1_t7344;
extern Il2CppGenericClass ICollection_1_t7344_GenericClass;
TypeInfo ICollection_1_t7344_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7344_MethodInfos/* methods */
	, ICollection_1_t7344_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7344_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7344_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7344_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7344_0_0_0/* byval_arg */
	, &ICollection_1_t7344_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7344_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DataSetLoadBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.DataSetLoadBehaviour>
extern Il2CppType IEnumerator_1_t5782_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41767_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DataSetLoadBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41767_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7346_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5782_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41767_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7346_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41767_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7346_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7346_0_0_0;
extern Il2CppType IEnumerable_1_t7346_1_0_0;
struct IEnumerable_1_t7346;
extern Il2CppGenericClass IEnumerable_1_t7346_GenericClass;
TypeInfo IEnumerable_1_t7346_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7346_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7346_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7346_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7346_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7346_0_0_0/* byval_arg */
	, &IEnumerable_1_t7346_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7346_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7345_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>
extern MethodInfo IList_1_get_Item_m41768_MethodInfo;
extern MethodInfo IList_1_set_Item_m41769_MethodInfo;
static PropertyInfo IList_1_t7345____Item_PropertyInfo = 
{
	&IList_1_t7345_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41768_MethodInfo/* get */
	, &IList_1_set_Item_m41769_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7345_PropertyInfos[] =
{
	&IList_1_t7345____Item_PropertyInfo,
	NULL
};
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
static ParameterInfo IList_1_t7345_IList_1_IndexOf_m41770_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t35_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41770_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41770_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7345_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7345_IList_1_IndexOf_m41770_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41770_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
static ParameterInfo IList_1_t7345_IList_1_Insert_m41771_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t35_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41771_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41771_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7345_IList_1_Insert_m41771_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41771_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7345_IList_1_RemoveAt_m41772_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41772_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41772_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7345_IList_1_RemoveAt_m41772_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41772_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7345_IList_1_get_Item_m41768_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41768_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41768_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7345_il2cpp_TypeInfo/* declaring_type */
	, &DataSetLoadBehaviour_t35_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7345_IList_1_get_Item_m41768_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41768_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
static ParameterInfo IList_1_t7345_IList_1_set_Item_m41769_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t35_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41769_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41769_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7345_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7345_IList_1_set_Item_m41769_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41769_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7345_MethodInfos[] =
{
	&IList_1_IndexOf_m41770_MethodInfo,
	&IList_1_Insert_m41771_MethodInfo,
	&IList_1_RemoveAt_m41772_MethodInfo,
	&IList_1_get_Item_m41768_MethodInfo,
	&IList_1_set_Item_m41769_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7345_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7344_il2cpp_TypeInfo,
	&IEnumerable_1_t7346_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7345_0_0_0;
extern Il2CppType IList_1_t7345_1_0_0;
struct IList_1_t7345;
extern Il2CppGenericClass IList_1_t7345_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7345_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7345_MethodInfos/* methods */
	, IList_1_t7345_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7345_il2cpp_TypeInfo/* element_class */
	, IList_1_t7345_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7345_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7345_0_0_0/* byval_arg */
	, &IList_1_t7345_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7345_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7347_il2cpp_TypeInfo;

// Vuforia.DataSetLoadAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_DataSetLoadAbstract.h"
#include "Qualcomm.Vuforia.UnityExtensions_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m41773_MethodInfo;
static PropertyInfo ICollection_1_t7347____Count_PropertyInfo = 
{
	&ICollection_1_t7347_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41773_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41774_MethodInfo;
static PropertyInfo ICollection_1_t7347____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7347_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41774_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7347_PropertyInfos[] =
{
	&ICollection_1_t7347____Count_PropertyInfo,
	&ICollection_1_t7347____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41773_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41773_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7347_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41773_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41774_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41774_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7347_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41774_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadAbstractBehaviour_t36_0_0_0;
extern Il2CppType DataSetLoadAbstractBehaviour_t36_0_0_0;
static ParameterInfo ICollection_1_t7347_ICollection_1_Add_m41775_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviour_t36_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41775_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41775_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7347_ICollection_1_Add_m41775_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41775_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41776_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41776_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41776_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadAbstractBehaviour_t36_0_0_0;
static ParameterInfo ICollection_1_t7347_ICollection_1_Contains_m41777_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviour_t36_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41777_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41777_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7347_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7347_ICollection_1_Contains_m41777_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41777_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadAbstractBehaviourU5BU5D_t5498_0_0_0;
extern Il2CppType DataSetLoadAbstractBehaviourU5BU5D_t5498_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7347_ICollection_1_CopyTo_m41778_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviourU5BU5D_t5498_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41778_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41778_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7347_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7347_ICollection_1_CopyTo_m41778_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41778_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadAbstractBehaviour_t36_0_0_0;
static ParameterInfo ICollection_1_t7347_ICollection_1_Remove_m41779_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviour_t36_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41779_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DataSetLoadAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41779_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7347_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7347_ICollection_1_Remove_m41779_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41779_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7347_MethodInfos[] =
{
	&ICollection_1_get_Count_m41773_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41774_MethodInfo,
	&ICollection_1_Add_m41775_MethodInfo,
	&ICollection_1_Clear_m41776_MethodInfo,
	&ICollection_1_Contains_m41777_MethodInfo,
	&ICollection_1_CopyTo_m41778_MethodInfo,
	&ICollection_1_Remove_m41779_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7349_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7347_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7349_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7347_0_0_0;
extern Il2CppType ICollection_1_t7347_1_0_0;
struct ICollection_1_t7347;
extern Il2CppGenericClass ICollection_1_t7347_GenericClass;
TypeInfo ICollection_1_t7347_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7347_MethodInfos/* methods */
	, ICollection_1_t7347_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7347_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7347_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7347_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7347_0_0_0/* byval_arg */
	, &ICollection_1_t7347_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7347_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DataSetLoadAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.DataSetLoadAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5784_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41780_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DataSetLoadAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41780_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7349_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5784_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41780_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7349_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41780_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7349_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7349_0_0_0;
extern Il2CppType IEnumerable_1_t7349_1_0_0;
struct IEnumerable_1_t7349;
extern Il2CppGenericClass IEnumerable_1_t7349_GenericClass;
TypeInfo IEnumerable_1_t7349_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7349_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7349_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7349_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7349_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7349_0_0_0/* byval_arg */
	, &IEnumerable_1_t7349_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7349_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5784_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41781_MethodInfo;
static PropertyInfo IEnumerator_1_t5784____Current_PropertyInfo = 
{
	&IEnumerator_1_t5784_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41781_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5784_PropertyInfos[] =
{
	&IEnumerator_1_t5784____Current_PropertyInfo,
	NULL
};
extern Il2CppType DataSetLoadAbstractBehaviour_t36_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41781_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41781_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5784_il2cpp_TypeInfo/* declaring_type */
	, &DataSetLoadAbstractBehaviour_t36_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41781_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5784_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41781_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5784_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5784_0_0_0;
extern Il2CppType IEnumerator_1_t5784_1_0_0;
struct IEnumerator_1_t5784;
extern Il2CppGenericClass IEnumerator_1_t5784_GenericClass;
TypeInfo IEnumerator_1_t5784_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5784_MethodInfos/* methods */
	, IEnumerator_1_t5784_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5784_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5784_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5784_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5784_0_0_0/* byval_arg */
	, &IEnumerator_1_t5784_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5784_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_49.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2797_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_49MethodDeclarations.h"

extern TypeInfo DataSetLoadAbstractBehaviour_t36_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14381_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDataSetLoadAbstractBehaviour_t36_m32003_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.DataSetLoadAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.DataSetLoadAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisDataSetLoadAbstractBehaviour_t36_m32003(__this, p0, method) (DataSetLoadAbstractBehaviour_t36 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2797____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2797_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2797, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2797____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2797_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2797, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2797_FieldInfos[] =
{
	&InternalEnumerator_1_t2797____array_0_FieldInfo,
	&InternalEnumerator_1_t2797____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14378_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2797____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2797_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14378_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2797____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2797_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14381_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2797_PropertyInfos[] =
{
	&InternalEnumerator_1_t2797____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2797____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2797_InternalEnumerator_1__ctor_m14377_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14377_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14377_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2797_InternalEnumerator_1__ctor_m14377_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14377_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14378_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14378_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2797_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14378_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14379_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14379_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2797_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14379_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14380_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14380_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2797_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14380_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadAbstractBehaviour_t36_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14381_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14381_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2797_il2cpp_TypeInfo/* declaring_type */
	, &DataSetLoadAbstractBehaviour_t36_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14381_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2797_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14377_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14378_MethodInfo,
	&InternalEnumerator_1_Dispose_m14379_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14380_MethodInfo,
	&InternalEnumerator_1_get_Current_m14381_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14380_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14379_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2797_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14378_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14380_MethodInfo,
	&InternalEnumerator_1_Dispose_m14379_MethodInfo,
	&InternalEnumerator_1_get_Current_m14381_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2797_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5784_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2797_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5784_il2cpp_TypeInfo, 7},
};
extern TypeInfo DataSetLoadAbstractBehaviour_t36_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2797_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14381_MethodInfo/* Method Usage */,
	&DataSetLoadAbstractBehaviour_t36_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDataSetLoadAbstractBehaviour_t36_m32003_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2797_0_0_0;
extern Il2CppType InternalEnumerator_1_t2797_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2797_GenericClass;
TypeInfo InternalEnumerator_1_t2797_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2797_MethodInfos/* methods */
	, InternalEnumerator_1_t2797_PropertyInfos/* properties */
	, InternalEnumerator_1_t2797_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2797_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2797_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2797_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2797_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2797_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2797_1_0_0/* this_arg */
	, InternalEnumerator_1_t2797_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2797_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2797_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2797)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7348_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m41782_MethodInfo;
extern MethodInfo IList_1_set_Item_m41783_MethodInfo;
static PropertyInfo IList_1_t7348____Item_PropertyInfo = 
{
	&IList_1_t7348_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41782_MethodInfo/* get */
	, &IList_1_set_Item_m41783_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7348_PropertyInfos[] =
{
	&IList_1_t7348____Item_PropertyInfo,
	NULL
};
extern Il2CppType DataSetLoadAbstractBehaviour_t36_0_0_0;
static ParameterInfo IList_1_t7348_IList_1_IndexOf_m41784_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviour_t36_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41784_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41784_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7348_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7348_IList_1_IndexOf_m41784_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41784_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DataSetLoadAbstractBehaviour_t36_0_0_0;
static ParameterInfo IList_1_t7348_IList_1_Insert_m41785_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviour_t36_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41785_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41785_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7348_IList_1_Insert_m41785_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41785_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7348_IList_1_RemoveAt_m41786_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41786_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41786_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7348_IList_1_RemoveAt_m41786_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41786_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7348_IList_1_get_Item_m41782_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType DataSetLoadAbstractBehaviour_t36_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41782_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41782_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7348_il2cpp_TypeInfo/* declaring_type */
	, &DataSetLoadAbstractBehaviour_t36_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7348_IList_1_get_Item_m41782_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41782_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DataSetLoadAbstractBehaviour_t36_0_0_0;
static ParameterInfo IList_1_t7348_IList_1_set_Item_m41783_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DataSetLoadAbstractBehaviour_t36_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41783_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DataSetLoadAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41783_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7348_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7348_IList_1_set_Item_m41783_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41783_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7348_MethodInfos[] =
{
	&IList_1_IndexOf_m41784_MethodInfo,
	&IList_1_Insert_m41785_MethodInfo,
	&IList_1_RemoveAt_m41786_MethodInfo,
	&IList_1_get_Item_m41782_MethodInfo,
	&IList_1_set_Item_m41783_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7348_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7347_il2cpp_TypeInfo,
	&IEnumerable_1_t7349_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7348_0_0_0;
extern Il2CppType IList_1_t7348_1_0_0;
struct IList_1_t7348;
extern Il2CppGenericClass IList_1_t7348_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7348_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7348_MethodInfos/* methods */
	, IList_1_t7348_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7348_il2cpp_TypeInfo/* element_class */
	, IList_1_t7348_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7348_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7348_0_0_0/* byval_arg */
	, &IList_1_t7348_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7348_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_18.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2798_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_18MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_14.h"
extern TypeInfo InvokableCall_1_t2799_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_14MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14384_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14386_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2798____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2798_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2798, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2798_FieldInfos[] =
{
	&CachedInvokableCall_1_t2798____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2798_CachedInvokableCall_1__ctor_m14382_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t35_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14382_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14382_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2798_CachedInvokableCall_1__ctor_m14382_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14382_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2798_CachedInvokableCall_1_Invoke_m14383_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14383_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DataSetLoadBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14383_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2798_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2798_CachedInvokableCall_1_Invoke_m14383_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14383_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2798_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14382_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14383_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14383_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14387_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2798_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14383_MethodInfo,
	&InvokableCall_1_Find_m14387_MethodInfo,
};
extern Il2CppType UnityAction_1_t2800_0_0_0;
extern TypeInfo UnityAction_1_t2800_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisDataSetLoadBehaviour_t35_m32013_MethodInfo;
extern TypeInfo DataSetLoadBehaviour_t35_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14389_MethodInfo;
extern TypeInfo DataSetLoadBehaviour_t35_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2798_RGCTXData[8] = 
{
	&UnityAction_1_t2800_0_0_0/* Type Usage */,
	&UnityAction_1_t2800_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDataSetLoadBehaviour_t35_m32013_MethodInfo/* Method Usage */,
	&DataSetLoadBehaviour_t35_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14389_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14384_MethodInfo/* Method Usage */,
	&DataSetLoadBehaviour_t35_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14386_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2798_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2798_1_0_0;
struct CachedInvokableCall_1_t2798;
extern Il2CppGenericClass CachedInvokableCall_1_t2798_GenericClass;
TypeInfo CachedInvokableCall_1_t2798_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2798_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2798_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2799_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2798_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2798_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2798_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2798_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2798_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2798_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2798_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2798)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_21.h"
extern TypeInfo UnityAction_1_t2800_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_21MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DataSetLoadBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DataSetLoadBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisDataSetLoadBehaviour_t35_m32013(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>
extern Il2CppType UnityAction_1_t2800_0_0_1;
FieldInfo InvokableCall_1_t2799____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2800_0_0_1/* type */
	, &InvokableCall_1_t2799_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2799, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2799_FieldInfos[] =
{
	&InvokableCall_1_t2799____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2799_InvokableCall_1__ctor_m14384_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14384_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14384_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2799_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2799_InvokableCall_1__ctor_m14384_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14384_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2800_0_0_0;
static ParameterInfo InvokableCall_1_t2799_InvokableCall_1__ctor_m14385_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2800_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14385_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14385_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2799_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2799_InvokableCall_1__ctor_m14385_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14385_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2799_InvokableCall_1_Invoke_m14386_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14386_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14386_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2799_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2799_InvokableCall_1_Invoke_m14386_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14386_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2799_InvokableCall_1_Find_m14387_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14387_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DataSetLoadBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14387_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2799_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2799_InvokableCall_1_Find_m14387_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14387_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2799_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14384_MethodInfo,
	&InvokableCall_1__ctor_m14385_MethodInfo,
	&InvokableCall_1_Invoke_m14386_MethodInfo,
	&InvokableCall_1_Find_m14387_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2799_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14386_MethodInfo,
	&InvokableCall_1_Find_m14387_MethodInfo,
};
extern TypeInfo UnityAction_1_t2800_il2cpp_TypeInfo;
extern TypeInfo DataSetLoadBehaviour_t35_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2799_RGCTXData[5] = 
{
	&UnityAction_1_t2800_0_0_0/* Type Usage */,
	&UnityAction_1_t2800_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDataSetLoadBehaviour_t35_m32013_MethodInfo/* Method Usage */,
	&DataSetLoadBehaviour_t35_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14389_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2799_0_0_0;
extern Il2CppType InvokableCall_1_t2799_1_0_0;
struct InvokableCall_1_t2799;
extern Il2CppGenericClass InvokableCall_1_t2799_GenericClass;
TypeInfo InvokableCall_1_t2799_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2799_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2799_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2799_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2799_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2799_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2799_0_0_0/* byval_arg */
	, &InvokableCall_1_t2799_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2799_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2799_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2799)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2800_UnityAction_1__ctor_m14388_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14388_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14388_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2800_UnityAction_1__ctor_m14388_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14388_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
static ParameterInfo UnityAction_1_t2800_UnityAction_1_Invoke_m14389_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t35_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14389_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14389_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2800_UnityAction_1_Invoke_m14389_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14389_GenericMethod/* genericMethod */

};
extern Il2CppType DataSetLoadBehaviour_t35_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2800_UnityAction_1_BeginInvoke_m14390_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DataSetLoadBehaviour_t35_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14390_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14390_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2800_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2800_UnityAction_1_BeginInvoke_m14390_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14390_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2800_UnityAction_1_EndInvoke_m14391_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14391_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DataSetLoadBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14391_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2800_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2800_UnityAction_1_EndInvoke_m14391_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14391_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2800_MethodInfos[] =
{
	&UnityAction_1__ctor_m14388_MethodInfo,
	&UnityAction_1_Invoke_m14389_MethodInfo,
	&UnityAction_1_BeginInvoke_m14390_MethodInfo,
	&UnityAction_1_EndInvoke_m14391_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14390_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14391_MethodInfo;
static MethodInfo* UnityAction_1_t2800_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14389_MethodInfo,
	&UnityAction_1_BeginInvoke_m14390_MethodInfo,
	&UnityAction_1_EndInvoke_m14391_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2800_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2800_1_0_0;
struct UnityAction_1_t2800;
extern Il2CppGenericClass UnityAction_1_t2800_GenericClass;
TypeInfo UnityAction_1_t2800_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2800_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2800_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2800_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2800_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2800_0_0_0/* byval_arg */
	, &UnityAction_1_t2800_1_0_0/* this_arg */
	, UnityAction_1_t2800_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2800_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2800)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5786_il2cpp_TypeInfo;

// Vuforia.DefaultInitializationErrorHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErrorHandler.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.DefaultInitializationErrorHandler>
extern MethodInfo IEnumerator_1_get_Current_m41787_MethodInfo;
static PropertyInfo IEnumerator_1_t5786____Current_PropertyInfo = 
{
	&IEnumerator_1_t5786_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41787_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5786_PropertyInfos[] =
{
	&IEnumerator_1_t5786____Current_PropertyInfo,
	NULL
};
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41787_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41787_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5786_il2cpp_TypeInfo/* declaring_type */
	, &DefaultInitializationErrorHandler_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41787_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5786_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41787_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5786_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5786_0_0_0;
extern Il2CppType IEnumerator_1_t5786_1_0_0;
struct IEnumerator_1_t5786;
extern Il2CppGenericClass IEnumerator_1_t5786_GenericClass;
TypeInfo IEnumerator_1_t5786_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5786_MethodInfos/* methods */
	, IEnumerator_1_t5786_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5786_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5786_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5786_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5786_0_0_0/* byval_arg */
	, &IEnumerator_1_t5786_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5786_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_50.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2801_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_50MethodDeclarations.h"

extern TypeInfo DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14396_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDefaultInitializationErrorHandler_t37_m32015_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.DefaultInitializationErrorHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.DefaultInitializationErrorHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisDefaultInitializationErrorHandler_t37_m32015(__this, p0, method) (DefaultInitializationErrorHandler_t37 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2801____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2801_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2801, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2801____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2801_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2801, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2801_FieldInfos[] =
{
	&InternalEnumerator_1_t2801____array_0_FieldInfo,
	&InternalEnumerator_1_t2801____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14393_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2801____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2801_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14393_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2801____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2801_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14396_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2801_PropertyInfos[] =
{
	&InternalEnumerator_1_t2801____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2801____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2801_InternalEnumerator_1__ctor_m14392_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14392_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14392_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2801_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2801_InternalEnumerator_1__ctor_m14392_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14392_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14393_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14393_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2801_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14393_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14394_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14394_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2801_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14394_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14395_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14395_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2801_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14395_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14396_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.DefaultInitializationErrorHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14396_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2801_il2cpp_TypeInfo/* declaring_type */
	, &DefaultInitializationErrorHandler_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14396_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2801_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14392_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14393_MethodInfo,
	&InternalEnumerator_1_Dispose_m14394_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14395_MethodInfo,
	&InternalEnumerator_1_get_Current_m14396_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14395_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14394_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2801_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14393_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14395_MethodInfo,
	&InternalEnumerator_1_Dispose_m14394_MethodInfo,
	&InternalEnumerator_1_get_Current_m14396_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2801_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5786_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2801_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5786_il2cpp_TypeInfo, 7},
};
extern TypeInfo DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2801_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14396_MethodInfo/* Method Usage */,
	&DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDefaultInitializationErrorHandler_t37_m32015_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2801_0_0_0;
extern Il2CppType InternalEnumerator_1_t2801_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2801_GenericClass;
TypeInfo InternalEnumerator_1_t2801_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2801_MethodInfos/* methods */
	, InternalEnumerator_1_t2801_PropertyInfos/* properties */
	, InternalEnumerator_1_t2801_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2801_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2801_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2801_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2801_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2801_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2801_1_0_0/* this_arg */
	, InternalEnumerator_1_t2801_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2801_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2801_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2801)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7350_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>
extern MethodInfo ICollection_1_get_Count_m41788_MethodInfo;
static PropertyInfo ICollection_1_t7350____Count_PropertyInfo = 
{
	&ICollection_1_t7350_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41788_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41789_MethodInfo;
static PropertyInfo ICollection_1_t7350____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7350_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41789_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7350_PropertyInfos[] =
{
	&ICollection_1_t7350____Count_PropertyInfo,
	&ICollection_1_t7350____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41788_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m41788_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7350_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41788_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41789_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41789_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7350_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41789_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
static ParameterInfo ICollection_1_t7350_ICollection_1_Add_m41790_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t37_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41790_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Add(T)
MethodInfo ICollection_1_Add_m41790_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7350_ICollection_1_Add_m41790_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41790_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41791_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Clear()
MethodInfo ICollection_1_Clear_m41791_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41791_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
static ParameterInfo ICollection_1_t7350_ICollection_1_Contains_m41792_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t37_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41792_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m41792_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7350_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7350_ICollection_1_Contains_m41792_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41792_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandlerU5BU5D_t5173_0_0_0;
extern Il2CppType DefaultInitializationErrorHandlerU5BU5D_t5173_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7350_ICollection_1_CopyTo_m41793_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandlerU5BU5D_t5173_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41793_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41793_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7350_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7350_ICollection_1_CopyTo_m41793_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41793_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
static ParameterInfo ICollection_1_t7350_ICollection_1_Remove_m41794_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t37_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41794_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultInitializationErrorHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m41794_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7350_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7350_ICollection_1_Remove_m41794_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41794_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7350_MethodInfos[] =
{
	&ICollection_1_get_Count_m41788_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41789_MethodInfo,
	&ICollection_1_Add_m41790_MethodInfo,
	&ICollection_1_Clear_m41791_MethodInfo,
	&ICollection_1_Contains_m41792_MethodInfo,
	&ICollection_1_CopyTo_m41793_MethodInfo,
	&ICollection_1_Remove_m41794_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7352_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7350_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7352_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7350_0_0_0;
extern Il2CppType ICollection_1_t7350_1_0_0;
struct ICollection_1_t7350;
extern Il2CppGenericClass ICollection_1_t7350_GenericClass;
TypeInfo ICollection_1_t7350_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7350_MethodInfos/* methods */
	, ICollection_1_t7350_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7350_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7350_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7350_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7350_0_0_0/* byval_arg */
	, &ICollection_1_t7350_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7350_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DefaultInitializationErrorHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.DefaultInitializationErrorHandler>
extern Il2CppType IEnumerator_1_t5786_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41795_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DefaultInitializationErrorHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41795_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7352_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5786_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41795_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7352_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41795_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7352_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7352_0_0_0;
extern Il2CppType IEnumerable_1_t7352_1_0_0;
struct IEnumerable_1_t7352;
extern Il2CppGenericClass IEnumerable_1_t7352_GenericClass;
TypeInfo IEnumerable_1_t7352_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7352_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7352_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7352_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7352_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7352_0_0_0/* byval_arg */
	, &IEnumerable_1_t7352_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7352_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7351_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>
extern MethodInfo IList_1_get_Item_m41796_MethodInfo;
extern MethodInfo IList_1_set_Item_m41797_MethodInfo;
static PropertyInfo IList_1_t7351____Item_PropertyInfo = 
{
	&IList_1_t7351_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41796_MethodInfo/* get */
	, &IList_1_set_Item_m41797_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7351_PropertyInfos[] =
{
	&IList_1_t7351____Item_PropertyInfo,
	NULL
};
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
static ParameterInfo IList_1_t7351_IList_1_IndexOf_m41798_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t37_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41798_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41798_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7351_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7351_IList_1_IndexOf_m41798_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41798_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
static ParameterInfo IList_1_t7351_IList_1_Insert_m41799_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t37_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41799_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41799_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7351_IList_1_Insert_m41799_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41799_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7351_IList_1_RemoveAt_m41800_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41800_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41800_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7351_IList_1_RemoveAt_m41800_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41800_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7351_IList_1_get_Item_m41796_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41796_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41796_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7351_il2cpp_TypeInfo/* declaring_type */
	, &DefaultInitializationErrorHandler_t37_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7351_IList_1_get_Item_m41796_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41796_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
static ParameterInfo IList_1_t7351_IList_1_set_Item_m41797_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t37_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41797_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultInitializationErrorHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41797_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7351_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7351_IList_1_set_Item_m41797_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41797_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7351_MethodInfos[] =
{
	&IList_1_IndexOf_m41798_MethodInfo,
	&IList_1_Insert_m41799_MethodInfo,
	&IList_1_RemoveAt_m41800_MethodInfo,
	&IList_1_get_Item_m41796_MethodInfo,
	&IList_1_set_Item_m41797_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7351_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7350_il2cpp_TypeInfo,
	&IEnumerable_1_t7352_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7351_0_0_0;
extern Il2CppType IList_1_t7351_1_0_0;
struct IList_1_t7351;
extern Il2CppGenericClass IList_1_t7351_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7351_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7351_MethodInfos/* methods */
	, IList_1_t7351_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7351_il2cpp_TypeInfo/* element_class */
	, IList_1_t7351_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7351_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7351_0_0_0/* byval_arg */
	, &IList_1_t7351_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7351_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_19.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2802_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_19MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_15.h"
extern TypeInfo InvokableCall_1_t2803_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_15MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14399_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14401_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2802____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2802_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2802, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2802_FieldInfos[] =
{
	&CachedInvokableCall_1_t2802____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2802_CachedInvokableCall_1__ctor_m14397_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t37_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14397_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14397_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2802_CachedInvokableCall_1__ctor_m14397_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14397_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2802_CachedInvokableCall_1_Invoke_m14398_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14398_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14398_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2802_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2802_CachedInvokableCall_1_Invoke_m14398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14398_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2802_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14397_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14398_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14398_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14402_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2802_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14398_MethodInfo,
	&InvokableCall_1_Find_m14402_MethodInfo,
};
extern Il2CppType UnityAction_1_t2804_0_0_0;
extern TypeInfo UnityAction_1_t2804_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisDefaultInitializationErrorHandler_t37_m32025_MethodInfo;
extern TypeInfo DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14404_MethodInfo;
extern TypeInfo DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2802_RGCTXData[8] = 
{
	&UnityAction_1_t2804_0_0_0/* Type Usage */,
	&UnityAction_1_t2804_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDefaultInitializationErrorHandler_t37_m32025_MethodInfo/* Method Usage */,
	&DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14404_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14399_MethodInfo/* Method Usage */,
	&DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14401_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2802_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2802_1_0_0;
struct CachedInvokableCall_1_t2802;
extern Il2CppGenericClass CachedInvokableCall_1_t2802_GenericClass;
TypeInfo CachedInvokableCall_1_t2802_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2802_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2802_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2803_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2802_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2802_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2802_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2802_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2802_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2802_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2802_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2802)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_22.h"
extern TypeInfo UnityAction_1_t2804_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_22MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DefaultInitializationErrorHandler>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DefaultInitializationErrorHandler>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisDefaultInitializationErrorHandler_t37_m32025(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>
extern Il2CppType UnityAction_1_t2804_0_0_1;
FieldInfo InvokableCall_1_t2803____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2804_0_0_1/* type */
	, &InvokableCall_1_t2803_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2803, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2803_FieldInfos[] =
{
	&InvokableCall_1_t2803____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2803_InvokableCall_1__ctor_m14399_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14399_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14399_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2803_InvokableCall_1__ctor_m14399_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14399_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2804_0_0_0;
static ParameterInfo InvokableCall_1_t2803_InvokableCall_1__ctor_m14400_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2804_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14400_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14400_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2803_InvokableCall_1__ctor_m14400_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14400_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2803_InvokableCall_1_Invoke_m14401_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14401_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14401_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2803_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2803_InvokableCall_1_Invoke_m14401_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14401_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2803_InvokableCall_1_Find_m14402_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14402_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DefaultInitializationErrorHandler>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14402_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2803_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2803_InvokableCall_1_Find_m14402_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14402_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2803_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14399_MethodInfo,
	&InvokableCall_1__ctor_m14400_MethodInfo,
	&InvokableCall_1_Invoke_m14401_MethodInfo,
	&InvokableCall_1_Find_m14402_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2803_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14401_MethodInfo,
	&InvokableCall_1_Find_m14402_MethodInfo,
};
extern TypeInfo UnityAction_1_t2804_il2cpp_TypeInfo;
extern TypeInfo DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2803_RGCTXData[5] = 
{
	&UnityAction_1_t2804_0_0_0/* Type Usage */,
	&UnityAction_1_t2804_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDefaultInitializationErrorHandler_t37_m32025_MethodInfo/* Method Usage */,
	&DefaultInitializationErrorHandler_t37_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14404_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2803_0_0_0;
extern Il2CppType InvokableCall_1_t2803_1_0_0;
struct InvokableCall_1_t2803;
extern Il2CppGenericClass InvokableCall_1_t2803_GenericClass;
TypeInfo InvokableCall_1_t2803_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2803_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2803_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2803_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2803_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2803_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2803_0_0_0/* byval_arg */
	, &InvokableCall_1_t2803_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2803_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2803_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2803)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2804_UnityAction_1__ctor_m14403_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14403_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14403_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2804_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2804_UnityAction_1__ctor_m14403_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14403_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
static ParameterInfo UnityAction_1_t2804_UnityAction_1_Invoke_m14404_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t37_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14404_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14404_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2804_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2804_UnityAction_1_Invoke_m14404_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14404_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultInitializationErrorHandler_t37_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2804_UnityAction_1_BeginInvoke_m14405_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DefaultInitializationErrorHandler_t37_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14405_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14405_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2804_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2804_UnityAction_1_BeginInvoke_m14405_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14405_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2804_UnityAction_1_EndInvoke_m14406_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14406_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultInitializationErrorHandler>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14406_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2804_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2804_UnityAction_1_EndInvoke_m14406_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14406_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2804_MethodInfos[] =
{
	&UnityAction_1__ctor_m14403_MethodInfo,
	&UnityAction_1_Invoke_m14404_MethodInfo,
	&UnityAction_1_BeginInvoke_m14405_MethodInfo,
	&UnityAction_1_EndInvoke_m14406_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14405_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14406_MethodInfo;
static MethodInfo* UnityAction_1_t2804_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14404_MethodInfo,
	&UnityAction_1_BeginInvoke_m14405_MethodInfo,
	&UnityAction_1_EndInvoke_m14406_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2804_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2804_1_0_0;
struct UnityAction_1_t2804;
extern Il2CppGenericClass UnityAction_1_t2804_GenericClass;
TypeInfo UnityAction_1_t2804_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2804_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2804_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2804_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2804_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2804_0_0_0/* byval_arg */
	, &UnityAction_1_t2804_1_0_0/* this_arg */
	, UnityAction_1_t2804_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2804_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2804)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5788_il2cpp_TypeInfo;

// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.QCARAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.QCARAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41801_MethodInfo;
static PropertyInfo IEnumerator_1_t5788____Current_PropertyInfo = 
{
	&IEnumerator_1_t5788_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41801_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5788_PropertyInfos[] =
{
	&IEnumerator_1_t5788____Current_PropertyInfo,
	NULL
};
extern Il2CppType QCARAbstractBehaviour_t71_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41801_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.QCARAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41801_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5788_il2cpp_TypeInfo/* declaring_type */
	, &QCARAbstractBehaviour_t71_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41801_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5788_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41801_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5788_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5788_0_0_0;
extern Il2CppType IEnumerator_1_t5788_1_0_0;
struct IEnumerator_1_t5788;
extern Il2CppGenericClass IEnumerator_1_t5788_GenericClass;
TypeInfo IEnumerator_1_t5788_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5788_MethodInfos/* methods */
	, IEnumerator_1_t5788_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5788_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5788_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5788_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5788_0_0_0/* byval_arg */
	, &IEnumerator_1_t5788_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5788_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_51.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2805_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_51MethodDeclarations.h"

extern TypeInfo QCARAbstractBehaviour_t71_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14411_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisQCARAbstractBehaviour_t71_m32027_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.QCARAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.QCARAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisQCARAbstractBehaviour_t71_m32027(__this, p0, method) (QCARAbstractBehaviour_t71 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2805____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2805, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2805____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2805, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2805_FieldInfos[] =
{
	&InternalEnumerator_1_t2805____array_0_FieldInfo,
	&InternalEnumerator_1_t2805____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14408_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2805____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2805_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14408_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2805____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2805_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14411_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2805_PropertyInfos[] =
{
	&InternalEnumerator_1_t2805____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2805____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2805_InternalEnumerator_1__ctor_m14407_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14407_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14407_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2805_InternalEnumerator_1__ctor_m14407_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14407_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14408_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14408_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14408_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14409_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14409_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14409_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14410_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14410_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14410_GenericMethod/* genericMethod */

};
extern Il2CppType QCARAbstractBehaviour_t71_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14411_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.QCARAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14411_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* declaring_type */
	, &QCARAbstractBehaviour_t71_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14411_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2805_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14407_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14408_MethodInfo,
	&InternalEnumerator_1_Dispose_m14409_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14410_MethodInfo,
	&InternalEnumerator_1_get_Current_m14411_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14410_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14409_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2805_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14408_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14410_MethodInfo,
	&InternalEnumerator_1_Dispose_m14409_MethodInfo,
	&InternalEnumerator_1_get_Current_m14411_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2805_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5788_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2805_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5788_il2cpp_TypeInfo, 7},
};
extern TypeInfo QCARAbstractBehaviour_t71_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2805_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14411_MethodInfo/* Method Usage */,
	&QCARAbstractBehaviour_t71_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisQCARAbstractBehaviour_t71_m32027_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2805_0_0_0;
extern Il2CppType InternalEnumerator_1_t2805_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2805_GenericClass;
TypeInfo InternalEnumerator_1_t2805_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2805_MethodInfos/* methods */
	, InternalEnumerator_1_t2805_PropertyInfos/* properties */
	, InternalEnumerator_1_t2805_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2805_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2805_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2805_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2805_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2805_1_0_0/* this_arg */
	, InternalEnumerator_1_t2805_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2805_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2805_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2805)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7353_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m41802_MethodInfo;
static PropertyInfo ICollection_1_t7353____Count_PropertyInfo = 
{
	&ICollection_1_t7353_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41802_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41803_MethodInfo;
static PropertyInfo ICollection_1_t7353____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7353_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41803_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7353_PropertyInfos[] =
{
	&ICollection_1_t7353____Count_PropertyInfo,
	&ICollection_1_t7353____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41802_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41802_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7353_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41802_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41803_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41803_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7353_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41803_GenericMethod/* genericMethod */

};
extern Il2CppType QCARAbstractBehaviour_t71_0_0_0;
extern Il2CppType QCARAbstractBehaviour_t71_0_0_0;
static ParameterInfo ICollection_1_t7353_ICollection_1_Add_m41804_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviour_t71_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41804_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41804_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7353_ICollection_1_Add_m41804_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41804_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41805_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41805_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41805_GenericMethod/* genericMethod */

};
extern Il2CppType QCARAbstractBehaviour_t71_0_0_0;
static ParameterInfo ICollection_1_t7353_ICollection_1_Contains_m41806_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviour_t71_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41806_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41806_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7353_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7353_ICollection_1_Contains_m41806_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41806_GenericMethod/* genericMethod */

};
extern Il2CppType QCARAbstractBehaviourU5BU5D_t5499_0_0_0;
extern Il2CppType QCARAbstractBehaviourU5BU5D_t5499_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7353_ICollection_1_CopyTo_m41807_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviourU5BU5D_t5499_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41807_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41807_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7353_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7353_ICollection_1_CopyTo_m41807_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41807_GenericMethod/* genericMethod */

};
extern Il2CppType QCARAbstractBehaviour_t71_0_0_0;
static ParameterInfo ICollection_1_t7353_ICollection_1_Remove_m41808_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviour_t71_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41808_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.QCARAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41808_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7353_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7353_ICollection_1_Remove_m41808_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41808_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7353_MethodInfos[] =
{
	&ICollection_1_get_Count_m41802_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41803_MethodInfo,
	&ICollection_1_Add_m41804_MethodInfo,
	&ICollection_1_Clear_m41805_MethodInfo,
	&ICollection_1_Contains_m41806_MethodInfo,
	&ICollection_1_CopyTo_m41807_MethodInfo,
	&ICollection_1_Remove_m41808_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7355_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7353_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7355_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7353_0_0_0;
extern Il2CppType ICollection_1_t7353_1_0_0;
struct ICollection_1_t7353;
extern Il2CppGenericClass ICollection_1_t7353_GenericClass;
TypeInfo ICollection_1_t7353_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7353_MethodInfos/* methods */
	, ICollection_1_t7353_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7353_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7353_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7353_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7353_0_0_0/* byval_arg */
	, &ICollection_1_t7353_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7353_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.QCARAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.QCARAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5788_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41809_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.QCARAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41809_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7355_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5788_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41809_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7355_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41809_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7355_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7355_0_0_0;
extern Il2CppType IEnumerable_1_t7355_1_0_0;
struct IEnumerable_1_t7355;
extern Il2CppGenericClass IEnumerable_1_t7355_GenericClass;
TypeInfo IEnumerable_1_t7355_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7355_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7355_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7355_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7355_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7355_0_0_0/* byval_arg */
	, &IEnumerable_1_t7355_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7355_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7354_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m41810_MethodInfo;
extern MethodInfo IList_1_set_Item_m41811_MethodInfo;
static PropertyInfo IList_1_t7354____Item_PropertyInfo = 
{
	&IList_1_t7354_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41810_MethodInfo/* get */
	, &IList_1_set_Item_m41811_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7354_PropertyInfos[] =
{
	&IList_1_t7354____Item_PropertyInfo,
	NULL
};
extern Il2CppType QCARAbstractBehaviour_t71_0_0_0;
static ParameterInfo IList_1_t7354_IList_1_IndexOf_m41812_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviour_t71_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41812_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41812_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7354_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7354_IList_1_IndexOf_m41812_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41812_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType QCARAbstractBehaviour_t71_0_0_0;
static ParameterInfo IList_1_t7354_IList_1_Insert_m41813_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviour_t71_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41813_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41813_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7354_IList_1_Insert_m41813_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41813_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7354_IList_1_RemoveAt_m41814_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41814_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41814_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7354_IList_1_RemoveAt_m41814_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41814_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7354_IList_1_get_Item_m41810_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType QCARAbstractBehaviour_t71_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41810_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41810_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7354_il2cpp_TypeInfo/* declaring_type */
	, &QCARAbstractBehaviour_t71_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7354_IList_1_get_Item_m41810_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41810_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType QCARAbstractBehaviour_t71_0_0_0;
static ParameterInfo IList_1_t7354_IList_1_set_Item_m41811_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &QCARAbstractBehaviour_t71_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41811_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.QCARAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41811_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7354_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7354_IList_1_set_Item_m41811_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41811_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7354_MethodInfos[] =
{
	&IList_1_IndexOf_m41812_MethodInfo,
	&IList_1_Insert_m41813_MethodInfo,
	&IList_1_RemoveAt_m41814_MethodInfo,
	&IList_1_get_Item_m41810_MethodInfo,
	&IList_1_set_Item_m41811_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7354_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7353_il2cpp_TypeInfo,
	&IEnumerable_1_t7355_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7354_0_0_0;
extern Il2CppType IList_1_t7354_1_0_0;
struct IList_1_t7354;
extern Il2CppGenericClass IList_1_t7354_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7354_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7354_MethodInfos/* methods */
	, IList_1_t7354_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7354_il2cpp_TypeInfo/* element_class */
	, IList_1_t7354_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7354_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7354_0_0_0/* byval_arg */
	, &IList_1_t7354_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7354_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7356_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>
extern MethodInfo ICollection_1_get_Count_m41815_MethodInfo;
static PropertyInfo ICollection_1_t7356____Count_PropertyInfo = 
{
	&ICollection_1_t7356_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41815_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41816_MethodInfo;
static PropertyInfo ICollection_1_t7356____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7356_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41816_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7356_PropertyInfos[] =
{
	&ICollection_1_t7356____Count_PropertyInfo,
	&ICollection_1_t7356____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41815_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41815_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7356_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41815_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41816_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41816_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7356_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41816_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorQCARBehaviour_t163_0_0_0;
extern Il2CppType IEditorQCARBehaviour_t163_0_0_0;
static ParameterInfo ICollection_1_t7356_ICollection_1_Add_m41817_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviour_t163_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41817_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41817_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7356_ICollection_1_Add_m41817_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41817_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41818_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41818_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41818_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorQCARBehaviour_t163_0_0_0;
static ParameterInfo ICollection_1_t7356_ICollection_1_Contains_m41819_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviour_t163_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41819_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41819_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7356_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7356_ICollection_1_Contains_m41819_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41819_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorQCARBehaviourU5BU5D_t5500_0_0_0;
extern Il2CppType IEditorQCARBehaviourU5BU5D_t5500_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7356_ICollection_1_CopyTo_m41820_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviourU5BU5D_t5500_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41820_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41820_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7356_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7356_ICollection_1_CopyTo_m41820_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41820_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorQCARBehaviour_t163_0_0_0;
static ParameterInfo ICollection_1_t7356_ICollection_1_Remove_m41821_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviour_t163_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41821_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorQCARBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41821_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7356_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7356_ICollection_1_Remove_m41821_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41821_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7356_MethodInfos[] =
{
	&ICollection_1_get_Count_m41815_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41816_MethodInfo,
	&ICollection_1_Add_m41817_MethodInfo,
	&ICollection_1_Clear_m41818_MethodInfo,
	&ICollection_1_Contains_m41819_MethodInfo,
	&ICollection_1_CopyTo_m41820_MethodInfo,
	&ICollection_1_Remove_m41821_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7358_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7356_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7358_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7356_0_0_0;
extern Il2CppType ICollection_1_t7356_1_0_0;
struct ICollection_1_t7356;
extern Il2CppGenericClass ICollection_1_t7356_GenericClass;
TypeInfo ICollection_1_t7356_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7356_MethodInfos/* methods */
	, ICollection_1_t7356_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7356_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7356_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7356_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7356_0_0_0/* byval_arg */
	, &ICollection_1_t7356_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7356_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorQCARBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorQCARBehaviour>
extern Il2CppType IEnumerator_1_t5790_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41822_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorQCARBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41822_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7358_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5790_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41822_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7358_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41822_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7358_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7358_0_0_0;
extern Il2CppType IEnumerable_1_t7358_1_0_0;
struct IEnumerable_1_t7358;
extern Il2CppGenericClass IEnumerable_1_t7358_GenericClass;
TypeInfo IEnumerable_1_t7358_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7358_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7358_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7358_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7358_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7358_0_0_0/* byval_arg */
	, &IEnumerable_1_t7358_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7358_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5790_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorQCARBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorQCARBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41823_MethodInfo;
static PropertyInfo IEnumerator_1_t5790____Current_PropertyInfo = 
{
	&IEnumerator_1_t5790_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41823_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5790_PropertyInfos[] =
{
	&IEnumerator_1_t5790____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorQCARBehaviour_t163_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41823_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorQCARBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41823_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5790_il2cpp_TypeInfo/* declaring_type */
	, &IEditorQCARBehaviour_t163_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41823_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5790_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41823_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5790_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5790_0_0_0;
extern Il2CppType IEnumerator_1_t5790_1_0_0;
struct IEnumerator_1_t5790;
extern Il2CppGenericClass IEnumerator_1_t5790_GenericClass;
TypeInfo IEnumerator_1_t5790_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5790_MethodInfos/* methods */
	, IEnumerator_1_t5790_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5790_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5790_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5790_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5790_0_0_0/* byval_arg */
	, &IEnumerator_1_t5790_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5790_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_52.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2806_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_52MethodDeclarations.h"

extern TypeInfo IEditorQCARBehaviour_t163_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14416_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisIEditorQCARBehaviour_t163_m32038_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorQCARBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.IEditorQCARBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisIEditorQCARBehaviour_t163_m32038(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2806____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2806, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2806____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2806, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2806_FieldInfos[] =
{
	&InternalEnumerator_1_t2806____array_0_FieldInfo,
	&InternalEnumerator_1_t2806____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14413_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2806____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2806_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14413_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2806____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2806_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14416_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2806_PropertyInfos[] =
{
	&InternalEnumerator_1_t2806____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2806____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2806_InternalEnumerator_1__ctor_m14412_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14412_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14412_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2806_InternalEnumerator_1__ctor_m14412_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14412_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14413_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14413_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14413_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14414_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14414_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14414_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14415_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14415_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14415_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorQCARBehaviour_t163_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14416_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.IEditorQCARBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14416_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* declaring_type */
	, &IEditorQCARBehaviour_t163_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14416_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2806_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14412_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14413_MethodInfo,
	&InternalEnumerator_1_Dispose_m14414_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14415_MethodInfo,
	&InternalEnumerator_1_get_Current_m14416_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14415_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14414_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2806_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14413_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14415_MethodInfo,
	&InternalEnumerator_1_Dispose_m14414_MethodInfo,
	&InternalEnumerator_1_get_Current_m14416_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2806_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5790_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2806_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5790_il2cpp_TypeInfo, 7},
};
extern TypeInfo IEditorQCARBehaviour_t163_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2806_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14416_MethodInfo/* Method Usage */,
	&IEditorQCARBehaviour_t163_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisIEditorQCARBehaviour_t163_m32038_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2806_0_0_0;
extern Il2CppType InternalEnumerator_1_t2806_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2806_GenericClass;
TypeInfo InternalEnumerator_1_t2806_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2806_MethodInfos/* methods */
	, InternalEnumerator_1_t2806_PropertyInfos/* properties */
	, InternalEnumerator_1_t2806_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2806_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2806_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2806_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2806_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2806_1_0_0/* this_arg */
	, InternalEnumerator_1_t2806_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2806_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2806_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2806)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7357_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>
extern MethodInfo IList_1_get_Item_m41824_MethodInfo;
extern MethodInfo IList_1_set_Item_m41825_MethodInfo;
static PropertyInfo IList_1_t7357____Item_PropertyInfo = 
{
	&IList_1_t7357_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41824_MethodInfo/* get */
	, &IList_1_set_Item_m41825_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7357_PropertyInfos[] =
{
	&IList_1_t7357____Item_PropertyInfo,
	NULL
};
extern Il2CppType IEditorQCARBehaviour_t163_0_0_0;
static ParameterInfo IList_1_t7357_IList_1_IndexOf_m41826_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviour_t163_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41826_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41826_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7357_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7357_IList_1_IndexOf_m41826_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41826_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorQCARBehaviour_t163_0_0_0;
static ParameterInfo IList_1_t7357_IList_1_Insert_m41827_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviour_t163_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41827_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41827_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7357_IList_1_Insert_m41827_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41827_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7357_IList_1_RemoveAt_m41828_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41828_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41828_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7357_IList_1_RemoveAt_m41828_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41828_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7357_IList_1_get_Item_m41824_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType IEditorQCARBehaviour_t163_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41824_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41824_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7357_il2cpp_TypeInfo/* declaring_type */
	, &IEditorQCARBehaviour_t163_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7357_IList_1_get_Item_m41824_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41824_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType IEditorQCARBehaviour_t163_0_0_0;
static ParameterInfo IList_1_t7357_IList_1_set_Item_m41825_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &IEditorQCARBehaviour_t163_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41825_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.IEditorQCARBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41825_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7357_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7357_IList_1_set_Item_m41825_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41825_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7357_MethodInfos[] =
{
	&IList_1_IndexOf_m41826_MethodInfo,
	&IList_1_Insert_m41827_MethodInfo,
	&IList_1_RemoveAt_m41828_MethodInfo,
	&IList_1_get_Item_m41824_MethodInfo,
	&IList_1_set_Item_m41825_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7357_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7356_il2cpp_TypeInfo,
	&IEnumerable_1_t7358_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7357_0_0_0;
extern Il2CppType IList_1_t7357_1_0_0;
struct IList_1_t7357;
extern Il2CppGenericClass IList_1_t7357_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7357_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7357_MethodInfos/* methods */
	, IList_1_t7357_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7357_il2cpp_TypeInfo/* element_class */
	, IList_1_t7357_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7357_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7357_0_0_0/* byval_arg */
	, &IList_1_t7357_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7357_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Action`1<Vuforia.QCARUnity/InitError>
#include "mscorlib_System_Action_1_gen.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Action_1_t127_il2cpp_TypeInfo;
// System.Action`1<Vuforia.QCARUnity/InitError>
#include "mscorlib_System_Action_1_genMethodDeclarations.h"

// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"


// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::.ctor(System.Object,System.IntPtr)
extern MethodInfo Action_1__ctor_m398_MethodInfo;
 void Action_1__ctor_m398 (Action_1_t127 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method){
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::Invoke(T)
extern MethodInfo Action_1_Invoke_m5337_MethodInfo;
 void Action_1_Invoke_m5337 (Action_1_t127 * __this, int32_t ___obj, MethodInfo* method){
	if(__this->___prev_9 != NULL)
	{
		Action_1_Invoke_m5337((Action_1_t127 *)__this->___prev_9,___obj, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___obj, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___obj, MethodInfo* method);
	((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
}
// System.IAsyncResult System.Action`1<Vuforia.QCARUnity/InitError>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern MethodInfo Action_1_BeginInvoke_m14417_MethodInfo;
 Object_t * Action_1_BeginInvoke_m14417 (Action_1_t127 * __this, int32_t ___obj, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method){
	void *__d_args[2] = {0};
	__d_args[0] = Box(InitializedTypeInfo(&InitError_t128_il2cpp_TypeInfo), &___obj);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::EndInvoke(System.IAsyncResult)
extern MethodInfo Action_1_EndInvoke_m14418_MethodInfo;
 void Action_1_EndInvoke_m14418 (Action_1_t127 * __this, Object_t * ___result, MethodInfo* method){
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Metadata Definition System.Action`1<Vuforia.QCARUnity/InitError>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Action_1_t127_Action_1__ctor_m398_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1__ctor_m398_GenericMethod;
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::.ctor(System.Object,System.IntPtr)
MethodInfo Action_1__ctor_m398_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Action_1__ctor_m398/* method */
	, &Action_1_t127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, Action_1_t127_Action_1__ctor_m398_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1__ctor_m398_GenericMethod/* genericMethod */

};
extern Il2CppType InitError_t128_0_0_0;
extern Il2CppType InitError_t128_0_0_0;
static ParameterInfo Action_1_t127_Action_1_Invoke_m5337_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &InitError_t128_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_Invoke_m5337_GenericMethod;
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::Invoke(T)
MethodInfo Action_1_Invoke_m5337_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Action_1_Invoke_m5337/* method */
	, &Action_1_t127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, Action_1_t127_Action_1_Invoke_m5337_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_Invoke_m5337_GenericMethod/* genericMethod */

};
extern Il2CppType InitError_t128_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_1_t127_Action_1_BeginInvoke_m14417_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &InitError_t128_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_BeginInvoke_m14417_GenericMethod;
// System.IAsyncResult System.Action`1<Vuforia.QCARUnity/InitError>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Action_1_BeginInvoke_m14417_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Action_1_BeginInvoke_m14417/* method */
	, &Action_1_t127_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93_Object_t_Object_t/* invoker_method */
	, Action_1_t127_Action_1_BeginInvoke_m14417_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_BeginInvoke_m14417_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo Action_1_t127_Action_1_EndInvoke_m14418_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_EndInvoke_m14418_GenericMethod;
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::EndInvoke(System.IAsyncResult)
MethodInfo Action_1_EndInvoke_m14418_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Action_1_EndInvoke_m14418/* method */
	, &Action_1_t127_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, Action_1_t127_Action_1_EndInvoke_m14418_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_EndInvoke_m14418_GenericMethod/* genericMethod */

};
static MethodInfo* Action_1_t127_MethodInfos[] =
{
	&Action_1__ctor_m398_MethodInfo,
	&Action_1_Invoke_m5337_MethodInfo,
	&Action_1_BeginInvoke_m14417_MethodInfo,
	&Action_1_EndInvoke_m14418_MethodInfo,
	NULL
};
static MethodInfo* Action_1_t127_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&Action_1_Invoke_m5337_MethodInfo,
	&Action_1_BeginInvoke_m14417_MethodInfo,
	&Action_1_EndInvoke_m14418_MethodInfo,
};
static Il2CppInterfaceOffsetPair Action_1_t127_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Action_1_t127_0_0_0;
extern Il2CppType Action_1_t127_1_0_0;
struct Action_1_t127;
extern Il2CppGenericClass Action_1_t127_GenericClass;
TypeInfo Action_1_t127_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`1"/* name */
	, "System"/* namespaze */
	, Action_1_t127_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Action_1_t127_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Action_1_t127_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Action_1_t127_il2cpp_TypeInfo/* cast_class */
	, &Action_1_t127_0_0_0/* byval_arg */
	, &Action_1_t127_1_0_0/* this_arg */
	, Action_1_t127_InterfacesOffsets/* interface_offsets */
	, &Action_1_t127_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_1_t127)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5792_il2cpp_TypeInfo;

// Vuforia.DefaultSmartTerrainEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventHandler.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>
extern MethodInfo IEnumerator_1_get_Current_m41829_MethodInfo;
static PropertyInfo IEnumerator_1_t5792____Current_PropertyInfo = 
{
	&IEnumerator_1_t5792_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41829_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5792_PropertyInfos[] =
{
	&IEnumerator_1_t5792____Current_PropertyInfo,
	NULL
};
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41829_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41829_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5792_il2cpp_TypeInfo/* declaring_type */
	, &DefaultSmartTerrainEventHandler_t41_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41829_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5792_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41829_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5792_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5792_0_0_0;
extern Il2CppType IEnumerator_1_t5792_1_0_0;
struct IEnumerator_1_t5792;
extern Il2CppGenericClass IEnumerator_1_t5792_GenericClass;
TypeInfo IEnumerator_1_t5792_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5792_MethodInfos/* methods */
	, IEnumerator_1_t5792_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5792_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5792_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5792_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5792_0_0_0/* byval_arg */
	, &IEnumerator_1_t5792_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5792_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_53.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2807_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_53MethodDeclarations.h"

extern TypeInfo DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14423_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDefaultSmartTerrainEventHandler_t41_m32049_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.DefaultSmartTerrainEventHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.DefaultSmartTerrainEventHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisDefaultSmartTerrainEventHandler_t41_m32049(__this, p0, method) (DefaultSmartTerrainEventHandler_t41 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2807____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2807, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2807____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2807, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2807_FieldInfos[] =
{
	&InternalEnumerator_1_t2807____array_0_FieldInfo,
	&InternalEnumerator_1_t2807____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14420_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2807____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2807_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14420_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2807____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2807_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14423_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2807_PropertyInfos[] =
{
	&InternalEnumerator_1_t2807____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2807____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2807_InternalEnumerator_1__ctor_m14419_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14419_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14419_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2807_InternalEnumerator_1__ctor_m14419_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14419_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14420_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14420_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14420_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14421_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14421_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14421_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14422_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14422_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14422_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14423_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14423_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* declaring_type */
	, &DefaultSmartTerrainEventHandler_t41_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14423_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2807_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14419_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14420_MethodInfo,
	&InternalEnumerator_1_Dispose_m14421_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14422_MethodInfo,
	&InternalEnumerator_1_get_Current_m14423_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14422_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14421_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2807_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14420_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14422_MethodInfo,
	&InternalEnumerator_1_Dispose_m14421_MethodInfo,
	&InternalEnumerator_1_get_Current_m14423_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2807_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5792_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2807_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5792_il2cpp_TypeInfo, 7},
};
extern TypeInfo DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2807_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14423_MethodInfo/* Method Usage */,
	&DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDefaultSmartTerrainEventHandler_t41_m32049_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2807_0_0_0;
extern Il2CppType InternalEnumerator_1_t2807_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2807_GenericClass;
TypeInfo InternalEnumerator_1_t2807_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2807_MethodInfos/* methods */
	, InternalEnumerator_1_t2807_PropertyInfos/* properties */
	, InternalEnumerator_1_t2807_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2807_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2807_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2807_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2807_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2807_1_0_0/* this_arg */
	, InternalEnumerator_1_t2807_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2807_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2807_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2807)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7359_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>
extern MethodInfo ICollection_1_get_Count_m41830_MethodInfo;
static PropertyInfo ICollection_1_t7359____Count_PropertyInfo = 
{
	&ICollection_1_t7359_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41830_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41831_MethodInfo;
static PropertyInfo ICollection_1_t7359____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7359_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41831_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7359_PropertyInfos[] =
{
	&ICollection_1_t7359____Count_PropertyInfo,
	&ICollection_1_t7359____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41830_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m41830_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7359_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41830_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41831_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41831_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7359_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41831_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
static ParameterInfo ICollection_1_t7359_ICollection_1_Add_m41832_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t41_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41832_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Add(T)
MethodInfo ICollection_1_Add_m41832_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7359_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7359_ICollection_1_Add_m41832_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41832_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41833_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Clear()
MethodInfo ICollection_1_Clear_m41833_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7359_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41833_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
static ParameterInfo ICollection_1_t7359_ICollection_1_Contains_m41834_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t41_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41834_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m41834_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7359_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7359_ICollection_1_Contains_m41834_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41834_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandlerU5BU5D_t5174_0_0_0;
extern Il2CppType DefaultSmartTerrainEventHandlerU5BU5D_t5174_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7359_ICollection_1_CopyTo_m41835_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandlerU5BU5D_t5174_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41835_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41835_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7359_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7359_ICollection_1_CopyTo_m41835_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41835_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
static ParameterInfo ICollection_1_t7359_ICollection_1_Remove_m41836_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t41_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41836_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultSmartTerrainEventHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m41836_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7359_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7359_ICollection_1_Remove_m41836_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41836_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7359_MethodInfos[] =
{
	&ICollection_1_get_Count_m41830_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41831_MethodInfo,
	&ICollection_1_Add_m41832_MethodInfo,
	&ICollection_1_Clear_m41833_MethodInfo,
	&ICollection_1_Contains_m41834_MethodInfo,
	&ICollection_1_CopyTo_m41835_MethodInfo,
	&ICollection_1_Remove_m41836_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7361_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7359_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7361_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7359_0_0_0;
extern Il2CppType ICollection_1_t7359_1_0_0;
struct ICollection_1_t7359;
extern Il2CppGenericClass ICollection_1_t7359_GenericClass;
TypeInfo ICollection_1_t7359_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7359_MethodInfos/* methods */
	, ICollection_1_t7359_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7359_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7359_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7359_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7359_0_0_0/* byval_arg */
	, &ICollection_1_t7359_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7359_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DefaultSmartTerrainEventHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.DefaultSmartTerrainEventHandler>
extern Il2CppType IEnumerator_1_t5792_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41837_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DefaultSmartTerrainEventHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41837_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7361_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5792_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41837_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7361_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41837_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7361_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7361_0_0_0;
extern Il2CppType IEnumerable_1_t7361_1_0_0;
struct IEnumerable_1_t7361;
extern Il2CppGenericClass IEnumerable_1_t7361_GenericClass;
TypeInfo IEnumerable_1_t7361_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7361_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7361_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7361_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7361_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7361_0_0_0/* byval_arg */
	, &IEnumerable_1_t7361_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7361_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7360_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>
extern MethodInfo IList_1_get_Item_m41838_MethodInfo;
extern MethodInfo IList_1_set_Item_m41839_MethodInfo;
static PropertyInfo IList_1_t7360____Item_PropertyInfo = 
{
	&IList_1_t7360_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41838_MethodInfo/* get */
	, &IList_1_set_Item_m41839_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7360_PropertyInfos[] =
{
	&IList_1_t7360____Item_PropertyInfo,
	NULL
};
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
static ParameterInfo IList_1_t7360_IList_1_IndexOf_m41840_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t41_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41840_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41840_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7360_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7360_IList_1_IndexOf_m41840_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41840_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
static ParameterInfo IList_1_t7360_IList_1_Insert_m41841_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t41_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41841_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41841_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7360_IList_1_Insert_m41841_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41841_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7360_IList_1_RemoveAt_m41842_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41842_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41842_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7360_IList_1_RemoveAt_m41842_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41842_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7360_IList_1_get_Item_m41838_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41838_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41838_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7360_il2cpp_TypeInfo/* declaring_type */
	, &DefaultSmartTerrainEventHandler_t41_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7360_IList_1_get_Item_m41838_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41838_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
static ParameterInfo IList_1_t7360_IList_1_set_Item_m41839_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t41_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41839_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultSmartTerrainEventHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41839_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7360_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7360_IList_1_set_Item_m41839_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41839_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7360_MethodInfos[] =
{
	&IList_1_IndexOf_m41840_MethodInfo,
	&IList_1_Insert_m41841_MethodInfo,
	&IList_1_RemoveAt_m41842_MethodInfo,
	&IList_1_get_Item_m41838_MethodInfo,
	&IList_1_set_Item_m41839_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7360_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7359_il2cpp_TypeInfo,
	&IEnumerable_1_t7361_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7360_0_0_0;
extern Il2CppType IList_1_t7360_1_0_0;
struct IList_1_t7360;
extern Il2CppGenericClass IList_1_t7360_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7360_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7360_MethodInfos/* methods */
	, IList_1_t7360_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7360_il2cpp_TypeInfo/* element_class */
	, IList_1_t7360_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7360_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7360_0_0_0/* byval_arg */
	, &IList_1_t7360_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7360_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_20.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2808_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_20MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_16.h"
extern TypeInfo InvokableCall_1_t2809_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_16MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14426_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14428_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2808____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2808_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2808, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2808_FieldInfos[] =
{
	&CachedInvokableCall_1_t2808____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2808_CachedInvokableCall_1__ctor_m14424_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t41_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14424_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14424_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2808_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2808_CachedInvokableCall_1__ctor_m14424_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14424_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2808_CachedInvokableCall_1_Invoke_m14425_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14425_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14425_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2808_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2808_CachedInvokableCall_1_Invoke_m14425_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14425_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2808_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14424_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14425_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14425_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14429_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2808_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14425_MethodInfo,
	&InvokableCall_1_Find_m14429_MethodInfo,
};
extern Il2CppType UnityAction_1_t2810_0_0_0;
extern TypeInfo UnityAction_1_t2810_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisDefaultSmartTerrainEventHandler_t41_m32059_MethodInfo;
extern TypeInfo DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14431_MethodInfo;
extern TypeInfo DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2808_RGCTXData[8] = 
{
	&UnityAction_1_t2810_0_0_0/* Type Usage */,
	&UnityAction_1_t2810_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDefaultSmartTerrainEventHandler_t41_m32059_MethodInfo/* Method Usage */,
	&DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14431_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14426_MethodInfo/* Method Usage */,
	&DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14428_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2808_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2808_1_0_0;
struct CachedInvokableCall_1_t2808;
extern Il2CppGenericClass CachedInvokableCall_1_t2808_GenericClass;
TypeInfo CachedInvokableCall_1_t2808_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2808_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2808_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2808_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2808_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2808_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2808_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2808_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2808_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2808_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2808)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_23.h"
extern TypeInfo UnityAction_1_t2810_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_23MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DefaultSmartTerrainEventHandler>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DefaultSmartTerrainEventHandler>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisDefaultSmartTerrainEventHandler_t41_m32059(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>
extern Il2CppType UnityAction_1_t2810_0_0_1;
FieldInfo InvokableCall_1_t2809____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2810_0_0_1/* type */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2809, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2809_FieldInfos[] =
{
	&InvokableCall_1_t2809____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2809_InvokableCall_1__ctor_m14426_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14426_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14426_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2809_InvokableCall_1__ctor_m14426_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14426_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2810_0_0_0;
static ParameterInfo InvokableCall_1_t2809_InvokableCall_1__ctor_m14427_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2810_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14427_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14427_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2809_InvokableCall_1__ctor_m14427_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14427_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2809_InvokableCall_1_Invoke_m14428_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14428_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14428_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2809_InvokableCall_1_Invoke_m14428_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14428_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2809_InvokableCall_1_Find_m14429_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14429_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DefaultSmartTerrainEventHandler>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14429_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2809_InvokableCall_1_Find_m14429_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14429_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2809_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14426_MethodInfo,
	&InvokableCall_1__ctor_m14427_MethodInfo,
	&InvokableCall_1_Invoke_m14428_MethodInfo,
	&InvokableCall_1_Find_m14429_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2809_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14428_MethodInfo,
	&InvokableCall_1_Find_m14429_MethodInfo,
};
extern TypeInfo UnityAction_1_t2810_il2cpp_TypeInfo;
extern TypeInfo DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2809_RGCTXData[5] = 
{
	&UnityAction_1_t2810_0_0_0/* Type Usage */,
	&UnityAction_1_t2810_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDefaultSmartTerrainEventHandler_t41_m32059_MethodInfo/* Method Usage */,
	&DefaultSmartTerrainEventHandler_t41_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14431_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2809_0_0_0;
extern Il2CppType InvokableCall_1_t2809_1_0_0;
struct InvokableCall_1_t2809;
extern Il2CppGenericClass InvokableCall_1_t2809_GenericClass;
TypeInfo InvokableCall_1_t2809_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2809_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2809_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2809_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2809_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2809_0_0_0/* byval_arg */
	, &InvokableCall_1_t2809_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2809_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2809_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2809)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2810_UnityAction_1__ctor_m14430_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14430_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14430_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2810_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2810_UnityAction_1__ctor_m14430_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14430_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
static ParameterInfo UnityAction_1_t2810_UnityAction_1_Invoke_m14431_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t41_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14431_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14431_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2810_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2810_UnityAction_1_Invoke_m14431_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14431_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultSmartTerrainEventHandler_t41_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2810_UnityAction_1_BeginInvoke_m14432_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DefaultSmartTerrainEventHandler_t41_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14432_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14432_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2810_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2810_UnityAction_1_BeginInvoke_m14432_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14432_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2810_UnityAction_1_EndInvoke_m14433_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14433_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultSmartTerrainEventHandler>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14433_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2810_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2810_UnityAction_1_EndInvoke_m14433_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14433_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2810_MethodInfos[] =
{
	&UnityAction_1__ctor_m14430_MethodInfo,
	&UnityAction_1_Invoke_m14431_MethodInfo,
	&UnityAction_1_BeginInvoke_m14432_MethodInfo,
	&UnityAction_1_EndInvoke_m14433_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14432_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14433_MethodInfo;
static MethodInfo* UnityAction_1_t2810_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14431_MethodInfo,
	&UnityAction_1_BeginInvoke_m14432_MethodInfo,
	&UnityAction_1_EndInvoke_m14433_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2810_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2810_1_0_0;
struct UnityAction_1_t2810;
extern Il2CppGenericClass UnityAction_1_t2810_GenericClass;
TypeInfo UnityAction_1_t2810_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2810_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2810_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2810_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2810_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2810_0_0_0/* byval_arg */
	, &UnityAction_1_t2810_1_0_0/* this_arg */
	, UnityAction_1_t2810_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2810_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2810)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_4.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2811_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<Vuforia.ReconstructionBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_4MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<Vuforia.ReconstructionBehaviour>
extern Il2CppType ReconstructionBehaviour_t38_0_0_6;
FieldInfo CastHelper_1_t2811____t_0_FieldInfo = 
{
	"t"/* name */
	, &ReconstructionBehaviour_t38_0_0_6/* type */
	, &CastHelper_1_t2811_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2811, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2811____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2811_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2811, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2811_FieldInfos[] =
{
	&CastHelper_1_t2811____t_0_FieldInfo,
	&CastHelper_1_t2811____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2811_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2811_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2811_0_0_0;
extern Il2CppType CastHelper_1_t2811_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2811_GenericClass;
TypeInfo CastHelper_1_t2811_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2811_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2811_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2811_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2811_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2811_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2811_0_0_0/* byval_arg */
	, &CastHelper_1_t2811_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2811_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2811)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Action`1<Vuforia.Prop>
#include "mscorlib_System_Action_1_gen_0.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Action_1_t130_il2cpp_TypeInfo;
// System.Action`1<Vuforia.Prop>
#include "mscorlib_System_Action_1_gen_0MethodDeclarations.h"



// System.Void System.Action`1<Vuforia.Prop>::.ctor(System.Object,System.IntPtr)
// System.Void System.Action`1<Vuforia.Prop>::Invoke(T)
// System.IAsyncResult System.Action`1<Vuforia.Prop>::BeginInvoke(T,System.AsyncCallback,System.Object)
// System.Void System.Action`1<Vuforia.Prop>::EndInvoke(System.IAsyncResult)
// Metadata Definition System.Action`1<Vuforia.Prop>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Action_1_t130_Action_1__ctor_m405_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1__ctor_m405_GenericMethod;
// System.Void System.Action`1<Vuforia.Prop>::.ctor(System.Object,System.IntPtr)
MethodInfo Action_1__ctor_m405_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Action_1__ctor_m14434_gshared/* method */
	, &Action_1_t130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, Action_1_t130_Action_1__ctor_m405_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1__ctor_m405_GenericMethod/* genericMethod */

};
extern Il2CppType Prop_t42_0_0_0;
extern Il2CppType Prop_t42_0_0_0;
static ParameterInfo Action_1_t130_Action_1_Invoke_m5186_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Prop_t42_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_Invoke_m5186_GenericMethod;
// System.Void System.Action`1<Vuforia.Prop>::Invoke(T)
MethodInfo Action_1_Invoke_m5186_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Action_1_Invoke_m14435_gshared/* method */
	, &Action_1_t130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, Action_1_t130_Action_1_Invoke_m5186_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_Invoke_m5186_GenericMethod/* genericMethod */

};
extern Il2CppType Prop_t42_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_1_t130_Action_1_BeginInvoke_m14436_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Prop_t42_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_BeginInvoke_m14436_GenericMethod;
// System.IAsyncResult System.Action`1<Vuforia.Prop>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Action_1_BeginInvoke_m14436_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Action_1_BeginInvoke_m14437_gshared/* method */
	, &Action_1_t130_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Action_1_t130_Action_1_BeginInvoke_m14436_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_BeginInvoke_m14436_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo Action_1_t130_Action_1_EndInvoke_m14438_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_EndInvoke_m14438_GenericMethod;
// System.Void System.Action`1<Vuforia.Prop>::EndInvoke(System.IAsyncResult)
MethodInfo Action_1_EndInvoke_m14438_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Action_1_EndInvoke_m14439_gshared/* method */
	, &Action_1_t130_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, Action_1_t130_Action_1_EndInvoke_m14438_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_EndInvoke_m14438_GenericMethod/* genericMethod */

};
static MethodInfo* Action_1_t130_MethodInfos[] =
{
	&Action_1__ctor_m405_MethodInfo,
	&Action_1_Invoke_m5186_MethodInfo,
	&Action_1_BeginInvoke_m14436_MethodInfo,
	&Action_1_EndInvoke_m14438_MethodInfo,
	NULL
};
extern MethodInfo Action_1_Invoke_m5186_MethodInfo;
extern MethodInfo Action_1_BeginInvoke_m14436_MethodInfo;
extern MethodInfo Action_1_EndInvoke_m14438_MethodInfo;
static MethodInfo* Action_1_t130_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&Action_1_Invoke_m5186_MethodInfo,
	&Action_1_BeginInvoke_m14436_MethodInfo,
	&Action_1_EndInvoke_m14438_MethodInfo,
};
static Il2CppInterfaceOffsetPair Action_1_t130_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Action_1_t130_0_0_0;
extern Il2CppType Action_1_t130_1_0_0;
struct Action_1_t130;
extern Il2CppGenericClass Action_1_t130_GenericClass;
TypeInfo Action_1_t130_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`1"/* name */
	, "System"/* namespaze */
	, Action_1_t130_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Action_1_t130_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Action_1_t130_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Action_1_t130_il2cpp_TypeInfo/* cast_class */
	, &Action_1_t130_0_0_0/* byval_arg */
	, &Action_1_t130_1_0_0/* this_arg */
	, Action_1_t130_InterfacesOffsets/* interface_offsets */
	, &Action_1_t130_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_1_t130)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_9.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Action_1_t2812_il2cpp_TypeInfo;
// System.Action`1<System.Object>
#include "mscorlib_System_Action_1_gen_9MethodDeclarations.h"



// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern MethodInfo Action_1__ctor_m14434_MethodInfo;
 void Action_1__ctor_m14434_gshared (Action_1_t2812 * __this, Object_t * ___object, IntPtr_t121 ___method, MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern MethodInfo Action_1_Invoke_m14435_MethodInfo;
 void Action_1_Invoke_m14435_gshared (Action_1_t2812 * __this, Object_t * ___obj, MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		Action_1_Invoke_m14435((Action_1_t2812 *)__this->___prev_9,___obj, method);
	}
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___obj, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___obj, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___obj,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern MethodInfo Action_1_BeginInvoke_m14437_MethodInfo;
 Object_t * Action_1_BeginInvoke_m14437_gshared (Action_1_t2812 * __this, Object_t * ___obj, AsyncCallback_t200 * ___callback, Object_t * ___object, MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern MethodInfo Action_1_EndInvoke_m14439_MethodInfo;
 void Action_1_EndInvoke_m14439_gshared (Action_1_t2812 * __this, Object_t * ___result, MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// Metadata Definition System.Action`1<System.Object>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Action_1_t2812_Action_1__ctor_m14434_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1__ctor_m14434_GenericMethod;
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
MethodInfo Action_1__ctor_m14434_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Action_1__ctor_m14434_gshared/* method */
	, &Action_1_t2812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, Action_1_t2812_Action_1__ctor_m14434_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1__ctor_m14434_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_1_t2812_Action_1_Invoke_m14435_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_Invoke_m14435_GenericMethod;
// System.Void System.Action`1<System.Object>::Invoke(T)
MethodInfo Action_1_Invoke_m14435_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Action_1_Invoke_m14435_gshared/* method */
	, &Action_1_t2812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, Action_1_t2812_Action_1_Invoke_m14435_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_Invoke_m14435_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_1_t2812_Action_1_BeginInvoke_m14437_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_BeginInvoke_m14437_GenericMethod;
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Action_1_BeginInvoke_m14437_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Action_1_BeginInvoke_m14437_gshared/* method */
	, &Action_1_t2812_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Action_1_t2812_Action_1_BeginInvoke_m14437_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_BeginInvoke_m14437_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo Action_1_t2812_Action_1_EndInvoke_m14439_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_EndInvoke_m14439_GenericMethod;
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
MethodInfo Action_1_EndInvoke_m14439_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Action_1_EndInvoke_m14439_gshared/* method */
	, &Action_1_t2812_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, Action_1_t2812_Action_1_EndInvoke_m14439_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_EndInvoke_m14439_GenericMethod/* genericMethod */

};
static MethodInfo* Action_1_t2812_MethodInfos[] =
{
	&Action_1__ctor_m14434_MethodInfo,
	&Action_1_Invoke_m14435_MethodInfo,
	&Action_1_BeginInvoke_m14437_MethodInfo,
	&Action_1_EndInvoke_m14439_MethodInfo,
	NULL
};
static MethodInfo* Action_1_t2812_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&Action_1_Invoke_m14435_MethodInfo,
	&Action_1_BeginInvoke_m14437_MethodInfo,
	&Action_1_EndInvoke_m14439_MethodInfo,
};
static Il2CppInterfaceOffsetPair Action_1_t2812_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Action_1_t2812_0_0_0;
extern Il2CppType Action_1_t2812_1_0_0;
struct Action_1_t2812;
extern Il2CppGenericClass Action_1_t2812_GenericClass;
TypeInfo Action_1_t2812_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`1"/* name */
	, "System"/* namespaze */
	, Action_1_t2812_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Action_1_t2812_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Action_1_t2812_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Action_1_t2812_il2cpp_TypeInfo/* cast_class */
	, &Action_1_t2812_0_0_0/* byval_arg */
	, &Action_1_t2812_1_0_0/* this_arg */
	, Action_1_t2812_InterfacesOffsets/* interface_offsets */
	, &Action_1_t2812_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_1_t2812)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// System.Action`1<Vuforia.Surface>
#include "mscorlib_System_Action_1_gen_1.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo Action_1_t131_il2cpp_TypeInfo;
// System.Action`1<Vuforia.Surface>
#include "mscorlib_System_Action_1_gen_1MethodDeclarations.h"



// System.Void System.Action`1<Vuforia.Surface>::.ctor(System.Object,System.IntPtr)
// System.Void System.Action`1<Vuforia.Surface>::Invoke(T)
// System.IAsyncResult System.Action`1<Vuforia.Surface>::BeginInvoke(T,System.AsyncCallback,System.Object)
// System.Void System.Action`1<Vuforia.Surface>::EndInvoke(System.IAsyncResult)
// Metadata Definition System.Action`1<Vuforia.Surface>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo Action_1_t131_Action_1__ctor_m407_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1__ctor_m407_GenericMethod;
// System.Void System.Action`1<Vuforia.Surface>::.ctor(System.Object,System.IntPtr)
MethodInfo Action_1__ctor_m407_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&Action_1__ctor_m14434_gshared/* method */
	, &Action_1_t131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, Action_1_t131_Action_1__ctor_m407_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1__ctor_m407_GenericMethod/* genericMethod */

};
extern Il2CppType Surface_t43_0_0_0;
extern Il2CppType Surface_t43_0_0_0;
static ParameterInfo Action_1_t131_Action_1_Invoke_m5183_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Surface_t43_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_Invoke_m5183_GenericMethod;
// System.Void System.Action`1<Vuforia.Surface>::Invoke(T)
MethodInfo Action_1_Invoke_m5183_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&Action_1_Invoke_m14435_gshared/* method */
	, &Action_1_t131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, Action_1_t131_Action_1_Invoke_m5183_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_Invoke_m5183_GenericMethod/* genericMethod */

};
extern Il2CppType Surface_t43_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo Action_1_t131_Action_1_BeginInvoke_m14440_ParameterInfos[] = 
{
	{"obj", 0, 134217728, &EmptyCustomAttributesCache, &Surface_t43_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_BeginInvoke_m14440_GenericMethod;
// System.IAsyncResult System.Action`1<Vuforia.Surface>::BeginInvoke(T,System.AsyncCallback,System.Object)
MethodInfo Action_1_BeginInvoke_m14440_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&Action_1_BeginInvoke_m14437_gshared/* method */
	, &Action_1_t131_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, Action_1_t131_Action_1_BeginInvoke_m14440_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_BeginInvoke_m14440_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo Action_1_t131_Action_1_EndInvoke_m14441_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod Action_1_EndInvoke_m14441_GenericMethod;
// System.Void System.Action`1<Vuforia.Surface>::EndInvoke(System.IAsyncResult)
MethodInfo Action_1_EndInvoke_m14441_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&Action_1_EndInvoke_m14439_gshared/* method */
	, &Action_1_t131_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, Action_1_t131_Action_1_EndInvoke_m14441_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &Action_1_EndInvoke_m14441_GenericMethod/* genericMethod */

};
static MethodInfo* Action_1_t131_MethodInfos[] =
{
	&Action_1__ctor_m407_MethodInfo,
	&Action_1_Invoke_m5183_MethodInfo,
	&Action_1_BeginInvoke_m14440_MethodInfo,
	&Action_1_EndInvoke_m14441_MethodInfo,
	NULL
};
extern MethodInfo Action_1_Invoke_m5183_MethodInfo;
extern MethodInfo Action_1_BeginInvoke_m14440_MethodInfo;
extern MethodInfo Action_1_EndInvoke_m14441_MethodInfo;
static MethodInfo* Action_1_t131_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&Action_1_Invoke_m5183_MethodInfo,
	&Action_1_BeginInvoke_m14440_MethodInfo,
	&Action_1_EndInvoke_m14441_MethodInfo,
};
static Il2CppInterfaceOffsetPair Action_1_t131_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType Action_1_t131_0_0_0;
extern Il2CppType Action_1_t131_1_0_0;
struct Action_1_t131;
extern Il2CppGenericClass Action_1_t131_GenericClass;
TypeInfo Action_1_t131_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "Action`1"/* name */
	, "System"/* namespaze */
	, Action_1_t131_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &Action_1_t131_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, Action_1_t131_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &Action_1_t131_il2cpp_TypeInfo/* cast_class */
	, &Action_1_t131_0_0_0/* byval_arg */
	, &Action_1_t131_1_0_0/* this_arg */
	, Action_1_t131_InterfacesOffsets/* interface_offsets */
	, &Action_1_t131_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (Action_1_t131)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5794_il2cpp_TypeInfo;

// Vuforia.DefaultTrackableEventHandler
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHandler.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.DefaultTrackableEventHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.DefaultTrackableEventHandler>
extern MethodInfo IEnumerator_1_get_Current_m41843_MethodInfo;
static PropertyInfo IEnumerator_1_t5794____Current_PropertyInfo = 
{
	&IEnumerator_1_t5794_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41843_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5794_PropertyInfos[] =
{
	&IEnumerator_1_t5794____Current_PropertyInfo,
	NULL
};
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41843_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.DefaultTrackableEventHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41843_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5794_il2cpp_TypeInfo/* declaring_type */
	, &DefaultTrackableEventHandler_t45_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41843_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5794_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41843_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5794_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5794_0_0_0;
extern Il2CppType IEnumerator_1_t5794_1_0_0;
struct IEnumerator_1_t5794;
extern Il2CppGenericClass IEnumerator_1_t5794_GenericClass;
TypeInfo IEnumerator_1_t5794_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5794_MethodInfos/* methods */
	, IEnumerator_1_t5794_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5794_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5794_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5794_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5794_0_0_0/* byval_arg */
	, &IEnumerator_1_t5794_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5794_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_54.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2813_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_54MethodDeclarations.h"

extern TypeInfo DefaultTrackableEventHandler_t45_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14446_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisDefaultTrackableEventHandler_t45_m32061_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.DefaultTrackableEventHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.DefaultTrackableEventHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisDefaultTrackableEventHandler_t45_m32061(__this, p0, method) (DefaultTrackableEventHandler_t45 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2813____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2813_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2813, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2813____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2813_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2813, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2813_FieldInfos[] =
{
	&InternalEnumerator_1_t2813____array_0_FieldInfo,
	&InternalEnumerator_1_t2813____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14443_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2813____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2813_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14443_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2813____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2813_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14446_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2813_PropertyInfos[] =
{
	&InternalEnumerator_1_t2813____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2813____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2813_InternalEnumerator_1__ctor_m14442_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14442_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14442_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2813_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2813_InternalEnumerator_1__ctor_m14442_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14442_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14443_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14443_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2813_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14443_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14444_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14444_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2813_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14444_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14445_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14445_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2813_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14445_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14446_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.DefaultTrackableEventHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14446_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2813_il2cpp_TypeInfo/* declaring_type */
	, &DefaultTrackableEventHandler_t45_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14446_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2813_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14442_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14443_MethodInfo,
	&InternalEnumerator_1_Dispose_m14444_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14445_MethodInfo,
	&InternalEnumerator_1_get_Current_m14446_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14445_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14444_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2813_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14443_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14445_MethodInfo,
	&InternalEnumerator_1_Dispose_m14444_MethodInfo,
	&InternalEnumerator_1_get_Current_m14446_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2813_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5794_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2813_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5794_il2cpp_TypeInfo, 7},
};
extern TypeInfo DefaultTrackableEventHandler_t45_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2813_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14446_MethodInfo/* Method Usage */,
	&DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisDefaultTrackableEventHandler_t45_m32061_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2813_0_0_0;
extern Il2CppType InternalEnumerator_1_t2813_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2813_GenericClass;
TypeInfo InternalEnumerator_1_t2813_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2813_MethodInfos/* methods */
	, InternalEnumerator_1_t2813_PropertyInfos/* properties */
	, InternalEnumerator_1_t2813_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2813_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2813_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2813_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2813_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2813_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2813_1_0_0/* this_arg */
	, InternalEnumerator_1_t2813_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2813_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2813_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2813)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7362_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>
extern MethodInfo ICollection_1_get_Count_m41844_MethodInfo;
static PropertyInfo ICollection_1_t7362____Count_PropertyInfo = 
{
	&ICollection_1_t7362_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41844_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41845_MethodInfo;
static PropertyInfo ICollection_1_t7362____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7362_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41845_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7362_PropertyInfos[] =
{
	&ICollection_1_t7362____Count_PropertyInfo,
	&ICollection_1_t7362____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41844_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m41844_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7362_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41844_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41845_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41845_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7362_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41845_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
static ParameterInfo ICollection_1_t7362_ICollection_1_Add_m41846_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t45_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41846_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Add(T)
MethodInfo ICollection_1_Add_m41846_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7362_ICollection_1_Add_m41846_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41846_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41847_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Clear()
MethodInfo ICollection_1_Clear_m41847_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41847_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
static ParameterInfo ICollection_1_t7362_ICollection_1_Contains_m41848_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t45_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41848_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m41848_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7362_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7362_ICollection_1_Contains_m41848_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41848_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandlerU5BU5D_t5175_0_0_0;
extern Il2CppType DefaultTrackableEventHandlerU5BU5D_t5175_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7362_ICollection_1_CopyTo_m41849_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandlerU5BU5D_t5175_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41849_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41849_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7362_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7362_ICollection_1_CopyTo_m41849_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41849_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
static ParameterInfo ICollection_1_t7362_ICollection_1_Remove_m41850_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t45_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41850_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.DefaultTrackableEventHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m41850_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7362_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7362_ICollection_1_Remove_m41850_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41850_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7362_MethodInfos[] =
{
	&ICollection_1_get_Count_m41844_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41845_MethodInfo,
	&ICollection_1_Add_m41846_MethodInfo,
	&ICollection_1_Clear_m41847_MethodInfo,
	&ICollection_1_Contains_m41848_MethodInfo,
	&ICollection_1_CopyTo_m41849_MethodInfo,
	&ICollection_1_Remove_m41850_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7364_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7362_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7364_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7362_0_0_0;
extern Il2CppType ICollection_1_t7362_1_0_0;
struct ICollection_1_t7362;
extern Il2CppGenericClass ICollection_1_t7362_GenericClass;
TypeInfo ICollection_1_t7362_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7362_MethodInfos/* methods */
	, ICollection_1_t7362_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7362_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7362_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7362_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7362_0_0_0/* byval_arg */
	, &ICollection_1_t7362_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7362_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DefaultTrackableEventHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.DefaultTrackableEventHandler>
extern Il2CppType IEnumerator_1_t5794_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41851_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.DefaultTrackableEventHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41851_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7364_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5794_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41851_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7364_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41851_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7364_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7364_0_0_0;
extern Il2CppType IEnumerable_1_t7364_1_0_0;
struct IEnumerable_1_t7364;
extern Il2CppGenericClass IEnumerable_1_t7364_GenericClass;
TypeInfo IEnumerable_1_t7364_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7364_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7364_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7364_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7364_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7364_0_0_0/* byval_arg */
	, &IEnumerable_1_t7364_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7364_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7363_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>
extern MethodInfo IList_1_get_Item_m41852_MethodInfo;
extern MethodInfo IList_1_set_Item_m41853_MethodInfo;
static PropertyInfo IList_1_t7363____Item_PropertyInfo = 
{
	&IList_1_t7363_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41852_MethodInfo/* get */
	, &IList_1_set_Item_m41853_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7363_PropertyInfos[] =
{
	&IList_1_t7363____Item_PropertyInfo,
	NULL
};
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
static ParameterInfo IList_1_t7363_IList_1_IndexOf_m41854_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t45_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41854_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41854_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7363_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7363_IList_1_IndexOf_m41854_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41854_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
static ParameterInfo IList_1_t7363_IList_1_Insert_m41855_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t45_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41855_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41855_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7363_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7363_IList_1_Insert_m41855_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41855_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7363_IList_1_RemoveAt_m41856_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41856_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41856_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7363_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7363_IList_1_RemoveAt_m41856_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41856_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7363_IList_1_get_Item_m41852_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41852_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41852_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7363_il2cpp_TypeInfo/* declaring_type */
	, &DefaultTrackableEventHandler_t45_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7363_IList_1_get_Item_m41852_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41852_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
static ParameterInfo IList_1_t7363_IList_1_set_Item_m41853_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t45_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41853_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.DefaultTrackableEventHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41853_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7363_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7363_IList_1_set_Item_m41853_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41853_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7363_MethodInfos[] =
{
	&IList_1_IndexOf_m41854_MethodInfo,
	&IList_1_Insert_m41855_MethodInfo,
	&IList_1_RemoveAt_m41856_MethodInfo,
	&IList_1_get_Item_m41852_MethodInfo,
	&IList_1_set_Item_m41853_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7363_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7362_il2cpp_TypeInfo,
	&IEnumerable_1_t7364_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7363_0_0_0;
extern Il2CppType IList_1_t7363_1_0_0;
struct IList_1_t7363;
extern Il2CppGenericClass IList_1_t7363_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7363_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7363_MethodInfos/* methods */
	, IList_1_t7363_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7363_il2cpp_TypeInfo/* element_class */
	, IList_1_t7363_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7363_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7363_0_0_0/* byval_arg */
	, &IList_1_t7363_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7363_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t3661_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>
extern MethodInfo ICollection_1_get_Count_m41857_MethodInfo;
static PropertyInfo ICollection_1_t3661____Count_PropertyInfo = 
{
	&ICollection_1_t3661_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41857_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41858_MethodInfo;
static PropertyInfo ICollection_1_t3661____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t3661_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41858_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t3661_PropertyInfos[] =
{
	&ICollection_1_t3661____Count_PropertyInfo,
	&ICollection_1_t3661____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41857_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m41857_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t3661_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41857_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41858_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41858_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t3661_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41858_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackableEventHandler_t135_0_0_0;
extern Il2CppType ITrackableEventHandler_t135_0_0_0;
static ParameterInfo ICollection_1_t3661_ICollection_1_Add_m41859_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandler_t135_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41859_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Add(T)
MethodInfo ICollection_1_Add_m41859_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t3661_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t3661_ICollection_1_Add_m41859_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41859_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41860_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Clear()
MethodInfo ICollection_1_Clear_m41860_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t3661_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41860_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackableEventHandler_t135_0_0_0;
static ParameterInfo ICollection_1_t3661_ICollection_1_Contains_m41861_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandler_t135_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41861_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m41861_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t3661_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t3661_ICollection_1_Contains_m41861_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41861_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackableEventHandlerU5BU5D_t3658_0_0_0;
extern Il2CppType ITrackableEventHandlerU5BU5D_t3658_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t3661_ICollection_1_CopyTo_m41862_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandlerU5BU5D_t3658_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41862_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41862_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t3661_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t3661_ICollection_1_CopyTo_m41862_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41862_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackableEventHandler_t135_0_0_0;
static ParameterInfo ICollection_1_t3661_ICollection_1_Remove_m41863_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandler_t135_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41863_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ITrackableEventHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m41863_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t3661_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t3661_ICollection_1_Remove_m41863_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41863_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t3661_MethodInfos[] =
{
	&ICollection_1_get_Count_m41857_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41858_MethodInfo,
	&ICollection_1_Add_m41859_MethodInfo,
	&ICollection_1_Clear_m41860_MethodInfo,
	&ICollection_1_Contains_m41861_MethodInfo,
	&ICollection_1_CopyTo_m41862_MethodInfo,
	&ICollection_1_Remove_m41863_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t3659_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t3661_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t3659_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t3661_0_0_0;
extern Il2CppType ICollection_1_t3661_1_0_0;
struct ICollection_1_t3661;
extern Il2CppGenericClass ICollection_1_t3661_GenericClass;
TypeInfo ICollection_1_t3661_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t3661_MethodInfos/* methods */
	, ICollection_1_t3661_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t3661_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t3661_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t3661_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t3661_0_0_0/* byval_arg */
	, &ICollection_1_t3661_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t3661_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ITrackableEventHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ITrackableEventHandler>
extern Il2CppType IEnumerator_1_t3660_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41864_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ITrackableEventHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41864_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t3659_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t3660_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41864_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t3659_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41864_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t3659_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t3659_0_0_0;
extern Il2CppType IEnumerable_1_t3659_1_0_0;
struct IEnumerable_1_t3659;
extern Il2CppGenericClass IEnumerable_1_t3659_GenericClass;
TypeInfo IEnumerable_1_t3659_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t3659_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t3659_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t3659_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t3659_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t3659_0_0_0/* byval_arg */
	, &IEnumerable_1_t3659_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t3659_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t3660_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.ITrackableEventHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ITrackableEventHandler>
extern MethodInfo IEnumerator_1_get_Current_m41865_MethodInfo;
static PropertyInfo IEnumerator_1_t3660____Current_PropertyInfo = 
{
	&IEnumerator_1_t3660_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41865_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t3660_PropertyInfos[] =
{
	&IEnumerator_1_t3660____Current_PropertyInfo,
	NULL
};
extern Il2CppType ITrackableEventHandler_t135_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41865_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ITrackableEventHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41865_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t3660_il2cpp_TypeInfo/* declaring_type */
	, &ITrackableEventHandler_t135_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41865_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t3660_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41865_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t3660_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t3660_0_0_0;
extern Il2CppType IEnumerator_1_t3660_1_0_0;
struct IEnumerator_1_t3660;
extern Il2CppGenericClass IEnumerator_1_t3660_GenericClass;
TypeInfo IEnumerator_1_t3660_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t3660_MethodInfos/* methods */
	, IEnumerator_1_t3660_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t3660_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t3660_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t3660_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t3660_0_0_0/* byval_arg */
	, &IEnumerator_1_t3660_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t3660_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_55.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2814_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_55MethodDeclarations.h"

extern TypeInfo ITrackableEventHandler_t135_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14451_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisITrackableEventHandler_t135_m32072_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ITrackableEventHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ITrackableEventHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisITrackableEventHandler_t135_m32072(__this, p0, method) (Object_t *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2814____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2814_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2814, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2814____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2814_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2814, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2814_FieldInfos[] =
{
	&InternalEnumerator_1_t2814____array_0_FieldInfo,
	&InternalEnumerator_1_t2814____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14448_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2814____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2814_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14448_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2814____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2814_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14451_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2814_PropertyInfos[] =
{
	&InternalEnumerator_1_t2814____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2814____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2814_InternalEnumerator_1__ctor_m14447_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14447_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14447_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2814_InternalEnumerator_1__ctor_m14447_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14447_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14448_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14448_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2814_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14448_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14449_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14449_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2814_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14449_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14450_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14450_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2814_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14450_GenericMethod/* genericMethod */

};
extern Il2CppType ITrackableEventHandler_t135_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14451_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ITrackableEventHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14451_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2814_il2cpp_TypeInfo/* declaring_type */
	, &ITrackableEventHandler_t135_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14451_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2814_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14447_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14448_MethodInfo,
	&InternalEnumerator_1_Dispose_m14449_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14450_MethodInfo,
	&InternalEnumerator_1_get_Current_m14451_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14450_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14449_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2814_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14448_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14450_MethodInfo,
	&InternalEnumerator_1_Dispose_m14449_MethodInfo,
	&InternalEnumerator_1_get_Current_m14451_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2814_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t3660_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2814_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t3660_il2cpp_TypeInfo, 7},
};
extern TypeInfo ITrackableEventHandler_t135_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2814_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14451_MethodInfo/* Method Usage */,
	&ITrackableEventHandler_t135_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisITrackableEventHandler_t135_m32072_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2814_0_0_0;
extern Il2CppType InternalEnumerator_1_t2814_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2814_GenericClass;
TypeInfo InternalEnumerator_1_t2814_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2814_MethodInfos/* methods */
	, InternalEnumerator_1_t2814_PropertyInfos/* properties */
	, InternalEnumerator_1_t2814_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2814_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2814_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2814_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2814_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2814_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2814_1_0_0/* this_arg */
	, InternalEnumerator_1_t2814_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2814_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2814_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2814)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t3665_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>
extern MethodInfo IList_1_get_Item_m41866_MethodInfo;
extern MethodInfo IList_1_set_Item_m41867_MethodInfo;
static PropertyInfo IList_1_t3665____Item_PropertyInfo = 
{
	&IList_1_t3665_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41866_MethodInfo/* get */
	, &IList_1_set_Item_m41867_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t3665_PropertyInfos[] =
{
	&IList_1_t3665____Item_PropertyInfo,
	NULL
};
extern Il2CppType ITrackableEventHandler_t135_0_0_0;
static ParameterInfo IList_1_t3665_IList_1_IndexOf_m41868_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandler_t135_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41868_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41868_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t3665_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t3665_IList_1_IndexOf_m41868_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41868_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ITrackableEventHandler_t135_0_0_0;
static ParameterInfo IList_1_t3665_IList_1_Insert_m41869_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandler_t135_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41869_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41869_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t3665_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t3665_IList_1_Insert_m41869_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41869_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t3665_IList_1_RemoveAt_m41870_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41870_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41870_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t3665_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t3665_IList_1_RemoveAt_m41870_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41870_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t3665_IList_1_get_Item_m41866_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ITrackableEventHandler_t135_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41866_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41866_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t3665_il2cpp_TypeInfo/* declaring_type */
	, &ITrackableEventHandler_t135_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t3665_IList_1_get_Item_m41866_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41866_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ITrackableEventHandler_t135_0_0_0;
static ParameterInfo IList_1_t3665_IList_1_set_Item_m41867_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ITrackableEventHandler_t135_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41867_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ITrackableEventHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41867_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t3665_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t3665_IList_1_set_Item_m41867_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41867_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t3665_MethodInfos[] =
{
	&IList_1_IndexOf_m41868_MethodInfo,
	&IList_1_Insert_m41869_MethodInfo,
	&IList_1_RemoveAt_m41870_MethodInfo,
	&IList_1_get_Item_m41866_MethodInfo,
	&IList_1_set_Item_m41867_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t3665_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t3661_il2cpp_TypeInfo,
	&IEnumerable_1_t3659_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t3665_0_0_0;
extern Il2CppType IList_1_t3665_1_0_0;
struct IList_1_t3665;
extern Il2CppGenericClass IList_1_t3665_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t3665_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t3665_MethodInfos/* methods */
	, IList_1_t3665_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t3665_il2cpp_TypeInfo/* element_class */
	, IList_1_t3665_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t3665_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t3665_0_0_0/* byval_arg */
	, &IList_1_t3665_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t3665_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_21.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2815_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_21MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_17.h"
extern TypeInfo InvokableCall_1_t2816_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_17MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14454_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14456_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2815____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2815_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2815, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2815_FieldInfos[] =
{
	&CachedInvokableCall_1_t2815____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2815_CachedInvokableCall_1__ctor_m14452_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t45_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14452_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14452_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2815_CachedInvokableCall_1__ctor_m14452_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14452_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2815_CachedInvokableCall_1_Invoke_m14453_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14453_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.DefaultTrackableEventHandler>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14453_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2815_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2815_CachedInvokableCall_1_Invoke_m14453_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14453_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2815_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14452_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14453_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14453_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14457_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2815_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14453_MethodInfo,
	&InvokableCall_1_Find_m14457_MethodInfo,
};
extern Il2CppType UnityAction_1_t2817_0_0_0;
extern TypeInfo UnityAction_1_t2817_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisDefaultTrackableEventHandler_t45_m32082_MethodInfo;
extern TypeInfo DefaultTrackableEventHandler_t45_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14459_MethodInfo;
extern TypeInfo DefaultTrackableEventHandler_t45_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2815_RGCTXData[8] = 
{
	&UnityAction_1_t2817_0_0_0/* Type Usage */,
	&UnityAction_1_t2817_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDefaultTrackableEventHandler_t45_m32082_MethodInfo/* Method Usage */,
	&DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14459_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14454_MethodInfo/* Method Usage */,
	&DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14456_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2815_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2815_1_0_0;
struct CachedInvokableCall_1_t2815;
extern Il2CppGenericClass CachedInvokableCall_1_t2815_GenericClass;
TypeInfo CachedInvokableCall_1_t2815_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2815_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2815_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2816_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2815_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2815_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2815_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2815_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2815_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2815_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2815_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2815)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_24.h"
extern TypeInfo UnityAction_1_t2817_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_24MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DefaultTrackableEventHandler>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.DefaultTrackableEventHandler>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisDefaultTrackableEventHandler_t45_m32082(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>
extern Il2CppType UnityAction_1_t2817_0_0_1;
FieldInfo InvokableCall_1_t2816____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2817_0_0_1/* type */
	, &InvokableCall_1_t2816_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2816, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2816_FieldInfos[] =
{
	&InvokableCall_1_t2816____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2816_InvokableCall_1__ctor_m14454_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14454_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14454_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2816_InvokableCall_1__ctor_m14454_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14454_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2817_0_0_0;
static ParameterInfo InvokableCall_1_t2816_InvokableCall_1__ctor_m14455_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2817_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14455_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14455_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2816_InvokableCall_1__ctor_m14455_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14455_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2816_InvokableCall_1_Invoke_m14456_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14456_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14456_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2816_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2816_InvokableCall_1_Invoke_m14456_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14456_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2816_InvokableCall_1_Find_m14457_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14457_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.DefaultTrackableEventHandler>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14457_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2816_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2816_InvokableCall_1_Find_m14457_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14457_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2816_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14454_MethodInfo,
	&InvokableCall_1__ctor_m14455_MethodInfo,
	&InvokableCall_1_Invoke_m14456_MethodInfo,
	&InvokableCall_1_Find_m14457_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2816_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14456_MethodInfo,
	&InvokableCall_1_Find_m14457_MethodInfo,
};
extern TypeInfo UnityAction_1_t2817_il2cpp_TypeInfo;
extern TypeInfo DefaultTrackableEventHandler_t45_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2816_RGCTXData[5] = 
{
	&UnityAction_1_t2817_0_0_0/* Type Usage */,
	&UnityAction_1_t2817_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisDefaultTrackableEventHandler_t45_m32082_MethodInfo/* Method Usage */,
	&DefaultTrackableEventHandler_t45_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14459_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2816_0_0_0;
extern Il2CppType InvokableCall_1_t2816_1_0_0;
struct InvokableCall_1_t2816;
extern Il2CppGenericClass InvokableCall_1_t2816_GenericClass;
TypeInfo InvokableCall_1_t2816_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2816_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2816_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2816_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2816_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2816_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2816_0_0_0/* byval_arg */
	, &InvokableCall_1_t2816_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2816_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2816_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2816)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2817_UnityAction_1__ctor_m14458_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14458_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14458_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2817_UnityAction_1__ctor_m14458_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14458_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
static ParameterInfo UnityAction_1_t2817_UnityAction_1_Invoke_m14459_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t45_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14459_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14459_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2817_UnityAction_1_Invoke_m14459_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14459_GenericMethod/* genericMethod */

};
extern Il2CppType DefaultTrackableEventHandler_t45_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2817_UnityAction_1_BeginInvoke_m14460_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &DefaultTrackableEventHandler_t45_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14460_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14460_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2817_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2817_UnityAction_1_BeginInvoke_m14460_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14460_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2817_UnityAction_1_EndInvoke_m14461_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14461_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.DefaultTrackableEventHandler>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14461_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2817_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2817_UnityAction_1_EndInvoke_m14461_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14461_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2817_MethodInfos[] =
{
	&UnityAction_1__ctor_m14458_MethodInfo,
	&UnityAction_1_Invoke_m14459_MethodInfo,
	&UnityAction_1_BeginInvoke_m14460_MethodInfo,
	&UnityAction_1_EndInvoke_m14461_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14460_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14461_MethodInfo;
static MethodInfo* UnityAction_1_t2817_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14459_MethodInfo,
	&UnityAction_1_BeginInvoke_m14460_MethodInfo,
	&UnityAction_1_EndInvoke_m14461_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2817_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2817_1_0_0;
struct UnityAction_1_t2817;
extern Il2CppGenericClass UnityAction_1_t2817_GenericClass;
TypeInfo UnityAction_1_t2817_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2817_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2817_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2817_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2817_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2817_0_0_0/* byval_arg */
	, &UnityAction_1_t2817_1_0_0/* this_arg */
	, UnityAction_1_t2817_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2817_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2817)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
// UnityEngine.CastHelper`1<Vuforia.TrackableBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_5.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CastHelper_1_t2818_il2cpp_TypeInfo;
// UnityEngine.CastHelper`1<Vuforia.TrackableBehaviour>
#include "UnityEngine_UnityEngine_CastHelper_1_gen_5MethodDeclarations.h"



// Metadata Definition UnityEngine.CastHelper`1<Vuforia.TrackableBehaviour>
extern Il2CppType TrackableBehaviour_t44_0_0_6;
FieldInfo CastHelper_1_t2818____t_0_FieldInfo = 
{
	"t"/* name */
	, &TrackableBehaviour_t44_0_0_6/* type */
	, &CastHelper_1_t2818_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2818, ___t_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType IntPtr_t121_0_0_6;
FieldInfo CastHelper_1_t2818____onePointerFurtherThanT_1_FieldInfo = 
{
	"onePointerFurtherThanT"/* name */
	, &IntPtr_t121_0_0_6/* type */
	, &CastHelper_1_t2818_il2cpp_TypeInfo/* parent */
	, offsetof(CastHelper_1_t2818, ___onePointerFurtherThanT_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CastHelper_1_t2818_FieldInfos[] =
{
	&CastHelper_1_t2818____t_0_FieldInfo,
	&CastHelper_1_t2818____onePointerFurtherThanT_1_FieldInfo,
	NULL
};
static MethodInfo* CastHelper_1_t2818_MethodInfos[] =
{
	NULL
};
static MethodInfo* CastHelper_1_t2818_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CastHelper_1_t2818_0_0_0;
extern Il2CppType CastHelper_1_t2818_1_0_0;
extern Il2CppGenericClass CastHelper_1_t2818_GenericClass;
TypeInfo CastHelper_1_t2818_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CastHelper`1"/* name */
	, "UnityEngine"/* namespaze */
	, CastHelper_1_t2818_MethodInfos/* methods */
	, NULL/* properties */
	, CastHelper_1_t2818_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CastHelper_1_t2818_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CastHelper_1_t2818_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CastHelper_1_t2818_il2cpp_TypeInfo/* cast_class */
	, &CastHelper_1_t2818_0_0_0/* byval_arg */
	, &CastHelper_1_t2818_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CastHelper_1_t2818_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CastHelper_1_t2818)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048840/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 0/* method_count */
	, 0/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 4/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5797_il2cpp_TypeInfo;

// UnityEngine.Collider
#include "UnityEngine_UnityEngine_Collider.h"


// T System.Collections.Generic.IEnumerator`1<UnityEngine.Collider>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<UnityEngine.Collider>
extern MethodInfo IEnumerator_1_get_Current_m41871_MethodInfo;
static PropertyInfo IEnumerator_1_t5797____Current_PropertyInfo = 
{
	&IEnumerator_1_t5797_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41871_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5797_PropertyInfos[] =
{
	&IEnumerator_1_t5797____Current_PropertyInfo,
	NULL
};
extern Il2CppType Collider_t132_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41871_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<UnityEngine.Collider>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41871_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5797_il2cpp_TypeInfo/* declaring_type */
	, &Collider_t132_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41871_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5797_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41871_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5797_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5797_0_0_0;
extern Il2CppType IEnumerator_1_t5797_1_0_0;
struct IEnumerator_1_t5797;
extern Il2CppGenericClass IEnumerator_1_t5797_GenericClass;
TypeInfo IEnumerator_1_t5797_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5797_MethodInfos/* methods */
	, IEnumerator_1_t5797_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5797_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5797_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5797_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5797_0_0_0/* byval_arg */
	, &IEnumerator_1_t5797_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5797_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<UnityEngine.Collider>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_56.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2819_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<UnityEngine.Collider>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_56MethodDeclarations.h"

extern TypeInfo Collider_t132_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14466_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisCollider_t132_m32084_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<UnityEngine.Collider>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Collider>(System.Int32)
#define Array_InternalArray__get_Item_TisCollider_t132_m32084(__this, p0, method) (Collider_t132 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<UnityEngine.Collider>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Collider>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Collider>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Collider>::MoveNext()
// T System.Array/InternalEnumerator`1<UnityEngine.Collider>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<UnityEngine.Collider>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2819____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2819_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2819, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2819____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2819_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2819, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2819_FieldInfos[] =
{
	&InternalEnumerator_1_t2819____array_0_FieldInfo,
	&InternalEnumerator_1_t2819____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14463_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2819____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2819_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14463_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2819____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2819_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14466_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2819_PropertyInfos[] =
{
	&InternalEnumerator_1_t2819____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2819____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2819_InternalEnumerator_1__ctor_m14462_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14462_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Collider>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14462_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2819_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2819_InternalEnumerator_1__ctor_m14462_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14462_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14463_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Collider>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14463_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2819_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14463_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14464_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Collider>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14464_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2819_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14464_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14465_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Collider>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14465_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2819_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14465_GenericMethod/* genericMethod */

};
extern Il2CppType Collider_t132_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14466_GenericMethod;
// T System.Array/InternalEnumerator`1<UnityEngine.Collider>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14466_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2819_il2cpp_TypeInfo/* declaring_type */
	, &Collider_t132_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14466_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2819_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14462_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14463_MethodInfo,
	&InternalEnumerator_1_Dispose_m14464_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14465_MethodInfo,
	&InternalEnumerator_1_get_Current_m14466_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14465_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14464_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2819_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14463_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14465_MethodInfo,
	&InternalEnumerator_1_Dispose_m14464_MethodInfo,
	&InternalEnumerator_1_get_Current_m14466_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2819_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5797_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2819_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5797_il2cpp_TypeInfo, 7},
};
extern TypeInfo Collider_t132_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2819_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14466_MethodInfo/* Method Usage */,
	&Collider_t132_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisCollider_t132_m32084_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2819_0_0_0;
extern Il2CppType InternalEnumerator_1_t2819_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2819_GenericClass;
TypeInfo InternalEnumerator_1_t2819_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2819_MethodInfos/* methods */
	, InternalEnumerator_1_t2819_PropertyInfos/* properties */
	, InternalEnumerator_1_t2819_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2819_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2819_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2819_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2819_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2819_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2819_1_0_0/* this_arg */
	, InternalEnumerator_1_t2819_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2819_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2819_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2819)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7365_il2cpp_TypeInfo;

#include "UnityEngine_ArrayTypes.h"


// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Collider>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Collider>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Collider>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<UnityEngine.Collider>
extern MethodInfo ICollection_1_get_Count_m41872_MethodInfo;
static PropertyInfo ICollection_1_t7365____Count_PropertyInfo = 
{
	&ICollection_1_t7365_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41872_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41873_MethodInfo;
static PropertyInfo ICollection_1_t7365____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7365_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41873_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7365_PropertyInfos[] =
{
	&ICollection_1_t7365____Count_PropertyInfo,
	&ICollection_1_t7365____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41872_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<UnityEngine.Collider>::get_Count()
MethodInfo ICollection_1_get_Count_m41872_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7365_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41872_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41873_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Collider>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41873_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7365_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41873_GenericMethod/* genericMethod */

};
extern Il2CppType Collider_t132_0_0_0;
extern Il2CppType Collider_t132_0_0_0;
static ParameterInfo ICollection_1_t7365_ICollection_1_Add_m41874_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Collider_t132_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41874_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Add(T)
MethodInfo ICollection_1_Add_m41874_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7365_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7365_ICollection_1_Add_m41874_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41874_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41875_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Clear()
MethodInfo ICollection_1_Clear_m41875_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7365_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41875_GenericMethod/* genericMethod */

};
extern Il2CppType Collider_t132_0_0_0;
static ParameterInfo ICollection_1_t7365_ICollection_1_Contains_m41876_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Collider_t132_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41876_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Contains(T)
MethodInfo ICollection_1_Contains_m41876_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7365_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7365_ICollection_1_Contains_m41876_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41876_GenericMethod/* genericMethod */

};
extern Il2CppType ColliderU5BU5D_t133_0_0_0;
extern Il2CppType ColliderU5BU5D_t133_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7365_ICollection_1_CopyTo_m41877_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ColliderU5BU5D_t133_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41877_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<UnityEngine.Collider>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41877_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7365_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7365_ICollection_1_CopyTo_m41877_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41877_GenericMethod/* genericMethod */

};
extern Il2CppType Collider_t132_0_0_0;
static ParameterInfo ICollection_1_t7365_ICollection_1_Remove_m41878_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Collider_t132_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41878_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<UnityEngine.Collider>::Remove(T)
MethodInfo ICollection_1_Remove_m41878_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7365_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7365_ICollection_1_Remove_m41878_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41878_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7365_MethodInfos[] =
{
	&ICollection_1_get_Count_m41872_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41873_MethodInfo,
	&ICollection_1_Add_m41874_MethodInfo,
	&ICollection_1_Clear_m41875_MethodInfo,
	&ICollection_1_Contains_m41876_MethodInfo,
	&ICollection_1_CopyTo_m41877_MethodInfo,
	&ICollection_1_Remove_m41878_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7367_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7365_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7367_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7365_0_0_0;
extern Il2CppType ICollection_1_t7365_1_0_0;
struct ICollection_1_t7365;
extern Il2CppGenericClass ICollection_1_t7365_GenericClass;
TypeInfo ICollection_1_t7365_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7365_MethodInfos/* methods */
	, ICollection_1_t7365_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7365_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7365_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7365_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7365_0_0_0/* byval_arg */
	, &ICollection_1_t7365_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7365_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Collider>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<UnityEngine.Collider>
extern Il2CppType IEnumerator_1_t5797_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41879_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<UnityEngine.Collider>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41879_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7367_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5797_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41879_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7367_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41879_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7367_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7367_0_0_0;
extern Il2CppType IEnumerable_1_t7367_1_0_0;
struct IEnumerable_1_t7367;
extern Il2CppGenericClass IEnumerable_1_t7367_GenericClass;
TypeInfo IEnumerable_1_t7367_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7367_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7367_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7367_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7367_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7367_0_0_0/* byval_arg */
	, &IEnumerable_1_t7367_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7367_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7366_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Collider>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Collider>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Collider>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<UnityEngine.Collider>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<UnityEngine.Collider>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<UnityEngine.Collider>
extern MethodInfo IList_1_get_Item_m41880_MethodInfo;
extern MethodInfo IList_1_set_Item_m41881_MethodInfo;
static PropertyInfo IList_1_t7366____Item_PropertyInfo = 
{
	&IList_1_t7366_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41880_MethodInfo/* get */
	, &IList_1_set_Item_m41881_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7366_PropertyInfos[] =
{
	&IList_1_t7366____Item_PropertyInfo,
	NULL
};
extern Il2CppType Collider_t132_0_0_0;
static ParameterInfo IList_1_t7366_IList_1_IndexOf_m41882_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &Collider_t132_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41882_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<UnityEngine.Collider>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41882_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7366_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7366_IList_1_IndexOf_m41882_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41882_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Collider_t132_0_0_0;
static ParameterInfo IList_1_t7366_IList_1_Insert_m41883_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &Collider_t132_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41883_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Collider>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41883_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7366_IList_1_Insert_m41883_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41883_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7366_IList_1_RemoveAt_m41884_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41884_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Collider>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41884_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7366_IList_1_RemoveAt_m41884_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41884_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7366_IList_1_get_Item_m41880_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Collider_t132_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41880_GenericMethod;
// T System.Collections.Generic.IList`1<UnityEngine.Collider>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41880_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7366_il2cpp_TypeInfo/* declaring_type */
	, &Collider_t132_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7366_IList_1_get_Item_m41880_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41880_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType Collider_t132_0_0_0;
static ParameterInfo IList_1_t7366_IList_1_set_Item_m41881_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &Collider_t132_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41881_GenericMethod;
// System.Void System.Collections.Generic.IList`1<UnityEngine.Collider>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41881_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7366_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7366_IList_1_set_Item_m41881_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41881_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7366_MethodInfos[] =
{
	&IList_1_IndexOf_m41882_MethodInfo,
	&IList_1_Insert_m41883_MethodInfo,
	&IList_1_RemoveAt_m41884_MethodInfo,
	&IList_1_get_Item_m41880_MethodInfo,
	&IList_1_set_Item_m41881_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7366_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7365_il2cpp_TypeInfo,
	&IEnumerable_1_t7367_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7366_0_0_0;
extern Il2CppType IList_1_t7366_1_0_0;
struct IList_1_t7366;
extern Il2CppGenericClass IList_1_t7366_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7366_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7366_MethodInfos/* methods */
	, IList_1_t7366_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7366_il2cpp_TypeInfo/* element_class */
	, IList_1_t7366_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7366_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7366_0_0_0/* byval_arg */
	, &IList_1_t7366_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7366_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5799_il2cpp_TypeInfo;

// Vuforia.GLErrorHandler
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.GLErrorHandler>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.GLErrorHandler>
extern MethodInfo IEnumerator_1_get_Current_m41885_MethodInfo;
static PropertyInfo IEnumerator_1_t5799____Current_PropertyInfo = 
{
	&IEnumerator_1_t5799_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41885_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5799_PropertyInfos[] =
{
	&IEnumerator_1_t5799____Current_PropertyInfo,
	NULL
};
extern Il2CppType GLErrorHandler_t46_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41885_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.GLErrorHandler>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41885_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5799_il2cpp_TypeInfo/* declaring_type */
	, &GLErrorHandler_t46_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41885_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5799_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41885_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5799_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5799_0_0_0;
extern Il2CppType IEnumerator_1_t5799_1_0_0;
struct IEnumerator_1_t5799;
extern Il2CppGenericClass IEnumerator_1_t5799_GenericClass;
TypeInfo IEnumerator_1_t5799_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5799_MethodInfos/* methods */
	, IEnumerator_1_t5799_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5799_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5799_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5799_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5799_0_0_0/* byval_arg */
	, &IEnumerator_1_t5799_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5799_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_57.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2820_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_57MethodDeclarations.h"

extern TypeInfo GLErrorHandler_t46_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14471_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisGLErrorHandler_t46_m32096_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.GLErrorHandler>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.GLErrorHandler>(System.Int32)
#define Array_InternalArray__get_Item_TisGLErrorHandler_t46_m32096(__this, p0, method) (GLErrorHandler_t46 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2820____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2820_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2820, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2820____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2820_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2820, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2820_FieldInfos[] =
{
	&InternalEnumerator_1_t2820____array_0_FieldInfo,
	&InternalEnumerator_1_t2820____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14468_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2820____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2820_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14468_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2820____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2820_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14471_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2820_PropertyInfos[] =
{
	&InternalEnumerator_1_t2820____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2820____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2820_InternalEnumerator_1__ctor_m14467_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14467_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14467_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2820_InternalEnumerator_1__ctor_m14467_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14467_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14468_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14468_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2820_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14468_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14469_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14469_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2820_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14469_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14470_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14470_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2820_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14470_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandler_t46_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14471_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.GLErrorHandler>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14471_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2820_il2cpp_TypeInfo/* declaring_type */
	, &GLErrorHandler_t46_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14471_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2820_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14467_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14468_MethodInfo,
	&InternalEnumerator_1_Dispose_m14469_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14470_MethodInfo,
	&InternalEnumerator_1_get_Current_m14471_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14470_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14469_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2820_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14468_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14470_MethodInfo,
	&InternalEnumerator_1_Dispose_m14469_MethodInfo,
	&InternalEnumerator_1_get_Current_m14471_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2820_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5799_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2820_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5799_il2cpp_TypeInfo, 7},
};
extern TypeInfo GLErrorHandler_t46_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2820_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14471_MethodInfo/* Method Usage */,
	&GLErrorHandler_t46_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisGLErrorHandler_t46_m32096_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2820_0_0_0;
extern Il2CppType InternalEnumerator_1_t2820_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2820_GenericClass;
TypeInfo InternalEnumerator_1_t2820_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2820_MethodInfos/* methods */
	, InternalEnumerator_1_t2820_PropertyInfos/* properties */
	, InternalEnumerator_1_t2820_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2820_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2820_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2820_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2820_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2820_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2820_1_0_0/* this_arg */
	, InternalEnumerator_1_t2820_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2820_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2820_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2820)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7368_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>
extern MethodInfo ICollection_1_get_Count_m41886_MethodInfo;
static PropertyInfo ICollection_1_t7368____Count_PropertyInfo = 
{
	&ICollection_1_t7368_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41886_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41887_MethodInfo;
static PropertyInfo ICollection_1_t7368____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7368_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41887_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7368_PropertyInfos[] =
{
	&ICollection_1_t7368____Count_PropertyInfo,
	&ICollection_1_t7368____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41886_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::get_Count()
MethodInfo ICollection_1_get_Count_m41886_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7368_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41886_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41887_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41887_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7368_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41887_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandler_t46_0_0_0;
extern Il2CppType GLErrorHandler_t46_0_0_0;
static ParameterInfo ICollection_1_t7368_ICollection_1_Add_m41888_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t46_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41888_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Add(T)
MethodInfo ICollection_1_Add_m41888_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7368_ICollection_1_Add_m41888_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41888_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41889_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Clear()
MethodInfo ICollection_1_Clear_m41889_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41889_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandler_t46_0_0_0;
static ParameterInfo ICollection_1_t7368_ICollection_1_Contains_m41890_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t46_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41890_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Contains(T)
MethodInfo ICollection_1_Contains_m41890_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7368_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7368_ICollection_1_Contains_m41890_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41890_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandlerU5BU5D_t5176_0_0_0;
extern Il2CppType GLErrorHandlerU5BU5D_t5176_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7368_ICollection_1_CopyTo_m41891_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandlerU5BU5D_t5176_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41891_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41891_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7368_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7368_ICollection_1_CopyTo_m41891_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41891_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandler_t46_0_0_0;
static ParameterInfo ICollection_1_t7368_ICollection_1_Remove_m41892_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t46_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41892_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.GLErrorHandler>::Remove(T)
MethodInfo ICollection_1_Remove_m41892_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7368_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7368_ICollection_1_Remove_m41892_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41892_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7368_MethodInfos[] =
{
	&ICollection_1_get_Count_m41886_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41887_MethodInfo,
	&ICollection_1_Add_m41888_MethodInfo,
	&ICollection_1_Clear_m41889_MethodInfo,
	&ICollection_1_Contains_m41890_MethodInfo,
	&ICollection_1_CopyTo_m41891_MethodInfo,
	&ICollection_1_Remove_m41892_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7370_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7368_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7370_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7368_0_0_0;
extern Il2CppType ICollection_1_t7368_1_0_0;
struct ICollection_1_t7368;
extern Il2CppGenericClass ICollection_1_t7368_GenericClass;
TypeInfo ICollection_1_t7368_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7368_MethodInfos/* methods */
	, ICollection_1_t7368_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7368_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7368_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7368_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7368_0_0_0/* byval_arg */
	, &ICollection_1_t7368_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7368_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.GLErrorHandler>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.GLErrorHandler>
extern Il2CppType IEnumerator_1_t5799_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41893_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.GLErrorHandler>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41893_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7370_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5799_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41893_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7370_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41893_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7370_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7370_0_0_0;
extern Il2CppType IEnumerable_1_t7370_1_0_0;
struct IEnumerable_1_t7370;
extern Il2CppGenericClass IEnumerable_1_t7370_GenericClass;
TypeInfo IEnumerable_1_t7370_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7370_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7370_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7370_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7370_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7370_0_0_0/* byval_arg */
	, &IEnumerable_1_t7370_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7370_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7369_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>
extern MethodInfo IList_1_get_Item_m41894_MethodInfo;
extern MethodInfo IList_1_set_Item_m41895_MethodInfo;
static PropertyInfo IList_1_t7369____Item_PropertyInfo = 
{
	&IList_1_t7369_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41894_MethodInfo/* get */
	, &IList_1_set_Item_m41895_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7369_PropertyInfos[] =
{
	&IList_1_t7369____Item_PropertyInfo,
	NULL
};
extern Il2CppType GLErrorHandler_t46_0_0_0;
static ParameterInfo IList_1_t7369_IList_1_IndexOf_m41896_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t46_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41896_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41896_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7369_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7369_IList_1_IndexOf_m41896_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41896_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType GLErrorHandler_t46_0_0_0;
static ParameterInfo IList_1_t7369_IList_1_Insert_m41897_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t46_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41897_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41897_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7369_IList_1_Insert_m41897_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41897_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7369_IList_1_RemoveAt_m41898_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41898_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41898_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7369_IList_1_RemoveAt_m41898_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41898_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7369_IList_1_get_Item_m41894_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType GLErrorHandler_t46_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41894_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41894_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7369_il2cpp_TypeInfo/* declaring_type */
	, &GLErrorHandler_t46_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7369_IList_1_get_Item_m41894_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41894_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType GLErrorHandler_t46_0_0_0;
static ParameterInfo IList_1_t7369_IList_1_set_Item_m41895_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t46_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41895_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.GLErrorHandler>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41895_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7369_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7369_IList_1_set_Item_m41895_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41895_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7369_MethodInfos[] =
{
	&IList_1_IndexOf_m41896_MethodInfo,
	&IList_1_Insert_m41897_MethodInfo,
	&IList_1_RemoveAt_m41898_MethodInfo,
	&IList_1_get_Item_m41894_MethodInfo,
	&IList_1_set_Item_m41895_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7369_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7368_il2cpp_TypeInfo,
	&IEnumerable_1_t7370_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7369_0_0_0;
extern Il2CppType IList_1_t7369_1_0_0;
struct IList_1_t7369;
extern Il2CppGenericClass IList_1_t7369_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7369_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7369_MethodInfos/* methods */
	, IList_1_t7369_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7369_il2cpp_TypeInfo/* element_class */
	, IList_1_t7369_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7369_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7369_0_0_0/* byval_arg */
	, &IList_1_t7369_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7369_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_22.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2821_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_22MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_18.h"
extern TypeInfo InvokableCall_1_t2822_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_18MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14474_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14476_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2821____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2821_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2821, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2821_FieldInfos[] =
{
	&CachedInvokableCall_1_t2821____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType GLErrorHandler_t46_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2821_CachedInvokableCall_1__ctor_m14472_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t46_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14472_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14472_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2821_CachedInvokableCall_1__ctor_m14472_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14472_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2821_CachedInvokableCall_1_Invoke_m14473_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14473_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.GLErrorHandler>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14473_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2821_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2821_CachedInvokableCall_1_Invoke_m14473_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14473_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2821_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14472_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14473_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14473_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14477_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2821_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14473_MethodInfo,
	&InvokableCall_1_Find_m14477_MethodInfo,
};
extern Il2CppType UnityAction_1_t2823_0_0_0;
extern TypeInfo UnityAction_1_t2823_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisGLErrorHandler_t46_m32106_MethodInfo;
extern TypeInfo GLErrorHandler_t46_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14479_MethodInfo;
extern TypeInfo GLErrorHandler_t46_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2821_RGCTXData[8] = 
{
	&UnityAction_1_t2823_0_0_0/* Type Usage */,
	&UnityAction_1_t2823_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGLErrorHandler_t46_m32106_MethodInfo/* Method Usage */,
	&GLErrorHandler_t46_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14479_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14474_MethodInfo/* Method Usage */,
	&GLErrorHandler_t46_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14476_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2821_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2821_1_0_0;
struct CachedInvokableCall_1_t2821;
extern Il2CppGenericClass CachedInvokableCall_1_t2821_GenericClass;
TypeInfo CachedInvokableCall_1_t2821_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2821_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2821_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2822_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2821_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2821_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2821_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2821_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2821_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2821_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2821_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2821)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_25.h"
extern TypeInfo UnityAction_1_t2823_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_25MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.GLErrorHandler>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.GLErrorHandler>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisGLErrorHandler_t46_m32106(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>
extern Il2CppType UnityAction_1_t2823_0_0_1;
FieldInfo InvokableCall_1_t2822____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2823_0_0_1/* type */
	, &InvokableCall_1_t2822_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2822, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2822_FieldInfos[] =
{
	&InvokableCall_1_t2822____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2822_InvokableCall_1__ctor_m14474_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14474_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14474_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2822_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2822_InvokableCall_1__ctor_m14474_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14474_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2823_0_0_0;
static ParameterInfo InvokableCall_1_t2822_InvokableCall_1__ctor_m14475_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2823_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14475_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14475_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2822_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2822_InvokableCall_1__ctor_m14475_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14475_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2822_InvokableCall_1_Invoke_m14476_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14476_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14476_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2822_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2822_InvokableCall_1_Invoke_m14476_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14476_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2822_InvokableCall_1_Find_m14477_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14477_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.GLErrorHandler>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14477_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2822_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2822_InvokableCall_1_Find_m14477_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14477_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2822_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14474_MethodInfo,
	&InvokableCall_1__ctor_m14475_MethodInfo,
	&InvokableCall_1_Invoke_m14476_MethodInfo,
	&InvokableCall_1_Find_m14477_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2822_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14476_MethodInfo,
	&InvokableCall_1_Find_m14477_MethodInfo,
};
extern TypeInfo UnityAction_1_t2823_il2cpp_TypeInfo;
extern TypeInfo GLErrorHandler_t46_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2822_RGCTXData[5] = 
{
	&UnityAction_1_t2823_0_0_0/* Type Usage */,
	&UnityAction_1_t2823_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisGLErrorHandler_t46_m32106_MethodInfo/* Method Usage */,
	&GLErrorHandler_t46_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14479_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2822_0_0_0;
extern Il2CppType InvokableCall_1_t2822_1_0_0;
struct InvokableCall_1_t2822;
extern Il2CppGenericClass InvokableCall_1_t2822_GenericClass;
TypeInfo InvokableCall_1_t2822_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2822_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2822_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2822_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2822_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2822_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2822_0_0_0/* byval_arg */
	, &InvokableCall_1_t2822_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2822_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2822_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2822)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2823_UnityAction_1__ctor_m14478_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14478_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14478_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2823_UnityAction_1__ctor_m14478_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14478_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandler_t46_0_0_0;
static ParameterInfo UnityAction_1_t2823_UnityAction_1_Invoke_m14479_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t46_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14479_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14479_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2823_UnityAction_1_Invoke_m14479_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14479_GenericMethod/* genericMethod */

};
extern Il2CppType GLErrorHandler_t46_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2823_UnityAction_1_BeginInvoke_m14480_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &GLErrorHandler_t46_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14480_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14480_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2823_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2823_UnityAction_1_BeginInvoke_m14480_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14480_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2823_UnityAction_1_EndInvoke_m14481_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14481_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.GLErrorHandler>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14481_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2823_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2823_UnityAction_1_EndInvoke_m14481_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14481_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2823_MethodInfos[] =
{
	&UnityAction_1__ctor_m14478_MethodInfo,
	&UnityAction_1_Invoke_m14479_MethodInfo,
	&UnityAction_1_BeginInvoke_m14480_MethodInfo,
	&UnityAction_1_EndInvoke_m14481_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14480_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14481_MethodInfo;
static MethodInfo* UnityAction_1_t2823_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14479_MethodInfo,
	&UnityAction_1_BeginInvoke_m14480_MethodInfo,
	&UnityAction_1_EndInvoke_m14481_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2823_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2823_1_0_0;
struct UnityAction_1_t2823;
extern Il2CppGenericClass UnityAction_1_t2823_GenericClass;
TypeInfo UnityAction_1_t2823_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2823_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2823_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2823_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2823_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2823_0_0_0/* byval_arg */
	, &UnityAction_1_t2823_1_0_0/* this_arg */
	, UnityAction_1_t2823_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2823_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2823)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5801_il2cpp_TypeInfo;

// Vuforia.HideExcessAreaBehaviour
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.HideExcessAreaBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.HideExcessAreaBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41899_MethodInfo;
static PropertyInfo IEnumerator_1_t5801____Current_PropertyInfo = 
{
	&IEnumerator_1_t5801_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41899_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5801_PropertyInfos[] =
{
	&IEnumerator_1_t5801____Current_PropertyInfo,
	NULL
};
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41899_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.HideExcessAreaBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41899_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5801_il2cpp_TypeInfo/* declaring_type */
	, &HideExcessAreaBehaviour_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41899_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5801_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41899_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5801_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5801_0_0_0;
extern Il2CppType IEnumerator_1_t5801_1_0_0;
struct IEnumerator_1_t5801;
extern Il2CppGenericClass IEnumerator_1_t5801_GenericClass;
TypeInfo IEnumerator_1_t5801_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5801_MethodInfos/* methods */
	, IEnumerator_1_t5801_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5801_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5801_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5801_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5801_0_0_0/* byval_arg */
	, &IEnumerator_1_t5801_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5801_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_58.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2824_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_58MethodDeclarations.h"

extern TypeInfo HideExcessAreaBehaviour_t47_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14486_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHideExcessAreaBehaviour_t47_m32108_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.HideExcessAreaBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.HideExcessAreaBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisHideExcessAreaBehaviour_t47_m32108(__this, p0, method) (HideExcessAreaBehaviour_t47 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2824____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2824_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2824, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2824____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2824_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2824, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2824_FieldInfos[] =
{
	&InternalEnumerator_1_t2824____array_0_FieldInfo,
	&InternalEnumerator_1_t2824____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14483_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2824____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2824_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14483_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2824____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2824_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14486_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2824_PropertyInfos[] =
{
	&InternalEnumerator_1_t2824____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2824____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2824_InternalEnumerator_1__ctor_m14482_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14482_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14482_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2824_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2824_InternalEnumerator_1__ctor_m14482_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14482_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14483_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14483_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2824_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14483_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14484_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14484_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2824_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14484_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14485_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14485_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2824_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14485_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14486_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14486_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2824_il2cpp_TypeInfo/* declaring_type */
	, &HideExcessAreaBehaviour_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14486_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2824_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14482_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14483_MethodInfo,
	&InternalEnumerator_1_Dispose_m14484_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14485_MethodInfo,
	&InternalEnumerator_1_get_Current_m14486_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14485_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14484_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2824_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14483_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14485_MethodInfo,
	&InternalEnumerator_1_Dispose_m14484_MethodInfo,
	&InternalEnumerator_1_get_Current_m14486_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2824_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5801_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2824_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5801_il2cpp_TypeInfo, 7},
};
extern TypeInfo HideExcessAreaBehaviour_t47_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2824_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14486_MethodInfo/* Method Usage */,
	&HideExcessAreaBehaviour_t47_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisHideExcessAreaBehaviour_t47_m32108_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2824_0_0_0;
extern Il2CppType InternalEnumerator_1_t2824_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2824_GenericClass;
TypeInfo InternalEnumerator_1_t2824_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2824_MethodInfos/* methods */
	, InternalEnumerator_1_t2824_PropertyInfos/* properties */
	, InternalEnumerator_1_t2824_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2824_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2824_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2824_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2824_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2824_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2824_1_0_0/* this_arg */
	, InternalEnumerator_1_t2824_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2824_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2824_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2824)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7371_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>
extern MethodInfo ICollection_1_get_Count_m41900_MethodInfo;
static PropertyInfo ICollection_1_t7371____Count_PropertyInfo = 
{
	&ICollection_1_t7371_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41900_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41901_MethodInfo;
static PropertyInfo ICollection_1_t7371____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7371_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41901_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7371_PropertyInfos[] =
{
	&ICollection_1_t7371____Count_PropertyInfo,
	&ICollection_1_t7371____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41900_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41900_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7371_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41900_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41901_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41901_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7371_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41901_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
static ParameterInfo ICollection_1_t7371_ICollection_1_Add_m41902_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t47_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41902_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41902_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7371_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7371_ICollection_1_Add_m41902_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41902_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41903_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41903_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7371_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41903_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
static ParameterInfo ICollection_1_t7371_ICollection_1_Contains_m41904_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t47_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41904_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41904_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7371_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7371_ICollection_1_Contains_m41904_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41904_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviourU5BU5D_t5177_0_0_0;
extern Il2CppType HideExcessAreaBehaviourU5BU5D_t5177_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7371_ICollection_1_CopyTo_m41905_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviourU5BU5D_t5177_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41905_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41905_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7371_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7371_ICollection_1_CopyTo_m41905_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41905_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
static ParameterInfo ICollection_1_t7371_ICollection_1_Remove_m41906_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t47_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41906_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41906_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7371_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7371_ICollection_1_Remove_m41906_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41906_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7371_MethodInfos[] =
{
	&ICollection_1_get_Count_m41900_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41901_MethodInfo,
	&ICollection_1_Add_m41902_MethodInfo,
	&ICollection_1_Clear_m41903_MethodInfo,
	&ICollection_1_Contains_m41904_MethodInfo,
	&ICollection_1_CopyTo_m41905_MethodInfo,
	&ICollection_1_Remove_m41906_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7373_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7371_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7373_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7371_0_0_0;
extern Il2CppType ICollection_1_t7371_1_0_0;
struct ICollection_1_t7371;
extern Il2CppGenericClass ICollection_1_t7371_GenericClass;
TypeInfo ICollection_1_t7371_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7371_MethodInfos/* methods */
	, ICollection_1_t7371_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7371_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7371_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7371_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7371_0_0_0/* byval_arg */
	, &ICollection_1_t7371_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7371_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.HideExcessAreaBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.HideExcessAreaBehaviour>
extern Il2CppType IEnumerator_1_t5801_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41907_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.HideExcessAreaBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41907_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7373_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5801_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41907_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7373_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41907_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7373_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7373_0_0_0;
extern Il2CppType IEnumerable_1_t7373_1_0_0;
struct IEnumerable_1_t7373;
extern Il2CppGenericClass IEnumerable_1_t7373_GenericClass;
TypeInfo IEnumerable_1_t7373_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7373_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7373_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7373_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7373_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7373_0_0_0/* byval_arg */
	, &IEnumerable_1_t7373_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7373_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7372_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>
extern MethodInfo IList_1_get_Item_m41908_MethodInfo;
extern MethodInfo IList_1_set_Item_m41909_MethodInfo;
static PropertyInfo IList_1_t7372____Item_PropertyInfo = 
{
	&IList_1_t7372_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41908_MethodInfo/* get */
	, &IList_1_set_Item_m41909_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7372_PropertyInfos[] =
{
	&IList_1_t7372____Item_PropertyInfo,
	NULL
};
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
static ParameterInfo IList_1_t7372_IList_1_IndexOf_m41910_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t47_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41910_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41910_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7372_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7372_IList_1_IndexOf_m41910_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41910_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
static ParameterInfo IList_1_t7372_IList_1_Insert_m41911_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t47_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41911_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41911_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7372_IList_1_Insert_m41911_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41911_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7372_IList_1_RemoveAt_m41912_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41912_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41912_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7372_IList_1_RemoveAt_m41912_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41912_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7372_IList_1_get_Item_m41908_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41908_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41908_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7372_il2cpp_TypeInfo/* declaring_type */
	, &HideExcessAreaBehaviour_t47_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7372_IList_1_get_Item_m41908_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41908_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
static ParameterInfo IList_1_t7372_IList_1_set_Item_m41909_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t47_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41909_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41909_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7372_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7372_IList_1_set_Item_m41909_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41909_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7372_MethodInfos[] =
{
	&IList_1_IndexOf_m41910_MethodInfo,
	&IList_1_Insert_m41911_MethodInfo,
	&IList_1_RemoveAt_m41912_MethodInfo,
	&IList_1_get_Item_m41908_MethodInfo,
	&IList_1_set_Item_m41909_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7372_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7371_il2cpp_TypeInfo,
	&IEnumerable_1_t7373_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7372_0_0_0;
extern Il2CppType IList_1_t7372_1_0_0;
struct IList_1_t7372;
extern Il2CppGenericClass IList_1_t7372_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7372_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7372_MethodInfos/* methods */
	, IList_1_t7372_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7372_il2cpp_TypeInfo/* element_class */
	, IList_1_t7372_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7372_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7372_0_0_0/* byval_arg */
	, &IList_1_t7372_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7372_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7374_il2cpp_TypeInfo;

// Vuforia.HideExcessAreaAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbstr.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m41913_MethodInfo;
static PropertyInfo ICollection_1_t7374____Count_PropertyInfo = 
{
	&ICollection_1_t7374_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41913_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41914_MethodInfo;
static PropertyInfo ICollection_1_t7374____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7374_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41914_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7374_PropertyInfos[] =
{
	&ICollection_1_t7374____Count_PropertyInfo,
	&ICollection_1_t7374____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41913_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41913_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7374_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41913_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41914_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41914_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7374_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41914_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaAbstractBehaviour_t48_0_0_0;
extern Il2CppType HideExcessAreaAbstractBehaviour_t48_0_0_0;
static ParameterInfo ICollection_1_t7374_ICollection_1_Add_m41915_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviour_t48_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41915_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41915_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7374_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7374_ICollection_1_Add_m41915_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41915_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41916_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41916_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7374_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41916_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaAbstractBehaviour_t48_0_0_0;
static ParameterInfo ICollection_1_t7374_ICollection_1_Contains_m41917_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviour_t48_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41917_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41917_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7374_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7374_ICollection_1_Contains_m41917_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41917_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaAbstractBehaviourU5BU5D_t5501_0_0_0;
extern Il2CppType HideExcessAreaAbstractBehaviourU5BU5D_t5501_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7374_ICollection_1_CopyTo_m41918_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviourU5BU5D_t5501_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41918_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41918_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7374_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7374_ICollection_1_CopyTo_m41918_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41918_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaAbstractBehaviour_t48_0_0_0;
static ParameterInfo ICollection_1_t7374_ICollection_1_Remove_m41919_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviour_t48_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41919_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.HideExcessAreaAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41919_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7374_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7374_ICollection_1_Remove_m41919_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41919_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7374_MethodInfos[] =
{
	&ICollection_1_get_Count_m41913_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41914_MethodInfo,
	&ICollection_1_Add_m41915_MethodInfo,
	&ICollection_1_Clear_m41916_MethodInfo,
	&ICollection_1_Contains_m41917_MethodInfo,
	&ICollection_1_CopyTo_m41918_MethodInfo,
	&ICollection_1_Remove_m41919_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7376_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7374_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7376_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7374_0_0_0;
extern Il2CppType ICollection_1_t7374_1_0_0;
struct ICollection_1_t7374;
extern Il2CppGenericClass ICollection_1_t7374_GenericClass;
TypeInfo ICollection_1_t7374_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7374_MethodInfos/* methods */
	, ICollection_1_t7374_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7374_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7374_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7374_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7374_0_0_0/* byval_arg */
	, &ICollection_1_t7374_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7374_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.HideExcessAreaAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.HideExcessAreaAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5803_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41920_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.HideExcessAreaAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41920_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7376_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5803_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41920_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7376_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41920_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7376_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7376_0_0_0;
extern Il2CppType IEnumerable_1_t7376_1_0_0;
struct IEnumerable_1_t7376;
extern Il2CppGenericClass IEnumerable_1_t7376_GenericClass;
TypeInfo IEnumerable_1_t7376_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7376_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7376_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7376_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7376_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7376_0_0_0/* byval_arg */
	, &IEnumerable_1_t7376_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7376_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5803_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41921_MethodInfo;
static PropertyInfo IEnumerator_1_t5803____Current_PropertyInfo = 
{
	&IEnumerator_1_t5803_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41921_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5803_PropertyInfos[] =
{
	&IEnumerator_1_t5803____Current_PropertyInfo,
	NULL
};
extern Il2CppType HideExcessAreaAbstractBehaviour_t48_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41921_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41921_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5803_il2cpp_TypeInfo/* declaring_type */
	, &HideExcessAreaAbstractBehaviour_t48_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41921_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5803_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41921_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5803_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5803_0_0_0;
extern Il2CppType IEnumerator_1_t5803_1_0_0;
struct IEnumerator_1_t5803;
extern Il2CppGenericClass IEnumerator_1_t5803_GenericClass;
TypeInfo IEnumerator_1_t5803_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5803_MethodInfos/* methods */
	, IEnumerator_1_t5803_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5803_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5803_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5803_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5803_0_0_0/* byval_arg */
	, &IEnumerator_1_t5803_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5803_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_59.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2825_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_59MethodDeclarations.h"

extern TypeInfo HideExcessAreaAbstractBehaviour_t48_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14491_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisHideExcessAreaAbstractBehaviour_t48_m32119_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.HideExcessAreaAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.HideExcessAreaAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisHideExcessAreaAbstractBehaviour_t48_m32119(__this, p0, method) (HideExcessAreaAbstractBehaviour_t48 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2825____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2825_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2825, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2825____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2825_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2825, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2825_FieldInfos[] =
{
	&InternalEnumerator_1_t2825____array_0_FieldInfo,
	&InternalEnumerator_1_t2825____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14488_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2825____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2825_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14488_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2825____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2825_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14491_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2825_PropertyInfos[] =
{
	&InternalEnumerator_1_t2825____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2825____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2825_InternalEnumerator_1__ctor_m14487_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14487_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14487_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2825_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2825_InternalEnumerator_1__ctor_m14487_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14487_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14488_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14488_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2825_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14488_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14489_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14489_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2825_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14489_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14490_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14490_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2825_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14490_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaAbstractBehaviour_t48_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14491_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14491_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2825_il2cpp_TypeInfo/* declaring_type */
	, &HideExcessAreaAbstractBehaviour_t48_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14491_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2825_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14487_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14488_MethodInfo,
	&InternalEnumerator_1_Dispose_m14489_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14490_MethodInfo,
	&InternalEnumerator_1_get_Current_m14491_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14490_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14489_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2825_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14488_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14490_MethodInfo,
	&InternalEnumerator_1_Dispose_m14489_MethodInfo,
	&InternalEnumerator_1_get_Current_m14491_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2825_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5803_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2825_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5803_il2cpp_TypeInfo, 7},
};
extern TypeInfo HideExcessAreaAbstractBehaviour_t48_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2825_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14491_MethodInfo/* Method Usage */,
	&HideExcessAreaAbstractBehaviour_t48_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisHideExcessAreaAbstractBehaviour_t48_m32119_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2825_0_0_0;
extern Il2CppType InternalEnumerator_1_t2825_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2825_GenericClass;
TypeInfo InternalEnumerator_1_t2825_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2825_MethodInfos/* methods */
	, InternalEnumerator_1_t2825_PropertyInfos/* properties */
	, InternalEnumerator_1_t2825_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2825_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2825_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2825_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2825_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2825_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2825_1_0_0/* this_arg */
	, InternalEnumerator_1_t2825_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2825_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2825_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2825)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7375_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m41922_MethodInfo;
extern MethodInfo IList_1_set_Item_m41923_MethodInfo;
static PropertyInfo IList_1_t7375____Item_PropertyInfo = 
{
	&IList_1_t7375_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41922_MethodInfo/* get */
	, &IList_1_set_Item_m41923_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7375_PropertyInfos[] =
{
	&IList_1_t7375____Item_PropertyInfo,
	NULL
};
extern Il2CppType HideExcessAreaAbstractBehaviour_t48_0_0_0;
static ParameterInfo IList_1_t7375_IList_1_IndexOf_m41924_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviour_t48_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41924_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41924_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7375_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7375_IList_1_IndexOf_m41924_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41924_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType HideExcessAreaAbstractBehaviour_t48_0_0_0;
static ParameterInfo IList_1_t7375_IList_1_Insert_m41925_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviour_t48_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41925_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41925_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7375_IList_1_Insert_m41925_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41925_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7375_IList_1_RemoveAt_m41926_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41926_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41926_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7375_IList_1_RemoveAt_m41926_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41926_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7375_IList_1_get_Item_m41922_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType HideExcessAreaAbstractBehaviour_t48_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41922_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41922_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7375_il2cpp_TypeInfo/* declaring_type */
	, &HideExcessAreaAbstractBehaviour_t48_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7375_IList_1_get_Item_m41922_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41922_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType HideExcessAreaAbstractBehaviour_t48_0_0_0;
static ParameterInfo IList_1_t7375_IList_1_set_Item_m41923_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaAbstractBehaviour_t48_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41923_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.HideExcessAreaAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41923_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7375_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7375_IList_1_set_Item_m41923_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41923_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7375_MethodInfos[] =
{
	&IList_1_IndexOf_m41924_MethodInfo,
	&IList_1_Insert_m41925_MethodInfo,
	&IList_1_RemoveAt_m41926_MethodInfo,
	&IList_1_get_Item_m41922_MethodInfo,
	&IList_1_set_Item_m41923_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7375_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7374_il2cpp_TypeInfo,
	&IEnumerable_1_t7376_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7375_0_0_0;
extern Il2CppType IList_1_t7375_1_0_0;
struct IList_1_t7375;
extern Il2CppGenericClass IList_1_t7375_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7375_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7375_MethodInfos/* methods */
	, IList_1_t7375_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7375_il2cpp_TypeInfo/* element_class */
	, IList_1_t7375_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7375_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7375_0_0_0/* byval_arg */
	, &IList_1_t7375_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7375_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_23.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo CachedInvokableCall_1_t2826_il2cpp_TypeInfo;
// UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_23MethodDeclarations.h"

// UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_19.h"
extern TypeInfo InvokableCall_1_t2827_il2cpp_TypeInfo;
// UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen_19MethodDeclarations.h"
extern MethodInfo InvokableCall_1__ctor_m14494_MethodInfo;
extern MethodInfo InvokableCall_1_Invoke_m14496_MethodInfo;


// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>::Invoke(System.Object[])
// Metadata Definition UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>
extern Il2CppType ObjectU5BU5D_t115_0_0_33;
FieldInfo CachedInvokableCall_1_t2826____m_Arg1_1_FieldInfo = 
{
	"m_Arg1"/* name */
	, &ObjectU5BU5D_t115_0_0_33/* type */
	, &CachedInvokableCall_1_t2826_il2cpp_TypeInfo/* parent */
	, offsetof(CachedInvokableCall_1_t2826, ___m_Arg1_1)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* CachedInvokableCall_1_t2826_FieldInfos[] =
{
	&CachedInvokableCall_1_t2826____m_Arg1_1_FieldInfo,
	NULL
};
extern Il2CppType Object_t117_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2826_CachedInvokableCall_1__ctor_m14492_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t117_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
	{"argument", 2, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t47_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1__ctor_m14492_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
MethodInfo CachedInvokableCall_1__ctor_m14492_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&CachedInvokableCall_1__ctor_m14008_gshared/* method */
	, &CachedInvokableCall_1_t2826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2826_CachedInvokableCall_1__ctor_m14492_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1__ctor_m14492_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo CachedInvokableCall_1_t2826_CachedInvokableCall_1_Invoke_m14493_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod CachedInvokableCall_1_Invoke_m14493_GenericMethod;
// System.Void UnityEngine.Events.CachedInvokableCall`1<Vuforia.HideExcessAreaBehaviour>::Invoke(System.Object[])
MethodInfo CachedInvokableCall_1_Invoke_m14493_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&CachedInvokableCall_1_Invoke_m14010_gshared/* method */
	, &CachedInvokableCall_1_t2826_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, CachedInvokableCall_1_t2826_CachedInvokableCall_1_Invoke_m14493_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &CachedInvokableCall_1_Invoke_m14493_GenericMethod/* genericMethod */

};
static MethodInfo* CachedInvokableCall_1_t2826_MethodInfos[] =
{
	&CachedInvokableCall_1__ctor_m14492_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14493_MethodInfo,
	NULL
};
extern MethodInfo CachedInvokableCall_1_Invoke_m14493_MethodInfo;
extern MethodInfo InvokableCall_1_Find_m14497_MethodInfo;
static MethodInfo* CachedInvokableCall_1_t2826_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&CachedInvokableCall_1_Invoke_m14493_MethodInfo,
	&InvokableCall_1_Find_m14497_MethodInfo,
};
extern Il2CppType UnityAction_1_t2828_0_0_0;
extern TypeInfo UnityAction_1_t2828_il2cpp_TypeInfo;
extern MethodInfo BaseInvokableCall_ThrowOnInvalidArg_TisHideExcessAreaBehaviour_t47_m32129_MethodInfo;
extern TypeInfo HideExcessAreaBehaviour_t47_il2cpp_TypeInfo;
extern MethodInfo UnityAction_1_Invoke_m14499_MethodInfo;
extern TypeInfo HideExcessAreaBehaviour_t47_il2cpp_TypeInfo;
static Il2CppRGCTXData CachedInvokableCall_1_t2826_RGCTXData[8] = 
{
	&UnityAction_1_t2828_0_0_0/* Type Usage */,
	&UnityAction_1_t2828_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisHideExcessAreaBehaviour_t47_m32129_MethodInfo/* Method Usage */,
	&HideExcessAreaBehaviour_t47_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14499_MethodInfo/* Method Usage */,
	&InvokableCall_1__ctor_m14494_MethodInfo/* Method Usage */,
	&HideExcessAreaBehaviour_t47_il2cpp_TypeInfo/* Class Usage */,
	&InvokableCall_1_Invoke_m14496_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType CachedInvokableCall_1_t2826_0_0_0;
extern Il2CppType CachedInvokableCall_1_t2826_1_0_0;
struct CachedInvokableCall_1_t2826;
extern Il2CppGenericClass CachedInvokableCall_1_t2826_GenericClass;
TypeInfo CachedInvokableCall_1_t2826_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "CachedInvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, CachedInvokableCall_1_t2826_MethodInfos/* methods */
	, NULL/* properties */
	, CachedInvokableCall_1_t2826_FieldInfos/* fields */
	, NULL/* events */
	, &InvokableCall_1_t2827_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &CachedInvokableCall_1_t2826_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, CachedInvokableCall_1_t2826_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &CachedInvokableCall_1_t2826_il2cpp_TypeInfo/* cast_class */
	, &CachedInvokableCall_1_t2826_0_0_0/* byval_arg */
	, &CachedInvokableCall_1_t2826_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &CachedInvokableCall_1_t2826_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, CachedInvokableCall_1_t2826_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (CachedInvokableCall_1_t2826)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 2/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif

// UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_26.h"
extern TypeInfo UnityAction_1_t2828_il2cpp_TypeInfo;
// UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_26MethodDeclarations.h"
struct BaseInvokableCall_t1075;
// Declaration System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.HideExcessAreaBehaviour>(System.Object)
// System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg<Vuforia.HideExcessAreaBehaviour>(System.Object)
#define BaseInvokableCall_ThrowOnInvalidArg_TisHideExcessAreaBehaviour_t47_m32129(__this/* static, unused */, p0, method) (void)BaseInvokableCall_ThrowOnInvalidArg_TisObject_t_m31501_gshared((Object_t *)__this/* static, unused */, (Object_t *)p0, method)


// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::Invoke(System.Object[])
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
// Metadata Definition UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>
extern Il2CppType UnityAction_1_t2828_0_0_1;
FieldInfo InvokableCall_1_t2827____Delegate_0_FieldInfo = 
{
	"Delegate"/* name */
	, &UnityAction_1_t2828_0_0_1/* type */
	, &InvokableCall_1_t2827_il2cpp_TypeInfo/* parent */
	, offsetof(InvokableCall_1_t2827, ___Delegate_0)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InvokableCall_1_t2827_FieldInfos[] =
{
	&InvokableCall_1_t2827____Delegate_0_FieldInfo,
	NULL
};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2827_InvokableCall_1__ctor_m14494_ParameterInfos[] = 
{
	{"target", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"theFunction", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14494_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::.ctor(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1__ctor_m14494_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14011_gshared/* method */
	, &InvokableCall_1_t2827_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2827_InvokableCall_1__ctor_m14494_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14494_GenericMethod/* genericMethod */

};
extern Il2CppType UnityAction_1_t2828_0_0_0;
static ParameterInfo InvokableCall_1_t2827_InvokableCall_1__ctor_m14495_ParameterInfos[] = 
{
	{"callback", 0, 134217728, &EmptyCustomAttributesCache, &UnityAction_1_t2828_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1__ctor_m14495_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
MethodInfo InvokableCall_1__ctor_m14495_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InvokableCall_1__ctor_m14012_gshared/* method */
	, &InvokableCall_1_t2827_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2827_InvokableCall_1__ctor_m14495_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1__ctor_m14495_GenericMethod/* genericMethod */

};
extern Il2CppType ObjectU5BU5D_t115_0_0_0;
static ParameterInfo InvokableCall_1_t2827_InvokableCall_1_Invoke_m14496_ParameterInfos[] = 
{
	{"args", 0, 134217728, &EmptyCustomAttributesCache, &ObjectU5BU5D_t115_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Invoke_m14496_GenericMethod;
// System.Void UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::Invoke(System.Object[])
MethodInfo InvokableCall_1_Invoke_m14496_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&InvokableCall_1_Invoke_m14013_gshared/* method */
	, &InvokableCall_1_t2827_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InvokableCall_1_t2827_InvokableCall_1_Invoke_m14496_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Invoke_m14496_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern Il2CppType MethodInfo_t142_0_0_0;
static ParameterInfo InvokableCall_1_t2827_InvokableCall_1_Find_m14497_ParameterInfos[] = 
{
	{"targetObj", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &MethodInfo_t142_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InvokableCall_1_Find_m14497_GenericMethod;
// System.Boolean UnityEngine.Events.InvokableCall`1<Vuforia.HideExcessAreaBehaviour>::Find(System.Object,System.Reflection.MethodInfo)
MethodInfo InvokableCall_1_Find_m14497_MethodInfo = 
{
	"Find"/* name */
	, (methodPointerType)&InvokableCall_1_Find_m14014_gshared/* method */
	, &InvokableCall_1_t2827_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t_Object_t/* invoker_method */
	, InvokableCall_1_t2827_InvokableCall_1_Find_m14497_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 198/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InvokableCall_1_Find_m14497_GenericMethod/* genericMethod */

};
static MethodInfo* InvokableCall_1_t2827_MethodInfos[] =
{
	&InvokableCall_1__ctor_m14494_MethodInfo,
	&InvokableCall_1__ctor_m14495_MethodInfo,
	&InvokableCall_1_Invoke_m14496_MethodInfo,
	&InvokableCall_1_Find_m14497_MethodInfo,
	NULL
};
static MethodInfo* InvokableCall_1_t2827_VTable[] =
{
	&Object_Equals_m304_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&Object_GetHashCode_m305_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&InvokableCall_1_Invoke_m14496_MethodInfo,
	&InvokableCall_1_Find_m14497_MethodInfo,
};
extern TypeInfo UnityAction_1_t2828_il2cpp_TypeInfo;
extern TypeInfo HideExcessAreaBehaviour_t47_il2cpp_TypeInfo;
static Il2CppRGCTXData InvokableCall_1_t2827_RGCTXData[5] = 
{
	&UnityAction_1_t2828_0_0_0/* Type Usage */,
	&UnityAction_1_t2828_il2cpp_TypeInfo/* Class Usage */,
	&BaseInvokableCall_ThrowOnInvalidArg_TisHideExcessAreaBehaviour_t47_m32129_MethodInfo/* Method Usage */,
	&HideExcessAreaBehaviour_t47_il2cpp_TypeInfo/* Class Usage */,
	&UnityAction_1_Invoke_m14499_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType InvokableCall_1_t2827_0_0_0;
extern Il2CppType InvokableCall_1_t2827_1_0_0;
struct InvokableCall_1_t2827;
extern Il2CppGenericClass InvokableCall_1_t2827_GenericClass;
TypeInfo InvokableCall_1_t2827_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "InvokableCall`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, InvokableCall_1_t2827_MethodInfos/* methods */
	, NULL/* properties */
	, InvokableCall_1_t2827_FieldInfos/* fields */
	, NULL/* events */
	, &BaseInvokableCall_t1075_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &InvokableCall_1_t2827_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, InvokableCall_1_t2827_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InvokableCall_1_t2827_il2cpp_TypeInfo/* cast_class */
	, &InvokableCall_1_t2827_0_0_0/* byval_arg */
	, &InvokableCall_1_t2827_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &InvokableCall_1_t2827_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InvokableCall_1_t2827_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InvokableCall_1_t2827)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048576/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 1/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 6/* vtable_count */
	, 0/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Void UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::.ctor(System.Object,System.IntPtr)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::Invoke(T0)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::EndInvoke(System.IAsyncResult)
// Metadata Definition UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>
extern Il2CppType Object_t_0_0_0;
extern Il2CppType IntPtr_t121_0_0_0;
static ParameterInfo UnityAction_1_t2828_UnityAction_1__ctor_m14498_ParameterInfos[] = 
{
	{"object", 0, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
	{"method", 1, 134217728, &EmptyCustomAttributesCache, &IntPtr_t121_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_IntPtr_t121 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1__ctor_m14498_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::.ctor(System.Object,System.IntPtr)
MethodInfo UnityAction_1__ctor_m14498_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&UnityAction_1__ctor_m14015_gshared/* method */
	, &UnityAction_1_t2828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_IntPtr_t121/* invoker_method */
	, UnityAction_1_t2828_UnityAction_1__ctor_m14498_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6278/* flags */
	, 3/* iflags */
	, 255/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1__ctor_m14498_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
static ParameterInfo UnityAction_1_t2828_UnityAction_1_Invoke_m14499_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t47_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_Invoke_m14499_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::Invoke(T0)
MethodInfo UnityAction_1_Invoke_m14499_MethodInfo = 
{
	"Invoke"/* name */
	, (methodPointerType)&UnityAction_1_Invoke_m14016_gshared/* method */
	, &UnityAction_1_t2828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2828_UnityAction_1_Invoke_m14499_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 10/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_Invoke_m14499_GenericMethod/* genericMethod */

};
extern Il2CppType HideExcessAreaBehaviour_t47_0_0_0;
extern Il2CppType AsyncCallback_t200_0_0_0;
extern Il2CppType Object_t_0_0_0;
static ParameterInfo UnityAction_1_t2828_UnityAction_1_BeginInvoke_m14500_ParameterInfos[] = 
{
	{"arg0", 0, 134217728, &EmptyCustomAttributesCache, &HideExcessAreaBehaviour_t47_0_0_0},
	{"callback", 1, 134217728, &EmptyCustomAttributesCache, &AsyncCallback_t200_0_0_0},
	{"object", 2, 134217728, &EmptyCustomAttributesCache, &Object_t_0_0_0},
};
extern Il2CppType IAsyncResult_t199_0_0_0;
extern void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_BeginInvoke_m14500_GenericMethod;
// System.IAsyncResult UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::BeginInvoke(T0,System.AsyncCallback,System.Object)
MethodInfo UnityAction_1_BeginInvoke_m14500_MethodInfo = 
{
	"BeginInvoke"/* name */
	, (methodPointerType)&UnityAction_1_BeginInvoke_m14017_gshared/* method */
	, &UnityAction_1_t2828_il2cpp_TypeInfo/* declaring_type */
	, &IAsyncResult_t199_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Object_t_Object_t_Object_t/* invoker_method */
	, UnityAction_1_t2828_UnityAction_1_BeginInvoke_m14500_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 11/* slot */
	, 3/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_BeginInvoke_m14500_GenericMethod/* genericMethod */

};
extern Il2CppType IAsyncResult_t199_0_0_0;
static ParameterInfo UnityAction_1_t2828_UnityAction_1_EndInvoke_m14501_ParameterInfos[] = 
{
	{"result", 0, 134217728, &EmptyCustomAttributesCache, &IAsyncResult_t199_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod UnityAction_1_EndInvoke_m14501_GenericMethod;
// System.Void UnityEngine.Events.UnityAction`1<Vuforia.HideExcessAreaBehaviour>::EndInvoke(System.IAsyncResult)
MethodInfo UnityAction_1_EndInvoke_m14501_MethodInfo = 
{
	"EndInvoke"/* name */
	, (methodPointerType)&UnityAction_1_EndInvoke_m14018_gshared/* method */
	, &UnityAction_1_t2828_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, UnityAction_1_t2828_UnityAction_1_EndInvoke_m14501_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 454/* flags */
	, 3/* iflags */
	, 12/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &UnityAction_1_EndInvoke_m14501_GenericMethod/* genericMethod */

};
static MethodInfo* UnityAction_1_t2828_MethodInfos[] =
{
	&UnityAction_1__ctor_m14498_MethodInfo,
	&UnityAction_1_Invoke_m14499_MethodInfo,
	&UnityAction_1_BeginInvoke_m14500_MethodInfo,
	&UnityAction_1_EndInvoke_m14501_MethodInfo,
	NULL
};
extern MethodInfo UnityAction_1_BeginInvoke_m14500_MethodInfo;
extern MethodInfo UnityAction_1_EndInvoke_m14501_MethodInfo;
static MethodInfo* UnityAction_1_t2828_VTable[] =
{
	&MulticastDelegate_Equals_m2241_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&MulticastDelegate_GetHashCode_m2242_MethodInfo,
	&Object_ToString_m306_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&Delegate_Clone_m2244_MethodInfo,
	&MulticastDelegate_GetObjectData_m2243_MethodInfo,
	&MulticastDelegate_GetInvocationList_m2245_MethodInfo,
	&MulticastDelegate_CombineImpl_m2246_MethodInfo,
	&MulticastDelegate_RemoveImpl_m2247_MethodInfo,
	&UnityAction_1_Invoke_m14499_MethodInfo,
	&UnityAction_1_BeginInvoke_m14500_MethodInfo,
	&UnityAction_1_EndInvoke_m14501_MethodInfo,
};
static Il2CppInterfaceOffsetPair UnityAction_1_t2828_InterfacesOffsets[] = 
{
	{ &ICloneable_t481_il2cpp_TypeInfo, 4},
	{ &ISerializable_t482_il2cpp_TypeInfo, 4},
};
extern Il2CppImage g_UnityEngine_dll_Image;
extern Il2CppType UnityAction_1_t2828_1_0_0;
struct UnityAction_1_t2828;
extern Il2CppGenericClass UnityAction_1_t2828_GenericClass;
TypeInfo UnityAction_1_t2828_il2cpp_TypeInfo = 
{
	&g_UnityEngine_dll_Image/* image */
	, NULL/* gc_desc */
	, "UnityAction`1"/* name */
	, "UnityEngine.Events"/* namespaze */
	, UnityAction_1_t2828_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, &MulticastDelegate_t325_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &UnityAction_1_t2828_il2cpp_TypeInfo/* element_class */
	, NULL/* implemented_interfaces */
	, UnityAction_1_t2828_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &UnityAction_1_t2828_il2cpp_TypeInfo/* cast_class */
	, &UnityAction_1_t2828_0_0_0/* byval_arg */
	, &UnityAction_1_t2828_1_0_0/* this_arg */
	, UnityAction_1_t2828_InterfacesOffsets/* interface_offsets */
	, &UnityAction_1_t2828_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (UnityAction_1_t2828)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 257/* flags */
	, 0/* rank */
	, false/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 4/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 13/* vtable_count */
	, 0/* interfaces_count */
	, 2/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5805_il2cpp_TypeInfo;

// Vuforia.ImageTargetBehaviour
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour.h"


// T System.Collections.Generic.IEnumerator`1<Vuforia.ImageTargetBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ImageTargetBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41927_MethodInfo;
static PropertyInfo IEnumerator_1_t5805____Current_PropertyInfo = 
{
	&IEnumerator_1_t5805_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41927_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5805_PropertyInfos[] =
{
	&IEnumerator_1_t5805____Current_PropertyInfo,
	NULL
};
extern Il2CppType ImageTargetBehaviour_t49_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41927_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ImageTargetBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41927_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5805_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetBehaviour_t49_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41927_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5805_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41927_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5805_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5805_0_0_0;
extern Il2CppType IEnumerator_1_t5805_1_0_0;
struct IEnumerator_1_t5805;
extern Il2CppGenericClass IEnumerator_1_t5805_GenericClass;
TypeInfo IEnumerator_1_t5805_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5805_MethodInfos/* methods */
	, IEnumerator_1_t5805_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5805_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5805_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5805_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5805_0_0_0/* byval_arg */
	, &IEnumerator_1_t5805_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5805_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_60.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2829_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_60MethodDeclarations.h"

extern TypeInfo ImageTargetBehaviour_t49_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14506_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisImageTargetBehaviour_t49_m32131_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ImageTargetBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ImageTargetBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisImageTargetBehaviour_t49_m32131(__this, p0, method) (ImageTargetBehaviour_t49 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2829____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2829_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2829, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2829____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2829_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2829, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2829_FieldInfos[] =
{
	&InternalEnumerator_1_t2829____array_0_FieldInfo,
	&InternalEnumerator_1_t2829____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14503_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2829____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2829_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14503_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2829____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2829_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14506_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2829_PropertyInfos[] =
{
	&InternalEnumerator_1_t2829____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2829____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2829_InternalEnumerator_1__ctor_m14502_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14502_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14502_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2829_InternalEnumerator_1__ctor_m14502_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14502_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14503_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14503_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14503_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14504_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14504_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14504_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14505_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14505_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14505_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetBehaviour_t49_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14506_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ImageTargetBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14506_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2829_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetBehaviour_t49_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14506_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2829_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14502_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14503_MethodInfo,
	&InternalEnumerator_1_Dispose_m14504_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14505_MethodInfo,
	&InternalEnumerator_1_get_Current_m14506_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14505_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14504_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2829_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14503_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14505_MethodInfo,
	&InternalEnumerator_1_Dispose_m14504_MethodInfo,
	&InternalEnumerator_1_get_Current_m14506_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2829_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5805_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2829_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5805_il2cpp_TypeInfo, 7},
};
extern TypeInfo ImageTargetBehaviour_t49_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2829_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14506_MethodInfo/* Method Usage */,
	&ImageTargetBehaviour_t49_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisImageTargetBehaviour_t49_m32131_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2829_0_0_0;
extern Il2CppType InternalEnumerator_1_t2829_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2829_GenericClass;
TypeInfo InternalEnumerator_1_t2829_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2829_MethodInfos/* methods */
	, InternalEnumerator_1_t2829_PropertyInfos/* properties */
	, InternalEnumerator_1_t2829_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2829_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2829_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2829_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2829_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2829_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2829_1_0_0/* this_arg */
	, InternalEnumerator_1_t2829_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2829_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2829_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2829)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7377_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>
extern MethodInfo ICollection_1_get_Count_m41928_MethodInfo;
static PropertyInfo ICollection_1_t7377____Count_PropertyInfo = 
{
	&ICollection_1_t7377_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41928_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41929_MethodInfo;
static PropertyInfo ICollection_1_t7377____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7377_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41929_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7377_PropertyInfos[] =
{
	&ICollection_1_t7377____Count_PropertyInfo,
	&ICollection_1_t7377____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41928_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41928_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7377_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41928_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41929_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41929_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7377_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41929_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetBehaviour_t49_0_0_0;
extern Il2CppType ImageTargetBehaviour_t49_0_0_0;
static ParameterInfo ICollection_1_t7377_ICollection_1_Add_m41930_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviour_t49_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41930_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41930_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7377_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7377_ICollection_1_Add_m41930_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41930_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41931_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41931_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7377_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41931_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetBehaviour_t49_0_0_0;
static ParameterInfo ICollection_1_t7377_ICollection_1_Contains_m41932_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviour_t49_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41932_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41932_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7377_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7377_ICollection_1_Contains_m41932_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41932_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetBehaviourU5BU5D_t5178_0_0_0;
extern Il2CppType ImageTargetBehaviourU5BU5D_t5178_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7377_ICollection_1_CopyTo_m41933_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviourU5BU5D_t5178_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41933_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41933_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7377_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7377_ICollection_1_CopyTo_m41933_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41933_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetBehaviour_t49_0_0_0;
static ParameterInfo ICollection_1_t7377_ICollection_1_Remove_m41934_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviour_t49_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41934_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41934_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7377_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7377_ICollection_1_Remove_m41934_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41934_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7377_MethodInfos[] =
{
	&ICollection_1_get_Count_m41928_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41929_MethodInfo,
	&ICollection_1_Add_m41930_MethodInfo,
	&ICollection_1_Clear_m41931_MethodInfo,
	&ICollection_1_Contains_m41932_MethodInfo,
	&ICollection_1_CopyTo_m41933_MethodInfo,
	&ICollection_1_Remove_m41934_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7379_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7377_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7379_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7377_0_0_0;
extern Il2CppType ICollection_1_t7377_1_0_0;
struct ICollection_1_t7377;
extern Il2CppGenericClass ICollection_1_t7377_GenericClass;
TypeInfo ICollection_1_t7377_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7377_MethodInfos/* methods */
	, ICollection_1_t7377_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7377_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7377_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7377_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7377_0_0_0/* byval_arg */
	, &ICollection_1_t7377_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7377_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ImageTargetBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ImageTargetBehaviour>
extern Il2CppType IEnumerator_1_t5805_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41935_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ImageTargetBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41935_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7379_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5805_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41935_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7379_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41935_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7379_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7379_0_0_0;
extern Il2CppType IEnumerable_1_t7379_1_0_0;
struct IEnumerable_1_t7379;
extern Il2CppGenericClass IEnumerable_1_t7379_GenericClass;
TypeInfo IEnumerable_1_t7379_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7379_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7379_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7379_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7379_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7379_0_0_0/* byval_arg */
	, &IEnumerable_1_t7379_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7379_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7378_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>
extern MethodInfo IList_1_get_Item_m41936_MethodInfo;
extern MethodInfo IList_1_set_Item_m41937_MethodInfo;
static PropertyInfo IList_1_t7378____Item_PropertyInfo = 
{
	&IList_1_t7378_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41936_MethodInfo/* get */
	, &IList_1_set_Item_m41937_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7378_PropertyInfos[] =
{
	&IList_1_t7378____Item_PropertyInfo,
	NULL
};
extern Il2CppType ImageTargetBehaviour_t49_0_0_0;
static ParameterInfo IList_1_t7378_IList_1_IndexOf_m41938_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviour_t49_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41938_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41938_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7378_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7378_IList_1_IndexOf_m41938_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41938_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ImageTargetBehaviour_t49_0_0_0;
static ParameterInfo IList_1_t7378_IList_1_Insert_m41939_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviour_t49_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41939_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41939_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7378_IList_1_Insert_m41939_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41939_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7378_IList_1_RemoveAt_m41940_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41940_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41940_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7378_IList_1_RemoveAt_m41940_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41940_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7378_IList_1_get_Item_m41936_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ImageTargetBehaviour_t49_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41936_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41936_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7378_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetBehaviour_t49_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7378_IList_1_get_Item_m41936_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41936_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ImageTargetBehaviour_t49_0_0_0;
static ParameterInfo IList_1_t7378_IList_1_set_Item_m41937_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ImageTargetBehaviour_t49_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41937_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41937_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7378_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7378_IList_1_set_Item_m41937_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41937_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7378_MethodInfos[] =
{
	&IList_1_IndexOf_m41938_MethodInfo,
	&IList_1_Insert_m41939_MethodInfo,
	&IList_1_RemoveAt_m41940_MethodInfo,
	&IList_1_get_Item_m41936_MethodInfo,
	&IList_1_set_Item_m41937_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7378_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7377_il2cpp_TypeInfo,
	&IEnumerable_1_t7379_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7378_0_0_0;
extern Il2CppType IList_1_t7378_1_0_0;
struct IList_1_t7378;
extern Il2CppGenericClass IList_1_t7378_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7378_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7378_MethodInfos/* methods */
	, IList_1_t7378_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7378_il2cpp_TypeInfo/* element_class */
	, IList_1_t7378_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7378_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7378_0_0_0/* byval_arg */
	, &IList_1_t7378_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7378_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7380_il2cpp_TypeInfo;

// Vuforia.ImageTargetAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetAbstract.h"


// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>
extern MethodInfo ICollection_1_get_Count_m41941_MethodInfo;
static PropertyInfo ICollection_1_t7380____Count_PropertyInfo = 
{
	&ICollection_1_t7380_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41941_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41942_MethodInfo;
static PropertyInfo ICollection_1_t7380____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7380_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41942_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7380_PropertyInfos[] =
{
	&ICollection_1_t7380____Count_PropertyInfo,
	&ICollection_1_t7380____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41941_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41941_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7380_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41941_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41942_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41942_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7380_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41942_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetAbstractBehaviour_t50_0_0_0;
extern Il2CppType ImageTargetAbstractBehaviour_t50_0_0_0;
static ParameterInfo ICollection_1_t7380_ICollection_1_Add_m41943_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetAbstractBehaviour_t50_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41943_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41943_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7380_ICollection_1_Add_m41943_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41943_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41944_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41944_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41944_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetAbstractBehaviour_t50_0_0_0;
static ParameterInfo ICollection_1_t7380_ICollection_1_Contains_m41945_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetAbstractBehaviour_t50_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41945_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41945_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7380_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7380_ICollection_1_Contains_m41945_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41945_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetAbstractBehaviourU5BU5D_t5502_0_0_0;
extern Il2CppType ImageTargetAbstractBehaviourU5BU5D_t5502_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7380_ICollection_1_CopyTo_m41946_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetAbstractBehaviourU5BU5D_t5502_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41946_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41946_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7380_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7380_ICollection_1_CopyTo_m41946_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41946_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetAbstractBehaviour_t50_0_0_0;
static ParameterInfo ICollection_1_t7380_ICollection_1_Remove_m41947_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetAbstractBehaviour_t50_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41947_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.ImageTargetAbstractBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41947_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7380_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7380_ICollection_1_Remove_m41947_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41947_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7380_MethodInfos[] =
{
	&ICollection_1_get_Count_m41941_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41942_MethodInfo,
	&ICollection_1_Add_m41943_MethodInfo,
	&ICollection_1_Clear_m41944_MethodInfo,
	&ICollection_1_Contains_m41945_MethodInfo,
	&ICollection_1_CopyTo_m41946_MethodInfo,
	&ICollection_1_Remove_m41947_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7382_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7380_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7382_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7380_0_0_0;
extern Il2CppType ICollection_1_t7380_1_0_0;
struct ICollection_1_t7380;
extern Il2CppGenericClass ICollection_1_t7380_GenericClass;
TypeInfo ICollection_1_t7380_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7380_MethodInfos/* methods */
	, ICollection_1_t7380_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7380_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7380_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7380_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7380_0_0_0/* byval_arg */
	, &ICollection_1_t7380_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7380_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ImageTargetAbstractBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.ImageTargetAbstractBehaviour>
extern Il2CppType IEnumerator_1_t5807_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41948_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.ImageTargetAbstractBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41948_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7382_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5807_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41948_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7382_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41948_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7382_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7382_0_0_0;
extern Il2CppType IEnumerable_1_t7382_1_0_0;
struct IEnumerable_1_t7382;
extern Il2CppGenericClass IEnumerable_1_t7382_GenericClass;
TypeInfo IEnumerable_1_t7382_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7382_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7382_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7382_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7382_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7382_0_0_0/* byval_arg */
	, &IEnumerable_1_t7382_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7382_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5807_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41949_MethodInfo;
static PropertyInfo IEnumerator_1_t5807____Current_PropertyInfo = 
{
	&IEnumerator_1_t5807_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41949_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5807_PropertyInfos[] =
{
	&IEnumerator_1_t5807____Current_PropertyInfo,
	NULL
};
extern Il2CppType ImageTargetAbstractBehaviour_t50_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41949_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41949_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5807_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetAbstractBehaviour_t50_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41949_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5807_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41949_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5807_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5807_0_0_0;
extern Il2CppType IEnumerator_1_t5807_1_0_0;
struct IEnumerator_1_t5807;
extern Il2CppGenericClass IEnumerator_1_t5807_GenericClass;
TypeInfo IEnumerator_1_t5807_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5807_MethodInfos/* methods */
	, IEnumerator_1_t5807_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5807_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5807_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5807_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5807_0_0_0/* byval_arg */
	, &IEnumerator_1_t5807_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5807_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
// System.Array/InternalEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_61.h"
#ifndef _MSC_VER
#else
#endif
extern TypeInfo InternalEnumerator_1_t2830_il2cpp_TypeInfo;
// System.Array/InternalEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_61MethodDeclarations.h"

extern TypeInfo ImageTargetAbstractBehaviour_t50_il2cpp_TypeInfo;
extern MethodInfo InternalEnumerator_1_get_Current_m14511_MethodInfo;
extern MethodInfo Array_InternalArray__get_Item_TisImageTargetAbstractBehaviour_t50_m32142_MethodInfo;
struct Array_t;
// Declaration !!0 System.Array::InternalArray__get_Item<Vuforia.ImageTargetAbstractBehaviour>(System.Int32)
// !!0 System.Array::InternalArray__get_Item<Vuforia.ImageTargetAbstractBehaviour>(System.Int32)
#define Array_InternalArray__get_Item_TisImageTargetAbstractBehaviour_t50_m32142(__this, p0, method) (ImageTargetAbstractBehaviour_t50 *)Array_InternalArray__get_Item_TisObject_t_m31436_gshared((Array_t *)__this, (int32_t)p0, method)


// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::.ctor(System.Array)
// System.Object System.Array/InternalEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::Dispose()
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::MoveNext()
// T System.Array/InternalEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::get_Current()
// Metadata Definition System.Array/InternalEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>
extern Il2CppType Array_t_0_0_1;
FieldInfo InternalEnumerator_1_t2830____array_0_FieldInfo = 
{
	"array"/* name */
	, &Array_t_0_0_1/* type */
	, &InternalEnumerator_1_t2830_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2830, ___array_0) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern Il2CppType Int32_t93_0_0_1;
FieldInfo InternalEnumerator_1_t2830____idx_1_FieldInfo = 
{
	"idx"/* name */
	, &Int32_t93_0_0_1/* type */
	, &InternalEnumerator_1_t2830_il2cpp_TypeInfo/* parent */
	, offsetof(InternalEnumerator_1_t2830, ___idx_1) + sizeof(Object_t)/* data */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static FieldInfo* InternalEnumerator_1_t2830_FieldInfos[] =
{
	&InternalEnumerator_1_t2830____array_0_FieldInfo,
	&InternalEnumerator_1_t2830____idx_1_FieldInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14508_MethodInfo;
static PropertyInfo InternalEnumerator_1_t2830____System_Collections_IEnumerator_Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2830_il2cpp_TypeInfo/* parent */
	, "System.Collections.IEnumerator.Current"/* name */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14508_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo InternalEnumerator_1_t2830____Current_PropertyInfo = 
{
	&InternalEnumerator_1_t2830_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &InternalEnumerator_1_get_Current_m14511_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* InternalEnumerator_1_t2830_PropertyInfos[] =
{
	&InternalEnumerator_1_t2830____System_Collections_IEnumerator_Current_PropertyInfo,
	&InternalEnumerator_1_t2830____Current_PropertyInfo,
	NULL
};
extern Il2CppType Array_t_0_0_0;
static ParameterInfo InternalEnumerator_1_t2830_InternalEnumerator_1__ctor_m14507_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &Array_t_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1__ctor_m14507_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::.ctor(System.Array)
MethodInfo InternalEnumerator_1__ctor_m14507_MethodInfo = 
{
	".ctor"/* name */
	, (methodPointerType)&InternalEnumerator_1__ctor_m13978_gshared/* method */
	, &InternalEnumerator_1_t2830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, InternalEnumerator_1_t2830_InternalEnumerator_1__ctor_m14507_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 6275/* flags */
	, 0/* iflags */
	, 255/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1__ctor_m14507_GenericMethod/* genericMethod */

};
extern Il2CppType Object_t_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14508_GenericMethod;
// System.Object System.Array/InternalEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
MethodInfo InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14508_MethodInfo = 
{
	"System.Collections.IEnumerator.get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared/* method */
	, &InternalEnumerator_1_t2830_il2cpp_TypeInfo/* declaring_type */
	, &Object_t_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2529/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14508_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_Dispose_m14509_GenericMethod;
// System.Void System.Array/InternalEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::Dispose()
MethodInfo InternalEnumerator_1_Dispose_m14509_MethodInfo = 
{
	"Dispose"/* name */
	, (methodPointerType)&InternalEnumerator_1_Dispose_m13982_gshared/* method */
	, &InternalEnumerator_1_t2830_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_Dispose_m14509_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_MoveNext_m14510_GenericMethod;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::MoveNext()
MethodInfo InternalEnumerator_1_MoveNext_m14510_MethodInfo = 
{
	"MoveNext"/* name */
	, (methodPointerType)&InternalEnumerator_1_MoveNext_m13984_gshared/* method */
	, &InternalEnumerator_1_t2830_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 486/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_MoveNext_m14510_GenericMethod/* genericMethod */

};
extern Il2CppType ImageTargetAbstractBehaviour_t50_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod InternalEnumerator_1_get_Current_m14511_GenericMethod;
// T System.Array/InternalEnumerator`1<Vuforia.ImageTargetAbstractBehaviour>::get_Current()
MethodInfo InternalEnumerator_1_get_Current_m14511_MethodInfo = 
{
	"get_Current"/* name */
	, (methodPointerType)&InternalEnumerator_1_get_Current_m13986_gshared/* method */
	, &InternalEnumerator_1_t2830_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetAbstractBehaviour_t50_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 2534/* flags */
	, 0/* iflags */
	, 7/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &InternalEnumerator_1_get_Current_m14511_GenericMethod/* genericMethod */

};
static MethodInfo* InternalEnumerator_1_t2830_MethodInfos[] =
{
	&InternalEnumerator_1__ctor_m14507_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14508_MethodInfo,
	&InternalEnumerator_1_Dispose_m14509_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14510_MethodInfo,
	&InternalEnumerator_1_get_Current_m14511_MethodInfo,
	NULL
};
extern MethodInfo InternalEnumerator_1_MoveNext_m14510_MethodInfo;
extern MethodInfo InternalEnumerator_1_Dispose_m14509_MethodInfo;
static MethodInfo* InternalEnumerator_1_t2830_VTable[] =
{
	&ValueType_Equals_m1961_MethodInfo,
	&Object_Finalize_m192_MethodInfo,
	&ValueType_GetHashCode_m1962_MethodInfo,
	&ValueType_ToString_m2052_MethodInfo,
	&InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14508_MethodInfo,
	&InternalEnumerator_1_MoveNext_m14510_MethodInfo,
	&InternalEnumerator_1_Dispose_m14509_MethodInfo,
	&InternalEnumerator_1_get_Current_m14511_MethodInfo,
};
static TypeInfo* InternalEnumerator_1_t2830_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
	&IEnumerator_1_t5807_il2cpp_TypeInfo,
};
static Il2CppInterfaceOffsetPair InternalEnumerator_1_t2830_InterfacesOffsets[] = 
{
	{ &IEnumerator_t266_il2cpp_TypeInfo, 4},
	{ &IDisposable_t139_il2cpp_TypeInfo, 6},
	{ &IEnumerator_1_t5807_il2cpp_TypeInfo, 7},
};
extern TypeInfo ImageTargetAbstractBehaviour_t50_il2cpp_TypeInfo;
static Il2CppRGCTXData InternalEnumerator_1_t2830_RGCTXData[3] = 
{
	&InternalEnumerator_1_get_Current_m14511_MethodInfo/* Method Usage */,
	&ImageTargetAbstractBehaviour_t50_il2cpp_TypeInfo/* Class Usage */,
	&Array_InternalArray__get_Item_TisImageTargetAbstractBehaviour_t50_m32142_MethodInfo/* Method Usage */,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType InternalEnumerator_1_t2830_0_0_0;
extern Il2CppType InternalEnumerator_1_t2830_1_0_0;
extern Il2CppGenericClass InternalEnumerator_1_t2830_GenericClass;
TypeInfo InternalEnumerator_1_t2830_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "InternalEnumerator`1"/* name */
	, ""/* namespaze */
	, InternalEnumerator_1_t2830_MethodInfos/* methods */
	, InternalEnumerator_1_t2830_PropertyInfos/* properties */
	, InternalEnumerator_1_t2830_FieldInfos/* fields */
	, NULL/* events */
	, &ValueType_t436_il2cpp_TypeInfo/* parent */
	, NULL/* nested_types */
	, &Array_t_il2cpp_TypeInfo/* nested_in */
	, &InternalEnumerator_1_t2830_il2cpp_TypeInfo/* element_class */
	, InternalEnumerator_1_t2830_InterfacesTypeInfos/* implemented_interfaces */
	, InternalEnumerator_1_t2830_VTable/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &InternalEnumerator_1_t2830_il2cpp_TypeInfo/* cast_class */
	, &InternalEnumerator_1_t2830_0_0_0/* byval_arg */
	, &InternalEnumerator_1_t2830_1_0_0/* this_arg */
	, InternalEnumerator_1_t2830_InterfacesOffsets/* interface_offsets */
	, &InternalEnumerator_1_t2830_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, InternalEnumerator_1_t2830_RGCTXData/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, sizeof (InternalEnumerator_1_t2830)+ sizeof (Il2CppObject)/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 1048845/* flags */
	, 0/* rank */
	, true/* valuetype */
	, false/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 2/* property_count */
	, 2/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 8/* vtable_count */
	, 3/* interfaces_count */
	, 3/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IList_1_t7381_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.IList`1<Vuforia.ImageTargetAbstractBehaviour>::IndexOf(T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetAbstractBehaviour>::Insert(System.Int32,T)
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetAbstractBehaviour>::RemoveAt(System.Int32)
// T System.Collections.Generic.IList`1<Vuforia.ImageTargetAbstractBehaviour>::get_Item(System.Int32)
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetAbstractBehaviour>::set_Item(System.Int32,T)
// Metadata Definition System.Collections.Generic.IList`1<Vuforia.ImageTargetAbstractBehaviour>
extern MethodInfo IList_1_get_Item_m41950_MethodInfo;
extern MethodInfo IList_1_set_Item_m41951_MethodInfo;
static PropertyInfo IList_1_t7381____Item_PropertyInfo = 
{
	&IList_1_t7381_il2cpp_TypeInfo/* parent */
	, "Item"/* name */
	, &IList_1_get_Item_m41950_MethodInfo/* get */
	, &IList_1_set_Item_m41951_MethodInfo/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IList_1_t7381_PropertyInfos[] =
{
	&IList_1_t7381____Item_PropertyInfo,
	NULL
};
extern Il2CppType ImageTargetAbstractBehaviour_t50_0_0_0;
static ParameterInfo IList_1_t7381_IList_1_IndexOf_m41952_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &ImageTargetAbstractBehaviour_t50_0_0_0},
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_IndexOf_m41952_GenericMethod;
// System.Int32 System.Collections.Generic.IList`1<Vuforia.ImageTargetAbstractBehaviour>::IndexOf(T)
MethodInfo IList_1_IndexOf_m41952_MethodInfo = 
{
	"IndexOf"/* name */
	, NULL/* method */
	, &IList_1_t7381_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7381_IList_1_IndexOf_m41952_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_IndexOf_m41952_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ImageTargetAbstractBehaviour_t50_0_0_0;
static ParameterInfo IList_1_t7381_IList_1_Insert_m41953_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"item", 1, 134217728, &EmptyCustomAttributesCache, &ImageTargetAbstractBehaviour_t50_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_Insert_m41953_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetAbstractBehaviour>::Insert(System.Int32,T)
MethodInfo IList_1_Insert_m41953_MethodInfo = 
{
	"Insert"/* name */
	, NULL/* method */
	, &IList_1_t7381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7381_IList_1_Insert_m41953_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_Insert_m41953_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7381_IList_1_RemoveAt_m41954_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_RemoveAt_m41954_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetAbstractBehaviour>::RemoveAt(System.Int32)
MethodInfo IList_1_RemoveAt_m41954_MethodInfo = 
{
	"RemoveAt"/* name */
	, NULL/* method */
	, &IList_1_t7381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93/* invoker_method */
	, IList_1_t7381_IList_1_RemoveAt_m41954_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_RemoveAt_m41954_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo IList_1_t7381_IList_1_get_Item_m41950_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType ImageTargetAbstractBehaviour_t50_0_0_0;
extern void* RuntimeInvoker_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_get_Item_m41950_GenericMethod;
// T System.Collections.Generic.IList`1<Vuforia.ImageTargetAbstractBehaviour>::get_Item(System.Int32)
MethodInfo IList_1_get_Item_m41950_MethodInfo = 
{
	"get_Item"/* name */
	, NULL/* method */
	, &IList_1_t7381_il2cpp_TypeInfo/* declaring_type */
	, &ImageTargetAbstractBehaviour_t50_0_0_0/* return_type */
	, RuntimeInvoker_Object_t_Int32_t93/* invoker_method */
	, IList_1_t7381_IList_1_get_Item_m41950_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_get_Item_m41950_GenericMethod/* genericMethod */

};
extern Il2CppType Int32_t93_0_0_0;
extern Il2CppType ImageTargetAbstractBehaviour_t50_0_0_0;
static ParameterInfo IList_1_t7381_IList_1_set_Item_m41951_ParameterInfos[] = 
{
	{"index", 0, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
	{"value", 1, 134217728, &EmptyCustomAttributesCache, &ImageTargetAbstractBehaviour_t50_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Int32_t93_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IList_1_set_Item_m41951_GenericMethod;
// System.Void System.Collections.Generic.IList`1<Vuforia.ImageTargetAbstractBehaviour>::set_Item(System.Int32,T)
MethodInfo IList_1_set_Item_m41951_MethodInfo = 
{
	"set_Item"/* name */
	, NULL/* method */
	, &IList_1_t7381_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Int32_t93_Object_t/* invoker_method */
	, IList_1_t7381_IList_1_set_Item_m41951_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IList_1_set_Item_m41951_GenericMethod/* genericMethod */

};
static MethodInfo* IList_1_t7381_MethodInfos[] =
{
	&IList_1_IndexOf_m41952_MethodInfo,
	&IList_1_Insert_m41953_MethodInfo,
	&IList_1_RemoveAt_m41954_MethodInfo,
	&IList_1_get_Item_m41950_MethodInfo,
	&IList_1_set_Item_m41951_MethodInfo,
	NULL
};
static TypeInfo* IList_1_t7381_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&ICollection_1_t7380_il2cpp_TypeInfo,
	&IEnumerable_1_t7382_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IList_1_t7381_0_0_0;
extern Il2CppType IList_1_t7381_1_0_0;
struct IList_1_t7381;
extern Il2CppGenericClass IList_1_t7381_GenericClass;
extern CustomAttributesCache IList_1_t2485__CustomAttributeCache;
TypeInfo IList_1_t7381_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IList`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IList_1_t7381_MethodInfos/* methods */
	, IList_1_t7381_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IList_1_t7381_il2cpp_TypeInfo/* element_class */
	, IList_1_t7381_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &IList_1_t2485__CustomAttributeCache/* custom_attributes_cache */
	, &IList_1_t7381_il2cpp_TypeInfo/* cast_class */
	, &IList_1_t7381_0_0_0/* byval_arg */
	, &IList_1_t7381_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IList_1_t7381_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 5/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 3/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo ICollection_1_t7383_il2cpp_TypeInfo;



// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::get_Count()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::get_IsReadOnly()
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::Add(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::Clear()
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::Contains(T)
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::CopyTo(T[],System.Int32)
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::Remove(T)
// Metadata Definition System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>
extern MethodInfo ICollection_1_get_Count_m41955_MethodInfo;
static PropertyInfo ICollection_1_t7383____Count_PropertyInfo = 
{
	&ICollection_1_t7383_il2cpp_TypeInfo/* parent */
	, "Count"/* name */
	, &ICollection_1_get_Count_m41955_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
extern MethodInfo ICollection_1_get_IsReadOnly_m41956_MethodInfo;
static PropertyInfo ICollection_1_t7383____IsReadOnly_PropertyInfo = 
{
	&ICollection_1_t7383_il2cpp_TypeInfo/* parent */
	, "IsReadOnly"/* name */
	, &ICollection_1_get_IsReadOnly_m41956_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* ICollection_1_t7383_PropertyInfos[] =
{
	&ICollection_1_t7383____Count_PropertyInfo,
	&ICollection_1_t7383____IsReadOnly_PropertyInfo,
	NULL
};
extern Il2CppType Int32_t93_0_0_0;
extern void* RuntimeInvoker_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_Count_m41955_GenericMethod;
// System.Int32 System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::get_Count()
MethodInfo ICollection_1_get_Count_m41955_MethodInfo = 
{
	"get_Count"/* name */
	, NULL/* method */
	, &ICollection_1_t7383_il2cpp_TypeInfo/* declaring_type */
	, &Int32_t93_0_0_0/* return_type */
	, RuntimeInvoker_Int32_t93/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_Count_m41955_GenericMethod/* genericMethod */

};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_get_IsReadOnly_m41956_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::get_IsReadOnly()
MethodInfo ICollection_1_get_IsReadOnly_m41956_MethodInfo = 
{
	"get_IsReadOnly"/* name */
	, NULL/* method */
	, &ICollection_1_t7383_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 1/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_get_IsReadOnly_m41956_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorImageTargetBehaviour_t136_0_0_0;
extern Il2CppType IEditorImageTargetBehaviour_t136_0_0_0;
static ParameterInfo ICollection_1_t7383_ICollection_1_Add_m41957_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorImageTargetBehaviour_t136_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Add_m41957_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::Add(T)
MethodInfo ICollection_1_Add_m41957_MethodInfo = 
{
	"Add"/* name */
	, NULL/* method */
	, &ICollection_1_t7383_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t/* invoker_method */
	, ICollection_1_t7383_ICollection_1_Add_m41957_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 2/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Add_m41957_GenericMethod/* genericMethod */

};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Clear_m41958_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::Clear()
MethodInfo ICollection_1_Clear_m41958_MethodInfo = 
{
	"Clear"/* name */
	, NULL/* method */
	, &ICollection_1_t7383_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 3/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Clear_m41958_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorImageTargetBehaviour_t136_0_0_0;
static ParameterInfo ICollection_1_t7383_ICollection_1_Contains_m41959_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorImageTargetBehaviour_t136_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Contains_m41959_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::Contains(T)
MethodInfo ICollection_1_Contains_m41959_MethodInfo = 
{
	"Contains"/* name */
	, NULL/* method */
	, &ICollection_1_t7383_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7383_ICollection_1_Contains_m41959_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 4/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Contains_m41959_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorImageTargetBehaviourU5BU5D_t5503_0_0_0;
extern Il2CppType IEditorImageTargetBehaviourU5BU5D_t5503_0_0_0;
extern Il2CppType Int32_t93_0_0_0;
static ParameterInfo ICollection_1_t7383_ICollection_1_CopyTo_m41960_ParameterInfos[] = 
{
	{"array", 0, 134217728, &EmptyCustomAttributesCache, &IEditorImageTargetBehaviourU5BU5D_t5503_0_0_0},
	{"arrayIndex", 1, 134217728, &EmptyCustomAttributesCache, &Int32_t93_0_0_0},
};
extern Il2CppType Void_t99_0_0_0;
extern void* RuntimeInvoker_Void_t99_Object_t_Int32_t93 (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_CopyTo_m41960_GenericMethod;
// System.Void System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::CopyTo(T[],System.Int32)
MethodInfo ICollection_1_CopyTo_m41960_MethodInfo = 
{
	"CopyTo"/* name */
	, NULL/* method */
	, &ICollection_1_t7383_il2cpp_TypeInfo/* declaring_type */
	, &Void_t99_0_0_0/* return_type */
	, RuntimeInvoker_Void_t99_Object_t_Int32_t93/* invoker_method */
	, ICollection_1_t7383_ICollection_1_CopyTo_m41960_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 5/* slot */
	, 2/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_CopyTo_m41960_GenericMethod/* genericMethod */

};
extern Il2CppType IEditorImageTargetBehaviour_t136_0_0_0;
static ParameterInfo ICollection_1_t7383_ICollection_1_Remove_m41961_ParameterInfos[] = 
{
	{"item", 0, 134217728, &EmptyCustomAttributesCache, &IEditorImageTargetBehaviour_t136_0_0_0},
};
extern Il2CppType Boolean_t106_0_0_0;
extern void* RuntimeInvoker_Boolean_t106_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod ICollection_1_Remove_m41961_GenericMethod;
// System.Boolean System.Collections.Generic.ICollection`1<Vuforia.IEditorImageTargetBehaviour>::Remove(T)
MethodInfo ICollection_1_Remove_m41961_MethodInfo = 
{
	"Remove"/* name */
	, NULL/* method */
	, &ICollection_1_t7383_il2cpp_TypeInfo/* declaring_type */
	, &Boolean_t106_0_0_0/* return_type */
	, RuntimeInvoker_Boolean_t106_Object_t/* invoker_method */
	, ICollection_1_t7383_ICollection_1_Remove_m41961_ParameterInfos/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 6/* slot */
	, 1/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &ICollection_1_Remove_m41961_GenericMethod/* genericMethod */

};
static MethodInfo* ICollection_1_t7383_MethodInfos[] =
{
	&ICollection_1_get_Count_m41955_MethodInfo,
	&ICollection_1_get_IsReadOnly_m41956_MethodInfo,
	&ICollection_1_Add_m41957_MethodInfo,
	&ICollection_1_Clear_m41958_MethodInfo,
	&ICollection_1_Contains_m41959_MethodInfo,
	&ICollection_1_CopyTo_m41960_MethodInfo,
	&ICollection_1_Remove_m41961_MethodInfo,
	NULL
};
extern TypeInfo IEnumerable_1_t7385_il2cpp_TypeInfo;
static TypeInfo* ICollection_1_t7383_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
	&IEnumerable_1_t7385_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType ICollection_1_t7383_0_0_0;
extern Il2CppType ICollection_1_t7383_1_0_0;
struct ICollection_1_t7383;
extern Il2CppGenericClass ICollection_1_t7383_GenericClass;
TypeInfo ICollection_1_t7383_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "ICollection`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, ICollection_1_t7383_MethodInfos/* methods */
	, ICollection_1_t7383_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &ICollection_1_t7383_il2cpp_TypeInfo/* element_class */
	, ICollection_1_t7383_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &ICollection_1_t7383_il2cpp_TypeInfo/* cast_class */
	, &ICollection_1_t7383_0_0_0/* byval_arg */
	, &ICollection_1_t7383_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &ICollection_1_t7383_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 7/* method_count */
	, 2/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif



// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorImageTargetBehaviour>::GetEnumerator()
// Metadata Definition System.Collections.Generic.IEnumerable`1<Vuforia.IEditorImageTargetBehaviour>
extern Il2CppType IEnumerator_1_t5809_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerable_1_GetEnumerator_m41962_GenericMethod;
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.IEnumerable`1<Vuforia.IEditorImageTargetBehaviour>::GetEnumerator()
MethodInfo IEnumerable_1_GetEnumerator_m41962_MethodInfo = 
{
	"GetEnumerator"/* name */
	, NULL/* method */
	, &IEnumerable_1_t7385_il2cpp_TypeInfo/* declaring_type */
	, &IEnumerator_1_t5809_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 1478/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerable_1_GetEnumerator_m41962_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerable_1_t7385_MethodInfos[] =
{
	&IEnumerable_1_GetEnumerator_m41962_MethodInfo,
	NULL
};
static TypeInfo* IEnumerable_1_t7385_InterfacesTypeInfos[] = 
{
	&IEnumerable_t1126_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerable_1_t7385_0_0_0;
extern Il2CppType IEnumerable_1_t7385_1_0_0;
struct IEnumerable_1_t7385;
extern Il2CppGenericClass IEnumerable_1_t7385_GenericClass;
TypeInfo IEnumerable_1_t7385_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerable`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerable_1_t7385_MethodInfos/* methods */
	, NULL/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerable_1_t7385_il2cpp_TypeInfo/* element_class */
	, IEnumerable_1_t7385_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerable_1_t7385_il2cpp_TypeInfo/* cast_class */
	, &IEnumerable_1_t7385_0_0_0/* byval_arg */
	, &IEnumerable_1_t7385_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerable_1_t7385_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 0/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 1/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifndef _MSC_VER
#else
#endif
extern TypeInfo IEnumerator_1_t5809_il2cpp_TypeInfo;



// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorImageTargetBehaviour>::get_Current()
// Metadata Definition System.Collections.Generic.IEnumerator`1<Vuforia.IEditorImageTargetBehaviour>
extern MethodInfo IEnumerator_1_get_Current_m41963_MethodInfo;
static PropertyInfo IEnumerator_1_t5809____Current_PropertyInfo = 
{
	&IEnumerator_1_t5809_il2cpp_TypeInfo/* parent */
	, "Current"/* name */
	, &IEnumerator_1_get_Current_m41963_MethodInfo/* get */
	, NULL/* set */
	, 0/* attrs */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */

};
static PropertyInfo* IEnumerator_1_t5809_PropertyInfos[] =
{
	&IEnumerator_1_t5809____Current_PropertyInfo,
	NULL
};
extern Il2CppType IEditorImageTargetBehaviour_t136_0_0_0;
extern void* RuntimeInvoker_Object_t (MethodInfo* method, void* obj, void** args);
extern Il2CppGenericMethod IEnumerator_1_get_Current_m41963_GenericMethod;
// T System.Collections.Generic.IEnumerator`1<Vuforia.IEditorImageTargetBehaviour>::get_Current()
MethodInfo IEnumerator_1_get_Current_m41963_MethodInfo = 
{
	"get_Current"/* name */
	, NULL/* method */
	, &IEnumerator_1_t5809_il2cpp_TypeInfo/* declaring_type */
	, &IEditorImageTargetBehaviour_t136_0_0_0/* return_type */
	, RuntimeInvoker_Object_t/* invoker_method */
	, NULL/* parameters */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, 3526/* flags */
	, 0/* iflags */
	, 0/* slot */
	, 0/* parameters_count */
	, false/* is_generic */
	, true/* is_inflated */
	, 0/* token */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* native_delegate_wrapper */
	, &IEnumerator_1_get_Current_m41963_GenericMethod/* genericMethod */

};
static MethodInfo* IEnumerator_1_t5809_MethodInfos[] =
{
	&IEnumerator_1_get_Current_m41963_MethodInfo,
	NULL
};
static TypeInfo* IEnumerator_1_t5809_InterfacesTypeInfos[] = 
{
	&IEnumerator_t266_il2cpp_TypeInfo,
	&IDisposable_t139_il2cpp_TypeInfo,
};
extern Il2CppImage g_mscorlib_dll_Image;
extern Il2CppType IEnumerator_1_t5809_0_0_0;
extern Il2CppType IEnumerator_1_t5809_1_0_0;
struct IEnumerator_1_t5809;
extern Il2CppGenericClass IEnumerator_1_t5809_GenericClass;
TypeInfo IEnumerator_1_t5809_il2cpp_TypeInfo = 
{
	&g_mscorlib_dll_Image/* image */
	, NULL/* gc_desc */
	, "IEnumerator`1"/* name */
	, "System.Collections.Generic"/* namespaze */
	, IEnumerator_1_t5809_MethodInfos/* methods */
	, IEnumerator_1_t5809_PropertyInfos/* properties */
	, NULL/* fields */
	, NULL/* events */
	, NULL/* parent */
	, NULL/* nested_types */
	, NULL/* nested_in */
	, &IEnumerator_1_t5809_il2cpp_TypeInfo/* element_class */
	, IEnumerator_1_t5809_InterfacesTypeInfos/* implemented_interfaces */
	, NULL/* vtable */
	, &EmptyCustomAttributesCache/* custom_attributes_cache */
	, &IEnumerator_1_t5809_il2cpp_TypeInfo/* cast_class */
	, &IEnumerator_1_t5809_0_0_0/* byval_arg */
	, &IEnumerator_1_t5809_1_0_0/* this_arg */
	, NULL/* interface_offsets */
	, &IEnumerator_1_t5809_GenericClass/* generic_class */
	, NULL/* generic_container */
	, NULL/* field_def_values */
	, NULL/* static_fields */
	, NULL/* rgctx_data */
	, (methodPointerType)NULL/* pinvoke_delegate_wrapper */
	, (methodPointerType)NULL/* marshal_to_native_func */
	, (methodPointerType)NULL/* marshal_from_native_func */
	, (methodPointerType)NULL/* marshal_cleanup_func */
	, 0/* instance_size */
	, 0/* element_size */
	, -1/* native_size */
	, 0/* static_fields_size */
	, 0/* thread_static_fields_size */
	, -1/* thread_static_fields_offset */
	, 161/* flags */
	, 0/* rank */
	, false/* valuetype */
	, true/* is_interface */
	, false/* initialized */
	, false/* enumtype */
	, true/* is_generic */
	, false/* has_references */
	, false/* init_pending */
	, false/* size_inited */
	, false/* has_finalize */
	, false/* has_cctor */
	, false/* is_blittable */
	, false/* is_pinnable */
	, 1/* method_count */
	, 1/* property_count */
	, 0/* field_count */
	, 0/* event_count */
	, 0/* nested_type_count */
	, 0/* vtable_count */
	, 2/* interfaces_count */
	, 0/* interface_offsets_count */

};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
