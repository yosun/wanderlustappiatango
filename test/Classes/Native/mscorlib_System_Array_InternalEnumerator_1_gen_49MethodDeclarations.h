﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>
struct InternalEnumerator_1_t2797;
// System.Object
struct Object_t;
// Vuforia.DataSetLoadAbstractBehaviour
struct DataSetLoadAbstractBehaviour_t36;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m14377(__this, ___array, method) (void)InternalEnumerator_1__ctor_m13978_gshared((InternalEnumerator_1_t2698 *)__this, (Array_t *)___array, method)
// System.Object System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14378(__this, method) (Object_t *)InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13980_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Void System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::Dispose()
#define InternalEnumerator_1_Dispose_m14379(__this, method) (void)InternalEnumerator_1_Dispose_m13982_gshared((InternalEnumerator_1_t2698 *)__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::MoveNext()
#define InternalEnumerator_1_MoveNext_m14380(__this, method) (bool)InternalEnumerator_1_MoveNext_m13984_gshared((InternalEnumerator_1_t2698 *)__this, method)
// T System.Array/InternalEnumerator`1<Vuforia.DataSetLoadAbstractBehaviour>::get_Current()
#define InternalEnumerator_1_get_Current_m14381(__this, method) (DataSetLoadAbstractBehaviour_t36 *)InternalEnumerator_1_get_Current_m13986_gshared((InternalEnumerator_1_t2698 *)__this, method)
