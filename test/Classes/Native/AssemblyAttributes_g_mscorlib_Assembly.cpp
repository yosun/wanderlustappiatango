﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
extern TypeInfo RuntimeCompatibilityAttribute_t179_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
extern MethodInfo RuntimeCompatibilityAttribute__ctor_m676_MethodInfo;
extern MethodInfo RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m677_MethodInfo;
extern TypeInfo ComVisibleAttribute_t537_il2cpp_TypeInfo;
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
extern MethodInfo ComVisibleAttribute__ctor_m2588_MethodInfo;
extern TypeInfo AssemblyFileVersionAttribute_t535_il2cpp_TypeInfo;
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
extern MethodInfo AssemblyFileVersionAttribute__ctor_m2586_MethodInfo;
extern TypeInfo AssemblyKeyFileAttribute_t1283_il2cpp_TypeInfo;
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
extern MethodInfo AssemblyKeyFileAttribute__ctor_m6714_MethodInfo;
extern TypeInfo SatelliteContractVersionAttribute_t1281_il2cpp_TypeInfo;
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttribute.h"
// System.Resources.SatelliteContractVersionAttribute
#include "mscorlib_System_Resources_SatelliteContractVersionAttributeMethodDeclarations.h"
extern MethodInfo SatelliteContractVersionAttribute__ctor_m6711_MethodInfo;
extern TypeInfo NeutralResourcesLanguageAttribute_t1286_il2cpp_TypeInfo;
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
extern MethodInfo NeutralResourcesLanguageAttribute__ctor_m6717_MethodInfo;
extern TypeInfo AssemblyProductAttribute_t533_il2cpp_TypeInfo;
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
extern MethodInfo AssemblyProductAttribute__ctor_m2584_MethodInfo;
extern TypeInfo AssemblyDescriptionAttribute_t530_il2cpp_TypeInfo;
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
extern MethodInfo AssemblyDescriptionAttribute__ctor_m2581_MethodInfo;
extern TypeInfo GuidAttribute_t536_il2cpp_TypeInfo;
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttributeMethodDeclarations.h"
extern MethodInfo GuidAttribute__ctor_m2587_MethodInfo;
extern TypeInfo CompilationRelaxationsAttribute_t906_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
extern MethodInfo CompilationRelaxationsAttribute__ctor_m6713_MethodInfo;
extern TypeInfo AssemblyTitleAttribute_t529_il2cpp_TypeInfo;
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
extern MethodInfo AssemblyTitleAttribute__ctor_m2580_MethodInfo;
extern TypeInfo DebuggableAttribute_t905_il2cpp_TypeInfo;
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
extern MethodInfo DebuggableAttribute__ctor_m5419_MethodInfo;
extern TypeInfo AssemblyCompanyAttribute_t532_il2cpp_TypeInfo;
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
extern MethodInfo AssemblyCompanyAttribute__ctor_m2583_MethodInfo;
extern TypeInfo CLSCompliantAttribute_t1285_il2cpp_TypeInfo;
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
extern MethodInfo CLSCompliantAttribute__ctor_m6716_MethodInfo;
extern TypeInfo AssemblyDelaySignAttribute_t1284_il2cpp_TypeInfo;
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
extern MethodInfo AssemblyDelaySignAttribute__ctor_m6715_MethodInfo;
extern TypeInfo AssemblyDefaultAliasAttribute_t1282_il2cpp_TypeInfo;
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
#include "mscorlib_System_Reflection_AssemblyDefaultAliasAttributeMethodDeclarations.h"
extern MethodInfo AssemblyDefaultAliasAttribute__ctor_m6712_MethodInfo;
extern TypeInfo AssemblyCopyrightAttribute_t534_il2cpp_TypeInfo;
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
extern MethodInfo AssemblyCopyrightAttribute__ctor_m2585_MethodInfo;
extern TypeInfo AssemblyInformationalVersionAttribute_t1280_il2cpp_TypeInfo;
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttri.h"
// System.Reflection.AssemblyInformationalVersionAttribute
#include "mscorlib_System_Reflection_AssemblyInformationalVersionAttriMethodDeclarations.h"
extern MethodInfo AssemblyInformationalVersionAttribute__ctor_m6710_MethodInfo;
extern TypeInfo StringFreezingAttribute_t1960_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttri.h"
// System.Runtime.CompilerServices.StringFreezingAttribute
#include "mscorlib_System_Runtime_CompilerServices_StringFreezingAttriMethodDeclarations.h"
extern MethodInfo StringFreezingAttribute__ctor_m11402_MethodInfo;
extern TypeInfo TypeLibVersionAttribute_t1978_il2cpp_TypeInfo;
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttrib.h"
// System.Runtime.InteropServices.TypeLibVersionAttribute
#include "mscorlib_System_Runtime_InteropServices_TypeLibVersionAttribMethodDeclarations.h"
extern MethodInfo TypeLibVersionAttribute__ctor_m11438_MethodInfo;
extern TypeInfo DefaultDependencyAttribute_t1957_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAt.h"
// System.Runtime.CompilerServices.DefaultDependencyAttribute
#include "mscorlib_System_Runtime_CompilerServices_DefaultDependencyAtMethodDeclarations.h"
extern MethodInfo DefaultDependencyAttribute__ctor_m11401_MethodInfo;
void g_mscorlib_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RuntimeCompatibilityAttribute_t179 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t179 *)il2cpp_codegen_object_new (&RuntimeCompatibilityAttribute_t179_il2cpp_TypeInfo);
		RuntimeCompatibilityAttribute__ctor_m676(tmp, &RuntimeCompatibilityAttribute__ctor_m676_MethodInfo);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m677(tmp, true, &RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m677_MethodInfo);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t537 * tmp;
		tmp = (ComVisibleAttribute_t537 *)il2cpp_codegen_object_new (&ComVisibleAttribute_t537_il2cpp_TypeInfo);
		ComVisibleAttribute__ctor_m2588(tmp, false, &ComVisibleAttribute__ctor_m2588_MethodInfo);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t535 * tmp;
		tmp = (AssemblyFileVersionAttribute_t535 *)il2cpp_codegen_object_new (&AssemblyFileVersionAttribute_t535_il2cpp_TypeInfo);
		AssemblyFileVersionAttribute__ctor_m2586(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), &AssemblyFileVersionAttribute__ctor_m2586_MethodInfo);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t1283 * tmp;
		tmp = (AssemblyKeyFileAttribute_t1283 *)il2cpp_codegen_object_new (&AssemblyKeyFileAttribute_t1283_il2cpp_TypeInfo);
		AssemblyKeyFileAttribute__ctor_m6714(tmp, il2cpp_codegen_string_new_wrapper("../silverlight.pub"), &AssemblyKeyFileAttribute__ctor_m6714_MethodInfo);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		SatelliteContractVersionAttribute_t1281 * tmp;
		tmp = (SatelliteContractVersionAttribute_t1281 *)il2cpp_codegen_object_new (&SatelliteContractVersionAttribute_t1281_il2cpp_TypeInfo);
		SatelliteContractVersionAttribute__ctor_m6711(tmp, il2cpp_codegen_string_new_wrapper("2.0.5.0"), &SatelliteContractVersionAttribute__ctor_m6711_MethodInfo);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		NeutralResourcesLanguageAttribute_t1286 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t1286 *)il2cpp_codegen_object_new (&NeutralResourcesLanguageAttribute_t1286_il2cpp_TypeInfo);
		NeutralResourcesLanguageAttribute__ctor_m6717(tmp, il2cpp_codegen_string_new_wrapper("en-US"), &NeutralResourcesLanguageAttribute__ctor_m6717_MethodInfo);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t533 * tmp;
		tmp = (AssemblyProductAttribute_t533 *)il2cpp_codegen_object_new (&AssemblyProductAttribute_t533_il2cpp_TypeInfo);
		AssemblyProductAttribute__ctor_m2584(tmp, il2cpp_codegen_string_new_wrapper("MONO Common language infrastructure"), &AssemblyProductAttribute__ctor_m2584_MethodInfo);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t530 * tmp;
		tmp = (AssemblyDescriptionAttribute_t530 *)il2cpp_codegen_object_new (&AssemblyDescriptionAttribute_t530_il2cpp_TypeInfo);
		AssemblyDescriptionAttribute__ctor_m2581(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), &AssemblyDescriptionAttribute__ctor_m2581_MethodInfo);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		GuidAttribute_t536 * tmp;
		tmp = (GuidAttribute_t536 *)il2cpp_codegen_object_new (&GuidAttribute_t536_il2cpp_TypeInfo);
		GuidAttribute__ctor_m2587(tmp, il2cpp_codegen_string_new_wrapper("BED7F4EA-1A96-11D2-8F08-00A0C9A6186D"), &GuidAttribute__ctor_m2587_MethodInfo);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t906 * tmp;
		tmp = (CompilationRelaxationsAttribute_t906 *)il2cpp_codegen_object_new (&CompilationRelaxationsAttribute_t906_il2cpp_TypeInfo);
		CompilationRelaxationsAttribute__ctor_m6713(tmp, 8, &CompilationRelaxationsAttribute__ctor_m6713_MethodInfo);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t529 * tmp;
		tmp = (AssemblyTitleAttribute_t529 *)il2cpp_codegen_object_new (&AssemblyTitleAttribute_t529_il2cpp_TypeInfo);
		AssemblyTitleAttribute__ctor_m2580(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), &AssemblyTitleAttribute__ctor_m2580_MethodInfo);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t905 * tmp;
		tmp = (DebuggableAttribute_t905 *)il2cpp_codegen_object_new (&DebuggableAttribute_t905_il2cpp_TypeInfo);
		DebuggableAttribute__ctor_m5419(tmp, 2, &DebuggableAttribute__ctor_m5419_MethodInfo);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t532 * tmp;
		tmp = (AssemblyCompanyAttribute_t532 *)il2cpp_codegen_object_new (&AssemblyCompanyAttribute_t532_il2cpp_TypeInfo);
		AssemblyCompanyAttribute__ctor_m2583(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), &AssemblyCompanyAttribute__ctor_m2583_MethodInfo);
		cache->attributes[12] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1285 * tmp;
		tmp = (CLSCompliantAttribute_t1285 *)il2cpp_codegen_object_new (&CLSCompliantAttribute_t1285_il2cpp_TypeInfo);
		CLSCompliantAttribute__ctor_m6716(tmp, true, &CLSCompliantAttribute__ctor_m6716_MethodInfo);
		cache->attributes[13] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t1284 * tmp;
		tmp = (AssemblyDelaySignAttribute_t1284 *)il2cpp_codegen_object_new (&AssemblyDelaySignAttribute_t1284_il2cpp_TypeInfo);
		AssemblyDelaySignAttribute__ctor_m6715(tmp, true, &AssemblyDelaySignAttribute__ctor_m6715_MethodInfo);
		cache->attributes[14] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDefaultAliasAttribute_t1282 * tmp;
		tmp = (AssemblyDefaultAliasAttribute_t1282 *)il2cpp_codegen_object_new (&AssemblyDefaultAliasAttribute_t1282_il2cpp_TypeInfo);
		AssemblyDefaultAliasAttribute__ctor_m6712(tmp, il2cpp_codegen_string_new_wrapper("mscorlib.dll"), &AssemblyDefaultAliasAttribute__ctor_m6712_MethodInfo);
		cache->attributes[15] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t534 * tmp;
		tmp = (AssemblyCopyrightAttribute_t534 *)il2cpp_codegen_object_new (&AssemblyCopyrightAttribute_t534_il2cpp_TypeInfo);
		AssemblyCopyrightAttribute__ctor_m2585(tmp, il2cpp_codegen_string_new_wrapper("(c) various MONO Authors"), &AssemblyCopyrightAttribute__ctor_m2585_MethodInfo);
		cache->attributes[16] = (Il2CppObject*)tmp;
	}
	{
		AssemblyInformationalVersionAttribute_t1280 * tmp;
		tmp = (AssemblyInformationalVersionAttribute_t1280 *)il2cpp_codegen_object_new (&AssemblyInformationalVersionAttribute_t1280_il2cpp_TypeInfo);
		AssemblyInformationalVersionAttribute__ctor_m6710(tmp, il2cpp_codegen_string_new_wrapper("3.0.40818.0"), &AssemblyInformationalVersionAttribute__ctor_m6710_MethodInfo);
		cache->attributes[17] = (Il2CppObject*)tmp;
	}
	{
		StringFreezingAttribute_t1960 * tmp;
		tmp = (StringFreezingAttribute_t1960 *)il2cpp_codegen_object_new (&StringFreezingAttribute_t1960_il2cpp_TypeInfo);
		StringFreezingAttribute__ctor_m11402(tmp, &StringFreezingAttribute__ctor_m11402_MethodInfo);
		cache->attributes[18] = (Il2CppObject*)tmp;
	}
	{
		TypeLibVersionAttribute_t1978 * tmp;
		tmp = (TypeLibVersionAttribute_t1978 *)il2cpp_codegen_object_new (&TypeLibVersionAttribute_t1978_il2cpp_TypeInfo);
		TypeLibVersionAttribute__ctor_m11438(tmp, 2, 0, &TypeLibVersionAttribute__ctor_m11438_MethodInfo);
		cache->attributes[19] = (Il2CppObject*)tmp;
	}
	{
		DefaultDependencyAttribute_t1957 * tmp;
		tmp = (DefaultDependencyAttribute_t1957 *)il2cpp_codegen_object_new (&DefaultDependencyAttribute_t1957_il2cpp_TypeInfo);
		DefaultDependencyAttribute__ctor_m11401(tmp, 1, &DefaultDependencyAttribute__ctor_m11401_MethodInfo);
		cache->attributes[20] = (Il2CppObject*)tmp;
	}
}
CustomAttributesCache g_mscorlib_Assembly__CustomAttributeCache = {
21,
NULL,
&g_mscorlib_Assembly_CustomAttributesCacheGenerator
};
