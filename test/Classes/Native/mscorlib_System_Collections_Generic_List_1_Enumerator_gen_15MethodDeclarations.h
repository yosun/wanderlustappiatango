﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>
struct Enumerator_t871;
// System.Object
struct Object_t;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t56;
// System.Collections.Generic.List`1<Vuforia.VirtualButtonAbstractBehaviour>
struct List_1_t868;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m25396(__this, ___l, method) (void)Enumerator__ctor_m14640_gshared((Enumerator_t2850 *)__this, (List_1_t150 *)___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m25397(__this, method) (Object_t *)Enumerator_System_Collections_IEnumerator_get_Current_m14641_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m25398(__this, method) (void)Enumerator_Dispose_m14642_gshared((Enumerator_t2850 *)__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m25399(__this, method) (void)Enumerator_VerifyState_m14643_gshared((Enumerator_t2850 *)__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m5255(__this, method) (bool)Enumerator_MoveNext_m14644_gshared((Enumerator_t2850 *)__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m5253(__this, method) (VirtualButtonAbstractBehaviour_t56 *)Enumerator_get_Current_m14645_gshared((Enumerator_t2850 *)__this, method)
