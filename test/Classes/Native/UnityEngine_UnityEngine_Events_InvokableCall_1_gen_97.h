﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.SurfaceAbstractBehaviour>
struct UnityAction_1_t3729;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceAbstractBehaviour>
struct InvokableCall_1_t3728  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.SurfaceAbstractBehaviour>::Delegate
	UnityAction_1_t3729 * ___Delegate_0;
};
