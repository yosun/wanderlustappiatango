﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARManagerImpl/AutoRotationState
struct AutoRotationState_t651;
struct AutoRotationState_t651_marshaled;

void AutoRotationState_t651_marshal(const AutoRotationState_t651& unmarshaled, AutoRotationState_t651_marshaled& marshaled);
void AutoRotationState_t651_marshal_back(const AutoRotationState_t651_marshaled& marshaled, AutoRotationState_t651& unmarshaled);
void AutoRotationState_t651_marshal_cleanup(AutoRotationState_t651_marshaled& marshaled);
