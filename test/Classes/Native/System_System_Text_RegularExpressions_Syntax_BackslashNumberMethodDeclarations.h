﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.RegularExpressions.Syntax.BackslashNumber
struct BackslashNumber_t1458;
// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t1295;
// System.Text.RegularExpressions.ICompiler
struct ICompiler_t1446;

// System.Void System.Text.RegularExpressions.Syntax.BackslashNumber::.ctor(System.Boolean,System.Boolean)
 void BackslashNumber__ctor_m7495 (BackslashNumber_t1458 * __this, bool ___ignore, bool ___ecma, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.RegularExpressions.Syntax.BackslashNumber::ResolveReference(System.String,System.Collections.Hashtable)
 bool BackslashNumber_ResolveReference_m7496 (BackslashNumber_t1458 * __this, String_t* ___num_str, Hashtable_t1295 * ___groups, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Syntax.BackslashNumber::Compile(System.Text.RegularExpressions.ICompiler,System.Boolean)
 void BackslashNumber_Compile_m7497 (BackslashNumber_t1458 * __this, Object_t * ___cmp, bool ___reverse, MethodInfo* method) IL2CPP_METHOD_ATTR;
