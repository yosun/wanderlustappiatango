﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CloudRecoImageTargetImpl
struct CloudRecoImageTargetImpl_t594;
// System.String
struct String_t;
// Vuforia.VirtualButton
struct VirtualButton_t595;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButton>
struct IEnumerable_1_t596;
// Vuforia.ImageTargetType
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetType.h"
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// Vuforia.RectangleData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_RectangleData.h"

// System.Void Vuforia.CloudRecoImageTargetImpl::.ctor(System.String,System.Int32,UnityEngine.Vector3)
 void CloudRecoImageTargetImpl__ctor_m2817 (CloudRecoImageTargetImpl_t594 * __this, String_t* ___name, int32_t ___id, Vector3_t13  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetType Vuforia.CloudRecoImageTargetImpl::get_ImageTargetType()
 int32_t CloudRecoImageTargetImpl_get_ImageTargetType_m2818 (CloudRecoImageTargetImpl_t594 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vuforia.CloudRecoImageTargetImpl::GetSize()
 Vector3_t13  CloudRecoImageTargetImpl_GetSize_m2819 (CloudRecoImageTargetImpl_t594 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.CloudRecoImageTargetImpl::SetSize(UnityEngine.Vector3)
 void CloudRecoImageTargetImpl_SetSize_m2820 (CloudRecoImageTargetImpl_t594 * __this, Vector3_t13  ___size, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButton Vuforia.CloudRecoImageTargetImpl::CreateVirtualButton(System.String,Vuforia.RectangleData)
 VirtualButton_t595 * CloudRecoImageTargetImpl_CreateVirtualButton_m2821 (CloudRecoImageTargetImpl_t594 * __this, String_t* ___name, RectangleData_t588  ___area, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.VirtualButton Vuforia.CloudRecoImageTargetImpl::GetVirtualButtonByName(System.String)
 VirtualButton_t595 * CloudRecoImageTargetImpl_GetVirtualButtonByName_m2822 (CloudRecoImageTargetImpl_t594 * __this, String_t* ___name, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.VirtualButton> Vuforia.CloudRecoImageTargetImpl::GetVirtualButtons()
 Object_t* CloudRecoImageTargetImpl_GetVirtualButtons_m2823 (CloudRecoImageTargetImpl_t594 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoImageTargetImpl::DestroyVirtualButton(Vuforia.VirtualButton)
 bool CloudRecoImageTargetImpl_DestroyVirtualButton_m2824 (CloudRecoImageTargetImpl_t594 * __this, VirtualButton_t595 * ___vb, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoImageTargetImpl::StartExtendedTracking()
 bool CloudRecoImageTargetImpl_StartExtendedTracking_m2825 (CloudRecoImageTargetImpl_t594 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.CloudRecoImageTargetImpl::StopExtendedTracking()
 bool CloudRecoImageTargetImpl_StopExtendedTracking_m2826 (CloudRecoImageTargetImpl_t594 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
