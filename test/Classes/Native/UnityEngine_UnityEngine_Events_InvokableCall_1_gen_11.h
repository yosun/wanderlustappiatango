﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<Vuforia.BackgroundPlaneBehaviour>
struct UnityAction_1_t2779;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>
struct InvokableCall_1_t2778  : public BaseInvokableCall_t1075
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<Vuforia.BackgroundPlaneBehaviour>::Delegate
	UnityAction_1_t2779 * ___Delegate_0;
};
