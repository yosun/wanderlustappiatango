﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.UserDefinedTargetBuildingAbstractBehaviour
struct UserDefinedTargetBuildingAbstractBehaviour_t82;
// Vuforia.IUserDefinedTargetEventHandler
struct IUserDefinedTargetEventHandler_t768;
// System.String
struct String_t;
// Vuforia.ImageTargetBuilder/FrameQuality
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder_.h"

// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::RegisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
 void UserDefinedTargetBuildingAbstractBehaviour_RegisterEventHandler_m4188 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.UserDefinedTargetBuildingAbstractBehaviour::UnregisterEventHandler(Vuforia.IUserDefinedTargetEventHandler)
 bool UserDefinedTargetBuildingAbstractBehaviour_UnregisterEventHandler_m4189 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, Object_t * ___eventHandler, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StartScanning()
 void UserDefinedTargetBuildingAbstractBehaviour_StartScanning_m4190 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::BuildNewTarget(System.String,System.Single)
 void UserDefinedTargetBuildingAbstractBehaviour_BuildNewTarget_m4191 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, String_t* ___targetName, float ___sceenSizeWidth, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::StopScanning()
 void UserDefinedTargetBuildingAbstractBehaviour_StopScanning_m4192 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::SetFrameQuality(Vuforia.ImageTargetBuilder/FrameQuality)
 void UserDefinedTargetBuildingAbstractBehaviour_SetFrameQuality_m4193 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, int32_t ___frameQuality, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Start()
 void UserDefinedTargetBuildingAbstractBehaviour_Start_m4194 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::Update()
 void UserDefinedTargetBuildingAbstractBehaviour_Update_m4195 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnEnable()
 void UserDefinedTargetBuildingAbstractBehaviour_OnEnable_m4196 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDisable()
 void UserDefinedTargetBuildingAbstractBehaviour_OnDisable_m4197 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnDestroy()
 void UserDefinedTargetBuildingAbstractBehaviour_OnDestroy_m4198 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnQCARStarted()
 void UserDefinedTargetBuildingAbstractBehaviour_OnQCARStarted_m4199 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::OnPause(System.Boolean)
 void UserDefinedTargetBuildingAbstractBehaviour_OnPause_m4200 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, bool ___pause, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.UserDefinedTargetBuildingAbstractBehaviour::.ctor()
 void UserDefinedTargetBuildingAbstractBehaviour__ctor_m611 (UserDefinedTargetBuildingAbstractBehaviour_t82 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
