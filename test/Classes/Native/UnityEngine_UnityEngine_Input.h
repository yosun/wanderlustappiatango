﻿#pragma once
#include <stdint.h>
// UnityEngine.Gyroscope
struct Gyroscope_t102;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Input
struct Input_t98  : public Object_t
{
};
struct Input_t98_StaticFields{
	// UnityEngine.Gyroscope UnityEngine.Input::m_MainGyro
	Gyroscope_t102 * ___m_MainGyro_0;
};
