﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>
struct HashSet_1_t769;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1066;
// System.Collections.Generic.IEnumerator`1<UnityEngine.MeshRenderer>
struct IEnumerator_1_t4388;
// UnityEngine.MeshRenderer[]
struct MeshRendererU5BU5D_t4386;
// UnityEngine.MeshRenderer
struct MeshRenderer_t168;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.MeshRenderer>
struct IEqualityComparer_1_t4387;
// System.Object
struct Object_t;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.HashSet`1/Enumerator<UnityEngine.MeshRenderer>
#include "System_Core_System_Collections_Generic_HashSet_1_Enumerator_.h"

// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::.ctor()
// System.Collections.Generic.HashSet`1<System.Object>
#include "System_Core_System_Collections_Generic_HashSet_1_gen_0MethodDeclarations.h"
#define HashSet_1__ctor_m5402(__this, method) (void)HashSet_1__ctor_m26882_gshared((HashSet_1_t4389 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1__ctor_m26883(__this, ___info, ___context, method) (void)HashSet_1__ctor_m26884_gshared((HashSet_1_t4389 *)__this, (SerializationInfo_t1066 *)___info, (StreamingContext_t1067 )___context, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26885(__this, method) (Object_t*)HashSet_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m26886_gshared((HashSet_1_t4389 *)__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26887(__this, method) (bool)HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m26888_gshared((HashSet_1_t4389 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::System.Collections.Generic.ICollection<T>.CopyTo(T[],System.Int32)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m26889(__this, ___array, ___index, method) (void)HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_CopyTo_m26890_gshared((HashSet_1_t4389 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::System.Collections.Generic.ICollection<T>.Add(T)
#define HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26891(__this, ___item, method) (void)HashSet_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m26892_gshared((HashSet_1_t4389 *)__this, (Object_t *)___item, method)
// System.Collections.IEnumerator System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::System.Collections.IEnumerable.GetEnumerator()
#define HashSet_1_System_Collections_IEnumerable_GetEnumerator_m26893(__this, method) (Object_t *)HashSet_1_System_Collections_IEnumerable_GetEnumerator_m26894_gshared((HashSet_1_t4389 *)__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::get_Count()
#define HashSet_1_get_Count_m26895(__this, method) (int32_t)HashSet_1_get_Count_m26896_gshared((HashSet_1_t4389 *)__this, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<T>)
#define HashSet_1_Init_m26897(__this, ___capacity, ___comparer, method) (void)HashSet_1_Init_m26898_gshared((HashSet_1_t4389 *)__this, (int32_t)___capacity, (Object_t*)___comparer, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::InitArrays(System.Int32)
#define HashSet_1_InitArrays_m26899(__this, ___size, method) (void)HashSet_1_InitArrays_m26900_gshared((HashSet_1_t4389 *)__this, (int32_t)___size, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::SlotsContainsAt(System.Int32,System.Int32,T)
#define HashSet_1_SlotsContainsAt_m26901(__this, ___index, ___hash, ___item, method) (bool)HashSet_1_SlotsContainsAt_m26902_gshared((HashSet_1_t4389 *)__this, (int32_t)___index, (int32_t)___hash, (Object_t *)___item, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::CopyTo(T[],System.Int32)
#define HashSet_1_CopyTo_m26903(__this, ___array, ___index, method) (void)HashSet_1_CopyTo_m26904_gshared((HashSet_1_t4389 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::CopyTo(T[],System.Int32,System.Int32)
#define HashSet_1_CopyTo_m26905(__this, ___array, ___index, ___count, method) (void)HashSet_1_CopyTo_m26906_gshared((HashSet_1_t4389 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___index, (int32_t)___count, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::Resize()
#define HashSet_1_Resize_m26907(__this, method) (void)HashSet_1_Resize_m26908_gshared((HashSet_1_t4389 *)__this, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::GetLinkHashCode(System.Int32)
#define HashSet_1_GetLinkHashCode_m26909(__this, ___index, method) (int32_t)HashSet_1_GetLinkHashCode_m26910_gshared((HashSet_1_t4389 *)__this, (int32_t)___index, method)
// System.Int32 System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::GetItemHashCode(T)
#define HashSet_1_GetItemHashCode_m26911(__this, ___item, method) (int32_t)HashSet_1_GetItemHashCode_m26912_gshared((HashSet_1_t4389 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::Add(T)
#define HashSet_1_Add_m5393(__this, ___item, method) (bool)HashSet_1_Add_m26913_gshared((HashSet_1_t4389 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::Clear()
#define HashSet_1_Clear_m5397(__this, method) (void)HashSet_1_Clear_m26914_gshared((HashSet_1_t4389 *)__this, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::Contains(T)
#define HashSet_1_Contains_m5392(__this, ___item, method) (bool)HashSet_1_Contains_m26915_gshared((HashSet_1_t4389 *)__this, (Object_t *)___item, method)
// System.Boolean System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::Remove(T)
#define HashSet_1_Remove_m26916(__this, ___item, method) (bool)HashSet_1_Remove_m26917_gshared((HashSet_1_t4389 *)__this, (Object_t *)___item, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define HashSet_1_GetObjectData_m26918(__this, ___info, ___context, method) (void)HashSet_1_GetObjectData_m26919_gshared((HashSet_1_t4389 *)__this, (SerializationInfo_t1066 *)___info, (StreamingContext_t1067 )___context, method)
// System.Void System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::OnDeserialization(System.Object)
#define HashSet_1_OnDeserialization_m26920(__this, ___sender, method) (void)HashSet_1_OnDeserialization_m26921_gshared((HashSet_1_t4389 *)__this, (Object_t *)___sender, method)
// System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1<UnityEngine.MeshRenderer>::GetEnumerator()
 Enumerator_t902  HashSet_1_GetEnumerator_m5394 (HashSet_1_t769 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
