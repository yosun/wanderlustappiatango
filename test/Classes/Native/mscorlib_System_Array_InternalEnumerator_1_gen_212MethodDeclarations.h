﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>
struct InternalEnumerator_1_t3427;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.UI.Image/Origin360
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin360.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m18822 (InternalEnumerator_1_t3427 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18823 (InternalEnumerator_1_t3427 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::Dispose()
 void InternalEnumerator_1_Dispose_m18824 (InternalEnumerator_1_t3427 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m18825 (InternalEnumerator_1_t3427 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.UI.Image/Origin360>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m18826 (InternalEnumerator_1_t3427 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
