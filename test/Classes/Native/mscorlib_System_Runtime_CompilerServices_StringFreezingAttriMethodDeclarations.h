﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.CompilerServices.StringFreezingAttribute
struct StringFreezingAttribute_t1960;

// System.Void System.Runtime.CompilerServices.StringFreezingAttribute::.ctor()
 void StringFreezingAttribute__ctor_m11402 (StringFreezingAttribute_t1960 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
