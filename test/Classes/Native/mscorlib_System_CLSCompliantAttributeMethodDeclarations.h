﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.CLSCompliantAttribute
struct CLSCompliantAttribute_t1285;

// System.Void System.CLSCompliantAttribute::.ctor(System.Boolean)
 void CLSCompliantAttribute__ctor_m6716 (CLSCompliantAttribute_t1285 * __this, bool ___isCompliant, MethodInfo* method) IL2CPP_METHOD_ATTR;
