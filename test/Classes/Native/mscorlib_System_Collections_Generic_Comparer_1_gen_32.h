﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.WordResult>
struct Comparer_1_t3982;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.WordResult>
struct Comparer_1_t3982  : public Object_t
{
};
struct Comparer_1_t3982_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.WordResult>::_default
	Comparer_1_t3982 * ____default_0;
};
