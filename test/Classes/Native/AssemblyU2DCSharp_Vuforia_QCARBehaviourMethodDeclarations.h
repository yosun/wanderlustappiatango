﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARBehaviour
struct QCARBehaviour_t70;

// System.Void Vuforia.QCARBehaviour::.ctor()
 void QCARBehaviour__ctor_m165 (QCARBehaviour_t70 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.QCARBehaviour::Awake()
 void QCARBehaviour_Awake_m166 (QCARBehaviour_t70 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
