﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>
struct KeyValuePair_2_t3941;
// System.Type
struct Type_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::.ctor(TKey,TValue)
 void KeyValuePair_2__ctor_m22834 (KeyValuePair_2_t3941 * __this, Type_t * ___key, uint16_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::get_Key()
 Type_t * KeyValuePair_2_get_Key_m22835 (KeyValuePair_2_t3941 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::set_Key(TKey)
 void KeyValuePair_2_set_Key_m22836 (KeyValuePair_2_t3941 * __this, Type_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::get_Value()
 uint16_t KeyValuePair_2_get_Value_m22837 (KeyValuePair_2_t3941 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::set_Value(TValue)
 void KeyValuePair_2_set_Value_m22838 (KeyValuePair_2_t3941 * __this, uint16_t ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>::ToString()
 String_t* KeyValuePair_2_ToString_m22839 (KeyValuePair_2_t3941 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
