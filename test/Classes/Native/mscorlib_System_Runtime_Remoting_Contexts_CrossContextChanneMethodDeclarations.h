﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Contexts.CrossContextChannel
struct CrossContextChannel_t1992;

// System.Void System.Runtime.Remoting.Contexts.CrossContextChannel::.ctor()
 void CrossContextChannel__ctor_m11479 (CrossContextChannel_t1992 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
