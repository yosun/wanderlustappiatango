﻿#pragma once
#include <stdint.h>
// UnityEngine.Transform
struct Transform_t10;
// System.Object
#include "mscorlib_System_Object.h"
// UnityEngine.Transform/Enumerator
struct Enumerator_t1001  : public Object_t
{
	// UnityEngine.Transform UnityEngine.Transform/Enumerator::outer
	Transform_t10 * ___outer_0;
	// System.Int32 UnityEngine.Transform/Enumerator::currentIndex
	int32_t ___currentIndex_1;
};
