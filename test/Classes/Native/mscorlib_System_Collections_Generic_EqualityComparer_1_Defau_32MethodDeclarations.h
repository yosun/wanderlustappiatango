﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image>
struct DefaultComparer_t3787;
// Vuforia.Image
struct Image_t560;

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image>::.ctor()
// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Object>
#include "mscorlib_System_Collections_Generic_EqualityComparer_1_Defau_0MethodDeclarations.h"
#define DefaultComparer__ctor_m21209(__this, method) (void)DefaultComparer__ctor_m14747_gshared((DefaultComparer_t2860 *)__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image>::GetHashCode(T)
#define DefaultComparer_GetHashCode_m21210(__this, ___obj, method) (int32_t)DefaultComparer_GetHashCode_m14748_gshared((DefaultComparer_t2860 *)__this, (Object_t *)___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.Image>::Equals(T,T)
#define DefaultComparer_Equals_m21211(__this, ___x, ___y, method) (bool)DefaultComparer_Equals_m14749_gshared((DefaultComparer_t2860 *)__this, (Object_t *)___x, (Object_t *)___y, method)
