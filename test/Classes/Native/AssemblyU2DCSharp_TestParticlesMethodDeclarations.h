﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// TestParticles
struct TestParticles_t28;

// System.Void TestParticles::.ctor()
 void TestParticles__ctor_m78 (TestParticles_t28 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::Start()
 void TestParticles_Start_m79 (TestParticles_t28 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::Update()
 void TestParticles_Update_m80 (TestParticles_t28 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::OnGUI()
 void TestParticles_OnGUI_m81 (TestParticles_t28 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::ShowParticle()
 void TestParticles_ShowParticle_m82 (TestParticles_t28 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::ParticleInformationWindow(System.Int32)
 void TestParticles_ParticleInformationWindow_m83 (TestParticles_t28 * __this, int32_t ___id, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestParticles::InfoWindow(System.Int32)
 void TestParticles_InfoWindow_m84 (TestParticles_t28 * __this, int32_t ___id, MethodInfo* method) IL2CPP_METHOD_ATTR;
