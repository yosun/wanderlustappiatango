﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Events.PersistentListenerMode>
struct InternalEnumerator_1_t4785;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Events.PersistentListenerMode
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Events.PersistentListenerMode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m29316 (InternalEnumerator_1_t4785 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Events.PersistentListenerMode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m29317 (InternalEnumerator_1_t4785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Events.PersistentListenerMode>::Dispose()
 void InternalEnumerator_1_Dispose_m29318 (InternalEnumerator_1_t4785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Events.PersistentListenerMode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m29319 (InternalEnumerator_1_t4785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<UnityEngine.Events.PersistentListenerMode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m29320 (InternalEnumerator_1_t4785 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
