﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.FieldOffsetAttribute
struct FieldOffsetAttribute_t1715;

// System.Void System.Runtime.InteropServices.FieldOffsetAttribute::.ctor(System.Int32)
 void FieldOffsetAttribute__ctor_m9742 (FieldOffsetAttribute_t1715 * __this, int32_t ___offset, MethodInfo* method) IL2CPP_METHOD_ATTR;
