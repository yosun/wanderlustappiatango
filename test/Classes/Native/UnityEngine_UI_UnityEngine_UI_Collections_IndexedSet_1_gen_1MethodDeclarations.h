﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t3223;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t266;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t455;
// System.Object[]
struct ObjectU5BU5D_t115;
// System.Predicate`1<System.Object>
struct Predicate_1_t2845;
// System.Comparison`1<System.Object>
struct Comparison_1_t2846;

// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
 void IndexedSet_1__ctor_m17125_gshared (IndexedSet_1_t3223 * __this, MethodInfo* method);
#define IndexedSet_1__ctor_m17125(__this, method) (void)IndexedSet_1__ctor_m17125_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
 Object_t * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17127_gshared (IndexedSet_1_t3223 * __this, MethodInfo* method);
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17127(__this, method) (Object_t *)IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m17127_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
 void IndexedSet_1_Add_m17128_gshared (IndexedSet_1_t3223 * __this, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_Add_m17128(__this, ___item, method) (void)IndexedSet_1_Add_m17128_gshared((IndexedSet_1_t3223 *)__this, (Object_t *)___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
 bool IndexedSet_1_Remove_m17129_gshared (IndexedSet_1_t3223 * __this, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_Remove_m17129(__this, ___item, method) (bool)IndexedSet_1_Remove_m17129_gshared((IndexedSet_1_t3223 *)__this, (Object_t *)___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
 Object_t* IndexedSet_1_GetEnumerator_m17131_gshared (IndexedSet_1_t3223 * __this, MethodInfo* method);
#define IndexedSet_1_GetEnumerator_m17131(__this, method) (Object_t*)IndexedSet_1_GetEnumerator_m17131_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
 void IndexedSet_1_Clear_m17132_gshared (IndexedSet_1_t3223 * __this, MethodInfo* method);
#define IndexedSet_1_Clear_m17132(__this, method) (void)IndexedSet_1_Clear_m17132_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
 bool IndexedSet_1_Contains_m17134_gshared (IndexedSet_1_t3223 * __this, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_Contains_m17134(__this, ___item, method) (bool)IndexedSet_1_Contains_m17134_gshared((IndexedSet_1_t3223 *)__this, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
 void IndexedSet_1_CopyTo_m17136_gshared (IndexedSet_1_t3223 * __this, ObjectU5BU5D_t115* ___array, int32_t ___arrayIndex, MethodInfo* method);
#define IndexedSet_1_CopyTo_m17136(__this, ___array, ___arrayIndex, method) (void)IndexedSet_1_CopyTo_m17136_gshared((IndexedSet_1_t3223 *)__this, (ObjectU5BU5D_t115*)___array, (int32_t)___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
 int32_t IndexedSet_1_get_Count_m17137_gshared (IndexedSet_1_t3223 * __this, MethodInfo* method);
#define IndexedSet_1_get_Count_m17137(__this, method) (int32_t)IndexedSet_1_get_Count_m17137_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
 bool IndexedSet_1_get_IsReadOnly_m17139_gshared (IndexedSet_1_t3223 * __this, MethodInfo* method);
#define IndexedSet_1_get_IsReadOnly_m17139(__this, method) (bool)IndexedSet_1_get_IsReadOnly_m17139_gshared((IndexedSet_1_t3223 *)__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
 int32_t IndexedSet_1_IndexOf_m17141_gshared (IndexedSet_1_t3223 * __this, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_IndexOf_m17141(__this, ___item, method) (int32_t)IndexedSet_1_IndexOf_m17141_gshared((IndexedSet_1_t3223 *)__this, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
 void IndexedSet_1_Insert_m17143_gshared (IndexedSet_1_t3223 * __this, int32_t ___index, Object_t * ___item, MethodInfo* method);
#define IndexedSet_1_Insert_m17143(__this, ___index, ___item, method) (void)IndexedSet_1_Insert_m17143_gshared((IndexedSet_1_t3223 *)__this, (int32_t)___index, (Object_t *)___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
 void IndexedSet_1_RemoveAt_m17145_gshared (IndexedSet_1_t3223 * __this, int32_t ___index, MethodInfo* method);
#define IndexedSet_1_RemoveAt_m17145(__this, ___index, method) (void)IndexedSet_1_RemoveAt_m17145_gshared((IndexedSet_1_t3223 *)__this, (int32_t)___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
 Object_t * IndexedSet_1_get_Item_m17146_gshared (IndexedSet_1_t3223 * __this, int32_t ___index, MethodInfo* method);
#define IndexedSet_1_get_Item_m17146(__this, ___index, method) (Object_t *)IndexedSet_1_get_Item_m17146_gshared((IndexedSet_1_t3223 *)__this, (int32_t)___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
 void IndexedSet_1_set_Item_m17148_gshared (IndexedSet_1_t3223 * __this, int32_t ___index, Object_t * ___value, MethodInfo* method);
#define IndexedSet_1_set_Item_m17148(__this, ___index, ___value, method) (void)IndexedSet_1_set_Item_m17148_gshared((IndexedSet_1_t3223 *)__this, (int32_t)___index, (Object_t *)___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
 void IndexedSet_1_RemoveAll_m17149_gshared (IndexedSet_1_t3223 * __this, Predicate_1_t2845 * ___match, MethodInfo* method);
#define IndexedSet_1_RemoveAll_m17149(__this, ___match, method) (void)IndexedSet_1_RemoveAll_m17149_gshared((IndexedSet_1_t3223 *)__this, (Predicate_1_t2845 *)___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
 void IndexedSet_1_Sort_m17150_gshared (IndexedSet_1_t3223 * __this, Comparison_1_t2846 * ___sortLayoutFunction, MethodInfo* method);
#define IndexedSet_1_Sort_m17150(__this, ___sortLayoutFunction, method) (void)IndexedSet_1_Sort_m17150_gshared((IndexedSet_1_t3223 *)__this, (Comparison_1_t2846 *)___sortLayoutFunction, method)
