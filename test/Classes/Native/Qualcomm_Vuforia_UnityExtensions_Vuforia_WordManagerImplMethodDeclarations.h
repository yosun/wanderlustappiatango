﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.WordManagerImpl
struct WordManagerImpl_t699;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>
struct IEnumerable_1_t689;
// System.Collections.Generic.IEnumerable`1<Vuforia.Word>
struct IEnumerable_1_t690;
// Vuforia.Word
struct Word_t691;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t60;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour>
struct IEnumerable_1_t692;
// UnityEngine.Transform
struct Transform_t10;
// Vuforia.QCARManagerImpl/WordData[]
struct WordDataU5BU5D_t654;
// Vuforia.QCARManagerImpl/WordResultData[]
struct WordResultDataU5BU5D_t655;
// System.Collections.Generic.IEnumerable`1<Vuforia.QCARManagerImpl/WordData>
struct IEnumerable_1_t700;
// System.Collections.Generic.IEnumerable`1<Vuforia.QCARManagerImpl/WordResultData>
struct IEnumerable_1_t701;
// Vuforia.WordResult
struct WordResult_t702;
// Vuforia.WordPrefabCreationMode
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_WordPrefabCreationM.h"

// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult> Vuforia.WordManagerImpl::GetActiveWordResults()
 Object_t* WordManagerImpl_GetActiveWordResults_m3097 (WordManagerImpl_t699 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult> Vuforia.WordManagerImpl::GetNewWords()
 Object_t* WordManagerImpl_GetNewWords_m3098 (WordManagerImpl_t699 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.Word> Vuforia.WordManagerImpl::GetLostWords()
 Object_t* WordManagerImpl_GetLostWords_m3099 (WordManagerImpl_t699 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.WordManagerImpl::TryGetWordBehaviour(Vuforia.Word,Vuforia.WordAbstractBehaviour&)
 bool WordManagerImpl_TryGetWordBehaviour_m3100 (WordManagerImpl_t699 * __this, Object_t * ___word, WordAbstractBehaviour_t60 ** ___behaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour> Vuforia.WordManagerImpl::GetTrackableBehaviours()
 Object_t* WordManagerImpl_GetTrackableBehaviours_m3101 (WordManagerImpl_t699 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::DestroyWordBehaviour(Vuforia.WordAbstractBehaviour,System.Boolean)
 void WordManagerImpl_DestroyWordBehaviour_m3102 (WordManagerImpl_t699 * __this, WordAbstractBehaviour_t60 * ___behaviour, bool ___destroyGameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::InitializeWordBehaviourTemplates(Vuforia.WordPrefabCreationMode,System.Int32)
 void WordManagerImpl_InitializeWordBehaviourTemplates_m3103 (WordManagerImpl_t699 * __this, int32_t ___wordPrefabCreationMode, int32_t ___maxInstances, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::InitializeWordBehaviourTemplates()
 void WordManagerImpl_InitializeWordBehaviourTemplates_m3104 (WordManagerImpl_t699 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::RemoveDestroyedTrackables()
 void WordManagerImpl_RemoveDestroyedTrackables_m3105 (WordManagerImpl_t699 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::UpdateWords(UnityEngine.Transform,Vuforia.QCARManagerImpl/WordData[],Vuforia.QCARManagerImpl/WordResultData[])
 void WordManagerImpl_UpdateWords_m3106 (WordManagerImpl_t699 * __this, Transform_t10 * ___arCameraTransform, WordDataU5BU5D_t654* ___newWordData, WordResultDataU5BU5D_t655* ___wordResults, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::SetWordBehavioursToNotFound()
 void WordManagerImpl_SetWordBehavioursToNotFound_m3107 (WordManagerImpl_t699 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::UpdateWords(System.Collections.Generic.IEnumerable`1<Vuforia.QCARManagerImpl/WordData>,System.Collections.Generic.IEnumerable`1<Vuforia.QCARManagerImpl/WordResultData>)
 void WordManagerImpl_UpdateWords_m3108 (WordManagerImpl_t699 * __this, Object_t* ___newWordData, Object_t* ___wordResults, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::UpdateWordResultPoses(UnityEngine.Transform,System.Collections.Generic.IEnumerable`1<Vuforia.QCARManagerImpl/WordResultData>)
 void WordManagerImpl_UpdateWordResultPoses_m3109 (WordManagerImpl_t699 * __this, Transform_t10 * ___arCameraTransform, Object_t* ___wordResults, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::AssociateWordResultsWithBehaviours()
 void WordManagerImpl_AssociateWordResultsWithBehaviours_m3110 (WordManagerImpl_t699 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::UnregisterLostWords()
 void WordManagerImpl_UnregisterLostWords_m3111 (WordManagerImpl_t699 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::UpdateWordBehaviourPoses()
 void WordManagerImpl_UpdateWordBehaviourPoses_m3112 (WordManagerImpl_t699 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.WordManagerImpl::AssociateWordBehaviour(Vuforia.WordResult)
 WordAbstractBehaviour_t60 * WordManagerImpl_AssociateWordBehaviour_m3113 (WordManagerImpl_t699 * __this, WordResult_t702 * ___wordResult, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.WordManagerImpl::AssociateWordBehaviour(Vuforia.WordResult,Vuforia.WordAbstractBehaviour)
 WordAbstractBehaviour_t60 * WordManagerImpl_AssociateWordBehaviour_m3114 (WordManagerImpl_t699 * __this, WordResult_t702 * ___wordResult, WordAbstractBehaviour_t60 * ___wordBehaviourTemplate, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.WordManagerImpl::InstantiateWordBehaviour(Vuforia.WordAbstractBehaviour)
 WordAbstractBehaviour_t60 * WordManagerImpl_InstantiateWordBehaviour_m3115 (Object_t * __this/* static, unused */, WordAbstractBehaviour_t60 * ___input, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordAbstractBehaviour Vuforia.WordManagerImpl::CreateWordBehaviour()
 WordAbstractBehaviour_t60 * WordManagerImpl_CreateWordBehaviour_m3116 (Object_t * __this/* static, unused */, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.WordManagerImpl::.ctor()
 void WordManagerImpl__ctor_m3117 (WordManagerImpl_t699 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
