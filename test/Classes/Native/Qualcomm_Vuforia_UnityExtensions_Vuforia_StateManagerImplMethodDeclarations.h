﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.StateManagerImpl
struct StateManagerImpl_t658;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>
struct IEnumerable_1_t725;
// Vuforia.WordManager
struct WordManager_t688;
// Vuforia.Trackable
struct Trackable_t550;
// Vuforia.DataSet
struct DataSet_t568;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t44;
// Vuforia.ImageTargetAbstractBehaviour
struct ImageTargetAbstractBehaviour_t50;
// Vuforia.ImageTarget
struct ImageTarget_t572;
// UnityEngine.GameObject
struct GameObject_t2;
// Vuforia.MarkerAbstractBehaviour
struct MarkerAbstractBehaviour_t58;
// Vuforia.Marker
struct Marker_t623;
// System.String
struct String_t;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t656;
// UnityEngine.Transform
struct Transform_t10;
// Vuforia.QCARManagerImpl/TrackableResultData[]
struct TrackableResultDataU5BU5D_t653;
// Vuforia.QCARManagerImpl/WordData[]
struct WordDataU5BU5D_t654;
// Vuforia.QCARManagerImpl/WordResultData[]
struct WordResultDataU5BU5D_t655;
// Vuforia.VirtualButtonAbstractBehaviour[]
struct VirtualButtonAbstractBehaviourU5BU5D_t729;
// Vuforia.MultiTargetAbstractBehaviour
struct MultiTargetAbstractBehaviour_t59;
// Vuforia.MultiTarget
struct MultiTarget_t573;
// Vuforia.CylinderTargetAbstractBehaviour
struct CylinderTargetAbstractBehaviour_t34;
// Vuforia.CylinderTarget
struct CylinderTarget_t571;
// Vuforia.ObjectTargetAbstractBehaviour
struct ObjectTargetAbstractBehaviour_t62;
// Vuforia.ObjectTarget
struct ObjectTarget_t554;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARManagerImpl/PoseData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Pos.h"

// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManagerImpl::GetActiveTrackableBehaviours()
 Object_t* StateManagerImpl_GetActiveTrackableBehaviours_m3983 (StateManagerImpl_t658 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManagerImpl::GetTrackableBehaviours()
 Object_t* StateManagerImpl_GetTrackableBehaviours_m3984 (StateManagerImpl_t658 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.WordManager Vuforia.StateManagerImpl::GetWordManager()
 WordManager_t688 * StateManagerImpl_GetWordManager_m3985 (StateManagerImpl_t658 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::DestroyTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean)
 void StateManagerImpl_DestroyTrackableBehavioursForTrackable_m3986 (StateManagerImpl_t658 * __this, Object_t * ___trackable, bool ___destroyGameObjects, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::AssociateMarkerBehaviours()
 void StateManagerImpl_AssociateMarkerBehaviours_m3987 (StateManagerImpl_t658 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::AssociateTrackableBehavioursForDataSet(Vuforia.DataSet)
 void StateManagerImpl_AssociateTrackableBehavioursForDataSet_m3988 (StateManagerImpl_t658 * __this, DataSet_t568 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::RegisterExternallyManagedTrackableBehaviour(Vuforia.TrackableBehaviour)
 void StateManagerImpl_RegisterExternallyManagedTrackableBehaviour_m3989 (StateManagerImpl_t658 * __this, TrackableBehaviour_t44 * ___trackableBehaviour, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UnregisterExternallyManagedTrackableBehaviour(System.Int32)
 void StateManagerImpl_UnregisterExternallyManagedTrackableBehaviour_m3990 (StateManagerImpl_t658 * __this, int32_t ___id, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::RemoveDestroyedTrackables()
 void StateManagerImpl_RemoveDestroyedTrackables_m3991 (StateManagerImpl_t658 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::ClearTrackableBehaviours()
 void StateManagerImpl_ClearTrackableBehaviours_m3992 (StateManagerImpl_t658 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.StateManagerImpl::FindOrCreateImageTargetBehaviourForTrackable(Vuforia.ImageTarget,UnityEngine.GameObject)
 ImageTargetAbstractBehaviour_t50 * StateManagerImpl_FindOrCreateImageTargetBehaviourForTrackable_m3993 (StateManagerImpl_t658 * __this, Object_t * ___trackable, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.StateManagerImpl::FindOrCreateImageTargetBehaviourForTrackable(Vuforia.ImageTarget,UnityEngine.GameObject,Vuforia.DataSet)
 ImageTargetAbstractBehaviour_t50 * StateManagerImpl_FindOrCreateImageTargetBehaviourForTrackable_m3994 (StateManagerImpl_t658 * __this, Object_t * ___trackable, GameObject_t2 * ___gameObject, DataSet_t568 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.StateManagerImpl::CreateNewMarkerBehaviourForMarker(Vuforia.Marker,System.String)
 MarkerAbstractBehaviour_t58 * StateManagerImpl_CreateNewMarkerBehaviourForMarker_m3995 (StateManagerImpl_t658 * __this, Object_t * ___trackable, String_t* ___gameObjectName, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MarkerAbstractBehaviour Vuforia.StateManagerImpl::CreateNewMarkerBehaviourForMarker(Vuforia.Marker,UnityEngine.GameObject)
 MarkerAbstractBehaviour_t58 * StateManagerImpl_CreateNewMarkerBehaviourForMarker_m3996 (StateManagerImpl_t658 * __this, Object_t * ___trackable, GameObject_t2 * ___gameObject, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::SetTrackableBehavioursForTrackableToNotFound(Vuforia.Trackable)
 void StateManagerImpl_SetTrackableBehavioursForTrackableToNotFound_m3997 (StateManagerImpl_t658 * __this, Object_t * ___trackable, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::EnableTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean)
 void StateManagerImpl_EnableTrackableBehavioursForTrackable_m3998 (StateManagerImpl_t658 * __this, Object_t * ___trackable, bool ___enabled, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::RemoveDisabledTrackablesFromQueue(System.Collections.Generic.LinkedList`1<System.Int32>&)
 void StateManagerImpl_RemoveDisabledTrackablesFromQueue_m3999 (StateManagerImpl_t658 * __this, LinkedList_1_t656 ** ___trackableIDs, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UpdateCameraPose(UnityEngine.Transform,Vuforia.QCARManagerImpl/TrackableResultData[],System.Int32)
 void StateManagerImpl_UpdateCameraPose_m4000 (StateManagerImpl_t658 * __this, Transform_t10 * ___arCameraTransform, TrackableResultDataU5BU5D_t653* ___trackableResultDataArray, int32_t ___originTrackableID, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UpdateTrackablePoses(UnityEngine.Transform,Vuforia.QCARManagerImpl/TrackableResultData[],System.Int32,System.Int32)
 void StateManagerImpl_UpdateTrackablePoses_m4001 (StateManagerImpl_t658 * __this, Transform_t10 * ___arCameraTransform, TrackableResultDataU5BU5D_t653* ___trackableResultDataArray, int32_t ___originTrackableID, int32_t ___frameIndex, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UpdateVirtualButtons(System.Int32,System.IntPtr)
 void StateManagerImpl_UpdateVirtualButtons_m4002 (StateManagerImpl_t658 * __this, int32_t ___numVirtualButtons, IntPtr_t121 ___virtualButtonPtr, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::UpdateWords(UnityEngine.Transform,Vuforia.QCARManagerImpl/WordData[],Vuforia.QCARManagerImpl/WordResultData[])
 void StateManagerImpl_UpdateWords_m4003 (StateManagerImpl_t658 * __this, Transform_t10 * ___arCameraTransform, WordDataU5BU5D_t654* ___wordData, WordResultDataU5BU5D_t655* ___wordResultData, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::AssociateVirtualButtonBehaviours(Vuforia.VirtualButtonAbstractBehaviour[],Vuforia.DataSet)
 void StateManagerImpl_AssociateVirtualButtonBehaviours_m4004 (StateManagerImpl_t658 * __this, VirtualButtonAbstractBehaviourU5BU5D_t729* ___vbBehaviours, DataSet_t568 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::CreateMissingDataSetTrackableBehaviours(Vuforia.DataSet)
 void StateManagerImpl_CreateMissingDataSetTrackableBehaviours_m4005 (StateManagerImpl_t658 * __this, DataSet_t568 * ___dataSet, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ImageTargetAbstractBehaviour Vuforia.StateManagerImpl::CreateImageTargetBehaviour(Vuforia.ImageTarget)
 ImageTargetAbstractBehaviour_t50 * StateManagerImpl_CreateImageTargetBehaviour_m4006 (StateManagerImpl_t658 * __this, Object_t * ___imageTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.MultiTargetAbstractBehaviour Vuforia.StateManagerImpl::CreateMultiTargetBehaviour(Vuforia.MultiTarget)
 MultiTargetAbstractBehaviour_t59 * StateManagerImpl_CreateMultiTargetBehaviour_m4007 (StateManagerImpl_t658 * __this, Object_t * ___multiTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.CylinderTargetAbstractBehaviour Vuforia.StateManagerImpl::CreateCylinderTargetBehaviour(Vuforia.CylinderTarget)
 CylinderTargetAbstractBehaviour_t34 * StateManagerImpl_CreateCylinderTargetBehaviour_m4008 (StateManagerImpl_t658 * __this, Object_t * ___cylinderTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.ObjectTargetAbstractBehaviour Vuforia.StateManagerImpl::CreateObjectTargetBehaviour(Vuforia.ObjectTarget)
 ObjectTargetAbstractBehaviour_t62 * StateManagerImpl_CreateObjectTargetBehaviour_m4009 (StateManagerImpl_t658 * __this, Object_t * ___objectTarget, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::InitializeMarkerBehaviour(Vuforia.MarkerAbstractBehaviour,Vuforia.Marker)
 void StateManagerImpl_InitializeMarkerBehaviour_m4010 (StateManagerImpl_t658 * __this, MarkerAbstractBehaviour_t58 * ___markerBehaviour, Object_t * ___marker, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::PositionCamera(Vuforia.TrackableBehaviour,UnityEngine.Transform,Vuforia.QCARManagerImpl/PoseData)
 void StateManagerImpl_PositionCamera_m4011 (StateManagerImpl_t658 * __this, TrackableBehaviour_t44 * ___trackableBehaviour, Transform_t10 * ___arCameraTransform, PoseData_t638  ___camToTargetPose, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::PositionTrackable(Vuforia.TrackableBehaviour,UnityEngine.Transform,Vuforia.QCARManagerImpl/PoseData)
 void StateManagerImpl_PositionTrackable_m4012 (StateManagerImpl_t658 * __this, TrackableBehaviour_t44 * ___trackableBehaviour, Transform_t10 * ___arCameraTransform, PoseData_t638  ___camToTargetPose, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.StateManagerImpl::.ctor()
 void StateManagerImpl__ctor_m4013 (StateManagerImpl_t658 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
