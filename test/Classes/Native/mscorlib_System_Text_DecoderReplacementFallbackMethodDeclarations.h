﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Text.DecoderReplacementFallback
struct DecoderReplacementFallback_t2147;
// System.String
struct String_t;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t2142;
// System.Object
struct Object_t;

// System.Void System.Text.DecoderReplacementFallback::.ctor()
 void DecoderReplacementFallback__ctor_m12246 (DecoderReplacementFallback_t2147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.DecoderReplacementFallback::.ctor(System.String)
 void DecoderReplacementFallback__ctor_m12247 (DecoderReplacementFallback_t2147 * __this, String_t* ___replacement, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Text.DecoderReplacementFallback::get_DefaultString()
 String_t* DecoderReplacementFallback_get_DefaultString_m12248 (DecoderReplacementFallback_t2147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallbackBuffer System.Text.DecoderReplacementFallback::CreateFallbackBuffer()
 DecoderFallbackBuffer_t2142 * DecoderReplacementFallback_CreateFallbackBuffer_m12249 (DecoderReplacementFallback_t2147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Text.DecoderReplacementFallback::Equals(System.Object)
 bool DecoderReplacementFallback_Equals_m12250 (DecoderReplacementFallback_t2147 * __this, Object_t * ___value, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.DecoderReplacementFallback::GetHashCode()
 int32_t DecoderReplacementFallback_GetHashCode_m12251 (DecoderReplacementFallback_t2147 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
