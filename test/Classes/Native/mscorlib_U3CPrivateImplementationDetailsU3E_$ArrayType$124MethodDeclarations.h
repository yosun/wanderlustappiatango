﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$124
struct $ArrayType$124_t2273;
struct $ArrayType$124_t2273_marshaled;

void $ArrayType$124_t2273_marshal(const $ArrayType$124_t2273& unmarshaled, $ArrayType$124_t2273_marshaled& marshaled);
void $ArrayType$124_t2273_marshal_back(const $ArrayType$124_t2273_marshaled& marshaled, $ArrayType$124_t2273& unmarshaled);
void $ArrayType$124_t2273_marshal_cleanup($ArrayType$124_t2273_marshaled& marshaled);
