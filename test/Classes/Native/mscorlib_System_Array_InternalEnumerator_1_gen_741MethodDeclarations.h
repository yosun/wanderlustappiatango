﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.TypeCode>
struct InternalEnumerator_1_t5155;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.TypeCode
#include "mscorlib_System_TypeCode.h"

// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m31425 (InternalEnumerator_1_t5155 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<System.TypeCode>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m31426 (InternalEnumerator_1_t5155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<System.TypeCode>::Dispose()
 void InternalEnumerator_1_Dispose_m31427 (InternalEnumerator_1_t5155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<System.TypeCode>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m31428 (InternalEnumerator_1_t5155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<System.TypeCode>::get_Current()
 int32_t InternalEnumerator_1_get_Current_m31429 (InternalEnumerator_1_t5155 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
