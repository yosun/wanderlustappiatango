﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<Vuforia.WordAbstractBehaviour>
struct Comparer_1_t4014;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<Vuforia.WordAbstractBehaviour>
struct Comparer_1_t4014  : public Object_t
{
};
struct Comparer_1_t4014_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Vuforia.WordAbstractBehaviour>::_default
	Comparer_1_t4014 * ____default_0;
};
