﻿#pragma once
#include <stdint.h>
// System.Security.Policy.StrongName
struct StrongName_t2133;
// System.IAsyncResult
struct IAsyncResult_t199;
// System.AsyncCallback
struct AsyncCallback_t200;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<System.Security.Policy.StrongName>
struct Comparison_1_t5115  : public MulticastDelegate_t325
{
};
