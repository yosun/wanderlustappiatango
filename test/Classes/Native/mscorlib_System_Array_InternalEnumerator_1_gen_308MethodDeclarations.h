﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>
struct InternalEnumerator_1_t3909;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// Vuforia.QCARManagerImpl/SurfaceData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Sur.h"

// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::.ctor(System.Array)
 void InternalEnumerator_1__ctor_m22496 (InternalEnumerator_1_t3909 * __this, Array_t * ___array, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::System.Collections.IEnumerator.get_Current()
 Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m22497 (InternalEnumerator_1_t3909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::Dispose()
 void InternalEnumerator_1_Dispose_m22498 (InternalEnumerator_1_t3909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::MoveNext()
 bool InternalEnumerator_1_MoveNext_m22499 (InternalEnumerator_1_t3909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Array/InternalEnumerator`1<Vuforia.QCARManagerImpl/SurfaceData>::get_Current()
 SurfaceData_t648  InternalEnumerator_1_get_Current_m22500 (InternalEnumerator_1_t3909 * __this, MethodInfo* method) IL2CPP_METHOD_ATTR;
