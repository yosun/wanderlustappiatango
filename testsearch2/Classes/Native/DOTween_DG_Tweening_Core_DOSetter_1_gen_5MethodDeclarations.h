﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>
struct DOSetter_1_t1042;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Rect
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23797_gshared (DOSetter_1_t1042 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m23797(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1042 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m23797_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23798_gshared (DOSetter_1_t1042 * __this, Rect_t132  ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m23798(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1042 *, Rect_t132 , const MethodInfo*))DOSetter_1_Invoke_m23798_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m23799_gshared (DOSetter_1_t1042 * __this, Rect_t132  ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m23799(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1042 *, Rect_t132 , AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m23799_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Rect>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23800_gshared (DOSetter_1_t1042 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m23800(__this, ___result, method) (( void (*) (DOSetter_1_t1042 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m23800_gshared)(__this, ___result, method)
