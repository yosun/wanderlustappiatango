﻿#pragma once
#include <stdint.h>
// System.String
struct String_t;
// System.Attribute
#include "mscorlib_System_Attribute.h"
// System.Reflection.AssemblyDefaultAliasAttribute
struct  AssemblyDefaultAliasAttribute_t1590  : public Attribute_t146
{
	// System.String System.Reflection.AssemblyDefaultAliasAttribute::name
	String_t* ___name_0;
};
