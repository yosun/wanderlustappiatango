﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Enumerator_t3516;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t698;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
struct Dictionary_2_t699;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_22.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"
#define Enumerator__ctor_m20938(__this, ___dictionary, method) (( void (*) (Enumerator_t3516 *, Dictionary_2_t699 *, const MethodInfo*))Enumerator__ctor_m15040_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20939(__this, method) (( Object_t * (*) (Enumerator_t3516 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15041_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20940(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3516 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15042_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20941(__this, method) (( Object_t * (*) (Enumerator_t3516 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15043_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20942(__this, method) (( Object_t * (*) (Enumerator_t3516 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15044_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::MoveNext()
#define Enumerator_MoveNext_m20943(__this, method) (( bool (*) (Enumerator_t3516 *, const MethodInfo*))Enumerator_MoveNext_m15045_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_Current()
#define Enumerator_get_Current_m20944(__this, method) (( KeyValuePair_2_t3515  (*) (Enumerator_t3516 *, const MethodInfo*))Enumerator_get_Current_m15046_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m20945(__this, method) (( String_t* (*) (Enumerator_t3516 *, const MethodInfo*))Enumerator_get_CurrentKey_m15047_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m20946(__this, method) (( List_1_t698 * (*) (Enumerator_t3516 *, const MethodInfo*))Enumerator_get_CurrentValue_m15048_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::VerifyState()
#define Enumerator_VerifyState_m20947(__this, method) (( void (*) (Enumerator_t3516 *, const MethodInfo*))Enumerator_VerifyState_m15049_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m20948(__this, method) (( void (*) (Enumerator_t3516 *, const MethodInfo*))Enumerator_VerifyCurrent_m15050_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>::Dispose()
#define Enumerator_Dispose_m20949(__this, method) (( void (*) (Enumerator_t3516 *, const MethodInfo*))Enumerator_Dispose_m15051_gshared)(__this, method)
