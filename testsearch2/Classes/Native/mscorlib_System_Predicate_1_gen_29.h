﻿#pragma once
#include <stdint.h>
// Vuforia.Trackable
struct Trackable_t571;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<Vuforia.Trackable>
struct  Predicate_1_t3422  : public MulticastDelegate_t314
{
};
