﻿#pragma once
// System.Array
#include "mscorlib_System_Array.h"
// System.Object[]
// System.Object[]
struct  ObjectU5BU5D_t124  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct  KeyValuePair_2U5BU5D_t3872  : public Array_t
{
};
// System.ValueType[]
// System.ValueType[]
struct  ValueTypeU5BU5D_t4453  : public Array_t
{
};
// System.String[]
// System.String[]
struct  StringU5BU5D_t15  : public Array_t
{
};
struct StringU5BU5D_t15_StaticFields{
};
// System.IConvertible[]
// System.IConvertible[]
struct  IConvertibleU5BU5D_t4454  : public Array_t
{
};
// System.IComparable[]
// System.IComparable[]
struct  IComparableU5BU5D_t4455  : public Array_t
{
};
// System.Collections.IEnumerable[]
// System.Collections.IEnumerable[]
struct  IEnumerableU5BU5D_t4456  : public Array_t
{
};
// System.ICloneable[]
// System.ICloneable[]
struct  ICloneableU5BU5D_t4457  : public Array_t
{
};
// System.IComparable`1<System.String>[]
// System.IComparable`1<System.String>[]
struct  IComparable_1U5BU5D_t4458  : public Array_t
{
};
// System.IEquatable`1<System.String>[]
// System.IEquatable`1<System.String>[]
struct  IEquatable_1U5BU5D_t4459  : public Array_t
{
};
// System.Int32[]
// System.Int32[]
struct  Int32U5BU5D_t27  : public Array_t
{
};
// System.IFormattable[]
// System.IFormattable[]
struct  IFormattableU5BU5D_t4460  : public Array_t
{
};
// System.IComparable`1<System.Int32>[]
// System.IComparable`1<System.Int32>[]
struct  IComparable_1U5BU5D_t4461  : public Array_t
{
};
// System.IEquatable`1<System.Int32>[]
// System.IEquatable`1<System.Int32>[]
struct  IEquatable_1U5BU5D_t4462  : public Array_t
{
};
// System.Collections.Generic.Link[]
// System.Collections.Generic.Link[]
struct  LinkU5BU5D_t3108  : public Array_t
{
};
// System.Collections.DictionaryEntry[]
// System.Collections.DictionaryEntry[]
struct  DictionaryEntryU5BU5D_t4463  : public Array_t
{
};
// System.Type[]
// System.Type[]
struct  TypeU5BU5D_t884  : public Array_t
{
};
struct TypeU5BU5D_t884_StaticFields{
};
// System.Reflection.IReflect[]
// System.Reflection.IReflect[]
struct  IReflectU5BU5D_t4464  : public Array_t
{
};
// System.Runtime.InteropServices._Type[]
// System.Runtime.InteropServices._Type[]
struct  _TypeU5BU5D_t4465  : public Array_t
{
};
// System.Reflection.MemberInfo[]
// System.Reflection.MemberInfo[]
struct  MemberInfoU5BU5D_t2371  : public Array_t
{
};
// System.Reflection.ICustomAttributeProvider[]
// System.Reflection.ICustomAttributeProvider[]
struct  ICustomAttributeProviderU5BU5D_t4466  : public Array_t
{
};
// System.Runtime.InteropServices._MemberInfo[]
// System.Runtime.InteropServices._MemberInfo[]
struct  _MemberInfoU5BU5D_t4467  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject[]>[]
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GameObject[]>[]
struct  KeyValuePair_2U5BU5D_t4038  : public Array_t
{
};
// System.Char[]
// System.Char[]
struct  CharU5BU5D_t119  : public Array_t
{
};
struct CharU5BU5D_t119_StaticFields{
};
// System.UInt16[]
// System.UInt16[]
struct  UInt16U5BU5D_t1066  : public Array_t
{
};
// System.IComparable`1<System.UInt16>[]
// System.IComparable`1<System.UInt16>[]
struct  IComparable_1U5BU5D_t4468  : public Array_t
{
};
// System.IEquatable`1<System.UInt16>[]
// System.IEquatable`1<System.UInt16>[]
struct  IEquatable_1U5BU5D_t4469  : public Array_t
{
};
// System.IComparable`1<System.Char>[]
// System.IComparable`1<System.Char>[]
struct  IComparable_1U5BU5D_t4470  : public Array_t
{
};
// System.IEquatable`1<System.Char>[]
// System.IEquatable`1<System.Char>[]
struct  IEquatable_1U5BU5D_t4471  : public Array_t
{
};
// System.Double[]
// System.Double[]
struct  DoubleU5BU5D_t2593  : public Array_t
{
};
// System.IComparable`1<System.Double>[]
// System.IComparable`1<System.Double>[]
struct  IComparable_1U5BU5D_t4472  : public Array_t
{
};
// System.IEquatable`1<System.Double>[]
// System.IEquatable`1<System.Double>[]
struct  IEquatable_1U5BU5D_t4473  : public Array_t
{
};
// System.Reflection.MethodInfo[]
// System.Reflection.MethodInfo[]
struct  MethodInfoU5BU5D_t149  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>[]
struct  List_1U5BU5D_t3178  : public Array_t
{
};
struct List_1U5BU5D_t3178_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct  KeyValuePair_2U5BU5D_t4079  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>[]
struct  KeyValuePair_2U5BU5D_t4076  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4089  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
// System.Collections.Generic.List`1<UnityEngine.UI.Text>[]
struct  List_1U5BU5D_t3268  : public Array_t
{
};
struct List_1U5BU5D_t3268_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>[]
struct  KeyValuePair_2U5BU5D_t4101  : public Array_t
{
};
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
// System.Collections.Generic.List`1<UnityEngine.UIVertex>[]
struct  List_1U5BU5D_t3286  : public Array_t
{
};
struct List_1U5BU5D_t3286_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>[]
struct  KeyValuePair_2U5BU5D_t4121  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4124  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.ICanvasElement,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4096  : public Array_t
{
};
// System.Enum[]
// System.Enum[]
struct  EnumU5BU5D_t4474  : public Array_t
{
};
struct EnumU5BU5D_t4474_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
// System.Collections.Generic.List`1<UnityEngine.Canvas>[]
struct  List_1U5BU5D_t3365  : public Array_t
{
};
struct List_1U5BU5D_t3365_StaticFields{
};
// System.Collections.Generic.List`1<UnityEngine.Component>[]
// System.Collections.Generic.List`1<UnityEngine.Component>[]
struct  List_1U5BU5D_t3367  : public Array_t
{
};
struct List_1U5BU5D_t3367_StaticFields{
};
// System.Single[]
// System.Single[]
struct  SingleU5BU5D_t591  : public Array_t
{
};
// System.IComparable`1<System.Single>[]
// System.IComparable`1<System.Single>[]
struct  IComparable_1U5BU5D_t4475  : public Array_t
{
};
// System.IEquatable`1<System.Single>[]
// System.IEquatable`1<System.Single>[]
struct  IEquatable_1U5BU5D_t4476  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
// System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,Vuforia.Image>[]
struct  KeyValuePair_2U5BU5D_t4164  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Trackable>[]
struct  KeyValuePair_2U5BU5D_t4172  : public Array_t
{
};
// System.Byte[]
// System.Byte[]
struct  ByteU5BU5D_t622  : public Array_t
{
};
// System.IComparable`1<System.Byte>[]
// System.IComparable`1<System.Byte>[]
struct  IComparable_1U5BU5D_t4477  : public Array_t
{
};
// System.IEquatable`1<System.Byte>[]
// System.IEquatable`1<System.Byte>[]
struct  IEquatable_1U5BU5D_t4478  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButton>[]
struct  KeyValuePair_2U5BU5D_t4177  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Marker>[]
struct  KeyValuePair_2U5BU5D_t4186  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>[]
struct  KeyValuePair_2U5BU5D_t4198  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.UInt16>[]
struct  KeyValuePair_2U5BU5D_t4195  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>[]
struct  KeyValuePair_2U5BU5D_t4205  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordAbstractBehaviour>[]
struct  KeyValuePair_2U5BU5D_t4212  : public Array_t
{
};
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>[]
struct  List_1U5BU5D_t3513  : public Array_t
{
};
struct List_1U5BU5D_t3513_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>>[]
struct  KeyValuePair_2U5BU5D_t4217  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Surface>[]
struct  KeyValuePair_2U5BU5D_t4230  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.SurfaceAbstractBehaviour>[]
struct  KeyValuePair_2U5BU5D_t4235  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.Prop>[]
struct  KeyValuePair_2U5BU5D_t4241  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.PropAbstractBehaviour>[]
struct  KeyValuePair_2U5BU5D_t4246  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour>[]
struct  KeyValuePair_2U5BU5D_t4251  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>[]
struct  KeyValuePair_2U5BU5D_t4256  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>[]
struct  KeyValuePair_2U5BU5D_t4261  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>[]
struct  KeyValuePair_2U5BU5D_t4268  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>[]
struct  KeyValuePair_2U5BU5D_t4277  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
// System.Collections.Generic.KeyValuePair`2<System.String,Vuforia.WebCamProfile/ProfileData>[]
struct  KeyValuePair_2U5BU5D_t4274  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>[]
struct  KeyValuePair_2U5BU5D_t4283  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>[]
struct  KeyValuePair_2U5BU5D_t4319  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>[]
struct  KeyValuePair_2U5BU5D_t4329  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4334  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.String>[]
struct  KeyValuePair_2U5BU5D_t4338  : public Array_t
{
};
// System.Byte[][]
// System.Byte[][]
struct  ByteU5BU5DU5BU5D_t1828  : public Array_t
{
};
// System.IntPtr[]
// System.IntPtr[]
struct  IntPtrU5BU5D_t1368  : public Array_t
{
};
struct IntPtrU5BU5D_t1368_StaticFields{
};
// System.Runtime.Serialization.ISerializable[]
// System.Runtime.Serialization.ISerializable[]
struct  ISerializableU5BU5D_t4479  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>[]
struct  KeyValuePair_2U5BU5D_t4358  : public Array_t
{
};
// System.Int64[]
// System.Int64[]
struct  Int64U5BU5D_t2592  : public Array_t
{
};
// System.IComparable`1<System.Int64>[]
// System.IComparable`1<System.Int64>[]
struct  IComparable_1U5BU5D_t4480  : public Array_t
{
};
// System.IEquatable`1<System.Int64>[]
// System.IEquatable`1<System.Int64>[]
struct  IEquatable_1U5BU5D_t4481  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>[]
struct  KeyValuePair_2U5BU5D_t4355  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Object>[]
struct  KeyValuePair_2U5BU5D_t4376  : public Array_t
{
};
// System.UInt64[]
// System.UInt64[]
struct  UInt64U5BU5D_t2428  : public Array_t
{
};
// System.IComparable`1<System.UInt64>[]
// System.IComparable`1<System.UInt64>[]
struct  IComparable_1U5BU5D_t4482  : public Array_t
{
};
// System.IEquatable`1<System.UInt64>[]
// System.IEquatable`1<System.UInt64>[]
struct  IEquatable_1U5BU5D_t4483  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>[]
struct  KeyValuePair_2U5BU5D_t4372  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct  KeyValuePair_2U5BU5D_t1379  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct  KeyValuePair_2U5BU5D_t4413  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
struct  KeyValuePair_2U5BU5D_t4386  : public Array_t
{
};
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
struct  IDictionary_2U5BU5D_t3858  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>[]
struct  KeyValuePair_2U5BU5D_t4392  : public Array_t
{
};
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>[]
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>[]
struct  IDictionary_2U5BU5D_t3862  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>[]
struct  KeyValuePair_2U5BU5D_t4398  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
struct  KeyValuePair_2U5BU5D_t4404  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>[]
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>[]
struct  KeyValuePair_2U5BU5D_t3870  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>[]
struct  KeyValuePair_2U5BU5D_t4409  : public Array_t
{
};
// System.Reflection.ConstructorInfo[]
// System.Reflection.ConstructorInfo[]
struct  ConstructorInfoU5BU5D_t1430  : public Array_t
{
};
struct ConstructorInfoU5BU5D_t1430_StaticFields{
};
// System.Runtime.InteropServices._ConstructorInfo[]
// System.Runtime.InteropServices._ConstructorInfo[]
struct  _ConstructorInfoU5BU5D_t4484  : public Array_t
{
};
// System.Reflection.MethodBase[]
// System.Reflection.MethodBase[]
struct  MethodBaseU5BU5D_t2597  : public Array_t
{
};
// System.Runtime.InteropServices._MethodBase[]
// System.Runtime.InteropServices._MethodBase[]
struct  _MethodBaseU5BU5D_t4485  : public Array_t
{
};
// System.Reflection.ParameterInfo[]
// System.Reflection.ParameterInfo[]
struct  ParameterInfoU5BU5D_t1431  : public Array_t
{
};
// System.Runtime.InteropServices._ParameterInfo[]
// System.Runtime.InteropServices._ParameterInfo[]
struct  _ParameterInfoU5BU5D_t4486  : public Array_t
{
};
// System.Reflection.PropertyInfo[]
// System.Reflection.PropertyInfo[]
struct  PropertyInfoU5BU5D_t1434  : public Array_t
{
};
// System.Runtime.InteropServices._PropertyInfo[]
// System.Runtime.InteropServices._PropertyInfo[]
struct  _PropertyInfoU5BU5D_t4487  : public Array_t
{
};
// System.Reflection.FieldInfo[]
// System.Reflection.FieldInfo[]
struct  FieldInfoU5BU5D_t1435  : public Array_t
{
};
// System.Runtime.InteropServices._FieldInfo[]
// System.Runtime.InteropServices._FieldInfo[]
struct  _FieldInfoU5BU5D_t4488  : public Array_t
{
};
// System.Attribute[]
// System.Attribute[]
struct  AttributeU5BU5D_t4489  : public Array_t
{
};
// System.Runtime.InteropServices._Attribute[]
// System.Runtime.InteropServices._Attribute[]
struct  _AttributeU5BU5D_t4490  : public Array_t
{
};
// System.Reflection.ParameterModifier[]
// System.Reflection.ParameterModifier[]
struct  ParameterModifierU5BU5D_t1438  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>[]
struct  KeyValuePair_2U5BU5D_t4421  : public Array_t
{
};
// System.UInt32[]
// System.UInt32[]
struct  UInt32U5BU5D_t1668  : public Array_t
{
};
// System.IComparable`1<System.UInt32>[]
// System.IComparable`1<System.UInt32>[]
struct  IComparable_1U5BU5D_t4491  : public Array_t
{
};
// System.IEquatable`1<System.UInt32>[]
// System.IEquatable`1<System.UInt32>[]
struct  IEquatable_1U5BU5D_t4492  : public Array_t
{
};
// System.Security.Cryptography.KeySizes[]
// System.Security.Cryptography.KeySizes[]
struct  KeySizesU5BU5D_t1692  : public Array_t
{
};
// System.Security.Cryptography.X509Certificates.X509Certificate[]
// System.Security.Cryptography.X509Certificates.X509Certificate[]
struct  X509CertificateU5BU5D_t1841  : public Array_t
{
};
// System.Runtime.Serialization.IDeserializationCallback[]
// System.Runtime.Serialization.IDeserializationCallback[]
struct  IDeserializationCallbackU5BU5D_t4493  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>[]
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Byte>[]
struct  KeyValuePair_2U5BU5D_t4439  : public Array_t
{
};
// System.Boolean[]
// System.Boolean[]
struct  BooleanU5BU5D_t1888  : public Array_t
{
};
struct BooleanU5BU5D_t1888_StaticFields{
};
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
// System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>[]
struct  KeyValuePair_2U5BU5D_t4435  : public Array_t
{
};
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct  KeyValuePair_2U5BU5D_t4446  : public Array_t
{
};
// System.Single[,]
// System.Single[,]
struct  SingleU5BU2CU5D_t4494  : public Array_t
{
};
// System.Delegate[]
// System.Delegate[]
struct  DelegateU5BU5D_t2591  : public Array_t
{
};
// System.Int16[]
// System.Int16[]
struct  Int16U5BU5D_t2607  : public Array_t
{
};
// System.IComparable`1<System.Int16>[]
// System.IComparable`1<System.Int16>[]
struct  IComparable_1U5BU5D_t4495  : public Array_t
{
};
// System.IEquatable`1<System.Int16>[]
// System.IEquatable`1<System.Int16>[]
struct  IEquatable_1U5BU5D_t4496  : public Array_t
{
};
// System.SByte[]
// System.SByte[]
struct  SByteU5BU5D_t2471  : public Array_t
{
};
// System.IComparable`1<System.SByte>[]
// System.IComparable`1<System.SByte>[]
struct  IComparable_1U5BU5D_t4497  : public Array_t
{
};
// System.IEquatable`1<System.SByte>[]
// System.IEquatable`1<System.SByte>[]
struct  IEquatable_1U5BU5D_t4498  : public Array_t
{
};
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
// Mono.Globalization.Unicode.CodePointIndexer/TableRange[]
struct  TableRangeU5BU5D_t2076  : public Array_t
{
};
// Mono.Globalization.Unicode.TailoringInfo[]
// Mono.Globalization.Unicode.TailoringInfo[]
struct  TailoringInfoU5BU5D_t2083  : public Array_t
{
};
// Mono.Globalization.Unicode.Contraction[]
// Mono.Globalization.Unicode.Contraction[]
struct  ContractionU5BU5D_t2092  : public Array_t
{
};
// Mono.Globalization.Unicode.Level2Map[]
// Mono.Globalization.Unicode.Level2Map[]
struct  Level2MapU5BU5D_t2093  : public Array_t
{
};
// Mono.Math.BigInteger[]
// Mono.Math.BigInteger[]
struct  BigIntegerU5BU5D_t2594  : public Array_t
{
};
struct BigIntegerU5BU5D_t2594_StaticFields{
};
// System.Collections.Hashtable/Slot[]
// System.Collections.Hashtable/Slot[]
struct  SlotU5BU5D_t2158  : public Array_t
{
};
// System.Collections.SortedList/Slot[]
// System.Collections.SortedList/Slot[]
struct  SlotU5BU5D_t2162  : public Array_t
{
};
// System.Diagnostics.StackFrame[]
// System.Diagnostics.StackFrame[]
struct  StackFrameU5BU5D_t2170  : public Array_t
{
};
// System.Globalization.Calendar[]
// System.Globalization.Calendar[]
struct  CalendarU5BU5D_t2178  : public Array_t
{
};
// System.Reflection.Emit.ModuleBuilder[]
// System.Reflection.Emit.ModuleBuilder[]
struct  ModuleBuilderU5BU5D_t2220  : public Array_t
{
};
struct ModuleBuilderU5BU5D_t2220_StaticFields{
};
// System.Runtime.InteropServices._ModuleBuilder[]
// System.Runtime.InteropServices._ModuleBuilder[]
struct  _ModuleBuilderU5BU5D_t4499  : public Array_t
{
};
// System.Reflection.Module[]
// System.Reflection.Module[]
struct  ModuleU5BU5D_t2596  : public Array_t
{
};
struct ModuleU5BU5D_t2596_StaticFields{
};
// System.Runtime.InteropServices._Module[]
// System.Runtime.InteropServices._Module[]
struct  _ModuleU5BU5D_t4500  : public Array_t
{
};
// System.Reflection.Emit.ParameterBuilder[]
// System.Reflection.Emit.ParameterBuilder[]
struct  ParameterBuilderU5BU5D_t2223  : public Array_t
{
};
// System.Runtime.InteropServices._ParameterBuilder[]
// System.Runtime.InteropServices._ParameterBuilder[]
struct  _ParameterBuilderU5BU5D_t4501  : public Array_t
{
};
// System.Reflection.Emit.GenericTypeParameterBuilder[]
// System.Reflection.Emit.GenericTypeParameterBuilder[]
struct  GenericTypeParameterBuilderU5BU5D_t2230  : public Array_t
{
};
// System.Reflection.Emit.MethodBuilder[]
// System.Reflection.Emit.MethodBuilder[]
struct  MethodBuilderU5BU5D_t2235  : public Array_t
{
};
// System.Runtime.InteropServices._MethodBuilder[]
// System.Runtime.InteropServices._MethodBuilder[]
struct  _MethodBuilderU5BU5D_t4502  : public Array_t
{
};
// System.Runtime.InteropServices._MethodInfo[]
// System.Runtime.InteropServices._MethodInfo[]
struct  _MethodInfoU5BU5D_t4503  : public Array_t
{
};
// System.Reflection.Emit.ConstructorBuilder[]
// System.Reflection.Emit.ConstructorBuilder[]
struct  ConstructorBuilderU5BU5D_t2236  : public Array_t
{
};
// System.Runtime.InteropServices._ConstructorBuilder[]
// System.Runtime.InteropServices._ConstructorBuilder[]
struct  _ConstructorBuilderU5BU5D_t4504  : public Array_t
{
};
// System.Reflection.Emit.PropertyBuilder[]
// System.Reflection.Emit.PropertyBuilder[]
struct  PropertyBuilderU5BU5D_t2237  : public Array_t
{
};
// System.Runtime.InteropServices._PropertyBuilder[]
// System.Runtime.InteropServices._PropertyBuilder[]
struct  _PropertyBuilderU5BU5D_t4505  : public Array_t
{
};
// System.Reflection.Emit.FieldBuilder[]
// System.Reflection.Emit.FieldBuilder[]
struct  FieldBuilderU5BU5D_t2238  : public Array_t
{
};
// System.Runtime.InteropServices._FieldBuilder[]
// System.Runtime.InteropServices._FieldBuilder[]
struct  _FieldBuilderU5BU5D_t4506  : public Array_t
{
};
// System.Runtime.Remoting.Messaging.Header[]
// System.Runtime.Remoting.Messaging.Header[]
struct  HeaderU5BU5D_t2566  : public Array_t
{
};
// System.Runtime.Remoting.Services.ITrackingHandler[]
// System.Runtime.Remoting.Services.ITrackingHandler[]
struct  ITrackingHandlerU5BU5D_t2615  : public Array_t
{
};
// System.Runtime.Remoting.Contexts.IContextAttribute[]
// System.Runtime.Remoting.Contexts.IContextAttribute[]
struct  IContextAttributeU5BU5D_t2603  : public Array_t
{
};
// System.DateTime[]
// System.DateTime[]
struct  DateTimeU5BU5D_t2617  : public Array_t
{
};
struct DateTimeU5BU5D_t2617_StaticFields{
};
// System.IComparable`1<System.DateTime>[]
// System.IComparable`1<System.DateTime>[]
struct  IComparable_1U5BU5D_t4507  : public Array_t
{
};
// System.IEquatable`1<System.DateTime>[]
// System.IEquatable`1<System.DateTime>[]
struct  IEquatable_1U5BU5D_t4508  : public Array_t
{
};
// System.Decimal[]
// System.Decimal[]
struct  DecimalU5BU5D_t2618  : public Array_t
{
};
struct DecimalU5BU5D_t2618_StaticFields{
};
// System.IComparable`1<System.Decimal>[]
// System.IComparable`1<System.Decimal>[]
struct  IComparable_1U5BU5D_t4509  : public Array_t
{
};
// System.IEquatable`1<System.Decimal>[]
// System.IEquatable`1<System.Decimal>[]
struct  IEquatable_1U5BU5D_t4510  : public Array_t
{
};
// System.TimeSpan[]
// System.TimeSpan[]
struct  TimeSpanU5BU5D_t2619  : public Array_t
{
};
struct TimeSpanU5BU5D_t2619_StaticFields{
};
// System.IComparable`1<System.TimeSpan>[]
// System.IComparable`1<System.TimeSpan>[]
struct  IComparable_1U5BU5D_t4511  : public Array_t
{
};
// System.IEquatable`1<System.TimeSpan>[]
// System.IEquatable`1<System.TimeSpan>[]
struct  IEquatable_1U5BU5D_t4512  : public Array_t
{
};
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
// System.Runtime.Serialization.Formatters.Binary.TypeTag[]
struct  TypeTagU5BU5D_t2620  : public Array_t
{
};
// System.MonoType[]
// System.MonoType[]
struct  MonoTypeU5BU5D_t2622  : public Array_t
{
};
// System.Byte[,]
// System.Byte[,]
struct  ByteU5BU2CU5D_t2402  : public Array_t
{
};
// System.Security.Policy.StrongName[]
// System.Security.Policy.StrongName[]
struct  StrongNameU5BU5D_t4015  : public Array_t
{
};
