﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<System.String>
struct DOSetter_1_t1051;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void DG.Tweening.Core.DOSetter`1<System.String>::.ctor(System.Object,System.IntPtr)
// DG.Tweening.Core.DOSetter`1<System.Object>
#include "DOTween_DG_Tweening_Core_DOSetter_1_gen_13MethodDeclarations.h"
#define DOSetter_1__ctor_m23846(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1051 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m23735_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.String>::Invoke(T)
#define DOSetter_1_Invoke_m23847(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1051 *, String_t*, const MethodInfo*))DOSetter_1_Invoke_m23736_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<System.String>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define DOSetter_1_BeginInvoke_m23848(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1051 *, String_t*, AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m23737_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<System.String>::EndInvoke(System.IAsyncResult)
#define DOSetter_1_EndInvoke_m23849(__this, ___result, method) (( void (*) (DOSetter_1_t1051 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m23738_gshared)(__this, ___result, method)
