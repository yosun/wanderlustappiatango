﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Diagnostics.DebuggerTypeProxyAttribute
struct DebuggerTypeProxyAttribute_t2169;
// System.Type
struct Type_t;

// System.Void System.Diagnostics.DebuggerTypeProxyAttribute::.ctor(System.Type)
extern "C" void DebuggerTypeProxyAttribute__ctor_m11070 (DebuggerTypeProxyAttribute_t2169 * __this, Type_t * ___type, const MethodInfo* method) IL2CPP_METHOD_ATTR;
