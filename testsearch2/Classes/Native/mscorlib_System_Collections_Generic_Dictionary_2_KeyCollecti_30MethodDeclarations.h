﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Enumerator_t3566;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Dictionary_2_t875;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m21999_gshared (Enumerator_t3566 * __this, Dictionary_2_t875 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m21999(__this, ___host, method) (( void (*) (Enumerator_t3566 *, Dictionary_2_t875 *, const MethodInfo*))Enumerator__ctor_m21999_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22000_gshared (Enumerator_t3566 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22000(__this, method) (( Object_t * (*) (Enumerator_t3566 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22000_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Dispose()
extern "C" void Enumerator_Dispose_m22001_gshared (Enumerator_t3566 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22001(__this, method) (( void (*) (Enumerator_t3566 *, const MethodInfo*))Enumerator_Dispose_m22001_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22002_gshared (Enumerator_t3566 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22002(__this, method) (( bool (*) (Enumerator_t3566 *, const MethodInfo*))Enumerator_MoveNext_m22002_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Current()
extern "C" int32_t Enumerator_get_Current_m22003_gshared (Enumerator_t3566 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22003(__this, method) (( int32_t (*) (Enumerator_t3566 *, const MethodInfo*))Enumerator_get_Current_m22003_gshared)(__this, method)
