﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t949;

// System.Void DG.Tweening.Core.ABSSequentiable::.ctor()
extern "C" void ABSSequentiable__ctor_m5351 (ABSSequentiable_t949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
