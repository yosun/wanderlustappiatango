﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.CharacterInfo
struct CharacterInfo_t1249;
struct CharacterInfo_t1249_marshaled;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Int32 UnityEngine.CharacterInfo::get_advance()
extern "C" int32_t CharacterInfo_get_advance_m6458 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_glyphWidth()
extern "C" int32_t CharacterInfo_get_glyphWidth_m6459 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_glyphHeight()
extern "C" int32_t CharacterInfo_get_glyphHeight_m6460 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_bearing()
extern "C" int32_t CharacterInfo_get_bearing_m6461 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_minY()
extern "C" int32_t CharacterInfo_get_minY_m6462 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_maxY()
extern "C" int32_t CharacterInfo_get_maxY_m6463 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_minX()
extern "C" int32_t CharacterInfo_get_minX_m6464 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CharacterInfo::get_maxX()
extern "C" int32_t CharacterInfo_get_maxX_m6465 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeftUnFlipped()
extern "C" Vector2_t19  CharacterInfo_get_uvBottomLeftUnFlipped_m6466 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRightUnFlipped()
extern "C" Vector2_t19  CharacterInfo_get_uvBottomRightUnFlipped_m6467 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRightUnFlipped()
extern "C" Vector2_t19  CharacterInfo_get_uvTopRightUnFlipped_m6468 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeftUnFlipped()
extern "C" Vector2_t19  CharacterInfo_get_uvTopLeftUnFlipped_m6469 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeft()
extern "C" Vector2_t19  CharacterInfo_get_uvBottomLeft_m6470 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRight()
extern "C" Vector2_t19  CharacterInfo_get_uvBottomRight_m6471 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRight()
extern "C" Vector2_t19  CharacterInfo_get_uvTopRight_m6472 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeft()
extern "C" Vector2_t19  CharacterInfo_get_uvTopLeft_m6473 (CharacterInfo_t1249 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
void CharacterInfo_t1249_marshal(const CharacterInfo_t1249& unmarshaled, CharacterInfo_t1249_marshaled& marshaled);
void CharacterInfo_t1249_marshal_back(const CharacterInfo_t1249_marshaled& marshaled, CharacterInfo_t1249& unmarshaled);
void CharacterInfo_t1249_marshal_cleanup(CharacterInfo_t1249_marshaled& marshaled);
