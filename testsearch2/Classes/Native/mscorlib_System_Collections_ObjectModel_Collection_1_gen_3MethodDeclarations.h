﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>
struct Collection_1_t3601;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t3596;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t802;
// System.Collections.Generic.IList`1<Vuforia.TargetFinder/TargetSearchResult>
struct IList_1_t3599;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor()
extern "C" void Collection_1__ctor_m22392_gshared (Collection_1_t3601 * __this, const MethodInfo* method);
#define Collection_1__ctor_m22392(__this, method) (( void (*) (Collection_1_t3601 *, const MethodInfo*))Collection_1__ctor_m22392_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22393_gshared (Collection_1_t3601 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22393(__this, method) (( bool (*) (Collection_1_t3601 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22393_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m22394_gshared (Collection_1_t3601 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m22394(__this, ___array, ___index, method) (( void (*) (Collection_1_t3601 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m22394_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m22395_gshared (Collection_1_t3601 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m22395(__this, method) (( Object_t * (*) (Collection_1_t3601 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m22395_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m22396_gshared (Collection_1_t3601 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m22396(__this, ___value, method) (( int32_t (*) (Collection_1_t3601 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m22396_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m22397_gshared (Collection_1_t3601 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m22397(__this, ___value, method) (( bool (*) (Collection_1_t3601 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m22397_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m22398_gshared (Collection_1_t3601 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m22398(__this, ___value, method) (( int32_t (*) (Collection_1_t3601 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m22398_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m22399_gshared (Collection_1_t3601 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m22399(__this, ___index, ___value, method) (( void (*) (Collection_1_t3601 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m22399_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m22400_gshared (Collection_1_t3601 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m22400(__this, ___value, method) (( void (*) (Collection_1_t3601 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m22400_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m22401_gshared (Collection_1_t3601 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m22401(__this, method) (( bool (*) (Collection_1_t3601 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m22401_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m22402_gshared (Collection_1_t3601 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m22402(__this, method) (( Object_t * (*) (Collection_1_t3601 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m22402_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m22403_gshared (Collection_1_t3601 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m22403(__this, method) (( bool (*) (Collection_1_t3601 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m22403_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m22404_gshared (Collection_1_t3601 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m22404(__this, method) (( bool (*) (Collection_1_t3601 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m22404_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m22405_gshared (Collection_1_t3601 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m22405(__this, ___index, method) (( Object_t * (*) (Collection_1_t3601 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m22405_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m22406_gshared (Collection_1_t3601 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m22406(__this, ___index, ___value, method) (( void (*) (Collection_1_t3601 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m22406_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Add(T)
extern "C" void Collection_1_Add_m22407_gshared (Collection_1_t3601 * __this, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define Collection_1_Add_m22407(__this, ___item, method) (( void (*) (Collection_1_t3601 *, TargetSearchResult_t726 , const MethodInfo*))Collection_1_Add_m22407_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Clear()
extern "C" void Collection_1_Clear_m22408_gshared (Collection_1_t3601 * __this, const MethodInfo* method);
#define Collection_1_Clear_m22408(__this, method) (( void (*) (Collection_1_t3601 *, const MethodInfo*))Collection_1_Clear_m22408_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::ClearItems()
extern "C" void Collection_1_ClearItems_m22409_gshared (Collection_1_t3601 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m22409(__this, method) (( void (*) (Collection_1_t3601 *, const MethodInfo*))Collection_1_ClearItems_m22409_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
extern "C" bool Collection_1_Contains_m22410_gshared (Collection_1_t3601 * __this, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define Collection_1_Contains_m22410(__this, ___item, method) (( bool (*) (Collection_1_t3601 *, TargetSearchResult_t726 , const MethodInfo*))Collection_1_Contains_m22410_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m22411_gshared (Collection_1_t3601 * __this, TargetSearchResultU5BU5D_t3596* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m22411(__this, ___array, ___index, method) (( void (*) (Collection_1_t3601 *, TargetSearchResultU5BU5D_t3596*, int32_t, const MethodInfo*))Collection_1_CopyTo_m22411_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m22412_gshared (Collection_1_t3601 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m22412(__this, method) (( Object_t* (*) (Collection_1_t3601 *, const MethodInfo*))Collection_1_GetEnumerator_m22412_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m22413_gshared (Collection_1_t3601 * __this, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m22413(__this, ___item, method) (( int32_t (*) (Collection_1_t3601 *, TargetSearchResult_t726 , const MethodInfo*))Collection_1_IndexOf_m22413_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m22414_gshared (Collection_1_t3601 * __this, int32_t ___index, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define Collection_1_Insert_m22414(__this, ___index, ___item, method) (( void (*) (Collection_1_t3601 *, int32_t, TargetSearchResult_t726 , const MethodInfo*))Collection_1_Insert_m22414_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m22415_gshared (Collection_1_t3601 * __this, int32_t ___index, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m22415(__this, ___index, ___item, method) (( void (*) (Collection_1_t3601 *, int32_t, TargetSearchResult_t726 , const MethodInfo*))Collection_1_InsertItem_m22415_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::Remove(T)
extern "C" bool Collection_1_Remove_m22416_gshared (Collection_1_t3601 * __this, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define Collection_1_Remove_m22416(__this, ___item, method) (( bool (*) (Collection_1_t3601 *, TargetSearchResult_t726 , const MethodInfo*))Collection_1_Remove_m22416_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m22417_gshared (Collection_1_t3601 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m22417(__this, ___index, method) (( void (*) (Collection_1_t3601 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m22417_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m22418_gshared (Collection_1_t3601 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m22418(__this, ___index, method) (( void (*) (Collection_1_t3601 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m22418_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
extern "C" int32_t Collection_1_get_Count_m22419_gshared (Collection_1_t3601 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m22419(__this, method) (( int32_t (*) (Collection_1_t3601 *, const MethodInfo*))Collection_1_get_Count_m22419_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
extern "C" TargetSearchResult_t726  Collection_1_get_Item_m22420_gshared (Collection_1_t3601 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m22420(__this, ___index, method) (( TargetSearchResult_t726  (*) (Collection_1_t3601 *, int32_t, const MethodInfo*))Collection_1_get_Item_m22420_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m22421_gshared (Collection_1_t3601 * __this, int32_t ___index, TargetSearchResult_t726  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m22421(__this, ___index, ___value, method) (( void (*) (Collection_1_t3601 *, int32_t, TargetSearchResult_t726 , const MethodInfo*))Collection_1_set_Item_m22421_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m22422_gshared (Collection_1_t3601 * __this, int32_t ___index, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m22422(__this, ___index, ___item, method) (( void (*) (Collection_1_t3601 *, int32_t, TargetSearchResult_t726 , const MethodInfo*))Collection_1_SetItem_m22422_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m22423_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m22423(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m22423_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::ConvertItem(System.Object)
extern "C" TargetSearchResult_t726  Collection_1_ConvertItem_m22424_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m22424(__this /* static, unused */, ___item, method) (( TargetSearchResult_t726  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m22424_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m22425_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m22425(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m22425_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m22426_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m22426(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m22426_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Vuforia.TargetFinder/TargetSearchResult>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m22427_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m22427(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m22427_gshared)(__this /* static, unused */, ___list, method)
