﻿#pragma once
#include <stdint.h>
// DG.Tweening.TweenCallback
struct TweenCallback_t109;
// System.Object
#include "mscorlib_System_Object.h"
// DG.Tweening.TweenType
#include "DOTween_DG_Tweening_TweenType.h"
// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_t949  : public Object_t
{
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_t109 * ___onStart_3;
};
