﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttribute.h"
// System.Diagnostics.DebuggableAttribute
#include "mscorlib_System_Diagnostics_DebuggableAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxati.h"
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilationRelaxatiMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribuMethodDeclarations.h"
extern TypeInfo* InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var;
extern TypeInfo* DebuggableAttribute_t903_il2cpp_TypeInfo_var;
extern TypeInfo* CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var;
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void g_Qualcomm_Vuforia_UnityExtensions_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1282);
		DebuggableAttribute_t903_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1283);
		CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1284);
		RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 10;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.Eyewear"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		DebuggableAttribute_t903 * tmp;
		tmp = (DebuggableAttribute_t903 *)il2cpp_codegen_object_new (DebuggableAttribute_t903_il2cpp_TypeInfo_var);
		DebuggableAttribute__ctor_m4698(tmp, 2, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		CompilationRelaxationsAttribute_t904 * tmp;
		tmp = (CompilationRelaxationsAttribute_t904 *)il2cpp_codegen_object_new (CompilationRelaxationsAttribute_t904_il2cpp_TypeInfo_var);
		CompilationRelaxationsAttribute__ctor_m4699(tmp, 8, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t167 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t167 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m534(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m535(tmp, true, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.NUnitTests"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.Editor"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.Premium.Editor"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.Premium"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("Qualcomm.Vuforia.UnityExtensions.UnitTests"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspector.h"
// UnityEngine.HideInInspector
#include "UnityEngine_UnityEngine_HideInInspectorMethodDeclarations.h"
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void BackgroundPlaneAbstractBehaviour_t40_CustomAttributesCacheGenerator_mNumDivisions(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void UnityCameraExtensions_t570_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void UnityCameraExtensions_t570_CustomAttributesCacheGenerator_UnityCameraExtensions_GetPixelHeightInt_m2673(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void UnityCameraExtensions_t570_CustomAttributesCacheGenerator_UnityCameraExtensions_GetPixelWidthInt_m2674(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void UnityCameraExtensions_t570_CustomAttributesCacheGenerator_UnityCameraExtensions_GetMaxDepthForVideoBackground_m2675(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExtensionAttribute_t905_il2cpp_TypeInfo_var;
void UnityCameraExtensions_t570_CustomAttributesCacheGenerator_UnityCameraExtensions_GetMinDepthForVideoBackground_m2676(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExtensionAttribute_t905_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1285);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExtensionAttribute_t905 * tmp;
		tmp = (ExtensionAttribute_t905 *)il2cpp_codegen_object_new (ExtensionAttribute_t905_il2cpp_TypeInfo_var);
		ExtensionAttribute__ctor_m4700(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void TrackableBehaviour_t52_CustomAttributesCacheGenerator_mTrackableName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void TrackableBehaviour_t52_CustomAttributesCacheGenerator_mPreviousScale(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void TrackableBehaviour_t52_CustomAttributesCacheGenerator_mPreserveChildSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void TrackableBehaviour_t52_CustomAttributesCacheGenerator_mInitializedInEditor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mDataSetPath(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mExtendedTracking(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mInitializeSmartTerrain(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mReconstructionToInitialize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mSmartTerrainOccluderBoundsMin(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mSmartTerrainOccluderBoundsMax(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mIsSmartTerrainOccluderOffset(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mSmartTerrainOccluderOffset(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mSmartTerrainOccluderRotation(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mSmartTerrainOccluderLockedInPlace(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mAutoSetOccluderFromTargetSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void ObjectTargetAbstractBehaviour_t72_CustomAttributesCacheGenerator_mAspectRatioXY(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void ObjectTargetAbstractBehaviour_t72_CustomAttributesCacheGenerator_mAspectRatioXZ(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void ObjectTargetAbstractBehaviour_t72_CustomAttributesCacheGenerator_mShowBoundingBox(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ObjectTargetAbstractBehaviour_t72_CustomAttributesCacheGenerator_mBBoxMin(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void ObjectTargetAbstractBehaviour_t72_CustomAttributesCacheGenerator_mBBoxMax(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void ObjectTargetAbstractBehaviour_t72_CustomAttributesCacheGenerator_mPreviewImage(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void TrackableImpl_t583_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void TrackableImpl_t583_CustomAttributesCacheGenerator_U3CIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void TrackableImpl_t583_CustomAttributesCacheGenerator_TrackableImpl_get_Name_m2734(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void TrackableImpl_t583_CustomAttributesCacheGenerator_TrackableImpl_set_Name_m2735(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void TrackableImpl_t583_CustomAttributesCacheGenerator_TrackableImpl_get_ID_m2736(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void TrackableImpl_t583_CustomAttributesCacheGenerator_TrackableImpl_set_ID_m2737(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
// Vuforia.ReconstructionAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ReconstructionAbstr.h"
extern const Il2CppType* ReconstructionAbstractBehaviour_t76_0_0_0_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
void ReconstructionFromTargetAbstractBehaviour_t78_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReconstructionAbstractBehaviour_t76_0_0_0_var = il2cpp_codegen_type_from_index(244);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(ReconstructionAbstractBehaviour_t76_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void SmartTerrainTrackableBehaviour_t597_CustomAttributesCacheGenerator_mMeshFilterToUpdate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void SmartTerrainTrackableBehaviour_t597_CustomAttributesCacheGenerator_mMeshColliderToUpdate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void SmartTerrainTrackerAbstractBehaviour_t80_CustomAttributesCacheGenerator_mAutoInitTracker(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void SmartTerrainTrackerAbstractBehaviour_t80_CustomAttributesCacheGenerator_mAutoStartTracker(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void SmartTerrainTrackerAbstractBehaviour_t80_CustomAttributesCacheGenerator_mAutoInitBuilder(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void SmartTerrainTrackerAbstractBehaviour_t80_CustomAttributesCacheGenerator_mSceneUnitsToMillimeter(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void CylinderTargetAbstractBehaviour_t44_CustomAttributesCacheGenerator_mTopDiameterRatio(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void CylinderTargetAbstractBehaviour_t44_CustomAttributesCacheGenerator_mBottomDiameterRatio(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void DataSet_t600_CustomAttributesCacheGenerator_DataSet_Exists_m2828(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is deprecated, please use the QCARUnity.StorageType version of the Exists method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void DataSet_t600_CustomAttributesCacheGenerator_DataSet_Load_m4844(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is deprecated, please use the QCARUnity.StorageType version of the Load method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void DataSetLoadAbstractBehaviour_t46_CustomAttributesCacheGenerator_mDataSetsToLoad(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void DataSetLoadAbstractBehaviour_t46_CustomAttributesCacheGenerator_mDataSetsToActivate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void DataSetLoadAbstractBehaviour_t46_CustomAttributesCacheGenerator_mExternalDatasetRoots(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t604_CustomAttributesCacheGenerator_U3CCenterU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t604_CustomAttributesCacheGenerator_U3CHalfExtentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t604_CustomAttributesCacheGenerator_U3CRotationU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t604_CustomAttributesCacheGenerator_OrientedBoundingBox_get_Center_m2836(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t604_CustomAttributesCacheGenerator_OrientedBoundingBox_set_Center_m2837(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t604_CustomAttributesCacheGenerator_OrientedBoundingBox_get_HalfExtents_m2838(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t604_CustomAttributesCacheGenerator_OrientedBoundingBox_set_HalfExtents_m2839(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t604_CustomAttributesCacheGenerator_OrientedBoundingBox_get_Rotation_m2840(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox_t604_CustomAttributesCacheGenerator_OrientedBoundingBox_set_Rotation_m2841(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_U3CCenterU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_U3CHalfExtentsU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_U3CRotationYU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_OrientedBoundingBox3D_get_Center_m2843(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_OrientedBoundingBox3D_set_Center_m2844(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_OrientedBoundingBox3D_get_HalfExtents_m2845(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_OrientedBoundingBox3D_set_HalfExtents_m2846(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_OrientedBoundingBox3D_get_RotationY_m2847(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_OrientedBoundingBox3D_set_RotationY_m2848(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void DataSetImpl_t584_CustomAttributesCacheGenerator_DataSetImpl_Load_m2912(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("The DataSet.StorageType enumeration is deprecated, please use QCARUnity.StorageType instead"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Tracker_t629_CustomAttributesCacheGenerator_U3CIsActiveU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Tracker_t629_CustomAttributesCacheGenerator_Tracker_get_IsActive_m2965(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Tracker_t629_CustomAttributesCacheGenerator_Tracker_set_IsActive_m2966(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MarkerImpl_t634_CustomAttributesCacheGenerator_U3CMarkerIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MarkerImpl_t634_CustomAttributesCacheGenerator_MarkerImpl_get_MarkerID_m2983(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void MarkerImpl_t634_CustomAttributesCacheGenerator_MarkerImpl_set_MarkerID_m2984(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void QCARManagerImpl_t666_CustomAttributesCacheGenerator_U3CVideoBackgroundTextureSetU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void QCARManagerImpl_t666_CustomAttributesCacheGenerator_QCARManagerImpl_set_VideoBackgroundTextureSet_m3034(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void QCARManagerImpl_t666_CustomAttributesCacheGenerator_QCARManagerImpl_get_VideoBackgroundTextureSet_m3035(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CU3Ec__DisplayClass3_t661_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void SmartTerrainTrackableImpl_t675_CustomAttributesCacheGenerator_U3CParentU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void SmartTerrainTrackableImpl_t675_CustomAttributesCacheGenerator_SmartTerrainTrackableImpl_get_Parent_m3080(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void SmartTerrainTrackableImpl_t675_CustomAttributesCacheGenerator_SmartTerrainTrackableImpl_set_Parent_m3081(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void WordList_t684_CustomAttributesCacheGenerator_WordList_LoadWordListFile_m5056(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is obsolete, please use the QCARUnity.StorageType enum overloaded method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void WordList_t684_CustomAttributesCacheGenerator_WordList_AddWordsFromFile_m5059(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is obsolete, please use the QCARUnity.StorageType enum overloaded method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void WordList_t684_CustomAttributesCacheGenerator_WordList_LoadFilterListFile_m5068(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is obsolete, please use the QCARUnity.StorageType enum overloaded method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void WordListImpl_t704_CustomAttributesCacheGenerator_WordListImpl_LoadWordListFile_m3175(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is obsolete, please use the QCARUnity.StorageType enum overloaded method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void WordListImpl_t704_CustomAttributesCacheGenerator_WordListImpl_AddWordsFromFile_m3178(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is obsolete, please use the QCARUnity.StorageType enum overloaded method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void WordListImpl_t704_CustomAttributesCacheGenerator_WordListImpl_LoadFilterListFile_m3187(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("DataSet.StorageType is obsolete, please use the QCARUnity.StorageType enum overloaded method instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void ISmartTerrainEventHandler_t783_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("This interface will be removed with the next Vuforia release. Please use ReconstructionBehaviour.Register...Callback instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// Vuforia.QCARAbstractBehaviour
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARAbstractBehavio.h"
extern const Il2CppType* QCARAbstractBehaviour_t75_0_0_0_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QCARAbstractBehaviour_t75_0_0_0_var = il2cpp_codegen_type_from_index(42);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(QCARAbstractBehaviour_t75_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator_mKeepARCameraAlive(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator_mKeepTrackableBehavioursAlive(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator_mKeepTextRecoBehaviourAlive(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator_mKeepUDTBuildingBehaviourAlive(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator_mKeepCloudRecoBehaviourAlive(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator_mKeepSmartTerrainAlive(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_mInitializedInEditor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_mMaximumExtentEnabled(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_mMaximumExtent(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_mAutomaticStart(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_mNavMeshUpdates(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_mNavMeshPadding(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_ReconstructionAbstractBehaviour_RegisterSmartTerrainEventHandler_m3991(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("This ISmartTerrainEventHandler interface will be removed with the next Vuforia release. Please use ReconstructionBehaviour.Register...Callback instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void PropAbstractBehaviour_t73_CustomAttributesCacheGenerator_mBoxColliderToUpdate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void TrackableSourceImpl_t732_CustomAttributesCacheGenerator_U3CTrackableSourcePtrU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void TrackableSourceImpl_t732_CustomAttributesCacheGenerator_TrackableSourceImpl_get_TrackableSourcePtr_m4077(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void TrackableSourceImpl_t732_CustomAttributesCacheGenerator_TrackableSourceImpl_set_TrackableSourcePtr_m4078(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void WebCamImpl_t616_CustomAttributesCacheGenerator_U3CIsTextureSizeAvailableU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void WebCamImpl_t616_CustomAttributesCacheGenerator_WebCamImpl_get_IsTextureSizeAvailable_m4103(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void WebCamImpl_t616_CustomAttributesCacheGenerator_WebCamImpl_set_IsTextureSizeAvailable_m4104(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void ITrackerEventHandler_t794_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("This interface will be removed with the next Vuforia release. Please use QCARBehaviour.RegisterQCARStartedCallback and RegisterTrackablesUpdatedCallback instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void ImageTargetAbstractBehaviour_t58_CustomAttributesCacheGenerator_mAspectRatio(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ImageTargetAbstractBehaviour_t58_CustomAttributesCacheGenerator_mImageTargetType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void MarkerAbstractBehaviour_t66_CustomAttributesCacheGenerator_mMarkerID(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_VuforiaLicenseKey(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mCameraOffset(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mAutoAdjustStereoCameraSkewing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mSceneScaleFactor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_CameraDeviceModeSetting(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_MaxSimultaneousImageTargets(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_MaxSimultaneousObjectTargets(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_UseDelayedLoadingObjectTargets(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_CameraDirection(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_MirrorVideoBackground(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mWorldCenterMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mWorldCenter(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mPrimaryCamera(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mSecondaryCamera(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_QCARAbstractBehaviour_RegisterTrackerEventHandler_m4166(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("The ITrackerEventHandler interface will be removed with the next Vuforia release. Please use QCARBehaviour.RegisterQCARStartedCallback and RegisterTrackablesUpdatedCallback instead."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mWordListFile(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mCustomWordListFile(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mAdditionalCustomWords(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mFilterMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mFilterListFile(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mAdditionalFilterWords(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mWordPrefabCreationMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mMaximumWordInstances(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
extern const Il2CppType* Camera_t3_0_0_0_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
void VideoBackgroundAbstractBehaviour_t90_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t3_0_0_0_var = il2cpp_codegen_type_from_index(29);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(Camera_t3_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void VideoBackgroundAbstractBehaviour_t90_CustomAttributesCacheGenerator_U3CVideoBackGroundMirroredU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void VideoBackgroundAbstractBehaviour_t90_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4246(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void VideoBackgroundAbstractBehaviour_t90_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4247(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void VirtualButtonAbstractBehaviour_t94_CustomAttributesCacheGenerator_mName(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void VirtualButtonAbstractBehaviour_t94_CustomAttributesCacheGenerator_mSensitivity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void VirtualButtonAbstractBehaviour_t94_CustomAttributesCacheGenerator_mHasUpdatedPose(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void VirtualButtonAbstractBehaviour_t94_CustomAttributesCacheGenerator_mPrevTransform(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void VirtualButtonAbstractBehaviour_t94_CustomAttributesCacheGenerator_mPrevParent(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void WebCamAbstractBehaviour_t96_CustomAttributesCacheGenerator_mPlayModeRenderVideo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void WebCamAbstractBehaviour_t96_CustomAttributesCacheGenerator_mDeviceNameSetInEditor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void WebCamAbstractBehaviour_t96_CustomAttributesCacheGenerator_mFlipHorizontally(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void WebCamAbstractBehaviour_t96_CustomAttributesCacheGenerator_mTurnOffWebCam(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void WordAbstractBehaviour_t101_CustomAttributesCacheGenerator_mMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* HideInInspector_t906_il2cpp_TypeInfo_var;
void WordAbstractBehaviour_t101_CustomAttributesCacheGenerator_mSpecificWord(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		HideInInspector_t906_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1286);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		HideInInspector_t906 * tmp;
		tmp = (HideInInspector_t906 *)il2cpp_codegen_object_new (HideInInspector_t906_il2cpp_TypeInfo_var);
		HideInInspector__ctor_m4714(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_Qualcomm_Vuforia_UnityExtensions_Assembly_AttributeGenerators[152] = 
{
	NULL,
	g_Qualcomm_Vuforia_UnityExtensions_Assembly_CustomAttributesCacheGenerator,
	BackgroundPlaneAbstractBehaviour_t40_CustomAttributesCacheGenerator_mNumDivisions,
	UnityCameraExtensions_t570_CustomAttributesCacheGenerator,
	UnityCameraExtensions_t570_CustomAttributesCacheGenerator_UnityCameraExtensions_GetPixelHeightInt_m2673,
	UnityCameraExtensions_t570_CustomAttributesCacheGenerator_UnityCameraExtensions_GetPixelWidthInt_m2674,
	UnityCameraExtensions_t570_CustomAttributesCacheGenerator_UnityCameraExtensions_GetMaxDepthForVideoBackground_m2675,
	UnityCameraExtensions_t570_CustomAttributesCacheGenerator_UnityCameraExtensions_GetMinDepthForVideoBackground_m2676,
	TrackableBehaviour_t52_CustomAttributesCacheGenerator_mTrackableName,
	TrackableBehaviour_t52_CustomAttributesCacheGenerator_mPreviousScale,
	TrackableBehaviour_t52_CustomAttributesCacheGenerator_mPreserveChildSize,
	TrackableBehaviour_t52_CustomAttributesCacheGenerator_mInitializedInEditor,
	DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mDataSetPath,
	DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mExtendedTracking,
	DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mInitializeSmartTerrain,
	DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mReconstructionToInitialize,
	DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mSmartTerrainOccluderBoundsMin,
	DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mSmartTerrainOccluderBoundsMax,
	DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mIsSmartTerrainOccluderOffset,
	DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mSmartTerrainOccluderOffset,
	DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mSmartTerrainOccluderRotation,
	DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mSmartTerrainOccluderLockedInPlace,
	DataSetTrackableBehaviour_t573_CustomAttributesCacheGenerator_mAutoSetOccluderFromTargetSize,
	ObjectTargetAbstractBehaviour_t72_CustomAttributesCacheGenerator_mAspectRatioXY,
	ObjectTargetAbstractBehaviour_t72_CustomAttributesCacheGenerator_mAspectRatioXZ,
	ObjectTargetAbstractBehaviour_t72_CustomAttributesCacheGenerator_mShowBoundingBox,
	ObjectTargetAbstractBehaviour_t72_CustomAttributesCacheGenerator_mBBoxMin,
	ObjectTargetAbstractBehaviour_t72_CustomAttributesCacheGenerator_mBBoxMax,
	ObjectTargetAbstractBehaviour_t72_CustomAttributesCacheGenerator_mPreviewImage,
	TrackableImpl_t583_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	TrackableImpl_t583_CustomAttributesCacheGenerator_U3CIDU3Ek__BackingField,
	TrackableImpl_t583_CustomAttributesCacheGenerator_TrackableImpl_get_Name_m2734,
	TrackableImpl_t583_CustomAttributesCacheGenerator_TrackableImpl_set_Name_m2735,
	TrackableImpl_t583_CustomAttributesCacheGenerator_TrackableImpl_get_ID_m2736,
	TrackableImpl_t583_CustomAttributesCacheGenerator_TrackableImpl_set_ID_m2737,
	ReconstructionFromTargetAbstractBehaviour_t78_CustomAttributesCacheGenerator,
	SmartTerrainTrackableBehaviour_t597_CustomAttributesCacheGenerator_mMeshFilterToUpdate,
	SmartTerrainTrackableBehaviour_t597_CustomAttributesCacheGenerator_mMeshColliderToUpdate,
	SmartTerrainTrackerAbstractBehaviour_t80_CustomAttributesCacheGenerator_mAutoInitTracker,
	SmartTerrainTrackerAbstractBehaviour_t80_CustomAttributesCacheGenerator_mAutoStartTracker,
	SmartTerrainTrackerAbstractBehaviour_t80_CustomAttributesCacheGenerator_mAutoInitBuilder,
	SmartTerrainTrackerAbstractBehaviour_t80_CustomAttributesCacheGenerator_mSceneUnitsToMillimeter,
	CylinderTargetAbstractBehaviour_t44_CustomAttributesCacheGenerator_mTopDiameterRatio,
	CylinderTargetAbstractBehaviour_t44_CustomAttributesCacheGenerator_mBottomDiameterRatio,
	DataSet_t600_CustomAttributesCacheGenerator_DataSet_Exists_m2828,
	DataSet_t600_CustomAttributesCacheGenerator_DataSet_Load_m4844,
	DataSetLoadAbstractBehaviour_t46_CustomAttributesCacheGenerator_mDataSetsToLoad,
	DataSetLoadAbstractBehaviour_t46_CustomAttributesCacheGenerator_mDataSetsToActivate,
	DataSetLoadAbstractBehaviour_t46_CustomAttributesCacheGenerator_mExternalDatasetRoots,
	OrientedBoundingBox_t604_CustomAttributesCacheGenerator_U3CCenterU3Ek__BackingField,
	OrientedBoundingBox_t604_CustomAttributesCacheGenerator_U3CHalfExtentsU3Ek__BackingField,
	OrientedBoundingBox_t604_CustomAttributesCacheGenerator_U3CRotationU3Ek__BackingField,
	OrientedBoundingBox_t604_CustomAttributesCacheGenerator_OrientedBoundingBox_get_Center_m2836,
	OrientedBoundingBox_t604_CustomAttributesCacheGenerator_OrientedBoundingBox_set_Center_m2837,
	OrientedBoundingBox_t604_CustomAttributesCacheGenerator_OrientedBoundingBox_get_HalfExtents_m2838,
	OrientedBoundingBox_t604_CustomAttributesCacheGenerator_OrientedBoundingBox_set_HalfExtents_m2839,
	OrientedBoundingBox_t604_CustomAttributesCacheGenerator_OrientedBoundingBox_get_Rotation_m2840,
	OrientedBoundingBox_t604_CustomAttributesCacheGenerator_OrientedBoundingBox_set_Rotation_m2841,
	OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_U3CCenterU3Ek__BackingField,
	OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_U3CHalfExtentsU3Ek__BackingField,
	OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_U3CRotationYU3Ek__BackingField,
	OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_OrientedBoundingBox3D_get_Center_m2843,
	OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_OrientedBoundingBox3D_set_Center_m2844,
	OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_OrientedBoundingBox3D_get_HalfExtents_m2845,
	OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_OrientedBoundingBox3D_set_HalfExtents_m2846,
	OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_OrientedBoundingBox3D_get_RotationY_m2847,
	OrientedBoundingBox3D_t605_CustomAttributesCacheGenerator_OrientedBoundingBox3D_set_RotationY_m2848,
	DataSetImpl_t584_CustomAttributesCacheGenerator_DataSetImpl_Load_m2912,
	Tracker_t629_CustomAttributesCacheGenerator_U3CIsActiveU3Ek__BackingField,
	Tracker_t629_CustomAttributesCacheGenerator_Tracker_get_IsActive_m2965,
	Tracker_t629_CustomAttributesCacheGenerator_Tracker_set_IsActive_m2966,
	MarkerImpl_t634_CustomAttributesCacheGenerator_U3CMarkerIDU3Ek__BackingField,
	MarkerImpl_t634_CustomAttributesCacheGenerator_MarkerImpl_get_MarkerID_m2983,
	MarkerImpl_t634_CustomAttributesCacheGenerator_MarkerImpl_set_MarkerID_m2984,
	QCARManagerImpl_t666_CustomAttributesCacheGenerator_U3CVideoBackgroundTextureSetU3Ek__BackingField,
	QCARManagerImpl_t666_CustomAttributesCacheGenerator_QCARManagerImpl_set_VideoBackgroundTextureSet_m3034,
	QCARManagerImpl_t666_CustomAttributesCacheGenerator_QCARManagerImpl_get_VideoBackgroundTextureSet_m3035,
	U3CU3Ec__DisplayClass3_t661_CustomAttributesCacheGenerator,
	SmartTerrainTrackableImpl_t675_CustomAttributesCacheGenerator_U3CParentU3Ek__BackingField,
	SmartTerrainTrackableImpl_t675_CustomAttributesCacheGenerator_SmartTerrainTrackableImpl_get_Parent_m3080,
	SmartTerrainTrackableImpl_t675_CustomAttributesCacheGenerator_SmartTerrainTrackableImpl_set_Parent_m3081,
	WordList_t684_CustomAttributesCacheGenerator_WordList_LoadWordListFile_m5056,
	WordList_t684_CustomAttributesCacheGenerator_WordList_AddWordsFromFile_m5059,
	WordList_t684_CustomAttributesCacheGenerator_WordList_LoadFilterListFile_m5068,
	WordListImpl_t704_CustomAttributesCacheGenerator_WordListImpl_LoadWordListFile_m3175,
	WordListImpl_t704_CustomAttributesCacheGenerator_WordListImpl_AddWordsFromFile_m3178,
	WordListImpl_t704_CustomAttributesCacheGenerator_WordListImpl_LoadFilterListFile_m3187,
	ISmartTerrainEventHandler_t783_CustomAttributesCacheGenerator,
	KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator,
	KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator_mKeepARCameraAlive,
	KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator_mKeepTrackableBehavioursAlive,
	KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator_mKeepTextRecoBehaviourAlive,
	KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator_mKeepUDTBuildingBehaviourAlive,
	KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator_mKeepCloudRecoBehaviourAlive,
	KeepAliveAbstractBehaviour_t64_CustomAttributesCacheGenerator_mKeepSmartTerrainAlive,
	ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_mInitializedInEditor,
	ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_mMaximumExtentEnabled,
	ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_mMaximumExtent,
	ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_mAutomaticStart,
	ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_mNavMeshUpdates,
	ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_mNavMeshPadding,
	ReconstructionAbstractBehaviour_t76_CustomAttributesCacheGenerator_ReconstructionAbstractBehaviour_RegisterSmartTerrainEventHandler_m3991,
	PropAbstractBehaviour_t73_CustomAttributesCacheGenerator_mBoxColliderToUpdate,
	TrackableSourceImpl_t732_CustomAttributesCacheGenerator_U3CTrackableSourcePtrU3Ek__BackingField,
	TrackableSourceImpl_t732_CustomAttributesCacheGenerator_TrackableSourceImpl_get_TrackableSourcePtr_m4077,
	TrackableSourceImpl_t732_CustomAttributesCacheGenerator_TrackableSourceImpl_set_TrackableSourcePtr_m4078,
	WebCamImpl_t616_CustomAttributesCacheGenerator_U3CIsTextureSizeAvailableU3Ek__BackingField,
	WebCamImpl_t616_CustomAttributesCacheGenerator_WebCamImpl_get_IsTextureSizeAvailable_m4103,
	WebCamImpl_t616_CustomAttributesCacheGenerator_WebCamImpl_set_IsTextureSizeAvailable_m4104,
	ITrackerEventHandler_t794_CustomAttributesCacheGenerator,
	ImageTargetAbstractBehaviour_t58_CustomAttributesCacheGenerator_mAspectRatio,
	ImageTargetAbstractBehaviour_t58_CustomAttributesCacheGenerator_mImageTargetType,
	MarkerAbstractBehaviour_t66_CustomAttributesCacheGenerator_mMarkerID,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_VuforiaLicenseKey,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mCameraOffset,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mAutoAdjustStereoCameraSkewing,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mSceneScaleFactor,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_CameraDeviceModeSetting,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_MaxSimultaneousImageTargets,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_MaxSimultaneousObjectTargets,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_UseDelayedLoadingObjectTargets,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_CameraDirection,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_MirrorVideoBackground,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mWorldCenterMode,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mWorldCenter,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mPrimaryCamera,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_mSecondaryCamera,
	QCARAbstractBehaviour_t75_CustomAttributesCacheGenerator_QCARAbstractBehaviour_RegisterTrackerEventHandler_m4166,
	TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mWordListFile,
	TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mCustomWordListFile,
	TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mAdditionalCustomWords,
	TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mFilterMode,
	TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mFilterListFile,
	TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mAdditionalFilterWords,
	TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mWordPrefabCreationMode,
	TextRecoAbstractBehaviour_t83_CustomAttributesCacheGenerator_mMaximumWordInstances,
	VideoBackgroundAbstractBehaviour_t90_CustomAttributesCacheGenerator,
	VideoBackgroundAbstractBehaviour_t90_CustomAttributesCacheGenerator_U3CVideoBackGroundMirroredU3Ek__BackingField,
	VideoBackgroundAbstractBehaviour_t90_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_get_VideoBackGroundMirrored_m4246,
	VideoBackgroundAbstractBehaviour_t90_CustomAttributesCacheGenerator_VideoBackgroundAbstractBehaviour_set_VideoBackGroundMirrored_m4247,
	VirtualButtonAbstractBehaviour_t94_CustomAttributesCacheGenerator_mName,
	VirtualButtonAbstractBehaviour_t94_CustomAttributesCacheGenerator_mSensitivity,
	VirtualButtonAbstractBehaviour_t94_CustomAttributesCacheGenerator_mHasUpdatedPose,
	VirtualButtonAbstractBehaviour_t94_CustomAttributesCacheGenerator_mPrevTransform,
	VirtualButtonAbstractBehaviour_t94_CustomAttributesCacheGenerator_mPrevParent,
	WebCamAbstractBehaviour_t96_CustomAttributesCacheGenerator_mPlayModeRenderVideo,
	WebCamAbstractBehaviour_t96_CustomAttributesCacheGenerator_mDeviceNameSetInEditor,
	WebCamAbstractBehaviour_t96_CustomAttributesCacheGenerator_mFlipHorizontally,
	WebCamAbstractBehaviour_t96_CustomAttributesCacheGenerator_mTurnOffWebCam,
	WordAbstractBehaviour_t101_CustomAttributesCacheGenerator_mMode,
	WordAbstractBehaviour_t101_CustomAttributesCacheGenerator_mSpecificWord,
	U3CPrivateImplementationDetailsU3EU7B66DCC020U2DEBD6U2D4DBAU2DA757U2D272BEBA33044U7D_t765_CustomAttributesCacheGenerator,
};
