﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>
struct DOSetter_1_t1036;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m5541_gshared (DOSetter_1_t1036 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m5541(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1036 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m5541_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m15220_gshared (DOSetter_1_t1036 * __this, Quaternion_t22  ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m15220(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1036 *, Quaternion_t22 , const MethodInfo*))DOSetter_1_Invoke_m15220_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m15221_gshared (DOSetter_1_t1036 * __this, Quaternion_t22  ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m15221(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1036 *, Quaternion_t22 , AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m15221_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<UnityEngine.Quaternion>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m15222_gshared (DOSetter_1_t1036 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m15222(__this, ___result, method) (( void (*) (DOSetter_1_t1036 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m15222_gshared)(__this, ___result, method)
