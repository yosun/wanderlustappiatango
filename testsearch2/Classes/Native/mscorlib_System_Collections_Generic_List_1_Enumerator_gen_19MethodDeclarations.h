﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>
struct Enumerator_t896;
// System.Object
struct Object_t;
// Vuforia.ITextRecoEventHandler
struct ITextRecoEventHandler_t795;
// System.Collections.Generic.List`1<Vuforia.ITextRecoEventHandler>
struct List_1_t758;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m23171(__this, ___l, method) (( void (*) (Enumerator_t896 *, List_1_t758 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23172(__this, method) (( Object_t * (*) (Enumerator_t896 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::Dispose()
#define Enumerator_Dispose_m23173(__this, method) (( void (*) (Enumerator_t896 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::VerifyState()
#define Enumerator_VerifyState_m23174(__this, method) (( void (*) (Enumerator_t896 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4669(__this, method) (( bool (*) (Enumerator_t896 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.ITextRecoEventHandler>::get_Current()
#define Enumerator_get_Current_m4668(__this, method) (( Object_t * (*) (Enumerator_t896 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
