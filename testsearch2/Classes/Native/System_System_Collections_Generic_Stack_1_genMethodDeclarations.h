﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<DG.Tweening.Tween>
struct Stack_1_t985;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<DG.Tweening.Tween>
struct IEnumerator_1_t4307;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// DG.Tweening.Tween
struct Tween_t940;
// System.Collections.Generic.Stack`1/Enumerator<DG.Tweening.Tween>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_0.h"

// System.Void System.Collections.Generic.Stack`1<DG.Tweening.Tween>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_1MethodDeclarations.h"
#define Stack_1__ctor_m5559(__this, method) (( void (*) (Stack_1_t985 *, const MethodInfo*))Stack_1__ctor_m15687_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<DG.Tweening.Tween>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m23774(__this, method) (( bool (*) (Stack_1_t985 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m15688_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<DG.Tweening.Tween>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m23775(__this, method) (( Object_t * (*) (Stack_1_t985 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m15689_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<DG.Tweening.Tween>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m23776(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t985 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m15690_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<DG.Tweening.Tween>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m23777(__this, method) (( Object_t* (*) (Stack_1_t985 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15691_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<DG.Tweening.Tween>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m23778(__this, method) (( Object_t * (*) (Stack_1_t985 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m15692_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<DG.Tweening.Tween>::Peek()
#define Stack_1_Peek_m23779(__this, method) (( Tween_t940 * (*) (Stack_1_t985 *, const MethodInfo*))Stack_1_Peek_m15693_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<DG.Tweening.Tween>::Pop()
#define Stack_1_Pop_m5555(__this, method) (( Tween_t940 * (*) (Stack_1_t985 *, const MethodInfo*))Stack_1_Pop_m15694_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<DG.Tweening.Tween>::Push(T)
#define Stack_1_Push_m5556(__this, ___t, method) (( void (*) (Stack_1_t985 *, Tween_t940 *, const MethodInfo*))Stack_1_Push_m15695_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<DG.Tweening.Tween>::get_Count()
#define Stack_1_get_Count_m23780(__this, method) (( int32_t (*) (Stack_1_t985 *, const MethodInfo*))Stack_1_get_Count_m15696_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<DG.Tweening.Tween>::GetEnumerator()
#define Stack_1_GetEnumerator_m23781(__this, method) (( Enumerator_t3697  (*) (Stack_1_t985 *, const MethodInfo*))Stack_1_GetEnumerator_m15697_gshared)(__this, method)
