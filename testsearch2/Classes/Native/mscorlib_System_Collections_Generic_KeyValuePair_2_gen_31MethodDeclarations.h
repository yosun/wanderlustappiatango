﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct KeyValuePair_2_t3636;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t94;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"
#define KeyValuePair_2__ctor_m22853(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3636 *, int32_t, VirtualButtonAbstractBehaviour_t94 *, const MethodInfo*))KeyValuePair_2__ctor_m16496_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Key()
#define KeyValuePair_2_get_Key_m22854(__this, method) (( int32_t (*) (KeyValuePair_2_t3636 *, const MethodInfo*))KeyValuePair_2_get_Key_m16497_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m22855(__this, ___value, method) (( void (*) (KeyValuePair_2_t3636 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16498_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Value()
#define KeyValuePair_2_get_Value_m22856(__this, method) (( VirtualButtonAbstractBehaviour_t94 * (*) (KeyValuePair_2_t3636 *, const MethodInfo*))KeyValuePair_2_get_Value_m16499_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m22857(__this, ___value, method) (( void (*) (KeyValuePair_2_t3636 *, VirtualButtonAbstractBehaviour_t94 *, const MethodInfo*))KeyValuePair_2_set_Value_m16500_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::ToString()
#define KeyValuePair_2_ToString_m22858(__this, method) (( String_t* (*) (KeyValuePair_2_t3636 *, const MethodInfo*))KeyValuePair_2_ToString_m16501_gshared)(__this, method)
