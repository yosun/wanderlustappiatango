﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<Vuforia.QCARUnity/InitError>
struct Action_1_t136;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::.ctor(System.Object,System.IntPtr)
// System.Action`1<System.Int32>
#include "mscorlib_System_Action_1_gen_9MethodDeclarations.h"
#define Action_1__ctor_m425(__this, ___object, ___method, method) (( void (*) (Action_1_t136 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m15241_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::Invoke(T)
#define Action_1_Invoke_m15242(__this, ___obj, method) (( void (*) (Action_1_t136 *, int32_t, const MethodInfo*))Action_1_Invoke_m15243_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<Vuforia.QCARUnity/InitError>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m15244(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t136 *, int32_t, AsyncCallback_t312 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m15245_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<Vuforia.QCARUnity/InitError>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m15246(__this, ___result, method) (( void (*) (Action_1_t136 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m15247_gshared)(__this, ___result, method)
