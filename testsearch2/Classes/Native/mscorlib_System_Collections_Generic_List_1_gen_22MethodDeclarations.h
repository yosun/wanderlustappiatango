﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t615;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerable`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerable_1_t4169;
// System.Collections.Generic.IEnumerator`1<Vuforia.Image/PIXEL_FORMAT>
struct IEnumerator_1_t4166;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ICollection_1_t4161;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Image/PIXEL_FORMAT>
struct ReadOnlyCollection_1_t3411;
// Vuforia.Image/PIXEL_FORMAT[]
struct PIXEL_FORMATU5BU5D_t3395;
// System.Predicate`1<Vuforia.Image/PIXEL_FORMAT>
struct Predicate_1_t3412;
// System.Comparison`1<Vuforia.Image/PIXEL_FORMAT>
struct Comparison_1_t3414;
// Vuforia.Image/PIXEL_FORMAT
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT.h"
// System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_45.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor()
// System.Collections.Generic.List`1<System.Int32>
#include "mscorlib_System_Collections_Generic_List_1_gen_27MethodDeclarations.h"
#define List_1__ctor_m4375(__this, method) (( void (*) (List_1_t615 *, const MethodInfo*))List_1__ctor_m4498_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m18986(__this, ___collection, method) (( void (*) (List_1_t615 *, Object_t*, const MethodInfo*))List_1__ctor_m4439_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Int32)
#define List_1__ctor_m18987(__this, ___capacity, method) (( void (*) (List_1_t615 *, int32_t, const MethodInfo*))List_1__ctor_m18988_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::.cctor()
#define List_1__cctor_m18989(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m18990_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18991(__this, method) (( Object_t* (*) (List_1_t615 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18992_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m18993(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t615 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m18994_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m18995(__this, method) (( Object_t * (*) (List_1_t615 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m18996_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m18997(__this, ___item, method) (( int32_t (*) (List_1_t615 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m18998_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m18999(__this, ___item, method) (( bool (*) (List_1_t615 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m19000_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19001(__this, ___item, method) (( int32_t (*) (List_1_t615 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m19002_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19003(__this, ___index, ___item, method) (( void (*) (List_1_t615 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m19004_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19005(__this, ___item, method) (( void (*) (List_1_t615 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m19006_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19007(__this, method) (( bool (*) (List_1_t615 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19008_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19009(__this, method) (( bool (*) (List_1_t615 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m19010_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19011(__this, method) (( Object_t * (*) (List_1_t615 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m19012_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19013(__this, method) (( bool (*) (List_1_t615 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m19014_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19015(__this, method) (( bool (*) (List_1_t615 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m19016_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19017(__this, ___index, method) (( Object_t * (*) (List_1_t615 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m19018_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19019(__this, ___index, ___value, method) (( void (*) (List_1_t615 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m19020_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Add(T)
#define List_1_Add_m19021(__this, ___item, method) (( void (*) (List_1_t615 *, int32_t, const MethodInfo*))List_1_Add_m19022_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19023(__this, ___newCount, method) (( void (*) (List_1_t615 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m19024_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19025(__this, ___collection, method) (( void (*) (List_1_t615 *, Object_t*, const MethodInfo*))List_1_AddCollection_m19026_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19027(__this, ___enumerable, method) (( void (*) (List_1_t615 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m19028_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19029(__this, ___collection, method) (( void (*) (List_1_t615 *, Object_t*, const MethodInfo*))List_1_AddRange_m19030_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::AsReadOnly()
#define List_1_AsReadOnly_m19031(__this, method) (( ReadOnlyCollection_1_t3411 * (*) (List_1_t615 *, const MethodInfo*))List_1_AsReadOnly_m19032_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Clear()
#define List_1_Clear_m19033(__this, method) (( void (*) (List_1_t615 *, const MethodInfo*))List_1_Clear_m19034_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Contains(T)
#define List_1_Contains_m19035(__this, ___item, method) (( bool (*) (List_1_t615 *, int32_t, const MethodInfo*))List_1_Contains_m19036_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19037(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t615 *, PIXEL_FORMATU5BU5D_t3395*, int32_t, const MethodInfo*))List_1_CopyTo_m19038_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Find(System.Predicate`1<T>)
#define List_1_Find_m19039(__this, ___match, method) (( int32_t (*) (List_1_t615 *, Predicate_1_t3412 *, const MethodInfo*))List_1_Find_m19040_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19041(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3412 *, const MethodInfo*))List_1_CheckMatch_m19042_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19043(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t615 *, int32_t, int32_t, Predicate_1_t3412 *, const MethodInfo*))List_1_GetIndex_m19044_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::GetEnumerator()
#define List_1_GetEnumerator_m19045(__this, method) (( Enumerator_t3413  (*) (List_1_t615 *, const MethodInfo*))List_1_GetEnumerator_m4440_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::IndexOf(T)
#define List_1_IndexOf_m19046(__this, ___item, method) (( int32_t (*) (List_1_t615 *, int32_t, const MethodInfo*))List_1_IndexOf_m19047_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19048(__this, ___start, ___delta, method) (( void (*) (List_1_t615 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m19049_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19050(__this, ___index, method) (( void (*) (List_1_t615 *, int32_t, const MethodInfo*))List_1_CheckIndex_m19051_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Insert(System.Int32,T)
#define List_1_Insert_m19052(__this, ___index, ___item, method) (( void (*) (List_1_t615 *, int32_t, int32_t, const MethodInfo*))List_1_Insert_m19053_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19054(__this, ___collection, method) (( void (*) (List_1_t615 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m19055_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Remove(T)
#define List_1_Remove_m19056(__this, ___item, method) (( bool (*) (List_1_t615 *, int32_t, const MethodInfo*))List_1_Remove_m19057_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19058(__this, ___match, method) (( int32_t (*) (List_1_t615 *, Predicate_1_t3412 *, const MethodInfo*))List_1_RemoveAll_m19059_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19060(__this, ___index, method) (( void (*) (List_1_t615 *, int32_t, const MethodInfo*))List_1_RemoveAt_m19061_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Reverse()
#define List_1_Reverse_m19062(__this, method) (( void (*) (List_1_t615 *, const MethodInfo*))List_1_Reverse_m19063_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort()
#define List_1_Sort_m19064(__this, method) (( void (*) (List_1_t615 *, const MethodInfo*))List_1_Sort_m19065_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19066(__this, ___comparison, method) (( void (*) (List_1_t615 *, Comparison_1_t3414 *, const MethodInfo*))List_1_Sort_m19067_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::ToArray()
#define List_1_ToArray_m19068(__this, method) (( PIXEL_FORMATU5BU5D_t3395* (*) (List_1_t615 *, const MethodInfo*))List_1_ToArray_m19069_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::TrimExcess()
#define List_1_TrimExcess_m19070(__this, method) (( void (*) (List_1_t615 *, const MethodInfo*))List_1_TrimExcess_m19071_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Capacity()
#define List_1_get_Capacity_m19072(__this, method) (( int32_t (*) (List_1_t615 *, const MethodInfo*))List_1_get_Capacity_m19073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19074(__this, ___value, method) (( void (*) (List_1_t615 *, int32_t, const MethodInfo*))List_1_set_Capacity_m19075_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Count()
#define List_1_get_Count_m19076(__this, method) (( int32_t (*) (List_1_t615 *, const MethodInfo*))List_1_get_Count_m19077_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::get_Item(System.Int32)
#define List_1_get_Item_m19078(__this, ___index, method) (( int32_t (*) (List_1_t615 *, int32_t, const MethodInfo*))List_1_get_Item_m19079_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>::set_Item(System.Int32,T)
#define List_1_set_Item_m19080(__this, ___index, ___value, method) (( void (*) (List_1_t615 *, int32_t, int32_t, const MethodInfo*))List_1_set_Item_m19081_gshared)(__this, ___index, ___value, method)
