﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct InternalEnumerator_1_t3185;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.EventSystems.RaycastResult
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15864_gshared (InternalEnumerator_1_t3185 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15864(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3185 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15864_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15865_gshared (InternalEnumerator_1_t3185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15865(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3185 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15865_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15866_gshared (InternalEnumerator_1_t3185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15866(__this, method) (( void (*) (InternalEnumerator_1_t3185 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15867_gshared (InternalEnumerator_1_t3185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15867(__this, method) (( bool (*) (InternalEnumerator_1_t3185 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15867_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t238  InternalEnumerator_1_get_Current_m15868_gshared (InternalEnumerator_1_t3185 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15868(__this, method) (( RaycastResult_t238  (*) (InternalEnumerator_1_t3185 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15868_gshared)(__this, method)
