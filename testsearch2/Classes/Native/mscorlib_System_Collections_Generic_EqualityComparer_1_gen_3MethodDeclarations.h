﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<System.UInt16>
struct EqualityComparer_1_t3486;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<System.UInt16>::.ctor()
extern "C" void EqualityComparer_1__ctor_m20331_gshared (EqualityComparer_1_t3486 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m20331(__this, method) (( void (*) (EqualityComparer_1_t3486 *, const MethodInfo*))EqualityComparer_1__ctor_m20331_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.UInt16>::.cctor()
extern "C" void EqualityComparer_1__cctor_m20332_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m20332(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m20332_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt16>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20333_gshared (EqualityComparer_1_t3486 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20333(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t3486 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m20333_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt16>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20334_gshared (EqualityComparer_1_t3486 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20334(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t3486 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m20334_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.UInt16>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.UInt16>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt16>::get_Default()
extern "C" EqualityComparer_1_t3486 * EqualityComparer_1_get_Default_m20335_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m20335(__this /* static, unused */, method) (( EqualityComparer_1_t3486 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m20335_gshared)(__this /* static, unused */, method)
