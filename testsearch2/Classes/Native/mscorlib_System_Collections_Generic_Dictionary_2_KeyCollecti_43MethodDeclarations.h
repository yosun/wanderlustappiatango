﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>
struct Enumerator_t3794;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t3789;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m25404_gshared (Enumerator_t3794 * __this, Dictionary_2_t3789 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m25404(__this, ___host, method) (( void (*) (Enumerator_t3794 *, Dictionary_2_t3789 *, const MethodInfo*))Enumerator__ctor_m25404_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m25405_gshared (Enumerator_t3794 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m25405(__this, method) (( Object_t * (*) (Enumerator_t3794 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m25405_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::Dispose()
extern "C" void Enumerator_Dispose_m25406_gshared (Enumerator_t3794 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m25406(__this, method) (( void (*) (Enumerator_t3794 *, const MethodInfo*))Enumerator_Dispose_m25406_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool Enumerator_MoveNext_m25407_gshared (Enumerator_t3794 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m25407(__this, method) (( bool (*) (Enumerator_t3794 *, const MethodInfo*))Enumerator_MoveNext_m25407_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Int64>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m25408_gshared (Enumerator_t3794 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m25408(__this, method) (( Object_t * (*) (Enumerator_t3794 *, const MethodInfo*))Enumerator_get_Current_m25408_gshared)(__this, method)
