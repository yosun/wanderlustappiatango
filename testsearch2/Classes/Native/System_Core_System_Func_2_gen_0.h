﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.ILayoutElement
struct ILayoutElement_t425;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct  Func_2_t382  : public MulticastDelegate_t314
{
};
