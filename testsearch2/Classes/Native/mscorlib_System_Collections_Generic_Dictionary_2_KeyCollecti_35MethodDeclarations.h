﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Enumerator_t3621;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,Vuforia.WebCamProfile/ProfileData>
struct Dictionary_2_t3616;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m22685_gshared (Enumerator_t3621 * __this, Dictionary_2_t3616 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m22685(__this, ___host, method) (( void (*) (Enumerator_t3621 *, Dictionary_2_t3616 *, const MethodInfo*))Enumerator__ctor_m22685_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22686_gshared (Enumerator_t3621 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22686(__this, method) (( Object_t * (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22686_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::Dispose()
extern "C" void Enumerator_Dispose_m22687_gshared (Enumerator_t3621 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22687(__this, method) (( void (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_Dispose_m22687_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22688_gshared (Enumerator_t3621 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22688(__this, method) (( bool (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_MoveNext_m22688_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m22689_gshared (Enumerator_t3621 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22689(__this, method) (( Object_t * (*) (Enumerator_t3621 *, const MethodInfo*))Enumerator_get_Current_m22689_gshared)(__this, method)
