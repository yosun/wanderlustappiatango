﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct Dictionary_2_t3852;
// System.Collections.Generic.ICollection`1<System.Type>
struct ICollection_1_t4159;
// System.Collections.Generic.ICollection`1<SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ICollection_1_t4385;
// System.Object
struct Object_t;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t1292;
// System.Type
struct Type_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct KeyCollection_t4388;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct ValueCollection_t4389;
// System.Collections.Generic.IEqualityComparer`1<System.Type>
struct IEqualityComparer_1_t3471;
// System.Collections.Generic.IDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
struct IDictionary_2_t1287;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>[]
struct KeyValuePair_2U5BU5D_t4386;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>>
struct IEnumerator_1_t4387;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_40.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__44.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_33MethodDeclarations.h"
#define Dictionary_2__ctor_m26138(__this, method) (( void (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2__ctor_m14896_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m26139(__this, ___comparer, method) (( void (*) (Dictionary_2_t3852 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14898_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m26140(__this, ___dictionary, method) (( void (*) (Dictionary_2_t3852 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14900_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Int32)
#define Dictionary_2__ctor_m26141(__this, ___capacity, method) (( void (*) (Dictionary_2_t3852 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m14902_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m26142(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t3852 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14904_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m26143(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3852 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m14906_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26144(__this, method) (( Object_t* (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m14908_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26145(__this, method) (( Object_t* (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m14910_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m26146(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t3852 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m14912_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m26147(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3852 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m14914_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m26148(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3852 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m14916_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m26149(__this, ___key, method) (( bool (*) (Dictionary_2_t3852 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m14918_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m26150(__this, ___key, method) (( void (*) (Dictionary_2_t3852 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m14920_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26151(__this, method) (( bool (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14922_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26152(__this, method) (( Object_t * (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14924_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26153(__this, method) (( bool (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14926_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26154(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t3852 *, KeyValuePair_2_t3856 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14928_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26155(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3852 *, KeyValuePair_2_t3856 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14930_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26156(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3852 *, KeyValuePair_2U5BU5D_t4386*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14932_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26157(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t3852 *, KeyValuePair_2_t3856 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14934_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m26158(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3852 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m14936_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26159(__this, method) (( Object_t * (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14938_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26160(__this, method) (( Object_t* (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14940_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26161(__this, method) (( Object_t * (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14942_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Count()
#define Dictionary_2_get_Count_m26162(__this, method) (( int32_t (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_get_Count_m14944_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Item(TKey)
#define Dictionary_2_get_Item_m26163(__this, ___key, method) (( ConstructorDelegate_t1292 * (*) (Dictionary_2_t3852 *, Type_t *, const MethodInfo*))Dictionary_2_get_Item_m14946_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m26164(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3852 *, Type_t *, ConstructorDelegate_t1292 *, const MethodInfo*))Dictionary_2_set_Item_m14948_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m26165(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t3852 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m14950_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m26166(__this, ___size, method) (( void (*) (Dictionary_2_t3852 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m14952_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m26167(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3852 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m14954_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m26168(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3856  (*) (Object_t * /* static, unused */, Type_t *, ConstructorDelegate_t1292 *, const MethodInfo*))Dictionary_2_make_pair_m14956_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m26169(__this /* static, unused */, ___key, ___value, method) (( Type_t * (*) (Object_t * /* static, unused */, Type_t *, ConstructorDelegate_t1292 *, const MethodInfo*))Dictionary_2_pick_key_m14958_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m26170(__this /* static, unused */, ___key, ___value, method) (( ConstructorDelegate_t1292 * (*) (Object_t * /* static, unused */, Type_t *, ConstructorDelegate_t1292 *, const MethodInfo*))Dictionary_2_pick_value_m14960_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m26171(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t3852 *, KeyValuePair_2U5BU5D_t4386*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m14962_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Resize()
#define Dictionary_2_Resize_m26172(__this, method) (( void (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_Resize_m14964_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(TKey,TValue)
#define Dictionary_2_Add_m26173(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t3852 *, Type_t *, ConstructorDelegate_t1292 *, const MethodInfo*))Dictionary_2_Add_m14966_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Clear()
#define Dictionary_2_Clear_m26174(__this, method) (( void (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_Clear_m14968_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m26175(__this, ___key, method) (( bool (*) (Dictionary_2_t3852 *, Type_t *, const MethodInfo*))Dictionary_2_ContainsKey_m14970_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m26176(__this, ___value, method) (( bool (*) (Dictionary_2_t3852 *, ConstructorDelegate_t1292 *, const MethodInfo*))Dictionary_2_ContainsValue_m14972_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m26177(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t3852 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m14974_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m26178(__this, ___sender, method) (( void (*) (Dictionary_2_t3852 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m14976_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(TKey)
#define Dictionary_2_Remove_m26179(__this, ___key, method) (( bool (*) (Dictionary_2_t3852 *, Type_t *, const MethodInfo*))Dictionary_2_Remove_m14978_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m26180(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t3852 *, Type_t *, ConstructorDelegate_t1292 **, const MethodInfo*))Dictionary_2_TryGetValue_m14980_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Keys()
#define Dictionary_2_get_Keys_m26181(__this, method) (( KeyCollection_t4388 * (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_get_Keys_m14982_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Values()
#define Dictionary_2_get_Values_m26182(__this, method) (( ValueCollection_t4389 * (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_get_Values_m14984_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m26183(__this, ___key, method) (( Type_t * (*) (Dictionary_2_t3852 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m14986_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m26184(__this, ___value, method) (( ConstructorDelegate_t1292 * (*) (Dictionary_2_t3852 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m14988_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m26185(__this, ___pair, method) (( bool (*) (Dictionary_2_t3852 *, KeyValuePair_2_t3856 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m14990_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m26186(__this, method) (( Enumerator_t4390  (*) (Dictionary_2_t3852 *, const MethodInfo*))Dictionary_2_GetEnumerator_m14992_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m26187(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, Type_t *, ConstructorDelegate_t1292 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m14994_gshared)(__this /* static, unused */, ___key, ___value, method)
