﻿#pragma once
#include <stdint.h>
// Vuforia.IUserDefinedTargetEventHandler[]
struct IUserDefinedTargetEventHandlerU5BU5D_t3656;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>
struct  List_1_t760  : public Object_t
{
	// T[] System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::_items
	IUserDefinedTargetEventHandlerU5BU5D_t3656* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::_version
	int32_t ____version_3;
};
struct List_1_t760_StaticFields{
	// T[] System.Collections.Generic.List`1<Vuforia.IUserDefinedTargetEventHandler>::EmptyArray
	IUserDefinedTargetEventHandlerU5BU5D_t3656* ___EmptyArray_4;
};
