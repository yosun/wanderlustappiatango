﻿#pragma once
#include <stdint.h>
// UnityEngine.GameObject
struct GameObject_t2;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Predicate`1<UnityEngine.GameObject>
struct  Predicate_1_t3217  : public MulticastDelegate_t314
{
};
