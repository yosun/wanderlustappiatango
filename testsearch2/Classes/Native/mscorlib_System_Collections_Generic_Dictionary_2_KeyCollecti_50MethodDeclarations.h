﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Enumerator_t3876;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t3874;

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m26558_gshared (Enumerator_t3876 * __this, Dictionary_2_t3874 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m26558(__this, ___host, method) (( void (*) (Enumerator_t3876 *, Dictionary_2_t3874 *, const MethodInfo*))Enumerator__ctor_m26558_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m26559_gshared (Enumerator_t3876 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m26559(__this, method) (( Object_t * (*) (Enumerator_t3876 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m26559_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void Enumerator_Dispose_m26560_gshared (Enumerator_t3876 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m26560(__this, method) (( void (*) (Enumerator_t3876 *, const MethodInfo*))Enumerator_Dispose_m26560_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool Enumerator_MoveNext_m26561_gshared (Enumerator_t3876 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m26561(__this, method) (( bool (*) (Enumerator_t3876 *, const MethodInfo*))Enumerator_MoveNext_m26561_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m26562_gshared (Enumerator_t3876 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m26562(__this, method) (( Object_t * (*) (Enumerator_t3876 *, const MethodInfo*))Enumerator_get_Current_m26562_gshared)(__this, method)
