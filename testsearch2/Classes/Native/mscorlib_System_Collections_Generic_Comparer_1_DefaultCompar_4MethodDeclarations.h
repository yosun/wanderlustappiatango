﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Comparer`1/DefaultComparer<System.UInt16>
struct DefaultComparer_t3708;

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.UInt16>::.ctor()
extern "C" void DefaultComparer__ctor_m24031_gshared (DefaultComparer_t3708 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m24031(__this, method) (( void (*) (DefaultComparer_t3708 *, const MethodInfo*))DefaultComparer__ctor_m24031_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.UInt16>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m24032_gshared (DefaultComparer_t3708 * __this, uint16_t ___x, uint16_t ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m24032(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t3708 *, uint16_t, uint16_t, const MethodInfo*))DefaultComparer_Compare_m24032_gshared)(__this, ___x, ___y, method)
