﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t952;
// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>
struct List_1_t953;
// DG.Tweening.Tween
#include "DOTween_DG_Tweening_Tween.h"
// DG.Tweening.Sequence
struct  Sequence_t131  : public Tween_t940
{
	// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.Sequence::sequencedTweens
	List_1_t952 * ___sequencedTweens_51;
	// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable> DG.Tweening.Sequence::_sequencedObjs
	List_1_t953 * ____sequencedObjs_52;
	// System.Single DG.Tweening.Sequence::lastTweenInsertTime
	float ___lastTweenInsertTime_53;
};
