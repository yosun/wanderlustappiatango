﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t1008;
struct VectorOptions_t1008_marshaled;

void VectorOptions_t1008_marshal(const VectorOptions_t1008& unmarshaled, VectorOptions_t1008_marshaled& marshaled);
void VectorOptions_t1008_marshal_back(const VectorOptions_t1008_marshaled& marshaled, VectorOptions_t1008& unmarshaled);
void VectorOptions_t1008_marshal_cleanup(VectorOptions_t1008_marshaled& marshaled);
