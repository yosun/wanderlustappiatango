﻿#pragma once
#include <stdint.h>
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t3299;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.UI.Graphic>
struct  List_1_t287  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::_items
	GraphicU5BU5D_t3299* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::_version
	int32_t ____version_3;
};
struct List_1_t287_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.UI.Graphic>::EmptyArray
	GraphicU5BU5D_t3299* ___EmptyArray_4;
};
