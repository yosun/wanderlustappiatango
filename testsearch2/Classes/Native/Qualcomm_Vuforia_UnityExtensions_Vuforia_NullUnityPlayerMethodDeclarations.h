﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.NullUnityPlayer
struct NullUnityPlayer_t154;
// System.String
struct String_t;
// Vuforia.QCARUnity/InitError
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARUnity_InitError.h"

// System.Void Vuforia.NullUnityPlayer::LoadNativeLibraries()
extern "C" void NullUnityPlayer_LoadNativeLibraries_m2747 (NullUnityPlayer_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::InitializePlatform()
extern "C" void NullUnityPlayer_InitializePlatform_m2748 (NullUnityPlayer_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.QCARUnity/InitError Vuforia.NullUnityPlayer::Start(System.String)
extern "C" int32_t NullUnityPlayer_Start_m2749 (NullUnityPlayer_t154 * __this, String_t* ___licenseKey, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::Update()
extern "C" void NullUnityPlayer_Update_m2750 (NullUnityPlayer_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::Dispose()
extern "C" void NullUnityPlayer_Dispose_m2751 (NullUnityPlayer_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnPause()
extern "C" void NullUnityPlayer_OnPause_m2752 (NullUnityPlayer_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnResume()
extern "C" void NullUnityPlayer_OnResume_m2753 (NullUnityPlayer_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::OnDestroy()
extern "C" void NullUnityPlayer_OnDestroy_m2754 (NullUnityPlayer_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.NullUnityPlayer::.ctor()
extern "C" void NullUnityPlayer__ctor_m485 (NullUnityPlayer_t154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
