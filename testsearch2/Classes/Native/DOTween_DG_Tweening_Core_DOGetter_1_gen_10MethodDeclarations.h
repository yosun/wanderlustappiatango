﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOGetter`1<UnityEngine.Color>
struct DOGetter_1_t1056;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C" void DOGetter_1__ctor_m24101_gshared (DOGetter_1_t1056 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOGetter_1__ctor_m24101(__this, ___object, ___method, method) (( void (*) (DOGetter_1_t1056 *, Object_t *, IntPtr_t, const MethodInfo*))DOGetter_1__ctor_m24101_gshared)(__this, ___object, ___method, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::Invoke()
extern "C" Color_t98  DOGetter_1_Invoke_m24102_gshared (DOGetter_1_t1056 * __this, const MethodInfo* method);
#define DOGetter_1_Invoke_m24102(__this, method) (( Color_t98  (*) (DOGetter_1_t1056 *, const MethodInfo*))DOGetter_1_Invoke_m24102_gshared)(__this, method)
// System.IAsyncResult DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * DOGetter_1_BeginInvoke_m24103_gshared (DOGetter_1_t1056 * __this, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOGetter_1_BeginInvoke_m24103(__this, ___callback, ___object, method) (( Object_t * (*) (DOGetter_1_t1056 *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOGetter_1_BeginInvoke_m24103_gshared)(__this, ___callback, ___object, method)
// T DG.Tweening.Core.DOGetter`1<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C" Color_t98  DOGetter_1_EndInvoke_m24104_gshared (DOGetter_1_t1056 * __this, Object_t * ___result, const MethodInfo* method);
#define DOGetter_1_EndInvoke_m24104(__this, ___result, method) (( Color_t98  (*) (DOGetter_1_t1056 *, Object_t *, const MethodInfo*))DOGetter_1_EndInvoke_m24104_gshared)(__this, ___result, method)
