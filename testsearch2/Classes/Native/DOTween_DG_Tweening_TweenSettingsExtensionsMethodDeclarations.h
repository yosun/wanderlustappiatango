﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.TweenSettingsExtensions
struct TweenSettingsExtensions_t108;
// DG.Tweening.Sequence
struct Sequence_t131;
// DG.Tweening.Tween
struct Tween_t940;
// DG.Tweening.Tweener
struct Tweener_t107;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t125;
// DG.Tweening.AxisConstraint
#include "DOTween_DG_Tweening_AxisConstraint.h"

// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Append(DG.Tweening.Sequence,DG.Tweening.Tween)
extern "C" Sequence_t131 * TweenSettingsExtensions_Append_m395 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, Tween_t940 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Join(DG.Tweening.Sequence,DG.Tweening.Tween)
extern "C" Sequence_t131 * TweenSettingsExtensions_Join_m396 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, Tween_t940 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Sequence DG.Tweening.TweenSettingsExtensions::Insert(DG.Tweening.Sequence,System.Single,DG.Tweening.Tween)
extern "C" Sequence_t131 * TweenSettingsExtensions_Insert_m400 (Object_t * __this /* static, unused */, Sequence_t131 * ___s, float ___atPosition, Tween_t940 * ___t, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,System.Boolean)
extern "C" Tweener_t107 * TweenSettingsExtensions_SetOptions_m5376 (Object_t * __this /* static, unused */, TweenerCore_3_t125 * ___t, bool ___snapping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>,DG.Tweening.AxisConstraint,System.Boolean)
extern "C" Tweener_t107 * TweenSettingsExtensions_SetOptions_m5377 (Object_t * __this /* static, unused */, TweenerCore_3_t125 * ___t, int32_t ___axisConstraint, bool ___snapping, const MethodInfo* method) IL2CPP_METHOD_ATTR;
