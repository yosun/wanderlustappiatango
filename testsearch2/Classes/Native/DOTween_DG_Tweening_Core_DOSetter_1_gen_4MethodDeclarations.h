﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>
struct DOSetter_1_t1039;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2.h"

// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::.ctor(System.Object,System.IntPtr)
extern "C" void DOSetter_1__ctor_m23770_gshared (DOSetter_1_t1039 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define DOSetter_1__ctor_m23770(__this, ___object, ___method, method) (( void (*) (DOSetter_1_t1039 *, Object_t *, IntPtr_t, const MethodInfo*))DOSetter_1__ctor_m23770_gshared)(__this, ___object, ___method, method)
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::Invoke(T)
extern "C" void DOSetter_1_Invoke_m23771_gshared (DOSetter_1_t1039 * __this, Color2_t1006  ___pNewValue, const MethodInfo* method);
#define DOSetter_1_Invoke_m23771(__this, ___pNewValue, method) (( void (*) (DOSetter_1_t1039 *, Color2_t1006 , const MethodInfo*))DOSetter_1_Invoke_m23771_gshared)(__this, ___pNewValue, method)
// System.IAsyncResult DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * DOSetter_1_BeginInvoke_m23772_gshared (DOSetter_1_t1039 * __this, Color2_t1006  ___pNewValue, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define DOSetter_1_BeginInvoke_m23772(__this, ___pNewValue, ___callback, ___object, method) (( Object_t * (*) (DOSetter_1_t1039 *, Color2_t1006 , AsyncCallback_t312 *, Object_t *, const MethodInfo*))DOSetter_1_BeginInvoke_m23772_gshared)(__this, ___pNewValue, ___callback, ___object, method)
// System.Void DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>::EndInvoke(System.IAsyncResult)
extern "C" void DOSetter_1_EndInvoke_m23773_gshared (DOSetter_1_t1039 * __this, Object_t * ___result, const MethodInfo* method);
#define DOSetter_1_EndInvoke_m23773(__this, ___result, method) (( void (*) (DOSetter_1_t1039 *, Object_t *, const MethodInfo*))DOSetter_1_EndInvoke_m23773_gshared)(__this, ___result, method)
