﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct Stack_1_t3366;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<UnityEngine.Component>>
struct IEnumerator_1_t4149;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t424;
// System.Collections.Generic.Stack`1/Enumerator<System.Collections.Generic.List`1<UnityEngine.Component>>
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen_5.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor()
// System.Collections.Generic.Stack`1<System.Object>
#include "System_System_Collections_Generic_Stack_1_gen_1MethodDeclarations.h"
#define Stack_1__ctor_m18407(__this, method) (( void (*) (Stack_1_t3366 *, const MethodInfo*))Stack_1__ctor_m15687_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m18408(__this, method) (( bool (*) (Stack_1_t3366 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m15688_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m18409(__this, method) (( Object_t * (*) (Stack_1_t3366 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m15689_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m18410(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t3366 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m15690_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m18411(__this, method) (( Object_t* (*) (Stack_1_t3366 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15691_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m18412(__this, method) (( Object_t * (*) (Stack_1_t3366 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m15692_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Peek()
#define Stack_1_Peek_m18413(__this, method) (( List_1_t424 * (*) (Stack_1_t3366 *, const MethodInfo*))Stack_1_Peek_m15693_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Pop()
#define Stack_1_Pop_m18414(__this, method) (( List_1_t424 * (*) (Stack_1_t3366 *, const MethodInfo*))Stack_1_Pop_m15694_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Push(T)
#define Stack_1_Push_m18415(__this, ___t, method) (( void (*) (Stack_1_t3366 *, List_1_t424 *, const MethodInfo*))Stack_1_Push_m15695_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_Count()
#define Stack_1_get_Count_m18416(__this, method) (( int32_t (*) (Stack_1_t3366 *, const MethodInfo*))Stack_1_get_Count_m15696_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Component>>::GetEnumerator()
#define Stack_1_GetEnumerator_m18417(__this, method) (( Enumerator_t4150  (*) (Stack_1_t3366 *, const MethodInfo*))Stack_1_GetEnumerator_m15697_gshared)(__this, method)
