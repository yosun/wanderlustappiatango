﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>
struct Enumerator_t3598;
// System.Object
struct Object_t;
// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t729;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m22356_gshared (Enumerator_t3598 * __this, List_1_t729 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m22356(__this, ___l, method) (( void (*) (Enumerator_t3598 *, List_1_t729 *, const MethodInfo*))Enumerator__ctor_m22356_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m22357_gshared (Enumerator_t3598 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m22357(__this, method) (( Object_t * (*) (Enumerator_t3598 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m22357_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C" void Enumerator_Dispose_m22358_gshared (Enumerator_t3598 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m22358(__this, method) (( void (*) (Enumerator_t3598 *, const MethodInfo*))Enumerator_Dispose_m22358_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::VerifyState()
extern "C" void Enumerator_VerifyState_m22359_gshared (Enumerator_t3598 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m22359(__this, method) (( void (*) (Enumerator_t3598 *, const MethodInfo*))Enumerator_VerifyState_m22359_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m22360_gshared (Enumerator_t3598 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m22360(__this, method) (( bool (*) (Enumerator_t3598 *, const MethodInfo*))Enumerator_MoveNext_m22360_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern "C" TargetSearchResult_t726  Enumerator_get_Current_m22361_gshared (Enumerator_t3598 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m22361(__this, method) (( TargetSearchResult_t726  (*) (Enumerator_t3598 *, const MethodInfo*))Enumerator_get_Current_m22361_gshared)(__this, method)
