﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>
struct Enumerator_t856;
// System.Object
struct Object_t;
// Vuforia.PropAbstractBehaviour
struct PropAbstractBehaviour_t73;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.PropAbstractBehaviour>
struct Dictionary_2_t717;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_31MethodDeclarations.h"
#define Enumerator__ctor_m21613(__this, ___host, method) (( void (*) (Enumerator_t856 *, Dictionary_2_t717 *, const MethodInfo*))Enumerator__ctor_m16551_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m21614(__this, method) (( Object_t * (*) (Enumerator_t856 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16552_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m21615(__this, method) (( void (*) (Enumerator_t856 *, const MethodInfo*))Enumerator_Dispose_m16553_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m4542(__this, method) (( bool (*) (Enumerator_t856 *, const MethodInfo*))Enumerator_MoveNext_m16554_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.PropAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m4541(__this, method) (( PropAbstractBehaviour_t73 * (*) (Enumerator_t856 *, const MethodInfo*))Enumerator_get_Current_m16555_gshared)(__this, method)
