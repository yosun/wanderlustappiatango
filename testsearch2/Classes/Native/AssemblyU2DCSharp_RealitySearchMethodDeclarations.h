﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// RealitySearch
struct RealitySearch_t13;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t2;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"
// UnityEngine.Quaternion
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void RealitySearch::.ctor()
extern "C" void RealitySearch__ctor_m10 (RealitySearch_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RealitySearch::BuildFromLocalDB()
extern "C" void RealitySearch_BuildFromLocalDB_m11 (RealitySearch_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RealitySearch::SearchFromInputField()
extern "C" void RealitySearch_SearchFromInputField_m12 (RealitySearch_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RealitySearch::Search(System.String)
extern "C" void RealitySearch_Search_m13 (RealitySearch_t13 * __this, String_t* ___tag, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RealitySearch::CreatePathFromCurrentTo(UnityEngine.GameObject)
extern "C" void RealitySearch_CreatePathFromCurrentTo_m14 (RealitySearch_t13 * __this, GameObject_t2 * ___g, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject RealitySearch::InstantiateFromPool(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" GameObject_t2 * RealitySearch_InstantiateFromPool_m15 (RealitySearch_t13 * __this, GameObject_t2 * ___g, Vector3_t14  ___pos, Quaternion_t22  ___rot, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RealitySearch::CreatePool(System.Int32)
extern "C" void RealitySearch_CreatePool_m16 (RealitySearch_t13 * __this, int32_t ___num, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RealitySearch::Awake()
extern "C" void RealitySearch_Awake_m17 (RealitySearch_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RealitySearch::Update()
extern "C" void RealitySearch_Update_m18 (RealitySearch_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
