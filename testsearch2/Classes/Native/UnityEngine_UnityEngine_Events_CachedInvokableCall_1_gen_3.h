﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t124;
// UnityEngine.Events.InvokableCall`1<System.Object>
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_gen.h"
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct  CachedInvokableCall_1_t3914  : public InvokableCall_1_t3208
{
	// System.Object[] UnityEngine.Events.CachedInvokableCall`1<System.Object>::m_Arg1
	ObjectU5BU5D_t124* ___m_Arg1_1;
};
