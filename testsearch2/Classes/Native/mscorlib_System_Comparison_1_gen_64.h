﻿#pragma once
#include <stdint.h>
// System.Security.Policy.StrongName
struct StrongName_t2444;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<System.Security.Policy.StrongName>
struct  Comparison_1_t4019  : public MulticastDelegate_t314
{
};
