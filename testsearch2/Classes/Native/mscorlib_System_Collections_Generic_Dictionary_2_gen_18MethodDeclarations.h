﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct Dictionary_2_t875;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t4073;
// System.Collections.Generic.ICollection`1<Vuforia.QCARManagerImpl/TrackableResultData>
struct ICollection_1_t4254;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct KeyCollection_t3565;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct ValueCollection_t3569;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3221;
// System.Collections.Generic.IDictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct IDictionary_2_t4255;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>[]
struct KeyValuePair_2U5BU5D_t4256;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>>
struct IEnumerator_1_t4257;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_26.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__24.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor()
extern "C" void Dictionary_2__ctor_m4582_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m4582(__this, method) (( void (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2__ctor_m4582_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m21925_gshared (Dictionary_2_t875 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m21925(__this, ___comparer, method) (( void (*) (Dictionary_2_t875 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21925_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m21926_gshared (Dictionary_2_t875 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m21926(__this, ___dictionary, method) (( void (*) (Dictionary_2_t875 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21926_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m21927_gshared (Dictionary_2_t875 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m21927(__this, ___capacity, method) (( void (*) (Dictionary_2_t875 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m21927_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m21928_gshared (Dictionary_2_t875 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m21928(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t875 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m21928_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m21929_gshared (Dictionary_2_t875 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m21929(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t875 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m21929_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21930_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21930(__this, method) (( Object_t* (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m21930_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21931_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21931(__this, method) (( Object_t* (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m21931_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m21932_gshared (Dictionary_2_t875 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m21932(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t875 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m21932_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m21933_gshared (Dictionary_2_t875 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m21933(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t875 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m21933_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m21934_gshared (Dictionary_2_t875 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m21934(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t875 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m21934_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m21935_gshared (Dictionary_2_t875 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m21935(__this, ___key, method) (( bool (*) (Dictionary_2_t875 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m21935_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m21936_gshared (Dictionary_2_t875 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m21936(__this, ___key, method) (( void (*) (Dictionary_2_t875 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m21936_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21937_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21937(__this, method) (( bool (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m21937_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21938_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21938(__this, method) (( Object_t * (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m21938_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21939_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21939(__this, method) (( bool (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m21939_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21940_gshared (Dictionary_2_t875 * __this, KeyValuePair_2_t3563  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21940(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t875 *, KeyValuePair_2_t3563 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m21940_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21941_gshared (Dictionary_2_t875 * __this, KeyValuePair_2_t3563  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21941(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t875 *, KeyValuePair_2_t3563 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m21941_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21942_gshared (Dictionary_2_t875 * __this, KeyValuePair_2U5BU5D_t4256* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21942(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t875 *, KeyValuePair_2U5BU5D_t4256*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m21942_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21943_gshared (Dictionary_2_t875 * __this, KeyValuePair_2_t3563  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21943(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t875 *, KeyValuePair_2_t3563 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m21943_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m21944_gshared (Dictionary_2_t875 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m21944(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t875 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m21944_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21945_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21945(__this, method) (( Object_t * (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m21945_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21946_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21946(__this, method) (( Object_t* (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m21946_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21947_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21947(__this, method) (( Object_t * (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m21947_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m21948_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m21948(__this, method) (( int32_t (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_get_Count_m21948_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Item(TKey)
extern "C" TrackableResultData_t648  Dictionary_2_get_Item_m21949_gshared (Dictionary_2_t875 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m21949(__this, ___key, method) (( TrackableResultData_t648  (*) (Dictionary_2_t875 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m21949_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m21950_gshared (Dictionary_2_t875 * __this, int32_t ___key, TrackableResultData_t648  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m21950(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t875 *, int32_t, TrackableResultData_t648 , const MethodInfo*))Dictionary_2_set_Item_m21950_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m21951_gshared (Dictionary_2_t875 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m21951(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t875 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m21951_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m21952_gshared (Dictionary_2_t875 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m21952(__this, ___size, method) (( void (*) (Dictionary_2_t875 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m21952_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m21953_gshared (Dictionary_2_t875 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m21953(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t875 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m21953_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3563  Dictionary_2_make_pair_m21954_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t648  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m21954(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3563  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t648 , const MethodInfo*))Dictionary_2_make_pair_m21954_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m21955_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t648  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m21955(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t648 , const MethodInfo*))Dictionary_2_pick_key_m21955_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::pick_value(TKey,TValue)
extern "C" TrackableResultData_t648  Dictionary_2_pick_value_m21956_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t648  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m21956(__this /* static, unused */, ___key, ___value, method) (( TrackableResultData_t648  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t648 , const MethodInfo*))Dictionary_2_pick_value_m21956_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m21957_gshared (Dictionary_2_t875 * __this, KeyValuePair_2U5BU5D_t4256* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m21957(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t875 *, KeyValuePair_2U5BU5D_t4256*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m21957_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Resize()
extern "C" void Dictionary_2_Resize_m21958_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m21958(__this, method) (( void (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_Resize_m21958_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m21959_gshared (Dictionary_2_t875 * __this, int32_t ___key, TrackableResultData_t648  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m21959(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t875 *, int32_t, TrackableResultData_t648 , const MethodInfo*))Dictionary_2_Add_m21959_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Clear()
extern "C" void Dictionary_2_Clear_m21960_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m21960(__this, method) (( void (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_Clear_m21960_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m21961_gshared (Dictionary_2_t875 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m21961(__this, ___key, method) (( bool (*) (Dictionary_2_t875 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m21961_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m21962_gshared (Dictionary_2_t875 * __this, TrackableResultData_t648  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m21962(__this, ___value, method) (( bool (*) (Dictionary_2_t875 *, TrackableResultData_t648 , const MethodInfo*))Dictionary_2_ContainsValue_m21962_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m21963_gshared (Dictionary_2_t875 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m21963(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t875 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m21963_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m21964_gshared (Dictionary_2_t875 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m21964(__this, ___sender, method) (( void (*) (Dictionary_2_t875 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m21964_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m21965_gshared (Dictionary_2_t875 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m21965(__this, ___key, method) (( bool (*) (Dictionary_2_t875 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m21965_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m21966_gshared (Dictionary_2_t875 * __this, int32_t ___key, TrackableResultData_t648 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m21966(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t875 *, int32_t, TrackableResultData_t648 *, const MethodInfo*))Dictionary_2_TryGetValue_m21966_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Keys()
extern "C" KeyCollection_t3565 * Dictionary_2_get_Keys_m21967_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m21967(__this, method) (( KeyCollection_t3565 * (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_get_Keys_m21967_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Values()
extern "C" ValueCollection_t3569 * Dictionary_2_get_Values_m21968_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m21968(__this, method) (( ValueCollection_t3569 * (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_get_Values_m21968_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m21969_gshared (Dictionary_2_t875 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m21969(__this, ___key, method) (( int32_t (*) (Dictionary_2_t875 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m21969_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::ToTValue(System.Object)
extern "C" TrackableResultData_t648  Dictionary_2_ToTValue_m21970_gshared (Dictionary_2_t875 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m21970(__this, ___value, method) (( TrackableResultData_t648  (*) (Dictionary_2_t875 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m21970_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m21971_gshared (Dictionary_2_t875 * __this, KeyValuePair_2_t3563  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m21971(__this, ___pair, method) (( bool (*) (Dictionary_2_t875 *, KeyValuePair_2_t3563 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m21971_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::GetEnumerator()
extern "C" Enumerator_t3567  Dictionary_2_GetEnumerator_m21972_gshared (Dictionary_2_t875 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m21972(__this, method) (( Enumerator_t3567  (*) (Dictionary_2_t875 *, const MethodInfo*))Dictionary_2_GetEnumerator_m21972_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t2002  Dictionary_2_U3CCopyToU3Em__0_m21973_gshared (Object_t * __this /* static, unused */, int32_t ___key, TrackableResultData_t648  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m21973(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, int32_t, TrackableResultData_t648 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m21973_gshared)(__this /* static, unused */, ___key, ___value, method)
