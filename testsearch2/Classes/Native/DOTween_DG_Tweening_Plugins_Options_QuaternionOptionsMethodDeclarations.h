﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Options.QuaternionOptions
struct QuaternionOptions_t977;
struct QuaternionOptions_t977_marshaled;

void QuaternionOptions_t977_marshal(const QuaternionOptions_t977& unmarshaled, QuaternionOptions_t977_marshaled& marshaled);
void QuaternionOptions_t977_marshal_back(const QuaternionOptions_t977_marshaled& marshaled, QuaternionOptions_t977& unmarshaled);
void QuaternionOptions_t977_marshal_cleanup(QuaternionOptions_t977_marshaled& marshaled);
