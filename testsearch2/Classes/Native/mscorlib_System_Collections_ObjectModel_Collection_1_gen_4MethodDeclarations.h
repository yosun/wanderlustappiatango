﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.UInt16>
struct Collection_1_t3704;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.UInt16[]
struct UInt16U5BU5D_t1066;
// System.Collections.Generic.IEnumerator`1<System.UInt16>
struct IEnumerator_1_t4200;
// System.Collections.Generic.IList`1<System.UInt16>
struct IList_1_t3702;

// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::.ctor()
extern "C" void Collection_1__ctor_m23985_gshared (Collection_1_t3704 * __this, const MethodInfo* method);
#define Collection_1__ctor_m23985(__this, method) (( void (*) (Collection_1_t3704 *, const MethodInfo*))Collection_1__ctor_m23985_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23986_gshared (Collection_1_t3704 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23986(__this, method) (( bool (*) (Collection_1_t3704 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m23986_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m23987_gshared (Collection_1_t3704 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m23987(__this, ___array, ___index, method) (( void (*) (Collection_1_t3704 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m23987_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m23988_gshared (Collection_1_t3704 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m23988(__this, method) (( Object_t * (*) (Collection_1_t3704 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m23988_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m23989_gshared (Collection_1_t3704 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m23989(__this, ___value, method) (( int32_t (*) (Collection_1_t3704 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m23989_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m23990_gshared (Collection_1_t3704 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m23990(__this, ___value, method) (( bool (*) (Collection_1_t3704 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m23990_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m23991_gshared (Collection_1_t3704 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m23991(__this, ___value, method) (( int32_t (*) (Collection_1_t3704 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m23991_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m23992_gshared (Collection_1_t3704 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m23992(__this, ___index, ___value, method) (( void (*) (Collection_1_t3704 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m23992_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m23993_gshared (Collection_1_t3704 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m23993(__this, ___value, method) (( void (*) (Collection_1_t3704 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m23993_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m23994_gshared (Collection_1_t3704 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m23994(__this, method) (( bool (*) (Collection_1_t3704 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m23994_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m23995_gshared (Collection_1_t3704 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m23995(__this, method) (( Object_t * (*) (Collection_1_t3704 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m23995_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m23996_gshared (Collection_1_t3704 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m23996(__this, method) (( bool (*) (Collection_1_t3704 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m23996_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m23997_gshared (Collection_1_t3704 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m23997(__this, method) (( bool (*) (Collection_1_t3704 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m23997_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m23998_gshared (Collection_1_t3704 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m23998(__this, ___index, method) (( Object_t * (*) (Collection_1_t3704 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m23998_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m23999_gshared (Collection_1_t3704 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m23999(__this, ___index, ___value, method) (( void (*) (Collection_1_t3704 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m23999_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::Add(T)
extern "C" void Collection_1_Add_m24000_gshared (Collection_1_t3704 * __this, uint16_t ___item, const MethodInfo* method);
#define Collection_1_Add_m24000(__this, ___item, method) (( void (*) (Collection_1_t3704 *, uint16_t, const MethodInfo*))Collection_1_Add_m24000_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::Clear()
extern "C" void Collection_1_Clear_m24001_gshared (Collection_1_t3704 * __this, const MethodInfo* method);
#define Collection_1_Clear_m24001(__this, method) (( void (*) (Collection_1_t3704 *, const MethodInfo*))Collection_1_Clear_m24001_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::ClearItems()
extern "C" void Collection_1_ClearItems_m24002_gshared (Collection_1_t3704 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m24002(__this, method) (( void (*) (Collection_1_t3704 *, const MethodInfo*))Collection_1_ClearItems_m24002_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::Contains(T)
extern "C" bool Collection_1_Contains_m24003_gshared (Collection_1_t3704 * __this, uint16_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m24003(__this, ___item, method) (( bool (*) (Collection_1_t3704 *, uint16_t, const MethodInfo*))Collection_1_Contains_m24003_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m24004_gshared (Collection_1_t3704 * __this, UInt16U5BU5D_t1066* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m24004(__this, ___array, ___index, method) (( void (*) (Collection_1_t3704 *, UInt16U5BU5D_t1066*, int32_t, const MethodInfo*))Collection_1_CopyTo_m24004_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.UInt16>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m24005_gshared (Collection_1_t3704 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m24005(__this, method) (( Object_t* (*) (Collection_1_t3704 *, const MethodInfo*))Collection_1_GetEnumerator_m24005_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m24006_gshared (Collection_1_t3704 * __this, uint16_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m24006(__this, ___item, method) (( int32_t (*) (Collection_1_t3704 *, uint16_t, const MethodInfo*))Collection_1_IndexOf_m24006_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m24007_gshared (Collection_1_t3704 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m24007(__this, ___index, ___item, method) (( void (*) (Collection_1_t3704 *, int32_t, uint16_t, const MethodInfo*))Collection_1_Insert_m24007_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m24008_gshared (Collection_1_t3704 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m24008(__this, ___index, ___item, method) (( void (*) (Collection_1_t3704 *, int32_t, uint16_t, const MethodInfo*))Collection_1_InsertItem_m24008_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::Remove(T)
extern "C" bool Collection_1_Remove_m24009_gshared (Collection_1_t3704 * __this, uint16_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m24009(__this, ___item, method) (( bool (*) (Collection_1_t3704 *, uint16_t, const MethodInfo*))Collection_1_Remove_m24009_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m24010_gshared (Collection_1_t3704 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m24010(__this, ___index, method) (( void (*) (Collection_1_t3704 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m24010_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m24011_gshared (Collection_1_t3704 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m24011(__this, ___index, method) (( void (*) (Collection_1_t3704 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m24011_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.UInt16>::get_Count()
extern "C" int32_t Collection_1_get_Count_m24012_gshared (Collection_1_t3704 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m24012(__this, method) (( int32_t (*) (Collection_1_t3704 *, const MethodInfo*))Collection_1_get_Count_m24012_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.UInt16>::get_Item(System.Int32)
extern "C" uint16_t Collection_1_get_Item_m24013_gshared (Collection_1_t3704 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m24013(__this, ___index, method) (( uint16_t (*) (Collection_1_t3704 *, int32_t, const MethodInfo*))Collection_1_get_Item_m24013_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m24014_gshared (Collection_1_t3704 * __this, int32_t ___index, uint16_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m24014(__this, ___index, ___value, method) (( void (*) (Collection_1_t3704 *, int32_t, uint16_t, const MethodInfo*))Collection_1_set_Item_m24014_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m24015_gshared (Collection_1_t3704 * __this, int32_t ___index, uint16_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m24015(__this, ___index, ___item, method) (( void (*) (Collection_1_t3704 *, int32_t, uint16_t, const MethodInfo*))Collection_1_SetItem_m24015_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m24016_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m24016(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m24016_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.UInt16>::ConvertItem(System.Object)
extern "C" uint16_t Collection_1_ConvertItem_m24017_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m24017(__this /* static, unused */, ___item, method) (( uint16_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m24017_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.UInt16>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m24018_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m24018(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m24018_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m24019_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m24019(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m24019_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.UInt16>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m24020_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m24020(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m24020_gshared)(__this /* static, unused */, ___list, method)
