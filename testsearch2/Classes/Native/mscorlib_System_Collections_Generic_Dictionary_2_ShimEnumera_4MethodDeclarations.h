﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct ShimEnumerator_t3589;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Dictionary_2_t876;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m22196_gshared (ShimEnumerator_t3589 * __this, Dictionary_2_t876 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m22196(__this, ___host, method) (( void (*) (ShimEnumerator_t3589 *, Dictionary_2_t876 *, const MethodInfo*))ShimEnumerator__ctor_m22196_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m22197_gshared (ShimEnumerator_t3589 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m22197(__this, method) (( bool (*) (ShimEnumerator_t3589 *, const MethodInfo*))ShimEnumerator_MoveNext_m22197_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Entry()
extern "C" DictionaryEntry_t2002  ShimEnumerator_get_Entry_m22198_gshared (ShimEnumerator_t3589 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m22198(__this, method) (( DictionaryEntry_t2002  (*) (ShimEnumerator_t3589 *, const MethodInfo*))ShimEnumerator_get_Entry_m22198_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m22199_gshared (ShimEnumerator_t3589 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m22199(__this, method) (( Object_t * (*) (ShimEnumerator_t3589 *, const MethodInfo*))ShimEnumerator_get_Key_m22199_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m22200_gshared (ShimEnumerator_t3589 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m22200(__this, method) (( Object_t * (*) (ShimEnumerator_t3589 *, const MethodInfo*))ShimEnumerator_get_Value_m22200_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m22201_gshared (ShimEnumerator_t3589 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m22201(__this, method) (( Object_t * (*) (ShimEnumerator_t3589 *, const MethodInfo*))ShimEnumerator_get_Current_m22201_gshared)(__this, method)
