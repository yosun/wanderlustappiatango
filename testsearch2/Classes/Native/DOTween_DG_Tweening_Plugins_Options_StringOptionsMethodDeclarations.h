﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_t1009;
struct StringOptions_t1009_marshaled;

void StringOptions_t1009_marshal(const StringOptions_t1009& unmarshaled, StringOptions_t1009_marshaled& marshaled);
void StringOptions_t1009_marshal_back(const StringOptions_t1009_marshaled& marshaled, StringOptions_t1009& unmarshaled);
void StringOptions_t1009_marshal_cleanup(StringOptions_t1009_marshaled& marshaled);
