﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.Marker>
struct List_1_t819;
// System.Object
struct Object_t;
// Vuforia.Marker
struct Marker_t745;
// System.Collections.Generic.IEnumerable`1<Vuforia.Marker>
struct IEnumerable_1_t772;
// System.Collections.Generic.IEnumerator`1<Vuforia.Marker>
struct IEnumerator_1_t4189;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.Marker>
struct ICollection_1_t4184;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.Marker>
struct ReadOnlyCollection_1_t3449;
// Vuforia.Marker[]
struct MarkerU5BU5D_t3443;
// System.Predicate`1<Vuforia.Marker>
struct Predicate_1_t3450;
// System.Comparison`1<Vuforia.Marker>
struct Comparison_1_t3451;
// System.Collections.Generic.List`1/Enumerator<Vuforia.Marker>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_5.h"

// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m19803(__this, method) (( void (*) (List_1_t819 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m4416(__this, ___collection, method) (( void (*) (List_1_t819 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::.ctor(System.Int32)
#define List_1__ctor_m19804(__this, ___capacity, method) (( void (*) (List_1_t819 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::.cctor()
#define List_1__cctor_m19805(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19806(__this, method) (( Object_t* (*) (List_1_t819 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19807(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t819 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19808(__this, method) (( Object_t * (*) (List_1_t819 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19809(__this, ___item, method) (( int32_t (*) (List_1_t819 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19810(__this, ___item, method) (( bool (*) (List_1_t819 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19811(__this, ___item, method) (( int32_t (*) (List_1_t819 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19812(__this, ___index, ___item, method) (( void (*) (List_1_t819 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19813(__this, ___item, method) (( void (*) (List_1_t819 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19814(__this, method) (( bool (*) (List_1_t819 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19815(__this, method) (( bool (*) (List_1_t819 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19816(__this, method) (( Object_t * (*) (List_1_t819 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19817(__this, method) (( bool (*) (List_1_t819 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19818(__this, method) (( bool (*) (List_1_t819 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19819(__this, ___index, method) (( Object_t * (*) (List_1_t819 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19820(__this, ___index, ___value, method) (( void (*) (List_1_t819 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Add(T)
#define List_1_Add_m19821(__this, ___item, method) (( void (*) (List_1_t819 *, Object_t *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19822(__this, ___newCount, method) (( void (*) (List_1_t819 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19823(__this, ___collection, method) (( void (*) (List_1_t819 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19824(__this, ___enumerable, method) (( void (*) (List_1_t819 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19825(__this, ___collection, method) (( void (*) (List_1_t819 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.Marker>::AsReadOnly()
#define List_1_AsReadOnly_m19826(__this, method) (( ReadOnlyCollection_1_t3449 * (*) (List_1_t819 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Clear()
#define List_1_Clear_m19827(__this, method) (( void (*) (List_1_t819 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::Contains(T)
#define List_1_Contains_m19828(__this, ___item, method) (( bool (*) (List_1_t819 *, Object_t *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19829(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t819 *, MarkerU5BU5D_t3443*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.Marker>::Find(System.Predicate`1<T>)
#define List_1_Find_m19830(__this, ___match, method) (( Object_t * (*) (List_1_t819 *, Predicate_1_t3450 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19831(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3450 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19832(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t819 *, int32_t, int32_t, Predicate_1_t3450 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.Marker>::GetEnumerator()
#define List_1_GetEnumerator_m4417(__this, method) (( Enumerator_t820  (*) (List_1_t819 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::IndexOf(T)
#define List_1_IndexOf_m19833(__this, ___item, method) (( int32_t (*) (List_1_t819 *, Object_t *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19834(__this, ___start, ___delta, method) (( void (*) (List_1_t819 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19835(__this, ___index, method) (( void (*) (List_1_t819 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Insert(System.Int32,T)
#define List_1_Insert_m19836(__this, ___index, ___item, method) (( void (*) (List_1_t819 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19837(__this, ___collection, method) (( void (*) (List_1_t819 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.Marker>::Remove(T)
#define List_1_Remove_m19838(__this, ___item, method) (( bool (*) (List_1_t819 *, Object_t *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19839(__this, ___match, method) (( int32_t (*) (List_1_t819 *, Predicate_1_t3450 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19840(__this, ___index, method) (( void (*) (List_1_t819 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Reverse()
#define List_1_Reverse_m19841(__this, method) (( void (*) (List_1_t819 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Sort()
#define List_1_Sort_m19842(__this, method) (( void (*) (List_1_t819 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19843(__this, ___comparison, method) (( void (*) (List_1_t819 *, Comparison_1_t3451 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.Marker>::ToArray()
#define List_1_ToArray_m19844(__this, method) (( MarkerU5BU5D_t3443* (*) (List_1_t819 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::TrimExcess()
#define List_1_TrimExcess_m19845(__this, method) (( void (*) (List_1_t819 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::get_Capacity()
#define List_1_get_Capacity_m19846(__this, method) (( int32_t (*) (List_1_t819 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19847(__this, ___value, method) (( void (*) (List_1_t819 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.Marker>::get_Count()
#define List_1_get_Count_m19848(__this, method) (( int32_t (*) (List_1_t819 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.Marker>::get_Item(System.Int32)
#define List_1_get_Item_m19849(__this, ___index, method) (( Object_t * (*) (List_1_t819 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.Marker>::set_Item(System.Int32,T)
#define List_1_set_Item_m19850(__this, ___index, ___value, method) (( void (*) (List_1_t819 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
