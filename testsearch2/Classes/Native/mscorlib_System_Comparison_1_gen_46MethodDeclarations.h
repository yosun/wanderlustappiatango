﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<Vuforia.IVideoBackgroundEventHandler>
struct Comparison_1_t3649;
// System.Object
struct Object_t;
// Vuforia.IVideoBackgroundEventHandler
struct IVideoBackgroundEventHandler_t178;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<Vuforia.IVideoBackgroundEventHandler>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.Object>
#include "mscorlib_System_Comparison_1_gen_4MethodDeclarations.h"
#define Comparison_1__ctor_m23080(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3649 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m15416_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<Vuforia.IVideoBackgroundEventHandler>::Invoke(T,T)
#define Comparison_1_Invoke_m23081(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3649 *, Object_t *, Object_t *, const MethodInfo*))Comparison_1_Invoke_m15417_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<Vuforia.IVideoBackgroundEventHandler>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m23082(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3649 *, Object_t *, Object_t *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m15418_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<Vuforia.IVideoBackgroundEventHandler>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m23083(__this, ___result, method) (( int32_t (*) (Comparison_1_t3649 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m15419_gshared)(__this, ___result, method)
