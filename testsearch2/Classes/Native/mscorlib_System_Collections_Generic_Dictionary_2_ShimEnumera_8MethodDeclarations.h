﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ShimEnumerator_t3883;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t3874;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m26610_gshared (ShimEnumerator_t3883 * __this, Dictionary_2_t3874 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m26610(__this, ___host, method) (( void (*) (ShimEnumerator_t3883 *, Dictionary_2_t3874 *, const MethodInfo*))ShimEnumerator__ctor_m26610_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m26611_gshared (ShimEnumerator_t3883 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m26611(__this, method) (( bool (*) (ShimEnumerator_t3883 *, const MethodInfo*))ShimEnumerator_MoveNext_m26611_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Entry()
extern "C" DictionaryEntry_t2002  ShimEnumerator_get_Entry_m26612_gshared (ShimEnumerator_t3883 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m26612(__this, method) (( DictionaryEntry_t2002  (*) (ShimEnumerator_t3883 *, const MethodInfo*))ShimEnumerator_get_Entry_m26612_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m26613_gshared (ShimEnumerator_t3883 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m26613(__this, method) (( Object_t * (*) (ShimEnumerator_t3883 *, const MethodInfo*))ShimEnumerator_get_Key_m26613_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m26614_gshared (ShimEnumerator_t3883 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m26614(__this, method) (( Object_t * (*) (ShimEnumerator_t3883 *, const MethodInfo*))ShimEnumerator_get_Value_m26614_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m26615_gshared (ShimEnumerator_t3883 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m26615(__this, method) (( Object_t * (*) (ShimEnumerator_t3883 *, const MethodInfo*))ShimEnumerator_get_Current_m26615_gshared)(__this, method)
