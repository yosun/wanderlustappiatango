﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.HMAC
#include "mscorlib_System_Security_Cryptography_HMAC.h"
// System.Security.Cryptography.HMACSHA384
struct  HMACSHA384_t2409  : public HMAC_t1818
{
	// System.Boolean System.Security.Cryptography.HMACSHA384::legacy
	bool ___legacy_11;
};
struct HMACSHA384_t2409_StaticFields{
	// System.Boolean System.Security.Cryptography.HMACSHA384::legacy_mode
	bool ___legacy_mode_10;
};
