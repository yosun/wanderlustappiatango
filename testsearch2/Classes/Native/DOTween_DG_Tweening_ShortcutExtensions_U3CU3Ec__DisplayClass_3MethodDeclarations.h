﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6
struct U3CU3Ec__DisplayClassc6_t969;
// UnityEngine.Vector3
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6::.ctor()
extern "C" void U3CU3Ec__DisplayClassc6__ctor_m5397 (U3CU3Ec__DisplayClassc6_t969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6::<DOScaleY>b__c4()
extern "C" Vector3_t14  U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c4_m5398 (U3CU3Ec__DisplayClassc6_t969 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.ShortcutExtensions/<>c__DisplayClassc6::<DOScaleY>b__c5(UnityEngine.Vector3)
extern "C" void U3CU3Ec__DisplayClassc6_U3CDOScaleYU3Eb__c5_m5399 (U3CU3Ec__DisplayClassc6_t969 * __this, Vector3_t14  ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
