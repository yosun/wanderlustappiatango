﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Reflection.AssemblyDescriptionAttribute
struct AssemblyDescriptionAttribute_t486;
// System.String
struct String_t;

// System.Void System.Reflection.AssemblyDescriptionAttribute::.ctor(System.String)
extern "C" void AssemblyDescriptionAttribute__ctor_m2449 (AssemblyDescriptionAttribute_t486 * __this, String_t* ___description, const MethodInfo* method) IL2CPP_METHOD_ATTR;
