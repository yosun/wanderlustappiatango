﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>
struct InternalEnumerator_1_t3728;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m24283_gshared (InternalEnumerator_1_t3728 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m24283(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3728 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m24283_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24284_gshared (InternalEnumerator_1_t3728 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24284(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3728 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24284_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m24285_gshared (InternalEnumerator_1_t3728 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m24285(__this, method) (( void (*) (InternalEnumerator_1_t3728 *, const MethodInfo*))InternalEnumerator_1_Dispose_m24285_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m24286_gshared (InternalEnumerator_1_t3728 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m24286(__this, method) (( bool (*) (InternalEnumerator_1_t3728 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m24286_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern "C" GcScoreData_t1312  InternalEnumerator_1_get_Current_m24287_gshared (InternalEnumerator_1_t3728 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m24287(__this, method) (( GcScoreData_t1312  (*) (InternalEnumerator_1_t3728 *, const MethodInfo*))InternalEnumerator_1_get_Current_m24287_gshared)(__this, method)
