﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
struct ThreadSafeDictionary_2_t3853;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t1378;
// System.Object
struct Object_t;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>
struct ThreadSafeDictionaryValueFactory_2_t3851;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t3872;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t4041;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern "C" void ThreadSafeDictionary_2__ctor_m26101_gshared (ThreadSafeDictionary_2_t3853 * __this, ThreadSafeDictionaryValueFactory_2_t3851 * ___valueFactory, const MethodInfo* method);
#define ThreadSafeDictionary_2__ctor_m26101(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t3853 *, ThreadSafeDictionaryValueFactory_2_t3851 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m26101_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m26103_gshared (ThreadSafeDictionary_2_t3853 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m26103(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t3853 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m26103_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Get(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_Get_m26105_gshared (ThreadSafeDictionary_2_t3853 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_Get_m26105(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t3853 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m26105_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::AddValue(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_AddValue_m26107_gshared (ThreadSafeDictionary_2_t3853 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_AddValue_m26107(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t3853 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m26107_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C" void ThreadSafeDictionary_2_Add_m26109_gshared (ThreadSafeDictionary_2_t3853 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m26109(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t3853 *, Object_t *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Add_m26109_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Keys()
extern "C" Object_t* ThreadSafeDictionary_2_get_Keys_m26111_gshared (ThreadSafeDictionary_2_t3853 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Keys_m26111(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t3853 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m26111_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(TKey)
extern "C" bool ThreadSafeDictionary_2_Remove_m26113_gshared (ThreadSafeDictionary_2_t3853 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_Remove_m26113(__this, ___key, method) (( bool (*) (ThreadSafeDictionary_2_t3853 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Remove_m26113_gshared)(__this, ___key, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool ThreadSafeDictionary_2_TryGetValue_m26115_gshared (ThreadSafeDictionary_2_t3853 * __this, Object_t * ___key, Object_t ** ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_TryGetValue_m26115(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t3853 *, Object_t *, Object_t **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m26115_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Values()
extern "C" Object_t* ThreadSafeDictionary_2_get_Values_m26117_gshared (ThreadSafeDictionary_2_t3853 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Values_m26117(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t3853 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m26117_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_get_Item_m26119_gshared (ThreadSafeDictionary_2_t3853 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Item_m26119(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t3853 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m26119_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C" void ThreadSafeDictionary_2_set_Item_m26121_gshared (ThreadSafeDictionary_2_t3853 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_set_Item_m26121(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t3853 *, Object_t *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m26121_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void ThreadSafeDictionary_2_Add_m26123_gshared (ThreadSafeDictionary_2_t3853 * __this, KeyValuePair_2_t3114  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m26123(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t3853 *, KeyValuePair_2_t3114 , const MethodInfo*))ThreadSafeDictionary_2_Add_m26123_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Clear()
extern "C" void ThreadSafeDictionary_2_Clear_m26125_gshared (ThreadSafeDictionary_2_t3853 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_Clear_m26125(__this, method) (( void (*) (ThreadSafeDictionary_2_t3853 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m26125_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool ThreadSafeDictionary_2_Contains_m26127_gshared (ThreadSafeDictionary_2_t3853 * __this, KeyValuePair_2_t3114  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Contains_m26127(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t3853 *, KeyValuePair_2_t3114 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m26127_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void ThreadSafeDictionary_2_CopyTo_m26129_gshared (ThreadSafeDictionary_2_t3853 * __this, KeyValuePair_2U5BU5D_t3872* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ThreadSafeDictionary_2_CopyTo_m26129(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t3853 *, KeyValuePair_2U5BU5D_t3872*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m26129_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Count()
extern "C" int32_t ThreadSafeDictionary_2_get_Count_m26131_gshared (ThreadSafeDictionary_2_t3853 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Count_m26131(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t3853 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m26131_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C" bool ThreadSafeDictionary_2_get_IsReadOnly_m26133_gshared (ThreadSafeDictionary_2_t3853 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_IsReadOnly_m26133(__this, method) (( bool (*) (ThreadSafeDictionary_2_t3853 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m26133_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool ThreadSafeDictionary_2_Remove_m26135_gshared (ThreadSafeDictionary_2_t3853 * __this, KeyValuePair_2_t3114  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Remove_m26135(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t3853 *, KeyValuePair_2_t3114 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m26135_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C" Object_t* ThreadSafeDictionary_2_GetEnumerator_m26137_gshared (ThreadSafeDictionary_2_t3853 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_GetEnumerator_m26137(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t3853 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m26137_gshared)(__this, method)
