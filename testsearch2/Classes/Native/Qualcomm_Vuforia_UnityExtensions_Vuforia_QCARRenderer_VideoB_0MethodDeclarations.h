﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.QCARRenderer/VideoBGCfgData
struct VideoBGCfgData_t668;
struct VideoBGCfgData_t668_marshaled;

void VideoBGCfgData_t668_marshal(const VideoBGCfgData_t668& unmarshaled, VideoBGCfgData_t668_marshaled& marshaled);
void VideoBGCfgData_t668_marshal_back(const VideoBGCfgData_t668_marshaled& marshaled, VideoBGCfgData_t668& unmarshaled);
void VideoBGCfgData_t668_marshal_cleanup(VideoBGCfgData_t668_marshaled& marshaled);
