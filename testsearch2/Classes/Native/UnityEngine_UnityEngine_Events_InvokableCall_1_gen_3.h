﻿#pragma once
#include <stdint.h>
// UnityEngine.Events.UnityAction`1<System.Byte>
struct UnityAction_1_t3347;
// UnityEngine.Events.BaseInvokableCall
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"
// UnityEngine.Events.InvokableCall`1<System.Byte>
struct  InvokableCall_1_t3348  : public BaseInvokableCall_t1345
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<System.Byte>::Delegate
	UnityAction_1_t3347 * ___Delegate_0;
};
