﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.Collection`1<System.Int32>
struct Collection_1_t3404;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Int32[]
struct Int32U5BU5D_t27;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t4081;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t3402;

// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::.ctor()
extern "C" void Collection_1__ctor_m19116_gshared (Collection_1_t3404 * __this, const MethodInfo* method);
#define Collection_1__ctor_m19116(__this, method) (( void (*) (Collection_1_t3404 *, const MethodInfo*))Collection_1__ctor_m19116_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19117_gshared (Collection_1_t3404 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19117(__this, method) (( bool (*) (Collection_1_t3404 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19117_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m19118_gshared (Collection_1_t3404 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m19118(__this, ___array, ___index, method) (( void (*) (Collection_1_t3404 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m19118_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m19119_gshared (Collection_1_t3404 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m19119(__this, method) (( Object_t * (*) (Collection_1_t3404 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m19119_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m19120_gshared (Collection_1_t3404 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m19120(__this, ___value, method) (( int32_t (*) (Collection_1_t3404 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m19120_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m19121_gshared (Collection_1_t3404 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m19121(__this, ___value, method) (( bool (*) (Collection_1_t3404 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m19121_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m19122_gshared (Collection_1_t3404 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m19122(__this, ___value, method) (( int32_t (*) (Collection_1_t3404 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m19122_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m19123_gshared (Collection_1_t3404 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m19123(__this, ___index, ___value, method) (( void (*) (Collection_1_t3404 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m19123_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m19124_gshared (Collection_1_t3404 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m19124(__this, ___value, method) (( void (*) (Collection_1_t3404 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m19124_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m19125_gshared (Collection_1_t3404 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m19125(__this, method) (( bool (*) (Collection_1_t3404 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m19125_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m19126_gshared (Collection_1_t3404 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m19126(__this, method) (( Object_t * (*) (Collection_1_t3404 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m19126_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m19127_gshared (Collection_1_t3404 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m19127(__this, method) (( bool (*) (Collection_1_t3404 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m19127_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m19128_gshared (Collection_1_t3404 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m19128(__this, method) (( bool (*) (Collection_1_t3404 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m19128_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m19129_gshared (Collection_1_t3404 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m19129(__this, ___index, method) (( Object_t * (*) (Collection_1_t3404 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m19129_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m19130_gshared (Collection_1_t3404 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m19130(__this, ___index, ___value, method) (( void (*) (Collection_1_t3404 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m19130_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Add(T)
extern "C" void Collection_1_Add_m19131_gshared (Collection_1_t3404 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m19131(__this, ___item, method) (( void (*) (Collection_1_t3404 *, int32_t, const MethodInfo*))Collection_1_Add_m19131_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Clear()
extern "C" void Collection_1_Clear_m19132_gshared (Collection_1_t3404 * __this, const MethodInfo* method);
#define Collection_1_Clear_m19132(__this, method) (( void (*) (Collection_1_t3404 *, const MethodInfo*))Collection_1_Clear_m19132_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::ClearItems()
extern "C" void Collection_1_ClearItems_m19133_gshared (Collection_1_t3404 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m19133(__this, method) (( void (*) (Collection_1_t3404 *, const MethodInfo*))Collection_1_ClearItems_m19133_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Contains(T)
extern "C" bool Collection_1_Contains_m19134_gshared (Collection_1_t3404 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m19134(__this, ___item, method) (( bool (*) (Collection_1_t3404 *, int32_t, const MethodInfo*))Collection_1_Contains_m19134_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m19135_gshared (Collection_1_t3404 * __this, Int32U5BU5D_t27* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m19135(__this, ___array, ___index, method) (( void (*) (Collection_1_t3404 *, Int32U5BU5D_t27*, int32_t, const MethodInfo*))Collection_1_CopyTo_m19135_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Int32>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m19136_gshared (Collection_1_t3404 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m19136(__this, method) (( Object_t* (*) (Collection_1_t3404 *, const MethodInfo*))Collection_1_GetEnumerator_m19136_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m19137_gshared (Collection_1_t3404 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m19137(__this, ___item, method) (( int32_t (*) (Collection_1_t3404 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m19137_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m19138_gshared (Collection_1_t3404 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m19138(__this, ___index, ___item, method) (( void (*) (Collection_1_t3404 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m19138_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m19139_gshared (Collection_1_t3404 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m19139(__this, ___index, ___item, method) (( void (*) (Collection_1_t3404 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m19139_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Remove(T)
extern "C" bool Collection_1_Remove_m19140_gshared (Collection_1_t3404 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m19140(__this, ___item, method) (( bool (*) (Collection_1_t3404 *, int32_t, const MethodInfo*))Collection_1_Remove_m19140_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m19141_gshared (Collection_1_t3404 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m19141(__this, ___index, method) (( void (*) (Collection_1_t3404 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m19141_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m19142_gshared (Collection_1_t3404 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m19142(__this, ___index, method) (( void (*) (Collection_1_t3404 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m19142_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::get_Count()
extern "C" int32_t Collection_1_get_Count_m19143_gshared (Collection_1_t3404 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m19143(__this, method) (( int32_t (*) (Collection_1_t3404 *, const MethodInfo*))Collection_1_get_Count_m19143_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m19144_gshared (Collection_1_t3404 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m19144(__this, ___index, method) (( int32_t (*) (Collection_1_t3404 *, int32_t, const MethodInfo*))Collection_1_get_Item_m19144_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m19145_gshared (Collection_1_t3404 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m19145(__this, ___index, ___value, method) (( void (*) (Collection_1_t3404 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m19145_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m19146_gshared (Collection_1_t3404 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m19146(__this, ___index, ___item, method) (( void (*) (Collection_1_t3404 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m19146_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m19147_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m19147(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m19147_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m19148_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m19148(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m19148_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m19149_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m19149(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m19149_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m19150_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m19150(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m19150_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m19151_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m19151(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m19151_gshared)(__this /* static, unused */, ___list, method)
