﻿#pragma once
#include <stdint.h>
// UnityEngine.Material
struct Material_t4;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// Vuforia.MaskOutAbstractBehaviour
struct  MaskOutAbstractBehaviour_t68  : public MonoBehaviour_t7
{
	// UnityEngine.Material Vuforia.MaskOutAbstractBehaviour::maskMaterial
	Material_t4 * ___maskMaterial_2;
};
