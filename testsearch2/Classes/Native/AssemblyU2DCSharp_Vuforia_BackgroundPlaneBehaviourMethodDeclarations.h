﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.BackgroundPlaneBehaviour
struct BackgroundPlaneBehaviour_t39;

// System.Void Vuforia.BackgroundPlaneBehaviour::.ctor()
extern "C" void BackgroundPlaneBehaviour__ctor_m141 (BackgroundPlaneBehaviour_t39 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
