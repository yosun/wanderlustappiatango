﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
struct Enumerator_t3257;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t3250;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_10.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16858_gshared (Enumerator_t3257 * __this, Dictionary_2_t3250 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m16858(__this, ___dictionary, method) (( void (*) (Enumerator_t3257 *, Dictionary_2_t3250 *, const MethodInfo*))Enumerator__ctor_m16858_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16859_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16859(__this, method) (( Object_t * (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16859_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t2002  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16860_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16860(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16860_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16861_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16861(__this, method) (( Object_t * (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16861_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16862_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16862(__this, method) (( Object_t * (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16862_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16863_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16863(__this, method) (( bool (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_MoveNext_m16863_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" KeyValuePair_2_t3253  Enumerator_get_Current_m16864_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16864(__this, method) (( KeyValuePair_2_t3253  (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_get_Current_m16864_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m16865_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m16865(__this, method) (( Object_t * (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_get_CurrentKey_m16865_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m16866_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m16866(__this, method) (( int32_t (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_get_CurrentValue_m16866_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m16867_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m16867(__this, method) (( void (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_VerifyState_m16867_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m16868_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m16868(__this, method) (( void (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_VerifyCurrent_m16868_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m16869_gshared (Enumerator_t3257 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16869(__this, method) (( void (*) (Enumerator_t3257 *, const MethodInfo*))Enumerator_Dispose_m16869_gshared)(__this, method)
