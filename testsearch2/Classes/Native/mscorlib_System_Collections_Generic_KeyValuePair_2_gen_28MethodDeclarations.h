﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>
struct KeyValuePair_2_t3609;
// Vuforia.ImageTarget
struct ImageTarget_t738;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_9MethodDeclarations.h"
#define KeyValuePair_2__ctor_m22502(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3609 *, int32_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m16496_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::get_Key()
#define KeyValuePair_2_get_Key_m22503(__this, method) (( int32_t (*) (KeyValuePair_2_t3609 *, const MethodInfo*))KeyValuePair_2_get_Key_m16497_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m22504(__this, ___value, method) (( void (*) (KeyValuePair_2_t3609 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m16498_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::get_Value()
#define KeyValuePair_2_get_Value_m22505(__this, method) (( Object_t * (*) (KeyValuePair_2_t3609 *, const MethodInfo*))KeyValuePair_2_get_Value_m16499_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m22506(__this, ___value, method) (( void (*) (KeyValuePair_2_t3609 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m16500_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.ImageTarget>::ToString()
#define KeyValuePair_2_ToString_m22507(__this, method) (( String_t* (*) (KeyValuePair_2_t3609 *, const MethodInfo*))KeyValuePair_2_ToString_m16501_gshared)(__this, method)
