﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct TweenerCore_3_t1046;
// DG.Tweening.Core.Enums.UpdateMode
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode.h"
// DG.Tweening.Core.Enums.UpdateNotice
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice.h"

// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::.ctor()
extern "C" void TweenerCore_3__ctor_m23815_gshared (TweenerCore_3_t1046 * __this, const MethodInfo* method);
#define TweenerCore_3__ctor_m23815(__this, method) (( void (*) (TweenerCore_3_t1046 *, const MethodInfo*))TweenerCore_3__ctor_m23815_gshared)(__this, method)
// System.Void DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Reset()
extern "C" void TweenerCore_3_Reset_m23816_gshared (TweenerCore_3_t1046 * __this, const MethodInfo* method);
#define TweenerCore_3_Reset_m23816(__this, method) (( void (*) (TweenerCore_3_t1046 *, const MethodInfo*))TweenerCore_3_Reset_m23816_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Validate()
extern "C" bool TweenerCore_3_Validate_m23817_gshared (TweenerCore_3_t1046 * __this, const MethodInfo* method);
#define TweenerCore_3_Validate_m23817(__this, method) (( bool (*) (TweenerCore_3_t1046 *, const MethodInfo*))TweenerCore_3_Validate_m23817_gshared)(__this, method)
// System.Single DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::UpdateDelay(System.Single)
extern "C" float TweenerCore_3_UpdateDelay_m23818_gshared (TweenerCore_3_t1046 * __this, float ___elapsed, const MethodInfo* method);
#define TweenerCore_3_UpdateDelay_m23818(__this, ___elapsed, method) (( float (*) (TweenerCore_3_t1046 *, float, const MethodInfo*))TweenerCore_3_UpdateDelay_m23818_gshared)(__this, ___elapsed, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::Startup()
extern "C" bool TweenerCore_3_Startup_m23819_gshared (TweenerCore_3_t1046 * __this, const MethodInfo* method);
#define TweenerCore_3_Startup_m23819(__this, method) (( bool (*) (TweenerCore_3_t1046 *, const MethodInfo*))TweenerCore_3_Startup_m23819_gshared)(__this, method)
// System.Boolean DG.Tweening.Core.TweenerCore`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>::ApplyTween(System.Single,System.Int32,System.Int32,System.Boolean,DG.Tweening.Core.Enums.UpdateMode,DG.Tweening.Core.Enums.UpdateNotice)
extern "C" bool TweenerCore_3_ApplyTween_m23820_gshared (TweenerCore_3_t1046 * __this, float ___prevPosition, int32_t ___prevCompletedLoops, int32_t ___newCompletedSteps, bool ___useInversePosition, int32_t ___updateMode, int32_t ___updateNotice, const MethodInfo* method);
#define TweenerCore_3_ApplyTween_m23820(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method) (( bool (*) (TweenerCore_3_t1046 *, float, int32_t, int32_t, bool, int32_t, int32_t, const MethodInfo*))TweenerCore_3_ApplyTween_m23820_gshared)(__this, ___prevPosition, ___prevCompletedLoops, ___newCompletedSteps, ___useInversePosition, ___updateMode, ___updateNotice, method)
