﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// DG.Tweening.Core.DOSetter`1<DG.Tweening.Color2>
struct  DOSetter_1_t1039  : public MulticastDelegate_t314
{
};
