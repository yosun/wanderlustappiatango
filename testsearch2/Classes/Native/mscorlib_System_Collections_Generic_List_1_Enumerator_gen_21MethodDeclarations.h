﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>
struct Enumerator_t901;
// System.Object
struct Object_t;
// Vuforia.IVirtualButtonEventHandler
struct IVirtualButtonEventHandler_t797;
// System.Collections.Generic.List`1<Vuforia.IVirtualButtonEventHandler>
struct List_1_t762;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m23413(__this, ___l, method) (( void (*) (Enumerator_t901 *, List_1_t762 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m23414(__this, method) (( Object_t * (*) (Enumerator_t901 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::Dispose()
#define Enumerator_Dispose_m23415(__this, method) (( void (*) (Enumerator_t901 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::VerifyState()
#define Enumerator_VerifyState_m23416(__this, method) (( void (*) (Enumerator_t901 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4693(__this, method) (( bool (*) (Enumerator_t901 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.IVirtualButtonEventHandler>::get_Current()
#define Enumerator_get_Current_m4692(__this, method) (( Object_t * (*) (Enumerator_t901 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
