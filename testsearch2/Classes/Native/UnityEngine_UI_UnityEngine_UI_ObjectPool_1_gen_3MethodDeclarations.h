﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.UI.ObjectPool`1<System.Object>
struct ObjectPool_1_t3176;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t3175;
// System.Object
struct Object_t;

// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
extern "C" void ObjectPool_1__ctor_m15674_gshared (ObjectPool_1_t3176 * __this, UnityAction_1_t3175 * ___actionOnGet, UnityAction_1_t3175 * ___actionOnRelease, const MethodInfo* method);
#define ObjectPool_1__ctor_m15674(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t3176 *, UnityAction_1_t3175 *, UnityAction_1_t3175 *, const MethodInfo*))ObjectPool_1__ctor_m15674_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countAll()
extern "C" int32_t ObjectPool_1_get_countAll_m15676_gshared (ObjectPool_1_t3176 * __this, const MethodInfo* method);
#define ObjectPool_1_get_countAll_m15676(__this, method) (( int32_t (*) (ObjectPool_1_t3176 *, const MethodInfo*))ObjectPool_1_get_countAll_m15676_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::set_countAll(System.Int32)
extern "C" void ObjectPool_1_set_countAll_m15678_gshared (ObjectPool_1_t3176 * __this, int32_t ___value, const MethodInfo* method);
#define ObjectPool_1_set_countAll_m15678(__this, ___value, method) (( void (*) (ObjectPool_1_t3176 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m15678_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countActive()
extern "C" int32_t ObjectPool_1_get_countActive_m15680_gshared (ObjectPool_1_t3176 * __this, const MethodInfo* method);
#define ObjectPool_1_get_countActive_m15680(__this, method) (( int32_t (*) (ObjectPool_1_t3176 *, const MethodInfo*))ObjectPool_1_get_countActive_m15680_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countInactive()
extern "C" int32_t ObjectPool_1_get_countInactive_m15682_gshared (ObjectPool_1_t3176 * __this, const MethodInfo* method);
#define ObjectPool_1_get_countInactive_m15682(__this, method) (( int32_t (*) (ObjectPool_1_t3176 *, const MethodInfo*))ObjectPool_1_get_countInactive_m15682_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Object>::Get()
extern "C" Object_t * ObjectPool_1_Get_m15684_gshared (ObjectPool_1_t3176 * __this, const MethodInfo* method);
#define ObjectPool_1_Get_m15684(__this, method) (( Object_t * (*) (ObjectPool_1_t3176 *, const MethodInfo*))ObjectPool_1_Get_m15684_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::Release(T)
extern "C" void ObjectPool_1_Release_m15686_gshared (ObjectPool_1_t3176 * __this, Object_t * ___element, const MethodInfo* method);
#define ObjectPool_1_Release_m15686(__this, ___element, method) (( void (*) (ObjectPool_1_t3176 *, Object_t *, const MethodInfo*))ObjectPool_1_Release_m15686_gshared)(__this, ___element, method)
