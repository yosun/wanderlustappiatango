﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttribute.h"
// System.CLSCompliantAttribute
#include "mscorlib_System_CLSCompliantAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToA.h"
// System.Runtime.CompilerServices.InternalsVisibleToAttribute
#include "mscorlib_System_Runtime_CompilerServices_InternalsVisibleToAMethodDeclarations.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttribute.h"
// System.Reflection.AssemblyKeyFileAttribute
#include "mscorlib_System_Reflection_AssemblyKeyFileAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttribute.h"
// System.Reflection.AssemblyDelaySignAttribute
#include "mscorlib_System_Reflection_AssemblyDelaySignAttributeMethodDeclarations.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttribute.h"
// System.Resources.NeutralResourcesLanguageAttribute
#include "mscorlib_System_Resources_NeutralResourcesLanguageAttributeMethodDeclarations.h"
extern TypeInfo* AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var;
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var;
extern TypeInfo* InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyKeyFileAttribute_t1591_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDelaySignAttribute_t1592_il2cpp_TypeInfo_var;
extern TypeInfo* NeutralResourcesLanguageAttribute_t1594_il2cpp_TypeInfo_var;
void g_Mono_Security_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		AssemblyProductAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(494);
		AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(497);
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(493);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1282);
		AssemblyKeyFileAttribute_t1591_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3116);
		AssemblyDelaySignAttribute_t1592_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3117);
		NeutralResourcesLanguageAttribute_t1594_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3119);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 12;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AssemblyCopyrightAttribute_t490 * tmp;
		tmp = (AssemblyCopyrightAttribute_t490 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m2453(tmp, il2cpp_codegen_string_new_wrapper("(c) 2003-2004 Various Authors"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t486 * tmp;
		tmp = (AssemblyDescriptionAttribute_t486 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m2449(tmp, il2cpp_codegen_string_new_wrapper("Mono.Security.dll"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t489 * tmp;
		tmp = (AssemblyProductAttribute_t489 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t489_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m2452(tmp, il2cpp_codegen_string_new_wrapper("MONO CLI"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t492 * tmp;
		tmp = (AssemblyTitleAttribute_t492 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m2455(tmp, il2cpp_codegen_string_new_wrapper("Mono.Security.dll"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, true, NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t488 * tmp;
		tmp = (AssemblyCompanyAttribute_t488 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m2451(tmp, il2cpp_codegen_string_new_wrapper("MONO development team"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t167 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t167 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m534(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m535(tmp, true, NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		InternalsVisibleToAttribute_t902 * tmp;
		tmp = (InternalsVisibleToAttribute_t902 *)il2cpp_codegen_object_new (InternalsVisibleToAttribute_t902_il2cpp_TypeInfo_var);
		InternalsVisibleToAttribute__ctor_m4697(tmp, il2cpp_codegen_string_new_wrapper("System, PublicKey=00240000048000009400000006020000002400005253413100040000010001008D56C76F9E8649383049F383C44BE0EC204181822A6C31CF5EB7EF486944D032188EA1D3920763712CCB12D75FB77E9811149E6148E5D32FBAAB37611C1878DDC19E20EF135D0CB2CFF2BFEC3D115810C3D9069638FE4BE215DBF795861920E5AB6F7DB2E2CEEF136AC23D5DD2BF031700AEC232F6C6B1C785B4305C123B37AB"), NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		AssemblyKeyFileAttribute_t1591 * tmp;
		tmp = (AssemblyKeyFileAttribute_t1591 *)il2cpp_codegen_object_new (AssemblyKeyFileAttribute_t1591_il2cpp_TypeInfo_var);
		AssemblyKeyFileAttribute__ctor_m7292(tmp, il2cpp_codegen_string_new_wrapper("../mono.pub"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDelaySignAttribute_t1592 * tmp;
		tmp = (AssemblyDelaySignAttribute_t1592 *)il2cpp_codegen_object_new (AssemblyDelaySignAttribute_t1592_il2cpp_TypeInfo_var);
		AssemblyDelaySignAttribute__ctor_m7293(tmp, true, NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
	{
		NeutralResourcesLanguageAttribute_t1594 * tmp;
		tmp = (NeutralResourcesLanguageAttribute_t1594 *)il2cpp_codegen_object_new (NeutralResourcesLanguageAttribute_t1594_il2cpp_TypeInfo_var);
		NeutralResourcesLanguageAttribute__ctor_m7295(tmp, il2cpp_codegen_string_new_wrapper("en-US"), NULL);
		cache->attributes[11] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger__ctor_m7409(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger__ctor_m7411(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger__ctor_m7413(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_SetBit_m7420(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_SetBit_m7421(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_ToString_m7424(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_ToString_m7425(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_op_Implicit_m7435(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_op_Modulus_m7439(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_op_Equality_m7445(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_op_Inequality_m7446(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var;
void ModulusRing_t1666_CustomAttributesCacheGenerator_ModulusRing_Pow_m7393(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3118);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CLSCompliantAttribute_t1593 * tmp;
		tmp = (CLSCompliantAttribute_t1593 *)il2cpp_codegen_object_new (CLSCompliantAttribute_t1593_il2cpp_TypeInfo_var);
		CLSCompliantAttribute__ctor_m7294(tmp, false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void ASN1_t1675_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PKCS12_t1701_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PKCS12_t1701_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PKCS12_t1701_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PKCS12_t1701_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void X509Certificate_t1705_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void X509Certificate_t1705_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map10(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void X509Certificate_t1705_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map11(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void X509CertificateCollection_t1700_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void X509ChainStatusFlags_t1709_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void X509Crl_t1711_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void X509Crl_t1711_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map13(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void X509ExtensionCollection_t1704_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ExtendedKeyUsageExtension_t1718_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map14(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void KeyUsages_t1720_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void CertTypes_t1722_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void CipherSuiteCollection_t1735_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void HttpsClientStream_t1755_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void HttpsClientStream_t1755_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void HttpsClientStream_t1755_CustomAttributesCacheGenerator_HttpsClientStream_U3CHttpsClientStreamU3Em__0_m7925(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void HttpsClientStream_t1755_CustomAttributesCacheGenerator_HttpsClientStream_U3CHttpsClientStreamU3Em__1_m7926(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void RSASslSignatureDeformatter_t1761_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map15(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void RSASslSignatureFormatter_t1763_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map16(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void SecurityProtocolType_t1766_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CPrivateImplementationDetailsU3E_t1805_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_Mono_Security_Assembly_AttributeGenerators[39] = 
{
	NULL,
	g_Mono_Security_Assembly_CustomAttributesCacheGenerator,
	BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger__ctor_m7409,
	BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger__ctor_m7411,
	BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger__ctor_m7413,
	BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_SetBit_m7420,
	BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_SetBit_m7421,
	BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_ToString_m7424,
	BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_ToString_m7425,
	BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_op_Implicit_m7435,
	BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_op_Modulus_m7439,
	BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_op_Equality_m7445,
	BigInteger_t1665_CustomAttributesCacheGenerator_BigInteger_op_Inequality_m7446,
	ModulusRing_t1666_CustomAttributesCacheGenerator_ModulusRing_Pow_m7393,
	ASN1_t1675_CustomAttributesCacheGenerator,
	PKCS12_t1701_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map5,
	PKCS12_t1701_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map6,
	PKCS12_t1701_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map7,
	PKCS12_t1701_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map8,
	X509Certificate_t1705_CustomAttributesCacheGenerator_U3CU3Ef__switchU24mapF,
	X509Certificate_t1705_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map10,
	X509Certificate_t1705_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map11,
	X509CertificateCollection_t1700_CustomAttributesCacheGenerator,
	X509ChainStatusFlags_t1709_CustomAttributesCacheGenerator,
	X509Crl_t1711_CustomAttributesCacheGenerator,
	X509Crl_t1711_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map13,
	X509ExtensionCollection_t1704_CustomAttributesCacheGenerator,
	ExtendedKeyUsageExtension_t1718_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map14,
	KeyUsages_t1720_CustomAttributesCacheGenerator,
	CertTypes_t1722_CustomAttributesCacheGenerator,
	CipherSuiteCollection_t1735_CustomAttributesCacheGenerator,
	HttpsClientStream_t1755_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	HttpsClientStream_t1755_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	HttpsClientStream_t1755_CustomAttributesCacheGenerator_HttpsClientStream_U3CHttpsClientStreamU3Em__0_m7925,
	HttpsClientStream_t1755_CustomAttributesCacheGenerator_HttpsClientStream_U3CHttpsClientStreamU3Em__1_m7926,
	RSASslSignatureDeformatter_t1761_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map15,
	RSASslSignatureFormatter_t1763_CustomAttributesCacheGenerator_U3CU3Ef__switchU24map16,
	SecurityProtocolType_t1766_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t1805_CustomAttributesCacheGenerator,
};
