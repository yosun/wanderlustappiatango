﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Raycaster
struct Raycaster_t26;
// UnityEngine.Camera
struct Camera_t3;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void Raycaster::.ctor()
extern "C" void Raycaster__ctor_m95 (Raycaster_t26 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Raycaster::RaycastIt(UnityEngine.Camera,UnityEngine.Vector2,System.Single)
extern "C" void Raycaster_RaycastIt_m96 (Raycaster_t26 * __this, Camera_t3 * ___cam, Vector2_t19  ___pos, float ___distance, const MethodInfo* method) IL2CPP_METHOD_ATTR;
