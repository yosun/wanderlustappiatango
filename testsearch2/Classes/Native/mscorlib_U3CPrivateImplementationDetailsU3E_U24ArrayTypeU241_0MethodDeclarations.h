﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// <PrivateImplementationDetails>/$ArrayType$120
struct U24ArrayTypeU24120_t2571;
struct U24ArrayTypeU24120_t2571_marshaled;

void U24ArrayTypeU24120_t2571_marshal(const U24ArrayTypeU24120_t2571& unmarshaled, U24ArrayTypeU24120_t2571_marshaled& marshaled);
void U24ArrayTypeU24120_t2571_marshal_back(const U24ArrayTypeU24120_t2571_marshaled& marshaled, U24ArrayTypeU24120_t2571& unmarshaled);
void U24ArrayTypeU24120_t2571_marshal_cleanup(U24ArrayTypeU24120_t2571_marshaled& marshaled);
