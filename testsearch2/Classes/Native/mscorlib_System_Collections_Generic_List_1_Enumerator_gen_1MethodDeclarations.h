﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>
struct Enumerator_t801;
// System.Object
struct Object_t;
// Vuforia.ICloudRecoEventHandler
struct ICloudRecoEventHandler_t767;
// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
struct List_1_t581;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m18603(__this, ___l, method) (( void (*) (Enumerator_t801 *, List_1_t581 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m18604(__this, method) (( Object_t * (*) (Enumerator_t801 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::Dispose()
#define Enumerator_Dispose_m18605(__this, method) (( void (*) (Enumerator_t801 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::VerifyState()
#define Enumerator_VerifyState_m18606(__this, method) (( void (*) (Enumerator_t801 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::MoveNext()
#define Enumerator_MoveNext_m4339(__this, method) (( bool (*) (Enumerator_t801 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.ICloudRecoEventHandler>::get_Current()
#define Enumerator_get_Current_m4338(__this, method) (( Object_t * (*) (Enumerator_t801 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
