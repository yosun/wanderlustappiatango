﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Capture>
struct InternalEnumerator_1_t3964;
// System.Object
struct Object_t;
// System.Text.RegularExpressions.Capture
struct Capture_t1929;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Capture>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m27377(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3964 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14882_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Capture>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27378(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3964 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14884_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Capture>::Dispose()
#define InternalEnumerator_1_Dispose_m27379(__this, method) (( void (*) (InternalEnumerator_1_t3964 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14886_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Capture>::MoveNext()
#define InternalEnumerator_1_MoveNext_m27380(__this, method) (( bool (*) (InternalEnumerator_1_t3964 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14888_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Capture>::get_Current()
#define InternalEnumerator_1_get_Current_m27381(__this, method) (( Capture_t1929 * (*) (InternalEnumerator_1_t3964 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14890_gshared)(__this, method)
