﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.EyewearComponentFactory
struct EyewearComponentFactory_t565;
// Vuforia.IEyewearComponentFactory
struct IEyewearComponentFactory_t564;

// Vuforia.IEyewearComponentFactory Vuforia.EyewearComponentFactory::get_Instance()
extern "C" Object_t * EyewearComponentFactory_get_Instance_m2637 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.EyewearComponentFactory::set_Instance(Vuforia.IEyewearComponentFactory)
extern "C" void EyewearComponentFactory_set_Instance_m2638 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.EyewearComponentFactory::.ctor()
extern "C" void EyewearComponentFactory__ctor_m2639 (EyewearComponentFactory_t565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
