﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.StateManager
struct StateManager_t719;
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour>
struct IEnumerable_1_t788;
// Vuforia.Trackable
struct Trackable_t571;
// Vuforia.WordManager
struct WordManager_t693;

// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManager::GetActiveTrackableBehaviours()
// System.Collections.Generic.IEnumerable`1<Vuforia.TrackableBehaviour> Vuforia.StateManager::GetTrackableBehaviours()
// System.Void Vuforia.StateManager::DestroyTrackableBehavioursForTrackable(Vuforia.Trackable,System.Boolean)
// Vuforia.WordManager Vuforia.StateManager::GetWordManager()
// System.Void Vuforia.StateManager::.ctor()
extern "C" void StateManager__ctor_m4027 (StateManager_t719 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
