﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>
struct U3CProcessMatchResponseU3Ec__Iterator0_1_t3847;
// System.Object
struct Object_t;

// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::.ctor()
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m25967_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t3847 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m25967(__this, method) (( void (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t3847 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m25967_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m25968_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t3847 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m25968(__this, method) (( Object_t * (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t3847 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m25968_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m25969_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t3847 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m25969(__this, method) (( Object_t * (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t3847 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m25969_gshared)(__this, method)
// System.Boolean UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::MoveNext()
extern "C" bool U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m25970_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t3847 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m25970(__this, method) (( bool (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t3847 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m25970_gshared)(__this, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::Dispose()
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m25971_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t3847 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m25971(__this, method) (( void (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t3847 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m25971_gshared)(__this, method)
