﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.SHA1Internal
struct SHA1Internal_t2423;
// System.Security.Cryptography.SHA1
#include "mscorlib_System_Security_Cryptography_SHA1.h"
// System.Security.Cryptography.SHA1CryptoServiceProvider
struct  SHA1CryptoServiceProvider_t2424  : public SHA1_t1821
{
	// System.Security.Cryptography.SHA1Internal System.Security.Cryptography.SHA1CryptoServiceProvider::sha
	SHA1Internal_t2423 * ___sha_4;
};
