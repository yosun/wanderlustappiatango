﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Nullable`1<System.Boolean>
struct Nullable_1_t126;
// System.Object
struct Object_t;
// System.String
struct String_t;
// System.Nullable`1<System.Boolean>
#include "mscorlib_System_Nullable_1_gen.h"

// System.Void System.Nullable`1<System.Boolean>::.ctor(T)
// System.Nullable`1<System.Byte>
#include "mscorlib_System_Nullable_1_gen_2MethodDeclarations.h"
#define Nullable_1__ctor_m385(__this, ___value, method) (( void (*) (Nullable_1_t126 *, bool, const MethodInfo*))Nullable_1__ctor_m15177_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
#define Nullable_1_get_HasValue_m5574(__this, method) (( bool (*) (Nullable_1_t126 *, const MethodInfo*))Nullable_1_get_HasValue_m15178_gshared)(__this, method)
// T System.Nullable`1<System.Boolean>::get_Value()
#define Nullable_1_get_Value_m5575(__this, method) (( bool (*) (Nullable_1_t126 *, const MethodInfo*))Nullable_1_get_Value_m15179_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Object)
#define Nullable_1_Equals_m15180(__this, ___other, method) (( bool (*) (Nullable_1_t126 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m15181_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m15182(__this, ___other, method) (( bool (*) (Nullable_1_t126 *, Nullable_1_t126 , const MethodInfo*))Nullable_1_Equals_m15183_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.Boolean>::GetHashCode()
#define Nullable_1_GetHashCode_m15184(__this, method) (( int32_t (*) (Nullable_1_t126 *, const MethodInfo*))Nullable_1_GetHashCode_m15185_gshared)(__this, method)
// System.String System.Nullable`1<System.Boolean>::ToString()
#define Nullable_1_ToString_m15186(__this, method) (( String_t* (*) (Nullable_1_t126 *, const MethodInfo*))Nullable_1_ToString_m15187_gshared)(__this, method)
