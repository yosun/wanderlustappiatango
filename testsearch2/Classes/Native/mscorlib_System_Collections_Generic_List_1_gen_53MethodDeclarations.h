﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1208;
// System.Object
struct Object_t;
// System.Byte[]
struct ByteU5BU5D_t622;
// System.Collections.Generic.IEnumerable`1<System.Byte[]>
struct IEnumerable_1_t4342;
// System.Collections.Generic.IEnumerator`1<System.Byte[]>
struct IEnumerator_1_t4343;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<System.Byte[]>
struct ICollection_1_t4344;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Byte[]>
struct ReadOnlyCollection_1_t3756;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_t1828;
// System.Predicate`1<System.Byte[]>
struct Predicate_1_t3757;
// System.Comparison`1<System.Byte[]>
struct Comparison_1_t3759;
// System.Collections.Generic.List`1/Enumerator<System.Byte[]>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_55.h"

// System.Void System.Collections.Generic.List`1<System.Byte[]>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m6968(__this, method) (( void (*) (List_1_t1208 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m24787(__this, ___collection, method) (( void (*) (List_1_t1208 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::.ctor(System.Int32)
#define List_1__ctor_m24788(__this, ___capacity, method) (( void (*) (List_1_t1208 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::.cctor()
#define List_1__cctor_m24789(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Byte[]>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m24790(__this, method) (( Object_t* (*) (List_1_t1208 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m24791(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1208 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m24792(__this, method) (( Object_t * (*) (List_1_t1208 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m24793(__this, ___item, method) (( int32_t (*) (List_1_t1208 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m24794(__this, ___item, method) (( bool (*) (List_1_t1208 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m24795(__this, ___item, method) (( int32_t (*) (List_1_t1208 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m24796(__this, ___index, ___item, method) (( void (*) (List_1_t1208 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m24797(__this, ___item, method) (( void (*) (List_1_t1208 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m24798(__this, method) (( bool (*) (List_1_t1208 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m24799(__this, method) (( bool (*) (List_1_t1208 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Byte[]>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m24800(__this, method) (( Object_t * (*) (List_1_t1208 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m24801(__this, method) (( bool (*) (List_1_t1208 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m24802(__this, method) (( bool (*) (List_1_t1208 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m24803(__this, ___index, method) (( Object_t * (*) (List_1_t1208 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m24804(__this, ___index, ___value, method) (( void (*) (List_1_t1208 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Add(T)
#define List_1_Add_m24805(__this, ___item, method) (( void (*) (List_1_t1208 *, ByteU5BU5D_t622*, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m24806(__this, ___newCount, method) (( void (*) (List_1_t1208 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m24807(__this, ___collection, method) (( void (*) (List_1_t1208 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m24808(__this, ___enumerable, method) (( void (*) (List_1_t1208 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m24809(__this, ___collection, method) (( void (*) (List_1_t1208 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Byte[]>::AsReadOnly()
#define List_1_AsReadOnly_m24810(__this, method) (( ReadOnlyCollection_1_t3756 * (*) (List_1_t1208 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Clear()
#define List_1_Clear_m24811(__this, method) (( void (*) (List_1_t1208 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::Contains(T)
#define List_1_Contains_m24812(__this, ___item, method) (( bool (*) (List_1_t1208 *, ByteU5BU5D_t622*, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m24813(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1208 *, ByteU5BU5DU5BU5D_t1828*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Byte[]>::Find(System.Predicate`1<T>)
#define List_1_Find_m24814(__this, ___match, method) (( ByteU5BU5D_t622* (*) (List_1_t1208 *, Predicate_1_t3757 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m24815(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3757 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m24816(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1208 *, int32_t, int32_t, Predicate_1_t3757 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Byte[]>::GetEnumerator()
#define List_1_GetEnumerator_m24817(__this, method) (( Enumerator_t3758  (*) (List_1_t1208 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::IndexOf(T)
#define List_1_IndexOf_m24818(__this, ___item, method) (( int32_t (*) (List_1_t1208 *, ByteU5BU5D_t622*, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m24819(__this, ___start, ___delta, method) (( void (*) (List_1_t1208 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m24820(__this, ___index, method) (( void (*) (List_1_t1208 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Insert(System.Int32,T)
#define List_1_Insert_m24821(__this, ___index, ___item, method) (( void (*) (List_1_t1208 *, int32_t, ByteU5BU5D_t622*, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m24822(__this, ___collection, method) (( void (*) (List_1_t1208 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Byte[]>::Remove(T)
#define List_1_Remove_m24823(__this, ___item, method) (( bool (*) (List_1_t1208 *, ByteU5BU5D_t622*, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m24824(__this, ___match, method) (( int32_t (*) (List_1_t1208 *, Predicate_1_t3757 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m24825(__this, ___index, method) (( void (*) (List_1_t1208 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Reverse()
#define List_1_Reverse_m24826(__this, method) (( void (*) (List_1_t1208 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Sort()
#define List_1_Sort_m24827(__this, method) (( void (*) (List_1_t1208 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m24828(__this, ___comparison, method) (( void (*) (List_1_t1208 *, Comparison_1_t3759 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Byte[]>::ToArray()
#define List_1_ToArray_m24829(__this, method) (( ByteU5BU5DU5BU5D_t1828* (*) (List_1_t1208 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::TrimExcess()
#define List_1_TrimExcess_m24830(__this, method) (( void (*) (List_1_t1208 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::get_Capacity()
#define List_1_get_Capacity_m24831(__this, method) (( int32_t (*) (List_1_t1208 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m24832(__this, ___value, method) (( void (*) (List_1_t1208 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Byte[]>::get_Count()
#define List_1_get_Count_m24833(__this, method) (( int32_t (*) (List_1_t1208 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Byte[]>::get_Item(System.Int32)
#define List_1_get_Item_m24834(__this, ___index, method) (( ByteU5BU5D_t622* (*) (List_1_t1208 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Byte[]>::set_Item(System.Int32,T)
#define List_1_set_Item_m24835(__this, ___index, ___value, method) (( void (*) (List_1_t1208 *, int32_t, ByteU5BU5D_t622*, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
