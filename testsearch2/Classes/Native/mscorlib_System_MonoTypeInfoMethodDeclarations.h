﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.MonoTypeInfo
struct MonoTypeInfo_t2541;

// System.Void System.MonoTypeInfo::.ctor()
extern "C" void MonoTypeInfo__ctor_m13692 (MonoTypeInfo_t2541 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
