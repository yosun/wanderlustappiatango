﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.IList`1<DG.Tweening.Core.ABSSequentiable>
struct IList_1_t3688;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Core.ABSSequentiable>
struct  ReadOnlyCollection_1_t3689  : public Object_t
{
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<DG.Tweening.Core.ABSSequentiable>::list
	Object_t* ___list_0;
};
