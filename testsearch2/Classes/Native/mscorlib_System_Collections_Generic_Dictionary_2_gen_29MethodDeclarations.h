﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct Dictionary_2_t1421;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t1377;
// System.Collections.Generic.ICollection`1<SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct ICollection_1_t4403;
// System.Object
struct Object_t;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct GetDelegate_t1290;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct KeyCollection_t3867;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct ValueCollection_t3868;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3093;
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct IDictionary_2_t1382;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>[]
struct KeyValuePair_2U5BU5D_t4404;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>
struct IEnumerator_1_t1429;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__37.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor()
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_33MethodDeclarations.h"
#define Dictionary_2__ctor_m7028(__this, method) (( void (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2__ctor_m14896_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m26348(__this, ___comparer, method) (( void (*) (Dictionary_2_t1421 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14898_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m26349(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1421 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14900_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Int32)
#define Dictionary_2__ctor_m26350(__this, ___capacity, method) (( void (*) (Dictionary_2_t1421 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m14902_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m26351(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1421 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m14904_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m26352(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1421 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m14906_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m26353(__this, method) (( Object_t* (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m14908_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m26354(__this, method) (( Object_t* (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m14910_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m26355(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1421 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m14912_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m26356(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1421 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m14914_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m26357(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1421 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m14916_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m26358(__this, ___key, method) (( bool (*) (Dictionary_2_t1421 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m14918_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m26359(__this, ___key, method) (( void (*) (Dictionary_2_t1421 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m14920_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m26360(__this, method) (( bool (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m14922_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m26361(__this, method) (( Object_t * (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m14924_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m26362(__this, method) (( bool (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m14926_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m26363(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1421 *, KeyValuePair_2_t1427 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m14928_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m26364(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1421 *, KeyValuePair_2_t1427 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m14930_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m26365(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1421 *, KeyValuePair_2U5BU5D_t4404*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m14932_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m26366(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1421 *, KeyValuePair_2_t1427 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m14934_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m26367(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1421 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m14936_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m26368(__this, method) (( Object_t * (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m14938_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m26369(__this, method) (( Object_t* (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m14940_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m26370(__this, method) (( Object_t * (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m14942_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Count()
#define Dictionary_2_get_Count_m26371(__this, method) (( int32_t (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_get_Count_m14944_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Item(TKey)
#define Dictionary_2_get_Item_m26372(__this, ___key, method) (( GetDelegate_t1290 * (*) (Dictionary_2_t1421 *, String_t*, const MethodInfo*))Dictionary_2_get_Item_m14946_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m26373(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1421 *, String_t*, GetDelegate_t1290 *, const MethodInfo*))Dictionary_2_set_Item_m14948_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m26374(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1421 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m14950_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m26375(__this, ___size, method) (( void (*) (Dictionary_2_t1421 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m14952_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m26376(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1421 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m14954_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m26377(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1427  (*) (Object_t * /* static, unused */, String_t*, GetDelegate_t1290 *, const MethodInfo*))Dictionary_2_make_pair_m14956_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m26378(__this /* static, unused */, ___key, ___value, method) (( String_t* (*) (Object_t * /* static, unused */, String_t*, GetDelegate_t1290 *, const MethodInfo*))Dictionary_2_pick_key_m14958_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m26379(__this /* static, unused */, ___key, ___value, method) (( GetDelegate_t1290 * (*) (Object_t * /* static, unused */, String_t*, GetDelegate_t1290 *, const MethodInfo*))Dictionary_2_pick_value_m14960_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m26380(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1421 *, KeyValuePair_2U5BU5D_t4404*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m14962_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Resize()
#define Dictionary_2_Resize_m26381(__this, method) (( void (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_Resize_m14964_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Add(TKey,TValue)
#define Dictionary_2_Add_m26382(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1421 *, String_t*, GetDelegate_t1290 *, const MethodInfo*))Dictionary_2_Add_m14966_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Clear()
#define Dictionary_2_Clear_m26383(__this, method) (( void (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_Clear_m14968_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m26384(__this, ___key, method) (( bool (*) (Dictionary_2_t1421 *, String_t*, const MethodInfo*))Dictionary_2_ContainsKey_m14970_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m26385(__this, ___value, method) (( bool (*) (Dictionary_2_t1421 *, GetDelegate_t1290 *, const MethodInfo*))Dictionary_2_ContainsValue_m14972_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m26386(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1421 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m14974_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m26387(__this, ___sender, method) (( void (*) (Dictionary_2_t1421 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m14976_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::Remove(TKey)
#define Dictionary_2_Remove_m26388(__this, ___key, method) (( bool (*) (Dictionary_2_t1421 *, String_t*, const MethodInfo*))Dictionary_2_Remove_m14978_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m26389(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1421 *, String_t*, GetDelegate_t1290 **, const MethodInfo*))Dictionary_2_TryGetValue_m14980_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Keys()
#define Dictionary_2_get_Keys_m26390(__this, method) (( KeyCollection_t3867 * (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_get_Keys_m14982_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::get_Values()
#define Dictionary_2_get_Values_m26391(__this, method) (( ValueCollection_t3868 * (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_get_Values_m14984_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m26392(__this, ___key, method) (( String_t* (*) (Dictionary_2_t1421 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m14986_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m26393(__this, ___value, method) (( GetDelegate_t1290 * (*) (Dictionary_2_t1421 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m14988_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m26394(__this, ___pair, method) (( bool (*) (Dictionary_2_t1421 *, KeyValuePair_2_t1427 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m14990_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m26395(__this, method) (( Enumerator_t3869  (*) (Dictionary_2_t1421 *, const MethodInfo*))Dictionary_2_GetEnumerator_m14992_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m26396(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, String_t*, GetDelegate_t1290 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m14994_gshared)(__this /* static, unused */, ___key, ___value, method)
