﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct Enumerator_t3638;
// System.Object
struct Object_t;
// Vuforia.VirtualButtonAbstractBehaviour
struct VirtualButtonAbstractBehaviour_t94;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
struct Dictionary_2_t744;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_31.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7MethodDeclarations.h"
#define Enumerator__ctor_m22887(__this, ___dictionary, method) (( void (*) (Enumerator_t3638 *, Dictionary_2_t744 *, const MethodInfo*))Enumerator__ctor_m16521_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22888(__this, method) (( Object_t * (*) (Enumerator_t3638 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16522_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m22889(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3638 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16523_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m22890(__this, method) (( Object_t * (*) (Enumerator_t3638 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16524_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m22891(__this, method) (( Object_t * (*) (Enumerator_t3638 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16525_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m22892(__this, method) (( bool (*) (Enumerator_t3638 *, const MethodInfo*))Enumerator_MoveNext_m16526_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m22893(__this, method) (( KeyValuePair_2_t3636  (*) (Enumerator_t3638 *, const MethodInfo*))Enumerator_get_Current_m16527_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m22894(__this, method) (( int32_t (*) (Enumerator_t3638 *, const MethodInfo*))Enumerator_get_CurrentKey_m16528_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m22895(__this, method) (( VirtualButtonAbstractBehaviour_t94 * (*) (Enumerator_t3638 *, const MethodInfo*))Enumerator_get_CurrentValue_m16529_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m22896(__this, method) (( void (*) (Enumerator_t3638 *, const MethodInfo*))Enumerator_VerifyState_m16530_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m22897(__this, method) (( void (*) (Enumerator_t3638 *, const MethodInfo*))Enumerator_VerifyCurrent_m16531_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.VirtualButtonAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m22898(__this, method) (( void (*) (Enumerator_t3638 *, const MethodInfo*))Enumerator_Dispose_m16532_gshared)(__this, method)
