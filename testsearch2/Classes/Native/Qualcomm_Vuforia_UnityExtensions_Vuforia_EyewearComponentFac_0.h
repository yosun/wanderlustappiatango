﻿#pragma once
#include <stdint.h>
// Vuforia.IEyewearComponentFactory
struct IEyewearComponentFactory_t564;
// System.Object
#include "mscorlib_System_Object.h"
// Vuforia.EyewearComponentFactory
struct  EyewearComponentFactory_t565  : public Object_t
{
};
struct EyewearComponentFactory_t565_StaticFields{
	// Vuforia.IEyewearComponentFactory Vuforia.EyewearComponentFactory::sInstance
	Object_t * ___sInstance_0;
};
