﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.SHA384
struct SHA384_t2427;

// System.Void System.Security.Cryptography.SHA384::.ctor()
extern "C" void SHA384__ctor_m12764 (SHA384_t2427 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
