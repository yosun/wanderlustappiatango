﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>
struct Enumerator_t831;
// System.Object
struct Object_t;
// Vuforia.ReconstructionAbstractBehaviour
struct ReconstructionAbstractBehaviour_t76;
// System.Collections.Generic.List`1<Vuforia.ReconstructionAbstractBehaviour>
struct List_1_t677;

// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::.ctor(System.Collections.Generic.List`1<T>)
// System.Collections.Generic.List`1/Enumerator<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25MethodDeclarations.h"
#define Enumerator__ctor_m20136(__this, ___l, method) (( void (*) (Enumerator_t831 *, List_1_t677 *, const MethodInfo*))Enumerator__ctor_m15329_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20137(__this, method) (( Object_t * (*) (Enumerator_t831 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::Dispose()
#define Enumerator_Dispose_m20138(__this, method) (( void (*) (Enumerator_t831 *, const MethodInfo*))Enumerator_Dispose_m15331_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::VerifyState()
#define Enumerator_VerifyState_m20139(__this, method) (( void (*) (Enumerator_t831 *, const MethodInfo*))Enumerator_VerifyState_m15332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::MoveNext()
#define Enumerator_MoveNext_m4463(__this, method) (( bool (*) (Enumerator_t831 *, const MethodInfo*))Enumerator_MoveNext_m15333_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Vuforia.ReconstructionAbstractBehaviour>::get_Current()
#define Enumerator_get_Current_m4462(__this, method) (( ReconstructionAbstractBehaviour_t76 * (*) (Enumerator_t831 *, const MethodInfo*))Enumerator_get_Current_m15334_gshared)(__this, method)
