﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.EyewearComponentFactory/NullEyewearComponentFactory
struct NullEyewearComponentFactory_t563;
// System.Type
struct Type_t;

// System.Type Vuforia.EyewearComponentFactory/NullEyewearComponentFactory::GetOVRInitControllerType()
extern "C" Type_t * NullEyewearComponentFactory_GetOVRInitControllerType_m2635 (NullEyewearComponentFactory_t563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.EyewearComponentFactory/NullEyewearComponentFactory::.ctor()
extern "C" void NullEyewearComponentFactory__ctor_m2636 (NullEyewearComponentFactory_t563 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
