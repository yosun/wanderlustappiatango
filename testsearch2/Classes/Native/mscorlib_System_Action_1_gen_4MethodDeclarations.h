﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Action`1<Vuforia.SmartTerrainInitializationInfo>
struct Action_1_t712;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.SmartTerrainInitializationInfo
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_SmartTerrainInitial.h"

// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Action_1__ctor_m21229_gshared (Action_1_t712 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Action_1__ctor_m21229(__this, ___object, ___method, method) (( void (*) (Action_1_t712 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m21229_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::Invoke(T)
extern "C" void Action_1_Invoke_m21230_gshared (Action_1_t712 * __this, SmartTerrainInitializationInfo_t594  ___obj, const MethodInfo* method);
#define Action_1_Invoke_m21230(__this, ___obj, method) (( void (*) (Action_1_t712 *, SmartTerrainInitializationInfo_t594 , const MethodInfo*))Action_1_Invoke_m21230_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<Vuforia.SmartTerrainInitializationInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Action_1_BeginInvoke_m21231_gshared (Action_1_t712 * __this, SmartTerrainInitializationInfo_t594  ___obj, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Action_1_BeginInvoke_m21231(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t712 *, SmartTerrainInitializationInfo_t594 , AsyncCallback_t312 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m21231_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<Vuforia.SmartTerrainInitializationInfo>::EndInvoke(System.IAsyncResult)
extern "C" void Action_1_EndInvoke_m21232_gshared (Action_1_t712 * __this, Object_t * ___result, const MethodInfo* method);
#define Action_1_EndInvoke_m21232(__this, ___result, method) (( void (*) (Action_1_t712 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m21232_gshared)(__this, ___result, method)
