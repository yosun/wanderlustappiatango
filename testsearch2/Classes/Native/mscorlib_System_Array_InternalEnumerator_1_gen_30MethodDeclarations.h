﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.Color>
struct InternalEnumerator_1_t3426;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// UnityEngine.Color
#include "UnityEngine_UnityEngine_Color.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m19409_gshared (InternalEnumerator_1_t3426 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m19409(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3426 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m19409_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19410_gshared (InternalEnumerator_1_t3426 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19410(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3426 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m19410_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m19411_gshared (InternalEnumerator_1_t3426 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m19411(__this, method) (( void (*) (InternalEnumerator_1_t3426 *, const MethodInfo*))InternalEnumerator_1_Dispose_m19411_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m19412_gshared (InternalEnumerator_1_t3426 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m19412(__this, method) (( bool (*) (InternalEnumerator_1_t3426 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m19412_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Color>::get_Current()
extern "C" Color_t98  InternalEnumerator_1_get_Current_m19413_gshared (InternalEnumerator_1_t3426 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m19413(__this, method) (( Color_t98  (*) (InternalEnumerator_1_t3426 *, const MethodInfo*))InternalEnumerator_1_get_Current_m19413_gshared)(__this, method)
