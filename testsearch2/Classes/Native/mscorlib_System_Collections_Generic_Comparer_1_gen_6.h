﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
struct Comparer_1_t3784;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
struct  Comparer_1_t3784  : public Object_t
{
};
struct Comparer_1_t3784_StaticFields{
	// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::_default
	Comparer_1_t3784 * ____default_0;
};
