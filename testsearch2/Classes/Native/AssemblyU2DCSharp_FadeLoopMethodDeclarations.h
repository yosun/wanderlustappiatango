﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// FadeLoop
struct FadeLoop_t20;

// System.Void FadeLoop::.ctor()
extern "C" void FadeLoop__ctor_m23 (FadeLoop_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FadeLoop::Update()
extern "C" void FadeLoop_Update_m24 (FadeLoop_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
