﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// WorldNav
struct WorldNav_t34;

// System.Void WorldNav::.ctor()
extern "C" void WorldNav__ctor_m111 (WorldNav_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::.cctor()
extern "C" void WorldNav__cctor_m112 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::Awake()
extern "C" void WorldNav_Awake_m113 (WorldNav_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::Update()
extern "C" void WorldNav_Update_m114 (WorldNav_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::Walk(System.Int32)
extern "C" void WorldNav_Walk_m115 (Object_t * __this /* static, unused */, int32_t ___dir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::WalkDirTrue(System.Int32)
extern "C" void WorldNav_WalkDirTrue_m116 (WorldNav_t34 * __this, int32_t ___dir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WorldNav::WalkDirFalse(System.Int32)
extern "C" void WorldNav_WalkDirFalse_m117 (WorldNav_t34 * __this, int32_t ___dir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
