﻿#pragma once
#include <stdint.h>
// UnityEngine.RectOffset
struct RectOffset_t377;
// DG.Tweening.Core.DOGetter`1<UnityEngine.RectOffset>
struct DOGetter_1_t1032;
// DG.Tweening.Core.DOSetter`1<UnityEngine.RectOffset>
struct DOSetter_1_t1033;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>
struct ABSTweenPlugin_3_t972;
// DG.Tweening.Tweener
#include "DOTween_DG_Tweening_Tweener.h"
// DG.Tweening.Plugins.Options.NoOptions
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions.h"
// DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>
struct  TweenerCore_3_t1031  : public Tweener_t107
{
	// T2 DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>::startValue
	RectOffset_t377 * ___startValue_53;
	// T2 DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>::endValue
	RectOffset_t377 * ___endValue_54;
	// T2 DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>::changeValue
	RectOffset_t377 * ___changeValue_55;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>::plugOptions
	NoOptions_t939  ___plugOptions_56;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>::getter
	DOGetter_1_t1032 * ___getter_57;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>::setter
	DOSetter_1_t1033 * ___setter_58;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>::tweenPlugin
	ABSTweenPlugin_3_t972 * ___tweenPlugin_59;
};
