﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.List`1<Vuforia.DataSet>
struct List_1_t631;
// System.Object
struct Object_t;
// Vuforia.DataSet
struct DataSet_t600;
// System.Collections.Generic.IEnumerable`1<Vuforia.DataSet>
struct IEnumerable_1_t771;
// System.Collections.Generic.IEnumerator`1<Vuforia.DataSet>
struct IEnumerator_1_t814;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.ICollection`1<Vuforia.DataSet>
struct ICollection_1_t4183;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.DataSet>
struct ReadOnlyCollection_1_t3438;
// Vuforia.DataSet[]
struct DataSetU5BU5D_t3436;
// System.Predicate`1<Vuforia.DataSet>
struct Predicate_1_t3439;
// System.Comparison`1<Vuforia.DataSet>
struct Comparison_1_t3441;
// System.Collections.Generic.List`1/Enumerator<Vuforia.DataSet>
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_46.h"

// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::.ctor()
// System.Collections.Generic.List`1<System.Object>
#include "mscorlib_System_Collections_Generic_List_1_gen_50MethodDeclarations.h"
#define List_1__ctor_m4406(__this, method) (( void (*) (List_1_t631 *, const MethodInfo*))List_1__ctor_m6998_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m19603(__this, ___collection, method) (( void (*) (List_1_t631 *, Object_t*, const MethodInfo*))List_1__ctor_m15260_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::.ctor(System.Int32)
#define List_1__ctor_m19604(__this, ___capacity, method) (( void (*) (List_1_t631 *, int32_t, const MethodInfo*))List_1__ctor_m15262_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::.cctor()
#define List_1__cctor_m19605(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m15264_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m19606(__this, method) (( Object_t* (*) (List_1_t631 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m7233_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m19607(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t631 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m7216_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m19608(__this, method) (( Object_t * (*) (List_1_t631 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m7212_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m19609(__this, ___item, method) (( int32_t (*) (List_1_t631 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m7221_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m19610(__this, ___item, method) (( bool (*) (List_1_t631 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m7223_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m19611(__this, ___item, method) (( int32_t (*) (List_1_t631 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m7224_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m19612(__this, ___index, ___item, method) (( void (*) (List_1_t631 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m7225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m19613(__this, ___item, method) (( void (*) (List_1_t631 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m7226_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m19614(__this, method) (( bool (*) (List_1_t631 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m7228_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m19615(__this, method) (( bool (*) (List_1_t631 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m7214_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m19616(__this, method) (( Object_t * (*) (List_1_t631 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m7215_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m19617(__this, method) (( bool (*) (List_1_t631 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m7217_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m19618(__this, method) (( bool (*) (List_1_t631 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m7218_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m19619(__this, ___index, method) (( Object_t * (*) (List_1_t631 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m7219_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m19620(__this, ___index, ___value, method) (( void (*) (List_1_t631 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m7220_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Add(T)
#define List_1_Add_m19621(__this, ___item, method) (( void (*) (List_1_t631 *, DataSet_t600 *, const MethodInfo*))List_1_Add_m7229_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m19622(__this, ___newCount, method) (( void (*) (List_1_t631 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m15282_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m19623(__this, ___collection, method) (( void (*) (List_1_t631 *, Object_t*, const MethodInfo*))List_1_AddCollection_m15284_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m19624(__this, ___enumerable, method) (( void (*) (List_1_t631 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m15286_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m19625(__this, ___collection, method) (( void (*) (List_1_t631 *, Object_t*, const MethodInfo*))List_1_AddRange_m15287_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Vuforia.DataSet>::AsReadOnly()
#define List_1_AsReadOnly_m19626(__this, method) (( ReadOnlyCollection_1_t3438 * (*) (List_1_t631 *, const MethodInfo*))List_1_AsReadOnly_m15289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Clear()
#define List_1_Clear_m19627(__this, method) (( void (*) (List_1_t631 *, const MethodInfo*))List_1_Clear_m7222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::Contains(T)
#define List_1_Contains_m19628(__this, ___item, method) (( bool (*) (List_1_t631 *, DataSet_t600 *, const MethodInfo*))List_1_Contains_m7230_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m19629(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t631 *, DataSetU5BU5D_t3436*, int32_t, const MethodInfo*))List_1_CopyTo_m7231_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<Vuforia.DataSet>::Find(System.Predicate`1<T>)
#define List_1_Find_m19630(__this, ___match, method) (( DataSet_t600 * (*) (List_1_t631 *, Predicate_1_t3439 *, const MethodInfo*))List_1_Find_m15294_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m19631(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t3439 *, const MethodInfo*))List_1_CheckMatch_m15296_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m19632(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t631 *, int32_t, int32_t, Predicate_1_t3439 *, const MethodInfo*))List_1_GetIndex_m15298_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Vuforia.DataSet>::GetEnumerator()
#define List_1_GetEnumerator_m19633(__this, method) (( Enumerator_t3440  (*) (List_1_t631 *, const MethodInfo*))List_1_GetEnumerator_m15299_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::IndexOf(T)
#define List_1_IndexOf_m19634(__this, ___item, method) (( int32_t (*) (List_1_t631 *, DataSet_t600 *, const MethodInfo*))List_1_IndexOf_m7234_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m19635(__this, ___start, ___delta, method) (( void (*) (List_1_t631 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m15302_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m19636(__this, ___index, method) (( void (*) (List_1_t631 *, int32_t, const MethodInfo*))List_1_CheckIndex_m15304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Insert(System.Int32,T)
#define List_1_Insert_m19637(__this, ___index, ___item, method) (( void (*) (List_1_t631 *, int32_t, DataSet_t600 *, const MethodInfo*))List_1_Insert_m7235_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m19638(__this, ___collection, method) (( void (*) (List_1_t631 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m15307_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<Vuforia.DataSet>::Remove(T)
#define List_1_Remove_m19639(__this, ___item, method) (( bool (*) (List_1_t631 *, DataSet_t600 *, const MethodInfo*))List_1_Remove_m7232_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m19640(__this, ___match, method) (( int32_t (*) (List_1_t631 *, Predicate_1_t3439 *, const MethodInfo*))List_1_RemoveAll_m15310_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m19641(__this, ___index, method) (( void (*) (List_1_t631 *, int32_t, const MethodInfo*))List_1_RemoveAt_m7227_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Reverse()
#define List_1_Reverse_m19642(__this, method) (( void (*) (List_1_t631 *, const MethodInfo*))List_1_Reverse_m15313_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Sort()
#define List_1_Sort_m19643(__this, method) (( void (*) (List_1_t631 *, const MethodInfo*))List_1_Sort_m15315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m19644(__this, ___comparison, method) (( void (*) (List_1_t631 *, Comparison_1_t3441 *, const MethodInfo*))List_1_Sort_m15317_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<Vuforia.DataSet>::ToArray()
#define List_1_ToArray_m19645(__this, method) (( DataSetU5BU5D_t3436* (*) (List_1_t631 *, const MethodInfo*))List_1_ToArray_m15319_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::TrimExcess()
#define List_1_TrimExcess_m19646(__this, method) (( void (*) (List_1_t631 *, const MethodInfo*))List_1_TrimExcess_m15321_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::get_Capacity()
#define List_1_get_Capacity_m19647(__this, method) (( int32_t (*) (List_1_t631 *, const MethodInfo*))List_1_get_Capacity_m15323_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m19648(__this, ___value, method) (( void (*) (List_1_t631 *, int32_t, const MethodInfo*))List_1_set_Capacity_m15325_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<Vuforia.DataSet>::get_Count()
#define List_1_get_Count_m19649(__this, method) (( int32_t (*) (List_1_t631 *, const MethodInfo*))List_1_get_Count_m7213_gshared)(__this, method)
// T System.Collections.Generic.List`1<Vuforia.DataSet>::get_Item(System.Int32)
#define List_1_get_Item_m19650(__this, ___index, method) (( DataSet_t600 * (*) (List_1_t631 *, int32_t, const MethodInfo*))List_1_get_Item_m7236_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<Vuforia.DataSet>::set_Item(System.Int32,T)
#define List_1_set_Item_m19651(__this, ___index, ___value, method) (( void (*) (List_1_t631 *, int32_t, DataSet_t600 *, const MethodInfo*))List_1_set_Item_m7237_gshared)(__this, ___index, ___value, method)
