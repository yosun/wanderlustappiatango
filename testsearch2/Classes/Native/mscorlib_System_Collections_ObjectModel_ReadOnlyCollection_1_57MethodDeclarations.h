﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>
struct ReadOnlyCollection_1_t3779;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t465;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t1371;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t4352;
// UnityEngine.UILineInfo
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m25186_gshared (ReadOnlyCollection_1_t3779 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m25186(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3779 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m25186_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m25187_gshared (ReadOnlyCollection_1_t3779 * __this, UILineInfo_t464  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m25187(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3779 *, UILineInfo_t464 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m25187_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m25188_gshared (ReadOnlyCollection_1_t3779 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m25188(__this, method) (( void (*) (ReadOnlyCollection_1_t3779 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m25188_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m25189_gshared (ReadOnlyCollection_1_t3779 * __this, int32_t ___index, UILineInfo_t464  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m25189(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3779 *, int32_t, UILineInfo_t464 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m25189_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m25190_gshared (ReadOnlyCollection_1_t3779 * __this, UILineInfo_t464  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m25190(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3779 *, UILineInfo_t464 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m25190_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m25191_gshared (ReadOnlyCollection_1_t3779 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m25191(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3779 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m25191_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" UILineInfo_t464  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m25192_gshared (ReadOnlyCollection_1_t3779 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m25192(__this, ___index, method) (( UILineInfo_t464  (*) (ReadOnlyCollection_1_t3779 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m25192_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m25193_gshared (ReadOnlyCollection_1_t3779 * __this, int32_t ___index, UILineInfo_t464  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m25193(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3779 *, int32_t, UILineInfo_t464 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m25193_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25194_gshared (ReadOnlyCollection_1_t3779 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25194(__this, method) (( bool (*) (ReadOnlyCollection_1_t3779 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m25194_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m25195_gshared (ReadOnlyCollection_1_t3779 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m25195(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3779 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m25195_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m25196_gshared (ReadOnlyCollection_1_t3779 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m25196(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3779 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m25196_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m25197_gshared (ReadOnlyCollection_1_t3779 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m25197(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3779 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m25197_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m25198_gshared (ReadOnlyCollection_1_t3779 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m25198(__this, method) (( void (*) (ReadOnlyCollection_1_t3779 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m25198_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m25199_gshared (ReadOnlyCollection_1_t3779 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m25199(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3779 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m25199_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m25200_gshared (ReadOnlyCollection_1_t3779 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m25200(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3779 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m25200_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m25201_gshared (ReadOnlyCollection_1_t3779 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m25201(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3779 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m25201_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m25202_gshared (ReadOnlyCollection_1_t3779 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m25202(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3779 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m25202_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25203_gshared (ReadOnlyCollection_1_t3779 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25203(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3779 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m25203_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m25204_gshared (ReadOnlyCollection_1_t3779 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m25204(__this, method) (( bool (*) (ReadOnlyCollection_1_t3779 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m25204_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m25205_gshared (ReadOnlyCollection_1_t3779 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m25205(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3779 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m25205_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m25206_gshared (ReadOnlyCollection_1_t3779 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m25206(__this, method) (( bool (*) (ReadOnlyCollection_1_t3779 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m25206_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m25207_gshared (ReadOnlyCollection_1_t3779 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m25207(__this, method) (( bool (*) (ReadOnlyCollection_1_t3779 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m25207_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m25208_gshared (ReadOnlyCollection_1_t3779 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m25208(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3779 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m25208_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m25209_gshared (ReadOnlyCollection_1_t3779 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m25209(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3779 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m25209_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m25210_gshared (ReadOnlyCollection_1_t3779 * __this, UILineInfo_t464  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m25210(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3779 *, UILineInfo_t464 , const MethodInfo*))ReadOnlyCollection_1_Contains_m25210_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m25211_gshared (ReadOnlyCollection_1_t3779 * __this, UILineInfoU5BU5D_t1371* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m25211(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3779 *, UILineInfoU5BU5D_t1371*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m25211_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m25212_gshared (ReadOnlyCollection_1_t3779 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m25212(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3779 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m25212_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m25213_gshared (ReadOnlyCollection_1_t3779 * __this, UILineInfo_t464  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m25213(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3779 *, UILineInfo_t464 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m25213_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m25214_gshared (ReadOnlyCollection_1_t3779 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m25214(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3779 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m25214_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t464  ReadOnlyCollection_1_get_Item_m25215_gshared (ReadOnlyCollection_1_t3779 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m25215(__this, ___index, method) (( UILineInfo_t464  (*) (ReadOnlyCollection_1_t3779 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m25215_gshared)(__this, ___index, method)
