﻿#pragma once
#include <stdint.h>
// System.Text.Encoding
#include "mscorlib_System_Text_Encoding.h"
// System.Text.UnicodeEncoding
struct  UnicodeEncoding_t2476  : public Encoding_t1367
{
	// System.Boolean System.Text.UnicodeEncoding::bigEndian
	bool ___bigEndian_28;
	// System.Boolean System.Text.UnicodeEncoding::byteOrderMark
	bool ___byteOrderMark_29;
};
