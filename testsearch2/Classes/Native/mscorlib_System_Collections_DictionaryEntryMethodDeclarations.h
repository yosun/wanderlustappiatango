﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.DictionaryEntry
struct DictionaryEntry_t2002;
// System.Object
struct Object_t;

// System.Void System.Collections.DictionaryEntry::.ctor(System.Object,System.Object)
extern "C" void DictionaryEntry__ctor_m9339 (DictionaryEntry_t2002 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.DictionaryEntry::get_Key()
extern "C" Object_t * DictionaryEntry_get_Key_m10966 (DictionaryEntry_t2002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Collections.DictionaryEntry::get_Value()
extern "C" Object_t * DictionaryEntry_get_Value_m10967 (DictionaryEntry_t2002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
