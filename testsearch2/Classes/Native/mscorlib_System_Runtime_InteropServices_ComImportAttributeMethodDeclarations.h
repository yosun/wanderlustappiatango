﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.InteropServices.ComImportAttribute
struct ComImportAttribute_t2060;

// System.Void System.Runtime.InteropServices.ComImportAttribute::.ctor()
extern "C" void ComImportAttribute__ctor_m10327 (ComImportAttribute_t2060 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
