﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1<System.Int64>
struct EqualityComparer_1_t3802;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.EqualityComparer`1<System.Int64>::.ctor()
extern "C" void EqualityComparer_1__ctor_m25462_gshared (EqualityComparer_1_t3802 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m25462(__this, method) (( void (*) (EqualityComparer_1_t3802 *, const MethodInfo*))EqualityComparer_1__ctor_m25462_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<System.Int64>::.cctor()
extern "C" void EqualityComparer_1__cctor_m25463_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m25463(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m25463_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int64>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25464_gshared (EqualityComparer_1_t3802 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25464(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t3802 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m25464_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int64>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25465_gshared (EqualityComparer_1_t3802 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25465(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t3802 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m25465_gshared)(__this, ___x, ___y, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<System.Int64>::GetHashCode(T)
// System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int64>::Equals(T,T)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.Int64>::get_Default()
extern "C" EqualityComparer_1_t3802 * EqualityComparer_1_get_Default_m25466_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m25466(__this /* static, unused */, method) (( EqualityComparer_1_t3802 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m25466_gshared)(__this /* static, unused */, method)
