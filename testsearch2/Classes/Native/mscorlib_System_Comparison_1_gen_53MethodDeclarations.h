﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Comparison`1<System.Char>
struct Comparison_1_t3714;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Comparison`1<System.Char>::.ctor(System.Object,System.IntPtr)
// System.Comparison`1<System.UInt16>
#include "mscorlib_System_Comparison_1_gen_52MethodDeclarations.h"
#define Comparison_1__ctor_m24077(__this, ___object, ___method, method) (( void (*) (Comparison_1_t3714 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m24033_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<System.Char>::Invoke(T,T)
#define Comparison_1_Invoke_m24078(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t3714 *, uint16_t, uint16_t, const MethodInfo*))Comparison_1_Invoke_m24034_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<System.Char>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m24079(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t3714 *, uint16_t, uint16_t, AsyncCallback_t312 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m24035_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<System.Char>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m24080(__this, ___result, method) (( int32_t (*) (Comparison_1_t3714 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m24036_gshared)(__this, ___result, method)
