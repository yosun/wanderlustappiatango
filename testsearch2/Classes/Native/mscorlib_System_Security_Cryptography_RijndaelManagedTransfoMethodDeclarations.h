﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Security.Cryptography.RijndaelManagedTransform
struct RijndaelManagedTransform_t2422;
// System.Security.Cryptography.Rijndael
struct Rijndael_t1832;
// System.Byte[]
struct ByteU5BU5D_t622;

// System.Void System.Security.Cryptography.RijndaelManagedTransform::.ctor(System.Security.Cryptography.Rijndael,System.Boolean,System.Byte[],System.Byte[])
extern "C" void RijndaelManagedTransform__ctor_m12729 (RijndaelManagedTransform_t2422 * __this, Rijndael_t1832 * ___algo, bool ___encryption, ByteU5BU5D_t622* ___key, ByteU5BU5D_t622* ___iv, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.RijndaelManagedTransform::System.IDisposable.Dispose()
extern "C" void RijndaelManagedTransform_System_IDisposable_Dispose_m12730 (RijndaelManagedTransform_t2422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Security.Cryptography.RijndaelManagedTransform::get_CanReuseTransform()
extern "C" bool RijndaelManagedTransform_get_CanReuseTransform_m12731 (RijndaelManagedTransform_t2422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Security.Cryptography.RijndaelManagedTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C" int32_t RijndaelManagedTransform_TransformBlock_m12732 (RijndaelManagedTransform_t2422 * __this, ByteU5BU5D_t622* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, ByteU5BU5D_t622* ___outputBuffer, int32_t ___outputOffset, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.RijndaelManagedTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern "C" ByteU5BU5D_t622* RijndaelManagedTransform_TransformFinalBlock_m12733 (RijndaelManagedTransform_t2422 * __this, ByteU5BU5D_t622* ___inputBuffer, int32_t ___inputOffset, int32_t ___inputCount, const MethodInfo* method) IL2CPP_METHOD_ATTR;
