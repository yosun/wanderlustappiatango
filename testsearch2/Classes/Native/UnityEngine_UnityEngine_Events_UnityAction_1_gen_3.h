﻿#pragma once
#include <stdint.h>
// UnityEngine.Component
struct Component_t113;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct  UnityAction_1_t379  : public MulticastDelegate_t314
{
};
