﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>
struct U3CCreateCastIteratorU3Ec__Iterator0_1_t3442;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t534;

// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::.ctor()
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m19696_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m19696(__this, method) (( void (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1__ctor_m19696_gshared)(__this, method)
// TResult System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<TResult>.get_Current()
extern "C" Object_t * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19697_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19697(__this, method) (( Object_t * (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CTResultU3E_get_Current_m19697_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m19698_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m19698(__this, method) (( Object_t * (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m19698_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m19699_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m19699(__this, method) (( Object_t * (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_IEnumerable_GetEnumerator_m19699_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
extern "C" Object_t* U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19700_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19700(__this, method) (( Object_t* (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_System_Collections_Generic_IEnumerableU3CTResultU3E_GetEnumerator_m19700_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::MoveNext()
extern "C" bool U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m19701_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m19701(__this, method) (( bool (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_MoveNext_m19701_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateCastIterator>c__Iterator0`1<System.Object>::Dispose()
extern "C" void U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m19702_gshared (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 * __this, const MethodInfo* method);
#define U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m19702(__this, method) (( void (*) (U3CCreateCastIteratorU3Ec__Iterator0_1_t3442 *, const MethodInfo*))U3CCreateCastIteratorU3Ec__Iterator0_1_Dispose_m19702_gshared)(__this, method)
