﻿#pragma once
#include <stdint.h>
// UnityEngine.CanvasGroup
struct CanvasGroup_t448;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Comparison`1<UnityEngine.CanvasGroup>
struct  Comparison_1_t3339  : public MulticastDelegate_t314
{
};
