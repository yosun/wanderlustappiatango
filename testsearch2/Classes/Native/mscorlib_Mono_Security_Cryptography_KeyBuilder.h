﻿#pragma once
#include <stdint.h>
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t1669;
// System.Object
#include "mscorlib_System_Object.h"
// Mono.Security.Cryptography.KeyBuilder
struct  KeyBuilder_t2105  : public Object_t
{
};
struct KeyBuilder_t2105_StaticFields{
	// System.Security.Cryptography.RandomNumberGenerator Mono.Security.Cryptography.KeyBuilder::rng
	RandomNumberGenerator_t1669 * ___rng_0;
};
