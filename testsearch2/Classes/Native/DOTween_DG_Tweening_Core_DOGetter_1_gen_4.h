﻿#pragma once
#include <stdint.h>
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// DG.Tweening.Color2
#include "DOTween_DG_Tweening_Color2.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// DG.Tweening.Core.DOGetter`1<DG.Tweening.Color2>
struct  DOGetter_1_t1038  : public MulticastDelegate_t314
{
};
