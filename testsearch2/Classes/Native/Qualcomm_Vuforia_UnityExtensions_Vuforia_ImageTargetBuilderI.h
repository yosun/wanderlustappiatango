﻿#pragma once
#include <stdint.h>
// Vuforia.TrackableSource
struct TrackableSource_t625;
// Vuforia.ImageTargetBuilder
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_ImageTargetBuilder.h"
// Vuforia.ImageTargetBuilderImpl
struct  ImageTargetBuilderImpl_t626  : public ImageTargetBuilder_t613
{
	// Vuforia.TrackableSource Vuforia.ImageTargetBuilderImpl::mTrackableSource
	TrackableSource_t625 * ___mTrackableSource_0;
};
