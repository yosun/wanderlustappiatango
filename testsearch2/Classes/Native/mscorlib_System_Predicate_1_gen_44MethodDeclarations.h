﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>
struct Predicate_1_t3604;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Object,System.IntPtr)
extern "C" void Predicate_1__ctor_m22436_gshared (Predicate_1_t3604 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Predicate_1__ctor_m22436(__this, ___object, ___method, method) (( void (*) (Predicate_1_t3604 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m22436_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>::Invoke(T)
extern "C" bool Predicate_1_Invoke_m22437_gshared (Predicate_1_t3604 * __this, TargetSearchResult_t726  ___obj, const MethodInfo* method);
#define Predicate_1_Invoke_m22437(__this, ___obj, method) (( bool (*) (Predicate_1_t3604 *, TargetSearchResult_t726 , const MethodInfo*))Predicate_1_Invoke_m22437_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C" Object_t * Predicate_1_BeginInvoke_m22438_gshared (Predicate_1_t3604 * __this, TargetSearchResult_t726  ___obj, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Predicate_1_BeginInvoke_m22438(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t3604 *, TargetSearchResult_t726 , AsyncCallback_t312 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m22438_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<Vuforia.TargetFinder/TargetSearchResult>::EndInvoke(System.IAsyncResult)
extern "C" bool Predicate_1_EndInvoke_m22439_gshared (Predicate_1_t3604 * __this, Object_t * ___result, const MethodInfo* method);
#define Predicate_1_EndInvoke_m22439(__this, ___result, method) (( bool (*) (Predicate_1_t3604 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m22439_gshared)(__this, ___result, method)
