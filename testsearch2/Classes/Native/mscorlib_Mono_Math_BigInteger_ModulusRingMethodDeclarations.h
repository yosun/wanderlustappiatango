﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t2102;
// Mono.Math.BigInteger
struct BigInteger_t2101;

// System.Void Mono.Math.BigInteger/ModulusRing::.ctor(Mono.Math.BigInteger)
extern "C" void ModulusRing__ctor_m10469 (ModulusRing_t2102 * __this, BigInteger_t2101 * ___modulus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/ModulusRing::BarrettReduction(Mono.Math.BigInteger)
extern "C" void ModulusRing_BarrettReduction_m10470 (ModulusRing_t2102 * __this, BigInteger_t2101 * ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t2101 * ModulusRing_Multiply_m10471 (ModulusRing_t2102 * __this, BigInteger_t2101 * ___a, BigInteger_t2101 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Difference(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t2101 * ModulusRing_Difference_m10472 (ModulusRing_t2102 * __this, BigInteger_t2101 * ___a, BigInteger_t2101 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t2101 * ModulusRing_Pow_m10473 (ModulusRing_t2102 * __this, BigInteger_t2101 * ___a, BigInteger_t2101 * ___k, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(System.UInt32,Mono.Math.BigInteger)
extern "C" BigInteger_t2101 * ModulusRing_Pow_m10474 (ModulusRing_t2102 * __this, uint32_t ___b, BigInteger_t2101 * ___exp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
