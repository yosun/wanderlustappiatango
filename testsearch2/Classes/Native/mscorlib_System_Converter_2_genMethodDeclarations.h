﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Converter`2<System.Object,System.Object>
struct Converter_2_t3982;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"

// System.Void System.Converter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" void Converter_2__ctor_m27541_gshared (Converter_2_t3982 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Converter_2__ctor_m27541(__this, ___object, ___method, method) (( void (*) (Converter_2_t3982 *, Object_t *, IntPtr_t, const MethodInfo*))Converter_2__ctor_m27541_gshared)(__this, ___object, ___method, method)
// TOutput System.Converter`2<System.Object,System.Object>::Invoke(TInput)
extern "C" Object_t * Converter_2_Invoke_m27542_gshared (Converter_2_t3982 * __this, Object_t * ___input, const MethodInfo* method);
#define Converter_2_Invoke_m27542(__this, ___input, method) (( Object_t * (*) (Converter_2_t3982 *, Object_t *, const MethodInfo*))Converter_2_Invoke_m27542_gshared)(__this, ___input, method)
// System.IAsyncResult System.Converter`2<System.Object,System.Object>::BeginInvoke(TInput,System.AsyncCallback,System.Object)
extern "C" Object_t * Converter_2_BeginInvoke_m27543_gshared (Converter_2_t3982 * __this, Object_t * ___input, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Converter_2_BeginInvoke_m27543(__this, ___input, ___callback, ___object, method) (( Object_t * (*) (Converter_2_t3982 *, Object_t *, AsyncCallback_t312 *, Object_t *, const MethodInfo*))Converter_2_BeginInvoke_m27543_gshared)(__this, ___input, ___callback, ___object, method)
// TOutput System.Converter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C" Object_t * Converter_2_EndInvoke_m27544_gshared (Converter_2_t3982 * __this, Object_t * ___result, const MethodInfo* method);
#define Converter_2_EndInvoke_m27544(__this, ___result, method) (( Object_t * (*) (Converter_2_t3982 *, Object_t *, const MethodInfo*))Converter_2_EndInvoke_m27544_gshared)(__this, ___result, method)
