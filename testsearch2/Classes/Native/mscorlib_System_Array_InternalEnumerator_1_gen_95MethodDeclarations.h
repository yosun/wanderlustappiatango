﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.Uri/UriScheme>
struct InternalEnumerator_1_t3978;
// System.Object
struct Object_t;
// System.Array
struct Array_t;
// System.Uri/UriScheme
#include "System_System_Uri_UriScheme.h"

// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m27521_gshared (InternalEnumerator_1_t3978 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m27521(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3978 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m27521_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27522_gshared (InternalEnumerator_1_t3978 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27522(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3978 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27522_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m27523_gshared (InternalEnumerator_1_t3978 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m27523(__this, method) (( void (*) (InternalEnumerator_1_t3978 *, const MethodInfo*))InternalEnumerator_1_Dispose_m27523_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m27524_gshared (InternalEnumerator_1_t3978 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m27524(__this, method) (( bool (*) (InternalEnumerator_1_t3978 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m27524_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
extern "C" UriScheme_t1990  InternalEnumerator_1_get_Current_m27525_gshared (InternalEnumerator_1_t3978 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m27525(__this, method) (( UriScheme_t1990  (*) (InternalEnumerator_1_t3978 *, const MethodInfo*))InternalEnumerator_1_get_Current_m27525_gshared)(__this, method)
