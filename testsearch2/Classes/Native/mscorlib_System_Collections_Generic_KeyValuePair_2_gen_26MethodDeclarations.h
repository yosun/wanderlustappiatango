﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>
struct KeyValuePair_2_t3563;
// System.String
struct String_t;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m21979_gshared (KeyValuePair_2_t3563 * __this, int32_t ___key, TrackableResultData_t648  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m21979(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3563 *, int32_t, TrackableResultData_t648 , const MethodInfo*))KeyValuePair_2__ctor_m21979_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Key()
extern "C" int32_t KeyValuePair_2_get_Key_m21980_gshared (KeyValuePair_2_t3563 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m21980(__this, method) (( int32_t (*) (KeyValuePair_2_t3563 *, const MethodInfo*))KeyValuePair_2_get_Key_m21980_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m21981_gshared (KeyValuePair_2_t3563 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m21981(__this, ___value, method) (( void (*) (KeyValuePair_2_t3563 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m21981_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::get_Value()
extern "C" TrackableResultData_t648  KeyValuePair_2_get_Value_m21982_gshared (KeyValuePair_2_t3563 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m21982(__this, method) (( TrackableResultData_t648  (*) (KeyValuePair_2_t3563 *, const MethodInfo*))KeyValuePair_2_get_Value_m21982_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m21983_gshared (KeyValuePair_2_t3563 * __this, TrackableResultData_t648  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m21983(__this, ___value, method) (( void (*) (KeyValuePair_2_t3563 *, TrackableResultData_t648 , const MethodInfo*))KeyValuePair_2_set_Value_m21983_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/TrackableResultData>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m21984_gshared (KeyValuePair_2_t3563 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m21984(__this, method) (( String_t* (*) (KeyValuePair_2_t3563 *, const MethodInfo*))KeyValuePair_2_ToString_m21984_gshared)(__this, method)
