﻿#pragma once
#include <stdint.h>
// System.Char[]
struct CharU5BU5D_t119;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Enum
struct  Enum_t174  : public ValueType_t530
{
};
struct Enum_t174_StaticFields{
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t119* ___split_char_0;
};
