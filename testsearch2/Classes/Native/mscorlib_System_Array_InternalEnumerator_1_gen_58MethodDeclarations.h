﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IUserProfile>
struct InternalEnumerator_1_t3718;
// System.Object
struct Object_t;
// UnityEngine.SocialPlatforms.IUserProfile
struct IUserProfile_t1461;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IUserProfile>::.ctor(System.Array)
// System.Array/InternalEnumerator`1<System.Object>
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#define InternalEnumerator_1__ctor_m24168(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3718 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14882_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IUserProfile>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m24169(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3718 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14884_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IUserProfile>::Dispose()
#define InternalEnumerator_1_Dispose_m24170(__this, method) (( void (*) (InternalEnumerator_1_t3718 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14886_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IUserProfile>::MoveNext()
#define InternalEnumerator_1_MoveNext_m24171(__this, method) (( bool (*) (InternalEnumerator_1_t3718 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14888_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IUserProfile>::get_Current()
#define InternalEnumerator_1_get_Current_m24172(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3718 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14890_gshared)(__this, method)
