﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Transform_1_t3587;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.IntPtr
#include "mscorlib_System_IntPtr.h"
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Object,System.IntPtr)
extern "C" void Transform_1__ctor_m22184_gshared (Transform_1_t3587 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Transform_1__ctor_m22184(__this, ___object, ___method, method) (( void (*) (Transform_1_t3587 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m22184_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,Vuforia.QCARManagerImpl/VirtualButtonData>::Invoke(TKey,TValue)
extern "C" VirtualButtonData_t649  Transform_1_Invoke_m22185_gshared (Transform_1_t3587 * __this, int32_t ___key, VirtualButtonData_t649  ___value, const MethodInfo* method);
#define Transform_1_Invoke_m22185(__this, ___key, ___value, method) (( VirtualButtonData_t649  (*) (Transform_1_t3587 *, int32_t, VirtualButtonData_t649 , const MethodInfo*))Transform_1_Invoke_m22185_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,Vuforia.QCARManagerImpl/VirtualButtonData>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C" Object_t * Transform_1_BeginInvoke_m22186_gshared (Transform_1_t3587 * __this, int32_t ___key, VirtualButtonData_t649  ___value, AsyncCallback_t312 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Transform_1_BeginInvoke_m22186(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t3587 *, int32_t, VirtualButtonData_t649 , AsyncCallback_t312 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m22186_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData,Vuforia.QCARManagerImpl/VirtualButtonData>::EndInvoke(System.IAsyncResult)
extern "C" VirtualButtonData_t649  Transform_1_EndInvoke_m22187_gshared (Transform_1_t3587 * __this, Object_t * ___result, const MethodInfo* method);
#define Transform_1_EndInvoke_m22187(__this, ___result, method) (( VirtualButtonData_t649  (*) (Transform_1_t3587 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m22187_gshared)(__this, ___result, method)
