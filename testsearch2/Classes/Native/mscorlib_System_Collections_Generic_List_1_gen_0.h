﻿#pragma once
#include <stdint.h>
// UnityEngine.EventSystems.BaseInputModule[]
struct BaseInputModuleU5BU5D_t3162;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct  List_1_t202  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::_items
	BaseInputModuleU5BU5D_t3162* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::_version
	int32_t ____version_3;
};
struct List_1_t202_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>::EmptyArray
	BaseInputModuleU5BU5D_t3162* ___EmptyArray_4;
};
