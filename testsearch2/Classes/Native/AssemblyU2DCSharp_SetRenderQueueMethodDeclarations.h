﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// SetRenderQueue
struct SetRenderQueue_t28;

// System.Void SetRenderQueue::.ctor()
extern "C" void SetRenderQueue__ctor_m97 (SetRenderQueue_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SetRenderQueue::Awake()
extern "C" void SetRenderQueue_Awake_m98 (SetRenderQueue_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
