﻿#pragma once
#include <stdint.h>
// UnityEngine.ReflectionProbe
struct ReflectionProbe_t1165;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// UnityEngine.Rendering.ReflectionProbeBlendInfo
struct  ReflectionProbeBlendInfo_t1320 
{
	// UnityEngine.ReflectionProbe UnityEngine.Rendering.ReflectionProbeBlendInfo::probe
	ReflectionProbe_t1165 * ___probe_0;
	// System.Single UnityEngine.Rendering.ReflectionProbeBlendInfo::weight
	float ___weight_1;
};
