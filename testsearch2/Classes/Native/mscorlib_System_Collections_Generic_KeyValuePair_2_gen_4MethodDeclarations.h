﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.String,System.String>
struct KeyValuePair_2_t1401;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.String>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m24745(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1401 *, String_t*, String_t*, const MethodInfo*))KeyValuePair_2__ctor_m15000_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m6961(__this, method) (( String_t* (*) (KeyValuePair_2_t1401 *, const MethodInfo*))KeyValuePair_2_get_Key_m15001_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.String>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m24746(__this, ___value, method) (( void (*) (KeyValuePair_2_t1401 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m15002_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m6962(__this, method) (( String_t* (*) (KeyValuePair_2_t1401 *, const MethodInfo*))KeyValuePair_2_get_Value_m15003_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.String>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m24747(__this, ___value, method) (( void (*) (KeyValuePair_2_t1401 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Value_m15004_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.String>::ToString()
#define KeyValuePair_2_ToString_m24748(__this, method) (( String_t* (*) (KeyValuePair_2_t1401 *, const MethodInfo*))KeyValuePair_2_ToString_m15005_gshared)(__this, method)
