﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.EqualityComparer`1<System.UInt16>
struct EqualityComparer_1_t3486;
// System.Object
#include "mscorlib_System_Object.h"
// System.Collections.Generic.EqualityComparer`1<System.UInt16>
struct  EqualityComparer_1_t3486  : public Object_t
{
};
struct EqualityComparer_1_t3486_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<System.UInt16>::_default
	EqualityComparer_1_t3486 * ____default_0;
};
