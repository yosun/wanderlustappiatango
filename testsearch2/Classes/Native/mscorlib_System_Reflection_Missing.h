﻿#pragma once
#include <stdint.h>
// System.Reflection.Missing
struct Missing_t2256;
// System.Object
#include "mscorlib_System_Object.h"
// System.Reflection.Missing
struct  Missing_t2256  : public Object_t
{
};
struct Missing_t2256_StaticFields{
	// System.Reflection.Missing System.Reflection.Missing::Value
	Missing_t2256 * ___Value_0;
};
