﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.IO.MonoIOStat
struct MonoIOStat_t2203;
struct MonoIOStat_t2203_marshaled;

void MonoIOStat_t2203_marshal(const MonoIOStat_t2203& unmarshaled, MonoIOStat_t2203_marshaled& marshaled);
void MonoIOStat_t2203_marshal_back(const MonoIOStat_t2203_marshaled& marshaled, MonoIOStat_t2203& unmarshaled);
void MonoIOStat_t2203_marshal_cleanup(MonoIOStat_t2203_marshaled& marshaled);
