﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Array/InternalEnumerator`1<System.UInt32>
struct InternalEnumerator_1_t3934;
// System.Object
struct Object_t;
// System.Array
struct Array_t;

// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m27070_gshared (InternalEnumerator_1_t3934 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m27070(__this, ___array, method) (( void (*) (InternalEnumerator_1_t3934 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m27070_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27071_gshared (InternalEnumerator_1_t3934 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27071(__this, method) (( Object_t * (*) (InternalEnumerator_1_t3934 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m27071_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m27072_gshared (InternalEnumerator_1_t3934 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m27072(__this, method) (( void (*) (InternalEnumerator_1_t3934 *, const MethodInfo*))InternalEnumerator_1_Dispose_m27072_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m27073_gshared (InternalEnumerator_1_t3934 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m27073(__this, method) (( bool (*) (InternalEnumerator_1_t3934 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m27073_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern "C" uint32_t InternalEnumerator_1_get_Current_m27074_gshared (InternalEnumerator_1_t3934 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m27074(__this, method) (( uint32_t (*) (InternalEnumerator_1_t3934 *, const MethodInfo*))InternalEnumerator_1_get_Current_m27074_gshared)(__this, method)
