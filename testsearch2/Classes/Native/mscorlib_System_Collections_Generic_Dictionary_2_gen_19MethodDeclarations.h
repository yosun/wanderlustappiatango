﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct Dictionary_2_t876;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t4073;
// System.Collections.Generic.ICollection`1<Vuforia.QCARManagerImpl/VirtualButtonData>
struct ICollection_1_t4259;
// System.Object
struct Object_t;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct KeyCollection_t3581;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct ValueCollection_t3585;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3221;
// System.Collections.Generic.IDictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
struct IDictionary_2_t4260;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1388;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>[]
struct KeyValuePair_2U5BU5D_t4261;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>>
struct IEnumerator_1_t4262;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t2001;
// Vuforia.QCARManagerImpl/VirtualButtonData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Vir.h"
// System.Runtime.Serialization.StreamingContext
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__25.h"
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor()
extern "C" void Dictionary_2__ctor_m4583_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m4583(__this, method) (( void (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2__ctor_m4583_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m22065_gshared (Dictionary_2_t876 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m22065(__this, ___comparer, method) (( void (*) (Dictionary_2_t876 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22065_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m22066_gshared (Dictionary_2_t876 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m22066(__this, ___dictionary, method) (( void (*) (Dictionary_2_t876 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22066_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m22067_gshared (Dictionary_2_t876 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m22067(__this, ___capacity, method) (( void (*) (Dictionary_2_t876 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m22067_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m22068_gshared (Dictionary_2_t876 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m22068(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t876 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m22068_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m22069_gshared (Dictionary_2_t876 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m22069(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t876 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2__ctor_m22069_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22070_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22070(__this, method) (( Object_t* (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m22070_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22071_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22071(__this, method) (( Object_t* (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m22071_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m22072_gshared (Dictionary_2_t876 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m22072(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t876 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m22072_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m22073_gshared (Dictionary_2_t876 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m22073(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t876 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m22073_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m22074_gshared (Dictionary_2_t876 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m22074(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t876 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m22074_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m22075_gshared (Dictionary_2_t876 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m22075(__this, ___key, method) (( bool (*) (Dictionary_2_t876 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m22075_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m22076_gshared (Dictionary_2_t876 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m22076(__this, ___key, method) (( void (*) (Dictionary_2_t876 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m22076_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22077_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22077(__this, method) (( bool (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m22077_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22078_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22078(__this, method) (( Object_t * (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m22078_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22079_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22079(__this, method) (( bool (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m22079_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22080_gshared (Dictionary_2_t876 * __this, KeyValuePair_2_t3578  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22080(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t876 *, KeyValuePair_2_t3578 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m22080_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22081_gshared (Dictionary_2_t876 * __this, KeyValuePair_2_t3578  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22081(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t876 *, KeyValuePair_2_t3578 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m22081_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22082_gshared (Dictionary_2_t876 * __this, KeyValuePair_2U5BU5D_t4261* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22082(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t876 *, KeyValuePair_2U5BU5D_t4261*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m22082_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22083_gshared (Dictionary_2_t876 * __this, KeyValuePair_2_t3578  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22083(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t876 *, KeyValuePair_2_t3578 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m22083_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m22084_gshared (Dictionary_2_t876 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m22084(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t876 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m22084_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22085_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22085(__this, method) (( Object_t * (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m22085_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22086_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22086(__this, method) (( Object_t* (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m22086_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22087_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22087(__this, method) (( Object_t * (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m22087_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m22088_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m22088(__this, method) (( int32_t (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_get_Count_m22088_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Item(TKey)
extern "C" VirtualButtonData_t649  Dictionary_2_get_Item_m22089_gshared (Dictionary_2_t876 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m22089(__this, ___key, method) (( VirtualButtonData_t649  (*) (Dictionary_2_t876 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m22089_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m22090_gshared (Dictionary_2_t876 * __this, int32_t ___key, VirtualButtonData_t649  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m22090(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t876 *, int32_t, VirtualButtonData_t649 , const MethodInfo*))Dictionary_2_set_Item_m22090_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m22091_gshared (Dictionary_2_t876 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m22091(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t876 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m22091_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m22092_gshared (Dictionary_2_t876 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m22092(__this, ___size, method) (( void (*) (Dictionary_2_t876 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m22092_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m22093_gshared (Dictionary_2_t876 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m22093(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t876 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m22093_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t3578  Dictionary_2_make_pair_m22094_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t649  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m22094(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t3578  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t649 , const MethodInfo*))Dictionary_2_make_pair_m22094_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m22095_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t649  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m22095(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t649 , const MethodInfo*))Dictionary_2_pick_key_m22095_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::pick_value(TKey,TValue)
extern "C" VirtualButtonData_t649  Dictionary_2_pick_value_m22096_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t649  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m22096(__this /* static, unused */, ___key, ___value, method) (( VirtualButtonData_t649  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t649 , const MethodInfo*))Dictionary_2_pick_value_m22096_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m22097_gshared (Dictionary_2_t876 * __this, KeyValuePair_2U5BU5D_t4261* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m22097(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t876 *, KeyValuePair_2U5BU5D_t4261*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m22097_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Resize()
extern "C" void Dictionary_2_Resize_m22098_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m22098(__this, method) (( void (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_Resize_m22098_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m22099_gshared (Dictionary_2_t876 * __this, int32_t ___key, VirtualButtonData_t649  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m22099(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t876 *, int32_t, VirtualButtonData_t649 , const MethodInfo*))Dictionary_2_Add_m22099_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Clear()
extern "C" void Dictionary_2_Clear_m22100_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m22100(__this, method) (( void (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_Clear_m22100_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m22101_gshared (Dictionary_2_t876 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m22101(__this, ___key, method) (( bool (*) (Dictionary_2_t876 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m22101_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m22102_gshared (Dictionary_2_t876 * __this, VirtualButtonData_t649  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m22102(__this, ___value, method) (( bool (*) (Dictionary_2_t876 *, VirtualButtonData_t649 , const MethodInfo*))Dictionary_2_ContainsValue_m22102_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m22103_gshared (Dictionary_2_t876 * __this, SerializationInfo_t1388 * ___info, StreamingContext_t1389  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m22103(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t876 *, SerializationInfo_t1388 *, StreamingContext_t1389 , const MethodInfo*))Dictionary_2_GetObjectData_m22103_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m22104_gshared (Dictionary_2_t876 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m22104(__this, ___sender, method) (( void (*) (Dictionary_2_t876 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m22104_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m22105_gshared (Dictionary_2_t876 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m22105(__this, ___key, method) (( bool (*) (Dictionary_2_t876 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m22105_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m22106_gshared (Dictionary_2_t876 * __this, int32_t ___key, VirtualButtonData_t649 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m22106(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t876 *, int32_t, VirtualButtonData_t649 *, const MethodInfo*))Dictionary_2_TryGetValue_m22106_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Keys()
extern "C" KeyCollection_t3581 * Dictionary_2_get_Keys_m22107_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m22107(__this, method) (( KeyCollection_t3581 * (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_get_Keys_m22107_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::get_Values()
extern "C" ValueCollection_t3585 * Dictionary_2_get_Values_m22108_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m22108(__this, method) (( ValueCollection_t3585 * (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_get_Values_m22108_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m22109_gshared (Dictionary_2_t876 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m22109(__this, ___key, method) (( int32_t (*) (Dictionary_2_t876 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m22109_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::ToTValue(System.Object)
extern "C" VirtualButtonData_t649  Dictionary_2_ToTValue_m22110_gshared (Dictionary_2_t876 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m22110(__this, ___value, method) (( VirtualButtonData_t649  (*) (Dictionary_2_t876 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m22110_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m22111_gshared (Dictionary_2_t876 * __this, KeyValuePair_2_t3578  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m22111(__this, ___pair, method) (( bool (*) (Dictionary_2_t876 *, KeyValuePair_2_t3578 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m22111_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::GetEnumerator()
extern "C" Enumerator_t3583  Dictionary_2_GetEnumerator_m22112_gshared (Dictionary_2_t876 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m22112(__this, method) (( Enumerator_t3583  (*) (Dictionary_2_t876 *, const MethodInfo*))Dictionary_2_GetEnumerator_m22112_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.QCARManagerImpl/VirtualButtonData>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t2002  Dictionary_2_U3CCopyToU3Em__0_m22113_gshared (Object_t * __this /* static, unused */, int32_t ___key, VirtualButtonData_t649  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m22113(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t2002  (*) (Object_t * /* static, unused */, int32_t, VirtualButtonData_t649 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m22113_gshared)(__this /* static, unused */, ___key, ___value, method)
