﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>
struct KeyValuePair_2_t1425;
// System.Type
struct Type_t;
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
struct SetDelegate_t1291;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::.ctor(TKey,TValue)
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_7MethodDeclarations.h"
#define KeyValuePair_2__ctor_m7030(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1425 *, Type_t *, SetDelegate_t1291 *, const MethodInfo*))KeyValuePair_2__ctor_m15000_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::get_Key()
#define KeyValuePair_2_get_Key_m26078(__this, method) (( Type_t * (*) (KeyValuePair_2_t1425 *, const MethodInfo*))KeyValuePair_2_get_Key_m15001_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m26079(__this, ___value, method) (( void (*) (KeyValuePair_2_t1425 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m15002_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::get_Value()
#define KeyValuePair_2_get_Value_m26080(__this, method) (( SetDelegate_t1291 * (*) (KeyValuePair_2_t1425 *, const MethodInfo*))KeyValuePair_2_get_Value_m15003_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m26081(__this, ___value, method) (( void (*) (KeyValuePair_2_t1425 *, SetDelegate_t1291 *, const MethodInfo*))KeyValuePair_2_set_Value_m15004_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>::ToString()
#define KeyValuePair_2_ToString_m26082(__this, method) (( String_t* (*) (KeyValuePair_2_t1425 *, const MethodInfo*))KeyValuePair_2_ToString_m15005_gshared)(__this, method)
