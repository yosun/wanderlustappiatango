﻿#pragma once
#include <stdint.h>
// System.Text.RegularExpressions.Match
struct Match_t1067;
// System.Text.StringBuilder
struct StringBuilder_t429;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Text.RegularExpressions.BaseMachine/MatchAppendEvaluator
struct  MatchAppendEvaluator_t1927  : public MulticastDelegate_t314
{
};
