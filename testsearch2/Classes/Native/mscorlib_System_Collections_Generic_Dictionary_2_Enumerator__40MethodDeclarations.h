﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Enumerator_t3907;
// System.Object
struct Object_t;
// UnityEngine.Event
struct Event_t323;
struct Event_t323_marshaled;
// System.Collections.Generic.Dictionary`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t1342;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_44.h"
// UnityEngine.TextEditor/TextEditOp
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int32>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__8MethodDeclarations.h"
#define Enumerator__ctor_m26820(__this, ___dictionary, method) (( void (*) (Enumerator_t3907 *, Dictionary_2_t1342 *, const MethodInfo*))Enumerator__ctor_m16858_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m26821(__this, method) (( Object_t * (*) (Enumerator_t3907 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16859_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m26822(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3907 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16860_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m26823(__this, method) (( Object_t * (*) (Enumerator_t3907 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16861_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m26824(__this, method) (( Object_t * (*) (Enumerator_t3907 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16862_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m26825(__this, method) (( bool (*) (Enumerator_t3907 *, const MethodInfo*))Enumerator_MoveNext_m16863_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m26826(__this, method) (( KeyValuePair_2_t3904  (*) (Enumerator_t3907 *, const MethodInfo*))Enumerator_get_Current_m16864_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m26827(__this, method) (( Event_t323 * (*) (Enumerator_t3907 *, const MethodInfo*))Enumerator_get_CurrentKey_m16865_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m26828(__this, method) (( int32_t (*) (Enumerator_t3907 *, const MethodInfo*))Enumerator_get_CurrentValue_m16866_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m26829(__this, method) (( void (*) (Enumerator_t3907 *, const MethodInfo*))Enumerator_VerifyState_m16867_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m26830(__this, method) (( void (*) (Enumerator_t3907 *, const MethodInfo*))Enumerator_VerifyCurrent_m16868_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m26831(__this, method) (( void (*) (Enumerator_t3907 *, const MethodInfo*))Enumerator_Dispose_m16869_gshared)(__this, method)
