﻿#pragma once
#include <stdint.h>
// System.Collections.Generic.List`1<DG.Tweening.Core.ABSSequentiable>
struct List_1_t953;
// DG.Tweening.Core.ABSSequentiable
struct ABSSequentiable_t949;
// System.ValueType
#include "mscorlib_System_ValueType.h"
// System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>
struct  Enumerator_t3691 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::l
	List_1_t953 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator<DG.Tweening.Core.ABSSequentiable>::current
	ABSSequentiable_t949 * ___current_3;
};
