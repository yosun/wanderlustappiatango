﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>
struct ReadOnlyCollection_1_t3600;
// System.Object
struct Object_t;
// System.Collections.Generic.IList`1<Vuforia.TargetFinder/TargetSearchResult>
struct IList_1_t3599;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t416;
// Vuforia.TargetFinder/TargetSearchResult[]
struct TargetSearchResultU5BU5D_t3596;
// System.Collections.Generic.IEnumerator`1<Vuforia.TargetFinder/TargetSearchResult>
struct IEnumerator_1_t802;
// Vuforia.TargetFinder/TargetSearchResult
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_TargetFinder_Target.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C" void ReadOnlyCollection_1__ctor_m22362_gshared (ReadOnlyCollection_1_t3600 * __this, Object_t* ___list, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m22362(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t3600 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m22362_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22363_gshared (ReadOnlyCollection_1_t3600 * __this, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22363(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t3600 *, TargetSearchResult_t726 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m22363_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22364_gshared (ReadOnlyCollection_1_t3600 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22364(__this, method) (( void (*) (ReadOnlyCollection_1_t3600 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m22364_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22365_gshared (ReadOnlyCollection_1_t3600 * __this, int32_t ___index, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22365(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t3600 *, int32_t, TargetSearchResult_t726 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m22365_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22366_gshared (ReadOnlyCollection_1_t3600 * __this, TargetSearchResult_t726  ___item, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22366(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t3600 *, TargetSearchResult_t726 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m22366_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22367_gshared (ReadOnlyCollection_1_t3600 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22367(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3600 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m22367_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C" TargetSearchResult_t726  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22368_gshared (ReadOnlyCollection_1_t3600 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22368(__this, ___index, method) (( TargetSearchResult_t726  (*) (ReadOnlyCollection_1_t3600 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m22368_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C" void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22369_gshared (ReadOnlyCollection_1_t3600 * __this, int32_t ___index, TargetSearchResult_t726  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22369(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3600 *, int32_t, TargetSearchResult_t726 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m22369_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22370_gshared (ReadOnlyCollection_1_t3600 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22370(__this, method) (( bool (*) (ReadOnlyCollection_1_t3600 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m22370_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22371_gshared (ReadOnlyCollection_1_t3600 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22371(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3600 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m22371_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22372_gshared (ReadOnlyCollection_1_t3600 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22372(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3600 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m22372_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m22373_gshared (ReadOnlyCollection_1_t3600 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m22373(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3600 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m22373_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Clear()
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Clear_m22374_gshared (ReadOnlyCollection_1_t3600 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m22374(__this, method) (( void (*) (ReadOnlyCollection_1_t3600 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m22374_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_Contains_m22375_gshared (ReadOnlyCollection_1_t3600 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m22375(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3600 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m22375_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22376_gshared (ReadOnlyCollection_1_t3600 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22376(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3600 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m22376_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Insert_m22377_gshared (ReadOnlyCollection_1_t3600 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m22377(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3600 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m22377_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.Remove(System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_Remove_m22378_gshared (ReadOnlyCollection_1_t3600 * __this, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m22378(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t3600 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m22378_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.RemoveAt(System.Int32)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22379_gshared (ReadOnlyCollection_1_t3600 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22379(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t3600 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m22379_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22380_gshared (ReadOnlyCollection_1_t3600 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22380(__this, method) (( bool (*) (ReadOnlyCollection_1_t3600 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m22380_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22381_gshared (ReadOnlyCollection_1_t3600 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22381(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t3600 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m22381_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22382_gshared (ReadOnlyCollection_1_t3600 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22382(__this, method) (( bool (*) (ReadOnlyCollection_1_t3600 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m22382_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22383_gshared (ReadOnlyCollection_1_t3600 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22383(__this, method) (( bool (*) (ReadOnlyCollection_1_t3600 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m22383_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * ReadOnlyCollection_1_System_Collections_IList_get_Item_m22384_gshared (ReadOnlyCollection_1_t3600 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m22384(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t3600 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m22384_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void ReadOnlyCollection_1_System_Collections_IList_set_Item_m22385_gshared (ReadOnlyCollection_1_t3600 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m22385(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t3600 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m22385_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::Contains(T)
extern "C" bool ReadOnlyCollection_1_Contains_m22386_gshared (ReadOnlyCollection_1_t3600 * __this, TargetSearchResult_t726  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m22386(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t3600 *, TargetSearchResult_t726 , const MethodInfo*))ReadOnlyCollection_1_Contains_m22386_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::CopyTo(T[],System.Int32)
extern "C" void ReadOnlyCollection_1_CopyTo_m22387_gshared (ReadOnlyCollection_1_t3600 * __this, TargetSearchResultU5BU5D_t3596* ___array, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m22387(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t3600 *, TargetSearchResultU5BU5D_t3596*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m22387_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::GetEnumerator()
extern "C" Object_t* ReadOnlyCollection_1_GetEnumerator_m22388_gshared (ReadOnlyCollection_1_t3600 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m22388(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t3600 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m22388_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::IndexOf(T)
extern "C" int32_t ReadOnlyCollection_1_IndexOf_m22389_gshared (ReadOnlyCollection_1_t3600 * __this, TargetSearchResult_t726  ___value, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m22389(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t3600 *, TargetSearchResult_t726 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m22389_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Count()
extern "C" int32_t ReadOnlyCollection_1_get_Count_m22390_gshared (ReadOnlyCollection_1_t3600 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m22390(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t3600 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m22390_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Vuforia.TargetFinder/TargetSearchResult>::get_Item(System.Int32)
extern "C" TargetSearchResult_t726  ReadOnlyCollection_1_get_Item_m22391_gshared (ReadOnlyCollection_1_t3600 * __this, int32_t ___index, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m22391(__this, ___index, method) (( TargetSearchResult_t726  (*) (ReadOnlyCollection_1_t3600 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m22391_gshared)(__this, ___index, method)
