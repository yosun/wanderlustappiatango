﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/TrackableResultData>
struct DefaultComparer_t3575;
// Vuforia.QCARManagerImpl/TrackableResultData
#include "Qualcomm_Vuforia_UnityExtensions_Vuforia_QCARManagerImpl_Tra.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/TrackableResultData>::.ctor()
extern "C" void DefaultComparer__ctor_m22062_gshared (DefaultComparer_t3575 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m22062(__this, method) (( void (*) (DefaultComparer_t3575 *, const MethodInfo*))DefaultComparer__ctor_m22062_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/TrackableResultData>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m22063_gshared (DefaultComparer_t3575 * __this, TrackableResultData_t648  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m22063(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3575 *, TrackableResultData_t648 , const MethodInfo*))DefaultComparer_GetHashCode_m22063_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Vuforia.QCARManagerImpl/TrackableResultData>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m22064_gshared (DefaultComparer_t3575 * __this, TrackableResultData_t648  ___x, TrackableResultData_t648  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m22064(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3575 *, TrackableResultData_t648 , TrackableResultData_t648 , const MethodInfo*))DefaultComparer_Equals_m22064_gshared)(__this, ___x, ___y, method)
