﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t1444;
// UnityEngine.Object
struct Object_t111;
struct Object_t111_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t124;

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// UnityEngine.Events.CachedInvokableCall`1<System.Byte>
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_1_gen_4MethodDeclarations.h"
#define CachedInvokableCall_1__ctor_m7059(__this, ___target, ___theFunction, ___argument, method) (( void (*) (CachedInvokableCall_1_t1444 *, Object_t111 *, MethodInfo_t *, bool, const MethodInfo*))CachedInvokableCall_1__ctor_m26870_gshared)(__this, ___target, ___theFunction, ___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::Invoke(System.Object[])
#define CachedInvokableCall_1_Invoke_m26871(__this, ___args, method) (( void (*) (CachedInvokableCall_1_t1444 *, ObjectU5BU5D_t124*, const MethodInfo*))CachedInvokableCall_1_Invoke_m26872_gshared)(__this, ___args, method)
