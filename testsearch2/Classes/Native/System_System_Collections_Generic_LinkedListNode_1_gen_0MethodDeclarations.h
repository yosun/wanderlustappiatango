﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t3939;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t3940;
// System.Object
struct Object_t;

// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C" void LinkedListNode_1__ctor_m27118_gshared (LinkedListNode_1_t3939 * __this, LinkedList_1_t3940 * ___list, Object_t * ___value, const MethodInfo* method);
#define LinkedListNode_1__ctor_m27118(__this, ___list, ___value, method) (( void (*) (LinkedListNode_1_t3939 *, LinkedList_1_t3940 *, Object_t *, const MethodInfo*))LinkedListNode_1__ctor_m27118_gshared)(__this, ___list, ___value, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C" void LinkedListNode_1__ctor_m27119_gshared (LinkedListNode_1_t3939 * __this, LinkedList_1_t3940 * ___list, Object_t * ___value, LinkedListNode_1_t3939 * ___previousNode, LinkedListNode_1_t3939 * ___nextNode, const MethodInfo* method);
#define LinkedListNode_1__ctor_m27119(__this, ___list, ___value, ___previousNode, ___nextNode, method) (( void (*) (LinkedListNode_1_t3939 *, LinkedList_1_t3940 *, Object_t *, LinkedListNode_1_t3939 *, LinkedListNode_1_t3939 *, const MethodInfo*))LinkedListNode_1__ctor_m27119_gshared)(__this, ___list, ___value, ___previousNode, ___nextNode, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
extern "C" void LinkedListNode_1_Detach_m27120_gshared (LinkedListNode_1_t3939 * __this, const MethodInfo* method);
#define LinkedListNode_1_Detach_m27120(__this, method) (( void (*) (LinkedListNode_1_t3939 *, const MethodInfo*))LinkedListNode_1_Detach_m27120_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
extern "C" LinkedList_1_t3940 * LinkedListNode_1_get_List_m27121_gshared (LinkedListNode_1_t3939 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_List_m27121(__this, method) (( LinkedList_1_t3940 * (*) (LinkedListNode_1_t3939 *, const MethodInfo*))LinkedListNode_1_get_List_m27121_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
extern "C" LinkedListNode_1_t3939 * LinkedListNode_1_get_Next_m27122_gshared (LinkedListNode_1_t3939 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Next_m27122(__this, method) (( LinkedListNode_1_t3939 * (*) (LinkedListNode_1_t3939 *, const MethodInfo*))LinkedListNode_1_get_Next_m27122_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
extern "C" Object_t * LinkedListNode_1_get_Value_m27123_gshared (LinkedListNode_1_t3939 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Value_m27123(__this, method) (( Object_t * (*) (LinkedListNode_1_t3939 *, const MethodInfo*))LinkedListNode_1_get_Value_m27123_gshared)(__this, method)
