﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>
struct Enumerator_t883;
// System.Object
struct Object_t;
// Vuforia.ImageTarget
struct ImageTarget_t738;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.ImageTarget>
struct Dictionary_2_t730;

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_31MethodDeclarations.h"
#define Enumerator__ctor_m22547(__this, ___host, method) (( void (*) (Enumerator_t883 *, Dictionary_2_t730 *, const MethodInfo*))Enumerator__ctor_m16551_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m22548(__this, method) (( Object_t * (*) (Enumerator_t883 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16552_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::Dispose()
#define Enumerator_Dispose_m22549(__this, method) (( void (*) (Enumerator_t883 *, const MethodInfo*))Enumerator_Dispose_m16553_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::MoveNext()
#define Enumerator_MoveNext_m4602(__this, method) (( bool (*) (Enumerator_t883 *, const MethodInfo*))Enumerator_MoveNext_m16554_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,Vuforia.ImageTarget>::get_Current()
#define Enumerator_get_Current_m4601(__this, method) (( Object_t * (*) (Enumerator_t883 *, const MethodInfo*))Enumerator_get_Current_m16555_gshared)(__this, method)
