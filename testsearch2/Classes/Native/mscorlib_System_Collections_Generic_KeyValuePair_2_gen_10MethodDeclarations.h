﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
struct KeyValuePair_2_t3253;
// System.Object
struct Object_t;
// System.String
struct String_t;

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m16833_gshared (KeyValuePair_2_t3253 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m16833(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t3253 *, Object_t *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m16833_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m16834_gshared (KeyValuePair_2_t3253 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m16834(__this, method) (( Object_t * (*) (KeyValuePair_2_t3253 *, const MethodInfo*))KeyValuePair_2_get_Key_m16834_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m16835_gshared (KeyValuePair_2_t3253 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m16835(__this, ___value, method) (( void (*) (KeyValuePair_2_t3253 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m16835_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m16836_gshared (KeyValuePair_2_t3253 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m16836(__this, method) (( int32_t (*) (KeyValuePair_2_t3253 *, const MethodInfo*))KeyValuePair_2_get_Value_m16836_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m16837_gshared (KeyValuePair_2_t3253 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m16837(__this, ___value, method) (( void (*) (KeyValuePair_2_t3253 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m16837_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m16838_gshared (KeyValuePair_2_t3253 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m16838(__this, method) (( String_t* (*) (KeyValuePair_2_t3253 *, const MethodInfo*))KeyValuePair_2_ToString_m16838_gshared)(__this, method)
