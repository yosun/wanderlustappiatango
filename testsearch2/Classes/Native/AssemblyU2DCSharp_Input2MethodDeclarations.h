﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Input2
struct Input2_t24;
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void Input2::.ctor()
extern "C" void Input2__ctor_m48 (Input2_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Input2::.cctor()
extern "C" void Input2__cctor_m49 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Input2::IsPointerOverUI()
extern "C" bool Input2_IsPointerOverUI_m50 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Input2::TouchBegin()
extern "C" bool Input2_TouchBegin_m51 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Input2::GetFirstPoint()
extern "C" Vector2_t19  Input2_GetFirstPoint_m52 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Input2::GetSecondPoint()
extern "C" Vector2_t19  Input2_GetSecondPoint_m53 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Input2::GetPointerCount()
extern "C" int32_t Input2_GetPointerCount_m54 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Input2::HasPointStarted(System.Int32)
extern "C" bool Input2_HasPointStarted_m55 (Object_t * __this /* static, unused */, int32_t ___num, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Input2::HasPointEnded(System.Int32)
extern "C" bool Input2_HasPointEnded_m56 (Object_t * __this /* static, unused */, int32_t ___num, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Input2::HasPointMoved(System.Int32)
extern "C" bool Input2_HasPointMoved_m57 (Object_t * __this /* static, unused */, int32_t ___num, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Input2::Pinch()
extern "C" float Input2_Pinch_m58 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Input2::Twist()
extern "C" float Input2_Twist_m59 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Input2::TwistInt()
extern "C" int32_t Input2_TwistInt_m60 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Input2::SwipeLeft(UnityEngine.Vector2)
extern "C" bool Input2_SwipeLeft_m61 (Object_t * __this /* static, unused */, Vector2_t19  ___swipedir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Input2::SwipeRight(UnityEngine.Vector2)
extern "C" bool Input2_SwipeRight_m62 (Object_t * __this /* static, unused */, Vector2_t19  ___swipedir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Input2::SwipeDown(UnityEngine.Vector2)
extern "C" bool Input2_SwipeDown_m63 (Object_t * __this /* static, unused */, Vector2_t19  ___swipedir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Input2::SwipeUp(UnityEngine.Vector2)
extern "C" bool Input2_SwipeUp_m64 (Object_t * __this /* static, unused */, Vector2_t19  ___swipedir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Input2::ContinuousSwipe()
extern "C" Vector2_t19  Input2_ContinuousSwipe_m65 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Input2::Swipe()
extern "C" Vector2_t19  Input2_Swipe_m66 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
