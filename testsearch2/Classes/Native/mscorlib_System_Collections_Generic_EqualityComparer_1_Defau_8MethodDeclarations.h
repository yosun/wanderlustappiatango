﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t3773;
// UnityEngine.UICharInfo
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m25114_gshared (DefaultComparer_t3773 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m25114(__this, method) (( void (*) (DefaultComparer_t3773 *, const MethodInfo*))DefaultComparer__ctor_m25114_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m25115_gshared (DefaultComparer_t3773 * __this, UICharInfo_t466  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m25115(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t3773 *, UICharInfo_t466 , const MethodInfo*))DefaultComparer_GetHashCode_m25115_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m25116_gshared (DefaultComparer_t3773 * __this, UICharInfo_t466  ___x, UICharInfo_t466  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m25116(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t3773 *, UICharInfo_t466 , UICharInfo_t466 , const MethodInfo*))DefaultComparer_Equals_m25116_gshared)(__this, ___x, ___y, method)
