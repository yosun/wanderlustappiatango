﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.InternalEyewear/EyewearCalibrationReading
struct EyewearCalibrationReading_t592;
struct EyewearCalibrationReading_t592_marshaled;

void EyewearCalibrationReading_t592_marshal(const EyewearCalibrationReading_t592& unmarshaled, EyewearCalibrationReading_t592_marshaled& marshaled);
void EyewearCalibrationReading_t592_marshal_back(const EyewearCalibrationReading_t592_marshaled& marshaled, EyewearCalibrationReading_t592& unmarshaled);
void EyewearCalibrationReading_t592_marshal_cleanup(EyewearCalibrationReading_t592_marshaled& marshaled);
