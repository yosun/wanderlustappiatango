﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Serialization.FixupRecord
struct FixupRecord_t2385;
// System.Runtime.Serialization.ObjectRecord
struct ObjectRecord_t2381;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Runtime.Serialization.ObjectManager
struct ObjectManager_t2374;

// System.Void System.Runtime.Serialization.FixupRecord::.ctor(System.Runtime.Serialization.ObjectRecord,System.Reflection.MemberInfo,System.Runtime.Serialization.ObjectRecord)
extern "C" void FixupRecord__ctor_m12459 (FixupRecord_t2385 * __this, ObjectRecord_t2381 * ___objectToBeFixed, MemberInfo_t * ___member, ObjectRecord_t2381 * ___objectRequired, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.FixupRecord::FixupImpl(System.Runtime.Serialization.ObjectManager)
extern "C" void FixupRecord_FixupImpl_m12460 (FixupRecord_t2385 * __this, ObjectManager_t2374 * ___manager, const MethodInfo* method) IL2CPP_METHOD_ATTR;
