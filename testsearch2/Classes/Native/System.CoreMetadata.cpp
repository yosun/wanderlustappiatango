﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include <cstring>
#include <string.h>
#include <stdio.h>
#ifndef _MSC_VER
#include <alloca.h>
#else
#include <malloc.h>
#endif
#include <cmath>
#include <limits>
#include <assert.h>
#include "stringLiterals.h"

extern TypeInfo U3CModuleU3E_t1583_il2cpp_TypeInfo;
// <Module>
#include "System_Core_U3CModuleU3E.h"
extern TypeInfo ExtensionAttribute_t905_il2cpp_TypeInfo;
// System.Runtime.CompilerServices.ExtensionAttribute
#include "System_Core_System_Runtime_CompilerServices_ExtensionAttribu.h"
extern TypeInfo MonoTODOAttribute_t1584_il2cpp_TypeInfo;
// System.MonoTODOAttribute
#include "System_Core_System_MonoTODOAttribute.h"
extern TypeInfo Link_t1596_il2cpp_TypeInfo;
extern TypeInfo Enumerator_t1597_il2cpp_TypeInfo;
extern TypeInfo PrimeHelper_t1598_il2cpp_TypeInfo;
extern TypeInfo HashSet_1_t1595_il2cpp_TypeInfo;
extern TypeInfo Check_t1585_il2cpp_TypeInfo;
// System.Linq.Check
#include "System_Core_System_Linq_Check.h"
extern TypeInfo U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo;
extern TypeInfo U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo;
extern TypeInfo Enumerable_t140_il2cpp_TypeInfo;
// System.Linq.Enumerable
#include "System_Core_System_Linq_Enumerable.h"
extern TypeInfo Action_t147_il2cpp_TypeInfo;
// System.Action
#include "System_Core_System_Action.h"
extern TypeInfo Func_2_t1601_il2cpp_TypeInfo;
extern TypeInfo U24ArrayTypeU24136_t1586_il2cpp_TypeInfo;
// <PrivateImplementationDetails>/$ArrayType$136
#include "System_Core_U3CPrivateImplementationDetailsU3E_U24ArrayTypeU.h"
extern TypeInfo U3CPrivateImplementationDetailsU3E_t1587_il2cpp_TypeInfo;
// <PrivateImplementationDetails>
#include "System_Core_U3CPrivateImplementationDetailsU3E.h"
#include <map>
struct TypeInfo;
struct MethodInfo;
TypeInfo* g_System_Core_Assembly_Types[16] = 
{
	&U3CModuleU3E_t1583_il2cpp_TypeInfo,
	&ExtensionAttribute_t905_il2cpp_TypeInfo,
	&MonoTODOAttribute_t1584_il2cpp_TypeInfo,
	&Link_t1596_il2cpp_TypeInfo,
	&Enumerator_t1597_il2cpp_TypeInfo,
	&PrimeHelper_t1598_il2cpp_TypeInfo,
	&HashSet_1_t1595_il2cpp_TypeInfo,
	&Check_t1585_il2cpp_TypeInfo,
	&U3CCreateCastIteratorU3Ec__Iterator0_1_t1599_il2cpp_TypeInfo,
	&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t1600_il2cpp_TypeInfo,
	&Enumerable_t140_il2cpp_TypeInfo,
	&Action_t147_il2cpp_TypeInfo,
	&Func_2_t1601_il2cpp_TypeInfo,
	&U24ArrayTypeU24136_t1586_il2cpp_TypeInfo,
	&U3CPrivateImplementationDetailsU3E_t1587_il2cpp_TypeInfo,
	NULL,
};
extern Il2CppImage g_System_Core_dll_Image;
Il2CppAssembly g_System_Core_Assembly = 
{
	{ "System.Core", NULL, NULL, "\x0\x24\x0\x0\x4\x80\x0\x0\x94\x0\x0\x0\x6\x2\x0\x0\x0\x24\x0\x0\x52\x53\x41\x31\x0\x4\x0\x0\x1\x0\x1\x0\x8D\x56\xC7\x6F\x9E\x86\x49\x38\x30\x49\xF3\x83\xC4\x4B\xE0\xEC\x20\x41\x81\x82\x2A\x6C\x31\xCF\x5E\xB7\xEF\x48\x69\x44\xD0\x32\x18\x8E\xA1\xD3\x92\x7\x63\x71\x2C\xCB\x12\xD7\x5F\xB7\x7E\x98\x11\x14\x9E\x61\x48\xE5\xD3\x2F\xBA\xAB\x37\x61\x1C\x18\x78\xDD\xC1\x9E\x20\xEF\x13\x5D\xC\xB2\xCF\xF2\xBF\xEC\x3D\x11\x58\x10\xC3\xD9\x6\x96\x38\xFE\x4B\xE2\x15\xDB\xF7\x95\x86\x19\x20\xE5\xAB\x6F\x7D\xB2\xE2\xCE\xEF\x13\x6A\xC2\x3D\x5D\xD2\xBF\x3\x17\x0\xAE\xC2\x32\xF6\xC6\xB1\xC7\x85\xB4\x30\x5C\x12\x3B\x37\xAB", { 0x7C, 0xEC, 0x85, 0xD7, 0xBE, 0xA7, 0x79, 0x8E }, 32772, 0, 1, 2, 0, 5, 0 },
	&g_System_Core_dll_Image,
	1,
};
extern const CustomAttributesCacheGenerator g_System_Core_Assembly_AttributeGenerators[30];
static const char* s_StringTable[31] = 
{
	"HashCode",
	"Next",
	"hashset",
	"next",
	"stamp",
	"current",
	"primes_table",
	"INITIAL_SIZE",
	"DEFAULT_LOAD_FACTOR",
	"NO_SLOT",
	"HASH_FLAG",
	"table",
	"links",
	"slots",
	"touched",
	"empty_slot",
	"count",
	"threshold",
	"comparer",
	"si",
	"generation",
	"source",
	"<$s_41>__0",
	"<element>__1",
	"$PC",
	"$current",
	"<$>source",
	"<$s_97>__0",
	"predicate",
	"<$>predicate",
	"$$field-0",
};
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
static const Il2CppFieldDefinition s_FieldTable[36] = 
{
	{ 0, 604, 0, 0 } ,
	{ 1, 604, 0, 0 } ,
	{ 2, 3131, 0, 0 } ,
	{ 3, 113, 0, 0 } ,
	{ 4, 113, 0, 0 } ,
	{ 5, 3132, 0, 0 } ,
	{ 6, 3137, 0, 0 } ,
	{ 7, 211, 0, 0 } ,
	{ 8, 794, 0, 0 } ,
	{ 9, 211, 0, 0 } ,
	{ 10, 211, 0, 0 } ,
	{ 11, 1573, 0, 0 } ,
	{ 12, 3146, 0, 0 } ,
	{ 13, 3148, 0, 0 } ,
	{ 14, 113, 0, 0 } ,
	{ 15, 113, 0, 0 } ,
	{ 16, 113, 0, 0 } ,
	{ 17, 113, 0, 0 } ,
	{ 18, 3149, 0, 0 } ,
	{ 19, 3150, 0, 0 } ,
	{ 20, 113, 0, 0 } ,
	{ 21, 3160, 0, 0 } ,
	{ 22, 3161, 0, 0 } ,
	{ 23, 3162, 0, 0 } ,
	{ 24, 672, 0, 0 } ,
	{ 25, 3162, 0, 0 } ,
	{ 26, 3160, 0, 0 } ,
	{ 21, 3170, 0, 0 } ,
	{ 27, 3171, 0, 0 } ,
	{ 23, 3172, 0, 0 } ,
	{ 28, 3173, 0, 0 } ,
	{ 24, 672, 0, 0 } ,
	{ 25, 3172, 0, 0 } ,
	{ 26, 3170, 0, 0 } ,
	{ 29, 3173, 0, 0 } ,
	{ 30, 3222, offsetof(U3CPrivateImplementationDetailsU3E_t1587_StaticFields, ___U24U24fieldU2D0_0), 0 } ,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
static const Il2CppFieldDefaultValue s_DefaultValues[5] = 
{
	{ 7, 26, 0 },
	{ 8, 15, 4 },
	{ 9, 26, 8 },
	{ 10, 26, 12 },
	{ 35, 3220, 16 },
};
static const uint8_t s_DefaultValueDataTable[152] = 
{
	0x0A,
	0x00,
	0x00,
	0x00,
	0x66,
	0x66,
	0x66,
	0x3F,
	0xFF,
	0xFF,
	0xFF,
	0xFF,
	0x00,
	0x00,
	0x00,
	0x80,
	0x0B,
	0x00,
	0x00,
	0x00,
	0x13,
	0x00,
	0x00,
	0x00,
	0x25,
	0x00,
	0x00,
	0x00,
	0x49,
	0x00,
	0x00,
	0x00,
	0x6D,
	0x00,
	0x00,
	0x00,
	0xA3,
	0x00,
	0x00,
	0x00,
	0xFB,
	0x00,
	0x00,
	0x00,
	0x6F,
	0x01,
	0x00,
	0x00,
	0x2D,
	0x02,
	0x00,
	0x00,
	0x37,
	0x03,
	0x00,
	0x00,
	0xD5,
	0x04,
	0x00,
	0x00,
	0x45,
	0x07,
	0x00,
	0x00,
	0xD9,
	0x0A,
	0x00,
	0x00,
	0x51,
	0x10,
	0x00,
	0x00,
	0x67,
	0x18,
	0x00,
	0x00,
	0x9B,
	0x24,
	0x00,
	0x00,
	0xE9,
	0x36,
	0x00,
	0x00,
	0x61,
	0x52,
	0x00,
	0x00,
	0x8B,
	0x7B,
	0x00,
	0x00,
	0x47,
	0xB9,
	0x00,
	0x00,
	0xE7,
	0x15,
	0x01,
	0x00,
	0xE1,
	0xA0,
	0x01,
	0x00,
	0x49,
	0x71,
	0x02,
	0x00,
	0xE5,
	0xA9,
	0x03,
	0x00,
	0xE3,
	0x7E,
	0x05,
	0x00,
	0x39,
	0x3E,
	0x08,
	0x00,
	0x67,
	0x5D,
	0x0C,
	0x00,
	0x09,
	0x8C,
	0x12,
	0x00,
	0xFF,
	0xD1,
	0x1B,
	0x00,
	0x13,
	0xBB,
	0x29,
	0x00,
	0x8B,
	0x98,
	0x3E,
	0x00,
	0xC1,
	0xE4,
	0x5D,
	0x00,
	0x21,
	0xD7,
	0x8C,
	0x00,
	0xAB,
	0x42,
	0xD3,
	0x00,
};
Il2CppImage g_System_Core_dll_Image = 
{
	 "System.Core.dll" ,
	&g_System_Core_Assembly,
	g_System_Core_Assembly_Types,
	15,
	NULL,
	s_StringTable,
	31,
	s_FieldTable,
	36,
	s_DefaultValues,
	5,
	s_DefaultValueDataTable,
	152,
	30,
	NULL,
	g_System_Core_Assembly_AttributeGenerators,
};
