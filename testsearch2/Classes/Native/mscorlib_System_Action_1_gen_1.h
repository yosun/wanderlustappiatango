﻿#pragma once
#include <stdint.h>
// Vuforia.Surface
struct Surface_t106;
// System.IAsyncResult
struct IAsyncResult_t311;
// System.AsyncCallback
struct AsyncCallback_t312;
// System.Object
struct Object_t;
// System.Void
#include "mscorlib_System_Void.h"
// System.MulticastDelegate
#include "mscorlib_System_MulticastDelegate.h"
// System.Action`1<Vuforia.Surface>
struct  Action_1_t138  : public MulticastDelegate_t314
{
};
