﻿#pragma once
#include <stdint.h>
// System.Object
struct Object_t;
// DG.Tweening.Tween
struct Tween_t940;
// DG.Tweening.Core.DOTweenComponent
struct DOTweenComponent_t941;
// System.Object
#include "mscorlib_System_Object.h"
// DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a
struct  U3CWaitForStartU3Ed__a_t947  : public Object_t
{
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::<>2__current
	Object_t * ___U3CU3E2__current_0;
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::<>1__state
	int32_t ___U3CU3E1__state_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::t
	Tween_t940 * ___t_2;
	// DG.Tweening.Core.DOTweenComponent DG.Tweening.Core.DOTweenComponent/<WaitForStart>d__a::<>4__this
	DOTweenComponent_t941 * ___U3CU3E4__this_3;
};
