﻿#pragma once
#include <stdint.h>
// UnityEngine.Renderer
struct Renderer_t17;
// UnityEngine.MonoBehaviour
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
// UnityEngine.Vector2
#include "UnityEngine_UnityEngine_Vector2.h"
// AnimateTexture
struct  AnimateTexture_t18  : public MonoBehaviour_t7
{
	// UnityEngine.Vector2 AnimateTexture::uvOffset
	Vector2_t19  ___uvOffset_2;
	// UnityEngine.Vector2 AnimateTexture::uvAnimationRate
	Vector2_t19  ___uvAnimationRate_3;
	// UnityEngine.Renderer AnimateTexture::renderer
	Renderer_t17 * ___renderer_4;
};
