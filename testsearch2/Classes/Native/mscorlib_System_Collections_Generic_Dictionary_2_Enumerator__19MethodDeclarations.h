﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordResult>
struct Enumerator_t3497;
// System.Object
struct Object_t;
// Vuforia.WordResult
struct WordResult_t701;
// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.WordResult>
struct Dictionary_2_t694;
// System.Collections.DictionaryEntry
#include "mscorlib_System_Collections_DictionaryEntry.h"
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.WordResult>
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_21.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordResult>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
// System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__7MethodDeclarations.h"
#define Enumerator__ctor_m20482(__this, ___dictionary, method) (( void (*) (Enumerator_t3497 *, Dictionary_2_t694 *, const MethodInfo*))Enumerator__ctor_m16521_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordResult>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m20483(__this, method) (( Object_t * (*) (Enumerator_t3497 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16522_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordResult>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m20484(__this, method) (( DictionaryEntry_t2002  (*) (Enumerator_t3497 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16523_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordResult>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m20485(__this, method) (( Object_t * (*) (Enumerator_t3497 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16524_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordResult>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m20486(__this, method) (( Object_t * (*) (Enumerator_t3497 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16525_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordResult>::MoveNext()
#define Enumerator_MoveNext_m20487(__this, method) (( bool (*) (Enumerator_t3497 *, const MethodInfo*))Enumerator_MoveNext_m16526_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordResult>::get_Current()
#define Enumerator_get_Current_m20488(__this, method) (( KeyValuePair_2_t3496  (*) (Enumerator_t3497 *, const MethodInfo*))Enumerator_get_Current_m16527_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordResult>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m20489(__this, method) (( int32_t (*) (Enumerator_t3497 *, const MethodInfo*))Enumerator_get_CurrentKey_m16528_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordResult>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m20490(__this, method) (( WordResult_t701 * (*) (Enumerator_t3497 *, const MethodInfo*))Enumerator_get_CurrentValue_m16529_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordResult>::VerifyState()
#define Enumerator_VerifyState_m20491(__this, method) (( void (*) (Enumerator_t3497 *, const MethodInfo*))Enumerator_VerifyState_m16530_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordResult>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m20492(__this, method) (( void (*) (Enumerator_t3497 *, const MethodInfo*))Enumerator_VerifyCurrent_m16531_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,Vuforia.WordResult>::Dispose()
#define Enumerator_Dispose_m20493(__this, method) (( void (*) (Enumerator_t3497 *, const MethodInfo*))Enumerator_Dispose_m16532_gshared)(__this, method)
