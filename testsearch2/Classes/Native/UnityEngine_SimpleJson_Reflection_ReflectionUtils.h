﻿#pragma once
#include <stdint.h>
// System.Object[]
struct ObjectU5BU5D_t124;
// System.Object
#include "mscorlib_System_Object.h"
// SimpleJson.Reflection.ReflectionUtils
struct  ReflectionUtils_t1299  : public Object_t
{
};
struct ReflectionUtils_t1299_StaticFields{
	// System.Object[] SimpleJson.Reflection.ReflectionUtils::EmptyObjects
	ObjectU5BU5D_t124* ___EmptyObjects_0;
};
