﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// Vuforia.CylinderTargetBehaviour
struct CylinderTargetBehaviour_t43;

// System.Void Vuforia.CylinderTargetBehaviour::.ctor()
extern "C" void CylinderTargetBehaviour__ctor_m143 (CylinderTargetBehaviour_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
