﻿#include "il2cpp-config.h"
#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttribute.h"
// System.Runtime.InteropServices.GuidAttribute
#include "mscorlib_System_Runtime_InteropServices_GuidAttributeMethodDeclarations.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttribute.h"
// System.Reflection.AssemblyDescriptionAttribute
#include "mscorlib_System_Reflection_AssemblyDescriptionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttribute.h"
// System.Reflection.AssemblyConfigurationAttribute
#include "mscorlib_System_Reflection_AssemblyConfigurationAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttribute.h"
// System.Reflection.AssemblyCompanyAttribute
#include "mscorlib_System_Reflection_AssemblyCompanyAttributeMethodDeclarations.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttribute.h"
// System.Reflection.AssemblyProductAttribute
#include "mscorlib_System_Reflection_AssemblyProductAttributeMethodDeclarations.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttribute.h"
// System.Reflection.AssemblyCopyrightAttribute
#include "mscorlib_System_Reflection_AssemblyCopyrightAttributeMethodDeclarations.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttribute.h"
// System.Runtime.InteropServices.ComVisibleAttribute
#include "mscorlib_System_Runtime_InteropServices_ComVisibleAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttribute.h"
// System.Reflection.AssemblyTitleAttribute
#include "mscorlib_System_Reflection_AssemblyTitleAttributeMethodDeclarations.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilit.h"
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
#include "mscorlib_System_Runtime_CompilerServices_RuntimeCompatibilitMethodDeclarations.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttribute.h"
// System.Reflection.AssemblyFileVersionAttribute
#include "mscorlib_System_Reflection_AssemblyFileVersionAttributeMethodDeclarations.h"
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttribute.h"
// System.Reflection.AssemblyTrademarkAttribute
#include "mscorlib_System_Reflection_AssemblyTrademarkAttributeMethodDeclarations.h"
extern TypeInfo* GuidAttribute_t485_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyConfigurationAttribute_t487_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyProductAttribute_t489_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var;
extern TypeInfo* ComVisibleAttribute_t491_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var;
extern TypeInfo* RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo_var;
extern TypeInfo* AssemblyTrademarkAttribute_t494_il2cpp_TypeInfo_var;
void g_UnityEngine_UI_Assembly_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GuidAttribute_t485_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(490);
		AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(491);
		AssemblyConfigurationAttribute_t487_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(492);
		AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(493);
		AssemblyProductAttribute_t489_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(494);
		AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(495);
		ComVisibleAttribute_t491_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(496);
		AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(497);
		RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(84);
		AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(498);
		AssemblyTrademarkAttribute_t494_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(499);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 11;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		GuidAttribute_t485 * tmp;
		tmp = (GuidAttribute_t485 *)il2cpp_codegen_object_new (GuidAttribute_t485_il2cpp_TypeInfo_var);
		GuidAttribute__ctor_m2448(tmp, il2cpp_codegen_string_new_wrapper("d4f464c7-9b15-460d-b4bc-2cacd1c1df73"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AssemblyDescriptionAttribute_t486 * tmp;
		tmp = (AssemblyDescriptionAttribute_t486 *)il2cpp_codegen_object_new (AssemblyDescriptionAttribute_t486_il2cpp_TypeInfo_var);
		AssemblyDescriptionAttribute__ctor_m2449(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AssemblyConfigurationAttribute_t487 * tmp;
		tmp = (AssemblyConfigurationAttribute_t487 *)il2cpp_codegen_object_new (AssemblyConfigurationAttribute_t487_il2cpp_TypeInfo_var);
		AssemblyConfigurationAttribute__ctor_m2450(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCompanyAttribute_t488 * tmp;
		tmp = (AssemblyCompanyAttribute_t488 *)il2cpp_codegen_object_new (AssemblyCompanyAttribute_t488_il2cpp_TypeInfo_var);
		AssemblyCompanyAttribute__ctor_m2451(tmp, il2cpp_codegen_string_new_wrapper("Microsoft"), NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
	{
		AssemblyProductAttribute_t489 * tmp;
		tmp = (AssemblyProductAttribute_t489 *)il2cpp_codegen_object_new (AssemblyProductAttribute_t489_il2cpp_TypeInfo_var);
		AssemblyProductAttribute__ctor_m2452(tmp, il2cpp_codegen_string_new_wrapper("guisystem"), NULL);
		cache->attributes[4] = (Il2CppObject*)tmp;
	}
	{
		AssemblyCopyrightAttribute_t490 * tmp;
		tmp = (AssemblyCopyrightAttribute_t490 *)il2cpp_codegen_object_new (AssemblyCopyrightAttribute_t490_il2cpp_TypeInfo_var);
		AssemblyCopyrightAttribute__ctor_m2453(tmp, il2cpp_codegen_string_new_wrapper("Copyright © Microsoft 2013"), NULL);
		cache->attributes[5] = (Il2CppObject*)tmp;
	}
	{
		ComVisibleAttribute_t491 * tmp;
		tmp = (ComVisibleAttribute_t491 *)il2cpp_codegen_object_new (ComVisibleAttribute_t491_il2cpp_TypeInfo_var);
		ComVisibleAttribute__ctor_m2454(tmp, false, NULL);
		cache->attributes[6] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTitleAttribute_t492 * tmp;
		tmp = (AssemblyTitleAttribute_t492 *)il2cpp_codegen_object_new (AssemblyTitleAttribute_t492_il2cpp_TypeInfo_var);
		AssemblyTitleAttribute__ctor_m2455(tmp, il2cpp_codegen_string_new_wrapper("guisystem"), NULL);
		cache->attributes[7] = (Il2CppObject*)tmp;
	}
	{
		RuntimeCompatibilityAttribute_t167 * tmp;
		tmp = (RuntimeCompatibilityAttribute_t167 *)il2cpp_codegen_object_new (RuntimeCompatibilityAttribute_t167_il2cpp_TypeInfo_var);
		RuntimeCompatibilityAttribute__ctor_m534(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m535(tmp, true, NULL);
		cache->attributes[8] = (Il2CppObject*)tmp;
	}
	{
		AssemblyFileVersionAttribute_t493 * tmp;
		tmp = (AssemblyFileVersionAttribute_t493 *)il2cpp_codegen_object_new (AssemblyFileVersionAttribute_t493_il2cpp_TypeInfo_var);
		AssemblyFileVersionAttribute__ctor_m2456(tmp, il2cpp_codegen_string_new_wrapper("1.0.0.0"), NULL);
		cache->attributes[9] = (Il2CppObject*)tmp;
	}
	{
		AssemblyTrademarkAttribute_t494 * tmp;
		tmp = (AssemblyTrademarkAttribute_t494 *)il2cpp_codegen_object_new (AssemblyTrademarkAttribute_t494_il2cpp_TypeInfo_var);
		AssemblyTrademarkAttribute__ctor_m2457(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
		cache->attributes[10] = (Il2CppObject*)tmp;
	}
}
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttribute.h"
// System.FlagsAttribute
#include "mscorlib_System_FlagsAttributeMethodDeclarations.h"
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void EventHandle_t201_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
// UnityEngine.AddComponentMenu
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void EventSystem_t116_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2476(tmp, il2cpp_codegen_string_new_wrapper("Event/Event System"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAt.h"
// UnityEngine.Serialization.FormerlySerializedAsAttribute
#include "UnityEngine_UnityEngine_Serialization_FormerlySerializedAsAtMethodDeclarations.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeField.h"
// UnityEngine.SerializeField
#include "UnityEngine_UnityEngine_SerializeFieldMethodDeclarations.h"
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void EventSystem_t116_CustomAttributesCacheGenerator_m_FirstSelected(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_Selected"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void EventSystem_t116_CustomAttributesCacheGenerator_m_sendNavigationEvents(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void EventSystem_t116_CustomAttributesCacheGenerator_m_DragThreshold(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAt.h"
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
#include "mscorlib_System_Runtime_CompilerServices_CompilerGeneratedAtMethodDeclarations.h"
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void EventSystem_t116_CustomAttributesCacheGenerator_U3CcurrentU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void EventSystem_t116_CustomAttributesCacheGenerator_EventSystem_get_current_m331(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void EventSystem_t116_CustomAttributesCacheGenerator_EventSystem_set_current_m784(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttribute.h"
// System.ObsoleteAttribute
#include "mscorlib_System_ObsoleteAttributeMethodDeclarations.h"
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void EventSystem_t116_CustomAttributesCacheGenerator_EventSystem_t116____lastSelectedGameObject_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("lastSelectedGameObject is no longer supported"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void EventTrigger_t211_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2476(tmp, il2cpp_codegen_string_new_wrapper("Event/Event Trigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void EventTrigger_t211_CustomAttributesCacheGenerator_m_Delegates(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("delegates"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void EventTrigger_t211_CustomAttributesCacheGenerator_delegates(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2479(tmp, il2cpp_codegen_string_new_wrapper("Please use triggers instead (UnityUpgradable)"), true, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ExecuteEvents_t233_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache13(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ExecuteEvents_t233_CustomAttributesCacheGenerator_ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m867(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void AxisEventData_t239_CustomAttributesCacheGenerator_U3CmoveVectorU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void AxisEventData_t239_CustomAttributesCacheGenerator_U3CmoveDirU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void AxisEventData_t239_CustomAttributesCacheGenerator_AxisEventData_get_moveVector_m892(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void AxisEventData_t239_CustomAttributesCacheGenerator_AxisEventData_set_moveVector_m893(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void AxisEventData_t239_CustomAttributesCacheGenerator_AxisEventData_get_moveDir_m894(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void AxisEventData_t239_CustomAttributesCacheGenerator_AxisEventData_set_moveDir_m895(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CpointerEnterU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3ClastPressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CrawPointerPressU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CpointerDragU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CpointerCurrentRaycastU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CpointerPressRaycastU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CeligibleForClickU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CpointerIdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CpositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CdeltaU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CpressPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CworldPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CworldNormalU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CclickTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CclickCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CscrollDeltaU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CuseDragThresholdU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CdraggingU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_U3CbuttonU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_pointerEnter_m904(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_pointerEnter_m905(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_lastPress_m906(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_lastPress_m907(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_rawPointerPress_m908(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_rawPointerPress_m909(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_pointerDrag_m910(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_pointerDrag_m911(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_pointerCurrentRaycast_m912(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_pointerCurrentRaycast_m913(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_pointerPressRaycast_m914(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_pointerPressRaycast_m915(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_eligibleForClick_m916(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_eligibleForClick_m917(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_pointerId_m918(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_pointerId_m919(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_position_m920(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_position_m921(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_delta_m922(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_delta_m923(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_pressPosition_m924(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_pressPosition_m925(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_worldPosition_m926(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_worldPosition_m927(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_worldNormal_m928(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_worldNormal_m929(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_clickTime_m930(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_clickTime_m931(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_clickCount_m932(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_clickCount_m933(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_scrollDelta_m934(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_scrollDelta_m935(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_useDragThreshold_m936(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_useDragThreshold_m937(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_dragging_m938(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_dragging_m939(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_button_m940(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_button_m941(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_t243____worldPosition_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Use either pointerCurrentRaycast.worldPosition or pointerPressRaycast.worldPosition"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_t243____worldNormal_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2478(tmp, il2cpp_codegen_string_new_wrapper("Use either pointerCurrentRaycast.worldNormal or pointerPressRaycast.worldNormal"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponent.h"
// UnityEngine.RequireComponent
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
// UnityEngine.EventSystems.EventSystem
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem.h"
extern const Il2CppType* EventSystem_t116_0_0_0_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
void BaseInputModule_t203_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		EventSystem_t116_0_0_0_var = il2cpp_codegen_type_from_index(19);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(EventSystem_t116_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void StandaloneInputModule_t252_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2476(tmp, il2cpp_codegen_string_new_wrapper("Event/Standalone Input Module"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_HorizontalAxis(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_VerticalAxis(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_SubmitButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_CancelButton(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_InputActionsPerSecond(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_RepeatDelay(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_AllowActivationOnMobileDevice(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void StandaloneInputModule_t252_CustomAttributesCacheGenerator_StandaloneInputModule_t252____inputMode_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2479(tmp, il2cpp_codegen_string_new_wrapper("Mode is no longer needed on input module as it handles both mouse and keyboard simultaneously."), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void InputMode_t251_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2479(tmp, il2cpp_codegen_string_new_wrapper("Mode is no longer needed on input module as it handles both mouse and keyboard simultaneously."), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void TouchInputModule_t253_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2476(tmp, il2cpp_codegen_string_new_wrapper("Event/Touch Input Module"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void TouchInputModule_t253_CustomAttributesCacheGenerator_m_AllowActivationOnStandalone(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void BaseRaycaster_t237_CustomAttributesCacheGenerator_BaseRaycaster_t237____priority_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2479(tmp, il2cpp_codegen_string_new_wrapper("Please use sortOrderPriority and renderOrderPriority"), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Camera
#include "UnityEngine_UnityEngine_Camera.h"
extern const Il2CppType* Camera_t3_0_0_0_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void Physics2DRaycaster_t254_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t3_0_0_0_var = il2cpp_codegen_type_from_index(29);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(Camera_t3_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2476(tmp, il2cpp_codegen_string_new_wrapper("Event/Physics 2D Raycaster"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Camera_t3_0_0_0_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t255_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Camera_t3_0_0_0_var = il2cpp_codegen_type_from_index(29);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(Camera_t3_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2476(tmp, il2cpp_codegen_string_new_wrapper("Event/Physics Raycaster"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t255_CustomAttributesCacheGenerator_m_EventMask(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t255_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void PhysicsRaycaster_t255_CustomAttributesCacheGenerator_PhysicsRaycaster_U3CRaycastU3Em__1_m1050(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttribute.h"
// System.Diagnostics.DebuggerHiddenAttribute
#include "mscorlib_System_Diagnostics_DebuggerHiddenAttributeMethodDeclarations.h"
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void TweenRunner_1_t502_CustomAttributesCacheGenerator_TweenRunner_1_Start_m2499(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t504_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t504_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2504(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t504_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2505(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t504_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Dispose_m2507(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CStartU3Ec__Iterator0_t504_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Reset_m2508(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void AnimationTriggers_t261_CustomAttributesCacheGenerator_m_NormalTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("normalTrigger"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void AnimationTriggers_t261_CustomAttributesCacheGenerator_m_HighlightedTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("highlightedTrigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_SelectedTrigger"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void AnimationTriggers_t261_CustomAttributesCacheGenerator_m_PressedTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("pressedTrigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void AnimationTriggers_t261_CustomAttributesCacheGenerator_m_DisabledTrigger(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("disabledTrigger"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void Button_t264_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Button"), 30, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Button_t264_CustomAttributesCacheGenerator_m_OnClick(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("onClick"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void Button_t264_CustomAttributesCacheGenerator_Button_OnFinishSubmit_m1089(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t265_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t265_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1078(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t265_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1079(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t265_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Dispose_m1081(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3COnFinishSubmitU3Ec__Iterator1_t265_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Reset_m1082(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t268_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t268_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t268_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__2_m1106(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CanvasUpdateRegistry_t268_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__3_m1107(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void ColorBlock_t272_CustomAttributesCacheGenerator_m_NormalColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("normalColor"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ColorBlock_t272_CustomAttributesCacheGenerator_m_HighlightedColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_SelectedColor"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("highlightedColor"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ColorBlock_t272_CustomAttributesCacheGenerator_m_PressedColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("pressedColor"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ColorBlock_t272_CustomAttributesCacheGenerator_m_DisabledColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("disabledColor"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttribute.h"
// UnityEngine.RangeAttribute
#include "UnityEngine_UnityEngine_RangeAttributeMethodDeclarations.h"
extern TypeInfo* RangeAttribute_t505_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ColorBlock_t272_CustomAttributesCacheGenerator_m_ColorMultiplier(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(505);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t505 * tmp;
		tmp = (RangeAttribute_t505 *)il2cpp_codegen_object_new (RangeAttribute_t505_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2513(tmp, 1.0f, 5.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void ColorBlock_t272_CustomAttributesCacheGenerator_m_FadeDuration(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("fadeDuration"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void FontData_t274_CustomAttributesCacheGenerator_m_Font(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("font"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void FontData_t274_CustomAttributesCacheGenerator_m_FontSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("fontSize"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void FontData_t274_CustomAttributesCacheGenerator_m_FontStyle(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("fontStyle"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void FontData_t274_CustomAttributesCacheGenerator_m_BestFit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void FontData_t274_CustomAttributesCacheGenerator_m_MinSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void FontData_t274_CustomAttributesCacheGenerator_m_MaxSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void FontData_t274_CustomAttributesCacheGenerator_m_Alignment(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("alignment"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void FontData_t274_CustomAttributesCacheGenerator_m_RichText(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("richText"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void FontData_t274_CustomAttributesCacheGenerator_m_HorizontalOverflow(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void FontData_t274_CustomAttributesCacheGenerator_m_VerticalOverflow(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void FontData_t274_CustomAttributesCacheGenerator_m_LineSpacing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.CanvasRenderer
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
// UnityEngine.RectTransform
#include "UnityEngine_UnityEngine_RectTransform.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
// UnityEngine.ExecuteInEditMode
#include "UnityEngine_UnityEngine_ExecuteInEditModeMethodDeclarations.h"
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
// UnityEngine.DisallowMultipleComponent
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
extern const Il2CppType* CanvasRenderer_t280_0_0_0_var;
extern const Il2CppType* RectTransform_t279_0_0_0_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t506_il2cpp_TypeInfo_var;
extern TypeInfo* DisallowMultipleComponent_t507_il2cpp_TypeInfo_var;
void Graphic_t285_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CanvasRenderer_t280_0_0_0_var = il2cpp_codegen_type_from_index(385);
		RectTransform_t279_0_0_0_var = il2cpp_codegen_type_from_index(382);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		ExecuteInEditMode_t506_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(506);
		DisallowMultipleComponent_t507_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(507);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(CanvasRenderer_t280_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(RectTransform_t279_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t506 * tmp;
		tmp = (ExecuteInEditMode_t506 *)il2cpp_codegen_object_new (ExecuteInEditMode_t506_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2514(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		DisallowMultipleComponent_t507 * tmp;
		tmp = (DisallowMultipleComponent_t507 *)il2cpp_codegen_object_new (DisallowMultipleComponent_t507_il2cpp_TypeInfo_var);
		DisallowMultipleComponent__ctor_m2515(tmp, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Graphic_t285_CustomAttributesCacheGenerator_m_Material(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_Mat"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Graphic_t285_CustomAttributesCacheGenerator_m_Color(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Graphic_t285_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Graphic_t285_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Graphic_t285_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__4_m1196(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Graphic_t285_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__5_m1197(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.Canvas
#include "UnityEngine_UnityEngine_Canvas.h"
extern const Il2CppType* Canvas_t281_0_0_0_var;
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
void GraphicRaycaster_t289_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t281_0_0_0_var = il2cpp_codegen_type_from_index(383);
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2476(tmp, il2cpp_codegen_string_new_wrapper("Event/Graphic Raycaster"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(Canvas_t281_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void GraphicRaycaster_t289_CustomAttributesCacheGenerator_m_IgnoreReversedGraphics(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("ignoreReversedGraphics"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void GraphicRaycaster_t289_CustomAttributesCacheGenerator_m_BlockingObjects(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("blockingObjects"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GraphicRaycaster_t289_CustomAttributesCacheGenerator_m_BlockingMask(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void GraphicRaycaster_t289_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void GraphicRaycaster_t289_CustomAttributesCacheGenerator_GraphicRaycaster_U3CRaycastU3Em__6_m1212(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void Image_t301_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Image"), 10, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Image_t301_CustomAttributesCacheGenerator_m_Sprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_Frame"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Image_t301_CustomAttributesCacheGenerator_m_Type(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Image_t301_CustomAttributesCacheGenerator_m_PreserveAspect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Image_t301_CustomAttributesCacheGenerator_m_FillCenter(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Image_t301_CustomAttributesCacheGenerator_m_FillMethod(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t505_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Image_t301_CustomAttributesCacheGenerator_m_FillAmount(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(505);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t505 * tmp;
		tmp = (RangeAttribute_t505 *)il2cpp_codegen_object_new (RangeAttribute_t505_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2513(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Image_t301_CustomAttributesCacheGenerator_m_FillClockwise(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Image_t301_CustomAttributesCacheGenerator_m_FillOrigin(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Input Field"), 31, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_TextComponent(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("text"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_Placeholder(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_ContentType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_InputType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("inputType"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_AsteriskChar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("asteriskChar"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_KeyboardType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("keyboardType"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_LineType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_HideMobileInput(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("hideMobileInput"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_CharacterValidation(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("validation"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_CharacterLimit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("characterLimit"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_EndEdit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("onSubmit"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_OnSubmit"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_OnValueChange(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("onValueChange"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_OnValidateInput(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("onValidateInput"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_SelectionColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("selectionColor"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("mValue"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t505_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_m_CaretBlinkRate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(505);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t505 * tmp;
		tmp = (RangeAttribute_t505 *)il2cpp_codegen_object_new (RangeAttribute_t505_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2513(tmp, 0.0f, 4.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_InputField_CaretBlink_m1337(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_InputField_MouseDragOutsideRect_m1354(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttribute.h"
// System.ParamArrayAttribute
#include "mscorlib_System_ParamArrayAttributeMethodDeclarations.h"
extern TypeInfo* ParamArrayAttribute_t508_il2cpp_TypeInfo_var;
void InputField_t12_CustomAttributesCacheGenerator_InputField_t12_InputField_SetToCustomIfContentTypeIsNot_m1405_Arg0_ParameterInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ParamArrayAttribute_t508_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(508);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ParamArrayAttribute_t508 * tmp;
		tmp = (ParamArrayAttribute_t508 *)il2cpp_codegen_object_new (ParamArrayAttribute_t508_il2cpp_TypeInfo_var);
		ParamArrayAttribute__ctor_m2519(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t315_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t315_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1275(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t315_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1276(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t315_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Dispose_m1278(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CCaretBlinkU3Ec__Iterator2_t315_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Reset_m1279(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t316_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t316_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1281(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t316_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1282(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t316_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Dispose_m1284(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CMouseDragOutsideRectU3Ec__Iterator3_t316_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Reset_m1285(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Navigation_t326_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("mode"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void Navigation_t326_CustomAttributesCacheGenerator_m_SelectOnUp(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("selectOnUp"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Navigation_t326_CustomAttributesCacheGenerator_m_SelectOnDown(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("selectOnDown"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void Navigation_t326_CustomAttributesCacheGenerator_m_SelectOnLeft(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("selectOnLeft"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Navigation_t326_CustomAttributesCacheGenerator_m_SelectOnRight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("selectOnRight"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FlagsAttribute_t495_il2cpp_TypeInfo_var;
void Mode_t325_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FlagsAttribute_t495_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(500);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FlagsAttribute_t495 * tmp;
		tmp = (FlagsAttribute_t495 *)il2cpp_codegen_object_new (FlagsAttribute_t495_il2cpp_TypeInfo_var);
		FlagsAttribute__ctor_m2458(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void RawImage_t328_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Raw Image"), 12, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void RawImage_t328_CustomAttributesCacheGenerator_m_Texture(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_Tex"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void RawImage_t328_CustomAttributesCacheGenerator_m_UVRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t279_0_0_0_var;
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
void Scrollbar_t333_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t279_0_0_0_var = il2cpp_codegen_type_from_index(382);
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Scrollbar"), 32, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(RectTransform_t279_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Scrollbar_t333_CustomAttributesCacheGenerator_m_HandleRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Scrollbar_t333_CustomAttributesCacheGenerator_m_Direction(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* RangeAttribute_t505_il2cpp_TypeInfo_var;
void Scrollbar_t333_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		RangeAttribute_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(505);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RangeAttribute_t505 * tmp;
		tmp = (RangeAttribute_t505 *)il2cpp_codegen_object_new (RangeAttribute_t505_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2513(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* RangeAttribute_t505_il2cpp_TypeInfo_var;
void Scrollbar_t333_CustomAttributesCacheGenerator_m_Size(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		RangeAttribute_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(505);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RangeAttribute_t505 * tmp;
		tmp = (RangeAttribute_t505 *)il2cpp_codegen_object_new (RangeAttribute_t505_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2513(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t505_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Scrollbar_t333_CustomAttributesCacheGenerator_m_NumberOfSteps(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(505);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t505 * tmp;
		tmp = (RangeAttribute_t505 *)il2cpp_codegen_object_new (RangeAttribute_t505_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2513(tmp, 0.0f, 11.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttribute.h"
// UnityEngine.SpaceAttribute
#include "UnityEngine_UnityEngine_SpaceAttributeMethodDeclarations.h"
extern TypeInfo* SpaceAttribute_t509_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Scrollbar_t333_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SpaceAttribute_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(509);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SpaceAttribute_t509 * tmp;
		tmp = (SpaceAttribute_t509 *)il2cpp_codegen_object_new (SpaceAttribute_t509_il2cpp_TypeInfo_var);
		SpaceAttribute__ctor_m2520(tmp, 6.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void Scrollbar_t333_CustomAttributesCacheGenerator_Scrollbar_ClickRepeat_m1480(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t334_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t334_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1446(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t334_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1447(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t334_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Dispose_m1449(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var;
void U3CClickRepeatU3Ec__Iterator4_t334_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Reset_m1450(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(504);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DebuggerHiddenAttribute_t503 * tmp;
		tmp = (DebuggerHiddenAttribute_t503 *)il2cpp_codegen_object_new (DebuggerHiddenAttribute_t503_il2cpp_TypeInfo_var);
		DebuggerHiddenAttribute__ctor_m2500(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttribute.h"
// UnityEngine.SelectionBaseAttribute
#include "UnityEngine_UnityEngine_SelectionBaseAttributeMethodDeclarations.h"
extern const Il2CppType* RectTransform_t279_0_0_0_var;
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t506_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
extern TypeInfo* SelectionBaseAttribute_t510_il2cpp_TypeInfo_var;
void ScrollRect_t339_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t279_0_0_0_var = il2cpp_codegen_type_from_index(382);
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		ExecuteInEditMode_t506_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(506);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		SelectionBaseAttribute_t510_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(510);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Scroll Rect"), 33, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t506 * tmp;
		tmp = (ExecuteInEditMode_t506 *)il2cpp_codegen_object_new (ExecuteInEditMode_t506_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2514(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(RectTransform_t279_0_0_0_var), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		SelectionBaseAttribute_t510 * tmp;
		tmp = (SelectionBaseAttribute_t510 *)il2cpp_codegen_object_new (SelectionBaseAttribute_t510_il2cpp_TypeInfo_var);
		SelectionBaseAttribute__ctor_m2521(tmp, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ScrollRect_t339_CustomAttributesCacheGenerator_m_Content(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ScrollRect_t339_CustomAttributesCacheGenerator_m_Horizontal(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ScrollRect_t339_CustomAttributesCacheGenerator_m_Vertical(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ScrollRect_t339_CustomAttributesCacheGenerator_m_MovementType(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ScrollRect_t339_CustomAttributesCacheGenerator_m_Elasticity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ScrollRect_t339_CustomAttributesCacheGenerator_m_Inertia(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ScrollRect_t339_CustomAttributesCacheGenerator_m_DecelerationRate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ScrollRect_t339_CustomAttributesCacheGenerator_m_ScrollSensitivity(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ScrollRect_t339_CustomAttributesCacheGenerator_m_HorizontalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ScrollRect_t339_CustomAttributesCacheGenerator_m_VerticalScrollbar(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ScrollRect_t339_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* DisallowMultipleComponent_t507_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t506_il2cpp_TypeInfo_var;
extern TypeInfo* SelectionBaseAttribute_t510_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisallowMultipleComponent_t507_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(507);
		ExecuteInEditMode_t506_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(506);
		SelectionBaseAttribute_t510_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(510);
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 4;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DisallowMultipleComponent_t507 * tmp;
		tmp = (DisallowMultipleComponent_t507 *)il2cpp_codegen_object_new (DisallowMultipleComponent_t507_il2cpp_TypeInfo_var);
		DisallowMultipleComponent__ctor_m2515(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t506 * tmp;
		tmp = (ExecuteInEditMode_t506 *)il2cpp_codegen_object_new (ExecuteInEditMode_t506_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2514(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SelectionBaseAttribute_t510 * tmp;
		tmp = (SelectionBaseAttribute_t510 *)il2cpp_codegen_object_new (SelectionBaseAttribute_t510_il2cpp_TypeInfo_var);
		SelectionBaseAttribute__ctor_m2521(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Selectable"), 70, NULL);
		cache->attributes[3] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_m_Navigation(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("navigation"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_m_Transition(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("transition"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_m_Colors(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("colors"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_m_SpriteState(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("spriteState"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_m_AnimationTriggers(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("animationTriggers"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttribute.h"
// UnityEngine.TooltipAttribute
#include "UnityEngine_UnityEngine_TooltipAttributeMethodDeclarations.h"
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t511_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_m_Interactable(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		TooltipAttribute_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t511 * tmp;
		tmp = (TooltipAttribute_t511 *)il2cpp_codegen_object_new (TooltipAttribute_t511_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Can the Selectable be interacted with?"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_m_TargetGraphic(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_HighlightGraphic"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("highlightGraphic"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_U3CisPointerInsideU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_U3CisPointerDownU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_U3ChasSelectionU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_Selectable_get_isPointerInside_m1565(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_Selectable_set_isPointerInside_m1566(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_Selectable_get_isPointerDown_m1567(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_Selectable_set_isPointerDown_m1568(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_Selectable_get_hasSelection_m1569(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_Selectable_set_hasSelection_m1570(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ObsoleteAttribute_t499_il2cpp_TypeInfo_var;
void Selectable_t266_CustomAttributesCacheGenerator_Selectable_IsPressed_m1596(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObsoleteAttribute_t499_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(503);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ObsoleteAttribute_t499 * tmp;
		tmp = (ObsoleteAttribute_t499 *)il2cpp_codegen_object_new (ObsoleteAttribute_t499_il2cpp_TypeInfo_var);
		ObsoleteAttribute__ctor_m2479(tmp, il2cpp_codegen_string_new_wrapper("Is Pressed no longer requires eventData"), false, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t279_0_0_0_var;
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
void Slider_t30_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t279_0_0_0_var = il2cpp_codegen_type_from_index(382);
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Slider"), 34, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(RectTransform_t279_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Slider_t30_CustomAttributesCacheGenerator_m_FillRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Slider_t30_CustomAttributesCacheGenerator_m_HandleRect(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* SpaceAttribute_t509_il2cpp_TypeInfo_var;
void Slider_t30_CustomAttributesCacheGenerator_m_Direction(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		SpaceAttribute_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(509);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SpaceAttribute_t509 * tmp;
		tmp = (SpaceAttribute_t509 *)il2cpp_codegen_object_new (SpaceAttribute_t509_il2cpp_TypeInfo_var);
		SpaceAttribute__ctor_m2520(tmp, 6.0f, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Slider_t30_CustomAttributesCacheGenerator_m_MinValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Slider_t30_CustomAttributesCacheGenerator_m_MaxValue(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Slider_t30_CustomAttributesCacheGenerator_m_WholeNumbers(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Slider_t30_CustomAttributesCacheGenerator_m_Value(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SpaceAttribute_t509_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Slider_t30_CustomAttributesCacheGenerator_m_OnValueChanged(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SpaceAttribute_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(509);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SpaceAttribute_t509 * tmp;
		tmp = (SpaceAttribute_t509 *)il2cpp_codegen_object_new (SpaceAttribute_t509_il2cpp_TypeInfo_var);
		SpaceAttribute__ctor_m2520(tmp, 6.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void SpriteState_t345_CustomAttributesCacheGenerator_m_HighlightedSprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("highlightedSprite"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_SelectedSprite"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void SpriteState_t345_CustomAttributesCacheGenerator_m_PressedSprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("pressedSprite"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void SpriteState_t345_CustomAttributesCacheGenerator_m_DisabledSprite(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("disabledSprite"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void Text_t31_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Text"), 11, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Text_t31_CustomAttributesCacheGenerator_m_FontData(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttribute.h"
// UnityEngine.TextAreaAttribute
#include "UnityEngine_UnityEngine_TextAreaAttributeMethodDeclarations.h"
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* TextAreaAttribute_t512_il2cpp_TypeInfo_var;
void Text_t31_CustomAttributesCacheGenerator_m_Text(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		TextAreaAttribute_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(512);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TextAreaAttribute_t512 * tmp;
		tmp = (TextAreaAttribute_t512 *)il2cpp_codegen_object_new (TextAreaAttribute_t512_il2cpp_TypeInfo_var);
		TextAreaAttribute__ctor_m2525(tmp, 3, 10, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t279_0_0_0_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void Toggle_t357_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t279_0_0_0_var = il2cpp_codegen_type_from_index(382);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(RectTransform_t279_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Toggle"), 35, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Toggle_t357_CustomAttributesCacheGenerator_m_Group(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t511_il2cpp_TypeInfo_var;
void Toggle_t357_CustomAttributesCacheGenerator_m_IsOn(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		TooltipAttribute_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_IsActive"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t511 * tmp;
		tmp = (TooltipAttribute_t511 *)il2cpp_codegen_object_new (TooltipAttribute_t511_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Is the toggle currently on or off?"), NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void ToggleGroup_t356_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Toggle Group"), 36, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ToggleGroup_t356_CustomAttributesCacheGenerator_m_AllowSwitchOff(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ToggleGroup_t356_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ToggleGroup_t356_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ToggleGroup_t356_CustomAttributesCacheGenerator_ToggleGroup_U3CAnyTogglesOnU3Em__7_m1738(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ToggleGroup_t356_CustomAttributesCacheGenerator_ToggleGroup_U3CActiveTogglesU3Em__8_m1739(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t279_0_0_0_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t506_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void AspectRatioFitter_t362_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t279_0_0_0_var = il2cpp_codegen_type_from_index(382);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		ExecuteInEditMode_t506_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(506);
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(RectTransform_t279_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t506 * tmp;
		tmp = (ExecuteInEditMode_t506 *)il2cpp_codegen_object_new (ExecuteInEditMode_t506_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2514(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("Layout/Aspect Ratio Fitter"), 142, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void AspectRatioFitter_t362_CustomAttributesCacheGenerator_m_AspectMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void AspectRatioFitter_t362_CustomAttributesCacheGenerator_m_AspectRatio(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* Canvas_t281_0_0_0_var;
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t506_il2cpp_TypeInfo_var;
void CanvasScaler_t366_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t281_0_0_0_var = il2cpp_codegen_type_from_index(383);
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		ExecuteInEditMode_t506_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("Layout/Canvas Scaler"), 101, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(Canvas_t281_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t506 * tmp;
		tmp = (ExecuteInEditMode_t506 *)il2cpp_codegen_object_new (ExecuteInEditMode_t506_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2514(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t511_il2cpp_TypeInfo_var;
void CanvasScaler_t366_CustomAttributesCacheGenerator_m_UiScaleMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		TooltipAttribute_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t511 * tmp;
		tmp = (TooltipAttribute_t511 *)il2cpp_codegen_object_new (TooltipAttribute_t511_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Determines how UI elements in the Canvas are scaled."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t511_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void CanvasScaler_t366_CustomAttributesCacheGenerator_m_ReferencePixelsPerUnit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t511 * tmp;
		tmp = (TooltipAttribute_t511 *)il2cpp_codegen_object_new (TooltipAttribute_t511_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("If a sprite has this 'Pixels Per Unit' setting, then one pixel in the sprite will cover one unit in the UI."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t511_il2cpp_TypeInfo_var;
void CanvasScaler_t366_CustomAttributesCacheGenerator_m_ScaleFactor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		TooltipAttribute_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t511 * tmp;
		tmp = (TooltipAttribute_t511 *)il2cpp_codegen_object_new (TooltipAttribute_t511_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Scales all UI elements in the Canvas by this factor."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t511_il2cpp_TypeInfo_var;
void CanvasScaler_t366_CustomAttributesCacheGenerator_m_ReferenceResolution(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		TooltipAttribute_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t511 * tmp;
		tmp = (TooltipAttribute_t511 *)il2cpp_codegen_object_new (TooltipAttribute_t511_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("The resolution the UI layout is designed for. If the screen resolution is larger, the UI will be scaled up, and if it's smaller, the UI will be scaled down. This is done in accordance with the Screen Match Mode."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t511_il2cpp_TypeInfo_var;
void CanvasScaler_t366_CustomAttributesCacheGenerator_m_ScreenMatchMode(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		TooltipAttribute_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t511 * tmp;
		tmp = (TooltipAttribute_t511 *)il2cpp_codegen_object_new (TooltipAttribute_t511_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("A mode used to scale the canvas area if the aspect ratio of the current resolution doesn't fit the reference resolution."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* RangeAttribute_t505_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t511_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void CanvasScaler_t366_CustomAttributesCacheGenerator_m_MatchWidthOrHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RangeAttribute_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(505);
		TooltipAttribute_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RangeAttribute_t505 * tmp;
		tmp = (RangeAttribute_t505 *)il2cpp_codegen_object_new (RangeAttribute_t505_il2cpp_TypeInfo_var);
		RangeAttribute__ctor_m2513(tmp, 0.0f, 1.0f, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t511 * tmp;
		tmp = (TooltipAttribute_t511 *)il2cpp_codegen_object_new (TooltipAttribute_t511_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("Determines if the scaling is using the width or height as reference, or a mix in between."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t511_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void CanvasScaler_t366_CustomAttributesCacheGenerator_m_PhysicalUnit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t511 * tmp;
		tmp = (TooltipAttribute_t511 *)il2cpp_codegen_object_new (TooltipAttribute_t511_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("The physical unit to specify positions and sizes in."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* TooltipAttribute_t511_il2cpp_TypeInfo_var;
void CanvasScaler_t366_CustomAttributesCacheGenerator_m_FallbackScreenDPI(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		TooltipAttribute_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		TooltipAttribute_t511 * tmp;
		tmp = (TooltipAttribute_t511 *)il2cpp_codegen_object_new (TooltipAttribute_t511_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("The DPI to assume if the screen DPI is not known."), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t511_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void CanvasScaler_t366_CustomAttributesCacheGenerator_m_DefaultSpriteDPI(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t511 * tmp;
		tmp = (TooltipAttribute_t511 *)il2cpp_codegen_object_new (TooltipAttribute_t511_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("The pixels per inch to use for sprites that have a 'Pixels Per Unit' setting that matches the 'Reference Pixels Per Unit' setting."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* TooltipAttribute_t511_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void CanvasScaler_t366_CustomAttributesCacheGenerator_m_DynamicPixelsPerUnit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		TooltipAttribute_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(511);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		TooltipAttribute_t511 * tmp;
		tmp = (TooltipAttribute_t511 *)il2cpp_codegen_object_new (TooltipAttribute_t511_il2cpp_TypeInfo_var);
		TooltipAttribute__ctor_m2522(tmp, il2cpp_codegen_string_new_wrapper("The amount of pixels per unit to use for dynamically created bitmaps in the UI, such as Text."), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t279_0_0_0_var;
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t506_il2cpp_TypeInfo_var;
void ContentSizeFitter_t368_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t279_0_0_0_var = il2cpp_codegen_type_from_index(382);
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		ExecuteInEditMode_t506_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("Layout/Content Size Fitter"), 141, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(RectTransform_t279_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t506 * tmp;
		tmp = (ExecuteInEditMode_t506 *)il2cpp_codegen_object_new (ExecuteInEditMode_t506_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2514(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ContentSizeFitter_t368_CustomAttributesCacheGenerator_m_HorizontalFit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void ContentSizeFitter_t368_CustomAttributesCacheGenerator_m_VerticalFit(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void GridLayoutGroup_t372_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("Layout/Grid Layout Group"), 152, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GridLayoutGroup_t372_CustomAttributesCacheGenerator_m_StartCorner(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GridLayoutGroup_t372_CustomAttributesCacheGenerator_m_StartAxis(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GridLayoutGroup_t372_CustomAttributesCacheGenerator_m_CellSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GridLayoutGroup_t372_CustomAttributesCacheGenerator_m_Spacing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GridLayoutGroup_t372_CustomAttributesCacheGenerator_m_Constraint(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void GridLayoutGroup_t372_CustomAttributesCacheGenerator_m_ConstraintCount(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void HorizontalLayoutGroup_t374_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("Layout/Horizontal Layout Group"), 150, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void HorizontalOrVerticalLayoutGroup_t375_CustomAttributesCacheGenerator_m_Spacing(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void HorizontalOrVerticalLayoutGroup_t375_CustomAttributesCacheGenerator_m_ChildForceExpandWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void HorizontalOrVerticalLayoutGroup_t375_CustomAttributesCacheGenerator_m_ChildForceExpandHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t279_0_0_0_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditMode_t506_il2cpp_TypeInfo_var;
void LayoutElement_t376_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t279_0_0_0_var = il2cpp_codegen_type_from_index(382);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		ExecuteInEditMode_t506_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(RectTransform_t279_0_0_0_var), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("Layout/Layout Element"), 140, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		ExecuteInEditMode_t506 * tmp;
		tmp = (ExecuteInEditMode_t506 *)il2cpp_codegen_object_new (ExecuteInEditMode_t506_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2514(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void LayoutElement_t376_CustomAttributesCacheGenerator_m_IgnoreLayout(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void LayoutElement_t376_CustomAttributesCacheGenerator_m_MinWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void LayoutElement_t376_CustomAttributesCacheGenerator_m_MinHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void LayoutElement_t376_CustomAttributesCacheGenerator_m_PreferredWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void LayoutElement_t376_CustomAttributesCacheGenerator_m_PreferredHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void LayoutElement_t376_CustomAttributesCacheGenerator_m_FlexibleWidth(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void LayoutElement_t376_CustomAttributesCacheGenerator_m_FlexibleHeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const Il2CppType* RectTransform_t279_0_0_0_var;
extern TypeInfo* ExecuteInEditMode_t506_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t170_il2cpp_TypeInfo_var;
extern TypeInfo* DisallowMultipleComponent_t507_il2cpp_TypeInfo_var;
void LayoutGroup_t373_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t279_0_0_0_var = il2cpp_codegen_type_from_index(382);
		ExecuteInEditMode_t506_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(506);
		RequireComponent_t170_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(88);
		DisallowMultipleComponent_t507_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(507);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 3;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t506 * tmp;
		tmp = (ExecuteInEditMode_t506 *)il2cpp_codegen_object_new (ExecuteInEditMode_t506_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2514(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		RequireComponent_t170 * tmp;
		tmp = (RequireComponent_t170 *)il2cpp_codegen_object_new (RequireComponent_t170_il2cpp_TypeInfo_var);
		RequireComponent__ctor_m539(tmp, il2cpp_codegen_type_get_object(RectTransform_t279_0_0_0_var), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
	{
		DisallowMultipleComponent_t507 * tmp;
		tmp = (DisallowMultipleComponent_t507 *)il2cpp_codegen_object_new (DisallowMultipleComponent_t507_il2cpp_TypeInfo_var);
		DisallowMultipleComponent__ctor_m2515(tmp, NULL);
		cache->attributes[2] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void LayoutGroup_t373_CustomAttributesCacheGenerator_m_Padding(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void LayoutGroup_t373_CustomAttributesCacheGenerator_m_ChildAlignment(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_Alignment"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutRebuilder_t381_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutRebuilder_t381_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutRebuilder_t381_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutRebuilder_t381_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutRebuilder_t381_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutRebuilder_t381_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__9_m1899(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutRebuilder_t381_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__A_m1900(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutRebuilder_t381_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__B_m1901(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutRebuilder_t381_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__C_m1902(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutRebuilder_t381_CustomAttributesCacheGenerator_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1903(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinWidthU3Em__E_m1915(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__F_m1916(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__10_m1917(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1918(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinHeightU3Em__12_m1919(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__13_m1920(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__14_m1921(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1922(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void VerticalLayoutGroup_t384_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("Layout/Vertical Layout Group"), 151, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t506_il2cpp_TypeInfo_var;
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void Mask_t385_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t506_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(506);
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t506 * tmp;
		tmp = (ExecuteInEditMode_t506 *)il2cpp_codegen_object_new (ExecuteInEditMode_t506_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2514(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Mask"), 13, NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
extern TypeInfo* FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var;
void Mask_t385_CustomAttributesCacheGenerator_m_ShowMaskGraphic(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(502);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 2;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
	{
		FormerlySerializedAsAttribute_t498 * tmp;
		tmp = (FormerlySerializedAsAttribute_t498 *)il2cpp_codegen_object_new (FormerlySerializedAsAttribute_t498_il2cpp_TypeInfo_var);
		FormerlySerializedAsAttribute__ctor_m2477(tmp, il2cpp_codegen_string_new_wrapper("m_ShowGraphic"), NULL);
		cache->attributes[1] = (Il2CppObject*)tmp;
	}
}
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttribute.h"
// System.Reflection.DefaultMemberAttribute
#include "mscorlib_System_Reflection_DefaultMemberAttributeMethodDeclarations.h"
extern TypeInfo* DefaultMemberAttribute_t514_il2cpp_TypeInfo_var;
void IndexedSet_1_t513_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DefaultMemberAttribute_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(513);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		DefaultMemberAttribute_t514 * tmp;
		tmp = (DefaultMemberAttribute_t514 *)il2cpp_codegen_object_new (DefaultMemberAttribute_t514_il2cpp_TypeInfo_var);
		DefaultMemberAttribute__ctor_m2543(tmp, il2cpp_codegen_string_new_wrapper("Item"), NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CanvasListPool_t388_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void CanvasListPool_t388_CustomAttributesCacheGenerator_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1944(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ComponentListPool_t391_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ComponentListPool_t391_CustomAttributesCacheGenerator_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1948(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ObjectPool_1_t515_CustomAttributesCacheGenerator_U3CcountAllU3Ek__BackingField(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ObjectPool_1_t515_CustomAttributesCacheGenerator_ObjectPool_1_get_countAll_m2562(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var;
void ObjectPool_1_t515_CustomAttributesCacheGenerator_ObjectPool_1_set_countAll_m2563(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		CompilerGeneratedAttribute_t169 * tmp;
		tmp = (CompilerGeneratedAttribute_t169 *)il2cpp_codegen_object_new (CompilerGeneratedAttribute_t169_il2cpp_TypeInfo_var);
		CompilerGeneratedAttribute__ctor_m537(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* ExecuteInEditMode_t506_il2cpp_TypeInfo_var;
void BaseVertexEffect_t392_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t506_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(506);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		ExecuteInEditMode_t506 * tmp;
		tmp = (ExecuteInEditMode_t506 *)il2cpp_codegen_object_new (ExecuteInEditMode_t506_il2cpp_TypeInfo_var);
		ExecuteInEditMode__ctor_m2514(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void Outline_t393_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Outline"), 15, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void PositionAsUV1_t395_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Position As UV1"), 16, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* AddComponentMenu_t497_il2cpp_TypeInfo_var;
void Shadow_t394_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AddComponentMenu_t497_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(501);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		AddComponentMenu_t497 * tmp;
		tmp = (AddComponentMenu_t497 *)il2cpp_codegen_object_new (AddComponentMenu_t497_il2cpp_TypeInfo_var);
		AddComponentMenu__ctor_m2509(tmp, il2cpp_codegen_string_new_wrapper("UI/Effects/Shadow"), 14, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Shadow_t394_CustomAttributesCacheGenerator_m_EffectColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Shadow_t394_CustomAttributesCacheGenerator_m_EffectDistance(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern TypeInfo* SerializeField_t168_il2cpp_TypeInfo_var;
void Shadow_t394_CustomAttributesCacheGenerator_m_UseGraphicAlpha(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SerializeField_t168_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	cache->count = 1;
	cache->attributes = (Il2CppObject**)il2cpp_gc_alloc_fixed(sizeof(Object_t *) * cache->count, 0);
	{
		SerializeField_t168 * tmp;
		tmp = (SerializeField_t168 *)il2cpp_codegen_object_new (SerializeField_t168_il2cpp_TypeInfo_var);
		SerializeField__ctor_m536(tmp, NULL);
		cache->attributes[0] = (Il2CppObject*)tmp;
	}
}
extern const CustomAttributesCacheGenerator g_UnityEngine_UI_Assembly_AttributeGenerators[350] = 
{
	NULL,
	g_UnityEngine_UI_Assembly_CustomAttributesCacheGenerator,
	EventHandle_t201_CustomAttributesCacheGenerator,
	EventSystem_t116_CustomAttributesCacheGenerator,
	EventSystem_t116_CustomAttributesCacheGenerator_m_FirstSelected,
	EventSystem_t116_CustomAttributesCacheGenerator_m_sendNavigationEvents,
	EventSystem_t116_CustomAttributesCacheGenerator_m_DragThreshold,
	EventSystem_t116_CustomAttributesCacheGenerator_U3CcurrentU3Ek__BackingField,
	EventSystem_t116_CustomAttributesCacheGenerator_EventSystem_get_current_m331,
	EventSystem_t116_CustomAttributesCacheGenerator_EventSystem_set_current_m784,
	EventSystem_t116_CustomAttributesCacheGenerator_EventSystem_t116____lastSelectedGameObject_PropertyInfo,
	EventTrigger_t211_CustomAttributesCacheGenerator,
	EventTrigger_t211_CustomAttributesCacheGenerator_m_Delegates,
	EventTrigger_t211_CustomAttributesCacheGenerator_delegates,
	ExecuteEvents_t233_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache13,
	ExecuteEvents_t233_CustomAttributesCacheGenerator_ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m867,
	AxisEventData_t239_CustomAttributesCacheGenerator_U3CmoveVectorU3Ek__BackingField,
	AxisEventData_t239_CustomAttributesCacheGenerator_U3CmoveDirU3Ek__BackingField,
	AxisEventData_t239_CustomAttributesCacheGenerator_AxisEventData_get_moveVector_m892,
	AxisEventData_t239_CustomAttributesCacheGenerator_AxisEventData_set_moveVector_m893,
	AxisEventData_t239_CustomAttributesCacheGenerator_AxisEventData_get_moveDir_m894,
	AxisEventData_t239_CustomAttributesCacheGenerator_AxisEventData_set_moveDir_m895,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CpointerEnterU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3ClastPressU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CrawPointerPressU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CpointerDragU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CpointerCurrentRaycastU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CpointerPressRaycastU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CeligibleForClickU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CpointerIdU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CpositionU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CdeltaU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CpressPositionU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CworldPositionU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CworldNormalU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CclickTimeU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CclickCountU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CscrollDeltaU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CuseDragThresholdU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CdraggingU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_U3CbuttonU3Ek__BackingField,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_pointerEnter_m904,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_pointerEnter_m905,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_lastPress_m906,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_lastPress_m907,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_rawPointerPress_m908,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_rawPointerPress_m909,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_pointerDrag_m910,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_pointerDrag_m911,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_pointerCurrentRaycast_m912,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_pointerCurrentRaycast_m913,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_pointerPressRaycast_m914,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_pointerPressRaycast_m915,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_eligibleForClick_m916,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_eligibleForClick_m917,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_pointerId_m918,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_pointerId_m919,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_position_m920,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_position_m921,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_delta_m922,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_delta_m923,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_pressPosition_m924,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_pressPosition_m925,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_worldPosition_m926,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_worldPosition_m927,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_worldNormal_m928,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_worldNormal_m929,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_clickTime_m930,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_clickTime_m931,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_clickCount_m932,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_clickCount_m933,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_scrollDelta_m934,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_scrollDelta_m935,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_useDragThreshold_m936,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_useDragThreshold_m937,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_dragging_m938,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_dragging_m939,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_get_button_m940,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_set_button_m941,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_t243____worldPosition_PropertyInfo,
	PointerEventData_t243_CustomAttributesCacheGenerator_PointerEventData_t243____worldNormal_PropertyInfo,
	BaseInputModule_t203_CustomAttributesCacheGenerator,
	StandaloneInputModule_t252_CustomAttributesCacheGenerator,
	StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_HorizontalAxis,
	StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_VerticalAxis,
	StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_SubmitButton,
	StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_CancelButton,
	StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_InputActionsPerSecond,
	StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_RepeatDelay,
	StandaloneInputModule_t252_CustomAttributesCacheGenerator_m_AllowActivationOnMobileDevice,
	StandaloneInputModule_t252_CustomAttributesCacheGenerator_StandaloneInputModule_t252____inputMode_PropertyInfo,
	InputMode_t251_CustomAttributesCacheGenerator,
	TouchInputModule_t253_CustomAttributesCacheGenerator,
	TouchInputModule_t253_CustomAttributesCacheGenerator_m_AllowActivationOnStandalone,
	BaseRaycaster_t237_CustomAttributesCacheGenerator_BaseRaycaster_t237____priority_PropertyInfo,
	Physics2DRaycaster_t254_CustomAttributesCacheGenerator,
	PhysicsRaycaster_t255_CustomAttributesCacheGenerator,
	PhysicsRaycaster_t255_CustomAttributesCacheGenerator_m_EventMask,
	PhysicsRaycaster_t255_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	PhysicsRaycaster_t255_CustomAttributesCacheGenerator_PhysicsRaycaster_U3CRaycastU3Em__1_m1050,
	TweenRunner_1_t502_CustomAttributesCacheGenerator_TweenRunner_1_Start_m2499,
	U3CStartU3Ec__Iterator0_t504_CustomAttributesCacheGenerator,
	U3CStartU3Ec__Iterator0_t504_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2504,
	U3CStartU3Ec__Iterator0_t504_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2505,
	U3CStartU3Ec__Iterator0_t504_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Dispose_m2507,
	U3CStartU3Ec__Iterator0_t504_CustomAttributesCacheGenerator_U3CStartU3Ec__Iterator0_Reset_m2508,
	AnimationTriggers_t261_CustomAttributesCacheGenerator_m_NormalTrigger,
	AnimationTriggers_t261_CustomAttributesCacheGenerator_m_HighlightedTrigger,
	AnimationTriggers_t261_CustomAttributesCacheGenerator_m_PressedTrigger,
	AnimationTriggers_t261_CustomAttributesCacheGenerator_m_DisabledTrigger,
	Button_t264_CustomAttributesCacheGenerator,
	Button_t264_CustomAttributesCacheGenerator_m_OnClick,
	Button_t264_CustomAttributesCacheGenerator_Button_OnFinishSubmit_m1089,
	U3COnFinishSubmitU3Ec__Iterator1_t265_CustomAttributesCacheGenerator,
	U3COnFinishSubmitU3Ec__Iterator1_t265_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1078,
	U3COnFinishSubmitU3Ec__Iterator1_t265_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1079,
	U3COnFinishSubmitU3Ec__Iterator1_t265_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Dispose_m1081,
	U3COnFinishSubmitU3Ec__Iterator1_t265_CustomAttributesCacheGenerator_U3COnFinishSubmitU3Ec__Iterator1_Reset_m1082,
	CanvasUpdateRegistry_t268_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	CanvasUpdateRegistry_t268_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7,
	CanvasUpdateRegistry_t268_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__2_m1106,
	CanvasUpdateRegistry_t268_CustomAttributesCacheGenerator_CanvasUpdateRegistry_U3CPerformUpdateU3Em__3_m1107,
	ColorBlock_t272_CustomAttributesCacheGenerator_m_NormalColor,
	ColorBlock_t272_CustomAttributesCacheGenerator_m_HighlightedColor,
	ColorBlock_t272_CustomAttributesCacheGenerator_m_PressedColor,
	ColorBlock_t272_CustomAttributesCacheGenerator_m_DisabledColor,
	ColorBlock_t272_CustomAttributesCacheGenerator_m_ColorMultiplier,
	ColorBlock_t272_CustomAttributesCacheGenerator_m_FadeDuration,
	FontData_t274_CustomAttributesCacheGenerator_m_Font,
	FontData_t274_CustomAttributesCacheGenerator_m_FontSize,
	FontData_t274_CustomAttributesCacheGenerator_m_FontStyle,
	FontData_t274_CustomAttributesCacheGenerator_m_BestFit,
	FontData_t274_CustomAttributesCacheGenerator_m_MinSize,
	FontData_t274_CustomAttributesCacheGenerator_m_MaxSize,
	FontData_t274_CustomAttributesCacheGenerator_m_Alignment,
	FontData_t274_CustomAttributesCacheGenerator_m_RichText,
	FontData_t274_CustomAttributesCacheGenerator_m_HorizontalOverflow,
	FontData_t274_CustomAttributesCacheGenerator_m_VerticalOverflow,
	FontData_t274_CustomAttributesCacheGenerator_m_LineSpacing,
	Graphic_t285_CustomAttributesCacheGenerator,
	Graphic_t285_CustomAttributesCacheGenerator_m_Material,
	Graphic_t285_CustomAttributesCacheGenerator_m_Color,
	Graphic_t285_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheE,
	Graphic_t285_CustomAttributesCacheGenerator_U3CU3Ef__amU24cacheF,
	Graphic_t285_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__4_m1196,
	Graphic_t285_CustomAttributesCacheGenerator_Graphic_U3Cs_VboPoolU3Em__5_m1197,
	GraphicRaycaster_t289_CustomAttributesCacheGenerator,
	GraphicRaycaster_t289_CustomAttributesCacheGenerator_m_IgnoreReversedGraphics,
	GraphicRaycaster_t289_CustomAttributesCacheGenerator_m_BlockingObjects,
	GraphicRaycaster_t289_CustomAttributesCacheGenerator_m_BlockingMask,
	GraphicRaycaster_t289_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	GraphicRaycaster_t289_CustomAttributesCacheGenerator_GraphicRaycaster_U3CRaycastU3Em__6_m1212,
	Image_t301_CustomAttributesCacheGenerator,
	Image_t301_CustomAttributesCacheGenerator_m_Sprite,
	Image_t301_CustomAttributesCacheGenerator_m_Type,
	Image_t301_CustomAttributesCacheGenerator_m_PreserveAspect,
	Image_t301_CustomAttributesCacheGenerator_m_FillCenter,
	Image_t301_CustomAttributesCacheGenerator_m_FillMethod,
	Image_t301_CustomAttributesCacheGenerator_m_FillAmount,
	Image_t301_CustomAttributesCacheGenerator_m_FillClockwise,
	Image_t301_CustomAttributesCacheGenerator_m_FillOrigin,
	InputField_t12_CustomAttributesCacheGenerator,
	InputField_t12_CustomAttributesCacheGenerator_m_TextComponent,
	InputField_t12_CustomAttributesCacheGenerator_m_Placeholder,
	InputField_t12_CustomAttributesCacheGenerator_m_ContentType,
	InputField_t12_CustomAttributesCacheGenerator_m_InputType,
	InputField_t12_CustomAttributesCacheGenerator_m_AsteriskChar,
	InputField_t12_CustomAttributesCacheGenerator_m_KeyboardType,
	InputField_t12_CustomAttributesCacheGenerator_m_LineType,
	InputField_t12_CustomAttributesCacheGenerator_m_HideMobileInput,
	InputField_t12_CustomAttributesCacheGenerator_m_CharacterValidation,
	InputField_t12_CustomAttributesCacheGenerator_m_CharacterLimit,
	InputField_t12_CustomAttributesCacheGenerator_m_EndEdit,
	InputField_t12_CustomAttributesCacheGenerator_m_OnValueChange,
	InputField_t12_CustomAttributesCacheGenerator_m_OnValidateInput,
	InputField_t12_CustomAttributesCacheGenerator_m_SelectionColor,
	InputField_t12_CustomAttributesCacheGenerator_m_Text,
	InputField_t12_CustomAttributesCacheGenerator_m_CaretBlinkRate,
	InputField_t12_CustomAttributesCacheGenerator_InputField_CaretBlink_m1337,
	InputField_t12_CustomAttributesCacheGenerator_InputField_MouseDragOutsideRect_m1354,
	InputField_t12_CustomAttributesCacheGenerator_InputField_t12_InputField_SetToCustomIfContentTypeIsNot_m1405_Arg0_ParameterInfo,
	U3CCaretBlinkU3Ec__Iterator2_t315_CustomAttributesCacheGenerator,
	U3CCaretBlinkU3Ec__Iterator2_t315_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1275,
	U3CCaretBlinkU3Ec__Iterator2_t315_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1276,
	U3CCaretBlinkU3Ec__Iterator2_t315_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Dispose_m1278,
	U3CCaretBlinkU3Ec__Iterator2_t315_CustomAttributesCacheGenerator_U3CCaretBlinkU3Ec__Iterator2_Reset_m1279,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t316_CustomAttributesCacheGenerator,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t316_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1281,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t316_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1282,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t316_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Dispose_m1284,
	U3CMouseDragOutsideRectU3Ec__Iterator3_t316_CustomAttributesCacheGenerator_U3CMouseDragOutsideRectU3Ec__Iterator3_Reset_m1285,
	Navigation_t326_CustomAttributesCacheGenerator_m_Mode,
	Navigation_t326_CustomAttributesCacheGenerator_m_SelectOnUp,
	Navigation_t326_CustomAttributesCacheGenerator_m_SelectOnDown,
	Navigation_t326_CustomAttributesCacheGenerator_m_SelectOnLeft,
	Navigation_t326_CustomAttributesCacheGenerator_m_SelectOnRight,
	Mode_t325_CustomAttributesCacheGenerator,
	RawImage_t328_CustomAttributesCacheGenerator,
	RawImage_t328_CustomAttributesCacheGenerator_m_Texture,
	RawImage_t328_CustomAttributesCacheGenerator_m_UVRect,
	Scrollbar_t333_CustomAttributesCacheGenerator,
	Scrollbar_t333_CustomAttributesCacheGenerator_m_HandleRect,
	Scrollbar_t333_CustomAttributesCacheGenerator_m_Direction,
	Scrollbar_t333_CustomAttributesCacheGenerator_m_Value,
	Scrollbar_t333_CustomAttributesCacheGenerator_m_Size,
	Scrollbar_t333_CustomAttributesCacheGenerator_m_NumberOfSteps,
	Scrollbar_t333_CustomAttributesCacheGenerator_m_OnValueChanged,
	Scrollbar_t333_CustomAttributesCacheGenerator_Scrollbar_ClickRepeat_m1480,
	U3CClickRepeatU3Ec__Iterator4_t334_CustomAttributesCacheGenerator,
	U3CClickRepeatU3Ec__Iterator4_t334_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1446,
	U3CClickRepeatU3Ec__Iterator4_t334_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1447,
	U3CClickRepeatU3Ec__Iterator4_t334_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Dispose_m1449,
	U3CClickRepeatU3Ec__Iterator4_t334_CustomAttributesCacheGenerator_U3CClickRepeatU3Ec__Iterator4_Reset_m1450,
	ScrollRect_t339_CustomAttributesCacheGenerator,
	ScrollRect_t339_CustomAttributesCacheGenerator_m_Content,
	ScrollRect_t339_CustomAttributesCacheGenerator_m_Horizontal,
	ScrollRect_t339_CustomAttributesCacheGenerator_m_Vertical,
	ScrollRect_t339_CustomAttributesCacheGenerator_m_MovementType,
	ScrollRect_t339_CustomAttributesCacheGenerator_m_Elasticity,
	ScrollRect_t339_CustomAttributesCacheGenerator_m_Inertia,
	ScrollRect_t339_CustomAttributesCacheGenerator_m_DecelerationRate,
	ScrollRect_t339_CustomAttributesCacheGenerator_m_ScrollSensitivity,
	ScrollRect_t339_CustomAttributesCacheGenerator_m_HorizontalScrollbar,
	ScrollRect_t339_CustomAttributesCacheGenerator_m_VerticalScrollbar,
	ScrollRect_t339_CustomAttributesCacheGenerator_m_OnValueChanged,
	Selectable_t266_CustomAttributesCacheGenerator,
	Selectable_t266_CustomAttributesCacheGenerator_m_Navigation,
	Selectable_t266_CustomAttributesCacheGenerator_m_Transition,
	Selectable_t266_CustomAttributesCacheGenerator_m_Colors,
	Selectable_t266_CustomAttributesCacheGenerator_m_SpriteState,
	Selectable_t266_CustomAttributesCacheGenerator_m_AnimationTriggers,
	Selectable_t266_CustomAttributesCacheGenerator_m_Interactable,
	Selectable_t266_CustomAttributesCacheGenerator_m_TargetGraphic,
	Selectable_t266_CustomAttributesCacheGenerator_U3CisPointerInsideU3Ek__BackingField,
	Selectable_t266_CustomAttributesCacheGenerator_U3CisPointerDownU3Ek__BackingField,
	Selectable_t266_CustomAttributesCacheGenerator_U3ChasSelectionU3Ek__BackingField,
	Selectable_t266_CustomAttributesCacheGenerator_Selectable_get_isPointerInside_m1565,
	Selectable_t266_CustomAttributesCacheGenerator_Selectable_set_isPointerInside_m1566,
	Selectable_t266_CustomAttributesCacheGenerator_Selectable_get_isPointerDown_m1567,
	Selectable_t266_CustomAttributesCacheGenerator_Selectable_set_isPointerDown_m1568,
	Selectable_t266_CustomAttributesCacheGenerator_Selectable_get_hasSelection_m1569,
	Selectable_t266_CustomAttributesCacheGenerator_Selectable_set_hasSelection_m1570,
	Selectable_t266_CustomAttributesCacheGenerator_Selectable_IsPressed_m1596,
	Slider_t30_CustomAttributesCacheGenerator,
	Slider_t30_CustomAttributesCacheGenerator_m_FillRect,
	Slider_t30_CustomAttributesCacheGenerator_m_HandleRect,
	Slider_t30_CustomAttributesCacheGenerator_m_Direction,
	Slider_t30_CustomAttributesCacheGenerator_m_MinValue,
	Slider_t30_CustomAttributesCacheGenerator_m_MaxValue,
	Slider_t30_CustomAttributesCacheGenerator_m_WholeNumbers,
	Slider_t30_CustomAttributesCacheGenerator_m_Value,
	Slider_t30_CustomAttributesCacheGenerator_m_OnValueChanged,
	SpriteState_t345_CustomAttributesCacheGenerator_m_HighlightedSprite,
	SpriteState_t345_CustomAttributesCacheGenerator_m_PressedSprite,
	SpriteState_t345_CustomAttributesCacheGenerator_m_DisabledSprite,
	Text_t31_CustomAttributesCacheGenerator,
	Text_t31_CustomAttributesCacheGenerator_m_FontData,
	Text_t31_CustomAttributesCacheGenerator_m_Text,
	Toggle_t357_CustomAttributesCacheGenerator,
	Toggle_t357_CustomAttributesCacheGenerator_m_Group,
	Toggle_t357_CustomAttributesCacheGenerator_m_IsOn,
	ToggleGroup_t356_CustomAttributesCacheGenerator,
	ToggleGroup_t356_CustomAttributesCacheGenerator_m_AllowSwitchOff,
	ToggleGroup_t356_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	ToggleGroup_t356_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	ToggleGroup_t356_CustomAttributesCacheGenerator_ToggleGroup_U3CAnyTogglesOnU3Em__7_m1738,
	ToggleGroup_t356_CustomAttributesCacheGenerator_ToggleGroup_U3CActiveTogglesU3Em__8_m1739,
	AspectRatioFitter_t362_CustomAttributesCacheGenerator,
	AspectRatioFitter_t362_CustomAttributesCacheGenerator_m_AspectMode,
	AspectRatioFitter_t362_CustomAttributesCacheGenerator_m_AspectRatio,
	CanvasScaler_t366_CustomAttributesCacheGenerator,
	CanvasScaler_t366_CustomAttributesCacheGenerator_m_UiScaleMode,
	CanvasScaler_t366_CustomAttributesCacheGenerator_m_ReferencePixelsPerUnit,
	CanvasScaler_t366_CustomAttributesCacheGenerator_m_ScaleFactor,
	CanvasScaler_t366_CustomAttributesCacheGenerator_m_ReferenceResolution,
	CanvasScaler_t366_CustomAttributesCacheGenerator_m_ScreenMatchMode,
	CanvasScaler_t366_CustomAttributesCacheGenerator_m_MatchWidthOrHeight,
	CanvasScaler_t366_CustomAttributesCacheGenerator_m_PhysicalUnit,
	CanvasScaler_t366_CustomAttributesCacheGenerator_m_FallbackScreenDPI,
	CanvasScaler_t366_CustomAttributesCacheGenerator_m_DefaultSpriteDPI,
	CanvasScaler_t366_CustomAttributesCacheGenerator_m_DynamicPixelsPerUnit,
	ContentSizeFitter_t368_CustomAttributesCacheGenerator,
	ContentSizeFitter_t368_CustomAttributesCacheGenerator_m_HorizontalFit,
	ContentSizeFitter_t368_CustomAttributesCacheGenerator_m_VerticalFit,
	GridLayoutGroup_t372_CustomAttributesCacheGenerator,
	GridLayoutGroup_t372_CustomAttributesCacheGenerator_m_StartCorner,
	GridLayoutGroup_t372_CustomAttributesCacheGenerator_m_StartAxis,
	GridLayoutGroup_t372_CustomAttributesCacheGenerator_m_CellSize,
	GridLayoutGroup_t372_CustomAttributesCacheGenerator_m_Spacing,
	GridLayoutGroup_t372_CustomAttributesCacheGenerator_m_Constraint,
	GridLayoutGroup_t372_CustomAttributesCacheGenerator_m_ConstraintCount,
	HorizontalLayoutGroup_t374_CustomAttributesCacheGenerator,
	HorizontalOrVerticalLayoutGroup_t375_CustomAttributesCacheGenerator_m_Spacing,
	HorizontalOrVerticalLayoutGroup_t375_CustomAttributesCacheGenerator_m_ChildForceExpandWidth,
	HorizontalOrVerticalLayoutGroup_t375_CustomAttributesCacheGenerator_m_ChildForceExpandHeight,
	LayoutElement_t376_CustomAttributesCacheGenerator,
	LayoutElement_t376_CustomAttributesCacheGenerator_m_IgnoreLayout,
	LayoutElement_t376_CustomAttributesCacheGenerator_m_MinWidth,
	LayoutElement_t376_CustomAttributesCacheGenerator_m_MinHeight,
	LayoutElement_t376_CustomAttributesCacheGenerator_m_PreferredWidth,
	LayoutElement_t376_CustomAttributesCacheGenerator_m_PreferredHeight,
	LayoutElement_t376_CustomAttributesCacheGenerator_m_FlexibleWidth,
	LayoutElement_t376_CustomAttributesCacheGenerator_m_FlexibleHeight,
	LayoutGroup_t373_CustomAttributesCacheGenerator,
	LayoutGroup_t373_CustomAttributesCacheGenerator_m_Padding,
	LayoutGroup_t373_CustomAttributesCacheGenerator_m_ChildAlignment,
	LayoutRebuilder_t381_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	LayoutRebuilder_t381_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	LayoutRebuilder_t381_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4,
	LayoutRebuilder_t381_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5,
	LayoutRebuilder_t381_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	LayoutRebuilder_t381_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__9_m1899,
	LayoutRebuilder_t381_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__A_m1900,
	LayoutRebuilder_t381_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__B_m1901,
	LayoutRebuilder_t381_CustomAttributesCacheGenerator_LayoutRebuilder_U3CRebuildU3Em__C_m1902,
	LayoutRebuilder_t381_CustomAttributesCacheGenerator_LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1903,
	LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache0,
	LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache2,
	LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache3,
	LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache4,
	LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache5,
	LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache6,
	LayoutUtility_t383_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache7,
	LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinWidthU3Em__E_m1915,
	LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__F_m1916,
	LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredWidthU3Em__10_m1917,
	LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1918,
	LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetMinHeightU3Em__12_m1919,
	LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__13_m1920,
	LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetPreferredHeightU3Em__14_m1921,
	LayoutUtility_t383_CustomAttributesCacheGenerator_LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1922,
	VerticalLayoutGroup_t384_CustomAttributesCacheGenerator,
	Mask_t385_CustomAttributesCacheGenerator,
	Mask_t385_CustomAttributesCacheGenerator_m_ShowMaskGraphic,
	IndexedSet_1_t513_CustomAttributesCacheGenerator,
	CanvasListPool_t388_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	CanvasListPool_t388_CustomAttributesCacheGenerator_CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1944,
	ComponentListPool_t391_CustomAttributesCacheGenerator_U3CU3Ef__amU24cache1,
	ComponentListPool_t391_CustomAttributesCacheGenerator_ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1948,
	ObjectPool_1_t515_CustomAttributesCacheGenerator_U3CcountAllU3Ek__BackingField,
	ObjectPool_1_t515_CustomAttributesCacheGenerator_ObjectPool_1_get_countAll_m2562,
	ObjectPool_1_t515_CustomAttributesCacheGenerator_ObjectPool_1_set_countAll_m2563,
	BaseVertexEffect_t392_CustomAttributesCacheGenerator,
	Outline_t393_CustomAttributesCacheGenerator,
	PositionAsUV1_t395_CustomAttributesCacheGenerator,
	Shadow_t394_CustomAttributesCacheGenerator,
	Shadow_t394_CustomAttributesCacheGenerator_m_EffectColor,
	Shadow_t394_CustomAttributesCacheGenerator_m_EffectDistance,
	Shadow_t394_CustomAttributesCacheGenerator_m_UseGraphicAlpha,
};
