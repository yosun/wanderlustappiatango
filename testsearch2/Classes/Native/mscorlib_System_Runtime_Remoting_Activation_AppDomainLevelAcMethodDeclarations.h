﻿#pragma once
#include <stdint.h>
#include <assert.h>
#include <exception>
#include "codegen/il2cpp-codegen.h"

// System.Runtime.Remoting.Activation.AppDomainLevelActivator
struct AppDomainLevelActivator_t2300;
// System.String
struct String_t;
// System.Runtime.Remoting.Activation.IActivator
struct IActivator_t2298;

// System.Void System.Runtime.Remoting.Activation.AppDomainLevelActivator::.ctor(System.String,System.Runtime.Remoting.Activation.IActivator)
extern "C" void AppDomainLevelActivator__ctor_m12117 (AppDomainLevelActivator_t2300 * __this, String_t* ___activationUrl, Object_t * ___next, const MethodInfo* method) IL2CPP_METHOD_ATTR;
